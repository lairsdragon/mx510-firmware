/*
 *  Teltonika RUT5xx board support
 *
 *  Copyright (C) 2011 Sergiy <piratfm@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 2 as published
 *  by the Free Software Foundation.
 */

#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/spi/spi.h>
#include <linux/spi/flash.h>
#include <linux/gpio.h>

#include <asm/mach-ralink/machine.h>
#include <asm/mach-ralink/dev-gpio-buttons.h>
#include <asm/mach-ralink/dev-gpio-leds.h>
#include <asm/mach-ralink/rt305x.h>
#include <asm/mach-ralink/rt305x_regs.h>

#include "devices.h"

#define RUT5XX_GPIO_MODEM_RESET     7
#define RUT5XX_GPIO_MODEM_POWER     8
#define RUT5XX_GPIO_BUTTON_RESET	10	/* active low */
#define RUT5XX_GPIO_HW_WATCHDOG     11
#define RUT5XX_GPIO_LED_STATUS      14

#define RUT5XX_KEYS_POLL_INTERVAL	20
#define RUT5XX_KEYS_DEBOUNCE_INTERVAL	(3 * RUT5XX_KEYS_POLL_INTERVAL)

const struct flash_platform_data rut5xx_flash = {
	.type		= "mx25l6405d",
};

struct spi_board_info __initdata rut5xx_spi_slave_info[] = {
	{
		.modalias	= "m25p80",
		.platform_data	= &rut5xx_flash,
		.irq		= -1,
		.max_speed_hz	= 50000000,
		.bus_num	= 0,
		.chip_select	= 0,
	},
};

static struct gpio_led rut5xx_gpio_leds[] __initdata = {
	{
		.name		= "rut5xx:green:status",
		.gpio		= RUT5XX_GPIO_LED_STATUS,
		.active_low	= 1,
	}
};

static struct gpio_keys_button rut5xx_gpio_buttons[] __initdata = {
	{
		.desc		= "reset",
		.type		= EV_KEY,
		.code		= KEY_RESTART,
		.debounce_interval = RUT5XX_KEYS_DEBOUNCE_INTERVAL,
		.gpio		= RUT5XX_GPIO_BUTTON_RESET,
		.active_low	= 1,
	}
};

static void __init rut5xx_init(void)
{
	rt305x_gpio_init(RT305X_GPIO_MODE_GPIO << RT305X_GPIO_MODE_UART0_SHIFT);
	rt305x_register_spi(rut5xx_spi_slave_info,
			    ARRAY_SIZE(rut5xx_spi_slave_info));

	rt305x_esw_data.vlan_config = RT305X_ESW_VLAN_CONFIG_WLLLL;
	rt305x_register_ethernet();
	ramips_register_gpio_leds(-1, ARRAY_SIZE(rut5xx_gpio_leds),
				  rut5xx_gpio_leds);
	ramips_register_gpio_buttons(-1, RUT5XX_KEYS_POLL_INTERVAL,
			     ARRAY_SIZE(rut5xx_gpio_buttons),
			     rut5xx_gpio_buttons);

	/* Reset to the mini PCI port is controlled by GPIO line */
	gpio_request(RUT5XX_GPIO_MODEM_RESET, "RUT5XX_GPIO_MODEM_RESET");
	gpio_direction_output(RUT5XX_GPIO_MODEM_RESET, 1);
	gpio_free(RUT5XX_GPIO_MODEM_RESET);

	/* Power to the mini PCI port is controlled by GPIO line */
	gpio_request(RUT5XX_GPIO_MODEM_POWER, "RUT5XX_GPIO_MODEM_POWER");
	gpio_direction_output(RUT5XX_GPIO_MODEM_POWER, 1);
	gpio_free(RUT5XX_GPIO_MODEM_POWER);

	/* HW watchdog gpio */
	gpio_request(RUT5XX_GPIO_HW_WATCHDOG, "RUT5XX_GPIO_HW_WATCHDOG");
	gpio_direction_output(RUT5XX_GPIO_HW_WATCHDOG, 1);
	gpio_free(RUT5XX_GPIO_HW_WATCHDOG);

	rt305x_register_wifi();
	rt305x_register_wdt();
    rt305x_register_usb();
}

MIPS_MACHINE(RAMIPS_MACH_RUT5XX, "RUT5XX", "Teltonika RUT5XX",
	     rut5xx_init);
