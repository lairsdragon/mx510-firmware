--[[
LuCI - Lua Configuration Interface

Copyright 2008 Steven Barth <steven@midlink.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

$Id: index.lua 7789 2011-10-26 03:04:18Z jow $
]]--

module("luci.controller.admin.index", package.seeall)

function index()
	local root = node()
	local ExpertMode
	if not root.target then
		root.target = alias("admin")
		root.index = true
	end

	local page   = node("admin")
	
	x = uci.cursor()
	firstLogin = x:get("teltonika", "sys", "first_login")
	ExpertMode = x:get("teltonika", "sys", "expert")
	
	if firstLogin == "1" then
		page.target = alias("admin", "system", "wizard", "step-pwd")
	else
		page.target  = firstchild()
	end
	
	--page.target  = firstchild()
	page.title   = _("Administration")
	page.order   = 10
	page.sysauth = "root"
	page.sysauth_authenticator = "htmlauth"
	page.ucidata = true
	page.index = true

	-- Empty services menu to be populated by addons
	entry({"admin", "services"}, firstchild(), _("Services"), 40).index = true

	entry({"admin", "vpn-menu"}, firstchild(), _("VPN"), 42).index = true
	
	if ExpertMode and ExpertMode == "on" then
		entry({"admin", "expert_mode"}, call("action_expert_mode"), _("Expert mode: on"), 80)
	else
		entry({"admin", "expert_mode"}, call("action_expert_mode"), _("Expert mode: off"), 80)
	end	
	
	entry({"admin", "logout"}, call("action_logout"), _("Logout"), 79)
	
	
end

function action_logout()
	local dsp = require "luci.dispatcher"
	local sauth = require "luci.sauth"
	x = uci.cursor()
	if x:get("teltonika", "sys", "expert") then
		x:set("teltonika", "sys", "expert", "off")
		x:save("teltonika")
		x.commit("teltonika")
	end
	if dsp.context.authsession then
		sauth.kill(dsp.context.authsession)
		dsp.context.urltoken.stok = nil
	end

	luci.http.header("Set-Cookie", "sysauth=; path=" .. dsp.build_url())
	luci.http.redirect(luci.dispatcher.build_url())
end

function action_expert_mode()
	x = uci.cursor()
	local ExpertMode = x:get("teltonika", "sys", "expert")
	
	if ExpertMode and ExpertMode == "on" then
		x:set("teltonika", "sys", "expert", "off")
	elseif ExpertMode and ExpertMode == "off" then
		x:set("teltonika", "sys", "expert", "on")
	end
	x:save("teltonika")
	luci.http.redirect(luci.dispatcher.build_url())
end
