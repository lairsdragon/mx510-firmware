--[[
LuCI - Lua Configuration Interface

Copyright 2008 Steven Barth <steven@midlink.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

$Id: routes.lua 6447 2010-11-16 19:06:51Z jow $
]]--

require("luci.tools.webadmin")
m = Map("network",
	translate("Routes"),
	translate("Routes specify over which interface and gateway a certain host or network " ..
		"can be reached."))
		
m:chain("gre_tunnel")
m:chain("openvpn")

local routes6 = luci.sys.net.routes6()
local bit = require "bit"

s = m:section(TypedSection, "route", translate("Static IPv4 Routes"))
s.addremove = true
s.anonymous = true

s.template  = "cbi/tblsection"

iface = s:option(ListValue, "interface", translate("Interface"))
luci.tools.webadmin.cbi_add_networks(iface)

t = s:option(Value, "target", translate("Target"), translate("Host-IP or Network"))
t.datatype = "ip4addr"
t.rmempty = false

n = s:option(Value, "netmask", translate("IPv4-Netmask"), translate("if target is a network"))
n.placeholder = "255.255.255.255"
n.datatype = "ip4addr"
n.rmempty = true

g = s:option(Value, "gateway", translate("IPv4-Gateway"))
g.datatype = "ip4addr"
g.rmempty = true

metric = s:option(Value, "metric", translate("Metric"))
metric.placeholder = 0
metric.datatype = "range(0,255)"
metric.rmempty = true


if routes6 then
	s = m:section(TypedSection, "route6", translate("Static IPv6 Routes"))
	s.addremove = true
	s.anonymous = true

	s.template  = "cbi/tblsection"

	iface = s:option(ListValue, "interface", translate("Interface"))
	luci.tools.webadmin.cbi_add_networks(iface)

	t = s:option(Value, "target", translate("Target"), translate("IPv6-Address or Network (CIDR)"))
	t.datatype = "ip6addr"
	t.rmempty = false

	g = s:option(Value, "gateway", translate("IPv6-Gateway"))
	g.datatype = "ip6addr"
	g.rmempty = true

	metric = s:option(Value, "metric", translate("Metric"))
	metric.placeholder = 0
	metric.datatype = "range(0,65535)" -- XXX: not sure
	metric.rmempty = true

end


return m
