--[[
LuCI - Lua Configuration Interface

Copyright 2008 Steven Barth <steven@midlink.org>
Copyright 2008-2011 Jo-Philipp Wich <xm@subsignal.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

$Id: ifaces.lua 7717 2011-10-13 16:26:59Z jow $
]]--

local fs = require "nixio.fs"
local ut = require "luci.util"
local nw = require "luci.model.network"
local fw = require "luci.model.firewall"

arg[1] = arg[1] or ""

arg[1] = "wwan"

local isWan, isLan

isWan = true
isLan = false

local has_dnsmasq  = fs.access("/etc/config/dhcp")
local has_firewall = fs.access("/etc/config/firewall")

m = Map("network", translate("Interfaces") .. " - " .. arg[1]:upper(), translate("On this page you can configure the network interfaces. You can bridge several interfaces by ticking the \"bridge interfaces\" field and enter the names of several network interfaces separated by spaces. You can also use <abbr title=\"Virtual Local Area Network\">VLAN</abbr> notation <samp>INTERFACE.VLANNR</samp> (<abbr title=\"exempli gratia (which is latin for 'for example', in case you wondered...)\">e.g.</abbr>: <samp>eth0.1</samp>)."))
m:chain("wireless")

if has_firewall then
	m:chain("firewall")
end

nw.init(m.uci)
fw.init(m.uci)

local net = nw:get_network(arg[1])

local function backup_ifnames(is_bridge)
	if not net:is_floating() and not m:get(net:name(), "_orig_ifname") then
		local ifcs = net:get_interfaces() or { net:get_interface() }
		if ifcs then
			local _, ifn
			local ifns = { }
			for _, ifn in ipairs(ifcs) do
				ifns[#ifns+1] = ifn:name()
			end
			if #ifns > 0 then
				m:set(net:name(), "_orig_ifname", table.concat(ifns, " "))
				m:set(net:name(), "_orig_bridge", tostring(net:is_bridge()))
			end
		end
	end
end


-- redirect to overview page if network does not exist anymore (e.g. after a revert)
if not net then
	luci.http.redirect(luci.dispatcher.build_url("admin/network/network"))
	return
end

-- protocol switch was requested, rebuild interface config and reload page
if m:formvalue("cbid.network.%s._switch" % net:name()) then
	-- get new protocol
	local ptype = m:formvalue("cbid.network.%s.proto" % net:name()) or "-"
	local proto = nw:get_protocol(ptype, net:name())
	if proto then
		-- backup default
		backup_ifnames()

		-- if current proto is not floating and target proto is not floating,
		-- then attempt to retain the ifnames
		--error(net:proto() .. " > " .. proto:proto())
		if not net:is_floating() and not proto:is_floating() then
			-- if old proto is a bridge and new proto not, then clip the
			-- interface list to the first ifname only
			if net:is_bridge() and proto:is_virtual() then
				local _, ifn
				local first = true
				for _, ifn in ipairs(net:get_interfaces() or { net:get_interface() }) do
					if first then
						first = false
					else
						net:del_interface(ifn)
					end
				end
				m:del(net:name(), "type")
			end

		-- if the current proto is floating, the target proto not floating,
		-- then attempt to restore ifnames from backup
		elseif net:is_floating() and not proto:is_floating() then
			-- if we have backup data, then re-add all orphaned interfaces
			-- from it and restore the bridge choice
			local br = (m:get(net:name(), "_orig_bridge") == "true")
			local ifn
			local ifns = { }
			for ifn in ut.imatch(m:get(net:name(), "_orig_ifname")) do
				ifn = nw:get_interface(ifn)
				if ifn and not ifn:get_network() then
					proto:add_interface(ifn)
					if not br then
						break
					end
				end
			end
			if br then
				m:set(net:name(), "type", "bridge")
			end

		-- in all other cases clear the ifnames
		else
			local _, ifc
			for _, ifc in ipairs(net:get_interfaces() or { net:get_interface() }) do
				net:del_interface(ifc)
			end
			m:del(net:name(), "type")
		end

		-- clear options
		local k, v
		for k, v in pairs(m:get(net:name())) do
			if k:sub(1,1) ~= "." and
			   k ~= "type" and
			   k ~= "ifname" and
			   k ~= "_orig_ifname" and
			   k ~= "_orig_bridge"
			then
				m:del(net:name(), k)
			end
		end

		-- set proto
		m:set(net:name(), "proto", proto:proto())
		m.uci:save("network")
		m.uci:save("wireless")

		-- reload page
		luci.http.redirect(luci.dispatcher.build_url("admin/network/wwan"))
		return
	end
end

-- dhcp setup was requested, create section and reload page
if m:formvalue("cbid.dhcp._enable._enable") then
	m.uci:section("dhcp", "dhcp", nil, {
		interface = arg[1],
		start     = "100",
		limit     = "150",
		leasetime = "12h"
	})

	m.uci:save("dhcp")
	luci.http.redirect(luci.dispatcher.build_url("admin/network/wan"))
	return
end

local ifc = net:get_interface()
local ifcName = ifc:name()

--luci.sys.call("echo \"ifaces.lua: ifcName: "..ifcName.."\" >> /tmp/log.log")

local is3G
if ifcName == "usb0" then
	is3G = true
else 
	is3G = false
end

--luci.sys.call("echo \"ifaces.lua: ["..ifc:name().."]\" >> /tmp/log.log")

-- Premtively create mode switch section so it would stay on top --
if isWan then
	sModeSwitch = m:section(NamedSection, arg[1], "interface", translate("Common configuration"))
	sModeSwitch.addremove = false
end

-- Bac to normal work --
s = m:section(NamedSection, arg[1], "interface", translate("Common Configuration"))
s.addremove = false

s:tab("general",  translate("General Setup"))
s:tab("advanced", translate("Advanced Settings"))
s:tab("physical", translate("Physical Settings"))

if has_firewall then
	s:tab("firewall", translate("Firewall Settings"))
end

-- Interface status --
--[[st = s:taboption("general", DummyValue, "__status", translate("Status"))

local function set_status()
	-- if current network is empty, print a warning
	if not net:is_floating() and net:is_empty() then
		st.template = "cbi/dvalue"
		st.network  = nil
		st.value    = translate("There is no device assigned yet, please attach a network device in the \"Physical Settings\" tab")
	else
		st.template = "admin_network/iface_status"
		st.network  = arg[1]
		st.value    = nil
	end
end

m.on_init = set_status
m.on_after_save = set_status]]

-- Protocol selection code starts here --
p = s:taboption("general", ListValue, "proto", translate("Protocol"))
p.default = net:proto()

if not net:is_installed() then
	p_install = s:taboption("general", Button, "_install")
	p_install.title      = translate("Protocol support is not installed")
	p_install.inputtitle = translate("Install package %q" % net:opkg_package())
	p_install.inputstyle = "apply"
	p_install:depends("proto", net:proto())

	function p_install.write()
		return luci.http.redirect(
			luci.dispatcher.build_url("admin/system/packages") ..
			"?submit=1&install=%s" % net:opkg_package()
		)
	end
end

-- Testing stuff --
--[[dasdas = s:taboption("general", ListValue, "ifname", translate("Protocol"))
dasdas:value("eth0.2", "wired")
dasdas:value("eth1", "WiMAX")
dasdas:value("usb0", "3g")
dasdas:value("br-lan", "lanBridge")]]

-- Protocol switch button --
p_switch = s:taboption("general", Button, "_switch")
p_switch.title      = translate("Really switch protocol?")
p_switch.inputtitle = translate("Switch protocol")
p_switch.inputstyle = "apply"
--p_switch.depends("ifname", "eth0.2")

local _, pr
for _, pr in ipairs(nw:get_protocols()) do
	if ((ifcName == "eth0.2" or ifcName == "wlan0" or ifcName == "br-lan") and (pr:proto() == "static" or pr:proto() == "dhcp")) or ((ifcName == "eth1" or ifcName == "usb0") and pr:proto() == "dhcp") then
	--if pr:proto() == "dhcp" then
		--luci.sys.call("echo \"iface.lua: proto dependancy: ["..pr:proto().."]\" >> /tmp/log.log")
		p:value(pr:proto(), pr:get_i18n())
		if pr:proto() ~= net:proto() then
			p_switch:depends("proto", pr:proto())
			--p_switch:depends("proto", "dhcp")
			--luci.sys.call("echo \"iface.lua: proto dependancy: ["..pr:proto().."]\" >> /tmp/log.log")
		end
	end
end

-- Dummy dependency so the button would stay hidden when only one protocol is included --
p_switch:depends("proto", "none")


auto = s:taboption("advanced", Flag, "auto", translate("Bring up on boot"))
auto.default = (net:proto() == "none") and auto.disabled or auto.enabled

if isWan then
	if not net:is_virtual() then
		--luci.sys.call("echo \"iface.lua: not is_virtual trpped!\" >> /tmp/log.log")
		--[[br = s:taboption("physical", Flag, "type", translate("Bridge interfaces"), translate("creates a bridge over specified interface(s)"))
		br.enabled = "bridge"
		br.rmempty = true
		br:depends("proto", "static")
		br:depends("proto", "dhcp")
		br:depends("proto", "none")]] -- removed the ability to manually toggle and set up interface bridging

		stp = sModeSwitch:option(Flag, "stp", translate("Enable <abbr title=\"Spanning Tree Protocol\">STP</abbr>"),
			translate("Enables the Spanning Tree Protocol on this bridge")) -- changed from s to sModeSwitch
		stp:depends("type", "bridge")
		stp.rmempty = true
	end

	if not net:is_floating() then
		ifname_single = sModeSwitch:option(Value, "ifname_single", translate("Interface")) -- changed from s to sModeSwitch
		ifname_single.nocreate = false -- remove the ability for the user to create a custom interface
		ifname_single.template = "cbi/network_ifacelist"
		ifname_single.widget = "radio"
		ifname_single.nobridges = true
		ifname_single.rmempty = false
		ifname_single.network = arg[1]
		ifname_single:depends("type", "")

		function ifname_single.cfgvalue(self, s)
			-- let the template figure out the related ifaces through the network model
			return nil
		end

		function ifname_single.write(self, s, val)
			local i
			local new_ifs = { }
			local old_ifs = { }

			for _, i in ipairs(net:get_interfaces() or { net:get_interface() }) do
				old_ifs[#old_ifs+1] = i:name()
			end

			for i in ut.imatch(val) do
				new_ifs[#new_ifs+1] = i

				-- if this is not a bridge, only assign first interface
				if self.option == "ifname_single" then
					break
				end
			end

			table.sort(old_ifs)
			table.sort(new_ifs)

			for i = 1, math.max(#old_ifs, #new_ifs) do
				if old_ifs[i] ~= new_ifs[i] then
					backup_ifnames()
					for i = 1, #old_ifs do
						net:del_interface(old_ifs[i])
					end
					for i = 1, #new_ifs do
						net:add_interface(new_ifs[i])
					end
					break
				end
			end
		end
	end

	if not net:is_virtual() then
		--luci.sys.call("echo \"iface.lua: not is_virtual(second) trpped!\" >> /tmp/log.log")
		ifname_multi = sModeSwitch:option(Value, "ifname_multi", translate("Interface")) -- changed from s to sModeSwitch
		ifname_multi.template = "cbi/network_ifacelist"
		ifname_multi.nobridges = true
		ifname_multi.rmempty = false
		ifname_multi.network = arg[1]
		ifname_multi.widget = "checkbox"
		ifname_multi:depends("type", "bridge")
		ifname_multi.cfgvalue = ifname_single.cfgvalue
		ifname_multi.write = ifname_single.write
	end

	local ifaceSwitch

	local i_switch

	i_switch = sModeSwitch:option(Button, "_modeSwitch")
	i_switch.title      = translate("Really switch modes?")
	i_switch.inputtitle = translate("Switch mode")
	i_switch.inputstyle = "apply"
end

if m:formvalue("cbid.network.wwan._modeSwitch") then
	local newInterface = m:formvalue("cbid.network.wan.ifname_single")
	--luci.sys.call("echo \"ifaces.lua: modeSwitchButton: ifName: "..newInterface.."\" >> /tmp/log.log")
	--[[if newInterface == "usb0" then
		is3G = true
	elseif newInterface == "wlan0" then
		--luci.sys.call("echo \"ifaceWan: modeSwitch: nI == wlan0\" >> /tmp/log.log")
		m:set("wan", "override", "wlan0")
		m:set("wan", "ifname", "eth0.8")
		m.uci:save("network")
		m.uci:commit("network")
		luci.http.redirect(luci.dispatcher.build_url("admin/network/wan"))
		return
	end]]
	if newInterface ~= "wlan0" then
		-- We're switching back from wwan
		m:set("wan", "override", "false")
		--net = nw:get_network("wan")
		m:set("wan", "ifname", "eth0.2")
		m.uci:save("network")
		m.uci:commit("network")
		return
	end
	--[[if (newInterface == "eth1" or newInterface == "usb0") and net:proto() == "static" then
		m:set("wan", "ifname", newInterface)
	-- initiate protocol switch to dhcp
		local ptype = "dhcp"
		local proto = nw:get_protocol(ptype, net:name())
		if proto then
			-- backup default
			backup_ifnames()

			-- if current proto is not floating and target proto is not floating,
			-- then attempt to retain the ifnames
			--error(net:proto() .. " > " .. proto:proto())
			if not net:is_floating() and not proto:is_floating() then
				-- if old proto is a bridge and new proto not, then clip the
				-- interface list to the first ifname only
				if net:is_bridge() and proto:is_virtual() then
					local _, ifn
					local first = true
					for _, ifn in ipairs(net:get_interfaces() or { net:get_interface() }) do
						if first then
							first = false
						else
							net:del_interface(ifn)
						end
					end
					m:del(net:name(), "type")
				end

			-- if the current proto is floating, the target proto not floating,
			-- then attempt to restore ifnames from backup
			elseif net:is_floating() and not proto:is_floating() then
				-- if we have backup data, then re-add all orphaned interfaces
				-- from it and restore the bridge choice
				local br = (m:get(net:name(), "_orig_bridge") == "true")
				local ifn
				local ifns = { }
				for ifn in ut.imatch(m:get(net:name(), "_orig_ifname")) do
					ifn = nw:get_interface(ifn)
					if ifn and not ifn:get_network() then
						proto:add_interface(ifn)
						if not br then
							break
						end
					end
				end
				if br then
					m:set(net:name(), "type", "bridge")
				end

			-- in all other cases clear the ifnames
			else
				local _, ifc
				for _, ifc in ipairs(net:get_interfaces() or { net:get_interface() }) do
					net:del_interface(ifc)
				end
				m:del(net:name(), "type")
			end

			-- clear options
			local k, v
			for k, v in pairs(m:get(net:name())) do
				if k:sub(1,1) ~= "." and
				k ~= "type" and
				k ~= "ifname" and
				k ~= "_orig_ifname" and
				k ~= "_orig_bridge"
				then
					m:del(net:name(), k)
				end
			end

			-- set proto
			m:set(net:name(), "proto", proto:proto())
			m.uci:save("network")
			m.uci:commit("network")
			m.uci:save("wireless")

			-- reload page
			luci.http.redirect(luci.dispatcher.build_url("admin/network/wan"))
			return
		end--]]
	--else
		m:set("wan", "ifname", newInterface)
		m.uci:save("network")
		m.uci:commit("network")
		m.uci:save("wireless")
		luci.http.redirect(luci.dispatcher.build_url("admin/network/wan"))
		return
	--end
end

if has_firewall then
	fwzone = s:taboption("firewall", Value, "_fwzone",
		translate("Create / Assign firewall-zone"),
		translate("Choose the firewall zone you want to assign to this interface. Select <em>unspecified</em> to remove the interface from the associated zone or fill out the <em>create</em> field to define a new zone and attach the interface to it."))

	fwzone.template = "cbi/firewall_zonelist"
	fwzone.network = arg[1]
	fwzone.rmempty = false

	function fwzone.cfgvalue(self, section)
		self.iface = section
		local z = fw:get_zone_by_network(section)
		return z and z:name()
	end

	function fwzone.write(self, section, value)
		local zone = fw:get_zone(value)

		if not zone and value == '-' then
			value = m:formvalue(self:cbid(section) .. ".newzone")
			if value and #value > 0 then
				zone = fw:add_zone(value)
			else
				fw:del_network(section)
			end
		end

		if zone then
			fw:del_network(section)
			zone:add_network(section)
		end
	end
end

function p.write() end
function p.remove() end
function p.validate(self, value, section)
	--luci.sys.call("echo \"ifaces.lua: p.validate: value: "..value..", net:proto(): "..net:proto()..";\" >> /tmp/log.log")
	if value == net:proto() then
		if not net:is_floating() and net:is_empty() then
			local ifn = ((br and (br:formvalue(section) == "bridge")) and ifname_multi:formvalue(section) or ifname_single:formvalue(section))
			--luci.sys.call("echo \"ifaces.lua: ifn: "..ifn.."\"")
			
			for ifn in ut.imatch(ifn) do
				return value
			end
			return nil, translate("The selected protocol needs a device assigned")
		end
	end
	return value
end
--[[ A copy of the above section
function p.validate(self, value, section)
	if value == net:proto() then
		if not net:is_floating() and net:is_empty() then
			local ifn = ((br and (br:formvalue(section) == "bridge"))
				and ifname_multi:formvalue(section)
			     or ifname_single:formvalue(section))

			for ifn in ut.imatch(ifn) do
				return value
			end
			return nil, translate("The selected protocol needs a device assigned")
		end
	end
	return value
end
]]


local form, ferr = loadfile(
	ut.libpath() .. "/model/cbi/admin_network/proto_%s.lua" % net:proto()
)

if not form then
	s:taboption("general", DummyValue, "_error",
		translate("Missing protocol extension for proto %q" % net:proto())
	).value = ferr
else
	setfenv(form, getfenv(1))(m, s, net)
end


local _, field
for _, field in ipairs(s.children) do
	if field ~= st and field ~= p and field ~= p_install and field ~= p_switch then
		if next(field.deps) then
			local _, dep
			for _, dep in ipairs(field.deps) do
				dep.deps.proto = net:proto()
			end
		else
			field:depends("proto", net:proto())
		end
	end
end

--
-- Display 3g configuration
--

local m3g, s3g, o3g

if is3G then
	luci.sys.call("echo \"ifaces.lua: [true]\" >> /tmp/log.log")
else
	luci.sys.call("echo \"ifaces.lua: [false]\" >> /tmp/log.log")
end

if isWan then
	luci.sys.call("echo \"ifaces.lua: [is Wan]\" >> /tmp/log.log")
else
	luci.sys.call("echo \"ifaces.lua: [is not Wan]\" >> /tmp/log.log")
end

if isWan and is3G then
	luci.sys.call("echo \"ifaces.lua: [it si tripping]\" >> /tmp/log.log")
	m3g = Map("network_3g", translate("3G Configuration"), 
		translate("Here you can configure your 3G settings."))
	m3g.addremove = false
	s3g = m3g:section(NamedSection, "userConf", "service3g", translate("3g Configuration"));
	s3g.addremove = false
	
	

	o3g = s3g:option(Flag, "enabled", "Enabled")

	o3g.enabled  = "1"
	o3g.disabled = "0"
	o3g.default  = o3g.enabled

	o3g = s3g:option(ListValue, "auth", translate("3G authentication method"))

	o3g.default = "chap"
	o3g:value("chap", translate("CHAP"))
	o3g:value("pap", translate("PAP"))


	o3g = s3g:option(Value, "APN", translate("APN"))


	o3g = s3g:option(Value, "Username", translate("Username"))

	o3g = s3g:option(Value, "Password", translate("Password"))
	o3g.password = true;

	o3g = s3g:option(Value, "PIN", translate("PIN number"))
	o3g.datatype = "range(0,9999)"
end

--
-- Display IP Aliases
--

if not net:is_floating() then
	s2 = m:section(TypedSection, "alias", translate("IP-Aliases"))
	s2.addremove = true

	s2:depends("interface", arg[1])
	s2.defaults.interface = arg[1]

	s2:tab("general", translate("General Setup"))
	s2.defaults.proto = "static"

	ip = s2:taboption("general", Value, "ipaddr", translate("<abbr title=\"Internet Protocol Version 4\">IPv4</abbr>-Address"))
	ip.optional = true
	ip.datatype = "ip4addr"

	nm = s2:taboption("general", Value, "netmask", translate("<abbr title=\"Internet Protocol Version 4\">IPv4</abbr>-Netmask"))
	nm.optional = true
	nm.datatype = "ip4addr"
	nm:value("255.255.255.0")
	nm:value("255.255.0.0")
	nm:value("255.0.0.0")

	gw = s2:taboption("general", Value, "gateway", translate("<abbr title=\"Internet Protocol Version 4\">IPv4</abbr>-Gateway"))
	gw.optional = true
	gw.datatype = "ip4addr"

	if has_ipv6 then
		s2:tab("ipv6", translate("IPv6 Setup"))

		ip6 = s2:taboption("ipv6", Value, "ip6addr", translate("<abbr title=\"Internet Protocol Version 6\">IPv6</abbr>-Address"), translate("<abbr title=\"Classless Inter-Domain Routing\">CIDR</abbr>-Notation: address/prefix"))
		ip6.optional = true
		ip6.datatype = "ip6addr"

		gw6 = s2:taboption("ipv6", Value, "ip6gw", translate("<abbr title=\"Internet Protocol Version 6\">IPv6</abbr>-Gateway"))
		gw6.optional = true
		gw6.datatype = "ip6addr"
	end

	s2:tab("advanced", translate("Advanced Settings"))

	bcast = s2:taboption("advanced", Value, "bcast", translate("<abbr title=\"Internet Protocol Version 4\">IPv4</abbr>-Broadcast"))
	bcast.optional = true
	bcast.datatype = "ip4addr"

	dns = s2:taboption("advanced", Value, "dns", translate("<abbr title=\"Domain Name System\">DNS</abbr>-Server"))
	dns.optional = true
	dns.datatype = "ip4addr"
end

--[[function p_switch.validate(self, value, sec)
	--luci.sys.call("echo \"ifaces.lua: ifname_single.validate: "..value.."\" >> /tmp/log.log")
	--return value
	local newIfcName = ifname_single:formvalue("wan")
	luci.sys.call("echo \"ifaces.lua: ifname_single.validate: wan: "..newIfcName.."\" >> /tmp/log.log")
	luci.sys.call("echo \"ifaces.lua: ifname_single.validate: proto: "..value.."\" >> /tmp/log.log")
	if (newIfcName == "eth1" or newIfcName == "usb0") and value == "static" then
		return "dhcp"
	else
		return value
	end
end]]

--[[function m.on_commit(map)
	luci.sys.call("echo \"m.on_commit tripped!\" >> /tmp/log.log")
	-- old interface name: ifcName
	local newIfcName = ifname_single:formvalue("wan")
	-- if there is a mismatch then: interface change detected
	if ifcName ~= newIfcName then
		luci.sys.call("echo \"change detected:\" >> /tmp/log.log")
		luci.sys.call("echo \"ifaces.lua: newName:["..newIfcName.."]; oldName:["..ifcName.."]\" >> /tmp/log.log")
	else 
		luci.sys.call("echo \"no change detected:\" >> /tmp/log.log")
		luci.sys.call("echo \"ifaces.lua: newName:["..newIfcName.."]; oldName:["..ifcName.."]\" >> /tmp/log.log")
	end
	
end]]

--
-- Display DNS settings if dnsmasq is available
--

if has_dnsmasq and net:proto() == "static" then
	m2 = Map("dhcp", "", "")

	local has_section = false

	m2.uci:foreach("dhcp", "dhcp", function(s)
		if s.interface == arg[1] then
			has_section = true
			return false
		end
	end)

	if not has_section then

		s = m2:section(TypedSection, "dhcp", translate("DHCP Server"))
		s.anonymous   = true
		s.cfgsections = function() return { "_enable" } end

		x = s:option(Button, "_enable")
		x.title      = translate("No DHCP Server configured for this interface")
		x.inputtitle = translate("Setup DHCP Server")
		x.inputstyle = "apply"

	else

		s = m2:section(TypedSection, "dhcp", translate("DHCP Server"))
		s.addremove = false
		s.anonymous = true
		s:tab("general",  translate("General Setup"))
		s:tab("advanced", translate("Advanced Settings"))

		function s.filter(self, section)
			return m2.uci:get("dhcp", section, "interface") == arg[1]
		end

		local ignore = s:taboption("general", Flag, "ignore",
			translate("Ignore interface"),
			translate("Disable <abbr title=\"Dynamic Host Configuration Protocol\">DHCP</abbr> for " ..
				"this interface."))

		local start = s:taboption("general", Value, "start", translate("Start"),
			translate("Lowest leased address as offset from the network address."))
		start.optional = true
		start.datatype = "uinteger"
		start.default = "100"

		local limit = s:taboption("general", Value, "limit", translate("Limit"),
			translate("Maximum number of leased addresses."))
		limit.optional = true
		limit.datatype = "uinteger"
		limit.default = "150"

		local ltime = s:taboption("general", Value, "leasetime", translate("Leasetime"),
			translate("Expiry time of leased addresses, minimum is 2 Minutes (<code>2m</code>)."))
		ltime.rmempty = true
		ltime.default = "12h"

		local dd = s:taboption("advanced", Flag, "dynamicdhcp",
			translate("Dynamic <abbr title=\"Dynamic Host Configuration Protocol\">DHCP</abbr>"),
			translate("Dynamically allocate DHCP addresses for clients. If disabled, only " ..
				"clients having static leases will be served."))
		dd.default = dd.enabled

		s:taboption("advanced", Flag, "force", translate("Force"),
			translate("Force DHCP on this network even if another server is detected."))

		-- XXX: is this actually useful?
		--s:taboption("advanced", Value, "name", translate("Name"),
		--	translate("Define a name for this network."))

		mask = s:taboption("advanced", Value, "netmask",
			translate("<abbr title=\"Internet Protocol Version 4\">IPv4</abbr>-Netmask"),
			translate("Override the netmask sent to clients. Normally it is calculated " ..
				"from the subnet that is served."))

		mask.optional = true
		mask.datatype = "ip4addr"

		s:taboption("advanced", DynamicList, "dhcp_option", translate("DHCP-Options"),
			translate("Define additional DHCP options, for example \"<code>6,192.168.2.1," ..
				"192.168.2.2</code>\" which advertises different DNS servers to clients."))

		for i, n in ipairs(s.children) do
			if n ~= ignore then
				n:depends("ignore", "")
			end
		end

	end
end

if isWan and is3G then
	--luci.sys.call("echo \"ifaces.lua: [and being returned...]\" >> /tmp/log.log")
	return m, m3g, m2
else
	return m, m2
end