------------------
-- Header stuff --
------------------

-- Helper Functions
-- custom Echo DIAGSTRING
local function cecho(string)
	luci.sys.call("echo \"" .. string .. "\" >> /tmp/log.log")
end

function getString(value)
	if type(value) == "string" then
		return value
	elseif type(value) == "table" then
		return "*is a table*"
	end
	return "*unknown*"
end

-- some defines
local __define__config_file_name = "network"
local __define__network = "wan"
local __define__page_entry_path = "admin/network/wan"

-- some external libs
local ut = require "luci.util"
local fs = require "nixio.fs"
local nw = require "luci.model.network"
local fw = require "luci.model.firewall"

-- Variables
-- Tables
local net, ifc
-- Basic variable set
local m, s, o
-- Page fields
local op_mode,			-- Operation Mode
      proto_sel,		-- Protocol selection (drop down list)
      proto_switch,		-- Protocol switch button
      no_nat			-- NAT disabling flag
-- Boolean variables
local has_firewall		-- true if firewall present
local has_multiwan		-- true if multiwan functionality is available
local isWiMAX			-- true if WAN is configured for eth1 (i.e. is working as a WiMAX router)
local savePressed		-- Used for immediate WiMAX controll toggling
local eth1fromVal		-- Used for immediate WiMAX controll toggling
-- Global variables/constants/defines etc...
local protoSwitchBtnName = "_switch"
local interfaceName
local networkProtocol
-- HTTP GET params
-- Interface
local httpInterface = luci.http.formvalue("if")
-- Protocol
local httpProto = luci.http.formvalue("prot")

cecho("Map entry!")
cecho("httpInterface: [" .. (httpInterface or "none") .. "]")
cecho("httpProto: [" .. (httpProto or "none")  .. "]")

------------------------
-- Map initialisation --
------------------------

has_firewall = fs.access("/etc/config/firewall")
has_multiwan = fs.access("/etc/config/multiwan")

m = Map(__define__config_file_name, "WAN", translate("On this page you can configure your WAN settings. Your WAN configuration determines how the router will be connecting to the internet."))
m:chain("wireless")
if has_firewall then
	m:chain("firewall")
end
if has_multiwan then
	m:chain("multiwan")
end

-- DIAGSTRING rem this!!! 
-- m.skip_apply = true -- No scripts will be relaunched after Save&Apply

nw.init(m.uci)
fw.init(m.uci)

net = nw:get_network(__define__network)
if not net then
	-- rebuild network!!!
end
ifc = net:get_interface()
if not ifc then
	-- no interface detected, what now?
end

savePressed = luci.http.formvalue("cbi.apply") and true or false
eth1fromVal = luci.http.formvalue("cbid.network.wan.ifname") == "eth1" or false

-- Explanation for how and why we use this way of determining wether isWiMAX is true
-- Instinctively I assumed that ifc:name() == "eth1" would be enough, but this caused a delay:
-- After configuring the mode to WiMAX and pressing save, I wouldn't see the WiMAX controlls
-- untill an additional reloading of the page. Same thing happened when I configured either Wired of Wifi FROM WiMAX:
-- WiMAX controlls would not dissapear immediatly after a Save, an additional reloading of the page would be required.
-- You can see what I'm trying to describe by simply shortening the if statement to 'if ifc:name() == "eth1" then'.
-- The 'and not (savePressed and not eth1fromVal) or savePressed and eth1fromVal' bit removes these delays.
if ifc:name() == "eth1" and not (savePressed and not eth1fromVal) or savePressed and eth1fromVal then
	isWiMAX = true;
end

interfaceName = httpInterface or luci.http.formvalue("cbid.network.wan.ifname") or ifc:name()
interface = nw:get_interface(interfaceName)
networkProtocol = httpProto or luci.http.formvalue("cbid.network.wan.proto") or net:proto()

cecho("iface: [" .. (ifc:name() or "none")  .. "]")
cecho("proto: [" .. (net:proto() or "none")  .. "]")

cecho("calc iface: [" .. (interfaceName or "none")  .. "]")
cecho("calc proto: [" .. (networkProtocol or "none")  .. "]")


-------------------------------------
-- Protocol switch button handling --
-------------------------------------

if m:formvalue("cbid." .. __define__config_file_name .. "." .. __define__network .. "." .. protoSwitchBtnName) then
	local ptype = m:formvalue("cbid." .. __define__config_file_name .. "." .. __define__network .. ".proto") or "dhcp"
	local iftype = m:formvalue("cbid." .. __define__config_file_name .. "." .. __define__network .. ".ifname") or "none"
	--m:set(__define__network, "proto", ptype)
	
	if new_protocol ~= old_protocol then
		cecho(" entry!")
		local k, v
		for k, v in pairs(m:get(__define__network)) do
			cecho("  k: [" .. getString(k) .. "]; v: [" .. getString(v) .. "]")
			if k:sub(1,1) ~= "." and
			   k ~= "type" and
			   k ~= "ifname" and
			   k ~= "_orig_ifname" and
			   k ~= "_orig_bridge"
			then
				cecho("  deleting above entry...!")
				m:del(__define__network, k)
			end
		end
		cecho(" exit!")
	end
	
	--m.uci:save(__define__config_file_name)
	luci.http.redirect(luci.dispatcher.build_url(__define__page_entry_path) .. "?if=" .. iftype .. "&prot=" .. ptype)
	return
end

------------------------
-- Section operations --
------------------------

sop_mode = m:section(NamedSection, __define__network, "interface", translate("Operation Mode"))

s = m:section(NamedSection, __define__network, "interface", translate("Common configuration"))

s:tab("general",  translate("General Setup"))
s:tab("advanced", translate("Advanced Settings"))

-----------------
-- Mode switch --
-----------------

op_mode = sop_mode:option(Value, "ifname", translate("Interface"))
op_mode.nocreate = true -- remove the ability for the user to create a custom interface
op_mode.template = "cbi/network_ifacelist"
op_mode.widget = "radio"
op_mode.nobridges = true
op_mode.rmempty = false
op_mode.network = __define__network
op_mode.onClickReload = true
if httpInterface then
	-- We are going to force selection from within network_ifacelist
	op_mode.forceNetworkSelection = httpInterface
end

--[[function op_mode.parse(self, section, novalid)
	cecho("written!")
	Value.parse(self, section, novalid)
end]]

function op_mode.write(self, section, val)
	net:del_interface(net:get_interface())
	net:add_interface(val)
end

function op_mode.cfgvalue(self, section)
	return nil
end

----------------------
-- General settings --
----------------------

-- Disable/Enable NAT
if has_firewall then
	no_nat = s:taboption("advanced", Flag, "masquerade",
	                  translate("Disable NAT"),
	                  translate("If checked, router will not perform NAT (Masquerade) on this interface"))

	no_nat.default = no_nat.disabled
	no_nat.rmempty = false

	function no_nat.cfgvalue(self, section)
		local cval = "0"
		m.uci:foreach("firewall", "zone", function(s)
			if s.name == "wan" then
				cval = s.masq
				if cval == "1" then	--//masq=1 means Disable NAT=0
					cval = "0"
				else
					cval = "1"
				end
				return cval
			end
		end)
		return cval
	end

	function no_nat.write(self, section, value)
		local fwzone = "nil"
		m.uci:foreach("firewall", "zone", function(s)
			if s.name == "wan" then
				fwzone = s[".name"]
				if value == "1" then	--//Disable NAT=1 means masq=0
					m.uci:set("firewall", fwzone, "masq", "0")
				else
					m.uci:set("firewall", fwzone, "masq", "1")
				end
				m.uci:save("firewall")
			end
		end)
	end
end
	
----------------------
-- Protocol prelude --
----------------------

-- Protocol selection: drop down list
proto_sel = s:taboption("general", ListValue, "proto", translate("Protocol"))

proto_sel.default = net:proto()
proto_sel:value("dhcp", "DHCP")
if interfaceName == "eth0.2" or interfaceName == "wlan0" then
	proto_sel:value("static", "Static")
end

proto_sel.forcewrite = true

for k, v in pairs(proto_sel.keylist) do
	cecho(" MAP: self.keylist: key: [" .. k .. "] and value: [" .. v .. "]")
end

function proto_sel.cfgvalue()
	return networkProtocol
end
	
-- Protocol switch button
proto_switch = s:taboption("general", Button, "_switch")
proto_switch.title      = translate("Really switch protocol?")
proto_switch.inputtitle = translate("Switch protocol")
proto_switch.inputstyle = "apply"
proto_switch:depends("proto", "none")

if interfaceName == "eth0.2" or interfaceName == "wlan0" then
	for _, i in ipairs({"static", "dhcp"}) do
		if networkProtocol ~= i then
			proto_switch:depends("proto", i)
		end
	end
end

-- Selected protocol inclusion
local form, ferr = loadfile(
	ut.libpath() .. "/model/cbi/admin_network/proto_%s_simp.lua" % networkProtocol
)

if not form then
	s:option(DummyValue, "_error",
		translate("Missing protocol extension for proto %q" % networkProtocol)
	).value = ferr
else
	if networkProtocol ~= net:proto() then
		setfenv(form, getfenv(1))(m, s, interface, true)
	else
		setfenv(form, getfenv(1))(m, s, interface)
	end
end

local _, field
for _, field in ipairs(s.children) do
	if field ~= op_mode and field ~= proto_sel and field ~= proto_switch and field ~= no_nat then
		if next(field.deps) then
			local _, dep
			for _, dep in ipairs(field.deps) do
				dep.deps.proto = networkProtocol
			end
		else
			field:depends("proto", networkProtocol)
		end
	end
end

----------------------
-- Map finalisation --
----------------------

function m.on_parse(self)
	local new_interface, old_interface
	local new_protocol, old_protocol
	
	old_interface = ifc:name()
	old_protocol = net:proto()
	
	new_interface = luci.http.formvalue("cbid." .. __define__config_file_name .. "." .. __define__network .. ".ifname")
	new_protocol = luci.http.formvalue("cbid." .. __define__config_file_name .. "." .. __define__network .. ".proto")
	
	if not new_interface or not new_protocol then
		return
	end
	
	--cecho("new_proto: [" .. new_protocol .. "]; old_proto: [" .. old_protocol .. "]")
	
	if new_protocol ~= old_protocol then
		--cecho(" entry!")
		local k, v
		for k, v in pairs(m:get(__define__network)) do
			--cecho("  k: [" .. getString(k) .. "]; v: [" .. getString(v) .. "]")
			if k:sub(1,1) ~= "." and
			   k ~= "type" and
			   k ~= "ifname" and
			   k ~= "_orig_ifname" and
			   k ~= "_orig_bridge"
			then
				--cecho("  deleting above entry...!")
				m:del(__define__network, k)
			end
		end
		--cecho(" exit!")
	end
	if new_interface ~= old_interface then
		-- Three cases are outlined:
		-- When the new interface is one of the mobile ones (by current standart they are the only ones that can be backup links) 
		--   therefore... when switching into one of them we need to check if backup wan is enabled and then disable it
		-- When the new interface is Wireless wan or wlan0 or wwan or w/e:
		--   Since WWAN and an Access Point cannot run simultaneously, we have to disable the AP AND
		--   if WWAN was enabled the last time it will get reenabled
		-- When the new interface is NOT WWAN:
		--   Opposite procedure to the one above (WWAN gets disabled, and AP if previously enabled, gets reenabled)
		
		if new_interface == "usb0" or new_interface == "eth1" then
			if m.uci:get("multiwan", "config", "enabled") == "1" then
				os.execute("/etc/init.d/multiwan stop")
				nw:backupLink_dis()
				nw:del_network("wan2")
				m.uci:save("multiwan")
				m.uci:commit("multiwan")
			end
		end
		
		if new_interface == "wlan0" then
			local wnet = nw:get_wifinet("radio0.network2") -- STA mode wifinet
			local apWnet = nw:get_wifinet("radio0.network1") -- AP mode wifi net
			if (wnet and wnet:get("user_enable") == "1") or (apWnet and apWnet:get("disabled") ~= "1") then
				luci.sys.call("env -i /sbin/wifi down >/dev/null 2>/dev/null")
				if apWnet and apWnet:get("disabled") ~= "1" then 
					apWnet:set("disabled", "1")
				end
				if wnet and wnet:get("user_enable") == "1" then
					wnet:set("disabled", nil)
				end
				nw:commit("wireless")
				luci.sys.call("env -i /sbin/wifi up >/dev/null 2>/dev/null")
			end
		end
		
		if new_interface ~= "wlan0" then
			local wnet = nw:get_wifinet("radio0.network2")
			local apWnet = nw:get_wifinet("radio0.network1")
			if (apWnet and apWnet:get("user_enable") == "1") or (wnet and wnet:get("disabled") ~= "1") then
				luci.sys.call("env -i /sbin/wifi down >/dev/null 2>/dev/null")
				if wnet and wnet:get("disabled") ~= "1" then 
					wnet:set("disabled", "1")
				end
				if apWnet and apWnet:get("user_enable") == "1" then
					apWnet:set("disabled", nil)
				end
				nw:commit("wireless")
				luci.sys.call("env -i /sbin/wifi up >/dev/null 2>/dev/null")
			end
		end
		
	end
end

--------------------
-- WiMAX settings --
-- -----------------
local reboot, roff, sW, mWim

if isWiMAX then
	mWim = Map(__define__config_file_name, translate("WiMAX"), 
	translate("Here you can disable WiMAX connection or reboot the modem."))

	-- mWim.skip_apply = true -- No scripts will be relaunched after Save&Apply
	
	sW = mWim:section(TypedSection, "_dummy", translate("WiMAX Management"))
	sW.addremove = false
	sW.anonymous = true

	reboot = sW:option(Button, "_reboot")
	reboot.title      = translate("Reboot WiMAX modem?")
	reboot.inputtitle = translate("Reboot")
	reboot.inputstyle = "apply"

	roff            = sW:option(Button, "_ro")
	roff.title      = translate("Turn on/off WiMAX")
	if nw:check_wimax_state() then
		roff.inputtitle = translate("Turn off")
	else
		roff.inputtitle = translate("Turn on")
	end
	roff.inputstyle = "apply"
	function sW.cfgsections()
		return { __define__network }
	end

	if mWim:formvalue("cbid." .. __define__config_file_name .. "." .. __define__network .. "._reboot") then
		nw:rr_wimax()
		luci.http.redirect(luci.dispatcher.build_url(__define__page_entry_path))
	end

	if mWim:formvalue("cbid." .. __define__config_file_name .. "." .. __define__network .. "._ro") then
		nw:ro_wimax()
		luci.http.redirect(luci.dispatcher.build_url(__define__page_entry_path))
	end

end

----------------
-- IP Aliases --
----------------

local ipaliases_s2, ipaliases_ip, ipaliases_nm, ipaliases_gw, ipaliases_ip6, ipaliases_gw6, ipaliases_bcast, ipaliases_dns

ipaliases_s2 = m:section(TypedSection, "alias", translate("IP-Aliases"))
ipaliases_s2.addremove = true

ipaliases_s2:depends("interface", __define__network)
ipaliases_s2.defaults.interface = __define__network

ipaliases_s2:tab("general", translate("General Setup"))
ipaliases_s2.defaults.proto = "static"

ipaliases_ip = ipaliases_s2:taboption("general", Value, "ipaddr", translate("IPv4-Address"))
ipaliases_ip.optional = true
ipaliases_ip.datatype = "ip4addr"

ipaliases_nm = ipaliases_s2:taboption("general", Value, "netmask", translate("IPv4-Netmask"))
ipaliases_nm.optional = true
ipaliases_nm.datatype = "ip4addr"
ipaliases_nm:value("255.255.255.0")
ipaliases_nm:value("255.255.0.0")
ipaliases_nm:value("255.0.0.0")

ipaliases_gw = ipaliases_s2:taboption("general", Value, "gateway", translate("IPv4-Gateway"))
ipaliases_gw.optional = true
ipaliases_gw.datatype = "ip4addr"

if has_ipv6 then
	ipaliases_s2:tab("ipv6", translate("IPv6 Setup"))

	ipaliases_ip6 = ipaliases_s2:taboption("ipv6", Value, "ip6addr", translate("IPv6-Address"), translate("CIDR-Notation: address/prefix"))
	ipaliases_ip6.optional = true
	ipaliases_ip6.datatype = "ip6addr"

	ipaliases_gw6 = ipaliases_s2:taboption("ipv6", Value, "ip6gw", translate("IPv6-Gateway"))
	ipaliases_gw6.optional = true
	ipaliases_gw6.datatype = "ip6addr"
end

ipaliases_s2:tab("advanced", translate("Advanced Settings"))

ipaliases_bcast = ipaliases_s2:taboption("advanced", Value, "bcast", translate("IPv4-Broadcast"))
ipaliases_bcast.optional = true
ipaliases_bcast.datatype = "ip4addr"

ipaliases_dns = ipaliases_s2:taboption("advanced", Value, "dns", translate("DNS-Server"))
ipaliases_dns.optional = true
ipaliases_dns.datatype = "ip4addr"

-----------------
-- End: return --
-----------------

cecho("Map end!")

if isWiMAX then
	return m, mWim
end
return m