--[[
config custom_interface '3g'
        option pin 'kazkas'
        option apn 'kazkas'
        option user 'kazkas'
        option password 'kazkas'
        option auth_mode 'chap' ARBA 'pap' (jei nerandu nieko ar kazka kita, laikau kad auth nenaudojama)
        option net_mode 'gsm' ARBA 'umts' ARBA 'auto' (prefered tinklas. jei nerandu nieko arba kazka kita laikau kad auto)
        option data_mode 'enabled' ARBA 'disabled' (ar leisti siusti duomenis. jei nera nieko ar kazkas kitas, laikau kad enabled)
]]
local moduleType = luci.util.trim(luci.sys.exec("uci get system.module.type"))
local m
if moduleType == "3g_ppp" then
m = Map("network", translate("3G Configuration"), 
	translate("Here you can configure your 3G settings."))
m.addremove = false

s = m:section(NamedSection, "ppp", "interface", translate("3G Configuration"));
s.addremove = false

o = s:option(Value, "apn", translate("APN"))

o = s:option(Value, "pincode", translate("PIN number"))
o.datatype = "range(0,9999)"

o = s:option(Value, "dialnumber", translate("Dialing number"))
o.default = "*99#"

auth = s:option(ListValue, "auth_mode", translate("3G authentication method"))
auth:value("chap", translate("CHAP"))
auth:value("pap", translate("PAP"))
auth:value("none", translate("none"))
auth.default = "none"

o = s:option(Value, "username", translate("Username"))
o:depends("auth_mode", "chap")
o:depends("auth_mode", "pap")

o = s:option(Value, "password", translate("Password"))
o:depends("auth_mode", "chap")
o:depends("auth_mode", "pap")
o.password = true;

o = s:option(ListValue, "service", translate("Service mode"))
o:value("gprs-only", translate("2G only"))
o:value("gprs", translate("2G preferred"))
o:value("umts-only", translate("3G only"))
o:value("umts", translate("3G preferred"))
o:value("auto", translate("automatic"))
o.default = "umts"

else

m = Map("network_3g", translate("3G Configuration"), 
	translate("Here you can configure your 3G settings."))
m.addremove = false

s = m:section(NamedSection, "3g", "custom_interface", translate("3G Configuration"));
s.addremove = false

o = s:option(Value, "apn", translate("APN"))

o = s:option(Value, "pin", translate("PIN number"))
o.datatype = "range(0,9999)"

auth = s:option(ListValue, "auth_mode", translate("3G authentication method"))

auth:value("chap", translate("CHAP"))
auth:value("pap", translate("PAP"))
auth:value("none", translate("none"))
auth.default = "none"

o = s:option(Value, "user", translate("Username"))
o:depends("auth_mode", "chap")
o:depends("auth_mode", "pap")

o = s:option(Value, "password", translate("Password"))
o:depends("auth_mode", "chap")
o:depends("auth_mode", "pap")
o.password = true;

o = s:option(ListValue, "net_mode", translate("Prefered network"))

o:value("gsm", translate("2G"))
o:value("umts", translate("3G"))
o:value("auto", translate("auto"))

o.default = "auto"

--[[o = s:option(Flag, "data_mode", translate("Data mode"))

o.enabled = "enabled"
o.disabled = "disabled"]]

end
return m