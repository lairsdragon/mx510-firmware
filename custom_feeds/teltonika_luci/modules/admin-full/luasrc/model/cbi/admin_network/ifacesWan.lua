------------------
-- Header stuff --
------------------

--FIXME: Requires module recognition/configuration rehaul

-- Helper Functions
-- custom Echo DIAGSTRING
local function cecho(string)
	luci.sys.call("echo \"" .. string .. "\" >> /tmp/log.log")
end

function getString(value)
	if type(value) == "string" then
		return value
	elseif type(value) == "table" then
		return "*is a table*"
	end
	return "*unknown*"
end

-- some defines
local __define__config_file_name = "network"
local __define__network = "wan"
local __define__page_entry_path = "admin/network/wan"

-- some external libs
local ut = require "luci.util"
local fs = require "nixio.fs"
local nw = require "luci.model.network"
local fw = require "luci.model.firewall"
require("luci.tools.webadmin")

-- Variables
-- Tables
local net, ifc
-- Basic variable set
local m, s, o, mWan
-- Page fields
local op_mode,			-- Operation Mode
      proto_sel,		-- Protocol selection (drop down list)
      proto_switch,		-- Protocol switch button
      no_nat			-- NAT disabling flag
-- Boolean variables
local has_firewall		-- true if firewall present
local has_multiwan		-- true if multiwan functionality is available
local has_conf3g		-- true if 3g config is available
local isWiMAX			-- true if WAN is configured for eth1 (i.e. is working as a WiMAX router)
local savePressed		-- Used for immediate WiMAX controll toggling
local eth1fromVal		-- Used for immediate WiMAX controll toggling
local ExpertMode 		-- Expert mode on/off
local ModuleType		-- '3g_ppp' or '3g-ppp'
local DoubleRun	= false		-- prevent write function being called twice
-- Global variables/constants/defines etc...
local protoSwitchBtnName = "_switch"
local interfaceName
local networkProtocol
-- HTTP GET params
-- Interface
local httpInterface = luci.http.formvalue("if")
-- Protocol
local httpProto = luci.http.formvalue("prot")

------------------------
-- Map initialisation --
------------------------

has_firewall = fs.access("/etc/config/firewall")
has_multiwan = fs.access("/etc/config/multiwan")

m = Map(__define__config_file_name, "WAN", translate("On this page you can configure your WAN settings. Your WAN configuration determines how the router will be connecting to the internet."))
m:chain("wireless")
if has_firewall then
	m:chain("firewall")
end
-- TODO review this!
-- if has_multiwan then
-- 	m:chain("multiwan")
-- end

-- DIAGSTRING rem this!!! 
--m.skip_apply = true -- No scripts will be relaunched after Save&Apply

nw.init(m.uci)
fw.init(m.uci)

--moduleThing = nw:getModule()

net = nw:get_network(__define__network)
if not net then
	-- rebuild network!!!
end
ifc = net:get_interface()
if not ifc then
	-- no interface detected, what now?
end

savePressed = luci.http.formvalue("cbi.apply") and true or false
eth1fromVal = luci.http.formvalue("cbid.network.wan.ifname") == "eth1" or false
ExpertMode = m.uci:get("teltonika", "sys", "expert")
ModuleType = m.uci:get("system", "module", "type")

-- Explanation for how and why we use this way of determining wether isWiMAX is true
-- Instinctively I assumed that ifc:name() == "eth1" would be enough, but this caused a delay:
-- After configuring the mode to WiMAX and pressing save, I wouldn't see the WiMAX controlls
-- untill an additional reloading of the page. Same thing happened when I configured either Wired of Wifi FROM WiMAX:
-- WiMAX controlls would not dissapear immediatly after a Save, an additional reloading of the page would be required.
-- You can see for you self what I'm trying to describe by simply shortening the if statement to 'if ifc:name() == "eth1" then'.
-- The 'and not (savePressed and not eth1fromVal) or savePressed and eth1fromVal' bit removes these delays.
if ifc:name() == "eth1" and not (savePressed and not eth1fromVal) or savePressed and eth1fromVal then
	isWiMAX = true;
end

interfaceName = httpInterface or luci.http.formvalue("cbid.network.wan.ifname") or ifc:name()
if interfaceName == "pppoe-wan" then
	interfaceName = "eth0.2"
end
interface = nw:get_interface(interfaceName)
networkProtocol = httpProto or luci.http.formvalue("cbid.network.wan.proto") or net:proto()

-- write value to config
local function WriteToConfig(config, section, option, value)
	local cfgValue
	if value then
		cfgValue = m.uci:get(config, section, option)
		if cfgValue ~= value then
			m.uci:set(config, section, option, value)
			m.uci.save(config)
		end
	else
		m.uci:set(config, section, option, "")
		m.uci.save(config)	
	end
end

-------------------------------------
-- Protocol switch button handling --
-------------------------------------

if m:formvalue("cbid." .. __define__config_file_name .. "." .. __define__network .. "." .. protoSwitchBtnName) then
	local ptype = m:formvalue("cbid." .. __define__config_file_name .. "." .. __define__network .. ".proto") or "dhcp"
	local iftype = m:formvalue("cbid." .. __define__config_file_name .. "." .. __define__network .. ".ifname") or "none"
	luci.http.redirect(luci.dispatcher.build_url(__define__page_entry_path) .. "?if=" .. iftype .. "&prot=" .. ptype)
	return
end

------------------------
-- Section operations --
------------------------

sop_mode = m:section(NamedSection, __define__network, "interface", translate("Operation Mode"))

s = m:section(NamedSection, __define__network, "interface", translate("Common configuration"))

s:tab("general",  translate("General Setup"))
if ExpertMode == "on" then
	s:tab("advanced", translate("Advanced Settings"))
end
-----------------
-- Mode switch --
-----------------

op_mode = sop_mode:option(Value, "ifname", translate("Interface"))
op_mode.nocreate = true -- remove the ability for the user to create a custom interface
op_mode.template = "cbi/network_ifacelist"
op_mode.widget = "radio"
op_mode.nobridges = true
op_mode.rmempty = false
op_mode.network = __define__network
op_mode.onClickReload = true
if httpInterface then
	-- We are going to force selection from within network_ifacelist
	op_mode.forceNetworkSelection = httpInterface
end

function op_mode.write(self, section, val)
	net:del_interface(net:get_interface(true))
	_ifc = net:get_interface(true)
	_ifcname = _ifc and _ifc:name() or "null"
	_val = val or "null"
	net:add_interface(val)
	-- WAN selected, disable quick wizard after next login
	if m.uci:get("teltonika", "sys", "first_login") then
		m.uci:delete("teltonika", "sys", "first_login")
		m.uci:save("teltonika")
		m.uci:commit("teltonika")
	end
end

function op_mode.cfgvalue(self, section)
	return nil
end

----------------------
-- General settings --
----------------------

-- Disable/Enable NAT
if has_firewall and ExpertMode == "on" then
	no_nat = s:taboption("advanced", Flag, "masquerade",
	                  translate("Disable NAT"),
	                  translate("If checked, router will not perform NAT (Masquerade) on this interface"))

	no_nat.default = no_nat.disabled
	no_nat.rmempty = false

	function no_nat.cfgvalue(self, section)
		local cval = "0"
		m.uci:foreach("firewall", "zone", function(s)
			if s.name == "wan" then
				cval = s.masq
				if cval == "1" then	--//masq=1 means Disable NAT=0
					cval = "0"
				else
					cval = "1"
				end
				return cval
			end
		end)
		return cval
	end

	function no_nat.write(self, section, value)
		local fwzone = "nil"
		m.uci:foreach("firewall", "zone", function(s)
			if s.name == "wan" then
				fwzone = s[".name"]
				if value == "1" then	--//Disable NAT=1 means masq=0
					m.uci:set("firewall", fwzone, "masq", "0")
				else
					m.uci:set("firewall", fwzone, "masq", "1")
				end
				m.uci:save("firewall")
			end
		end)
	end
end

----------------------
-- Protocol prelude --
----------------------
-- Protocol selection: drop down list
proto_sel = s:taboption("general", ListValue, "proto", translate("Protocol"))

proto_sel.default = net:proto()
proto_sel:value("dhcp", "DHCP client")
if interfaceName == "eth0.2" or interfaceName == "wlan0" then
	proto_sel:value("static", "Static")
	if interfaceName == "eth0.2" then
		proto_sel:value("pppoe", "PPPoE")
	end
end
proto_sel.forcewrite = true

function proto_sel.cfgvalue()
	return networkProtocol
end

-- Protocol switch button
proto_switch = s:taboption("general", Button, "_switch")
proto_switch.title      = translate("Really switch protocol?")
proto_switch.inputtitle = translate("Switch protocol")
proto_switch.inputstyle = "apply"
proto_switch:depends("proto", "none")

if interfaceName == "eth0.2" or interfaceName == "wlan0" then
	for _, i in ipairs({"static", "dhcp"}) do
		if networkProtocol ~= i then
			proto_switch:depends("proto", i)
		end
	end
	if interfaceName == "eth0.2" and networkProtocol ~= "pppoe" then
		proto_switch:depends("proto", "pppoe")
	end
end

-- Selected protocol inclusion
local form, ferr = loadfile(
	ut.libpath() .. "/model/cbi/admin_network/proto_%s_simp.lua" % networkProtocol
)

-- workaround: added interfaceName == "3g-ppp"
if not form or interfaceName == "3g-ppp" then
	s:option(DummyValue, "_error",
		translate("Missing protocol extension for proto %q" % networkProtocol)
	).value = ferr
else
	if networkProtocol ~= net:proto() then
		setfenv(form, getfenv(1))(m, s, interface, true)
	else
		setfenv(form, getfenv(1))(m, s, interface)
	end
end

local _, field
for _, field in ipairs(s.children) do
	if field ~= op_mode and field ~= proto_sel and field ~= proto_switch and field ~= no_nat then
		if next(field.deps) then
			local _, dep
			for _, dep in ipairs(field.deps) do
				dep.deps.proto = networkProtocol
			end
		else
			field:depends("proto", networkProtocol)
		end
	end
end

----------------------
-- Map finalisation --
----------------------

function m.on_parse(self)
	local new_interface, old_interface
	local new_protocol, old_protocol
	local vpnPIDstatus = luci.sys.call("pidof openvpn > /dev/null")

	old_interface = ifc:name()
	old_protocol = net:proto()
	
	new_interface = luci.http.formvalue("cbid." .. __define__config_file_name .. "." .. __define__network .. ".ifname")
	new_protocol = luci.http.formvalue("cbid." .. __define__config_file_name .. "." .. __define__network .. ".proto")
	
	if not new_interface or not new_protocol then
		return
	end
	
	if new_protocol ~= old_protocol then
		local k, v
		for k, v in pairs(m:get(__define__network)) do
			if k:sub(1,1) ~= "." and
			   k ~= "type" and
			   k ~= "ifname" and
			   k ~= "_orig_ifname" and
			   k ~= "_orig_bridge"
			then
				m:del(__define__network, k)
			end
		end
	end
	
	if new_interface == "3g-ppp" then
		m.uci:set("network", "ppp", "enabled", "1")
		m.uci:set("network", "wan", "proto", "none")
	end
	if new_interface ~= "3g-ppp" and old_interface == "3g-ppp" then
		m.uci:set("network", "ppp", "enabled", "0")
	end
	m.uci:save("network")
	m.uci:commit("network")
	if new_interface ~= old_interface then
		-- Three cases are outlined:
		-- When the new interface is one of the mobile ones (by current standart they are the only ones that can be backup links) 
		--   therefore... when switching into one of them we need to check if backup wan is enabled and then disable it
		-- When the new interface is Wireless wan or wlan0 or wwan or w/e:
		--   Since WWAN and an Access Point cannot run simultaneously, we have to disable the AP AND
		--   if WWAN was enabled the last time it will get reenabled
		-- When the new interface is NOT WWAN:
		--   Opposite procedure to the one above (WWAN gets disabled, and AP if previously enabled, gets reenabled)
		
		if new_interface == "usb0" or new_interface == "eth1" or new_interface == "3g-ppp" or new_interface == "wm0" then
			if m.uci:get("multiwan", "config", "enabled") == "1" then
				--os.execute("/etc/init.d/multiwan stop 2>/dev/null 1>&2")
				nw:backupLink_dis()
				nw:del_network("wan2")
				m.uci:save("multiwan")
				m.uci:commit("multiwan")
			end
		end

		if new_interface == "wlan0" then
			local wnet = nw:get_wifinet("radio0.network2") -- STA mode wifinet
			local apWnet = nw:get_wifinet("radio0.network1") -- AP mode wifi net
			if (wnet and wnet:get("user_enable") == "1") or (apWnet and apWnet:get("disabled") ~= "1") then
				luci.sys.call("env -i /sbin/wifi down >/dev/null 2>/dev/null")
				if apWnet and apWnet:get("disabled") ~= "1" then 
					apWnet:set("disabled", "1")
				end
				if wnet and wnet:get("user_enable") == "1" then
					wnet:set("disabled", nil)
				end
				nw:commit("wireless")
				luci.sys.call("env -i /sbin/wifi up >/dev/null 2>/dev/null")
			end
		end
		
		if new_interface ~= "wlan0" then
			local wnet = nw:get_wifinet("radio0.network2")
			local apWnet = nw:get_wifinet("radio0.network1")
			if (apWnet and apWnet:get("user_enable") == "1") or (wnet and wnet:get("disabled") ~= "1") then
				luci.sys.call("env -i /sbin/wifi down >/dev/null 2>/dev/null")
				if wnet and wnet:get("disabled") ~= "1" then 
					wnet:set("disabled", "1")
				end
				if apWnet and apWnet:get("user_enable") == "1" then
					apWnet:set("disabled", nil)
				end
				nw:commit("wireless")
				luci.sys.call("env -i /sbin/wifi up >/dev/null 2>/dev/null")
			end
		end
	end

	--if vpnPIDstatus == 0 then
    --            luci.sys.call("/etc/init.d/openvpn reload > /dev/null")
    --   end
end
--------------------------------------------------------------------------------
-- 3G settings goes here
--------------------------------------------------------------------------------
local apn, pin, dial, auth, user, pass, smode, dial, netsel, mcc, mnc, acc

if interfaceName == "3g-ppp" and ModuleType == "3g_ppp" then
	-- apn --
	apn = s:taboption("general", Value, "__apn", translate("APN"))
	function apn.cfgvalue(self, section)
		return m.uci:get("network", "ppp", "apn") or ""
	end	
	
	-- pin --
	pin = s:taboption("general", Value, "__pincode", translate("PIN number"))
	pin.datatype = "range(0,9999)"
	function pin.cfgvalue(self, section)
		return m.uci:get("network", "ppp", "pincode") or ""
	end
	
	-- dial number --
	--dial = s:taboption("general", Value, "__dialnumber", translate("Dialing number"))
	--dial.default = "*99#"

	-- auth mode --
	auth = s:taboption("general", ListValue, "__auth_mode", translate("Authentication method"))
	auth:value("chap", translate("CHAP"))
	auth:value("pap", translate("PAP"))
	auth:value("none", translate("none"))
	auth.default = "none"
	function auth.cfgvalue(self, section)
		return m.uci:get("network", "ppp", "auth_mode") or "none"
	end
	
	-- username --
	user = s:taboption("general", Value, "__username", translate("Username"))
	user:depends("__auth_mode", "chap")
	user:depends("__auth_mode", "pap")
	function user.cfgvalue(self, section)
		return m.uci:get("network", "ppp", "username") or ""
	end
	
	-- password --
	pass = s:taboption("general", Value, "__password", translate("Password"))
	pass:depends("__auth_mode", "chap")
	pass:depends("__auth_mode", "pap")
	pass.password = true
	function pass.cfgvalue(self, section)
		return m.uci:get("network", "ppp", "password") or ""
	end
	
	-- connection type --
	smode = s:taboption("general", ListValue, "__service", translate("Connection type"))
	smode:value("gprs-only", translate("2G/GPRS/EDGE only"))
-- 	smode:value("gprs", translate("2G/GPRS/EDGE preferred"))
	smode:value("umts-only", translate("3G/UMTS/HSPA only"))
-- 	smode:value("umts", translate("3G/UMTS/HSPA preferred"))
	smode:value("auto", translate("automatic"))
	smode.default = "auto"
	function smode.cfgvalue(self, section)
		return m.uci:get("network", "ppp", "service") or ""
	end
	
	-- network mode --
	netsel = s:taboption("general", ListValue, "__regmode", translate("Network selection"))
	netsel:value("auto", translate("auto"))
	netsel:value("manual", translate("manual"))
	netsel.default = "auto"
	netsel.forcewrite = true
	function netsel.cfgvalue(self, section)
		return m.uci:get("network", "ppp", "regmode") or ""
	end
	
	-- mcc --
	mcc = s:taboption("general", Value, "__mcc", translate("MCC"))
	mcc:depends("__regmode", "manual")
	--mcc.datatype    = "uinteger"
	function mcc.cfgvalue(self, section)
		return m.uci:get("network", "ppp", "mcc") or ""
	end
	
	-- mnc --
	mnc = s:taboption("general", Value, "__mnc", translate("MNC"))
	mnc:depends("__regmode", "manual")
	--mnc.datatype    = "uinteger"
	function mnc.cfgvalue(self, section)
		return m.uci:get("network", "ppp", "mnc") or ""
	end
	
	-- access technology --
	--[[acc = s:taboption("general", ListValue, "__radio", translate("Access technology"))
	acc:depends("__regmode", "manual")
	acc:value("0", translate("GSM"))
	acc:value("2", translate("UMTS"))
	function acc.cfgvalue(self, section)
		return m.uci:get("network", "ppp", "radio") or ""
	end]]
		
	function netsel.write(self, section, value)
		WriteToConfig("network", "ppp", "apn", apn:formvalue(section))
		WriteToConfig("network", "ppp", "pincode", pin:formvalue(section))
		WriteToConfig("network", "ppp", "auth_mode", auth:formvalue(section))
		WriteToConfig("network", "ppp", "username", user:formvalue(section))
		WriteToConfig("network", "ppp", "password", pass:formvalue(section))
		WriteToConfig("network", "ppp", "service", smode:formvalue(section))
		WriteToConfig("network", "ppp", "regmode", netsel:formvalue(section))
		WriteToConfig("network", "ppp", "mcc", mcc:formvalue(section))
		WriteToConfig("network", "ppp", "mnc", mnc:formvalue(section))
		--WriteToConfig("network", "ppp", "radio", acc:formvalue(section))
	end
end

--------------------
-- WiMAX settings --
-- -----------------
local reboot, roff, sW, mWim

if isWiMAX then
	mWim = Map(__define__config_file_name, translate("WiMAX"), 
	translate("Here you can disable WiMAX connection or reboot the modem."))

	-- mWim.skip_apply = true -- No scripts will be relaunched after Save&Apply
	
	sW = mWim:section(TypedSection, "_dummy", translate("WiMAX Management"))
	sW.addremove = false
	sW.anonymous = true

	reboot = sW:option(Button, "_reboot")
	reboot.title      = translate("Reboot WiMAX modem?")
	reboot.inputtitle = translate("Reboot")
	reboot.inputstyle = "apply"

	roff            = sW:option(Button, "_ro")
	roff.title      = translate("Turn on/off WiMAX")
	if nw:check_wimax_state() then
		roff.inputtitle = translate("Turn off")
	else
		roff.inputtitle = translate("Turn on")
	end
	roff.inputstyle = "apply"
	function sW.cfgsections()
		return { __define__network }
	end

	if mWim:formvalue("cbid." .. __define__config_file_name .. "." .. __define__network .. "._reboot") then
		nw:rr_wimax()
		luci.http.redirect(luci.dispatcher.build_url(__define__page_entry_path))
	end

	if mWim:formvalue("cbid." .. __define__config_file_name .. "." .. __define__network .. "._ro") then
		nw:ro_wimax()
		luci.http.redirect(luci.dispatcher.build_url(__define__page_entry_path))
	end

end

----------------
-- IP Aliases --
----------------

local ipaliases_s2, ipaliases_ip, ipaliases_nm, ipaliases_gw, ipaliases_ip6, ipaliases_gw6, ipaliases_bcast, ipaliases_dns

if ExpertMode == "on" then
	ipaliases_s2 = m:section(TypedSection, "alias", translate("IP-Aliases"))
	ipaliases_s2.addremove = true

	ipaliases_s2:depends("interface", __define__network)
	ipaliases_s2.defaults.interface = __define__network

	ipaliases_s2:tab("general", translate("General Setup"))
	ipaliases_s2.defaults.proto = "static"

	ipaliases_ip = ipaliases_s2:taboption("general", Value, "ipaddr", translate("IPv4-Address"))
	ipaliases_ip.optional = true
	ipaliases_ip.datatype = "ip4addr"

	ipaliases_nm = ipaliases_s2:taboption("general", Value, "netmask", translate("IPv4-Netmask"))
	ipaliases_nm.optional = true
	ipaliases_nm.datatype = "ip4addr"
	ipaliases_nm:value("255.255.255.0")
	ipaliases_nm:value("255.255.0.0")
	ipaliases_nm:value("255.0.0.0")

	ipaliases_gw = ipaliases_s2:taboption("general", Value, "gateway", translate("IPv4-Gateway"))
	ipaliases_gw.optional = true
	ipaliases_gw.datatype = "ip4addr"

	if has_ipv6 then
		ipaliases_s2:tab("ipv6", translate("IPv6 Setup"))

		ipaliases_ip6 = ipaliases_s2:taboption("ipv6", Value, "ip6addr", translate("IPv6-Address"), translate("CIDR-Notation: address/prefix"))
		ipaliases_ip6.optional = true
		ipaliases_ip6.datatype = "ip6addr"

		ipaliases_gw6 = ipaliases_s2:taboption("ipv6", Value, "ip6gw", translate("IPv6-Gateway"))
		ipaliases_gw6.optional = true
		ipaliases_gw6.datatype = "ip6addr"
	end
	
	ipaliases_s2:tab("advanced", translate("Advanced Settings"))

	ipaliases_bcast = ipaliases_s2:taboption("advanced", Value, "bcast", translate("IPv4-Broadcast"))
	ipaliases_bcast.optional = true
	ipaliases_bcast.datatype = "ip4addr"

	ipaliases_dns = ipaliases_s2:taboption("advanced", Value, "dns", translate("DNS-Server"))
	ipaliases_dns.optional = true
	ipaliases_dns.datatype = "ip4addr"
end
----------------------------------------------------------------------------------------------------
-- BACKUP WAN
----------------------------------------------------------------------------------------------------
-- FIXME: This shit is rapidly descending into 104* territory, everything, including network.lua (the one that's +30Kb) have to
-- be reviewed to allow for better multi module support!!!!!
-- NOTE: Programmer_2 answer to comment above: we do not have time to code this properly...
-- FIXME: Programmer_2: do not even try to understand this...

mWan = Map("multiwan", translate("Backup Link"),
	translate("Here you can setup your backup link. If your conventional WAN connection, such as wired Ethernet or Wifi, fails, the backup link will enable and take over to keep the router connected."))

moduleStruct = nw:getModule()

local g3_wan_ifname = "usb0" 

local wanifname = nw:get_wan_ifname()

local module = nw:get_module()
local moduleIfname = nw:get_module_fname()
-- eth1 <- wimax
if module == "3g_ppp" then
	g3_wan_ifname = "3g-ppp"
end
s = mWan:section(NamedSection, "config", "multiwan")

if wanifname == g3_wan_ifname or wanifname == "eth1" or wanifname == "wm0" then
	if wanifname == g3_wan_ifname then
		e = s:option(Flag, "enabled", translate("Enable"), translate("3G selected as wan - cannot enable backup link."))
	else
		e = s:option(Flag, "enabled", translate("Enable"), translate("WiMAX selected as wan - cannot enable backup link."))
	end
	e.hardDisabled = true
else
	e = s:option(Flag, "enabled", translate("Enable"))
end


e.rmempty = false
e.default = e.enabled

function e.write(self, section, value)
	local wanifname = nw:get_wan_ifname()
	local PreviousState = mWan.uci:get("multiwan", "config", "enabled")
	local CurrentState = value
	local fail = false
	if DoubleRun == false then
		DoubleRun = true
	else
		return
	end	
		
	if CurrentState ~= PreviousState then
		if CurrentState == "1" and (wanifname == g3_wan_ifname or wanifname == "eth1" or wanifname == "wm0") then
			if wanifname == g3_wan_ifname then
				m.message = translate("wrn:3G selected as wan - cannot enable backup link.")
			else
				m.message = translate("wrn:WiMAX selected as wan - cannot enable backup link.")
			end
			return
		end
		
		if CurrentState == "1" then
			if module == "3g_ppp" then
				mWan.uci:set("network", "ppp", "enabled", "1")
				mWan.uci:set("network", "ppp", "mwmode", "1")
				mWan.uci:save("network")
				mWan.uci:commit("network")
			
				local opts = { proto	= "dhcp",
								ifname	= g3_wan_ifname }
			
				nw:add_network("wan2", opts)
				local zn = fw:get_zone("wan")
				zn:add_network("wan2")
				--os.execute("/etc/init.d/multiwan enable")
			else
				fail = true
			end
		else
			if module == "3g_ppp" then
				mWan.uci:set("network", "ppp", "enabled", "0")
				mWan.uci:set("network", "ppp", "mwmode", "0")
				mWan.uci:save("network")
				mWan.uci:commit("network")
				nw:del_network("wan2")
				--os.execute("/etc/init.d/multiwan stop 2>/dev/null 1>&2")
			else
				fail = true
			end
			
		end	
		
		if fail then
			m.message = translate("wrn:No 3G module detected!")
			return
		end
		nw:save("network")
		nw:commit("network")
		Flag.write(self, section, value)
	end	
	
	--[[
	if wanifname == g3_wan_ifname or wanifname == "eth1" or wanifname == "wm0" then
		--Flag.write(self, section, "0")
		if wanifname == g3_wan_ifname then
			m.message = translate("wrn:3G selected as wan - cannot enable backup link.")
		else
			m.message = translate("wrn:WiMAX selected as wan - cannot enable backup link.")
		end
		--luci.http.redirect(luci.dispatcher.build_url("admin/network/wan"))
		return
	else
		if value == "0" then
			if module == "3g_ppp" then
				mWan.uci:set("network", "ppp", "enabled", "0")
				mWan.uci:save("network")
				mWan.uci:commit("network")
			end
			nw:del_network("wan2")
			os.execute("/etc/init.d/multiwan stop")
		else
			if module == "3g_ppp" then
				mWan.uci:set("network", "ppp", "enabled", "1")
				mWan.uci:save("network")
				mWan.uci:commit("network")
			end
			local opts = {
				proto	= "dhcp",
				ifname	= g3_wan_ifname
			}
			if module == "wimax" then
				opts.ifname = moduleStruct:get_iface()
			end
			nw:add_network("wan2", opts)
			local zn = fw:get_zone("wan")
			zn:add_network("wan2")
			--os.execute("/etc/init.d/multiwan enable")
		end
		nw:save("network")
		nw:commit("network")
		Flag.write(self, section, value)
	end]]
end

s = mWan:section(NamedSection, "wan", "interface", translate("Timing & other parameters"),
	translate("Timing & other parameters will indicate how and when it will be determined that your conventional connection has gone down."))
s.addremove = false

--[[weight = s:option(ListValue, "weight", translate("Load Balancer Distribution"))
weight:value("10", "10")
weight:value("9", "9")
weight:value("8", "8")
weight:value("7", "7")
weight:value("6", "6")
weight:value("5", "5")
weight:value("4", "4")
weight:value("3", "3")
weight:value("2", "2")
weight:value("1", "1")
weight:value("disable", translate("None"))
weight.default = "10"
weight.optional = false
weight.rmempty = false]]

interval = s:option(ListValue, "health_interval", translate("Health Monitor Interval"))
interval:value("disable", translate("Disable"))
interval:value("5", "5 sec.")
interval:value("10", "10 sec.")
interval:value("20", "20 sec.")
interval:value("30", "30 sec.")
interval:value("60", "60 sec.")
interval:value("120", "120 sec.")
interval.default = "10"
interval.optional = false
interval.rmempty = false

--icmp_hosts = s:option(ListValue, "icmp_hosts", translate("Health Monitor ICMP Host(s)"))
icmp_hosts = s:option(Value, "icmp_hosts", translate("Health Monitor ICMP Host(s)"))
icmp_hosts:value("disable", translate("Disable"))
icmp_hosts:value("dns", "DNS Server(s)")
icmp_hosts:value("gateway", "WAN Gateway")
icmp_hosts.default = "dns"
icmp_hosts.optional = false
icmp_hosts.rmempty = false

timeout = s:option(ListValue, "timeout", translate("Health Monitor ICMP Timeout"))
timeout:value("1", "1 sec.")
timeout:value("2", "2 sec.")
timeout:value("3", "3 sec.")
timeout:value("4", "4 sec.")
timeout:value("5", "5 sec.")
timeout:value("10", "10 sec.")
timeout.default = "3"
timeout.optional = false
timeout.rmempty = false

fail = s:option(ListValue, "health_fail_retries", translate("Attempts Before WAN Failover"))
fail:value("1", "1")
fail:value("3", "3")
fail:value("5", "5")
fail:value("10", "10")
fail:value("15", "15")
fail:value("20", "20")
fail.default = "3"
fail.optional = false
fail.rmempty = false

recovery = s:option(ListValue, "health_recovery_retries", translate("Attempts Before WAN Recovery"))
recovery:value("1", "1")
recovery:value("3", "3")
recovery:value("5", "5")
recovery:value("10", "10")
recovery:value("15", "15")
recovery:value("20", "20")
recovery.default = "5"
recovery.optional = false
recovery.rmempty = false

--[[failover_to = s:option(ListValue, "failover_to", translate("Failover Traffic Destination"))
failover_to:value("disable", translate("None"))
luci.tools.webadmin.cbi_add_networks(failover_to)
failover_to:value("fastbalancer", translate("Load Balancer(Performance)"))
failover_to:value("balancer", translate("Load Balancer(Compatibility)"))
failover_to.default = "balancer"
failover_to.optional = false
failover_to.rmempty = false]]

--[[dns = s:option(Value, "dns", translate("DNS Server(s)"))
dns:value("auto", translate("Auto"))
dns.default = "auto"
dns.optional = false
dns.rmempty = true]]

s = mWan:section(NamedSection, "wan2", "interface", translate("Backup ICMP host"),
	translate("A remote host that will be used to test wether your backup link is alive."))
s.addremove = false

o = s:option(Value, "icmp_hosts", translate("ICMP host"))
o.optional = false
o.datatype = "ip4addr"

--[[s = m:section(TypedSection, "mwanfw", translate("Multi-WAN Traffic Rules"),
	translate("Configure rules for directing outbound traffic through specified WAN Uplinks."))
s.template = "cbi/tblsection"
s.anonymous = true
s.addremove = true

src = s:option(Value, "src", translate("Source Address"))
src.rmempty = true
src:value("", translate("all"))
luci.tools.webadmin.cbi_add_knownips(src)

dst = s:option(Value, "dst", translate("Destination Address"))
dst.rmempty = true
dst:value("", translate("all"))
luci.tools.webadmin.cbi_add_knownips(dst)

proto = s:option(Value, "proto", translate("Protocol"))
proto:value("", translate("all"))
proto:value("tcp", "TCP")
proto:value("udp", "UDP")
proto:value("icmp", "ICMP")
proto.rmempty = true

ports = s:option(Value, "ports", translate("Ports"))
ports.rmempty = true
ports:value("", translate("all", translate("all")))

wanrule = s:option(ListValue, "wanrule", translate("WAN Uplink"))
luci.tools.webadmin.cbi_add_networks(wanrule)
wanrule:value("fastbalancer", translate("Load Balancer(Performance)"))
wanrule:value("balancer", translate("Load Balancer(Compatibility)"))
wanrule.default = "fastbalancer"
wanrule.optional = false
wanrule.rmempty = false

s = m:section(NamedSection, "config", "", "")
s.addremove = false

default_route = s:option(ListValue, "default_route", translate("Default Route"))
luci.tools.webadmin.cbi_add_networks(default_route)
default_route:value("fastbalancer", translate("Load Balancer(Performance)"))
default_route:value("balancer", translate("Load Balancer(Compatibility)"))
default_route.default = "balancer"
default_route.optional = false
default_route.rmempty = false]]

-- NOTE Programmer_2 comment about function below: 
-- First of all you should know, that original back_wan lua file had this function enabled, but not anymore.
-- But why? The reason why I commented the code below is that weird things started to occur (e.g. 3G link has higher 
-- priority then wired link, so multiwan won't work as expected)

-- function mWan.on_parse(self)
-- 	-- We will ettempt to push multiwan to the very end of the parse chain, hopefully making it run last in the init script sequence, hence fixing the problem that has been plagueing me for fucking ever
-- 	--luci.sys.call("echo \"on_parse called\" >> /tmp/log.log")
-- 	self.parsechain[1] = "network"
-- 	self.parsechain[2] = "firewall"
-- 	self.parsechain[3] = "multiwan"
-- 	--for k, v in pairs(self.parsechain) do
-- 		--luci.sys.call("echo \"k: [" .. k .. "], v: [" .. v .. "]\" >> /tmp/log.log")
-- 	--end
-- end

-----------------
-- End: return --
-----------------
if isWiMAX then
	return m, mWim, mWan
end
return m, mWan

