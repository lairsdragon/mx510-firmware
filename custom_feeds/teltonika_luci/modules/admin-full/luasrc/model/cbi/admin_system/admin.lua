--[[
Teltonika R&D. ver 0.1
]]--


local fs = require "nixio.fs"
local uci  = require "luci.model.uci".cursor()
local fw = require "luci.model.firewall"

--m = Map("system", translate("Administration properties"),
	--translate("Changes the administration password, log level and provides SSH access control."))
m = Map("system")

s = m:section(TypedSection, "_dummy", "Administrator Password")
s.addremove = false
s.anonymous = true

pw1 = s:option(Value, "pw1", translate("Password"))
pw1.password = true

pw2 = s:option(Value, "pw2", translate("Confirmation"))
pw2.password = true

function s.cfgsections()
	return { "_pass" }
end

function m.on_commit(map)
	local v1 = pw1:formvalue("_pass")
	local v2 = pw2:formvalue("_pass")

	if v1 and v2 and #v1 > 0 and #v2 > 0 then
		if v1 == v2 then
			if luci.sys.user.setpasswd(luci.dispatcher.context.authuser, v1) == 0 then
				m.message = translate("scs: Password successfully changed!")
			else
				m.message = translate("err: Unknown Error, password not changed!")
			end
		else
			m.message = translate("Given password confirmation did not match, password not changed!")
		end
	end
end
--[[
s2 = m:section(TypedSection, "system", translate("Logging"))
s2.addremove = false

con = s2:option(ListValue, "conloglevel", translate("System log level"))
con:value(8, translate("Debug"))
con:value(7, translate("Info"))
con:value(6, translate("Notice"))
con:value(5, translate("Warning"))
con:value(4, translate("Error"))
con:value(3, translate("Critical"))
con:value(2, translate("Alert"))
con:value(1, translate("Emergency"))

conLog = s2:option(Button, "_log")
conLog.title      = translate("System Log")
conLog.inputtitle = translate("Show")
conLog.inputstyle = "apply"

conLog = s2:option(Button, "_kerlog")
conLog.title      = translate("Kernel Log")
conLog.inputtitle = translate("Show")
conLog.inputstyle = "apply"

if m:formvalue("cbid.system.cfg02e48a._log") then
	luci.http.redirect(luci.dispatcher.build_url("admin/status/syslog"))
end

if m:formvalue("cbid.system.cfg02e48a._kerlog") then
	luci.http.redirect(luci.dispatcher.build_url("admin/status/dmesg"))
end


fw.init(uci)

-- cron = s2:option(ListValue, "cronloglevel", translate("Cron Log Level"))
-- cron.default = 8
-- cron:value(5, translate("Debug"))
-- cron:value(8, translate("Normal"))
-- cron:value(9, translate("Warning"))

if fs.access("/etc/config/dropbear") then

	m2 = Map("dropbear", "","")
	m2:chain("firewall")

	s = m2:section(TypedSection, "dropbear", translate("SSH Access control"))
	s.anonymous = true
	s.addremove = false
	
	p = s:option(ListValue, "enable",  translate("SSH Access"))
	p:value(1, "Enabled") -- Key and value pairs
	p:value(0, "Disabled")
	p.default = 1
	
	pt = s:option(Value, "Port", translate("Port"),
	translate("Port to listen for SSH access."))

	pt.datatype = "port"
	pt.default  = 22

	o = s:option(Flag, "_sshWanAccess", translate("Remote SSH Access"))
	o.rmempty = false
	o.enabled = "1"
	o.disabled = "0"

	function o.write(self, section)
		local fval = self:formvalue(section)
		local fvalPort = pt:formvalue(section)
		if not fval then
			fval = "0"
		end
		
		local cval
		m2.uci:foreach("dropbear", "dropbear", function(s)
			cval = s.Interface
			dropBearInstName = s[".name"]
		end)
		if cval == "lan" then
			cval = "0"
		elseif not cval or cval == "wan" then
			cval = "1"
		end
		
		local dropBearInstName
		m2.uci:foreach("dropbear", "dropbear", function(s)
			dropBearInstName = s[".name"]
		end)
		
		local fwRuleInstName = "nil"
		local needsPortUpdate = false
		m2.uci:foreach("firewall", "rule", function(s)
			if s.name == "Enable_SSH_WAN" then
				fwRuleInstName = s[".name"]
				if s.dest_port ~= fvalPort then
					needsPortUpdate = true
				end
				if not (cval == "1" and s.enabled ~= "0") then
					cval = "0"
				end
			end
		end)
		if needsPortUpdate == true then
			m2.uci:set("firewall", fwRuleInstName, "dest_port", fvalPort)
			m2.uci:save("firewall")
		end
		if fwRuleInstName == "nil" then
			local wanZone = fw:get_zone("wan")
			if not wanZone then
				m.message = translate("err: Error: could not add firewall rule!")
				return
			end
			local options = {
				target 		= "ACCEPT",
				proto 		= "tcp udp",
				dest_port 	= fvalPort,
				name 		= "Enable_SSH_WAN",
				enabled		= "0"
			}
			cval = "0"
			wanZone:add_rule(options)
			m2.uci:save("firewall")
		end
		
-- 		if ((fval == self.enabled) and (cval == "0")) or ((fval == self.disabled) and (cval == "1")) then
			if fval == self.enabled then
				m2.uci:delete("firewall", fwRuleInstName, "enabled")
				m2.uci:delete("dropbear", dropBearInstName, "Interface")
			elseif fval == self.disabled then
				m2.uci:set("firewall", fwRuleInstName, "enabled", "0")
				m2.uci:set("dropbear", dropBearInstName, "Interface", "lan")
			end
			m2.uci:save("dropbear")
			m2.uci:save("firewall")
-- 		end
	end

	function o.cfgvalue(self, section)
		local cval
		local fwRuleEn = false
		m2.uci:foreach("dropbear", "dropbear", function(s)
			cval = s.Interface
		end)
		m2.uci:foreach("firewall", "rule", function(s)
			if s.name == "Enable_SSH_WAN" and s.enabled ~= "0" then
				fwRuleEn = true
			end
		end)
		
		if (not cval) then return self.enabled end
			if (cval == "wan") and (fwRuleEn == true) then
				return self.enabled
			else 
				return self.disabled
			end
	end

-- 	o = s:option(Value, "_test", translate("for testing purposes"))
-- 	function o.cfgvalue(self, section)
-- 		return "cheese"
-- 	end
-- 	
-- 	o = s:option(Flag, "_test2", translate("for testing purposes2"))
-- 	function o.cfgvalue(self, section)
-- 		return self.enabled
-- 	end
	
end

if fs.access("/etc/config/uhttpd") then
	m3 = Map("uhttpd", "", "")
	m3:chain("firewall")
	
	s = m3:section(NamedSection, "main", "uhttpd", translate("Web Access control"))
	
	prt = s:option(Value, "listen_http",  translate("HTTP Web Server port"))
	prt.datatype = "port"
	prt.rmempty = false

	function prt.cfgvalue(self, section)	
		local cport = AbstractValue.cfgvalue(self, section)
		if cport then
			return cport:gsub("(%d+.%d+.%d+.%d+:)","")
		end
	end

	function prt.write(self, section, value)	
		uci:set("firewall","service_HTTP","dest_port", value)
		uci:save("firewall")
		AbstractValue.write(self, section, "0.0.0.0:"..value)
	end	
	
	o = s:option(Flag, "_httpWanAccess", translate("Remote HTTP Access"))
	o.rmempty = false

	function o.write(self, section)
		local fval = self:formvalue(section)
		local fvalPort = prt:formvalue(section)
		if not fval then
			fval = "0"
		end
		
		local cval = uci:get("uhttpd", "main", "wan_access")
		if not cval then
			cval = "0"
		end
		
		local fwRuleInstName = "nil"
		local needsPortUpdate = false
		uci:foreach("firewall", "rule", function(s)
			if s.name == "Enable_HTTP_WAN" then
				fwRuleInstName = s[".name"]
				if s.dest_port ~= fvalPort then
					needsPortUpdate = true
				end
			end
		end)
		if needsPortUpdate == true then
			uci:set("firewall", fwRuleInstName, "dest_port", fvalPort)
			uci:save("firewall")
		end
		if fwRuleInstName == "nil" then
			local wanZone = fw:get_zone("wan")
			if not wanZone then
				m.message = translate("err: Error: could not add firewall rule!")
				return
			end
			local options = {
				target 		= "ACCEPT",
				proto 		= "tcp udp",
				dest_port 	= fvalPort,
				name 		= "Enable_HTTP_WAN",
				enabled		= "0"
			}
			wanZone:add_rule(options)
			uci:save("firewall")
			
			uci:foreach("firewall", "rule", function(s)
				if s.name == "Enable_HTTP_WAN" then
					fwRuleInstName = s[".name"]
					if s.dest_port ~= fvalPort then
						needsPortUpdate = true
					end
				end
			end)
		end
		
		if cval ~= fval then
			if fval == "1" then
				uci:delete("firewall", fwRuleInstName, "enabled")
				uci:set("uhttpd", "main", "wan_access", "1")
			elseif fval == "0" then
				uci:set("firewall", fwRuleInstName, "enabled", "0")
				uci:delete("uhttpd", "main", "wan_access")
			end
			uci:save("uhttpd")
			uci:save("firewall")
		end
	end

	function o.cfgvalue(self, section)
		local cval = uci:get("uhttpd", "main", "wan_access")
		if not cval then
			cval = "0"
		end
		return cval
	end
	
	prt_https = s:option(Value, "listen_https",  translate("HTTPS Web Server port"))
	prt_https.datatype = "port"
	prt_https.rmempty = false

	function prt_https.cfgvalue(self, section)	
		local cport = AbstractValue.cfgvalue(self, section)
		if cport then
			return cport:gsub("(%d+.%d+.%d+.%d+:)","")
		end
	end

	function prt_https.write(self, section, value)	
		uci:set("firewall","service_HTTPS","dest_port", value)
		uci:save("firewall")
		AbstractValue.write(self, section, "0.0.0.0:"..value)
	end	
	
	o = s:option(Flag, "_httpsWanAccess", translate("Remote HTTPS Access"))
	o.rmempty = false

	function o.write(self, section)
		local fval = self:formvalue(section)
		local fvalPort = prt_https:formvalue(section)
		if not fval then
			fval = "0"
		end
		
		local cval = uci:get("uhttpd", "main", "wan_https_access")
		if not cval then
			cval = "0"
		end
		
		local fwRuleInstName = "nil"
		local needsPortUpdate = false
		uci:foreach("firewall", "rule", function(s)
			if s.name == "Enable_HTTPS_WAN" then
				fwRuleInstName = s[".name"]
				if s.dest_port ~= fvalPort then
					needsPortUpdate = true
				end
			end
		end)
		if needsPortUpdate == true then
			uci:set("firewall", fwRuleInstName, "dest_port", fvalPort)
			uci:save("firewall")
		end
		if fwRuleInstName == "nil" then
			local wanZone = fw:get_zone("wan")
			if not wanZone then
				m.message = translate("err: Error: could not add firewall rule!")
				return
			end
			local options = {
				target 		= "ACCEPT",
				proto 		= "tcp udp",
				dest_port 	= fvalPort,
				name 		= "Enable_HTTPS_WAN",
				enabled		= "0"
			}
			wanZone:add_rule(options)
			uci:save("firewall")
			
			uci:foreach("firewall", "rule", function(s)
				if s.name == "Enable_HTTPS_WAN" then
					fwRuleInstName = s[".name"]
					if s.dest_port ~= fvalPort then
						needsPortUpdate = true
					end
				end
			end)
		end
		if cval ~= fval then
			if fval == "1" then
				uci:delete("firewall", fwRuleInstName, "enabled")
				uci:set("uhttpd", "main", "wan_https_access", "1")
			elseif fval == "0" then
				uci:set("firewall", fwRuleInstName, "enabled", "0")
				uci:delete("uhttpd", "main", "wan_https_access")
			end
			uci:save("uhttpd")
			uci:save("firewall")
		end
	end

	function o.cfgvalue(self, section)
		local cval = uci:get("uhttpd", "main", "wan_https_access")
		if not cval then
			cval = "0"
		end
		return cval
	end
	
end
]]
return m, m2, m3
