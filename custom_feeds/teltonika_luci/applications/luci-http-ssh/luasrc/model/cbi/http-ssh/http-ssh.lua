local fs = require "nixio.fs"
local uci  = require "luci.model.uci".cursor()
local fw = require "luci.model.firewall"

local deathTrap = false

fw.init(uci)

if fs.access("/etc/config/dropbear") then

	m2 = Map("dropbear", "","")
	m2:chain("firewall")

	s = m2:section(TypedSection, "dropbear", translate("SSH Access control"))
	s.anonymous = true
	s.addremove = false
	
	p = s:option(ListValue, "enable",  translate("SSH Access"))
	p:value(1, "Enabled") -- Key and value pairs
	p:value(0, "Disabled")
	p.default = 1
	
	pt = s:option(Value, "Port", translate("Port"),
	translate("Port to listen for SSH access."))

	pt.datatype = "port"
	pt.default  = 22

	o = s:option(Flag, "_sshWanAccess", translate("Remote SSH Access"))
	o.rmempty = false
	o.enabled = "1"
	o.disabled = "0"

	local doubleTap = true
	
	function o.write(self, section)
		local fval = self:formvalue(section)
		local fvalPort = pt:formvalue(section)
		local dropBearInstName
		
		-- stop this function being called twice
		if not deathTrap then deathTrap = true
		else return end
		
		-- fix some firewall rules incompatibility issues
		if not fval then
			fval = "0"
		else
			fval = ""
		end
	
		local fwRuleInstName = "nil"
		local needsPortUpdate = false
		m2.uci:foreach("firewall", "rule", function(s)
			if s.name == "Enable_SSH_WAN" then
				fwRuleInstName = s[".name"]
				if s.dest_port ~= fvalPort then
					needsPortUpdate = true
				end
				if s.enabled ~= fval then
					needsPortUpdate = true	
				end
			end
		end)
		
		if needsPortUpdate == true then
			m2.uci:set("firewall", fwRuleInstName, "dest_port", fvalPort)
			m2.uci:set("firewall", fwRuleInstName, "enabled", fval)
			m2.uci:save("firewall")
		end
		
		if fwRuleInstName == "nil" then
			local wanZone = fw:get_zone("wan")
			if not wanZone then
				m2.message = translate("err: Error: could not add firewall rule!")
				return
			end
			local options = {
				target 		= "ACCEPT",
				proto 		= "tcp udp",
				dest_port 	= fvalPort,
				name 		= "Enable_SSH_WAN",
				enabled		= fval
			}
			wanZone:add_rule(options)
			m2.uci:save("firewall")
		end
		m2.uci.commit("firewall")
	end

	function o.cfgvalue(self, section)
		local fwRuleEn = false

		m2.uci:foreach("firewall", "rule", function(s)
			if s.name == "Enable_SSH_WAN" and s.enabled ~= "0" then
				fwRuleEn = true
			end
		end)
		
		if fwRuleEn then 
			return self.enabled
		else
			return self.disabled
		end
	end

-- 	o = s:option(Value, "_test", translate("for testing purposes"))
-- 	function o.cfgvalue(self, section)
-- 		return "cheese"
-- 	end
-- 	
-- 	o = s:option(Flag, "_test2", translate("for testing purposes2"))
-- 	function o.cfgvalue(self, section)
-- 		return self.enabled
-- 	end
	
end

if fs.access("/etc/config/uhttpd") then
	m3 = Map("uhttpd", "", "")
	m3:chain("firewall")
	
	s = m3:section(NamedSection, "main", "uhttpd", translate("Web Access control"))
	
	prt = s:option(Value, "listen_http",  translate("HTTP Web Server port"))
	prt.datatype = "port"
	prt.rmempty = false

	function prt.cfgvalue(self, section)	
		local cport = AbstractValue.cfgvalue(self, section)
		if cport then
			return cport:gsub("(%d+.%d+.%d+.%d+:)","")
		end
	end

	function prt.write(self, section, value)
		-- check if user inserted different http and https ports
		portHttp = value
		portHttps = luci.http.formvalue("cbid.uhttpd.main.listen_https")
		if portHttp and portHttps then
			if portHttp == portHttps then
				m3.message = translate("err: Error: HTTP port must be different!")
				return
			end	
		end
		uci:set("firewall","service_HTTP","dest_port", value)
		uci:save("firewall")
		AbstractValue.write(self, section, "0.0.0.0:"..value)
	end	
	
	o = s:option(Flag, "_httpWanAccess", translate("Remote HTTP Access"))
	o.rmempty = false

	function o.write(self, section)
		local fval = self:formvalue(section)
		local fvalPort = prt:formvalue(section)
		if not fval then
			fval = "0"
		end
		
		local cval = uci:get("uhttpd", "main", "wan_access")
		if not cval then
			cval = "0"
		end
		
		local fwRuleInstName = "nil"
		local needsPortUpdate = false
		uci:foreach("firewall", "rule", function(s)
			if s.name == "Enable_HTTP_WAN" then
				fwRuleInstName = s[".name"]
				if s.dest_port ~= fvalPort then
					needsPortUpdate = true
				end
			end
		end)
		if needsPortUpdate == true then
			uci:set("firewall", fwRuleInstName, "dest_port", fvalPort)
			uci:save("firewall")
		end
		if fwRuleInstName == "nil" then
			local wanZone = fw:get_zone("wan")
			if not wanZone then
				m3.message = translate("err: Error: could not add firewall rule!")
				return
			end
			local options = {
				target 		= "ACCEPT",
				proto 		= "tcp udp",
				dest_port 	= fvalPort,
				name 		= "Enable_HTTP_WAN",
				enabled		= "0"
			}
			wanZone:add_rule(options)
			uci:save("firewall")
			
			uci:foreach("firewall", "rule", function(s)
				if s.name == "Enable_HTTP_WAN" then
					fwRuleInstName = s[".name"]
					if s.dest_port ~= fvalPort then
						needsPortUpdate = true
					end
				end
			end)
		end
		
		if cval ~= fval then
			if fval == "1" then
				uci:delete("firewall", fwRuleInstName, "enabled")
				uci:set("uhttpd", "main", "wan_access", "1")
			elseif fval == "0" then
				uci:set("firewall", fwRuleInstName, "enabled", "0")
				uci:delete("uhttpd", "main", "wan_access")
			end
			uci:save("uhttpd")
			uci:save("firewall")
		end
	end

	function o.cfgvalue(self, section)
		local cval = uci:get("uhttpd", "main", "wan_access")
		if not cval then
			cval = "0"
		end
		return cval
	end
	
	prt_https = s:option(Value, "listen_https",  translate("HTTPS Web Server port"))
	prt_https.datatype = "port"
	prt_https.rmempty = false

	function prt_https.cfgvalue(self, section)	
		local cport = AbstractValue.cfgvalue(self, section)
		if cport then
			return cport:gsub("(%d+.%d+.%d+.%d+:)","")
		end
	end

	function prt_https.write(self, section, value)	
		-- check if user inserted different http and https ports
		portHttp = prt:formvalue(section)
		portHttps = value
		if portHttp and portHttps then
			if portHttp == portHttps then
				m3.message = translate("err: Error: HTTPS port must be different!")
				return
			end	
		end
		
		uci:set("firewall","service_HTTPS","dest_port", value)
		uci:save("firewall")
		AbstractValue.write(self, section, "0.0.0.0:"..value)
	end	
	
	o = s:option(Flag, "_httpsWanAccess", translate("Remote HTTPS Access"))
	o.rmempty = false
	o.forcewrite = true

	function o.write(self, section)		
		local fval = self:formvalue(section)
		local fvalPort = prt_https:formvalue(section)
		if not fval then
			fval = "0"
		end
		
		local cval = uci:get("uhttpd", "main", "wan_https_access")
		if not cval then
			cval = "0"
		end
		
		local fwRuleInstName = "nil"
		local needsPortUpdate = false
		uci:foreach("firewall", "rule", function(s)
			if s.name == "Enable_HTTPS_WAN" then
				fwRuleInstName = s[".name"]
				if s.dest_port ~= fvalPort then
					needsPortUpdate = true
				end
			end
		end)
		if needsPortUpdate == true then
			uci:set("firewall", fwRuleInstName, "dest_port", fvalPort)
			uci:save("firewall")
		end
		if fwRuleInstName == "nil" then
			local wanZone = fw:get_zone("wan")
			if not wanZone then
				m3.message = translate("err: Error: could not add firewall rule!")
				return
			end
			local options = {
				target 		= "ACCEPT",
				proto 		= "tcp udp",
				dest_port 	= fvalPort,
				name 		= "Enable_HTTPS_WAN",
				enabled		= "0"
			}
			wanZone:add_rule(options)
			uci:save("firewall")
			
			uci:foreach("firewall", "rule", function(s)
				if s.name == "Enable_HTTPS_WAN" then
					fwRuleInstName = s[".name"]
					if s.dest_port ~= fvalPort then
						needsPortUpdate = true
					end
				end
			end)
		end
		if cval ~= fval then
			if fval == "1" then
				uci:delete("firewall", fwRuleInstName, "enabled")
				uci:set("uhttpd", "main", "wan_https_access", "1")
			elseif fval == "0" then
				uci:set("firewall", fwRuleInstName, "enabled", "0")
				uci:delete("uhttpd", "main", "wan_https_access")
			end
			uci:save("uhttpd")
			uci:save("firewall")
		end
	end

	function o.cfgvalue(self, section)
		local cval = uci:get("uhttpd", "main", "wan_https_access")
		if not cval then
			cval = "0"
		end
		return cval
	end
	
end

return m2, m3