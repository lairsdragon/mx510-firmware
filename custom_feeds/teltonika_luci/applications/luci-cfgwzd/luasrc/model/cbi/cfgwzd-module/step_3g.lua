local moduleType = luci.util.trim(luci.sys.exec("uci get system.module.type"))

local m, s, o
if moduleType == "3g_ppp" then
m = Map("network", translate("3G Configuration"), 
	translate("Next, let's configure your 3G settings so you can start using internet right away."))
m.wizStep = 2
m.addremove = false

s = m:section(NamedSection, "ppp", "interface", translate("3G Configuration"));
s.addremove = false

o = s:option(Value, "apn", translate("APN"))

o = s:option(Value, "pincode", translate("PIN number"))
o.datatype = "range(0,9999)"

--o = s:option(Value, "dialnumber", translate("Dialing number"))
--o.default = "*99#"

o = s:option(ListValue, "auth_mode", translate("Authentication method"))
o:value("chap", translate("CHAP"))
o:value("pap", translate("PAP"))
o:value("none", translate("none"))
o.default = "none"

o = s:option(Value, "username", translate("Username"))
o:depends("auth_mode", "chap")
o:depends("auth_mode", "pap")

o = s:option(Value, "password", translate("Password"))
o.password = true;
o:depends("auth_mode", "chap")
o:depends("auth_mode", "pap")

o = s:option(ListValue, "service", translate("Connection type"))
o:value("gprs-only", translate("2G/GPRS/EDGE only"))
-- o:value("gprs", translate("2G/GPRS/EDGE preferred"))
o:value("umts-only", translate("3G/UMTS/HSPA only"))
-- o:value("umts", translate("3G/UMTS/HSPA preferred"))
o:value("auto", translate("automatic"))
o.default = "auto"

o = s:option(ListValue, "regmode", translate("Network selection"))
o:value("auto", translate("auto"))
o:value("manual", translate("manual"))
o.default = "auto"
	
o = s:option(Value, "mcc", translate("MCC"))
o:depends("regmode", "manual")
	
o = s:option(Value, "mnc", translate("MNC"))
o:depends("regmode", "manual")

--[[o = s:option(ListValue, "radio", translate("Access technology"))
o:depends("regmode", "manual")
o:value("0", translate("GSM"))
o:value("2", translate("UMTS"))]]

else
m = Map("network_3g", translate("Step - 3G"), 
		translate("Next, let's configure your 3G settings so you can start using internet right away."))

m.wizStep = 2

--[[
config custom_interface '3g'
        option pin 'kazkas'
        option apn 'kazkas'
        option user 'kazkas'
        option password 'kazkas'
        option auth_mode 'chap' ARBA 'pap' (jei nerandu nieko ar kazka kita, laikau kad auth nenaudojama)
        option net_mode 'gsm' ARBA 'umts' ARBA 'auto' (prefered tinklas. jei nerandu nieko arba kazka kita laikau kad auto)
        option data_mode 'enabled' ARBA 'disabled' (ar leisti siusti duomenis. jei nera nieko ar kazkas kitas, laikau kad enabled)
]]
m.addremove = false

s = m:section(NamedSection, "3g", "custom_interface", translate("3G Configuration"));
s.addremove = false

o = s:option(Value, "apn", translate("APN"))

o = s:option(Value, "pin", translate("PIN number"))
o.datatype = "range(0,9999)"

auth = s:option(ListValue, "auth_mode", translate("3G authentication method"))

auth:value("chap", translate("CHAP"))
auth:value("pap", translate("PAP"))
auth:value("none", translate("none"))
auth.default = "none"

o = s:option(Value, "user", translate("Username"))
o:depends("auth_mode", "chap")
o:depends("auth_mode", "pap")

o = s:option(Value, "password", translate("Password"))
o:depends("auth_mode", "chap")
o:depends("auth_mode", "pap")
o.password = true;

o = s:option(ListValue, "net_mode", translate("Prefered network"))

o:value("gsm", translate("2G"))
o:value("umts", translate("3G"))
o:value("auto", translate("auto"))

o.default = "auto"

--[[o = s:option(Flag, "data_mode", translate("Data mode"))

o.enabled = "enabled"
o.disabled = "disabled"]]
end
if m:formvalue("cbi.wizard.next") then
	luci.http.redirect(luci.dispatcher.build_url("admin/system/wizard/step-lan"))
end

return m