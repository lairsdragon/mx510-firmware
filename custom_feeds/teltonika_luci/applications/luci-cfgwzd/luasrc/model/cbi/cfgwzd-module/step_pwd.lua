local m, s, o

--
-- Password
--

m = Map("system", translate("Step - Password"),
	translate("First, let's change your router password from the default one."))
m:chain("network")

m.wizStep = 1

s = m:section(TypedSection, "_dummy", "")
s.addremove = false
s.anonymous = true

function s.cfgsections()
	return { "_pass" }
end

pw1 = s:option(Value, "pw1", translate("Password"))
pw1.password = true

pw2 = s:option(Value, "pw2", translate("Confirmation"))
pw2.password = true

function nextStep()
	local x, a
	x = uci.cursor()
	a = x:get("system", "module", "type")
	if a == "3g" or a == "3g_ppp"  then
		luci.http.redirect(luci.dispatcher.build_url("admin/system/wizard/step-3g"))
	else
		luci.http.redirect(luci.dispatcher.build_url("admin/system/wizard/step-lan"))
	end
	return
end

if m:formvalue("cbi.wizard.next") then
	local v1 = pw1:formvalue("_pass")
	local v2 = pw2:formvalue("_pass")

	if v1 and v2 and #v1 > 0 and #v2 > 0 then
		if v1 == v2 then
			if luci.sys.user.setpasswd(luci.dispatcher.context.authuser, v1) == 0 then
				m.message = translate("scs:Password successfully changed!")
				nextStep()
			else
				m.message = translate("err:Unknown Error, password not changed!")
			end
		else
			m.message = translate("err:Given password confirmation did not match, password not changed!")
		end
	else
		nextStep()
	end
end

if m:formvalue("cbi.wizard.skip") then
	x = uci.cursor()
	x:delete("teltonika", "sys", "first_login")
	
	x:commit("teltonika")
	luci.http.redirect(luci.dispatcher.build_url("/admin/status/sysinfo"))
end

return m