--[[
LuCI - Lua Configuration Interface

Copyright 2008 Steven Barth <steven@midlink.org>
Copyright 2008-2011 Jo-Philipp Wich <xm@subsignal.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

$Id: ifaces.lua 7717 2011-10-13 16:26:59Z jow $
]]--
local m, s, o

local fs = require "nixio.fs"
local ut = require "luci.util"
local nw = require "luci.model.network"
local fw = require "luci.model.firewall"
local has_dnsmasq  = fs.access("/etc/config/dhcp")

arg[1] = "lan"

m = Map("network", translate("Step - LAN"), translate("Here we will configure the basic settings of a typical LAN configuration. The wizard will cover 2 basic configurations: static IP address LAN and DHCP client."))
m:chain("wireless")

m.wizStep = 2

if m:formvalue("cbid.network._wired._next") then
	x = uci.cursor()
	x:set("system", "wizard", "1")
	x:commit("system")
	luci.http.redirect(luci.dispatcher.build_url("admin/status"))
	return
end

if m:formvalue("cbi.apply") then
	m.message = translate("wrn:Configuration applied. If there are no error you can proccedd to the next step.")
end

nw.init(m.uci)

local net = nw:get_network(arg[1])

local function backup_ifnames(is_bridge)
	if not net:is_floating() and not m:get(net:name(), "_orig_ifname") then
		local ifcs = net:get_interfaces() or { net:get_interface() }
		if ifcs then
			local _, ifn
			local ifns = { }
			for _, ifn in ipairs(ifcs) do
				ifns[#ifns+1] = ifn:name()
			end
			if #ifns > 0 then
				m:set(net:name(), "_orig_ifname", table.concat(ifns, " "))
				m:set(net:name(), "_orig_bridge", tostring(net:is_bridge()))
			end
		end
	end
end


-- redirect to overview page if network does not exist anymore (e.g. after a revert)
if not net then
	luci.http.redirect(luci.dispatcher.build_url("admin/network/network"))
	return
end

-- protocol switch was requested, rebuild interface config and reload page
if m:formvalue("cbid.network.%s._switch" % net:name()) then
	-- get new protocol
	local ptype = m:formvalue("cbid.network.%s.proto" % net:name()) or "-"
	local proto = nw:get_protocol(ptype, net:name())
	if proto then
		-- backup default
		backup_ifnames()

		-- if current proto is not floating and target proto is not floating,
		-- then attempt to retain the ifnames
		--error(net:proto() .. " > " .. proto:proto())
		if not net:is_floating() and not proto:is_floating() then
			-- if old proto is a bridge and new proto not, then clip the
			-- interface list to the first ifname only
			if net:is_bridge() and proto:is_virtual() then
				local _, ifn
				local first = true
				for _, ifn in ipairs(net:get_interfaces() or { net:get_interface() }) do
					if first then
						first = false
					else
						net:del_interface(ifn)
					end
				end
				m:del(net:name(), "type")
			end

		-- if the current proto is floating, the target proto not floating,
		-- then attempt to restore ifnames from backup
		elseif net:is_floating() and not proto:is_floating() then
			-- if we have backup data, then re-add all orphaned interfaces
			-- from it and restore the bridge choice
			local br = (m:get(net:name(), "_orig_bridge") == "true")
			local ifn
			local ifns = { }
			for ifn in ut.imatch(m:get(net:name(), "_orig_ifname")) do
				ifn = nw:get_interface(ifn)
				if ifn and not ifn:get_network() then
					proto:add_interface(ifn)
					if not br then
						break
					end
				end
			end
			if br then
				m:set(net:name(), "type", "bridge")
			end

		-- in all other cases clear the ifnames
		else
			local _, ifc
			for _, ifc in ipairs(net:get_interfaces() or { net:get_interface() }) do
				net:del_interface(ifc)
			end
			m:del(net:name(), "type")
		end

		-- clear options
		local k, v
		for k, v in pairs(m:get(net:name())) do
			if k:sub(1,1) ~= "." and
			   k ~= "type" and
			   k ~= "ifname" and
			   k ~= "_orig_ifname" and
			   k ~= "_orig_bridge"
			then
				m:del(net:name(), k)
			end
		end

		-- set proto
		m:set(net:name(), "proto", proto:proto())
		m.uci:save("network")
		m.uci:save("wireless")

		-- reload page
		luci.http.redirect(luci.dispatcher.build_url("admin/wizard/step-lan"))
		return
	end
end

local ifc = net:get_interface()	

s = m:section(NamedSection, arg[1], "interface", translate("Common Configuration"))
s.addremove = false

--s:tab("general",  translate("General Setup"))
--s:tab("advanced", translate("Advanced Settings"))
--s:tab("physical", translate("Physical Settings"))

p = s:option(ListValue, "proto", translate("Protocol"))
p.default = net:proto()

p_switch = s:option(Button, "_switch")
p_switch.title      = translate("Really switch protocol?")
p_switch.inputtitle = translate("Switch protocol")
p_switch.inputstyle = "apply"

--[[local _, pr
for _, pr in ipairs(nw:get_protocols()) do
	p:value(pr:proto(), pr:get_i18n())
	if pr:proto() ~= net:proto() then
		p_switch:depends("proto", pr:proto())
	end
end]]

p:value("static", "Static address")
p:value("dhcp", "DCHP Client")
if "static" ~= net:proto() then
	p_switch:depends("proto", "static")
else
	p_switch:depends("proto", "dhcp")
end

--[[auto = s:taboption("advanced", Flag, "auto", translate("Bring up on boot"))
auto.default = (net:proto() == "none") and auto.disabled or auto.enabled]]

function p.write() end
function p.remove() end
function p.validate(self, value, section)
	if value == net:proto() then
		if not net:is_floating() and net:is_empty() then
			local ifn = ((br and (br:formvalue(section) == "bridge"))
				and ifname_multi:formvalue(section)
			     or ifname_single:formvalue(section))

			for ifn in ut.imatch(ifn) do
				return value
			end
			return nil, translate("The selected protocol needs a device assigned")
		end
	end
	return value
end

local thisIsAWizard = true

local form, ferr = loadfile(
	ut.libpath() .. "/model/cbi/admin_network/proto_%s.lua" % net:proto()
)

if not form then
	s:option(DummyValue, "_error",
		translate("Missing protocol extension for proto %q" % net:proto())
	).value = ferr
else
	setfenv(form, getfenv(1))(m, s, net, thisIsAWizard)
end


local _, field
for _, field in ipairs(s.children) do
	if field ~= st and field ~= p and field ~= p_install and field ~= p_switch then
		if next(field.deps) then
			local _, dep
			for _, dep in ipairs(field.deps) do
				dep.deps.proto = net:proto()
			end
		else
			field:depends("proto", net:proto())
		end
	end
end

if m:formvalue("cbi.wizard.next") then
	luci.http.redirect(luci.dispatcher.build_url("admin/system/wizard/step-wifi"))
end

local includeDHCP

if has_dnsmasq and net:proto() == "static" then
	includeDHCP = true
	m2 = Map("dhcp", "", "")

	local has_section = false

	m2.uci:foreach("dhcp", "dhcp", function(s)
		if s.interface == arg[1] then
			has_section = true
			return false
		end
	end)

	if not has_section then

		s = m2:section(TypedSection, "dhcp", translate("DHCP Server"))
		s.anonymous   = true
		s.cfgsections = function() return { "_enable" } end

		x = s:option(Button, "_enable")
		x.title      = translate("No DHCP Server configured for this interface")
		x.inputtitle = translate("Setup DHCP Server")
		x.inputstyle = "apply"

	else

		s = m2:section(TypedSection, "dhcp", translate("DHCP Server"))
		s.addremove = false
		s.anonymous = true
		--s:tab("general",  translate("General Setup"))
		--s:tab("advanced", translate("Advanced Settings"))

		function s.filter(self, section)
			return m2.uci:get("dhcp", section, "interface") == arg[1]
		end

		local ignore = s:option(Flag, "ignore",
			translate("Disable"),
			nil)--translate("Disable DHCP."))

		local start = s:option(Value, "start", translate("Start IP address"),
			nil)--translate("Lowest leased address as offset from the network address."))
		start.optional = true
		start.datatype = "or(uinteger,ip4addr)"
		start.default = "100"

		--[[local limit = s:option(Value, "limit", translate("Limit"),
			nil)--translate("Maximum number of leased addresses."))
		limit.optional = true
		limit.datatype = "uinteger"
		limit.default = "150"]]
		
		local endnum = s:option(Value, "endnum", translate("End IP address"))
		endnum.datatype = "ip4addr"
		endnum.default = "192.168.1.250"
		endnum.forcewrite = true
		
		function endnum.write(self, section, value)
			Value.write( self, section, value )
			--[[local isInteger = false
			local startNumAddr = start:formvalue(section)
			local endNum = value]]
			local limit
			
			local startIP = start:formvalue(section)
			local endIP = value
			
			local a1, a2, a3, a4 = startIP:match("^(%d+)%.(%d+)%.(%d+)%.(%d+)$")
			local b1, b2, b3, b4 = endIP:match("^(%d+)%.(%d+)%.(%d+)%.(%d+)$")
			limit = tonumber(b4) - tonumber(a4) + 1
			
			--[[if startNumAddr and startNumAddr ~= "" and endNum and endNum ~= "" then
				isInteger = is_integer(startNumAddr)
			end	
			
			if isInteger then
				limit = tonumber(endNum) - tonumber(startNumAddr) + 1
			else
				local b1, b2, b3, b4 = startNumAddr:match("^(%d+)%.(%d+)%.(%d+)%.(%d+)$")
				limit = tonumber(endNum) - b4 + 1
			end	]]
			
			x:set("dhcp", "lan", "limit", limit)
			x:save("dhcp")
			--x:commit("dhcp")
		end

		local ltime = s:option(Value, "leasetime", translate("Leasetime"),
			translate("Expiry time of leased addresses, minimum is 2 Minutes (<code>2m</code>)."))
		ltime.rmempty = true
		ltime.default = "12h"

		--[[local dd = s:taboption("advanced", Flag, "dynamicdhcp",
			translate("Dynamic DHCP"),
			nil)--translate("Dynamically allocate DHCP addresses for clients. If disabled, only " ..
-- 				"clients having static leases will be served."))
		dd.default = dd.enabled

		s:taboption("advanced", Flag, "force", translate("Force"),
			translate("Force DHCP on this network even if another server is detected."))

		-- XXX: is this actually useful?
		--s:taboption("advanced", Value, "name", translate("Name"),
		--	translate("Define a name for this network."))

		mask = s:taboption("advanced", Value, "netmask",
			translate("IPv4 netmask"),
			nil)--translate("Override the netmask sent to clients. Normally it is calculated " ..
-- 				"from the subnet that is served."))

		mask.optional = true
		mask.datatype = "ip4addr"

		s:taboption("advanced", DynamicList, "dhcp_option", translate("DHCP-Options"),
			translate("Define additional DHCP options, for example \"<code>6,192.168.2.1," ..
				"192.168.2.2</code>\" which advertises different DNS servers to clients."))]]

		for i, n in ipairs(s.children) do
			if n ~= ignore then
				n:depends("ignore", "")
			end
		end

	end
end

if includeDHCP then
	return m, m2
else
	return m
end