module("luci.controller.cfgwzd-module", package.seeall)
require("uci")

function index()
	local order, 		-- Used to indicate the step number
	      firstLogin, 	-- Self explanatory
	      is3G		-- Self explanatory
	
	order = 1
	
	-- Find out wheter this is the first login or not
	x = uci.cursor()
	firstLogin = x:get("teltonika", "sys", "first_login")
	-- Find out what kind of module we have here
	-- For the time being we will be finding yout wether we have a 3g module or not
	if x:get("system", "module", "type") == "3g" or x:get("system", "module", "type") == "3g_ppp" then
		is3G = true
	end
	
	entry({"admin", "system", "wizard"}, firstchild(), "Configuration Wizard", 1).dependent=false


	entry({"admin", "system", "wizard", "step-pwd"}, cbi("cfgwzd-module/step_pwd"), "Step "..order.." - Password", 10)
	order = order + 1
	if is3G then
		entry({"admin", "system", "wizard", "step-3g"}, cbi("cfgwzd-module/step_3g"), "Step "..order.." - 3G", 20)
		order = order + 1
	end
	entry({"admin", "system", "wizard", "step-lan"}, cbi("cfgwzd-module/step_lan"), "Step "..order.." - LAN", 30)
	order = order + 1
	entry({"admin", "system", "wizard", "step-wifi"}, cbi("cfgwzd-module/step_wifi"), "Step "..order.." - WiFi", 40)
	
		
end