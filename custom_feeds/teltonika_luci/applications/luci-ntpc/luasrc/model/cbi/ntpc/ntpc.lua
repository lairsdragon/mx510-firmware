--[[
LuCI - Lua Configuration Interface

Copyright 2008 Steven Barth <steven@midlink.org>
Copyright 2008 Jo-Philipp Wich <xm@leipzig.freifunk.net>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

$Id: ntpc.lua 6065 2010-04-14 11:36:13Z ben $
]]--
require("luci.sys")
require("luci.sys.zoneinfo")
require("luci.tools.webadmin")
require("luci.fs")
require("luci.config")

local port

local function cecho(string)
	luci.sys.call("echo \"" .. string .. "\" >> /tmp/log.log")
end

m = Map("ntpclient", translate("Time Synchronisation"), translate("Synchronizes the system time"))

s = m:section(TypedSection, "ntpclient", translate("General"))
s.anonymous = true
s.addremove = false

s:option(DummyValue, "_time", translate("Current system time")).value = os.date("%c")

local tzone = s:option(ListValue, "zoneName", translate("Time zone"))
tzone:value("UTC")
for i, zone in ipairs(luci.sys.zoneinfo.TZ) do
	tzone:value(zone[1])
end

function tzone.write(self, section, value)
	local cfgName
	local cfgTimezone
	
	Value.write(self, section, value)
	
	local function lookup_zone(title)
		for _, zone in ipairs(luci.sys.zoneinfo.TZ) do
			if zone[1] == title then return zone[2] end
		end
	end
	
	m.uci:foreach("system", "system", function(s)
		cfgName = s[".name"]
		cfgTimezone = s.timezone
	end)
	
	
	local timezone = lookup_zone(value) or "GMT0"
	m.uci:set("system", cfgName, "timezone", timezone)
	m.uci:save("system")
	m.uci:commit("system")
	luci.fs.writefile("/etc/TZ", timezone .. "\n")
end

s:option(Flag, "enabled", translate("Run NTP client on startup"))

s:option(Value, "interval", translate("Update interval (in seconds)")).rmempty = true
s:option(Value, "count", translate("Count of time measurements"), translate("empty = infinite")).rmempty = true


s2 = m:section(TypedSection, "ntpdrift", translate("Clock Adjustment"))
s2.anonymous = true
s2.addremove = false
s2:option(Value, "freq", translate("Offset frequency")).rmempty = true

s3 = m:section(TypedSection, "ntpserver", translate("Time Servers"))
s3.anonymous = true
s3.addremove = true
s3.template = "cbi/tblsection"

s3:option(Value, "hostname", translate("Hostname"))

port = s3:option(Value, "port", translate("Port"))
port.rmempty = true
port.default = "123"
return m
