-- gre tunnel
local m, s, e, v, o, t, k, l, n

m = Map("gre_tunnel", translate("GRE Tunnel"), translate("GRE tunneling over TCP/IP network settings"))
s = m:section(NamedSection, "gre_tunnel")
s.addremove = false

-- enable gre tunnel
e = s:option(Flag, "enable", translate("Enable GRE Tunnel"))
e.rmempty = false

-- ttl
v = s:option(Value, "ttl", translate("TTL"),translate("Value [0-255]"))
v.datatype ="range(0,255)"

-- pmtud
o = s:option(Flag, "pmtud", translate("PMTUD"))

-- remote tunnel IP
t = s:option(Value, "remote_ip", translate("Remote network IP"))
t.datatype = "ipaddr"
-- function t:validate(Value)
-- 	local failure
-- 	if not Value:match("[0-9]+\.[0-9]+\.[0-9]+\.0") then
-- 		m.message = translate("err: Remote tunnel network address is incorrect! (Addres must be in x.x.x.0 notation)")
-- 		failure = true
-- 	end
-- 	if not failure then
-- 		return Value
-- 	end	
-- 	return nil
-- end

-- cidr
l = s:option(Value, "rcidr", translate("Remote network CIDR"),translate("CIDR (netmask) value [0-32]"))
l.datatype ="range(0,32)"

t = s:option(Value, "tun_ip", translate("Local tunnel IP"))
t.datatype = "ipaddr"

l = s:option(Value, "tun_cidr", translate("Local tunnel CIDR"),translate("CIDR (netmask) value [0-32]"))
l.datatype ="range(0,32)"

-- remote network IP
k = s:option(Value, "remote_net", translate("Remote endpoint IP address"))
k.datatype = "ip4addr"


-- mtu
n = s:option(Value, "mtu", translate("MTU"),translate("MTU value [0-1500]"))
n.datatype ="range(0,1500)"

return m
