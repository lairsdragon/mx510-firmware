--[[
LuCI - Lua Configuration Interface

Copyright 2008 Steven Barth <steven@midlink.org>
Copyright 2008 Jo-Philipp Wich <xm@leipzig.freifunk.net>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

$Id: coovachilli.lua 3442 2008-09-25 10:12:21Z jow $
]]--

local function cecho(string)
	luci.sys.call("echo \"" .. string .. "\" >> /tmp/log.log")
end

local 	m, 		-- General name for a map
		s, 		-- General name for a section
		o, 		-- General name for an option
		
		scc, 	-- Coovachilli configuration section
		cen, 	-- Coovachilli enable flag;
		net, 	-- Network option; The IP address of the router on the hotspot client network: [xxx.xxx.xxx.xxx/cindr]
		ral, 	-- Radius Listen option; The IP address of the radius server [xxx.xxx.xxx.xxx]
		rs1, 	-- Radius server #1 option;
		rs2, 	-- Radius server #2 option;
		rap, 	-- Radius authentication port option;
		hnm, 	-- Hostspot name option;
		rcp, 	-- Radius accounting port option;
		ras, 	-- Radius secret key option;
		ual, 	-- UAM allowed Dynamic list;
		
		sle, 	-- Logging settings section;
		len, 	-- Enable Logging flag;
		
		sft, 	-- FTP options section;
		fen, 	-- Upload via FTP enable flag;
		fhs, 	-- FTP host option;
		fus, 	-- FTP Username;
		fpw, 	-- FTP password option;
		fpt, 	-- FTP port option;
		fhr, 	-- FTP hour option;
		fmn, 	-- FTP min option;
		fwd, 	-- FTP Day option;
		
		snt, 	-- Time Section;
		tnm, 	-- Name of the interval;
		tns, 	-- Fixed time / interval switch;
		thr, 	-- Time hour;
		tmn, 	-- Time minutes;
		twd, 	-- Time weekday;
		tnt 	-- Time interval;
		
local nw = require "luci.model.network"

m = Map( "coovachilli", 
	translate( "Wireless hotspot configuration" ), 
	translate( "Here you can configure all the essential settings needed for a wireless hotspot." ) )

m:chain("wireless")

nw.init(m.uci)

-----------------------------------------------------------------------

scc = m:section( NamedSection, "general", "general", "General Settings" )

cen = scc:option( Flag, "enabled", "Enabled" )
cen.rmempty = false
cen.forcewrite = true
function cen.write(self, section, value)
	local currValue = self.map:get(section, self.option, value)
	if value and currValue and value ~= currValue then
		local apWnet = nw:get_wifinet("radio0.network1") -- AP mode wifi net
		luci.sys.call("env -i /sbin/wifi down >/dev/null 2>/dev/null")
		if value == self.enabled then
			apWnet:set("network", nil)
		else
			apWnet:set("network", "lan")
		end
		nw:commit("wireless")
		luci.sys.call("env -i /sbin/wifi up >/dev/null 2>/dev/null")
	end
	return self.map:set(section, self.option, value)
end

net = scc:option( Value, "net", "AP IP", "The IP address of the router on the hotspot network." )

--ral = scc:option( Value, "radiuslisten", "Radius listen" )

rs1 = scc:option( Value, "radiusserver1", "Radius server #1" )

rs2 = scc:option( Value, "radiusserver2", "Radius server #2" )

rap = scc:option( Value, "radiusauthport", "Authentication port" )
rap.datatype = "port"

ras = scc:option( Value, "radiusacctport", "Accounting port" )
ras.datatype = "port"

hnm = scc:option( Value, "hotspotname", "Hotspot name" )

rcp = scc:option( Value, "radiussecret", "Secret key" )
rcp.password = true

--ual = scc:option( DynamicList, "uamallowed", "Allowed hosts" )

-----------------------------------------------------------------------

sle = m:section( NamedSection, "logging", "logging", "Logging Settings" )

len = sle:option( Flag, "enabled", "Enabled" )

-----------------------------------------------------------------------

sft = m:section( NamedSection, "ftp", "ftp", "FTP upload settings" )

fen = sft:option( Flag, "enabled", "Enabled" )

fhs = sft:option( Value, "host", "Server address" )

fus = sft:option( Value, "user", "Username" )

fpw = sft:option( Value, "psw", "Password" )
fpw.password = true

fpt = sft:option( Value, "port", "Port" )
fpt.datatype = "port"

-----------------------------------------------------------------------

snt = m:section( TypedSection, "interval", "FTP upload intervals", "You can configure your timing settings for the log upload via FTP feature here." )
snt.addremove = false
snt.anonymous = true

tnm = snt:option( Value, "descr", "Description" )

tns = snt:option( ListValue, "fixed", "Mode" )
tns:value( "1", "Fixed" )
tns:value( "0", "Interval" )

thr = snt:option( Value, "fixed_hour", "Hour" )
thr.datatype = "range(0,23)"
thr:depends( "fixed", "1" )

tmn = snt:option( Value, "fixed_minute", "Minute" )
tmn.datatype = "range(0,59)"
tmn:depends( "fixed", "1" )

twd = snt:option( Value, "weekdays", "Weekdays", "Enter 3 letter weekday keys separated by commas. E.g. Monday, Tuesday and Friday would be \"mod,tue,fri\"" )
-- cecho("parsing...")
-- function twd.validate(self, value)
-- 	cecho("validating...")
-- 	if self.datatype and value then
-- 		if type(value) == "table" then
-- 			local v
-- 			for _, v in ipairs(value) do
-- 				if v and #v > 0 and not verify_datatype(self.datatype, v) then
-- 					return nil
-- 				end
-- 			end
-- 		else
-- 			if not verify_datatype(self.datatype, value) then
-- 				return nil
-- 			end
-- 		end
-- 	end
-- 	
-- 	function split(str, c)
-- 		cecho(" ")
-- 		cecho(" ")
-- 		cecho(" ")
-- 		cecho("**entry")
-- 		local a = string.find(str, c)
-- 		cecho("a: [" .. (a or "null") .. "]")
-- 		local str = string.gsub(str, c, "", 1)
-- 		cecho("str: [" .. (str or "null") .. "]")
-- 		local aCount = 0
-- 		local start = 1
-- 		local array = {}
-- 		local last = 0
-- 		
-- 		while a do
-- 			array[aCount] = string.sub(str, start, a - 1)
-- 			cecho("array[aCount]: [" .. (array[aCount] or "null") .. "]")
-- 			start = a
-- 			cecho("start: [" .. (start or "null") .. "]")
-- 			a = string.find(str, c)
-- 			cecho("a: [" .. (a or "null") .. "]")
-- 			str = string.gsub(str, c, "", 1)
-- 			cecho("str: [" .. (str or "null") .. "]")
-- 			aCount = aCount + 1
-- 			cecho("aCount: [" .. (aCount or "null") .. "]")
-- 		end
-- 		cecho("**end...")
-- 		return array
-- 	end
-- 	
-- 	local das = {}
-- 	das = split(",mon,wen,tue,", ",")
-- 	
-- 	for i,v in ipairs(das) do 
-- 		cecho("i: [" .. i .. "], k: [" .. v .. "]")
-- 	end
-- 	
-- 	return value
-- end

tnt = snt:option( ListValue, "interval_time", "Upload interval" )
tnt:value("1", "1 Hour")
tnt:value("2", "2 Hours")
tnt:value("4", "4 Hours")
tnt:value("8", "8 Hours")
tnt:value("12", "12 Hours")
tnt:value("24", "24 Hours")
tnt:depends( "fixed", "0" )

-----------------------------------------------------------------------

function m.on_parse(self)
	-- We will ettempt to push multiwan to the very end of the parse chain, hopefully making it run last in the init script sequence, hence fixing the problem that has been plagueing me for fucking ever
	--luci.sys.call("echo \"on_parse called\" >> /tmp/log.log")
	self.parsechain[1] = "wireless"
	self.parsechain[2] = "coovachilli"
-- 	for k, v in pairs(self.parsechain) do
-- 		luci.sys.call("echo \"k: [" .. k .. "], v: [" .. v .. "]\" >> /tmp/log.log")
-- 	end
end

return m
