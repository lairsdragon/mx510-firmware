local fs = require "nixio.fs"
local uci  = require "luci.model.uci".cursor()
local fw = require "luci.model.firewall"

--m = Map("system", translate("Administration properties"),
	--translate("Changes the administration password, log level and provides SSH access control."))
m = Map("system")

s2 = m:section(TypedSection, "system", translate("Logging"))
s2.addremove = false

con = s2:option(ListValue, "conloglevel", translate("System log level"))
con:value(8, translate("Debug"))
con:value(7, translate("Info"))
con:value(6, translate("Notice"))
con:value(5, translate("Warning"))
con:value(4, translate("Error"))
con:value(3, translate("Critical"))
con:value(2, translate("Alert"))
con:value(1, translate("Emergency"))

function con.write(self, section, value)
	Value.write(self, section, value)
	m.message = translate("wrn:Router reboot is required for changes to take effect.")
end

conLog = s2:option(Button, "_log")
conLog.title      = translate("System Log")
conLog.inputtitle = translate("Show")
conLog.inputstyle = "apply"

conLog = s2:option(Button, "_kerlog")
conLog.title      = translate("Kernel Log")
conLog.inputtitle = translate("Show")
conLog.inputstyle = "apply"

if m:formvalue("cbid.system.cfg02e48a._log") then
	luci.http.redirect(luci.dispatcher.build_url("admin/status/syslog"))
end

if m:formvalue("cbid.system.cfg02e48a._kerlog") then
	luci.http.redirect(luci.dispatcher.build_url("admin/status/dmesg"))
end


fw.init(uci)

-- cron = s2:option(ListValue, "cronloglevel", translate("Cron Log Level"))
-- cron.default = 8
-- cron:value(5, translate("Debug"))
-- cron:value(8, translate("Normal"))
-- cron:value(9, translate("Warning"))

return m