module("luci.controller.firewall", package.seeall)

function index()
	
	entry({"admin", "network", "port-fwd"}, cbi("firewall/port-fwd"), _("Port Forwarding"), 50)

	--arcombine(cbi("firewall/port-fwd"), cbi("firewall/fwd-details"))
	entry({"admin", "network", "firewall"},
		alias("admin", "network", "firewall", "zones"),
		_("Firewall"), 51).i18n = "firewall"
		
	--entry({"admin", "network", "port-forward"}, ),_("Port forwarding"), 51)	
	
	entry( {"admin", "network", "fwd-details"}, cbi("firewall/fwd-details"), nil).leaf = true
	
	entry({"admin", "network", "firewall", "zones"},
		arcombine(cbi("firewall/zones"), cbi("firewall/zone-details")),
		_("General Settings"), 10).leaf = true
		
		--[[
	entry({"admin", "network", "firewall", "forwards"},
		arcombine(cbi("firewall/forwards"), cbi("firewall/forward-details")),
		nil, 20).leaf = true]]

	entry({"admin", "network", "firewall", "rules"},
		arcombine(cbi("firewall/rules"), cbi("firewall/rule-details")),
		_("Traffic Rules"), 30).leaf = true

	entry({"admin", "network", "firewall", "custom"},
		cbi("firewall/custom"),
		_("Custom Rules"), 40).leaf = true
end
