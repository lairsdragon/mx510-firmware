--[[
LuCI - Lua Configuration Interface

Copyright 2008 Steven Barth <steven@midlink.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

$Id: zones.lua 8108 2011-12-19 21:16:31Z jow $
]]--

local ds = require "luci.dispatcher"
local fw = require "luci.model.firewall"

local m, s, o, p, i, v, z, mdex

m = Map("firewall",
	translate("Firewall"),
	translate("General settings allows you to set up default firewall policy."))

fw.init(m.uci)

s = m:section(TypedSection, "defaults", translate("General Settings"))
s.anonymous = true
s.addremove = false


mdex = s:option(Flag,"mdex_masq", translate("Masquerade LAN interface"))
mdex.enabled = "1"
mdex.disabled = "0"
mdex.default = mdex.enabled

s:option(Flag, "syn_flood", translate("Enable SYN-flood protection"))

o = s:option(Flag, "drop_invalid", translate("Drop invalid packets"))
o.default = o.disabled

p = {
	s:option(ListValue, "input", translate("Input")),
	s:option(ListValue, "output", translate("Output")),
	s:option(ListValue, "forward", translate("Forward"))
}

for i, v in ipairs(p) do
	v:value("REJECT", translate("reject"))
	v:value("DROP", translate("drop"))
	v:value("ACCEPT", translate("accept"))
end
-- DMZ disabled MDEX
--s = m:section(NamedSection, "DMZ", "dmz", "DMZ configuration")

--dmz_en = s:option(Flag, "enabled", "Enabled")
--dmz_en.rmempty = false

--[[function dzm_en.cfgvalue(self, section)
	local rtnVal
	if self.map:get(section, "enabled") == "0" then
		rtnVal = "0"
	else
		rtnVal = "1"
	end
	return rtnVal 
end]]

--[[function dzm_en.write(self, section, value)
	if value ~= "0" then
		value = nil
	end
	return Flag.write(self, section, value)
end]]

--z = s:option(ListValue, "src", translate("Source zone"))
--z:value("wan", translate("WAN"))
--z:value("vpn", translate("VPN"))

--o = s:option(Value, "dest_ip", "DMZ host IP address")
--o.datatype = "ip4addr"

--[[
s = m:section(TypedSection, "zone", translate("Zones"))
s.template = "cbi/tblsection"
s.anonymous = true
s.addremove = false
s.extedit   = false

function s.create(self)
	local z = fw:new_zone()
	if z then
		luci.http.redirect(
			ds.build_url("admin", "network", "firewall", "zones", z.sid)
		)
	end
end

function s.remove(self, section)
	return fw:del_zone(section)
end

o = s:option(DummyValue, "_info", translate("Zone ⇒ Forwardings"))
o.template = "cbi/firewall_zoneforwards"
o.cfgvalue = function(self, section)
	return self.map:get(section, "name")
end

p = {
	s:option(ListValue, "input", translate("Input")),
	s:option(ListValue, "output", translate("Output")),
	s:option(ListValue, "forward", translate("Forward"))
}

for i, v in ipairs(p) do
	v:value("REJECT", translate("reject"))
	v:value("DROP", translate("drop"))
	v:value("ACCEPT", translate("accept"))
end

s:option(Flag, "masq", translate("Masquerading"))
s:option(Flag, "mtu_fix", translate("MSS clamping"))]]

return m
