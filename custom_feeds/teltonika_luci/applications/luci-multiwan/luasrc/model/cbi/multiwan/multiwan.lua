
-- FIXME: This shit is rapidly descending into 104* territory, everything, including network.lua (the one that's +30Kb) have to
-- be reviewed to allow for better multi module support!!!!!

require("luci.tools.webadmin")

local nw = require "luci.model.network"
local fw = require "luci.model.firewall"

m = Map("multiwan", translate("Backup Link"),
	translate("Here you can setup your backup link. If your conventional WAN connection, such as wired Ethernet or Wifi, fails, the backup link will enable and take over to keep the router connected."))
m:chain("network")
m:chain("firewall")

nw.init(m.uci)
fw.init(m.uci)

moduleStruct = nw:getModule()

local g3_wan_ifname = "usb0" 

local wanifname = nw:get_wan_ifname()

local module = nw:get_module()
local moduleIfname = nw:get_module_fname()

if module == "3g_ppp" then
	g3_wan_ifname = "3g-ppp"
end
s = m:section(NamedSection, "config", "multiwan")

if wanifname == g3_wan_ifname or wanifname == "eth1" or wanifname == "wm0" then
	if wanifname == g3_wan_ifname then
		e = s:option(Flag, "enabled", translate("Enable"), translate("3G selected as wan - cannot enable backup link."))
	else
		e = s:option(Flag, "enabled", translate("Enable"), translate("WiMAX selected as wan - cannot enable backup link."))
	end
	e.hardDisabled = true
else
	e = s:option(Flag, "enabled", translate("Enable"))
end


e.rmempty = false
e.default = e.enabled

function e.write(self, section, value)
	local wanifname = nw:get_wan_ifname()
	if wanifname == g3_wan_ifname or wanifname == "eth1" or wanifname == "wm0" then
		--Flag.write(self, section, "0")
		if wanifname == g3_wan_ifname then
			m.message = translate("wrn:3G selected as wan - cannot enable backup link.")
		else
			m.message = translate("wrn:WiMAX selected as wan - cannot enable backup link.")
		end
		luci.http.redirect(luci.dispatcher.build_url("admin/network/multiwan"))
		return
	else
		if value == "0" then
			if module == "3g_ppp" then
				m.uci:set("network", "ppp", "enabled", "0")
				m.uci:save("network")
				m.uci:commit("network")
			end
			nw:del_network("wan2")
			os.execute("/etc/init.d/multiwan stop")
		else
			if module == "3g_ppp" then
				m.uci:set("network", "ppp", "enabled", "1")
				m.uci:save("network")
				m.uci:commit("network")
			end
			local opts = {
				proto	= "dhcp",
				ifname	= g3_wan_ifname
			}
			if module == "wimax" then
				opts.ifname = moduleStruct:get_iface()
			end
			nw:add_network("wan2", opts)
			local zn = fw:get_zone("wan")
			zn:add_network("wan2")
			--os.execute("/etc/init.d/multiwan enable")
		end
		nw:save("network")
		nw:commit("network")
		Flag.write(self, section, value)
	end
end

s = m:section(NamedSection, "wan", "interface", translate("Timing & other parameters"),
	translate("Timing & other parameters will indicate how and when it will be determined that your conventional connection has gone down."))
s.addremove = false

--[[weight = s:option(ListValue, "weight", translate("Load Balancer Distribution"))
weight:value("10", "10")
weight:value("9", "9")
weight:value("8", "8")
weight:value("7", "7")
weight:value("6", "6")
weight:value("5", "5")
weight:value("4", "4")
weight:value("3", "3")
weight:value("2", "2")
weight:value("1", "1")
weight:value("disable", translate("None"))
weight.default = "10"
weight.optional = false
weight.rmempty = false]]

interval = s:option(ListValue, "health_interval", translate("Health Monitor Interval"))
interval:value("disable", translate("Disable"))
interval:value("5", "5 sec.")
interval:value("10", "10 sec.")
interval:value("20", "20 sec.")
interval:value("30", "30 sec.")
interval:value("60", "60 sec.")
interval:value("120", "120 sec.")
interval.default = "10"
interval.optional = false
interval.rmempty = false

icmp_hosts = s:option(Value, "icmp_hosts", translate("Health Monitor ICMP Host(s)"))
icmp_hosts:value("disable", translate("Disable"))
icmp_hosts:value("dns", "DNS Server(s)")
icmp_hosts:value("gateway", "WAN Gateway")
icmp_hosts.default = "dns"
icmp_hosts.optional = false
icmp_hosts.rmempty = false

timeout = s:option(ListValue, "timeout", translate("Health Monitor ICMP Timeout"))
timeout:value("1", "1 sec.")
timeout:value("2", "2 sec.")
timeout:value("3", "3 sec.")
timeout:value("4", "4 sec.")
timeout:value("5", "5 sec.")
timeout:value("10", "10 sec.")
timeout.default = "3"
timeout.optional = false
timeout.rmempty = false

fail = s:option(ListValue, "health_fail_retries", translate("Attempts Before WAN Failover"))
fail:value("1", "1")
fail:value("3", "3")
fail:value("5", "5")
fail:value("10", "10")
fail:value("15", "15")
fail:value("20", "20")
fail.default = "3"
fail.optional = false
fail.rmempty = false

recovery = s:option(ListValue, "health_recovery_retries", translate("Attempts Before WAN Recovery"))
recovery:value("1", "1")
recovery:value("3", "3")
recovery:value("5", "5")
recovery:value("10", "10")
recovery:value("15", "15")
recovery:value("20", "20")
recovery.default = "5"
recovery.optional = false
recovery.rmempty = false

--[[failover_to = s:option(ListValue, "failover_to", translate("Failover Traffic Destination"))
failover_to:value("disable", translate("None"))
luci.tools.webadmin.cbi_add_networks(failover_to)
failover_to:value("fastbalancer", translate("Load Balancer(Performance)"))
failover_to:value("balancer", translate("Load Balancer(Compatibility)"))
failover_to.default = "balancer"
failover_to.optional = false
failover_to.rmempty = false]]

dns = s:option(Value, "dns", translate("DNS Server(s)"))
dns:value("auto", translate("Auto"))
dns.default = "auto"
dns.optional = false
dns.rmempty = true

s = m:section(NamedSection, "wan2", "interface", translate("Backup ICMP host"),
	translate("A remote host that will be used to test wether your backup link is alive."))
s.addremove = false

o = s:option(Value, "icmp_hosts", translate("ICMP host"))
o.optional = false
o.datatype = "ip4addr"

--[[s = m:section(TypedSection, "mwanfw", translate("Multi-WAN Traffic Rules"),
	translate("Configure rules for directing outbound traffic through specified WAN Uplinks."))
s.template = "cbi/tblsection"
s.anonymous = true
s.addremove = true

src = s:option(Value, "src", translate("Source Address"))
src.rmempty = true
src:value("", translate("all"))
luci.tools.webadmin.cbi_add_knownips(src)

dst = s:option(Value, "dst", translate("Destination Address"))
dst.rmempty = true
dst:value("", translate("all"))
luci.tools.webadmin.cbi_add_knownips(dst)

proto = s:option(Value, "proto", translate("Protocol"))
proto:value("", translate("all"))
proto:value("tcp", "TCP")
proto:value("udp", "UDP")
proto:value("icmp", "ICMP")
proto.rmempty = true

ports = s:option(Value, "ports", translate("Ports"))
ports.rmempty = true
ports:value("", translate("all", translate("all")))

wanrule = s:option(ListValue, "wanrule", translate("WAN Uplink"))
luci.tools.webadmin.cbi_add_networks(wanrule)
wanrule:value("fastbalancer", translate("Load Balancer(Performance)"))
wanrule:value("balancer", translate("Load Balancer(Compatibility)"))
wanrule.default = "fastbalancer"
wanrule.optional = false
wanrule.rmempty = false

s = m:section(NamedSection, "config", "", "")
s.addremove = false

default_route = s:option(ListValue, "default_route", translate("Default Route"))
luci.tools.webadmin.cbi_add_networks(default_route)
default_route:value("fastbalancer", translate("Load Balancer(Performance)"))
default_route:value("balancer", translate("Load Balancer(Compatibility)"))
default_route.default = "balancer"
default_route.optional = false
default_route.rmempty = false]]

function m.on_parse(self)
	-- We will ettempt to push multiwan to the very end of the parse chain, hopefully making it run last in the init script sequence, hence fixing the problem that has been plagueing me for fucking ever
	--luci.sys.call("echo \"on_parse called\" >> /tmp/log.log")
	self.parsechain[1] = "network"
	self.parsechain[2] = "firewall"
	self.parsechain[3] = "multiwan"
	--[[for k, v in pairs(self.parsechain) do
		luci.sys.call("echo \"k: [" .. k .. "], v: [" .. v .. "]\" >> /tmp/log.log")
	end]]
end

return m
