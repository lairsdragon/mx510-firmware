module("luci.controller.ping-reboot", package.seeall)

function index()
    entry({"admin", "services", "ping-reboot"}, cbi("ping-reboot/ping-reboot"), _("Ping / Periodic Reboot"), 10)
end

