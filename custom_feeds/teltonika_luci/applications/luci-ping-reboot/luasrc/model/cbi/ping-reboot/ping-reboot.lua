-- PING Reboot
local m, s, e, v, t, k, l

m = Map("ping_reboot", translate("Ping Reboot"))
s = m:section(NamedSection, "ping_reboot")
s.addremove = false

-- enable ping reboot option
e = s:option(Flag, "enable", translate("Enable Ping Reboot"))
e.rmempty = false

-- enable router reboot
v = s:option(Flag, "reboot", translate("Reboot router if no echo received"))
v.default = v.enabled
v.rmempty = false

-- ping inverval column and number validation
t = s:option(ListValue, "time", translate("Interval between pings (min)"), translate("Minimum 5 minutes"))
t:value("5", translate("5 min"))
t:value("15", translate("15 min"))
t:value("30", translate("30 min"))
t:value("60", translate("1 hour"))
t:value("120", translate("2 hours"))

-- number of retries and number validation
k = s:option(Value, "retry", translate("Retry count"))
k.default = "5"
k.datatype = "range(1,9999)"

-- host to ping
l = s:option(Value, "host", translate("Server to ping"), translate("e.g. 192.168.1.1 (or www.host.com if DNS server configured correctly)"))
l.default = "127.0.0.1"

f = Map("perio_reboot", translate("Periodic Reboot"), " ")
s = f:section(NamedSection, "perio_reboot")
s.addremove = false

-- enable periodic reboot option
e = s:option(Flag, "enable", translate("Enable Periodic Reboot"))
e.rmempty = false

-- period interval h
t = s:option(Value, "hours", translate("Hour"))
t.default = "24"
t.datatype = "range(0,23)"

-- period interval min
t = s:option(Value, "minutes", translate("Minute"), translate(""))
t.default = "0"
t.datatype = "range(0,59)"

return m, f
