--[[
LuCI - Lua Configuration Interface

Copyright 2008 Steven Barth <steven@midlink.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

$Id: openvpn.lua 5448 2009-10-31 15:54:11Z jow $
]]--

local fs  = require "nixio.fs"
local sys = require "luci.sys"
local uci = require "luci.model.uci".cursor()
-- local mix = require "luci.mtask"
local CFG_MAP = "openvpn"
local CFG_SEC = "openvpn"

local m ,s, o

local function cecho(string)
	luci.sys.call("echo \"" .. string .. "\" >> /tmp/log.log")
end

m = Map("openvpn", translate("OpenVPN"))
m.spec_dir = nil
m.pageaction = false

s = m:section( TypedSection, "openvpn", translate("OpenVPN instances"), translate("Below is a list of configured OpenVPN instances and their current state") )
s.addremove = true
s.anonymous = true
s.template = "cbi/tblsection"
s.template_addremove = "openvpn/vpn_add_rem"
--s.addremoveAdd = true
--[[uci:foreach(CFG_MAP, CFG_SEC, function(sec)
	-- Entry signifies that there already is a section, therefore we will disable the ability to 
	-- add or remove another section
	s.addremoveAdd = false
end)]]
s.extedit = luci.dispatcher.build_url("admin", "vpn-menu", "openvpn-tlt", "cbasic", "%s")

local name = s:option( DummyValue, "_name", translate("Tunnel Name") )

function name.cfgvalue(self, section)
	return section or "Unknown"
end

local dev = s:option( DummyValue, "dev", translate("Tun/Tap") )

function dev.cfgvalue(self, section)
	local val = AbstractValue.cfgvalue(self, section)
	return val and string.upper(val) or "-"
end


local proto = s:option( DummyValue, "proto", translate("Protocol") )

function proto.cfgvalue(self, section)
	local val = AbstractValue.cfgvalue(self, section)
	return val and string.upper(val) or "-"
end

local port = s:option( DummyValue, "port", translate("Port") )

function port.cfgvalue(self, section)
	local val = AbstractValue.cfgvalue(self, section)
	return val or "-"
end

local status = s:option( DummyValue, "_status", translate("Status") )

function status.cfgvalue(self, section)
	local pid = fs.readfile("/var/run/openvpn-%s.pid" % section)
	local vpn_enabled
	
	uci:foreach("openvpn", "openvpn",
		function (s)
			vpn_enabled = s.enable
		end)
	
	if vpn_enabled == '1' then
		if pid and #pid > 0 and tonumber(pid) ~= nil then
			return (sys.process.signal(pid, 0))
				and translate("Enabled, running")
				or  translate("Enabled, not running")
		end
		return translate("Enabled not running")
	else
		if pid and #pid > 0 and tonumber(pid) ~= nil then
			return (sys.process.signal(pid, 0))
				and translate("Dissabled")
				or  translate("Disabled")
		end
		return translate("Disabled")
	end
end


function s.parse(self, section)
-- 	mix.echo("OpenVPN sekcijos parsinimas")
	local cfgname = luci.http.formvalue("cbid." .. self.config .. "." .. self.sectiontype .. ".name")
	
	-- 'Delete' button does not commit uci changes. So we will do it manually. And here another problem 
	-- occurs: 'Delete' button has very long name including vpn instance name and I don't know that 
	-- instance name. So I will scan through uci config and try to find out if such instance name exists
	-- as form element. FIXME investigate if another more inteligent approach is available here (O_o)
	local delButtonFormString = "cbi.rts." .. self.config .. "."
	local delButtonPress
	local configName
	local uFound
	
	uci:foreach("openvpn", "openvpn", function(x)
		configName = x[".name"] or ""
		delButtonFormString = delButtonFormString .. configName
		if luci.http.formvalue(delButtonFormString) then
			delButtonPress = true
		end
		uFound = true
	end)
	
	if delButtonPress then
		uci.delete("openvpn", configName)
		uci.save("openvpn")
		uci.commit("openvpn")
		luci.sys.call("/etc/init.d/openvpn restart >/dev/null")
		-- delete buttons is pressed, don't execute function 'openvpn_new'
		cfgname = false
	end	
	
	if cfgname then
		if not uFound then
			openvpn_new(self, cfgname)
		else
			m.message = translate("Only one VPN instance is allowed.")
			return
		end	
	end
	TypedSection.parse( self, section )
end


function openvpn_new(self,name)
	local t = {}
	local role = luci.http.formvalue("cbid." .. self.config .. "." .. self.sectiontype .. ".role")
	
	if name and #name > 0 and role then
		if not (string.find(name, "[%c?%p?%s?]+") == nil) then
			m.message = translate("err: Only alphanumeric characters allowed.")
		else
			name = (role.."_"..name)
			
			-- check existing vpn interfaces
			--[[local ifaceNum = 2
			local ifaceName = "tun"
			local ifaceVpn
			local Max = 0
			local cfgNumber
			local cfgName
			local foundFlag = false
			
			uci:foreach("openvpn", "openvpn", function(s) 
				cfgName = s[".name"]
				cfgNumber = s.dev_position
				if  tonumber(cfgNumber) > Max then
					Max = tonumber(cfgNumber)
				end	
				foundFlag = true
			end)
			
			if foundFlag then
				ifaceNum = Max + 1
			end]]
			
			ifaceVpn = "tun0"
			
			if role == "mdex" then
				t["dev"]          = ifaceVpn
				t["tls_client"]   = "1"
				t["comp_lzo"]     = "1"
				t["ns_cert_type"] = "server"
				t["cipher"]       = "BF-CBC"
				t["tun_mtu"]      = "1500"
				t["fragment"]     = "1300"
				t["mssfix"]       = "1"
				t["float"]        = "1"
				t["nobind"]       = "1"
				t["reneg_sec"]    = "86400"
				t["auth_user_pass"] = "/etc/openvpn/" .. name .."_login"
				t["pull"]         = "1"
				t["verb"]         = "4"
				t["remote"]       = "fixedip.mdex.de"
				t["persist-key"]  = "1"
				t["persist-tun"]  = "1"
				t["dis_nat_vpn"]  = "1"
				t["explicit_exit_notify_mdex"] = "2"
			else
				t["dev"]          = ifaceVpn
				t["persist_key"] = "1"
				t["persist_tun"] = "1"
			end
			t["verb"] = "5"
			if role == "mdex" then
			--	t["port"] = "443"
			else
				t["port"] = "1194"
			end
			if role == "server" then
				t["ifconfig_pool_persist"] = "/tmp/ipp.txt"
				t["status"] = "/tmp/openvpn-status.log"
				t["keepallive"] = "10 120"
			end
			uci:section("openvpn", "openvpn", name,t)
			uci:save("openvpn")
			m.message = translate("scs: New OpenVPN instance created successfully, configure it")
		end
	else
		m.message = translate("err: To create new OpenVPN instance, name should be specified!")
	end
	
end

return m
