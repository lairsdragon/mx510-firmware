local sys = require "luci.sys"
local uci = require "luci.model.uci".cursor()
--local mix = require("luci.mtask")

local m, s, s2, s3, s4, o

local m = Map("racoon", translate("IPsec"), translate("Basic ipsec configuration"))
m:chain("firewall")

s = m:section( NamedSection, "ipsec1", "tunnel", translate("Description") )

ipsec_on = s:option( Flag, "enabled", translate("Enable IPsec") )

function ipsec_on.on_enable(self,section)
	sys.call("/etc/init.d/racoon enable")
end

function ipsec_on.on_disable(self,section)
	sys.call("/etc/init.d/racoon stop")
	sys.call("/etc/init.d/racoon disable")
end


local cfgmode  = s:option( ListValue, "cfgmode", translate("IPSec key exchange mode") )
cfgmode.nowrite = true
cfgmode.default = "ike"
cfgmode:value("ike", "Auto Key (IKE)")
--cfgmode:value("man", "Manual Key")


-- Manual dependencies END-------------------------------------------------------------------------------

o = s:option( ListValue, "exchange_mode", translate("Mode") )
o:depends({cfgmode="ike"})
o.default = "main"
o:value("main","main")
o:value("aggressive","aggressive")
o:value("base","base")

o = s:option( Flag, "nat_traversal", translate("Enable NAT traversal") )
o = s:option( Flag, "initial_contact", translate("Enable initial contact") )

-- IKE dependencies -------------------------------------------------------------------------------

local iden  = s:option( ListValue, "my_identifier_type", translate("My identifier type") )
iden.nowrite = true
iden.default = "address"
iden:value("fqdn", "fqdn")
iden:value("user_fqdn", "user fqdn")
iden:value("address", "address")

o = s:option( Value, "my_identifier", translate("My identifier") )
o:depends({cfgmode="ike"})
function o.validate(self, value, section)
	if iden:formvalue(section) == "address" then
		if not value:match("[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+") then
			m.message = translate("IP address expected in \"My identifier\" field")
			return nil
		end
	end
	return value
end

o = s:option( Value, "pre_shared_key", translate("Preshare Key"),translate("(Length [6-32])") )
o:depends({cfgmode="ike"})

o = s:option( Value, "remote", translate("Remote VPN endpoint"),translate("IP address") )
o:depends({cfgmode="ike"})
o.datatype = "ipaddr"
-- Section2 p1_proposal-------------------------------------------------------------------------------

s2 = m:section( NamedSection, "pre_3des_sha1", "p1_proposal", translate("Phase 1") )

o = s2:option( ListValue, "encryption_algorithm", translate("Encryption") )
o.default = "3des"
o:value("3des","3des")
o:value("aes 128","aes 128")
o:value("aes 192","aes 192")
o:value("aes 256","aes 256")

o = s2:option( ListValue, "hash_algorithm", translate("Hash") )
o.default = "sha1"
o:value("sha1", "sha1")
o:value("md5", "md5")

o = s2:option( ListValue, "dh_group", translate("Dh group") )
o.default = "modp1024"
o:value("modp768", "modp768")
o:value("modp1024", "modp1024")
o:value("modp1536", "modp1536")

s3 = m:section( NamedSection, "g2_aes_sha1", "p2_proposal", translate("Phase 2") )

o = s3:option( ListValue, "pfs_group", translate("PFS group") )
o.default = "modp1024"
o:value("modp768", "modp768")
o:value("modp1024", "modp1024")
o:value("modp1536", "modp1536")

o = s3:option( ListValue, "encryption_algorithm", translate("Encryption") )
o.default = "3des"
o:value("3des","3des")
o:value("aes 128","aes 128")
o:value("aes 192","aes 192")
o:value("aes 256","aes 256")

o = s3:option( ListValue, "authentication_algorithm", translate("Authentication") )
o.default = "hmac_sha1"
o:value("hmac_sha1", "hmac_sha1")
o:value("hmac_md5", "hmac_md5")
--Section sainfo ---------------------------------------------------------------------------------------

s4 = m:section( NamedSection, "lan1", "sainfo", translate("Remote network secure group") )

local remip = s4:option( Value, "remote_lan", translate("IP address") )
remip.datatype = "ipaddr"
remip.nowrite = true
--[[
function remip.cfgvalue(self, section) 
	local val = self.map:get(section, "remote_subnet")
	if val then
		return val:match("(%d+%.%d+%.%d+%.%d+)")
	end
end
]]--

local remmask = s4:option( Value, "remote_mask", translate("Subnet mask"), translate("(Number [0-32])") )
remmask.datatype = "range(0,32)"
function remmask.validate(self, value, section)
	remote = luci.ip.IPv4(remip:formvalue(section) .. "/" .. value)
	ip = uci:get("network", "lan", "ipaddr")
	lan = luci.ip.IPv4(ip)
	
	if remote:contains(lan) then
		m.message = translate("\"Remote network secure group\" should differ from device LAN IP (" .. ip .. ")")
		return nil
	end
	return value
end

--[[
function remmask.cfgvalue(self, section)
	local val = AbstractValue.cfgvalue(self, section)
	local mask
	if val then 
		mask = val:match("(%/%d+)")
		mask = mask:gsub("(%/)","")
		return mask
	end
end

function remmask.write(self, section, remote_mask)
	local mip = require("luci.ip") -- ip module
	local local_ip   = uci:get("network", "lan", "ipaddr")
	local local_mask = uci:get("network", "lan", "netmask")
	local remote_ip = remip:formvalue(section)
		
	if local_ip and local_mask and remote_ip and remote_mask then
		local cidr = mip.IPv4(local_ip, local_mask)

		if cidr then
			local ip = cidr:network()
			local mask = cidr:prefix()
			self.map:set(section, "local_subnet", (ip:string() .. "/" .. mask) )	
		end
		AbstractValue.write(self, section, remote_ip .. "/".. remote_mask)
	end
end
]]--

--o = s:option( DynamicList, "_p1_proposal", translate("Phase 1 Name"), translate("") )
--o:depends({cfgmode="ike"})


--o = s:option( DynamicList, "_p2_proposal", translate("Phase 2 Name"), translate("") )
--o:depends({cfgmode="ike"})


local s5 = m:section( NamedSection, "keepalive", "keepalive", translate("Tunnel keep alive") )

o = s5:option( Flag, "enabled", translate("Enable keep alive") )


o = s5:option( Value, "ping_ipaddr", translate("Ping IP address") )
o.datatype = "ipaddr"
o.nowrite = true

o = s5:option( Value, "ping_period", translate("Ping period (seconds)") )
o.nowrite = true
o.datatype ="range(0,9999999)"

function m.state_handler(self, state)
	if state == FORM_CHANGED then
		m.success = translate("Ipsec settings: Successfully changed")
	end
	return state
end

--[[

function m.state_handler(self, state)
	if state == FORM_CHANGED then
		local enabled = uci:get("racoon", "ipsec1", "enabled")
		
		if enabled then 
			--print("IPsec service is enabled...")
			if enabled then
				--luci.sys.call("/etc/init.d/racoon stop; /etc/init.d/racoon start; /etc/init.d/racoon enable") 
			else
				--print("IPsec service is disabled...")
				--luci.sys.call("/etc/init.d/racoon stop")
			end
		end
	end

	return state
end

]]--

--[[
o = s:option( ListValue, "keyexc", translate("IPSec key exchange mode") )
o.default = "manual"
o:value("manual", "Manual Key")
o:value("auto", "Auto Key (IKE)")

o = s:option( ListValue, "mode", translate("Mode") )
o.default = "tun"
o:value("tun", "Tunnel")
o:value("tran","Transport")


o = s:option( Value, "ripaddr", translate("Remote VPN endpoint") )


s2 = m:section( NamedSection, "secure", "ipsec", translate("Remote network secure group"), translate("Secure group") )

o = s2:option( Value, "ipaddr", translate("IP address") )
o = s2:option( Value, "netmask", translate("Subnet mask") )


s3 = m:section( NamedSection, "encapsulation", "ipsec", translate("Authentication header (AH) settings") )

o = s3:option( ListValue, "encproto", translate("Encapsulation protocol") )
o.default = "ah"
o:value("ah",  "ah")
o:value("esp", "esp")

o = s3:option( Value, "inspi", translate("Inbound SPI") )

o = s3:option( Value, "outspi", translate("Outbound SPI") )

o = s3:option( ListValue, "auth", translate("Authentification algorithm") )
o.default = "hmac-md5"
o:value("hmac-md5",  "hmac-md5")
o:value("hmac-sha1", "hmac-sha1")
o:value("keyed-md5", "keyed-md5")
o:value("keyed-sha1","keyed-sha1")
o:value("hmac-sha2-256", "hmac-sha2-256")
o:value("hmac-sha2-384","hmac-sha2-384")
o:value("hmac-sha2-512","hmac-sha2-512")
o:value("hmac-ripemd160","hmac-ripemd160")
o:value("aes-xcbc-mac", "aes-xcbc-mac")

o = s3:option( Value, "sharekey", translate("Preshare key") )

s4 = m:section( NamedSection, "keepalive", "ipsec", translate("Tunnel keep alive") )

o = s4:option( Value, "ping_ipaddr", translate("Ping IP address") )
o = s4:option( Value, "ping_period", translate("Ping period (seconds)") )
]]--
return m
