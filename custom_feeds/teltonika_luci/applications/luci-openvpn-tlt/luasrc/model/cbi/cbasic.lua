local sys = require("luci.sys")
local ipc = require("luci.ip")
local uci = require("luci.model.uci").cursor()
-- local mix = require("luci.mtask")
local mode, o, deathTrap = false

-- debug -----------------------------------------------------------------------
local function cecho(string)
	luci.sys.call("echo \"" .. string .. "\" >> /tmp/log.log")
end

local VPN_INST, TMODE

if arg[1] then
	VPN_INST = arg[1]
else
	--print("[Openvpn.cbasic] Fatal Err: Pass openvpn instance failed")
	--Shoud redirect back to overview
	return nil
end

-- split by word ---------------------------------------------------------------
function  split_by_word(config, section, option, order)
	local uci_l = require "luci.model.uci".cursor()
	local values={}
	if config and section and option and order then
		local val = uci_l:get(config, section, option)
		if val then
			for v in val:gmatch("[%w+%.]+") do 
				table.insert(values, v) 
			end
		end
	end
	return values[order]
end

local m = Map("openvpn","OpenVPN instance: %s" % {VPN_INST}, "")

if VPN_INST then
	TMODE = VPN_INST:match("%l+")
	if TMODE == "client" then
		m.spec_dir = "/etc/openvpn/"
	elseif TMODE == "mdex" then
		m.spec_dir = "/etc/openvpn/"
	elseif TMODE == "server" then
		m.spec_dir = "/etc/easy-rsa/keys/"
	else
		--print("[Openvpn.cbasic] Fatal Err: get mode failed")
		return
	end
end

-- auth file fill --------------------------------------------------------------
local function fill_auth_file()
	local user_ = m:formvalue("cbid.openvpn." .. VPN_INST .. ".username_auth") or ""
	local pass_ = m:formvalue("cbid.openvpn." .. VPN_INST .. ".password_auth") or ""
	local dir_  = uci:get("openvpn", VPN_INST, "auth_user_pass") or ""
	
	if not dir_ or dir == "" then
		return
	end
		
	luci.sys.call("rm " .. dir_)
	if user_ and user_ ~= "" then
		luci.sys.call("echo " .. user_ .. " > " .. dir_)
	end
	if pass_ and pass_ ~= "" then
		luci.sys.call("echo " .. pass_ .. " >> " .. dir_)
	end
end	

--------------------------------------------------------------------------------
-- advanced start/stop procedure for openvpn. ucitrack do not support reload function
-- function m.on_commit(map)
-- 	local vpnEnable = m:formvalue("cbid.openvpn." .. VPN_INST .. ".enable")
-- 	-- it's not PID, this function returns fail or success (shell var $? = 0 or 1)
-- 	local vpnPIDstatus = luci.sys.call("pidof openvpn > /dev/null")
-- 	local vpnRunning = false
-- 	
-- 	uci:commit("firewall")
-- 	
-- 	if vpnPIDstatus == 0 then
-- 		vpnRunning = true
-- 	end	
-- 	
-- 	if vpnEnable then
-- 		if vpnRunning then
-- 			luci.sys.call("/etc/init.d/openvpn reload > /dev/null")
-- 		else
-- 			luci.sys.call("/etc/init.d/openvpn start > /dev/null")
-- 		end
-- 	else
-- 		luci.sys.call("/etc/init.d/openvpn stop > /dev/null")
-- 	end
-- end

local s = m:section( NamedSection, VPN_INST, "openvpn", translate("Main settings"), " ")

--#########################Same settings for server/client #####################

o = s:option( Flag, "enable", translate("Enable") )
o.rmempty = false
o.forcewrite = true
function o.write(self, section, value)
	if deathTrap then return
	else deathTrap = true end	
	
	if not TMODE == "mdex" then --Enable NAT in case mdex VPN disabled it
		local fwzone = "nil"
		uci:foreach("firewall", "zone", function(s)
			if s.name == "vpn" then
				fwzone = s[".name"]
				uci:set("firewall", fwzone, "masq", "1")
				uci:save("firewall")
			end
		end)
	end
	
	if TMODE == "mdex" then
		-- fill auth file /etc/openvpn/xxx if instance is mdex
		fill_auth_file()
	end		
	
	local function modify_firewall(state, port)
		uci:set("firewall", "VPN", "dest_port", port)
		uci:set("firewall", "VPN", "target", state)
		uci:save("firewall")
	end
	
	-- configure firewall
	local fwPort, fwState, fEnable
	if TMODE == "mdex" then
		local protocol = m:formvalue("cbid.openvpn." .. VPN_INST .. ".proto")
		if protocol == "udp" then
			fwPort = m:formvalue("cbid.openvpn." .. VPN_INST .. ".__port_udp")
			fwState = "ACCEPT"
		elseif protocol == "tcp-client"	then
			fwPort = m:formvalue("cbid.openvpn." .. VPN_INST .. ".__port_tcp")
			fwState = "DROP"
		end
		
		fEnable = m:formvalue("cbid.openvpn.mdex_OpenVPN.enable")
		if fEnable ~= "1" then
			fwState = "DROP"
		end
	else
		fwPort = m:formvalue("cbid.openvpn." .. VPN_INST .. ".port")
		fwState = "ACCEPT"
	end
	
	if fwState and fwPort and fwPort ~= "" then	
		modify_firewall(fwState, fwPort)
	end	
	
	-----------------------
	-- mssfix workaround --
	-----------------------
	local tmpMssfix = m:formvalue("cbid.openvpn." .. VPN_INST .. ".__mssfix")
	local tmpFragment = m:formvalue("cbid.openvpn." .. VPN_INST .. ".fragment")
	if tmpMssfix == "1" and tmpFragment then
		uci:set("openvpn", VPN_INST, "mssfix", tmpFragment)
	else
		uci:delete("openvpn", VPN_INST, "mssfix")
	end
	uci:save("openvpn")
		
	if value == self.enabled then
		uci:set("openvpn", VPN_INST, "enable", "1")
		uci:save("openvpn")
	else
		uci:set("openvpn", VPN_INST, "enable", "0")
		uci:save("openvpn")
	end
	uci:commit("openvpn")
end

local dev_type
if TMODE == "mdex" then
else
	dev_type = s:option( ListValue, "__dev", "Tun/Tap", translate("Type of used device") )
	dev_type.default = "tun"
	dev_type:value("tun0", "Tun (tunnel)")
	dev_type:value("tap0", "Tap (bridged)")
end


local proto
if VPN_INST then
	TMODE = VPN_INST:match("%l+")
	if TMODE == "client" then
		proto = s:option( ListValue, "proto", translate("Protocol") )
		proto.default = "udp"
		proto:value("udp", "UDP")
		proto:value("tcp-client", "TCP")
	elseif TMODE == "mdex" then
		proto = s:option( ListValue, "proto", translate("Protocol") )
		proto:value("udp", "UDP")
		proto:value("tcp-client", "TCP")
		proto.default = "udp"
	elseif TMODE == "server" then
		 proto = s:option( ListValue, "proto", translate("Protocol") )
		proto.default = "udp"
		proto:value("udp", "UDP")
		proto:value("tcp-server", "TCP")
	end
end
local  port, porta
if TMODE == "mdex" then
	port = s:option(Value, "__port_udp", translate("Port"))
	port:depends({proto="udp"})
	port.default = "9300"
	port.datatype = "port"
	port:value("9300")
	
	function port.write(self, section, value)
		Value.write(self, section, value)
		uci:set("openvpn", VPN_INST, "port", value)
		uci:save("openvpn")
		uci:commit("openvpn")
	end	
	
	porta = s:option( Value, "__port_tcp", translate("Port") )
	porta:depends({proto="tcp-client"})
	porta.datatype="port"
	porta.default = "443"
	porta:value("443")
	
	function porta.write(self, section, value)
		Value.write(self, section, value)
		uci:set("openvpn", VPN_INST, "port", value)
		uci:save("openvpn")
		uci:commit("openvpn")
	end
	
else
	port = s:option( Value, "port", translate("Port"), translate("TCP/UDP port for both, local and remote") )
	port.datatype = "port"
end

local auth, comp

if TMODE == "mdex" then
	comp = s:option( Flag, "comp_lzo", "Enable LZO compression")
else
	comp = s:option( Flag, "comp_lzo", "LZO", translate("Use fast LZO compression") )
	auth = s:option( ListValue,"_auth", translate("Authentication") )
	auth.default = "tls"
	auth:value("skey", "Static key")
	auth:value("tls", "Tls")
	auth.nowrite = true
	function auth.cfgvalue(self, section) 
		local stat = self.map:get(section, self.option)
		--local stat = self.map:get(section, "secret")
		return stat and stat or "tls"
	end
end



if TMODE == "mdex" then
	o = s:option( Value,"remote", translate("Remote host/IP address") )
	
	o = s:option( ListValue, "cipher", "Cipher")
	o:value("none", "none")
	o:value("DES-CBC", "DES-CBC");
	o:value("RC2-CBC", "RC2-CBC");
	o:value("DES-EDE-CBC", "DES-EDE-CBC");
	o:value("DES-EDE3-CBC", "DES-EDE3-CBC");
	o:value("BF-CBC", "BF-CBC");
	o:value("RC2-40-CBC", "RC2-40-CBC");
	o:value("CAST5-CBC", "CAST5-CBC");
	o:value("RC2-64-CBC", "RC2-64-CBC");
	o:value("AES-128-CBC", "AES-128-CBC");
	o:value("AES-192-CBC", "AES-192-CBC");
	o:value("AES-256-CBC", "AES-256-CBC");
	o:value("SEED-CBC", "SEED-CBC");
	
	function o.write(self, section, value)
		uci:set("openvpn", VPN_INST, "cipher", value)
		uci:save("openvpn")
	end
	
	o = s:option( Value, "fragment", "Maximum fragment size")
	o:depends({proto="udp"})
	o.default = "1300"
	o.forcewrite=true
		
	o = s:option(Flag, "__mssfix", "Limit TCP packet size (mssfix)")
	o:depends({proto="udp"})
	o.enabled = "1"
	o.disabled = "0"
		
	no_nat = s:option( Flag, "dis_nat_vpn", "Enable VPN NAT") --Disable NAT functionality for VPN (tun0) zone
	no_nat.rmempty = false

	function no_nat.write(self, section, value)
		uci:set("openvpn", VPN_INST, "dis_nat_vpn", value)
		uci:save("openvpn")
		local fwzone = "nil"
		uci:foreach("firewall", "zone", function(s)
			if s.name == "vpn" then
				fwzone = s[".name"]
				if value == "1" then	--//Enable NAT=1 means masq=1
					uci:set("firewall", fwzone, "masq", "1")
				else
					uci:set("firewall", fwzone, "masq", "0")
				end
				uci:save("firewall")
			end
		end)
	end
	
	o = s:option( Flag, "float", "Allow remote peer's IP change")
	function o.write(self, section, value)
		uci:set("openvpn", VPN_INST, "float", value)
		uci:save("openvpn")
	end	
	o = s:option( Flag, "nobind", "Do not bind to local address")
	function o.write(self, section, value)
		uci:set("openvpn", VPN_INST, "nobind", value)
		uci:save("openvpn")
	end
	o = s:option( Value, "reneg_sec", "Key renegotiation interval (s)")
	function o.write(self, section, value)
		uci:set("openvpn", VPN_INST, "reneg_sec", value)
		uci:save("openvpn")
	end
	o = s:option( Value, "explicit_exit_notify_mdex", "Exit message count")
	o:depends({proto="udp"})
	o.default = "2"
	function o.write(self, section, value)
		uci:set("openvpn", VPN_INST, "explicit_exit_notify_mdex", value)
		uci:save("openvpn")
	end
	
	o = s:option( Flag,"redirect_gateway", translate("Override gateway") )
	function o.cfgvalue(self, section)
		if uci:get("openvpn", VPN_INST, "redirect_gateway") == "def1" then
			return "1"
		else
			return "0"
		end
	end

	function o.write(self, section, value)
		uci:set("openvpn", VPN_INST, "redirect_gateway", "def1")
		uci:save("openvpn")
	end

	o = s:option(Value, "extra_ops", translate("Extra options"))
	function o.write(self, section, value)
		uci:set("openvpn", VPN_INST, "extra_ops", value)
		uci:save("openvpn")
	end
	
	local s = m:section( NamedSection, VPN_INST, "openvpn", translate("Authentication"), " ")
	
	o = s:option( Value,"username_auth", translate("Username") )
	
	function o.write(self, section, value)
		uci:set("openvpn", VPN_INST, "username_auth", value)
		uci:save("openvpn")
	end
	
	o = s:option( Value,"password_auth", translate("Password") )
	o.password = true
	
	function o.write(self, section, value)	
		uci:set("openvpn", VPN_INST, "password_auth", value)
		uci:save("openvpn")
		-- NOTE: Programmer_2 comment about system call at the end of this function.
		-- This will cause serios problems when password changes and username remains the same.
		-- We will use external function fill_auth_file()
		--sys.exec("echo " .. value .. " >> `uci get openvpn.".. VPN_INST .. ".auth_user_pass`")
	end
	
	o = s:option( FileUpload, "ca", translate("Certificate authority") )
	o = s:option( FileUpload, "crl_verify", translate("Certificate revocation list") )
end
if TMODE == "client" then

	o = s:option( Value,"remote", translate("Remote host/IP address") )
	--o.datatype = "ip4addr"
	
	o = s:option( Value, "resolv_retry", translate("Resolve Retry") )
	o.forcewrite = true
	
	function o.cfgvalue(self, section)
		local val = self.map:get(section, self.option)
		return val and val or "infinite"
	end
	
	function o.write(self, section, value) 
		if not value then
			value = "infinite"
		end
		AbstractValue.write(self, section, value)
	end
	
	o = s:option( Flag, "nobind", translate("No Bind"), translate("Do not bind to local address and port") )
	o:depends({dev="tun", _auth="tls"})	
	o:depends({dev="tap", _auth="tls"})
	
	keep = s:option( Value, "keepalive", translate("Keep alive"), translate("Helper directive to simplify the expression of --ping and --ping-restart") )
	
	function keep:validate(Value)
	local failure
	if Value == "" then return Value end
	if not Value:match("%d+ %d+") then
		m.message = translate("Keep alive value is not correct! Must be two integer values, e.g.: 10 120")
		failure = true
		return nil
	end

	return Value
	
end

	--[[#########################Client settings depdends on tls only############################]]--
	o = s:option( Flag, "client", translate("Client") )
	o:depends({_auth="tls"})
	
	--[[#########################Client settings depdends on tun and skey############################]]--
	--[[ Ifconfig option ]]--

	local ifconf_local = s:option( Value, "_ifconfig" ,translate("Local tunnel endpoint IP") )
	ifconf_local:depends({__dev="tun0", _auth="skey"})
	ifconf_local.datatype = "ip4addr"
	ifconf_local.nowrite = true

	
	function ifconf_local.cfgvalue(self, section)
		return split_by_word(self.config, section, "ifconfig", 1)
	end

	local ifconf_remote = s:option( Value, "ifconfig", translate("Remote tunnel endpoint IP") )
	ifconf_remote:depends({__dev="tun0", _auth="skey"})
	ifconf_remote.forcewrite = true
	ifconf_remote.datatype = "ip4addr"

	function ifconf_remote.write(self,section, ip2) 
		local ip1 = ifconf_local:formvalue(section)
			
		if ip1 and ip2 then
			AbstractValue.write(self, section, ip1.." "..ip2)
		end
	end
	
	function ifconf_remote.cfgvalue(self, section)
		return split_by_word(self.config, section, "ifconfig", 2)
	end
	
	-- Route option
	local route_ip = s:option( Value, "_route" ,translate("Remote network IP address") )
	route_ip:depends({__dev="tun0", _auth="skey"})
	route_ip.nowrite = true
	route_ip.datatype = "ipaddr"
	
	function route_ip.cfgvalue(self, section)
		return split_by_word(self.config, section, "route", 1)
	end

	local route_mask = s:option( Value, "route", translate("Remote network IP netmask") )
	route_mask:depends({__dev="tun0", _auth="skey"})
	route_mask.forcewrite = true
	route_mask.datatype = "ip4addr"

	function route_mask.cfgvalue(self, section)
		return split_by_word(self.config, section, "route", 2)
	end
	
	function route_mask.write(self, section, remote_mask)
		local remote_ip = route_ip:formvalue(section)

		if remote_ip and remote_mask then
			AbstractValue.write(self, section, remote_ip.." "..remote_mask)
		end
	end
	
	-- ######################### Client settings depdends on tap and tls ###########################
	o = s:option( DummyValue, "user")
	
	function o:render() end
	
	function o:parse(section)
		local dev_val  = dev_type:formvalue(section)
		local auth_val = auth:formvalue(section)
		
		if auth_val=="tls" and dev_val=="tap" then
			AbstractValue.write(self, section, "root")
			self.section.changed = true
		else
			AbstractValue.remove(self, section)
			self.section.changed = true
		end
	end

	-- ######################### Client settings depdends on tun and tls ###########################
	o = s:option(DummyValue, "push")
	function o:render() end
	
	function o:parse(section)
		local dev_val  = dev_type:formvalue(section)
		local auth_val = auth:formvalue(section)

		if auth_val=="tls" and dev_val=="tun" then
			local net_ip   = uci:get("network", "lan", "ipaddr")
			local net_mask = uci:get("network", "lan", "netmask")
			if net_ip and net_mask then
				local cidr_inst = ipc.IPv4(net_ip, net_mask)
				if cidr_inst then
					local ip = cidr_inst:network()
					local mask = cidr_inst:mask()
					if ip and mask then
						if AbstractValue.write(self, section, "route "..ip:string().." "..mask:string()) then
							self.section.changed = true
						end
					end	
				end
			end
		else
			AbstractValue.remove(self, section)
			self.section.changed = true
		end
	end
end


if TMODE == "server" then
	--######################### Server settings depdends on tls only ############################
	o = s:option( Flag, "client_to_client", translate("Client to client"), translate("Allow client-to-client traffic") )
	o:depends({ _auth="tls"})

	o = s:option( Flag, "duplicate_cn", translate("Allow duplicate certificates") )
	o:depends({ _auth="tls"})

	o = s:option( Value, "keepalive", translate("Keep alive"), translate("Helper directive to simplify the expression of --ping and --ping-restart in server mode configurations") )
	o:depends({_auth="tls"})
	
	o = s:option( DummyValue, "ifconfig_pool_persist")
	function o:render() end
	
	function o:parse(section)
		local auth_val = auth:formvalue(section)

		if auth_val=="tls" then
			AbstractValue.write(self, section, "/tmp/ipp.txt")
			self.map:set(section, "status", "/tmp/openvpn-status.log")
			self.section.changed = true
		else
			AbstractValue.remove(self, section)
			self.map:del(section, "status")
			self.section.changed = true
		end
	end
	
	-- ######################### Server settings depdends on tun and skey ##########################
	-- Ifconfig option
	local ifconf_local = s:option( Value, "_ifconfig" ,translate("Local tunnel endpoint IP") )
	ifconf_local:depends({__dev="tun0", _auth="skey"})
	ifconf_local.datatype = "ip4addr"
	
	function ifconf_local.write() end
	
	function ifconf_local.cfgvalue(self, section)
		return split_by_word(self.config, section, "ifconfig", 1)
	end

	local ifconf_remote = s:option( Value, "ifconfig", translate("Remote tunnel endpoint IP") )
	ifconf_remote:depends({__dev="tun0", _auth="skey"})
	ifconf_remote.forcewrite = true
	ifconf_remote.datatype = "ip4addr"
		
	function ifconf_remote.write(self,section, ip2) 
		local ip1 = ifconf_local:formvalue(section)
			
		if ip1 and ip2 then
			AbstractValue.write(self, section, ip1.." "..ip2)
		end
	end
	
	function ifconf_remote.cfgvalue(self, section)
		return split_by_word(self.config, section, "ifconfig", 2)
	end
	
	-- resolv_retry option
	o = s:option( Value, "resolv_retry", translate("Resolve Retry") )
	o:depends({__dev="tun0", _auth="skey"})
	o.forcewrite = true
	
	function o.cfgvalue(self, section)
		local val = self.map:get(section, self.option)
		return val and val or "infinite"
	end
	
	function o.write(self, section, value) 
		if not value then
			value = "infinite"
		end
		AbstractValue.write(self, section, value)
	end
	
	
	-- Route option
	local sroute_ip = s:option( Value, "_route" ,translate("Remote network IP address") )
	sroute_ip.nowrite = true
	sroute_ip:depends({__dev="tun0", _auth="skey"})
	
	function sroute_ip.cfgvalue(self, section)
		return split_by_word(self.config, section, "route", 1)
	end

	local sroute_mask = s:option( Value, "route", translate("Remote network netmask") )
	sroute_mask:depends({__dev="tun0", _auth="skey"})
	sroute_mask.forcewrite = true
	sroute_mask.datatype = "ipaddr"

	function sroute_mask.cfgvalue(self, section)
		return split_by_word(self.config, section, "route", 2)
	end
	
	function sroute_mask.write(self, section, remote_mask)
		local remote_ip = sroute_ip:formvalue(section)

		if remote_ip and remote_mask then
			AbstractValue.write(self, section, remote_ip.." "..remote_mask)
		end
	end
	
	-- ################# Server settings depdends on tap and tls ###################################
	-- server_bridge option
	local sbridge_from = s:option( Value, "_server_bridge_from", translate("IP address pool for clients, from") )
	sbridge_from:depends({dev="tap", _auth="tls"})
	sbridge_from.datatype = "ipaddr"
	sbridge_from.nowrite = true

	function sbridge_from.cfgvalue(self, section)
		return split_by_word(self.config, section, "server_bridge", 3)
	end

	local sbridge_to = s:option( Value, "server_bridge", translate("IP address pool for clients, to") )
	sbridge_to:depends({dev="tap", _auth="tls"})
	sbridge_to.forcewrite = true
	sbridge_to.datatype = "ipaddr"

	function sbridge_to.write(self, section, br_to) 
			local net_ip   = uci:get("network", "lan", "ipaddr")
			local net_mask = uci:get("network", "lan", "netmask")
			local br_from = sbridge_from:formvalue(section)
			
			if net_ip and net_mask and br_from and br_to then
				local s_bridge = net_ip.." "..net_mask.." "..br_from.." "..br_to
				AbstractValue.write(self, section, s_bridge)
			end
	end
	
	function sbridge_to.cfgvalue(self, section)
		return split_by_word(self.config, section, "server_bridge", 4)
	end	
		
	-- ################# Server settings depdends on tun and tls ###################################
	-- server option
	local serv_ip = s:option( Value, "_server", translate("Server IP address") )
	serv_ip:depends({dev="tun", _auth="tls"})
	serv_ip.nowrite = true
	serv_ip.datatype = "ip4addr"
	
	function serv_ip.cfgvalue(self, section)
		return split_by_word(self.config, section, "server", 1)
	end

	local serv_mask = s:option( Value, "server" ,translate("Server netmask") )
	serv_mask:depends({dev="tun", _auth="tls"})
	serv_mask.forcewrite = true
	serv_mask.datatype = "ipaddr"

	function serv_mask.cfgvalue(self, section)
		return split_by_word(self.config, section, "server", 2)
	end
	
	function serv_mask.write(self, section, serv_mask)
		local sip = serv_ip:formvalue(section)
		
		if sip and serv_mask then
			AbstractValue.write(self, section, sip .. " ".. serv_mask)
		end
	end
	
	-- push option
	o = s:option(DummyValue, "push")
	function o:render() end
	
	function o:parse(section)
		local dev_val  = dev_type:formvalue(section)
		local auth_val = auth:formvalue(section)

		if auth_val=="tls" and dev_val=="tun" then
			local net_ip   = uci:get("network", "lan", "ipaddr")
			local net_mask = uci:get("network", "lan", "netmask")
			if net_ip and net_mask then
				local cidr_inst = ipc.IPv4(net_ip, net_mask)
				if cidr_inst then
					local ip = cidr_inst:network()
					local mask = cidr_inst:mask()
					if ip and mask then
						if AbstractValue.write(self, section, "route "..ip:string().." "..mask:string()) then
							self.section.changed = true
						end
					end	
				end
			end
		else
			AbstractValue.remove(self, section)
			self.section.changed = true
		end
	end
	
	-- client_config_dir option
	o = s:option( DummyValue, "client_config_dir")
	function o:render() end
	
	function o:parse(section)
		local dev_val  = dev_type:formvalue(section)
		local auth_val = auth:formvalue(section)

		if auth_val=="tls" and dev_val=="tun" then
			AbstractValue.write(self, section, "/etc/openvpn/ccd/")
			self.section.changed = true
		else
			AbstractValue.remove(self, section)
			self.section.changed = true
		end
	end
end

if TMODE == "client" then
	local s = m:section( NamedSection, VPN_INST, "openvpn", translate("Authentication"), " ")
	
	o = s:option( FileUpload, "ca", translate("Certificate authority") )
	o:depends({_auth="tls"})

	o = s:option( FileUpload, "cert", translate("Client certificate") )
	o:depends({_auth="tls"})

	o = s:option( FileUpload, "key", translate("Client key") )
	o:depends({_auth="tls"})
	
	o = s:option( FileUpload, "secret", translate("Static pre-shared key") )
	o:depends({_auth="skey"})
elseif TMODE == "server" then
	local s = m:section( NamedSection, VPN_INST, "openvpn", translate("Authentication"), " ")

	o = s:option( FileUpload, "ca", translate("Certificate authority") )
	o:depends({_auth="tls"})

	o = s:option( FileUpload, "cert", translate("Server certificate") )
	o:depends({_auth="tls"})

	o = s:option( FileUpload, "key", translate("Server key") )
	o:depends({_auth="tls"})
	
	o = s:option( FileUpload, "dh", translate("Diffie Hellman parameters") )
	o:depends({_auth="tls"})
	
	o = s:option( FileUpload, "secret", translate("Static pre-shared key") )
	o:depends({_auth="skey"})
else
	
end

return m
