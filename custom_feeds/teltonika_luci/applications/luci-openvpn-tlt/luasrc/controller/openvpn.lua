--[[
LuCI - Lua Configuration Interface

Copyright 2008 Steven Barth <steven@midlink.org>
Copyright 2008 Jo-Philipp Wich <xm@leipzig.freifunk.net>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

$Id: openvpn.lua 7362 2011-08-12 13:16:27Z jow $
]]--

module("luci.controller.openvpn", package.seeall)

function index()
	entry( {"admin", "vpn-menu", "openvpn-tlt"}, cbi("openvpn"), _("OpenVPN"), 1).i18n = "openvpn-tlt"
	entry( {"admin", "vpn-menu", "openvpn-tlt", "cbasic"}, cbi("cbasic"), nil).leaf = true
	--entry( {"admin", "vpn-menu", "ipsec"}, cbi("ipsec"), _("IPsec"), 30)
	entry( {"admin", "vpn-menu", "ipsec"}, cbi("ipsec"), nil, nil)
end
