local m 
local s, t, p, h, k
local o
local psw1, psw2, psw3, psw4
local telnum1, telnum2, telnum3, telnum4

m = Map("sms_utils", translate("SMS Utilities"), translate("Settings for SMS Utilities"))

----------------------------------------------------------------------------------------------------
-- SMS Reboot
----------------------------------------------------------------------------------------------------
s = m:section(TypedSection, "sms_reboot", translate("SMS Reboot"), translate("Reboot router via SMS message"))
s.anonymous = true
s.addremove = false

o = s:option(Flag, "enable", translate("Enable SMS Reboot"))
o.rmempty = false

psw1 = s:option(Value, "psw", translate("SMS text"))
psw1:depends("enable", "1")

function psw1:validate(Value)
	local failure	
	if not Value:match("^.*$") then
		m.message = translate("err: SMS Reboot text is incorrect!")
		failure = true
	end
	if not failure then
		return Value
	end
	return nil
end

telnum1 = s:option(DynamicList, "telnum", translate("Sender phone number"), translate("e.g. +491234567890"))
telnum1:depends("enable", "1")
function telnum1:validate(Values)
	local failure
	local txtMsg1 = m:formvalue("cbid.sms_utils.sms_reboot.psw")
	for k,v in pairs(Values) do
		if not v:match("^[+%d]%d*$") and v ~= "" then
			m.message = translate("err: SMS Reboot sender phone number ".. v .." is incorrect!")
			failure = true
		end
	end
	if not failure then
		if txtMsg1 == "" then
			m.message = translate("err: SMS Reboot text field is empty!")
			return nil
		else
			return Values
		end	
	end
	return nil
end
 
o = s:option(Flag, "status_sms", translate("Get status"), translate("Get detailed router connection information via SMS message after SMS reboot"))
o:depends("enable", "1")

----------------------------------------------------------------------------------------------------
-- router status via sms
----------------------------------------------------------------------------------------------------
t = m:section(TypedSection, "status_send", translate("Status via SMS"), translate("Get network status via SMS"))
t.addremove = false
t.anonymous = true

o = t:option(Flag, "enable", translate("Enable SMS Status"))
o.rmempty = false


psw2 = t:option(Value, "psw", translate("SMS text"))
psw2:depends("enable", "1")

function psw2:validate(Value)
	local failure
	if not Value:match("^.*$") then
		m.message = translate("err: SMS Status text is incorrect!")
		failure = true
	end

	if not failure then
		return Value
	end

	return nil
end

telnum2 = t:option(DynamicList, "telnum", translate("Sender phone number"), translate("e.g. +491234567890"))
telnum2:depends("enable", "1")

function telnum2:validate(Values)
	local failure
	local txtMsg2 = m:formvalue("cbid.sms_utils.status_send.psw")
	local txtMsg1 = m:formvalue("cbid.sms_utils.sms_reboot.psw")
	for k,v in pairs(Values) do
		if not v:match("^[+%d]%d*$") and v ~= "" then
			m.message = translate("err: Status via SMS sender phone number ".. v .." is incorrect!")
			failure = true
		end
	end
	if not failure then
		if txtMsg2 == "" then
			m.message = translate("err: Status via SMS text field is empty!")
			return nil
		elseif txtMsg2 == txtMsg1 then
			m.message = translate("err: SMS Reboot text and Status via SMS text must be different!")
			return nil
		else	
			return Values
		end	
	end
	return nil
end

----------------------------------------------------------------------------------------------------
-- factory reset
----------------------------------------------------------------------------------------------------
p = m:section(TypedSection, "reset_settings", translate("Factory reset via SMS"), translate("Reset router settings to factory default values"))

o = p:option(Flag, "enable", translate("Enable factory reset"))
o.rmempty = false

psw3 = p:option(Value, "psw", translate("SMS text"))
psw3:depends("enable", "1")

function psw3:validate(Value)
	local failure
	if not Value:match("^.*$") then
		m.message = translate("err: Factory reset text is incorrect!")
		failure = true
	end

	if not failure then
		return Value	
	end

	return nil
end

telnum3 = p:option(DynamicList, "telnum", translate("Sender phone number"), translate("e.g. +491234567890"))
telnum3:depends("enable", "1")

function telnum3:validate(Values)
	local failure = false
	local txtMsg3 = m:formvalue("cbid.sms_utils.reset_settings.psw")
	local txtMsg2 = m:formvalue("cbid.sms_utils.status_send.psw")
	local txtMsg1 = m:formvalue("cbid.sms_utils.sms_reboot.psw")
	for k,v in pairs(Values) do
		if not v:match("^[+%d]%d*$") and v ~= "" then
			m.message = translate("err: Factory reset sender phone number ".. v .." is incorrect!")
			failure = true
		end
	end
	
	if not failure then
		if txtMsg3 == "" then
			m.message = translate("err: SMS Reboot text field is empty!")
			return nil
		elseif txtMsg3 == txtMsg1 or txtMsg3 == txtMsg2 then
			m.message = translate("err: Factory reset text must be different!")
			return nil
		else
			return Values
		end
	end
	
	return nil
end
----------------------------------------------------------------------------------------------------
-- set uci via sms
----------------------------------------------------------------------------------------------------
h = m:section(TypedSection, "set_settings", translate("Configuration via SMS"), translate("Set configuration parameters via sms using uci syntax"))

o = h:option(Flag, "enable", translate("Enable configuration via SMS"))
o.rmempty = false

telnum4 = h:option(DynamicList, "telnum", translate("Sender phone number"), translate("e.g. +491234567890"))
telnum4:depends("enable", "1")

function telnum4:validate(Values)
	local failure = false
	
	for k,v in pairs(Values) do
		if not v:match("^[+%d]%d*$") and v ~= "" then
			m.message = translate("err: Configuration set number of sender ".. v .." is incorrect!")
			failure = true
		end
	end
	
	if not failure then
		return Values
	end
	
	return nil
end

----------------------------------------------------------------------------------------------------
-- send sms from lan
----------------------------------------------------------------------------------------------------

k = m:section(TypedSection, "sendmsg_lan", translate("Send SMS from LAN"), translate("Send SMS via HTTP Post/Get from LAN"))

o = k:option(Flag, "enable", translate("Enable send SMS"))
o.rmempty = false

return m
