module("luci.controller.sms-utilities", package.seeall)
require("uci")

function index()
	x = uci.cursor()
	if x:get("system", "module", "type") == "3g" or x:get("system", "module", "type") == "3g_ppp" then
		entry({"admin", "services", "sms-utilities"}, cbi("sms-utilities/sms-utilities"), _("SMS Utilities"), 85)
	end
end

