local m, s, o, ip, mac, name

m = Map("dhcp", translate("Static DHCP IP address allocation"), translate("Configure static DHCP IP allocation"))
m:chain("network");

s = m:section(TypedSection, "host", "")
s.addremove = true
s.anonymous = true

m.redirect = luci.dispatcher.build_url("admin/network/lan")

--
-- ip
--
ip = s:option(Value, "ip", translate("Client IP address"))
ip.datatype = "ipaddr"
function ip:parse(section)
	local fIP = ip:formvalue(section)
	if fName == "" then
		m.message = translate("err: Please fill in IP address field.")
		return nil
	end
	AbstractValue.write(self, section, fIP)
end

--
-- mac
--
mac = s:option(Value, "mac", translate("Client MAC address"))
mac.datatype = "macaddr"
function mac:parse(section)
	local fMac = mac:formvalue(section)
	if fMac == "" then
		m.message = translate("err: Please fill in MAC address field.")
		return nil
	end
	AbstractValue.write(self, section, fMac)
end

-- disable name, use always blank

--
-- name
--
-- name = s:option(Value, "name", translate("Client name"))
-- name.forcewrite = false
-- function name:parse(section)
-- 	local fName = name:formvalue(section)
-- -- Allow empty name field
-- -- 	if fName == "" then
-- -- 		m.message = translate("err: Please fill in Client name field.")
-- -- 		return nil
-- -- 	end
-- 	if fName and #fName > 0 then
-- 		if not (string.find(fName, "[%c?%p?%s?]+") == nil) then
-- 			m.message = translate("err: Only alphanumeric characters allowed in Client name field.")
-- 			return nil
-- 		end
-- 	end
-- 	AbstractValue.write(self, section, fName)
-- end

return m
