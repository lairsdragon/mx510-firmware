/* 
 * atd.c - minimal gsmd client for GSM/3G/4G
 * which acts like a AT command handling server
 * over TCP/IP socket.
 * 
 * Copyright (c) 2013 Teltonika M2M Solutions
 *
 * Authors: Justinas Grauslis	(initial version)
 *
 * Revision 1.0 2013/05/16 justinas
 * Initial release
 */

/* informing libs about threads */
#define _REENTRANT

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <pthread.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <getopt.h>
#include <pwd.h>
#include <syslog.h>
#include <time.h>
#include <netinet/in.h>
#include <libgsmd/libgsmd.h>

#define RUN_AS_USER		"root"
#define DAEMON_NAME		"atd"
#define LOCK_FILE			"/var/lock/atd.lock"
#define BSIZE 				1024
#define PERIOD				100000 /* usec */
/* default config */
#define DEFPORT 			9000
#define DEFTIMEOUT			10 /* sec */
#define DEFDEBUG			0

#define dout(enabled, fmt, args...) (enabled == 0) ? 0 : \
syslog(LOG_NOTICE, "debug at %s:%s:%d: " fmt, \
__FILE__, __func__, __LINE__, ##args)

typedef struct gconfig {
	int port;
	int timeout;
	int debug;
} gconfig;

/* some globals to make world more intresting */
char rxbuf[BSIZE], txbuf[BSIZE];
int sockfd, newsockfd, rlen = 0;
int atret = -1, thread_finished = 0;
gconfig conf;
struct lgsm_handle *lgsmh = NULL;

static void usage(void)
{
	fprintf(stdout, "\nusage: atd OPTIONS\n");
	fprintf(stdout, "  -p, --port <PORT>     Port to listen\n");
	fprintf(stdout, "  -t, --timout <SEC>    Time to wait modem until timeout\n");
	fprintf(stdout, "  -d, --debug           Enable debug mode\n");
	fprintf(stdout, "  -h, --help            Print this information\n\n");
}

static void init_config(void) {
	conf.port = DEFPORT;
	conf.timeout = DEFTIMEOUT;
	conf.debug = DEFDEBUG;
}

static int init_atd(void) {
	/* connect to gsmd daemon */
	if ((lgsmh = lgsm_init(LGSMD_DEVICE_GSMD)) == NULL) {
		syslog(LOG_ERR, "cannot connect to gsmd\n");
		return -1;
	}
	return 0;
}

static void reinit_gsmd(void) {
	if (lgsm_exit(lgsmh) != 0)
		syslog(LOG_NOTICE, 
			"error disconnecting from gsmd (reinit)\n");
	if ((lgsmh = lgsm_init(LGSMD_DEVICE_GSMD)) == NULL) 
		syslog(LOG_ERR, 
			"error connecting to gsmd (reinit)\n");
}

static void soft_exit(int status) {
	int ret = 0;
	if (lgsm_exit(lgsmh) != 0) {
		syslog(LOG_NOTICE, 
			"dirty disconnect from gsmd\n");
		ret = -1;
	}
	if (close(sockfd)) {
		syslog(LOG_NOTICE, 
			"error closing socket\n");
		ret = -1;
	}
	if (ret == 0)
		syslog(LOG_NOTICE, "clean exit\n");
	closelog();
	exit(status);
}

/* for debug */	
static void serve_usr1() {
	dout(conf.debug, 
		"this is serve_usr1 routine\n");
}

/* for debug */	
static void serve_usr2() {
	dout(conf.debug, 
		"this is serve_usr2 routine\n");
}

void *thread_longcall(void *data) {
	if (pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL))
		syslog(LOG_ERR, "thread set state failed");
	
	if (pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL))
		syslog(LOG_ERR, "thread set type failed");

	rlen = BSIZE;
	atret = lgsm_passthrough(lgsmh, rxbuf, txbuf, &rlen);
	thread_finished = 1;
	pthread_exit(0);
}

static int run_or_timeout(void) {
	pthread_t longcall_thread;
	pthread_attr_t attr;
	unsigned long wait = conf.timeout*1000000;

	if (pthread_attr_init(&attr))
		return -1;

	if(pthread_attr_setdetachstate(&attr, 
			PTHREAD_CREATE_DETACHED))
		return -1;

	if (pthread_create(&longcall_thread, 
			&attr, thread_longcall, NULL))
		return -1;

	while (wait > 0) {
		usleep(PERIOD);
		if (thread_finished) {
			return 0;
		}
		wait-=PERIOD;
	}
	/* NOTE: if thread exits between check and
	 * cancel call - very bad things will happen */
	if (pthread_cancel(longcall_thread))
		return -1;
	return 1;
}

static void parent_sig_handler(int signum) {
	dout(conf.debug, "recieved signal %d", signum);

    switch(signum) {
    	case SIGCHLD:
			dout(conf.debug, "killing myself\n");
        	exit(EXIT_SUCCESS);
        	break;
    	case SIGTERM:
			dout(conf.debug, "killing myself\n");
        	exit(EXIT_SUCCESS);
        	break;
		case SIGALRM:
			dout(conf.debug, "killing myself\n");
        	exit(EXIT_SUCCESS);
        	break;
		case SIGUSR1:
			dout(conf.debug, "killing myself\n");
        	exit(EXIT_SUCCESS);
        	break;
    }
}

static void child_sig_handler(int signum) {
	dout(conf.debug, "recieved signal %d", signum);
	/* TODO implement handler */
    switch(signum) {
    	case SIGTERM:
			soft_exit(EXIT_SUCCESS);
        	break;
		case SIGALRM:
        	break;
		case SIGPIPE:
			syslog(LOG_ERR, "broken TCP stream\n");
        	break;
		case SIGUSR1:
			serve_usr1();
        	break;
		case SIGUSR2:
			serve_usr2();
        	break;
    }
}

static void daemonize() {
	pid_t pid, sid, parent;
	struct passwd *pw = NULL;
	int fd = -1;
	char lockpid[64];

	/* open logs */
	openlog(DAEMON_NAME, LOG_PID, LOG_LOCAL5);
	dout(conf.debug, "daemonizing...\n\n");
	/* already a daemon */
	dout(conf.debug, "checking if already a daemon\n");
    if ((pid = getppid()) == 1) {
		syslog(LOG_NOTICE, "already daemon, switching to tasks\n");
		return;
	}
	/* trap signals */
	dout(conf.debug, "trapping parent signals\n");
    if (signal(SIGCHLD, parent_sig_handler) == SIG_ERR)
		syslog(LOG_ERR, "error registering handler for SIGCHLD\n");
    if (signal(SIGTERM, parent_sig_handler) == SIG_ERR)
		syslog(LOG_ERR, "error registering handler for SIGTERM\n");
	if (signal(SIGALRM, parent_sig_handler) == SIG_ERR)
		syslog(LOG_ERR, "error registering handler for SIGALRM\n");
	if (signal(SIGUSR1, parent_sig_handler) == SIG_ERR)
		syslog(LOG_ERR, "error registering handler for SIGUSR1\n");
	/* fork off the parent process */
	dout(conf.debug, "forking off the parent\n");
	if ((pid = fork()) < 0) {
		syslog(LOG_ERR, "fork error\n");
		exit(EXIT_FAILURE);
	}
	/* exit the parent process */
	if (pid > 0) {
		dout(conf.debug, "waiting for child\n");
		alarm(3);
		pause();
		dout(conf.debug, "exiting parent\n");
		exit(EXIT_SUCCESS);
	}
	/* we are executing as child now */
    parent = getppid();
	/* change the file mode mask */
	dout(conf.debug, "changing file mask\n");
	umask(0);                
	/* create a new SID */
	dout(conf.debug, "creating new session\n");
	if ((sid = setsid()) < 0) {
		syslog(LOG_ERR, "failed creating new SID; exiting\n");
		exit(EXIT_FAILURE);
	}
	/* change working directory */
	dout(conf.debug, "changing working directory\n");
	if ((chdir("/")) < 0) {
		syslog(LOG_ERR, "failed changing directory; exiting\n");
		exit(EXIT_FAILURE);
	}  
	/* close file descriptors */
	dout(conf.debug, "closing file descriptors\n");
	if (close(STDIN_FILENO) != 0)
		syslog(LOG_ERR, "error closing STDIN_FILENO\n");
	if (close(STDOUT_FILENO) != 0)
		syslog(LOG_ERR, "error closing STDOUT_FILENO\n");
	if (close(STDERR_FILENO) != 0)
		syslog(LOG_ERR, "error closing STDERR_FILENO\n");
	/* create lock file */
	dout(conf.debug, "creating lock file\n");
	if ((fd = open(LOCK_FILE, O_RDWR | O_CREAT, 0640)) < 0) {
		syslog(LOG_ERR, "unable to create lock file\n");
		exit(EXIT_FAILURE);
	}
	if (lockf(fd, F_TLOCK, 0) < 0) {
		syslog(LOG_NOTICE, "already running, exiting");
		exit(EXIT_SUCCESS);
	}
	sprintf(lockpid, "%d\n", getpid());
	write(fd, lockpid, strlen(lockpid));
	/* drop user if there is one */
	dout(conf.debug, "changing user\n");
    if (getuid() == 0 || geteuid() == 0) {
        if ((pw = getpwnam(RUN_AS_USER)) != NULL) {
            dout(conf.debug, "setting user to " RUN_AS_USER);
            if (setuid(pw->pw_uid) != 0)
				syslog(LOG_ERR, "error seting uid\n");
        } else {
			syslog(LOG_NOTICE, "cannot set user to " RUN_AS_USER);
		}
    } else {
		syslog(LOG_NOTICE, "we have no root privilegies\n");
	}
	/* register child signal handlers */
	dout(conf.debug, "trapping child signals\n");
	if (signal(SIGTERM, child_sig_handler) == SIG_ERR)
		syslog(LOG_ERR, "error registering handler for SIGTERM\n");
	if (signal(SIGALRM, child_sig_handler) == SIG_ERR)
		syslog(LOG_ERR, "error registering handler for SIGALRM\n");
	if (signal(SIGUSR1, child_sig_handler) == SIG_ERR)
		syslog(LOG_ERR, "error registering handler for SIGUSR1\n");
	if (signal(SIGUSR2, child_sig_handler) == SIG_ERR)
		syslog(LOG_ERR, "error registering handler for SIGUSR2\n");
	if (signal(SIGHUP, SIG_IGN) == SIG_ERR)
		syslog(LOG_ERR, "error registering handler for SIGHUP\n");
    if (signal(SIGCHLD, SIG_IGN) == SIG_ERR)
		syslog(LOG_ERR, "error registering handler for SIGCHLD\n");
    if (signal(SIGTSTP, SIG_IGN) == SIG_ERR)
		syslog(LOG_ERR, "error registering handler for SIGTSTP\n");
    if (signal(SIGTTOU, SIG_IGN) == SIG_ERR)
		syslog(LOG_ERR, "error registering handler for SIGTTOU\n");
    if (signal(SIGTTIN, SIG_IGN) == SIG_ERR)
		syslog(LOG_ERR, "error registering handler for SIGTTIN\n");
	/* tell parent that we are ok*/
	dout(conf.debug, "killing parent\n");
    if (kill(parent, SIGUSR1) != 0)
		syslog(LOG_ERR, "error sending SIGUSR1 to parent\n");
	dout(conf.debug, "daemonization completed\n");
}

static void run_atd(void) {
	char address[BSIZE];
	char timeout_msg[] = "timeout";
	char error_msg[] = "error (check atd log on router)";
	int rcount, ret;
	size_t load, sent;
	socklen_t clilen;
	struct sockaddr_in serv_addr, cli_addr;
	
	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		syslog(LOG_ERR, "error creating socket\n");
		soft_exit(EXIT_FAILURE);
	}
	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(conf.port);
	
	if(bind(sockfd, (struct sockaddr *)&serv_addr,sizeof(serv_addr)) < 0) {
		close(sockfd);
		syslog(LOG_ERR, "error binding socket\n");
		soft_exit(EXIT_FAILURE);
	}
	inet_ntop(AF_INET, &(serv_addr.sin_addr), address, BSIZE);
	dout(conf.debug, "socket created: addr=%s port=%d", 
		 address, ntohs(serv_addr.sin_port));
	listen(sockfd, 5);
    clilen = sizeof(cli_addr);
	syslog(LOG_NOTICE, "listening for clients\n");

	do {
		memset(rxbuf,'\x0', BSIZE);
		memset(txbuf,'\x0', BSIZE);
		
		if ((newsockfd = accept(sockfd,(struct sockaddr *)&cli_addr, &clilen)) == -1) {
			syslog(LOG_ERR, "failed to accept connection\n");
			continue;
		}
		inet_ntop(AF_INET, &(cli_addr.sin_addr), address, BSIZE);
		dout(conf.debug, "connection from: addr=%s port=%d", 
			address, ntohs(cli_addr.sin_port));
		rcount = read(newsockfd, rxbuf, BSIZE);

		if (rcount > 0) {
			/* route AT command here */
			dout(conf.debug, "recieved from client: \"%s\"\n", rxbuf);
			/* executing call to gsmd using timeout implemented
			 via pthread with polling */
			ret = run_or_timeout();
			thread_finished = 0;
			dout(conf.debug, "got from gsmd \"%s\" count=%d\n", txbuf, rlen);
			
			if (ret == 1) {
				/* NOTE: not sure if reinit is really neccesary,
				 * it kind helped several times */
				reinit_gsmd();
				syslog(LOG_ERR, "request to gsmd has timed out\n");
				strncpy(txbuf, timeout_msg, sizeof(timeout_msg));
				rlen = sizeof(timeout_msg);
			} else if (atret <= 0 || ret == -1) {
				syslog(LOG_ERR, "error handling gsmd output\n");
				strncpy(txbuf, error_msg, sizeof(error_msg));
				rlen = sizeof(error_msg);
			}
		} else {
			syslog(LOG_ERR, "socket read error, dropping connection\n");
			close(newsockfd);
			continue;
		}
		/* send doesnt terminate deamon if socket is closed */
		if (rlen > 0) {
			sent = 0;
			do  {
					/* NOTICE: non-blocking send may cause some troubles */
					sent += send(newsockfd, txbuf + sent, 
						rlen - sent, MSG_DONTWAIT);
				} while (sent < rlen);
			dout(conf.debug, "client satisfied, closing connection\n");
		} else {
			dout(conf.debug, "no data to send, dropping connection");
		}
		close(newsockfd);
	} while(1);
}

int main(int argc, char **argv)
{
	int c, option_index = 0;
	
	static struct option long_options[] =
	{
		{"port",		optional_argument,		0,	'p'},
		{"timeout",		optional_argument,		0,	't'},
		{"debug",		no_argument,			0,	'd'},
		{"help",		no_argument,			0,	'h'},
	};
	
	/* init config to defaults */
	init_config();

	/* parse CLI arguments */
	while (1)
	{
		if ((c = getopt_long_only(argc, argv, 
				"p:t:dh", 
				long_options, &option_index)) == -1)
			break;
     
		switch (c)
		{
			case 'p':
				/* FIXME: somehow optarg becomes NULL on long option. 
				 * Have no fucking clue why. It shoud work with 
				 * long options too. */
				if (optarg == NULL) {
					fprintf(stdout, 
						"%s: cannot parse CLI argument\n", 
						__FILE__);
					exit(0);
				}
				conf.port = atoi(optarg);
				if (conf.port < 1 || conf.port > 65535) {
					fprintf(stdout, 
						"%s: invalid port number \"%d\" (valid 1 - 65535)\n", 
						__FILE__, conf.port);
					exit(0);
				}
				break;
			case 't':
				if (optarg == NULL) {
						fprintf(stdout, 
							"%s: cannot parse CLI argument\n", 
							__FILE__);
						exit(0);
				}
				conf.timeout = atoi(optarg);
				if (conf.timeout < 1 || conf.timeout > 1000) {
						fprintf(stdout, 
							"%s: invalid timeout value \"%d\" (valid 1 - 1000)\n", 
							__FILE__, conf.timeout);
						exit(0);
				}
				break;
			case 'd':
				conf.debug = 1;
				break;
			case 'h':
				usage();
				exit(0);
			default:
				usage();
				exit(0);
		}
	}
	
	/* conventional daemonization */
	daemonize();
	
	/* initialization code */
	dout(conf.debug, "initializing daemon\n");
	if (init_atd()) {
		syslog(LOG_ERR, 
			"daemon initialization failed\n");
		closelog();
		exit(EXIT_FAILURE);
	}
	
	/* main daemon tasks */
	run_atd();
	
	/* we shoud not get here */
	return 0;
}
