/* required for multithreaded execution */
#define _REENTRANT
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <fcntl.h>
#include <unistd.h>
#include <syslog.h>
#include <errno.h>
#include <pwd.h>
#include <signal.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
/* Gobi API and uci API */
#include "GobiConnectionMgmtAPI.h"
#include "uci.h"

#define NAME "/var/lock/gobisocket"
#define LOCK_FILE "/var/lock/gobid"
#define DAEMON_NAME "gobid"
#define RUN_AS_USER "root"
#define EXIT_SUCCESS 0
#define EXIT_FAILURE 1
#define GENERIC_BUF_SIZE 64
#define UCI_ENTRY_SIZE 128
#define SOCK_BUF_SIZE 4096

typedef struct {
    char node[256];
    char key[16];
} __attribute__((packed)) qc_devices;

typedef struct {
    WORD	mcc;
    WORD 	mnc;
    ULONG	in_use;
    ULONG	roaming;
    ULONG	forbidden;
    ULONG	preferred;
    CHAR	description[255];
} __attribute__((packed)) net_info;

static char *hardware_state[] = {
    "on",
    "off",
    "restarting",
    "unknown"
};

static char *gobi_api_state[] = {
    "active",
    "innactive",
    "unknown"
};

static char *radio_interfaces[] = {
    "no service",
    "CDMA 1xRTT",
    "CDMA 1xEV-DO",
    "AMPS (unsupported)",
    "GSM",
    "UMTS"
};

static char *session_state[] = {
    "unknown",
    "disconnected",
    "connected",
    "suspended",
    "authenticating"
};

static char *pin_state[] = {
    "not initialized",
    "enabled, not verified",
    "enabled, verified",
    "disabled",
    "blocked",
    "permanently blocked",
	"unknown"
};

static char *power_state[] = {
    "online",
    "low power mode",
    "factory test mode",
    "offline",
    "reset",
    "power off",
    "persistent low power mode"
};

static char *networks[] = {
    "unknown",
    "current serving network",
    "not current serving network, available"
};

static char *roaming[] = {
    "unknown",
    "home",
    "roam"
};

static char *forbidden[] = {
    "unknown",
    "forbidden",
    "not forbidden"
};

static char *preferred[] = {
    "unknown",
    "preferred",
    "not preferred"
};

static char *registration_state[] = {
    "not registered",
    "registered",
    "searching",
    "denied",
    "unknown"
};

static char *domain_state[] = {
    "unknown",
    "attached",
    "detached"
};

static char *ran_type[] = {
    "unknown",
    "CDMA 2000",
    "UMTS"
};

static char *radiotech_type[] = {
    "no service",
    "CDMA 1xRTT",
    "CDMA 1xEV-DO",
    "AMPS",
    "GSM",
    "UMTS"
};

static char *data_bearer[] = {
    "unknown",
    "CDMA 1xRTT",
    "CDMA 1xEV-DO Rev 0",
    "GPRS",
    "WCDMA",
    "CDMA 1xEV-DO Rev A",
    "EDGE",
    "HSDPA DL, WCDMA UL",
    "WCDMA DL, HSUPA UL",
    "HSDPA DL, HSUPA UL"
};

static char *roam_state[] = {
    "roaming",
    "home",
    "roaming partner"
};

static char *wan_mode[] = {
    "3g",
	"3g as backup",
    "not 3g",
	"unknown"
};

static char *last_entered_pin[] = {
    "correct",
	"incorrect",
    "unknown"
};

static char *update_condition[] = {
    "success",
	"hardware off",
    "api inncactive",
	"no sim",
	"link unregistered",
	"ds disconnected"
};

static char *autoconnection_mode[] = {
    "disabled",
	"enabled",
    "paused",
	"unknown"
};

static char *sim_state[] = {
    "inserted",
	"not inserted",
	"unknown"
};

/* stores UCI configuration from /etc/config */
typedef struct global_config {
	char pin[UCI_ENTRY_SIZE];
	char apn[UCI_ENTRY_SIZE];
	char user[UCI_ENTRY_SIZE];
	char password[UCI_ENTRY_SIZE];
	char auth_mode[UCI_ENTRY_SIZE];
	char net_mode[UCI_ENTRY_SIZE];	
} global_config;

/* stores various status data for modem and environment */
typedef struct global_status {
	/* generic */
	ULONG wan_mode;
	ULONG hw_state;
	ULONG api_state; 
	/* device */
	char manufacturer[GENERIC_BUF_SIZE];
	char model_id[GENERIC_BUF_SIZE];
	char fw_rev[GENERIC_BUF_SIZE];
	char hw_rev[GENERIC_BUF_SIZE];
	char imei[GENERIC_BUF_SIZE];
	/* sim */
	ULONG sim_state;
	char imsi[GENERIC_BUF_SIZE];
	char iccid[GENERIC_BUF_SIZE];
	/* pin */
	ULONG pin_state;
	ULONG pin_rleft;
	ULONG pin_uleft;
	ULONG pin_last;
	/* registration */
	ULONG reg_state;
	char net_name[GENERIC_BUF_SIZE];
	char iface[GENERIC_BUF_SIZE];
	INT8 signal;
	ULONG ran;
	char net_pref[GENERIC_BUF_SIZE];
	ULONG roaming;
	ULONG cs_dom;
	ULONG ps_dom;
	WORD mcc;
	WORD mnc;
	/* data session */
	ULONG ds_state;
	ULONG autoconn_mode;
	ULONG bearer;
	ULONG ds_max_tx;
	ULONG ds_max_rx;
	ULONG cur_tx;
	ULONG cur_rx;
	ULONG pack_tx_ok;
	ULONG pack_tx_fail;
	ULONG pack_rx_ok;
	ULONG pack_rx_fail;
	ULONG pack_tx_over;
	ULONG pack_rx_over;
	ULONGLONG tx_bytes;
	ULONGLONG rx_bytes;
	char ip[GENERIC_BUF_SIZE];
	char duration[GENERIC_BUF_SIZE];	
} global_status;

/* globals shared between main and server threads */
ULONG session_id = 0;
global_status modem_status;
global_config modem_configuration;
pthread_mutex_t work_mutex;
static unsigned int stall_period = 0;
static char uci_search_result[UCI_ENTRY_SIZE];

/* utility functions */
int is_set(char *str);
void usage(char* ret_buf);
static void sig_handler(int signum);
static void daemonize(const char *lockfile);
void parse_preference(ULONG pref, char* ret_buf);
/* gobi API related */
int init_gapi(void);
int close_gapi(void);
int is_gapi(void);
/* modem state changers */
int set_power_mode(ULONG mode); 
int set_preference(char *mode);
int persist_preference(global_config *gconfig);
int auto_register(void);
int link_register(global_config *gconfig);
int man_connect(char *argvn);
int ds_disconnect(void);
int ds_connect(global_config *gconfig);
int enable_pin(char *pin, global_status *gstatus);
int disable_pin(char *pin, global_status *gstatus);
int enter_pin(char *pin, global_status *gstatus);
int unblock_pin(char *puk, char *pin, global_status *gstatus);
int set_pin(global_config *gconfig, global_status *gstatus);
/* countermeasure functions */
int hard_on(void);
int hard_off(void);
int soft_reset(void);
int dhcp_renew(void);
int abort_request(void);
/* status gathering functions */
int scan_network(char* ret_buf);
int get_wan_mode(void);
int get_hardware_state(void);
int update_wan_mode(global_status *gstatus);
int update_hw_state(global_status *gstatus);
int update_api_state(global_status *gstatus);
int update_manufac(global_status *gstatus);
int update_model(global_status *gstatus);
int update_fw_rev(global_status *gstatus);
int update_hw_rev(global_status *gstatus); 
int update_imei(global_status *gstatus);
int update_simstate(global_status *gstatus);
int update_imsi(global_status *gstatus);
int update_iccid(global_status *gstatus);
int update_pin(global_status *gstatus);
int update_preference(global_status *gstatus);
int update_reginfo(global_status *gstatus);
int update_signal(global_status *gstatus);
int update_session_state(global_status *gstatus);
int update_autoconn_mode(global_status *gstatus);
int update_bearer(global_status *gstatus);
int update_rates(global_status *gstatus);
int update_bytes(global_status *gstatus);
int update_packets(global_status *gstatus);
int update_ip(global_status *gstatus);
int update_duration(global_status *gstatus); 
int update_status_total(global_status *gstatus);
/* UCI config related */
int load_config(global_config *gconfig);
char *get_config_entry(struct uci_context *ctx, char *entry_name);
/* server thread function */
void *server_thread_function(void *arg);
/* other */
int export_status(global_status *gstatus, char *ret_buf);
int export_config(global_config *gconfig, char *ret_buf);
void maintain_link(global_config *gconfig, global_status *gstatus);

int main(int argc, char *argv[]) {
	int ret;
	unsigned int sp;
	pthread_t server_thread;
    pthread_attr_t thread_attr;

    if (argc != 1)
        printf("gobid: all arguments will be ignored\n");
	
    /* open logging service */
	openlog(DAEMON_NAME, LOG_PID, LOG_LOCAL5);
	syslog(LOG_NOTICE, "starting...\n");
	
	/* daemonize */
    daemonize("/var/lock/gobid");
	syslog(LOG_NOTICE, "daemonized\n");
	
	/* load config from UCI */
	if (load_config(&modem_configuration))
		syslog(LOG_NOTICE, "failed to load config on start, using defaults\n");
	/* init Gobi API */
	if (ret = init_gapi()) {
		syslog(LOG_ERR, "failed to init api on start (%d)\n", ret);
	} else {
		/* check sim status */
		if (ret = update_simstate(&modem_status))
			syslog(LOG_NOTICE, "failed to update sim state on start (%d)\n", ret);
		/* do not register and connect if no sim */
		if (modem_status.sim_state == 1) {
			syslog(LOG_ERR, "sim is not inserted on start (%d)\n", ret);
		} else {
			/* 	set initial last attemt state to unknown */
			modem_status.pin_last = 2;
			/* enter pin if required */
			if (ret = update_pin(&modem_status))
				syslog(LOG_NOTICE, "failed to update pin state on start (%d)\n", ret);
			if (modem_status.pin_state == 4 ||
				modem_status.pin_state == 5) {
				syslog(LOG_ERR, "sim is blocked or rejected\n");
			} else if (modem_status.pin_state == 1) {
				if (!is_set(modem_configuration.pin)) {
					syslog(LOG_ERR, "pin is required, but not provided\n");
				} else {
					ret = set_pin(&modem_configuration, &modem_status);
					if (ret == 0) {
						syslog(LOG_NOTICE, "pin entered on start (%d)\n", ret);
						sleep(2);
						/* perform initial autoregistration */
						if (ret = link_register(&modem_configuration))
							syslog(LOG_NOTICE, "failed to register on start (%d)\n", ret);
						sleep(3);
						if (ret = update_session_state(&modem_status))
							syslog(LOG_NOTICE, "failed to update session state on start (%d)\n", ret);
						/* remove autoconnection mode */
						set_autoconn(0);
						/* set preference */
						persist_preference(&modem_configuration);
						sleep(4);
						/* disconnect if connected */
						if (modem_status.ds_state == 2) {
							if(ret = ds_disconnect())
								syslog(LOG_NOTICE, "failed to disconnect on start (%d)\n", ret);
							sleep(2);
						}
						/* force connect if 3G mode */
						if (modem_status.wan_mode != 2) {
							if (ret = ds_connect(&modem_configuration))
								syslog(LOG_NOTICE, "failed to connect on start (%d)\n", ret);
							sleep(2);
						}
					} else if (ret = 1012) {
						syslog(LOG_ERR, "pin is incorrect on start (%d)\n", ret);
					} else {
						syslog(LOG_ERR, "failed to enter pin on start (%d)\n", ret);
					}
				}
			} else {
				/* perform initial autoregistration */
				if (ret = link_register(&modem_configuration))
					syslog(LOG_NOTICE, "failed to register on start (%d)\n", ret);
				sleep(3);
				if (ret = update_session_state(&modem_status))
					syslog(LOG_NOTICE, "failed to update session state on start (%d)\n", ret);
				/* remove autoconnection mode */
				set_autoconn(0);
				/* set preference */
				persist_preference(&modem_configuration);
				sleep(4);
				/* disconnect if connected */
				if (modem_status.ds_state == 2) {
					if(ret = ds_disconnect())
						syslog(LOG_NOTICE, "failed to disconnect on start (%d)\n", ret);
					sleep(2);
				}
				/* force connect if 3G mode */
				if (modem_status.wan_mode != 2) {
					if (ret = ds_connect(&modem_configuration))
						syslog(LOG_NOTICE, "failed to connect on start (%d)\n", ret);
					sleep(2);
				}
			}	
		}
		/* perform final status update */
		if (ret = update_status_total(&modem_status))
			syslog(LOG_NOTICE, "status updated on start with condition (%s)\n", 
			update_condition[ret]);
	}
	/* create server thread */
    if (ret = pthread_mutex_init(&work_mutex, NULL)) {
		syslog(LOG_ERR, "failed to initialize mutex (%d)\n", ret);
        exit(EXIT_FAILURE);
    }
    
    if (ret = pthread_attr_init(&thread_attr)) {
		syslog(LOG_ERR, "failed to create attribute (%d)\n", ret);
        exit(EXIT_FAILURE);
    }
    if (ret = pthread_attr_setdetachstate(&thread_attr, 
	PTHREAD_CREATE_DETACHED)) {
		syslog(LOG_ERR, "failed to set detached attribute (%d)\n", ret);
        exit(EXIT_FAILURE);
    }
    if (ret = pthread_create(&server_thread, &thread_attr, 
		server_thread_function, (void *)&modem_status)) {
		syslog(LOG_ERR, "failed to create server thread (%d)\n", ret);
        exit(EXIT_FAILURE);
    }
    (void)pthread_attr_destroy(&thread_attr);
	syslog(LOG_NOTICE, "server thread created, changing main thread to polling state\n");
    
    do {
		pthread_mutex_lock(&work_mutex);
		sp = stall_period;
		pthread_mutex_unlock(&work_mutex);
		/* stall if required */
		sleep(sp);
		/* do link maintenance here */
		pthread_mutex_lock(&work_mutex);
		update_status_total(&modem_status);
		pthread_mutex_unlock(&work_mutex);
		maintain_link(&modem_configuration, &modem_status);
    } while(1);
    exit(EXIT_SUCCESS);
}

void usage(char* ret_buf) {
    sprintf(&ret_buf[strlen(ret_buf)],"\t(commands)\n\
\tget - get status or specified parameter\n\
\t\t[all | wanmode | hwstate | apistate | manufacturer\n\
\t\tmodel | imei | simstate | iccid | imsi | pinstate\n\
\t\tpinrleft | pinuleft | pinlast | preference | regstate\n\
\t\tnetname | iface | signal | ran | dstate | autoconn | bearer\n\
\t\tip | duration | pktx | pkrx | bytestx | bytesrx | id | stall]\n\
\thw - change modem hardware state\n\
\t\t[on | off | hardreset | softreset]\n\
\tapi - change Gobi API state\n\
\t\t[bind | unbind | rebind]\n\
\tpin - perform specified action on pin\n\
\t\t[enter <pin> | enable <pin>\n\
\t\tdisable <pin> | unblock <puk> <pin>]\n\
\tprefer - change prefered network type\n\
\t\t[gsm | umts | auto]\n\
\tregister - initiate network registration\n\
\t\t[auto | manual <mcc> <mnc> <network>]\n\
\tpowermode <mode> - set modem power mode\n\
\tnetscan - perform a network scan\n\
\tconnect <apn> <user> <pass> <auth> - set up a data session\n\
\tautoconnect - switch connection mode\n\
\t\t[on | off]\n\
\tdisconnect - stop data session\n\
\tconfig - perform action on UCI configuration\n\
\t\t[show | load]\n\
\tstall <secs> - suspend polling for specified amount of time\n\
\trenew - send DHCP renew request\n\
\tcancel - cancel last request/call to gobi API\n\
\tkill - kill daemon\n");
}

void *server_thread_function(void *arg) {
	int sockfd, newsockfd, n, i, ret;
	socklen_t clilen;
	struct global_status *status;
	struct global_config *config;
	struct sockaddr_in serv_addr, cli_addr;
	size_t load, sent, left;
	/* buffers */
	char buf[4*GENERIC_BUF_SIZE];
	/* socket command arguments */
	char argv1[GENERIC_BUF_SIZE]; 
	char argv2[GENERIC_BUF_SIZE];
	char argv3[GENERIC_BUF_SIZE]; 
	char argv4[GENERIC_BUF_SIZE];
	char argv5[GENERIC_BUF_SIZE]; 
	char argv6[GENERIC_BUF_SIZE];
	char argv7[GENERIC_BUF_SIZE]; 
	char ret_buf[SOCK_BUF_SIZE];
	status = (global_status *)arg;
	/* can not pass this via args */
	config = &modem_configuration;
	
	for (i = 0; i <= 2; i ++) {
        if ((sockfd = socket(AF_INET,SOCK_STREAM,0)) < 0)
            syslog(LOG_NOTICE, "error creating socket\n");
        bzero((char *) &serv_addr, sizeof(serv_addr));
        serv_addr.sin_family = AF_INET;
        serv_addr.sin_addr.s_addr = INADDR_ANY;
        serv_addr.sin_port = htons(777);

        if(bind(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr)) < 0) {
            close(sockfd);
            unlink(NAME);
        } else if (i==2) {
            close(sockfd);
            unlink(NAME);
            syslog(LOG_NOTICE, "error binding  socket\n");
        } else {
            break;
        }
	}
	listen(sockfd, 5);
    clilen = sizeof(cli_addr);

	do {
        newsockfd = accept(sockfd,(struct sockaddr *)&cli_addr, &clilen);

		if (newsockfd == -1) {
            syslog(LOG_NOTICE, "failed to accept connection\n");
            continue;
        }
        n = read(newsockfd, buf, 4*GENERIC_BUF_SIZE);
      
        if (n>0) {
            buf[n]='\x0';
            sscanf(buf,"%s %s %s %s %s %s %s", 
				argv1, argv2, argv3, argv4, argv5, argv6, argv7);
			pthread_mutex_lock(&work_mutex);
			/* passive operations */
			if(strcmp("get", argv1) == 0) {
                if(strlen(argv2) == 0) {
                    sprintf(&ret_buf[strlen(ret_buf)],
						"gobid: get: argument is required\n");
                } else if (strcmp("all", argv2) == 0) {
					update_status_total(status);
					export_status(status, ret_buf);
				} else if (strcmp("wanmode", argv2) == 0) {
					if (ret = update_wan_mode(status))
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
					else
						sprintf(&ret_buf[strlen(ret_buf)],
							"%s\n",wan_mode[status->wan_mode]);
				} else if(strcmp("id", argv2) == 0) {
					sprintf(&ret_buf[strlen(ret_buf)],
							"%d\n",session_id);
				} else if (strcmp("hwstate", argv2) == 0) {
					if (ret = update_hw_state(status))
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
					else
						sprintf(&ret_buf[strlen(ret_buf)],"%s\n",
							hardware_state[status->hw_state]);
				} else if (strcmp("apistate", argv2) == 0) {
					if (ret = update_api_state(status))
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
					else
						sprintf(&ret_buf[strlen(ret_buf)],"%s\n",
							gobi_api_state[status->api_state]);
				} else if (strcmp("manufacturer", argv2) == 0) {
					if (ret = update_manufac(status))
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
					else
						sprintf(&ret_buf[strlen(ret_buf)],"%s\n",
							status->manufacturer);
				} else if (strcmp("model", argv2) == 0) {
					if (ret = update_model(status))
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
					else
						sprintf(&ret_buf[strlen(ret_buf)],
							"%s\n",status->model_id);
				} else if (strcmp("imei", argv2) == 0) {
					if (ret = update_imei(status))
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
					else
						sprintf(&ret_buf[strlen(ret_buf)],
							"%s\n",status->imei);
				} else if (strcmp("iccid", argv2) == 0) {
					if (ret = update_iccid(status))
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
					else
						sprintf(&ret_buf[strlen(ret_buf)],
							"%s\n",status->iccid);
				} else if (strcmp("imsi", argv2) == 0) {
					if (ret = update_imsi(status))
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
					else
						sprintf(&ret_buf[strlen(ret_buf)],
							"%s\n",status->imsi);
				} else if (strcmp("simstate", argv2) == 0) {
					if (ret = update_simstate(status))
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
					else
						sprintf(&ret_buf[strlen(ret_buf)],
							"%s\n",sim_state[status->sim_state]);
				} else if (strcmp("pinstate", argv2) == 0) {
					if (ret = update_pin(status))
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
					else 
						sprintf(&ret_buf[strlen(ret_buf)],"%s\n",
							pin_state[status->pin_state]);
				} else if (strcmp("pinrleft", argv2) == 0) {
					if (ret = update_pin(status))
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
					else 
						sprintf(&ret_buf[strlen(ret_buf)],
							"%lu\n",status->pin_rleft);
				} else if (strcmp("pinuleft", argv2) == 0) {
					if (ret = update_pin(status))
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
					else 
						sprintf(&ret_buf[strlen(ret_buf)],
							"%lu\n",status->pin_uleft);
				} else if (strcmp("pinlast", argv2) == 0) {
					sprintf(&ret_buf[strlen(ret_buf)],"%s\n",
						last_entered_pin[status->pin_last]);
				} else if (strcmp("regstate", argv2) == 0) {
					if (ret = update_reginfo(status))
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
					else
						sprintf(&ret_buf[strlen(ret_buf)],"%s\n",
							registration_state[status->reg_state]);
				} else if (strcmp("netname", argv2) == 0) {
					if (ret = update_reginfo(status))
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
					else
						sprintf(&ret_buf[strlen(ret_buf)],"%s\n",
							status->net_name);
				} else if (strcmp("iface", argv2) == 0) {
					if (ret = update_reginfo(status))
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
					else
						sprintf(&ret_buf[strlen(ret_buf)],"%s\n",
							status->iface);
				} else if (strcmp("signal", argv2) == 0) {
					if (ret = update_signal(status))
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
					else
						sprintf(&ret_buf[strlen(ret_buf)],"%d\n",
							status->signal);
				} else if (strcmp("preference", argv2) == 0) {
					if (ret = update_preference(status))
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
					else
						sprintf(&ret_buf[strlen(ret_buf)],"%s\n", 
							status->net_pref);
				} else if (strcmp("ran", argv2) == 0) {
					if (ret = update_reginfo(status))
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
					else
						sprintf(&ret_buf[strlen(ret_buf)],"%s\n",
							ran_type[status->ran]);
				} else if (strcmp("dstate", argv2) == 0) {
					if (ret = update_session_state(status))
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
					else
						sprintf(&ret_buf[strlen(ret_buf)],"%s\n",
							session_state[status->ds_state]);
				} else if (strcmp("autoconn", argv2) == 0) {
					if (ret = update_autoconn_mode(status))
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
					else
						sprintf(&ret_buf[strlen(ret_buf)],"%s\n",
							autoconnection_mode[status->autoconn_mode]);
				} else if (strcmp("bearer", argv2) == 0) {
					if (ret = update_bearer(status))
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
					else
						sprintf(&ret_buf[strlen(ret_buf)],"%s\n",
							data_bearer[status->bearer]);
				} else if (strcmp("ip", argv2) == 0) {
					if (ret = update_ip(status))
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
					else
						sprintf(&ret_buf[strlen(ret_buf)],"%s\n",
							status->ip);
				} else if (strcmp("duration", argv2) == 0) {
					if (ret = update_duration(status))
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
					else
						sprintf(&ret_buf[strlen(ret_buf)],"%s\n",
							status->duration);
				} else if (strcmp("pktx", argv2) == 0) {
					if (ret = update_packets(status))
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);	
					else
						sprintf(&ret_buf[strlen(ret_buf)],"%lu\n",
							status->pack_tx_ok);
				} else if (strcmp("pkrx", argv2) == 0) {
					if (ret = update_packets(status))
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);	
					else
						sprintf(&ret_buf[strlen(ret_buf)],"%lu\n",
							status->pack_rx_ok);
				} else if (strcmp("bytestx", argv2) == 0) {
					if (ret = update_bytes(status))
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);	
					else
						sprintf(&ret_buf[strlen(ret_buf)],"%lu\n",
							status->tx_bytes);
				} else if (strcmp("bytesrx", argv2) == 0) {
					if (ret = update_bytes(status))
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);	
					else
						sprintf(&ret_buf[strlen(ret_buf)],"%lu\n",
							status->rx_bytes);
				} else if (strcmp("stall", argv2) == 0) {
					sprintf(&ret_buf[strlen(ret_buf)],
							"%d\n", stall_period);
				} else {
					sprintf(&ret_buf[strlen(ret_buf)],
						"gobid: get: incorrect argument\n");
				}
			/* active operations */
			} else if (strcmp("hw", argv1) == 0) {
                if(strlen(argv2) == 0) {
                    sprintf(&ret_buf[strlen(ret_buf)],
						"gobid: hw: argument is required\n");
                } else if (strcmp("on", argv2) == 0) {
					if (ret = hard_on())
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
					else
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation completed (%d)\n", ret);
				} else if (strcmp("off", argv2) == 0) {
					if (ret = hard_off())
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
					else
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation completed (%d)\n", ret);
				} else if (strcmp("hardreset", argv2) == 0) {
					if (ret = hard_off()) {
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
					} else {
						if (ret = hard_on())
							sprintf(&ret_buf[strlen(ret_buf)],
								"gobid: operation failed (%d)\n", ret);
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation completed (%d)\n", ret);
					}
				} else if (strcmp("softreset", argv2) == 0) {
					if (ret = soft_reset())
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
					else
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation completed (%d)\n", ret);
				} else {
					sprintf(&ret_buf[strlen(ret_buf)],
						"gobid: hw: incorrect argument\n");
				}
			} else if(strcmp("api", argv1) == 0) {
                if(strlen(argv2) == 0) {
                    sprintf(&ret_buf[strlen(ret_buf)],
						"gobid: api: argument is required\n");
                } else if (strcmp("bind", argv2) == 0) {
					if (ret = init_gapi())
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
					else
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation completed (%d)\n", ret);
				} else if (strcmp("unbind", argv2) == 0) {
					if (ret = close_gapi())
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
					else
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation completed (%d)\n", ret);
				} else if (strcmp("rebind", argv2) == 0) {
					if (ret = close_gapi()) {
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
					} else {
						if (ret = init_gapi())
							sprintf(&ret_buf[strlen(ret_buf)],
								"gobid: operation failed (%d)\n", ret);
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation completed (%d)\n", ret);
					}
				} else {
					sprintf(&ret_buf[strlen(ret_buf)],
						"gobid: api: incorrect argument\n");
				}
			} else if(strcmp("prefer", argv1) == 0) {
                if(strlen(argv2) == 0) {
                    sprintf(&ret_buf[strlen(ret_buf)],
						"gobid: prefer: argument is required\n");
                } else if (strcmp("gsm", argv2) == 0) {
					if (ret = set_preference("gsm"))
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
					else
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation completed (%d)\n", ret);
				} else if (strcmp("umts", argv2) == 0) {
					if (ret = set_preference("umts"))
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
					else
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation completed (%d)\n", ret);
				} else if (strcmp("auto", argv2) == 0) {
					if (ret = set_preference("auto"))
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
					else
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation completed (%d)\n", ret);
				} else {
					sprintf(&ret_buf[strlen(ret_buf)],
						"gobid: prefer: incorrect argument\n");
				}
			} else if(strcmp("pin", argv1) == 0) {
                if(strlen(argv2) == 0) {
                    sprintf(&ret_buf[strlen(ret_buf)],
						"gobid: pin: argument is required\n");
                } else if (strcmp("enter", argv2) == 0) {
					ret = enter_pin(argv3, status);
					if (ret == 0)
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation completed (%d)\n", ret);
					else if (ret == 1012)
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: incorrect pin (%d)\n", ret);
					else
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);	
				} else if (strcmp("enable", argv2) == 0) {
					if (ret = enable_pin(argv3, status))
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
					else
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation completed (%d)\n", ret);
				} else if (strcmp("disable", argv2) == 0) {
					ret = disable_pin(argv3, status);
					if (ret == 0)
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation completed (%d)\n", ret);
					else if (ret == 1012)
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: incorrect pin (%d)\n", ret);
					else
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
				} else if (strcmp("unblock", argv2) == 0) {
					if (ret = unblock_pin(argv3, argv4, status))
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
					else
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation completed (%d)\n", ret);
				} else {
					sprintf(&ret_buf[strlen(ret_buf)],
						"gobid: pin: incorrect argument\n");
				}
			} else if(strcmp("register", argv1) == 0) {
                if(strlen(argv2) == 0) {
                    sprintf(&ret_buf[strlen(ret_buf)],
						"gobid: register: argument is required\n");
                } else if (strcmp("auto", argv2) == 0) {
					if (ret = auto_register())
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
					else
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation completed (%d)\n", ret);
				} else if (strcmp("manual", argv2) == 0) {
					if (ret = manual_register(atoi(argv3), atoi(argv4), argv5))
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
					else
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation completed (%d)\n", ret);
				} else {
					sprintf(&ret_buf[strlen(ret_buf)],
						"gobid: register: incorrect argument\n");
				}
			} else if(strcmp("powermode", argv1) == 0) {
                if(strlen(argv2) == 0) {
					sprintf(&ret_buf[strlen(ret_buf)],
						"gobid: powermode: argument is required\n");
                } else if (strlen(argv2) > 0) {
					if (ret = set_power_mode(atoi(argv2)))
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
					else
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation completed (%d)\n", ret);
				} else {
					sprintf(&ret_buf[strlen(ret_buf)],
						"gobid: powermode: incorrect argument\n");
				}
			} else if(strcmp("connect", argv1) == 0) {
				memset(argv7,'\x0',sizeof(argv7));
				sprintf(&argv7[strlen(argv7)],
						"%s %s %s %s", argv2, argv3, argv4, argv5);
				if (ret = man_connect(argv7))
					sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
				else
					sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation completed (%d)\n", ret);
			} else if(strcmp("autoconnect", argv1) == 0) {
                if(strlen(argv2) == 0) {
                    sprintf(&ret_buf[strlen(ret_buf)],
						"gobid: autoconnect: argument is required\n");
                } else if (strcmp("on", argv2) == 0) {
					if (ret = set_autoconn(1))
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
					else
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation completed (%d)\n", ret);
				} else if (strcmp("off", argv2) == 0) {
					if (ret = set_autoconn(0))
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
					else
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation completed (%d)\n", ret);
				} else {
					sprintf(&ret_buf[strlen(ret_buf)],
						"gobid: autoconnect: incorrect argument\n");
				}
			} else if(strcmp("disconnect", argv1) == 0) {
				if (ret = ds_disconnect())
					sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
				else
					sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation completed (%d)\n", ret);
			} else if(strcmp("config", argv1) == 0) {
                if(strlen(argv2) == 0) {
                    sprintf(&ret_buf[strlen(ret_buf)],
						"gobid: config: argument is required\n");
                } else if (strcmp("show", argv2) == 0) {
					if (ret = export_config(config, ret_buf))
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
				} else if (strcmp("load", argv2) == 0) {
					if (ret = load_config(config))
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
					else
						sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation completed (%d)\n", ret);
				} else {
					sprintf(&ret_buf[strlen(ret_buf)],
						"gobid: config: incorrect argument\n");
				}
			} else if(strcmp("renew", argv1) == 0) {
				ret = dhcp_renew();
				sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation completed (%d)\n", ret);
			} else if(strcmp("cancel", argv1) == 0) {
				if (ret = abort_request())
					sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation failed (%d)\n", ret);
				else
					sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation completed (%d)\n", ret);
			} else if(strcmp("stall", argv1) == 0) {
                if(strlen(argv2) == 0) {
					sprintf(&ret_buf[strlen(ret_buf)],
						"gobid: stall: argument is required\n");
                } else if (strlen(argv2) > 0) {
					stall_period = atoi(argv2);
					sprintf(&ret_buf[strlen(ret_buf)],
							"gobid: operation completed (%d)\n", stall_period);
				} else {
					sprintf(&ret_buf[strlen(ret_buf)],
						"gobid: stall: incorrect argument\n");
				}
			} else if(strcmp("netscan", argv1) == 0) {
				scan_network(ret_buf);
    		} else if(strcmp("help", argv1) == 0) {
	    		usage(ret_buf);
            } else if((strcmp("kill", buf)==0)) {
				/* send msg to client */
				sprintf(&ret_buf[strlen(ret_buf)],
						"gobid: raising sigterm\n");
				send(newsockfd, ret_buf, strlen(ret_buf), 
					 MSG_NOSIGNAL | MSG_DONTWAIT);
				close(newsockfd);
				/* generate signal */
				raise(SIGTERM);
                break;
            } else {
                sprintf(&ret_buf[strlen(ret_buf)],
					"gobid: incorrect command\n");
            }
			pthread_mutex_unlock(&work_mutex);
            /* send doesnt terminate deamon if socket is closed */
            if ((load = strlen(ret_buf)) > 0) {
				sent = 0;
				do  {
						sent += send(newsockfd, ret_buf + sent, 
							load - sent, MSG_NOSIGNAL | MSG_DONTWAIT);
					} while (sent < load);
            }
			close(newsockfd);
            memset(ret_buf,'\x0',sizeof(ret_buf));
            memset(argv1,'\x0',sizeof(argv1));
            memset(argv2,'\x0',sizeof(argv2));
			memset(argv3,'\x0',sizeof(argv3));
            memset(argv4,'\x0',sizeof(argv4));
			memset(argv5,'\x0',sizeof(argv5));
            memset(argv6,'\x0',sizeof(argv6));
			memset(argv7,'\x0',sizeof(argv7));	
		}

    } while(1);
    pthread_exit(NULL);
}

void maintain_link(global_config *gconfig, global_status *gstatus)
{
	ULONG ret, wanmode, hwstate, apistate, regstate, is_pin;
	ULONG pinstate, pinlast, dsstate, simstate, rleft;
	static unsigned int rebinds; 
	static unsigned int reregs;
	static unsigned int reconns;
	static unsigned int resets;

	/* make test params local copies */
	pthread_mutex_lock(&work_mutex);
	wanmode = gstatus->wan_mode;
	hwstate = gstatus->hw_state;
	apistate = gstatus->api_state;
	simstate = gstatus->sim_state;
	regstate = gstatus->reg_state;
	pinstate = gstatus->pin_state;
	rleft = gstatus->pin_rleft;
	pinlast = gstatus->pin_last;
	dsstate = gstatus->ds_state;
	is_pin = is_set(gconfig->pin);
	pthread_mutex_unlock(&work_mutex);

	/* if no hardware found schedule hard reset */
	if (hwstate != 0) {
		resets++;
	/* re-initialize API if not active */
	} else if (apistate != 0) {
		syslog(LOG_NOTICE, "maintain: api not active, trying to rebind\n");
		syslog(LOG_NOTICE, "maintain: reb=%d rer=%d rec=%d res=%d\n", rebinds, reregs, reconns, resets);
		rebinds++;
		pthread_mutex_lock(&work_mutex);
		close_gapi();
		pthread_mutex_unlock(&work_mutex);
		sleep(5);
		pthread_mutex_lock(&work_mutex);
		init_gapi();
		pthread_mutex_unlock(&work_mutex);
	/* re-register if not registered */
	} else if (regstate != 1) {
		/* do nothing if no sim or pin is blocked or pin entered incorrectly */	
		if (pinstate == 4 || pinstate == 5 || 
		pinlast != 0 || (pinstate == 1 && !is_pin) || simstate == 1) {
			syslog(LOG_NOTICE, "maintain: stalling due SIM/PIN condition\n");
		/* enter pin if required and if last enter was not incorrect */
		} else if (pinstate == 1 &&
		pinlast != 1) {
			syslog(LOG_NOTICE, "maintain: entering pin\n");
			pthread_mutex_lock(&work_mutex);
			ret = set_pin(gconfig, gstatus);
			pthread_mutex_unlock(&work_mutex);
			sleep(5);
			/* update pin_last to incorrect */
			if (ret == 1) {
				syslog(LOG_NOTICE, "maintain: incorrect pin\n");
			/* register */	
			} else if (ret == 0) {
				syslog(LOG_NOTICE, "maintain: not registered, trying to reregister (pin was entered)\n");
				syslog(LOG_NOTICE, "maintain: reb=%d rer=%d rec=%d res=%d\n", rebinds, reregs, reconns, resets);
				reregs++;
				pthread_mutex_lock(&work_mutex);
				link_register(gconfig);
				pthread_mutex_unlock(&work_mutex);
				sleep(5);
				pthread_mutex_lock(&work_mutex);
				persist_preference(gconfig);
				pthread_mutex_unlock(&work_mutex);
			/* update restart counter on other status */
			} else {
				syslog(LOG_NOTICE, "maintain: pin enter error (%d)\n", ret);
				reregs++;
			}
		/* register without pin enter if disabled or already entered */
		} else if (pinstate == 2 ||
		pinstate == 3) {
			syslog(LOG_NOTICE, "maintain: not registered, trying to reregister (pin enter was not required)\n");
			syslog(LOG_NOTICE, "maintain: reb=%d rer=%d rec=%d res=%d\n", rebinds, reregs, reconns, resets);
			reregs++;
			pthread_mutex_lock(&work_mutex);
			link_register(gconfig);
			pthread_mutex_unlock(&work_mutex);
			sleep(5);
			pthread_mutex_lock(&work_mutex);
			persist_preference(gconfig);
			pthread_mutex_unlock(&work_mutex);
		/* update restart counter on other status */
		} else {
			syslog(LOG_NOTICE, "maintain: abnormal sim or pin status\n");
			reregs++;
		}
	/* disconnect if 3G is not WAN/backup WAN */	
	} else if (wanmode == 2 && dsstate == 2) {
		syslog(LOG_NOTICE, "maintain: we should not be connected, disconnecting\n");
		reconns++;
		pthread_mutex_lock(&work_mutex);
		ds_disconnect();
		pthread_mutex_unlock(&work_mutex);
	/* reconnect if no or low quality data session */
	} else if (dsstate != 2  && 
				wanmode != 2) {
		syslog(LOG_NOTICE, "maintain: no or bad data session, trying to start ds\n");
		syslog(LOG_NOTICE, "maintain: reb=%d rer=%d rec=%d res=%d\n", rebinds, reregs, reconns, resets);
		reconns++;
		pthread_mutex_lock(&work_mutex);
		ds_disconnect();
		pthread_mutex_unlock(&work_mutex);
		sleep(5);
		pthread_mutex_lock(&work_mutex);
		ds_connect(gconfig);
		pthread_mutex_unlock(&work_mutex);
	/* everything is ok, reset counters */
	} else {
		rebinds = 0;
		reregs = 0;
		reconns = 0;
		resets = 0;
	}

	/* perform hard reset if any other try fails specified times in sequence */
	if (rebinds == 3 || reregs == 200 || 
	reconns == 3 || (resets == 5 && regstate != 1)) {
		syslog(LOG_NOTICE, "maintain: hard reset due too many retries\n");
		syslog(LOG_NOTICE, "maintain: reb=%d rer=%d rec=%d res=%d\n", rebinds, reregs, reconns, resets);
		rebinds = 0;
		reregs = 0;
		reconns = 0;
		resets = 0;
		pthread_mutex_lock(&work_mutex);
		close_gapi();
		pthread_mutex_unlock(&work_mutex);
		sleep(5);
		pthread_mutex_lock(&work_mutex);
		hard_off();
		pthread_mutex_unlock(&work_mutex);
		sleep(30);
		pthread_mutex_lock(&work_mutex);
		hard_on();
		pthread_mutex_unlock(&work_mutex);
		sleep(60);
		pthread_mutex_lock(&work_mutex);
		init_gapi();
		pthread_mutex_unlock(&work_mutex);
	}
	/* sleep for check interval time slice */
	sleep(120);
}

char *get_config_entry(struct uci_context *ctx, char *entry_name)
{
	struct uci_ptr ptr;
	struct uci_element *e;
	char name[UCI_ENTRY_SIZE];

	strncpy(name, entry_name, UCI_ENTRY_SIZE);
	if (uci_lookup_ptr(ctx, &ptr, name, 1) != UCI_OK)
		return NULL;
	e = ptr.last;
	if (!(ptr.flags & UCI_LOOKUP_COMPLETE))
		return NULL;
	switch(e->type) {
		case UCI_TYPE_SECTION:
			if ((ptr.s->type == NULL) || 
			(strcmp(ptr.s->type, "") == 0))			
				return NULL;	
			else 
				strncpy(uci_search_result, 
				(const char *)ptr.s->type, UCI_ENTRY_SIZE);
			break;
		case UCI_TYPE_OPTION:
			if ((ptr.o->v.string == NULL) || 
			(strcmp(ptr.o->v.string, "") == 0))
				return NULL;	
			else
				strncpy(uci_search_result, 
				(const char *)ptr.o->v.string, UCI_ENTRY_SIZE);
			break;
		default:
			break;
	}
	return &uci_search_result[0];
}

int load_config(global_config *gconfig)
{
	char *entryp;
	struct uci_context *ctx;
	
	if (!(ctx = uci_alloc_context())) {
		/* default config */
		strcpy(gconfig->pin, "");
		strcpy(gconfig->apn, "");
		strcpy(gconfig->user, "");
		strcpy(gconfig->password, "");
		strcpy(gconfig->auth_mode, "");
		strcpy(gconfig->net_mode, "");
		return -1;
	}
	/* get pin */
	if ((entryp = get_config_entry(ctx, 
	"network_3g.3g.pin")) == NULL)
		strcpy(gconfig->pin, "");
	else
		strncpy(gconfig->pin, entryp, UCI_ENTRY_SIZE);
	/* get apn */
	if ((entryp = get_config_entry(ctx, 
	"network_3g.3g.apn")) == NULL)
		strcpy(gconfig->apn, "");
	else
		strncpy(gconfig->apn, entryp, UCI_ENTRY_SIZE);
	/* get user */
	if ((entryp = get_config_entry(ctx, 
	"network_3g.3g.user")) == NULL)
		strcpy(gconfig->user, "");
	else
		strncpy(gconfig->user, entryp, UCI_ENTRY_SIZE);
	/* get password */
	if ((entryp = get_config_entry(ctx, 
	"network_3g.3g.password")) == NULL)
		strcpy(gconfig->password, "");
	else
		strncpy(gconfig->password, entryp, UCI_ENTRY_SIZE);
	/* get auth_mode */
	if ((entryp = get_config_entry(ctx, 
	"network_3g.3g.auth_mode")) == NULL)
		strcpy(gconfig->auth_mode, "");
	else
		strncpy(gconfig->auth_mode, entryp, UCI_ENTRY_SIZE);
	/* get net_mode */
	if ((entryp = get_config_entry(ctx, 
	"network_3g.3g.net_mode")) == NULL)
		strcpy(gconfig->net_mode, "");
	else
		strncpy(gconfig->net_mode, entryp, UCI_ENTRY_SIZE);
	uci_free_context(ctx);
	return 0;
}

/* detects if 3G is required at all */
int get_wan_mode(void) {
	char *entryp;
	char wan1[UCI_ENTRY_SIZE], wan2[UCI_ENTRY_SIZE];
	struct uci_context *ctx;
	if (!(ctx = uci_alloc_context())) {
		/* unknown */
		return 3;
	} else {
		if ((entryp = get_config_entry(ctx, 
		"network.wan.ifname")) == NULL)
			strcpy(wan1, "");
		else
			strcpy(wan1, entryp);
		if ((entryp = get_config_entry(ctx, 
		"network.wan2.ifname")) == NULL)
			strcpy(wan2, "");
		else
			strcpy(wan2, entryp);
		uci_free_context(ctx);
		if ((strcmp(wan1, "usb0") == 0))
			/* 3g */
			return 0;
		else if ((strcmp(wan2, "usb0") == 0))
			/* 3g as backup */
			return 1;
		else
			/* not 3g */
			return 2;
	}	
}

/* basically detects if modem is attached to RT at all */
int get_hardware_state(void) {
	char *entryp;
	struct uci_context *ctx;
	
	if (!(ctx = uci_alloc_context())) {
		/* unknown */
		return 3;
	} else {
		entryp = get_config_entry(ctx, "system.module.type");
		uci_free_context(ctx);
		if (entryp == NULL)
			/* off */
			return 1;
		else if (strcmp(entryp, "3g") == 0)
			/* on */
			return 0;
		else 
			return 1;
	}		
}

int update_wan_mode(global_status *gstatus) {
	gstatus->wan_mode = get_wan_mode();
	return 0;
}

int update_hw_state(global_status *gstatus) {
	gstatus->hw_state = get_hardware_state();
	return 0;
}

int update_api_state(global_status *gstatus) {
	ULONG ret;
	char dev_node[GENERIC_BUF_SIZE]; 
	char dev_key[GENERIC_BUF_SIZE];

	ret = QCWWANGetConnectedDeviceID(GENERIC_BUF_SIZE, 
	dev_node, GENERIC_BUF_SIZE, dev_key);
	if (ret == 0)
		gstatus->api_state = 0;
	else if (ret == 8)
		gstatus->api_state = 1;
	else
		gstatus->api_state = 2;
	return (int)ret;
}

int update_manufac(global_status *gstatus) {
	ULONG ret;
	char manufacturer[GENERIC_BUF_SIZE];

	if(ret = GetManufacturer(GENERIC_BUF_SIZE, 
	manufacturer)) 
		sprintf(gstatus->manufacturer, "unknown");
	else
		strcpy(gstatus->manufacturer, manufacturer);
	return (int)ret;
}

int update_model(global_status *gstatus) {
	ULONG ret;
	char model[GENERIC_BUF_SIZE];

	if(ret = GetModelID(GENERIC_BUF_SIZE, model))
		sprintf(gstatus->model_id, "unknown");
	else
		strcpy(gstatus->model_id, model);
	return (int)ret;
}

int update_fw_rev(global_status *gstatus) {
	ULONG ret;
	char fw_rev[GENERIC_BUF_SIZE];

	if(ret = GetFirmwareRevision(GENERIC_BUF_SIZE, fw_rev))
		sprintf(gstatus->fw_rev, "unknown");
	else
		strcpy(gstatus->fw_rev, fw_rev);
	return (int)ret;
}

int update_hw_rev(global_status *gstatus) {
	ULONG ret;
	char hw_rev[GENERIC_BUF_SIZE];

	if(ret = GetHardwareRevision(GENERIC_BUF_SIZE, hw_rev))
		sprintf(gstatus->hw_rev, "unknown");
	else
		strcpy(gstatus->hw_rev, hw_rev);
	return (int)ret;
}

int update_imei(global_status *gstatus) {
	ULONG ret;
	char esn[GENERIC_BUF_SIZE]; 
	char imei[GENERIC_BUF_SIZE]; 
    char imeid[GENERIC_BUF_SIZE];

	if(ret = GetSerialNumbers(GENERIC_BUF_SIZE, esn, 
	GENERIC_BUF_SIZE, imei, 
	GENERIC_BUF_SIZE, imeid))
		sprintf(gstatus->imei, "unknown");
	else 
		strcpy(gstatus->imei, imei);
	return (int)ret;
}

int update_simstate(global_status *gstatus) {
	ULONG ret_gid, ret_gpin;
	ULONG pin_state, pin_rleft, pin_uleft;
	char iccid[GENERIC_BUF_SIZE];
	/* using not direct functions to inquire state, 
	because there is no explicit API call for that */
	ret_gid = UIMGetICCID(GENERIC_BUF_SIZE, iccid); 
	ret_gpin = UIMGetPINStatus(1, &pin_state, &pin_rleft, &pin_uleft);
	/* guesing inserted */
	if (ret_gid == 0 || ret_gpin == 0) {
		gstatus->sim_state = 0;
	/* guesing not inserted */		
	} else if (ret_gid == 1003 && ret_gpin == 1003) {
		gstatus->sim_state = 1;
	/* unknown */
	} else {
		gstatus->sim_state = 2;
		return (int)ret_gid;
	}	
	return 0;
}

int update_pin(global_status *gstatus) {
	ULONG pin_state, pin_rleft, pin_uleft, ret;
	if (ret = UIMGetPINStatus(1, &pin_state, 
	&pin_rleft, &pin_uleft)) {
        gstatus->pin_state = 6;
		gstatus->pin_rleft = 0;
		gstatus->pin_uleft = 0;
    }
	else {
		gstatus->pin_state = pin_state;
		gstatus->pin_rleft = pin_rleft;
		gstatus->pin_uleft = pin_uleft;
	}
	return (int)ret;
}

int update_iccid(global_status *gstatus) {
	ULONG ret;
	char iccid[GENERIC_BUF_SIZE]; 

	if(ret = UIMGetICCID(GENERIC_BUF_SIZE, iccid))
		sprintf(gstatus->iccid, "unknown");
	else 
		strcpy(gstatus->iccid, iccid);
	return (int)ret;
}

int update_imsi(global_status *gstatus) {
	ULONG ret;
	char imsi[GENERIC_BUF_SIZE];

	if (ret = GetIMSI(GENERIC_BUF_SIZE, imsi))
		strcpy(gstatus->imsi, "unknown");
	else
		strcpy(gstatus->imsi, imsi);
	return (int)ret;
}

int update_reginfo(global_status *gstatus) {
	WORD mcc, mnc;
	BYTE rif_size = 10;
	ULONG i, ret, reg_state, cs_dom, ps_dom, ran, roaming, rifs[10];
	char name[GENERIC_BUF_SIZE], iface[GENERIC_BUF_SIZE];

	if (ret = GetServingNetwork(&reg_state, &cs_dom, 
	&ps_dom, &ran, &rif_size, (BYTE *)rifs, &roaming, 
	&mcc, &mnc, GENERIC_BUF_SIZE, name)) {
		gstatus->reg_state = 4;
		sprintf(gstatus->net_name, "unknown");
		sprintf(gstatus->iface, "unknown");
		gstatus->ran = 0;
		gstatus->roaming = 0;
		gstatus->cs_dom = 0;
		gstatus->ps_dom = 0; 
		gstatus->mcc = 0; 
		gstatus->mnc = 0;
		gstatus->signal = 0;
    }
	else {
		gstatus->reg_state = reg_state;
		strcpy(gstatus->net_name, name);
		gstatus->ran = ran;
		gstatus->roaming = roaming;
		gstatus->cs_dom = cs_dom;
		gstatus->ps_dom = ps_dom;
		gstatus->mcc = mcc;
		gstatus->mnc = mnc;
		iface[0]='\0';
		for(i=0; i<rif_size; i++) {
        	sprintf(&iface[strlen(iface)],"%s ", 
			radiotech_type[rifs[i]]);
		}
		strcpy(gstatus->iface, iface);
	}
	return (int)ret;
}

int update_signal(global_status *gstatus) {
	INT8 sig_stren;
	ULONG sig_size, sig_rifs, ret;
	
	if(ret = GetSignalStrengths(&sig_size, 
	&sig_stren, &sig_rifs))
        gstatus->signal = 0;
	else
		gstatus->signal = sig_stren;
	return (int)ret;
}

int update_preference(global_status *gstatus) {
	ULONG tech_pref, dur_pref, ptech_pref, ret;
	gstatus->net_pref[0] = '\0';
	if (ret = GetNetworkPreference(&tech_pref, 
	&dur_pref, &ptech_pref)) {
        sprintf(&gstatus->net_pref[strlen(gstatus->net_pref)],"unknown\n");
    } else {
		if (dur_pref == 0)
			parse_preference(ptech_pref, gstatus->net_pref);
		else if (dur_pref == 1)
			parse_preference(tech_pref, gstatus->net_pref);
		else 
			sprintf(&gstatus->net_pref[strlen(gstatus->net_pref)],"unknown\n");
	}
	return (int)ret;
}

void parse_preference(ULONG pref, char* ret_buf) {
    ULONG pref2 = pref >> 2;
    switch(pref&0x3) {
    case 0:
        sprintf(&ret_buf[strlen(ret_buf)],"auto");
        break;
    case 1:
        sprintf(&ret_buf[strlen(ret_buf)],"CDMA - ");
        if(pref2 & 0x1) sprintf(&ret_buf[strlen(ret_buf)],"Analog ");
        if(pref2 & 0x2) sprintf(&ret_buf[strlen(ret_buf)],"1xRTT ");
        if(pref2 & 0x4) sprintf(&ret_buf[strlen(ret_buf)],"1xEV-DO ");
        break;
    case 2:
        sprintf(&ret_buf[strlen(ret_buf)],"UMTS - ");
        if(pref2 & 0x1) sprintf(&ret_buf[strlen(ret_buf)],"GSM ");
        if(pref2 & 0x2) sprintf(&ret_buf[strlen(ret_buf)],"WCDMA ");
        break;
    default:
        sprintf(&ret_buf[strlen(ret_buf)],"unknown");
    }
}

int update_session_state(global_status *gstatus) {
	ULONG ds_state, ret;
	if(ret = GetSessionState(&ds_state))
		gstatus->ds_state = 0;
	else
    	gstatus->ds_state = ds_state;
	return (int)ret;
}

int update_autoconn_mode(global_status *gstatus) {
	ULONG mode, roaming, ret;
	if(ret = GetEnhancedAutoconnect(&mode, &roaming))
		gstatus->autoconn_mode = 3;
	else
    	gstatus->autoconn_mode = mode;
	return (int)ret;
}

int update_bearer(global_status *gstatus) {
	ULONG bearer, ret;
	if(ret = GetDataBearerTechnology(&bearer))
		gstatus->bearer = 0;
    else
		gstatus->bearer = bearer;
	return (int)ret;
}

int update_rates(global_status *gstatus) {
	ULONG cur_tx, cur_rx, ds_max_tx, ds_max_rx, ret;
	if(ret = GetConnectionRate(&cur_tx, &cur_rx, 
	&ds_max_tx, &ds_max_rx)) {
		gstatus->cur_tx = 0;
		gstatus->cur_rx = 0;
    	gstatus->ds_max_tx = 0;
		gstatus->ds_max_rx = 0;
    }
	else {
		gstatus->cur_tx = cur_tx;
		gstatus->cur_rx = cur_rx;
    	gstatus->ds_max_tx = ds_max_tx;
		gstatus->ds_max_rx = ds_max_rx;
	}
	return (int)ret;
}

int update_bytes(global_status *gstatus) {
	ULONGLONG tx_bytes, rx_bytes, ret;
	if(ret = GetByteTotals(&tx_bytes, &rx_bytes)) {
    	gstatus->tx_bytes = 0;
		gstatus->rx_bytes = 0; 
    }
	else {
		gstatus->tx_bytes = tx_bytes;
		gstatus->rx_bytes = rx_bytes;
	}
	return (int)ret;
}

int update_packets(global_status *gstatus) {
	ULONG pack_tx_ok, pack_rx_ok, pack_tx_fail, ret; 
	ULONG pack_rx_fail, pack_tx_over, pack_rx_over;
	if(ret = GetPacketStatus(&pack_tx_ok, &pack_rx_ok, 
	&pack_tx_fail, &pack_rx_fail, 
	&pack_tx_over, &pack_rx_over)) {
        gstatus->pack_tx_ok = 0;
		gstatus->pack_rx_ok = 0;
		gstatus->pack_tx_fail = 0;
		gstatus->pack_rx_fail = 0;
		gstatus->pack_tx_over = 0;
		gstatus->pack_rx_over = 0;
    }
	else {
		gstatus->pack_tx_ok = pack_tx_ok;
		gstatus->pack_rx_ok = pack_rx_ok;
		gstatus->pack_tx_fail = pack_tx_fail;
		gstatus->pack_rx_fail = pack_rx_fail;
		gstatus->pack_tx_over = pack_tx_over;
		gstatus->pack_rx_over = pack_rx_over;
	}
	return (int)ret;
}

int update_ip(global_status *gstatus) {
	ULONG ret, ip;
	char address[GENERIC_BUF_SIZE];
	if(ret = GetIPAddress(&ip)) {
    	sprintf(gstatus->ip, "unknown");
    }
	else {
		address[0] = '\0';
    	sprintf(&address[strlen(address)],"%d.%d.%d.%d",
        *(((BYTE *)&ip) +3),
        *(((BYTE *)&ip) +2),
        *(((BYTE *)&ip) +1),
        *(((BYTE *)&ip)));
		strcpy(gstatus->ip, address);
	}
	return (int)ret;
}

int update_duration(global_status *gstatus) {
	ULONG ret;
	ULONGLONG duration, seconds, days, hours, minutes;
	if(ret = GetSessionDuration(&duration)) {
        sprintf(gstatus->duration, "unknown");
    }
	else {
		seconds = duration / 1000;
		days = seconds / 86400;
		seconds = seconds % 86400;
		hours = seconds / 3600;
		seconds = seconds % 3600;
		minutes = seconds / 60;
		seconds = seconds % 60;
		sprintf(gstatus->duration, 
			"%ddays %dh. %dmin. %dsec.", 
			(int)days, (int)hours, 
			(int)minutes, (int)seconds);
	}
	return (int)ret;
}

int update_status_total(global_status *gstatus) {
	update_wan_mode(gstatus);
	update_hw_state(gstatus);
	/* if modem off no point to continue */
	if (gstatus->hw_state != 0)
		return 1;
	update_api_state(gstatus);
	/* if api innactive no point to continue */
	if (gstatus->api_state != 0)
		return 2;
	update_manufac(gstatus);
	update_model(gstatus);
	update_fw_rev(gstatus);
	update_hw_rev(gstatus);
	update_imei(gstatus);
	update_simstate(gstatus);
	/* if no sim no point to continue */
	if (gstatus->sim_state != 0)
		return 3;
	update_iccid(gstatus);
    update_pin(gstatus);
	update_imsi(gstatus);
	update_preference(gstatus);
	update_reginfo(gstatus);
	/* if unregistered no point to continue */
	if (gstatus->reg_state != 1)
		return 4;
	update_signal(gstatus);
	update_session_state(gstatus);
	/* if disconnected no point to continue */
	if (gstatus->ds_state != 2)
		return 5;
	update_autoconn_mode(gstatus);
	update_bearer(gstatus);
    update_rates(gstatus);
	update_bytes(gstatus);
    update_packets(gstatus);
	update_ip(gstatus);
    update_duration(gstatus);
	return 0;   
}
								
int export_status(global_status *gstatus, char *ret_buf) {
	sprintf(&ret_buf[strlen(ret_buf)],"\n(Generic)\n");
	sprintf(&ret_buf[strlen(ret_buf)],"WAN mode     = %s\n", 
		wan_mode[gstatus->wan_mode]);
	sprintf(&ret_buf[strlen(ret_buf)],"Modem state  = %s\n", 
		hardware_state[gstatus->hw_state]);
	if (gstatus->hw_state != 0) 
		return 1;
	sprintf(&ret_buf[strlen(ret_buf)],"API state    = %s\n\n", 
		gobi_api_state[gstatus->api_state]);
	if (gstatus->api_state != 0)
		return 2;
	sprintf(&ret_buf[strlen(ret_buf)],"(Device)\n");
	sprintf(&ret_buf[strlen(ret_buf)],"Manufacturer = %s\n", 
		gstatus->manufacturer);
	sprintf(&ret_buf[strlen(ret_buf)],"Model        = %s\n", 
		gstatus->model_id);
	sprintf(&ret_buf[strlen(ret_buf)],"Hardware rev = %s\n", 
		gstatus->hw_rev);
	sprintf(&ret_buf[strlen(ret_buf)],"Firmware rev = %s\n", 
		gstatus->fw_rev);
	sprintf(&ret_buf[strlen(ret_buf)],"Imei         = %s\n\n", 
		gstatus->imei);
	sprintf(&ret_buf[strlen(ret_buf)],"(Sim)\n");
	sprintf(&ret_buf[strlen(ret_buf)],"State        = %s\n", 
		sim_state[gstatus->sim_state]);
	if (gstatus->sim_state != 0) 
		return 3;
	sprintf(&ret_buf[strlen(ret_buf)],"Iccid        = %s\n", 
		gstatus->iccid);
	sprintf(&ret_buf[strlen(ret_buf)],"Imsi         = %s\n\n", 
		gstatus->imsi);
	sprintf(&ret_buf[strlen(ret_buf)],"(Pin)\n");
	sprintf(&ret_buf[strlen(ret_buf)],"State        = %s\n", 
		pin_state[gstatus->pin_state]);
	sprintf(&ret_buf[strlen(ret_buf)],"Verifies     = %lu\n", 
		gstatus->pin_rleft);
	sprintf(&ret_buf[strlen(ret_buf)],"Unblocks     = %lu\n", 
		gstatus->pin_uleft);
	sprintf(&ret_buf[strlen(ret_buf)],"Last attempt = %s\n\n", 
		last_entered_pin[gstatus->pin_last]);
	sprintf(&ret_buf[strlen(ret_buf)],"(Registration)\n");
	sprintf(&ret_buf[strlen(ret_buf)],"State        = %s\n", 
		registration_state[gstatus->reg_state]);
	if (gstatus->reg_state != 1) 
		return 4;
	sprintf(&ret_buf[strlen(ret_buf)],"Network name = %s\n", 
		gstatus->net_name);
	sprintf(&ret_buf[strlen(ret_buf)],"Network type = %s\n", 
		ran_type[gstatus->ran]);
	sprintf(&ret_buf[strlen(ret_buf)],"Interface    = %s\n", 
		gstatus->iface);
	sprintf(&ret_buf[strlen(ret_buf)],"Signal       = %ddBm\n", 
		gstatus->signal);
	sprintf(&ret_buf[strlen(ret_buf)],"Preference   = %s\n", 
		gstatus->net_pref);
	sprintf(&ret_buf[strlen(ret_buf)],"Roaming      = %s\n", 
	roaming[gstatus->roaming]);
	sprintf(&ret_buf[strlen(ret_buf)],"Network code = %lu\n", 
		gstatus->mnc);
	sprintf(&ret_buf[strlen(ret_buf)],"Country code = %lu\n", 
		gstatus->mcc);
	sprintf(&ret_buf[strlen(ret_buf)],"CS status    = %s\n", 
		domain_state[gstatus->cs_dom]);
	sprintf(&ret_buf[strlen(ret_buf)],"PS status    = %s\n\n", 
		domain_state[gstatus->ps_dom]);
	sprintf(&ret_buf[strlen(ret_buf)],"(Data session)\n");
	sprintf(&ret_buf[strlen(ret_buf)],"State        = %s\n", 
		session_state[gstatus->ds_state]);
	if (gstatus->ds_state != 2)
		return 5;
	sprintf(&ret_buf[strlen(ret_buf)],"Autoconn     = %s\n", 
		autoconnection_mode[gstatus->autoconn_mode]);
	sprintf(&ret_buf[strlen(ret_buf)],"Bearer       = %s\n", 
		data_bearer[gstatus->bearer]);
	sprintf(&ret_buf[strlen(ret_buf)],"Max TX rate  = %dbps\n", 
		gstatus->ds_max_tx);
	sprintf(&ret_buf[strlen(ret_buf)],"Max RX rate  = %dbps\n", 
		gstatus->ds_max_rx);
	sprintf(&ret_buf[strlen(ret_buf)],"TX rate      = %dbps\n", 
		gstatus->cur_tx);
	sprintf(&ret_buf[strlen(ret_buf)],"RX rate      = %dbps\n", 
		gstatus->cur_rx);
	sprintf(&ret_buf[strlen(ret_buf)],"TX bytes     = %lu\n", 
		gstatus->tx_bytes);
	sprintf(&ret_buf[strlen(ret_buf)],"RX bytes     = %lu\n", 
		gstatus->rx_bytes);
	sprintf(&ret_buf[strlen(ret_buf)],"TX packets   = %d\n", 
		gstatus->pack_tx_ok);
	sprintf(&ret_buf[strlen(ret_buf)],"RX packets   = %d\n", 
		gstatus->pack_rx_ok);
	sprintf(&ret_buf[strlen(ret_buf)],"TX dropped   = %d\n", 
		gstatus->pack_tx_over);
	sprintf(&ret_buf[strlen(ret_buf)],"RX dropped   = %d\n", 
		gstatus->pack_rx_over);
	sprintf(&ret_buf[strlen(ret_buf)],"TX errors    = %d\n", 
		gstatus->pack_tx_fail);
	sprintf(&ret_buf[strlen(ret_buf)],"RX errors    = %d\n", 
		gstatus->pack_rx_fail);
	sprintf(&ret_buf[strlen(ret_buf)],"IP address   = %s\n", 
		gstatus->ip);
	sprintf(&ret_buf[strlen(ret_buf)],"Duration     = %s\n", 
		gstatus->duration);	
	return 0;	
}

int export_config(global_config *gconfig, char *ret_buf) {
	sprintf(&ret_buf[strlen(ret_buf)],"\n(Configuration)\n");
	sprintf(&ret_buf[strlen(ret_buf)],"pin          = %s\n", 
		gconfig->pin);
	sprintf(&ret_buf[strlen(ret_buf)],"apn          = %s\n", 
		gconfig->apn);
	sprintf(&ret_buf[strlen(ret_buf)],"user         = %s\n", 
		gconfig->user);
	sprintf(&ret_buf[strlen(ret_buf)],"password     = %s\n", 
		gconfig->password);
	sprintf(&ret_buf[strlen(ret_buf)],"auth_mode    = %s\n", 
		gconfig->auth_mode);
	sprintf(&ret_buf[strlen(ret_buf)],"net_mode     = %s\n", 
		gconfig->net_mode);
	return 0;	
}

int dhcp_renew(void) {
	return system("kill -SIGUSR1 `pidof udhcpc`; sleep 1; \
		kill -SIGUSR1 `pidof udhcpc`; sleep 1; \
		kill -SIGUSR1 `pidof udhcpc`");
}

int hard_on(void) {
	int ret;
	ret = system("echo 7 > /sys/class/gpio/export; \
	echo 8 > /sys/class/gpio/export; \
	echo out > /sys/class/gpio/gpio7/direction; \
	echo out > /sys/class/gpio/gpio8/direction; \
	echo 1 > /sys/devices/virtual/gpio/gpio7/value; \
	echo 1 > /sys/devices/virtual/gpio/gpio8/value; \
	echo 7 > /sys/class/gpio/unexport; \
	echo 8 > /sys/class/gpio/unexport;");
	return ret;
}

int hard_off(void) {
	int ret;
	ret = system("echo 7 > /sys/class/gpio/export; \
	echo 8 > /sys/class/gpio/export; \
	echo out > /sys/class/gpio/gpio7/direction; \
	echo out > /sys/class/gpio/gpio8/direction; \
	echo 0 > /sys/devices/virtual/gpio/gpio7/value; \
	echo 0 > /sys/devices/virtual/gpio/gpio8/value; \
	echo 7 > /sys/class/gpio/unexport; \
	echo 8 > /sys/class/gpio/unexport;");
	return ret;
}

int soft_reset(void) {
	ULONG ret;
	if(ret = SetPower(3))
		return (int)ret;
	ret = SetPower(4);
	return (int)ret;
}

int is_set(char *str)
{
	if (str == NULL || (strcmp(str, "") == 0))
		return 0;
	else
		return 1;
}

int init_gapi(void) {
	BYTE size = 10;
	qc_devices devs[10];
	ULONG ret;

	if(ret = QCWWANEnumerateDevices(&size, 
	(BYTE*)devs))
    	return (int)ret;
	ret = QCWWANConnect(devs[0].node, devs[0].key);
	return (int)ret;
}

int close_gapi(void) {
	return (int)QCWWANDisconnect();
}

int is_gapi(void) {
	char node[128], key[128];
	return (int)QCWWANGetConnectedDeviceID(128, 
		node, 128, key);
}

int persist_preference(global_config *gconfig) {
	int ret;
	if (strcmp(gconfig->net_mode, "gsm") == 0)
		ret = set_preference("gsm");
	else if (strcmp(gconfig->net_mode, "umts") == 0)
		ret = set_preference("umts");
	else
		ret = set_preference("auto");
	return ret;
}

int link_register(global_config *gconfig) {
	 return auto_register();		
}

int auto_register(void) {
	return (int)InitiateNetworkRegistration(1, 0, 0, 0);	
}

int manual_register(int mcc, int mnc, char *net) {
	ULONG ret;
	if (strcmp(net, "gsm") == 0)
		ret = InitiateNetworkRegistration(2, (WORD)mcc, (WORD)mnc, 4);
	else if (strcmp(net, "umts") == 0)
		ret = InitiateNetworkRegistration(2, (WORD)mcc, (WORD)mnc, 5);
	else
		return -1;
	return (int)ret;
}

int set_autoconn(int mode) {
	ULONG ret, roam = 0;	
	if (mode == 1)
		ret = SetEnhancedAutoconnect(1, &roam);
	else
		ret = SetEnhancedAutoconnect(0, &roam);
	return (int)ret;
}

int man_connect(char *argvn) {
    ULONG failure_reason, ret;
	/* apn */
    char argv1[32];
	/* user */
    char argv2[32];
	/* password */
    char argv3[32];
	/* authentication */
    ULONG argv4=0;

    memset(argv1,'\x0',sizeof(argv1));
    memset(argv2,'\x0',sizeof(argv2));
    memset(argv3,'\x0',sizeof(argv3));
    if(strlen(argvn)>0) {
        sscanf(argvn,"%[^ ]%*[ ]%[^ ]%*[ ]%[^ ] %d",
			argv1, argv2, argv3, &argv4);
        ret = StartDataSession( NULL, NULL, NULL, NULL, NULL, 
		argv1, NULL, &argv4, argv2, argv3, 
		&session_id, &failure_reason);
    } else {
        ret = StartDataSession( NULL, NULL, NULL, NULL, NULL, 
		NULL, NULL, NULL, NULL, NULL, 
		&session_id, &failure_reason);     
    }
	/* do not renew on failure */
	if (!ret)
		dhcp_renew();
    return (int)ret;
}

int ds_disconnect(void) {
	ULONG ret;
	if (ret = StopDataSession(session_id)) {
		QCWWANCancel();
		CancelDataSession();
	}
	return (int)ret;
}

int ds_connect(global_config *gconfig)
{
	ULONG failureReason, auth_type, ret;

	if (is_set(gconfig->auth_mode) && 
	(strcmp(gconfig->auth_mode, "none") != 0)) {
		if (strcmp(gconfig->auth_mode, "pap") == 0)
			auth_type = 1;
		else if (strcmp(gconfig->auth_mode, "chap") == 0)
			auth_type = 2;
		else
			auth_type = 3;
		if (is_set(gconfig->apn)) {
			ret = StartDataSession(NULL, NULL, NULL, NULL, NULL, 
			gconfig->apn, NULL, &auth_type, gconfig->user, 
			gconfig->password, &session_id, &failureReason);
		}
		else {
			ret = StartDataSession(NULL, NULL, NULL, NULL, NULL, 
			NULL, NULL, &auth_type, gconfig->user, 
			gconfig->password, &session_id, &failureReason);
		}
	}
	else {
		if (is_set(gconfig->apn)) {
			ret = StartDataSession(NULL, NULL, NULL, NULL, NULL, 
			gconfig->apn, NULL, NULL, NULL, NULL, 
			&session_id, &failureReason);
		}
		else {
			ret = StartDataSession(NULL, NULL, NULL, NULL, NULL, 
			NULL, NULL, NULL, NULL, NULL, 
			&session_id, &failureReason);
		}
	}
	/* do not renew on failure */
	if (!ret)
		dhcp_renew();
	return (int)ret;
}

int set_preference(char *mode) {
	ULONG ret;
	if(strcmp(mode, "gsm") == 0)
		ret = SetNetworkPreference(6,0);	
	else if (strcmp(mode, "umts") == 0)
		ret = SetNetworkPreference(10,0);
	else 
		ret = SetNetworkPreference(0,0);
	return (int)ret;
}

int scan_network(char* ret_buf) {
	BYTE list_size = 10;
    ULONG ret, i;
    net_info net_list[10];
    
    if(ret = PerformNetworkScan(&list_size, (BYTE*)net_list)) {
        sprintf(&ret_buf[strlen(ret_buf)],
			"gobid: operation failed (%ld)\n", ret);
    }
    sprintf(&ret_buf[strlen(ret_buf)],"(scan results)\n\n");
    for(i=0 ; i<list_size ; i++) {
        sprintf(&ret_buf[strlen(ret_buf)],"MCC          = %d\n", 
			net_list[i].mcc);
        sprintf(&ret_buf[strlen(ret_buf)],"MNC          = %d\n", 
			net_list[i].mnc);
        sprintf(&ret_buf[strlen(ret_buf)],"Usage        = %s\n", 
			networks[net_list[i].in_use]);
        sprintf(&ret_buf[strlen(ret_buf)],"Roaming      = %s\n", 
			roaming[net_list[i].roaming]);
        sprintf(&ret_buf[strlen(ret_buf)],"Forbidden    = %s\n", 
			forbidden[net_list[i].forbidden]);
        sprintf(&ret_buf[strlen(ret_buf)],"Preferred    = %s\n", 
			preferred[net_list[i].preferred]);
        sprintf(&ret_buf[strlen(ret_buf)],"Description  = %s\n\n", 
			net_list[i].description);
    }
    return (int)ret;
}

int set_power_mode(ULONG mode) {
    return (int)SetPower(mode);
}

int abort_request(void) {
	return (int)QCWWANCancel();
}

/* manual pin verification */
int enter_pin(char *pin, global_status *gstatus) {
    ULONG ret;
    ret = UIMVerifyPIN(1, pin, 
		&gstatus->pin_rleft, &gstatus->pin_uleft);
	return (int)ret;
}

/* manual pin enable */
int enable_pin(char *pin, global_status *gstatus) {
    ULONG ret;
    ret = UIMSetPINProtection(1, 1, pin, 
		&gstatus->pin_rleft, &gstatus->pin_uleft);
	return (int)ret;
}

/* manual pin disable */
int disable_pin(char *pin, global_status *gstatus) {
    ULONG ret;
    ret = UIMSetPINProtection(1, 0, pin, 
		&gstatus->pin_rleft, &gstatus->pin_uleft);
	return (int)ret;
}

/* manual pin unblock */
int unblock_pin(char *puk, char *pin, global_status *gstatus) {
    ULONG ret;
    ret = UIMUnblockPIN(1, puk, pin, 
		&gstatus->pin_rleft, &gstatus->pin_uleft);
	return (int)ret;
}

int set_pin(global_config *gconfig, global_status *gstatus) {
	ULONG ret;
    ret = UIMVerifyPIN(1, gconfig->pin, &gstatus->pin_rleft, 
	&gstatus->pin_uleft);
	if (ret == 0) {
		/* set successful */
		gstatus->pin_last = 0;
	} else if (ret == 1012) {
		/* incorrect pin */
		gstatus->pin_last = 1;
	} else {
		/* undetectible error */
		gstatus->pin_last = 2;
	}
	return (int)ret;
}

static void sig_handler(int signum) {
	syslog(LOG_NOTICE, "recieved signal (%d)", signum);
    switch(signum) {
    case SIGALRM:
		StopDataSession(session_id);
		QCWWANCancel();
        QCWWANDisconnect();
        closelog();
        exit(EXIT_FAILURE);
        break;
    case SIGUSR1:
		StopDataSession(session_id);
		QCWWANCancel();
        QCWWANDisconnect();
        closelog();
        exit(EXIT_SUCCESS);
        break;
    case SIGCHLD:
		StopDataSession(session_id);
		QCWWANCancel();
        QCWWANDisconnect();
        closelog();
        exit(EXIT_FAILURE);
        break;
    case SIGTERM:
		StopDataSession(session_id);
		QCWWANCancel();
        QCWWANDisconnect();
        closelog();
        exit(EXIT_FAILURE);
        break;
    }
}

static void daemonize( const char *lockfile ) {
    pid_t pid, sid, parent;
    int lfp = -1;
	char str[10];
	
	/* perform initial status update */
	update_wan_mode(&modem_status);	
	update_hw_state(&modem_status);
	
	/* exit if no hardware found */
	if (modem_status.hw_state == 1) {
		syslog(LOG_ERR, "hardware is not found on start, exiting\n");
		exit(EXIT_FAILURE);
	}

    /* already a daemon */
    if ( getppid() == 1 ) return;

    /* Trap signals that we expect to recieve */
    signal(SIGCHLD,sig_handler);
    signal(SIGUSR1,sig_handler);
    signal(SIGALRM,sig_handler);
    signal(SIGTERM,sig_handler);
	
    /* Fork off the parent process */
    pid = fork();
    if (pid < 0) {
        syslog( LOG_ERR, "unable to fork daemon, code=%d (%s)",
                errno, strerror(errno) );
        exit(EXIT_FAILURE);
    }
    
    /* If we got a good PID, then we can exit the parent process. */
    if (pid > 0) {

        /* Wait for confirmation from the child via SIGTERM or SIGCHLD, or
           for two seconds to elapse (SIGALRM).  pause() should not return. */
        alarm(2);
        pause();

        exit(EXIT_FAILURE);
    }

    /* At this point we are executing as the child process */
    parent = getppid();

    /* Create a new SID for the child process */
    sid = setsid();
    if (sid < 0) {
        syslog( LOG_ERR, "unable to create a new session, code %d (%s)",
                errno, strerror(errno) );
        exit(EXIT_FAILURE);
    }

    /* Redirect standard files to /dev/null */
    freopen( "/dev/null", "r", stdin);
    freopen( "/dev/null", "w", stdout);
    freopen( "/dev/null", "w", stderr);
	
	/* Change the file mode mask */
    umask(0);
	
	/* Change the current working directory.  This prevents the current
       directory from being locked; hence not being able to remove it. */
    if ((chdir("/")) < 0) {
        syslog( LOG_ERR, "unable to change directory to %s, code %d (%s)",
                "/", errno, strerror(errno) );
        exit(EXIT_FAILURE);
    }
    
    /* Create the lock file as the current user */
    if ( lockfile && lockfile[0] ) {
        lfp = open(lockfile,O_RDWR|O_CREAT,0640);
        if ( lfp < 0 ) {
            syslog( LOG_ERR, "unable to create lock file %s, code=%d (%s)",
                    lockfile, errno, strerror(errno) );
            exit(EXIT_FAILURE);
        }
        if ( lockf(lfp,F_TLOCK,0) < 0 ) {
            syslog( LOG_ERR, "already running, exiting" );
            exit(EXIT_SUCCESS);
        }
        sprintf(str,"%d\n",getpid());
		write(lfp,str,strlen(str));
    }
	
    /* Drop user if there is one, and we were run as root */
    if ( getuid() == 0 || geteuid() == 0 ) {
        struct passwd *pw = getpwnam(RUN_AS_USER);
        if ( pw ) {
            syslog( LOG_NOTICE, "setting user to " RUN_AS_USER );
            setuid( pw->pw_uid );
        }
    }
	
	/* Cancel certain signals */
    signal(SIGCHLD,SIG_DFL); /* A child process dies */
    signal(SIGTSTP,SIG_IGN); /* Various TTY signals */
    signal(SIGTTOU,SIG_IGN);
    signal(SIGTTIN,SIG_IGN);
    signal(SIGHUP, SIG_IGN); /* Ignore hangup signal */

    /* Tell the parent process that we are A-okay */
    kill( parent, SIGUSR1 );
}
