#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/un.h>
#include <sys/time.h>
#include <netdb.h>

#define NAME "/var/lock/gobisocket"
#define GENERIC_BUF_SIZE 64
#define SOCK_BUF_SIZE 4096
#define ANS_TIMEOUT 5

int main(int argc, char *argv[]) {
    int sockfd, n, i, retval, portno;
	char buffer[SOCK_BUF_SIZE];
	char buff[4*GENERIC_BUF_SIZE];
    struct sockaddr_in  serv_addr;
    struct timeval tv;
    fd_set rfds;
    struct hostent *server;

    portno = 777;
    server = gethostbyname("127.0.0.1");

    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        perror("failed to create socket");

    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr,
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
    serv_addr.sin_port = htons(portno);

    if (connect(sockfd, (struct sockaddr *)
                &serv_addr, sizeof(serv_addr)) < 0)
        perror("failed to connect");

   	/* watch stdin (fd 0) to see when it has input */
    FD_ZERO(&rfds);
    FD_SET(sockfd, &rfds);
   	/* limit blocking for some specific time */
    tv.tv_sec = ANS_TIMEOUT;
    tv.tv_usec = 0;
    memset(buff,'\x0',sizeof(buff));

    if (argc != 1) {
        for (i=1; i < argc; i++) {
            sprintf(&buff[strlen(buff)],"%s ", argv[i]);
        }
    } else {
        printf("at least one parameter is required\n");
        exit(0);
    }

    buff[strlen(buff)-1] = '\x0';
    write(sockfd, buff, strlen(buff));
    retval = select(sockfd+1, &rfds, NULL, NULL, &tv);

    if (retval == -1) {
        perror("failed to select");
    } else if (retval) {
		n = read(sockfd, buffer, SOCK_BUF_SIZE);
		write(1, buffer, n);
    } else {
        printf("no data within %d seconds\n", ANS_TIMEOUT);
    }
    exit(0);
}

