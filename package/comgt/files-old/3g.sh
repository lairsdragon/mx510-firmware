
. /lib/teltonika-functions.sh

set_3g_led() {
	# set on WRT54G3G only
	[ -f /proc/diag/model ] || return 0
	grep -q "WRT54G3G" /proc/diag/model >/dev/null || return 0
	echo "$1" > /proc/diag/led/3g_green
	echo "$2" > /proc/diag/led/3g_blue
	grep -q "WRT54G3G$" /proc/diag/model >/dev/null || return 0
	echo "$3" > /proc/diag/led/3g_blink
}

scan_3g() {
	local device
	config_get device "$1" device

	# try to figure out the device if it's invalid
	[ -n "$device" -a -e "$device" ] || {
		for device in /dev/ttyUSB0 /dev/ttyUSB1 /dev/ttyUSB2 /dev/tts/2 /dev/usb/tts/0 /dev/noz0; do
			[ -e "$device" ] && {
				config_set "$1" device "$device"
				break
			}
		done
	}

	# enable 3G with the 3G button by default
	local button
	config_get button "$1" button
	[ -z "$button" ] && {
		config_set "$1" button 1
	}
}

stop_interface_3g() {
	stop_interface_ppp "$1"
	set_3g_led 0 0 0
	killall gcom >/dev/null 2>/dev/null
}

setup_interface_3g() {
	local iface="$1"
	local config="$2"
	local chat="/etc/chatscripts/3g.chat"
	local evidpid=$(tlt_get_ext_vidpid)

	local device
	config_get device "$config" device

	local maxwait
	config_get maxwait "$config" maxwait
	maxwait=${maxwait:-20}
	while [ ! -e "$device" -a $maxwait -gt 0 ];do # wait for driver loading to catch up
		maxwait=$(($maxwait - 1))
		sleep 1
	done

	for module in slhc ppp_generic ppp_async; do
		/sbin/insmod $module 2>&- >&-
	done

	local apn
	config_get apn "$config" apn

	local service
	config_get service "$config" service

	local pincode
	config_get pincode "$config" pincode

	local mtu
	config_get mtu "$config" mtu
	
	local dialnumber
	config_get dialnumber "$config" dialnumber
	
	local regmode
	config_get regmode "$config" regmode
	
	local mcc
	config_get mcc "$config" mcc
	
	local mnc
	config_get mnc "$config" mnc
	
	local radio
	config_get radio "$config" radio
	
	set_3g_led 1 0 1
	
	# NOTE: since we do not analyse answer from modem here,
	# we only pass AT command using "gsmget" util.
	# FIXME: all following service modes are NOT supported in
	# different modems, and if they are, most often they are not supported
	# in the same manner, so we SHOULD develop some unified service
	# scheme or dynamic mode selection based on used modem. No time
	# for it today though. Also, unsoliced reports generated
	# by some AT commands here tend to crash GSMD.
	case "$service" in
		cdma |\
		evdo) 
			chat="/etc/chatscripts/evdo.chat"
			;;
		
		# 3G preferred
		umts)
			case "$evidpid" in
				# Option GTM661W
				0AF0:9000)
					gsmget --at "AT_OPSYS=3,2"
					;;
				# Telit HE910-EUD
				1BC7:0021)
					# NOTE: this device does not support 3G preferred, using auto instead
					gsmget --at "AT+WS46=25"
					;;
				# Sierra MC8700
				1199:68A3)
					gsmget --at "AT!SELRAT=03"
					gsmget --at "AT!BAND=00"
					;;
				# Huawei EM820W
				12D1:1404)
					gsmget --at "AT^SYSCFG=2,2,3FFFFFFF,1,2"
					;;
				# Broadmobi LP10
				2020:1005)
					gsmget --at "AT+BMMODODR=2"
					;;
				*)
					echo "$config(3g): no service case for this VID:PID"
					;;
			esac
			;;
		
		# 3G only
		umts-only)
			case "$evidpid" in
				# Option GTM661W
				0AF0:9000)
					gsmget --at "AT_OPSYS=1,2"
					;;
				# Telit HE910-EUD
				1BC7:0021)
					gsmget --at "AT+WS46=22"
					;;
				# Sierra MC8700
				1199:68A3)
					gsmget --at "AT!SELRAT=01"
					gsmget --at "AT!BAND=08"
					;;
				# Huawei EM820W
				12D1:1404)
					gsmget --at "AT^SYSCFG=14,0,3FFFFFFF,1,2"
					;;
				# Broadmobi LP10
				2020:1005)
					gsmget --at "AT+BMMODODR=1"
					;;
				*)
					echo "$config(3g): no service case for this VID:PID"
					;;
			esac
			;;
		
		# 2G preferred
		gprs)
			case "$evidpid" in
				# Option GTM661W
				0AF0:9000)
					gsmget --at "AT_OPSYS=2,2"
					;;
				# Telit HE910-EUD
				1BC7:0021)
					# NOTE: this device does not support 2G preferred, using auto instead
					gsmget --at "AT+WS46=25"
					;;
				# Sierra MC8700
				1199:68A3)
					gsmget --at "AT!SELRAT=04"
					gsmget --at "AT!BAND=00"
					;;
				# Huawei EM820W
				12D1:1404)
					gsmget --at "AT^SYSCFG=2,1,3FFFFFFF,1,2"
					;;
				# Broadmobi LP10
				2020:1005)
					gsmget --at "AT+BMMODODR=4"
					;;
				*)
					echo "$config(3g): no service case for this VID:PID"
					;;
			esac
			;;
		
		# 2G only
		gprs-only)
			case "$evidpid" in
				# Option GTM661W
				0AF0:9000)
					gsmget --at "AT_OPSYS=0,2"
					;;
				# Telit HE910-EUD
				1BC7:0021)
					gsmget --at "AT+WS46=12"
					;;
				# Sierra MC8700
				1199:68A3)
					gsmget --at "AT!SELRAT=02"
					gsmget --at "AT!BAND=05"
					;;
				# Huawei EM820W
				12D1:1404)
					gsmget --at "AT^SYSCFG=13,0,3FFFFFFF,1,2"
					;;
				# Broadmobi LP10
				2020:1005)
					gsmget --at "AT+BMMODODR=3"
					;;
				*)
					echo "$config(3g): no service case for this VID:PID"
					;;
			esac
			;;
		
		# automatic
		auto)
			case "$evidpid" in
				# Option GTM661W
				0AF0:9000)
					# NOTE: this device does not support auto, using 3g preferred instead
					gsmget --at "AT_OPSYS=3,2"
					;;
				# Telit HE910-EUD
				1BC7:0021)
					gsmget --at "AT+WS46=25"
					;;
				# Sierra MC8700
				1199:68A3)
					gsmget --at "AT!SELRAT=00"
					gsmget --at "AT!BAND=00"
					;;
				# Huawei EM820W
				12D1:1404)
					gsmget --at "AT^SYSCFG=2,0,3FFFFFFF,1,2"
					;;
				# Broadmobi LP10
				2020:1005)
					# NOTE: this device does not support auto, using 3g preferred instead
					gsmget --at "AT+BMMODODR=2"
					;;
				*)
					echo "$config(3g): no service case for this VID:PID"
					;;
			esac
			;;
		
		*)
			echo "$config(3g): service mode not supported."
			;;
	esac
	
	# Workaround to fucking GSMD
	sleep 1
	
	if [ "$(pidof gsmd)" ] 
	then
		echo "$config(3g): gsmd is running"
	else
		/etc/init.d/gsmd start
		sleep 3
	fi
		
	test -z "$pincode" || {
		if [ `gsmget --at "AT+CPIN?" | grep -c "READY"` -ne "1" ]
		then
			if [ `gsmget --at "AT+CPIN=\"$pincode\"" | grep -c "OK"` -ne "1" ]
			then
				echo "$config(3g): Failed to set the PIN code."
			else
				echo "$config(3g): PIN code set successfully."
			fi
		else
			echo "$config(3g): PIN not required."
		fi
		#PINCODE="$pincode" gcom -d "$device" -s /etc/gcom/setpin.gcom || {
		#	echo "$config(3g): Failed to set the PIN code."
		#	set_3g_led 0 0 0
		#	return 1
		#}
	}
	
	set_3g_led 1 0 0
	
	# don't start 3G connetion if it's not required
	local enabled
	config_get enabled "$config" enabled
	if [ $enabled == "0" ]; then
		echo "$config(3g): 3G not enabled."
		return 0
	fi
	
	# making chat script
	case "$service" in
		cdma |\
		evdo)
			rm -f $chat > /dev/null 2>&1
			printf "ABORT	BUSY\n" >> $chat
			printf "ABORT 	'NO CARRIER'\n" >> $chat
			printf "ABORT	ERROR\n" >> $chat
			printf "ABORT 	'NO DIAL TONE'\n" >> $chat
			printf "ABORT 	'NO ANSWER'\n" >> $chat
			printf "ABORT 	DELAYED\n" >> $chat
			printf "REPORT	CONNECT\n" >> $chat
			printf "TIMEOUT	10\n" >> $chat
			printf "'' 		AT\n" >> $chat
			printf "OK 		ATZ\n" >> $chat
			printf "SAY     'Calling CDMA/EVDO'\n" >> $chat
			printf "TIMEOUT	30\n" >> $chat
			printf "OK		ATD$dialnumber\n" >> $chat
			printf "CONNECT	''\n" >> $chat
			sync
			;;
				
		*)
			# default to UMTS
			rm -f $chat > /dev/null 2>&1
			printf "ABORT   BUSY\n" >> $chat
			printf "ABORT   'NO CARRIER'\n" >> $chat
			printf "ABORT   ERROR\n" >> $chat
			printf "REPORT  CONNECT\n" >> $chat
			printf "TIMEOUT 10\n" >> $chat
			printf "\"\"      ATZ\n" >> $chat
			printf "OK      \"AT&F\"\n" >> $chat
			printf "OK      \"ATE1\"\n" >> $chat
			
			if [ "$regmode" == "manual" ]
			then
				printf "OK      'AT+COPS=1,2,\"$mcc$mnc\",$radio'\n" >> $chat
			else
				printf "OK      \"AT+COPS=0,2\"\n" >> $chat
			fi
			
			printf "OK      'AT+CGDCONT=1,\"IP\",\"\$USE_APN\"'\n" >> $chat
			printf "SAY     \"Calling UMTS/GPRS\"\n" >> $chat
			printf "TIMEOUT 30\n" >> $chat
			printf "OK      \"ATD$dialnumber\"\n" >> $chat
			printf "CONNECT ' '\n" >> $chat
			sync
			;;
	esac
			
	config_set "$config" "connect" "${apn:+USE_APN=$apn }/usr/sbin/chat -t5 -v -E -f $chat"
	start_pppd "$config" \
		noaccomp \
		nopcomp \
		novj \
		nobsdcomp \
		noauth \
		lock \
		crtscts \
		${mtu:+mtu $mtu mru $mtu} \
		115200 "$device"
}
