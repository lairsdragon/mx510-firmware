#!/bin/sh
# Check for new sms messages every 60s

while true; do
	sleep 60
	if [ `uci get sms_utils.sms_reboot.enable` -eq 1 ] || [ `uci get sms_utils.status_send.enable` -eq 1 ] ; then
		comgt -d /dev/ttyUSB0 -s /sbin/sms_utils/sms_reboot.scr
	fi
done
