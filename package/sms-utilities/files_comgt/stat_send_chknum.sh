#!/bin/sh
# Compare sender phone number with phone numbers from config file

telnum_sender=$1
telnum_db=`uci get sms_utils.status_send.telnum`

echo $telnum_db | grep $telnum_sender >> /dev/null

if [ $? = 0 ]; then
	echo "Found"
else
	echo "Not Found"
fi
