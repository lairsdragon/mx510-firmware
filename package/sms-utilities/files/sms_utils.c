#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <libgsmd/libgsmd.h>
#include <libgsmd/pin.h>
#include <libgsmd/misc.h>
#include <libgsmd/sms.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <syslog.h>
#include <netinet/in.h>
#include <net/if.h>
#include <arpa/inet.h>

// uncomment DBG_FLAG to see all debug garbage!
//#define DBG_FLAG

#define BUF_LEN			512
#define SMS_TXT_LEN		160
#define SMS_PDU_LEN		300
#define NUMBER_LEN		16
#define READ_ERR		1
#define READ_OK			0

#ifdef DBG_FLAG
#define DBG(fmt, args...)	fprintf(stdout, fmt, ##args)
#else
#define DBG(fmt, args...)
#endif
#define ERR(fmt, args...)	fprintf(stderr, fmt, ##args)

/* -------------------------------------------------------------------------- */

char gVidPid[BUF_LEN / 16];
char logBuffer[256];
struct lgsm_handle *lgsmh;
int gsm_fd;
int totalMessages = 0;

typedef struct {
	char SMSC_Length;
	char SMSC_Type;
	char SMSC_Number[32];
	char FO_SMSD;
	char AddrLength;
	char AddrType;
	char Sender[32];
	char TP_PID;
	char TP_DCS;
	char TimeStamp[24];
	char DataLength;
	char Data[161];
} SMS;

/* -------------------------------------------------------------------------- */

int message_parser(char *pBuffer, int iBufLength, SMS *pSMS);
int Check_New_MSG_Telit(char *rx_buf, int Index);
void Delete_SMS(unsigned char u8index);
char Send_SMS_Text(char *sTxt, char *sNumber);
void Send_Router_Status(char *psNumber);
char Send_Router_Status_Telit(char *psNumber);
char Status_SMS_After_Reboot();
char SMS_Text_Reader(char *pSenderNumber, char *pTxtSMS, unsigned char SmsDelIndex);
static void timeout(int sig);
void quit(int exit_code);
void Reboot_System(void);
static int sms_handler(struct lgsm_handle *lh, struct gsmd_msg_hdr *gmh);
static char send_cmgr_cmd(int msgIndex, char *rxBuffer, int *rxLen);
static int lgsm_packet_handler();
char testing_debug(char *arg);
char sms_count_in_mem(int *total, int *used);

/* -------------------------------------------------------------------------- */

char testing_debug(char *arg)
{
	
}

/*
 * message_parser
 */
int message_parser(char *pBuffer, int iBufLength, SMS *pSMS)
{
	char *ascii, tmp[170];
	int addrLenLoop;
	int index, i, j;
	
	index = 0;
	pSMS->SMSC_Length = pBuffer[index++];
	
	pSMS->SMSC_Type = pBuffer[index++];
	
	// pSMS.SMSC_Length - 1 <- because SMSC_Type is already filled
	for (j = 0, i = 0; j < pSMS->SMSC_Length - 1; j++) {
		pSMS->SMSC_Number[i] = (pBuffer[index] & 0x0F) + 0x30;
		i++;
		pSMS->SMSC_Number[i] = ((pBuffer[index] & 0xF0) >> 4) + 0x30;
		i++;
		index++;
	}
	// now we will remove 'F' at the end of SMSC_Number or insert '\0' otherwise
	pSMS->SMSC_Number[i] = '\0';
	if (pSMS->SMSC_Number[i-1] == 0x3F)
		pSMS->SMSC_Number[i-1] = '\0';
	
	pSMS->FO_SMSD = pBuffer[index++];
	
	pSMS->AddrLength = pBuffer[index++];
	if ((pSMS->AddrLength % 2) != 0)
		addrLenLoop = (pSMS->AddrLength / 2) + 1;
	else
		addrLenLoop = pSMS->AddrLength / 2;

	pSMS->AddrType = pBuffer[index++];

	for (j = 0, i = 0; j < addrLenLoop; j++) {
		pSMS->Sender[i] = (pBuffer[index] & 0x0F) + 0x30;
		i++;
		pSMS->Sender[i] = ((pBuffer[index] & 0xF0) >> 4) + 0x30;
		i++;
		index++;
	}
	// now we will remove 'F' at the end of SMSC_Number or insert '\0' otherwise
	pSMS->Sender[i] = '\0';
	if (pSMS->Sender[i-1] == 0x3F)
		pSMS->Sender[i-1] = '\0';
	
	pSMS->TP_PID = pBuffer[index++];
	
	pSMS->TP_DCS = pBuffer[index++];
	
	for (i = 0, j = 0; i < 7; i++) {
		tmp[j++] = pBuffer[index] & 0x0F;
		tmp[j++] = (pBuffer[index++] & 0xF0) >> 4;
	}
	// convert time stamp to human-readable format
	sprintf(pSMS->TimeStamp, "20%i%i-%i%i-%i%i %i%i:%i%i:%i%i", tmp[0], tmp[1], tmp[2],
			tmp[3], tmp[4], tmp[5], tmp[6], tmp[7], tmp[8], tmp[9], tmp[10], tmp[11]);
	
	pSMS->DataLength = pBuffer[index++];
	
	i = 0;
	while (index < iBufLength) {
		tmp[i] = pBuffer[index++];
		i++;
	}
	tmp[i] = '\0';
	pdu_to_ascii(tmp, i, &ascii);
	// FIXME: god damn issues with null terminating in function above. 
	// No idea why the hell function do not add '\0' at the end of string!
	ascii[pSMS->DataLength] = '\0';
	strcpy(pSMS->Data, ascii);
	free(ascii);

#ifdef DBG_FLAG
	DBG("SMSC_Length: %i\n", pSMS->SMSC_Length);
	DBG("SMSC_Type: %02X\n", pSMS->SMSC_Type);
	DBG("SMSC_Number: %s\n", pSMS->SMSC_Number);
	DBG("FO_SMSD: %02X\n", pSMS->FO_SMSD);
	DBG("AddrLength: %i\n", pSMS->AddrLength);
	DBG("AddrType: %02X\n", pSMS->AddrType);
	DBG("Sender: %s\n", pSMS->Sender);
	DBG("TP_PID: %02X\n", pSMS->TP_PID);
	DBG("TP_DCS: %02X\n", pSMS->TP_DCS);
	DBG("TimeStamp: %s\n", pSMS->TimeStamp);
	DBG("DataLength: %i\n", pSMS->DataLength);
	DBG("Data: %s\n", pSMS->Data);
#endif	
	return 0;
}

/*
 *  Check_New_MSG_Telit
 */
int Check_New_MSG_Telit(char *rx_buf, int Index)
{
	SMS strNewSMS;
	char *pChar;
	char SMSTextPDU[300], smsTxt[161], Number[16], smsNumber[18];
	int rlen = 256, i, j;
	int Stat, iMessageSizePDU;
	
	if (pChar = strchr(rx_buf, '\n'))
	{
		DBG("Incoming MSG\n");
		strcpy(SMSTextPDU, pChar+1);	
		DBG("SMS TEXT PDU:%s\n", SMSTextPDU);
		iMessageSizePDU = strlen(SMSTextPDU);
		
		j = 0;
		// kadangi PDU parseris skirtas apdorot hex skaiciam, o ne hex string, modifikuojam buferi.
		for (i = 0; i < iMessageSizePDU; i++)
		{
			SMSTextPDU[j] = ((SMSTextPDU[i] <= '9') ? SMSTextPDU[i] - 0x30 : SMSTextPDU[i] - 0x37) << 4;
			i++;
			SMSTextPDU[j] |= (SMSTextPDU[i] <= '9') ? SMSTextPDU[i] - 0x30 : SMSTextPDU[i] - 0x37;
			j++;
		}
		iMessageSizePDU = j;
		message_parser(SMSTextPDU, iMessageSizePDU, &strNewSMS);
		strcpy(smsNumber, "+");
		strcat(smsNumber, strNewSMS.Sender);
		strcpy(smsTxt, strNewSMS.Data);
		DBG("TEXT:%s NUMBER:%s\n", smsTxt, smsNumber);
		sprintf(logBuffer, "new SMS received. Text: %s. Sender: %s", smsTxt, smsNumber);
		syslog(LOG_NOTICE, logBuffer);
		SMS_Text_Reader(smsNumber, smsTxt, Index);
	}
}

/*
 *  Delete_SMS
 */
void Delete_SMS(unsigned char u8index)
{
	struct lgsm_sms_delete sms_del;
  	
  	sms_del.index = u8index;
	sms_del.delflg = 0; // delete messages by index
	lgsm_sms_delete(lgsmh, &sms_del);
	lgsm_packet_handler();
}

/*
 * Send_SMS_Text
 */
char Send_SMS_Text(char *sTxt, char *sNumber)
{
	struct lgsm_sms sms;
	int ret;
	int cnt_7bit;
	char *pdu;
	int pdu_len;
	
	sms.ask_ds = 0; // ask device status
	strcpy(sms.addr, sNumber);
	pdu_len = ascii_to_pdu(sTxt, &pdu, &cnt_7bit);
	sms.length = cnt_7bit;
	memcpy(sms.data, pdu, pdu_len); 
	//packing_7bit_character(sTxt, &sms); // <- do not use this! 
	DBG("SENDING\n");
	lgsm_sms_send(lgsmh, &sms);
	free(pdu);
	lgsm_packet_handler();
	return 0;
}

/*
 * Send_Router_Status
 */
void Send_Router_Status(char *psNumber)
{
	/* Telit HE910-EUD */
	if (!strcmp(gVidPid, "1BC7:0021\n"))
		Send_Router_Status_Telit(psNumber);
	else
		return;
}

/*
 * Send_Router_Status_Telit
 */
char Send_Router_Status_Telit(char *psNumber)
{
	FILE *pCMD;
	char *pTxt;
	char ret, cConnected = 0;
	char BufferInfo[BUF_LEN];
	char BufferSMS[SMS_TXT_LEN];
	char sOperator[50], sSignal[10], sConnection[20], sIP[30];
	
	pCMD = popen("gsmget -s", "r");
	fgets(BufferInfo, BUF_LEN, pCMD);
	BufferInfo[strlen(BufferInfo)-1] = '\0'; //remove new line
	if (!strcmp(BufferInfo, "connected"))
		cConnected = 1;

	if (!cConnected) {
		pCMD = popen("gsmget --operator --signal", "r");
		fgets(BufferInfo, BUF_LEN, pCMD);
		BufferInfo[strlen(BufferInfo)-1] = '\0';
		strncpy(sOperator, BufferInfo, sizeof(sOperator));
		fgets(BufferInfo, BUF_LEN, pCMD);
		BufferInfo[strlen(BufferInfo)-1] = '\0';
		strncpy(sSignal, BufferInfo, sizeof(sSignal));
		sprintf(BufferSMS, "3G state: diconnected. Operator %s. Signal strength: %s dBm.", sOperator, sSignal);
		
	} else {	
		pCMD = popen("gsmget --ip 3g-ppp --conntype --operator --signal", "r");
		fgets(BufferInfo, BUF_LEN, pCMD);
		BufferInfo[strlen(BufferInfo)-1] = '\0';
		strncpy(sIP, BufferInfo, sizeof(sIP));
		fgets(BufferInfo, BUF_LEN, pCMD);
		BufferInfo[strlen(BufferInfo)-1] = '\0';
		strncpy(sConnection, BufferInfo, sizeof(sConnection));
		fgets(BufferInfo, BUF_LEN, pCMD);
		BufferInfo[strlen(BufferInfo)-1] = '\0';
		strncpy(sOperator, BufferInfo, sizeof(sOperator));
		fgets(BufferInfo, BUF_LEN, pCMD);
		BufferInfo[strlen(BufferInfo)-1] = '\0';
		strncpy(sSignal, BufferInfo, sizeof(sSignal));
		sprintf(BufferSMS, "3G state: connected. IP: %s. Connection type: %s. Operator: %s. Signal strength: %s dBm.", sIP, sConnection, sOperator, sSignal);
	}	
	DBG("Receiver number:%s\n", psNumber);
	DBG("SMS text:%s\n", BufferSMS);
	Send_SMS_Text(BufferSMS, psNumber);
	pclose(pCMD);
}

/*
 * Status_SMS_After_Reboot
 */
char Status_SMS_After_Reboot()
{
	FILE *pCMD;
	char Buffer[32];
	char cSendStatusFlag = 0;
	
	pCMD = popen("uci get sms_utils.reboot_set.reboot", "r");
	fgets(Buffer, 3, pCMD);
	Buffer[strlen(Buffer)-1] = '\0'; //remove new line
	if (!strcmp(Buffer, "1"))
		cSendStatusFlag = 1;

	if (cSendStatusFlag) {
		pCMD = popen("uci get sms_utils.reboot_set.telnum", "r");
		fgets(Buffer, 16, pCMD);
		Buffer[strlen(Buffer)-1] = '\0'; //remove new line
		if (strlen(Buffer) > 3)
			Send_Router_Status(Buffer);
		system("uci set sms_utils.reboot_set.reboot=0 && uci delete sms_utils.reboot_set.telnum && uci commit sms_utils");
	}
}

/*
 * SMS_Text_Reader
 */
char SMS_Text_Reader(char *pSenderNumber, char *pTxtSMS, unsigned char SmsDelIndex)
{
	char successFlag;
	struct enableFlags {
		char reboot[4];
		char status[4];
		char reset[4];
		char config[4];
	} Enable;
	int i;
	char resetSettingsFlag = 0;
	FILE *pCMD;
	char ret;
	char cmdBuffer[161];
	char uciTxt[160], uciNumber[130], uciEnableTxt[4], uciEnableStat[4];
	char *pNumberFound;
	
	// check which function is enabled
	pCMD = popen("uci get sms_utils.sms_reboot.enable", "r");
	fgets(Enable.reboot, 4, pCMD);
	Enable.reboot[1] = '\0';
	pCMD = popen("uci get sms_utils.status_send.enable", "r");
	fgets(Enable.status, 4, pCMD);
	Enable.status[1] = '\0';
	pCMD = popen("uci get sms_utils.reset_settings.enable", "r");
	fgets(Enable.reset, 4, pCMD);
	Enable.reset[1] = '\0';
	pCMD = popen("uci get sms_utils.set_settings.enable", "r");
	fgets(Enable.config, 4, pCMD);
	Enable.config[1] = '\0';
	
	// ----- SMS REBOOT -----	
	if (!strcmp(Enable.reboot, "1")) {
		strcpy(uciTxt, "");
		pCMD = popen("uci get sms_utils.sms_reboot.psw", "r");
		fgets(uciTxt, sizeof(uciTxt), pCMD);
		uciTxt[strlen(uciTxt)-1] = '\0'; //remove new line
		strcpy(uciNumber, "");
		pCMD = popen("uci get sms_utils.sms_reboot.telnum", "r");
		fgets(uciNumber, sizeof(uciNumber), pCMD);
		uciNumber[strlen(uciNumber)-1] = '\0';
		strcpy(uciEnableStat, "");
		pCMD = popen("uci get sms_utils.sms_reboot.status_sms", "r");
		fgets(uciEnableStat, sizeof(uciEnableStat), pCMD);
		uciEnableStat[strlen(uciEnableStat)-1] = '\0';
		
		successFlag = 0;
		if (!strcmp(uciTxt, pTxtSMS)) {
			if (uciNumber[0] == 0)
				successFlag = 1;
			else if (strstr(uciNumber, pSenderNumber))
				successFlag = 1;
		}
		
		if (successFlag) {
			if (!strcmp(uciEnableStat, "1")) {
				sprintf(uciTxt, "uci set sms_utils.reboot_set.reboot=1 && uci set sms_utils.reboot_set.telnum=%s && uci commit sms_utils", pSenderNumber);
				system(uciTxt);
			}	
			sleep(2);
			Delete_SMS(SmsDelIndex);
			Reboot_System();
			DBG("Rebooting system...");
			quit(0);
		}
	}
	
	// ----- STATUS BY SMS -----
	if (!strcmp(Enable.status, "1")) {
		strcpy(uciTxt, "");
		pCMD = popen("uci get sms_utils.status_send.psw", "r");
		fgets(uciTxt, sizeof(uciTxt), pCMD);
		uciTxt[strlen(uciTxt)-1] = '\0'; //remove new line
		strcpy(uciNumber, "");
		pCMD = popen("uci get sms_utils.status_send.telnum", "r");
		fgets(uciNumber, sizeof(uciNumber), pCMD);
		uciNumber[strlen(uciNumber)-1] = '\0';
		
		successFlag = 0;
		if (!strcmp(uciTxt, pTxtSMS)) {
			if (uciNumber[0] == 0)
				successFlag = 1;
			else if (strstr(uciNumber, pSenderNumber))
				successFlag = 1;
		}
		
		if (successFlag)
			Send_Router_Status(pSenderNumber);
	}
	
	// ----- RESET CONFIGURATION -----
	if (!strcmp(Enable.reset, "1")) {
		strcpy(uciTxt, "");
		pCMD = popen("uci get sms_utils.reset_settings.psw", "r");
		fgets(uciTxt, sizeof(uciTxt), pCMD);
		uciTxt[strlen(uciTxt)-1] = '\0'; //remove new line
		strcpy(uciNumber, "");
		pCMD = popen("uci get sms_utils.reset_settings.telnum", "r");
		fgets(uciNumber, sizeof(uciNumber), pCMD);
		uciNumber[strlen(uciNumber)-1] = '\0';
		
		successFlag = 0;
		if (!strcmp(uciTxt, pTxtSMS)) {
			if (uciNumber[0] == 0)
				successFlag = 1;
			else if (strstr(uciNumber, pSenderNumber))
				successFlag = 1;
		}
		
		if (successFlag)
			resetSettingsFlag = 1;
	}
	
	// ----- SET CONFIGURATION -----
		if (!strcmp(Enable.config, "1")) {
			strcpy(uciNumber, "");
			pCMD = popen("uci get sms_utils.set_settings.telnum", "r");
			fgets(uciNumber, sizeof(uciNumber), pCMD);
			uciNumber[strlen(uciNumber)-1] = '\0';
			
			successFlag = 0;
			if (pTxtSMS[0] == 'u' && pTxtSMS[1] == 'c' && pTxtSMS[2] == 'i' && pTxtSMS[3] == ' ') {
				if (uciNumber[0] == 0)
					successFlag = 1;
				else if (strstr(uciNumber, pSenderNumber))
					successFlag = 1;
			}
			
			if (successFlag)
				system(pTxtSMS);
	}
	
	// ----- TEH END. TODO: rework is needed above -----
	sleep(2);
	Delete_SMS(SmsDelIndex);
	
	if (resetSettingsFlag) {
		system("/etc/init.d/modem stop");
		system("killall dropbear uhttpd; sleep 1; mtd -r erase rootfs_data");
		quit(2);
	}
	return 0;
}	

/*
 * timeout handler
 */
static void timeout(int sig)
{
	closelog();
	DBG("no response, exit\n");
	exit(0);
}

/*
 * quit
 */
void quit(int exit_code)
{
	closelog();
	if (lgsmh)
		lgsm_exit(lgsmh);
	exit(exit_code);
}

/*
 * Reboot_System
 */
void Reboot_System(void)
{
	syslog(LOG_NOTICE, "rebooting router");
	system("/sbin/reboot");
}

/*
 * sms_handler
 */
static int sms_handler(struct lgsm_handle *lh, struct gsmd_msg_hdr *gmh)
{
	char payload[GSMD_SMS_DATA_MAXLEN];
	int *result;
	struct gsmd_sms_list *sms;
	struct gsmd_addr *addr;
	struct gsmd_sms_storage *mem;
	static const char *msgtype[] = {"Unread", "Received", "Unsent", "Sent"};
	static const char *memtype[] = {"Unknown", "Broadcast", "Me message", "MT", "SIM", "TA", "SR"};
	char sSenderNumber[32];
	DBG("SMS handler function\n");

	switch (gmh->msg_subtype) {
	case GSMD_SMS_LIST:
	case GSMD_SMS_READ:
		sms = (struct gsmd_sms_list *) ((void *) gmh + sizeof(*gmh));
		if(sms->payload.is_voicemail)
			DBG("it's a voicemail \n");
		DBG("%s message %i from/to %s%s, at %i%i-%i%i-%i%i "
				"%i%i:%i%i:%i%i, GMT%c%i\n",
				msgtype[sms->stat], sms->index,
				((sms->addr.type & __GSMD_TOA_TON_MASK) ==
				 GSMD_TOA_TON_INTERNATIONAL) ? "+" : "",
				sms->addr.number,
				sms->time_stamp[0] & 0xf,
				sms->time_stamp[0] >> 4,
				sms->time_stamp[1] & 0xf,
				sms->time_stamp[1] >> 4,
				sms->time_stamp[2] & 0xf,
				sms->time_stamp[2] >> 4,
				sms->time_stamp[3] & 0xf,
				sms->time_stamp[3] >> 4,
				sms->time_stamp[4] & 0xf,
				sms->time_stamp[4] >> 4,
				sms->time_stamp[5] & 0xf,
				sms->time_stamp[5] >> 4,
				(sms->time_stamp[6] & 8) ? '-' : '+',
				(((sms->time_stamp[6] << 4) |
				  (sms->time_stamp[6] >> 4)) & 0x3f) >> 2);
		if (sms->payload.coding_scheme == ALPHABET_DEFAULT) {
			unpacking_7bit_character(&sms->payload, payload);
			DBG("\"%s\"\n", payload);
		} else if (sms->payload.coding_scheme == ALPHABET_8BIT)
			DBG("8-bit encoded data\n");
		else if (sms->payload.coding_scheme == ALPHABET_UCS2)
			DBG("Unicode-16 encoded text\n");
		if (sms->is_last)
			DBG("Last sms");
			
		// read sms text and decide what to do next
		sprintf(sSenderNumber, "%s%s", ((sms->addr.type & __GSMD_TOA_TON_MASK) == GSMD_TOA_TON_INTERNATIONAL) ? "+" : "", sms->addr.number);
		SMS_Text_Reader(sSenderNumber, payload, sms->index);
		
		break;
	case GSMD_SMS_SEND:

		result = (int *) ((void *) gmh + sizeof(*gmh));
		if (*result >= 0) {
			DBG("Send: message sent as ref %i\n", *result);
			syslog(LOG_NOTICE, "SMS sent successfully");
			break;
		}

		switch (-*result) {
		case 42:
			DBG("Store: congestion\n");
			break;
		default:
			DBG("Store: error %i\n", *result);
			break;
		}
		break;
	case GSMD_SMS_WRITE:
		
		result = (int *) ((void *) gmh + sizeof(*gmh));
		if (*result >= 0) {
			DBG("Store: message stored with index %i\n",
					*result);
			break;
		}

		switch (-*result) {
		case GSM0705_CMS_SIM_NOT_INSERTED:
			DBG("Store: SIM not inserted\n");
			break;
		default:
			DBG("Store: error %i\n", *result);
			break;
		}
		break;
	case GSMD_SMS_DELETE:
		
		result = (int *) ((void *) gmh + sizeof(*gmh));
		switch (*result) {
		case 0:
			DBG("SMS delete: success\n");
			syslog(LOG_NOTICE, "SMS delete success\n");
			break;
		case GSM0705_CMS_SIM_NOT_INSERTED:
			DBG("Delete: SIM not inserted\n");
			break;
		case GSM0705_CMS_INVALID_MEMORY_INDEX:
			DBG("Delete: invalid memory index\n");
			break;
		default:
			DBG("Delete: error %i\n", *result);
			break;
		}
		break;
	case GSMD_SMS_GET_MSG_STORAGE:
		mem = (struct gsmd_sms_storage *)
			((void *) gmh + sizeof(*gmh));
		totalMessages = mem->mem[0].total;
		
		// if memory is empty, why we should scan messages then?
		if (!mem->mem[0].used)
			totalMessages = 0;
		
		DBG("mem1: %s (%i)       Occupied: %i / %i\n",
				memtype[mem->mem[0].memtype],
				mem->mem[0].memtype,
				mem->mem[0].used,
				mem->mem[0].total);
		DBG("mem2: %s (%i)       Occupied: %i / %i\n",
				memtype[mem->mem[1].memtype],
				mem->mem[1].memtype,
				mem->mem[1].used,
				mem->mem[1].total);
		DBG("mem3: %s (%i)       Occupied: %i / %i\n",
				memtype[mem->mem[2].memtype],
				mem->mem[2].memtype,
				mem->mem[2].used,
				mem->mem[2].total);
		
		break;
	case GSMD_SMS_GET_SERVICE_CENTRE:
		addr = (struct gsmd_addr *) ((void *) gmh + sizeof(*gmh));
		DBG("Number of the default Service Centre is %s\n",
				addr->number);
		
		break;
	default:
		
		return -EINVAL;
	}
	return 0;
}

/*
 * main
 */
int main(int argc, char **argv)
{
	FILE *pCMD;
	char at_buf[BUF_LEN];
	int rlen = BUF_LEN;
	int ret;
	char rxBuf[BUF_LEN];
	int rxLen = BUF_LEN;
	int ii;
	int totalSMS;
	int usedSMS;
	const char ident[] = "SMS_utilities";
	
	/* update device vid:pid first */
	pCMD = popen("echo \"`uci get system.module.vid`:`uci get system.module.pid`\"", "r");
	fgets(gVidPid, 32, pCMD);
	
	signal(SIGALRM, timeout);
	alarm(30);
	
	/* Connect to gsmd daemon */
	lgsmh = lgsm_init(LGSMD_DEVICE_GSMD);
	if (!lgsmh) {
		ERR("Can't connect to gsmd\n");
		exit(1);
	}

   	gsm_fd = lgsm_fd(lgsmh);
   	if (gsm_fd <= 0)
   		ERR("Bad file descriptor\n");
	
	/* Telit HE910-EUD SMS kind of initialization */
	if (!strcmp(gVidPid, "1BC7:0021\n")) {
		if (lgsm_passthrough(lgsmh, "AT+CSMP=17,167,0,16", at_buf, &rlen) <= 0)
			fprintf(stderr, "Can't initialize SMS in Telit modem\n");
	}
	
	openlog(ident, LOG_CONS | LOG_NDELAY, LOG_USER);
	
	/* register phone handler */
	lgsm_register_handler(lgsmh, GSMD_MSG_SMS, sms_handler);
	
	Status_SMS_After_Reboot(lgsmh);
	
	/* Telit HE910-EUD */
	if (!strcmp(gVidPid, "1BC7:0021\n")) {
		// count total messages in memory, starting from 1
		if (sms_count_in_mem(&totalSMS, &usedSMS) == READ_OK) {
			if (usedSMS == 0)
				totalMessages = 0; // do not loop
			else
				totalMessages = totalSMS;
		} else {
			ERR("Total/Used SMS count error");
			closelog();
			quit(0);
		}	
		
		for (ii = 1; ii <= totalMessages; ii++) {
			rxLen = BUF_LEN;
			if (send_cmgr_cmd(ii, rxBuf, rxLen) == READ_OK) {
				//sleep(2);
				Check_New_MSG_Telit(rxBuf, ii);
			}
		}	
	}

	closelog();
	quit(0);
	return 0;
}

static char send_cmgr_cmd(int msgIndex, char *rxBuffer, int *rxLen)
{
	char atBuffer[32];

	sprintf(atBuffer, "AT+CMGR=%i", msgIndex);
	if (lgsm_passthrough(lgsmh, atBuffer, rxBuffer, &rxLen) <= 0)
		return READ_ERR;
	
	if (!rxBuffer[0])
		return READ_ERR;

	if (!strncmp(rxBuffer, "OK", 2))
		return READ_ERR;
		
	return READ_OK;	
}

static int lgsm_packet_handler()
{
	int rc = 0;
	char buf[BUF_LEN]; 
	
	rc = read(gsm_fd, buf + rc, sizeof(buf) - rc) + rc;
	if (rc <= 0) {
		ERR("ERROR reading from gsm_fd\n");
		quit(2);
	}
	
	rc = lgsm_handle_packet(lgsmh, buf, rc);
	if (rc < 0) {
		fprintf(stderr, "ERROR processing packet: %d(%s)\n", rc, strerror(-rc));
		rc = 0;
	}
	
	return 0;
}

char sms_count_in_mem(int *total, int *used)
{
	char atBuffer[32];
	char rxBuffer[128];
	int rxLen = 128;
	int used_, total_;

	sprintf(atBuffer, "AT+CPMS?");
	if (lgsm_passthrough(lgsmh, atBuffer, rxBuffer, &rxLen) <= 0)
		return READ_ERR;
	
	if (sscanf(rxBuffer, "+CPMS: \"SM\",%d,%d,", &used_, &total_) != 2)
		return READ_ERR;
	
	*total = total_;
	*used = used_;

	return READ_OK;
}