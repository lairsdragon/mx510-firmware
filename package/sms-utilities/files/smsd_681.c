#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <malloc.h>
#include <signal.h>
#include <signal.h>
#include <getopt.h>
// Gobi API and uci API
#include "GobiConnectionMgmtAPI.h"
#include "uci.h"

#include "smsd_681.h"

/************************************************
* debug
************************************************/
//#define DBG_FLAG
#ifdef DBG_FLAG
#define DBG(fmt, args...)	fprintf(stderr, fmt, ##args)
#else
#define DBG(fmt, args...)
#endif
#define ERR(fmt, args...)	fprintf(stderr, fmt, ##args)

/************************************************
* global variables
************************************************/
static char uci_search_result[UCI_ENTRY_SIZE];
global_status modem_status;
global_config modem_configuration;
ULONG session_id = 0;
char ifname[RX_BUF_LEN / 16];

/************************************************
* prototypes
************************************************/
// gobi API related
int init_gapi(void);
int close_gapi(void);
int is_gapi(void);
// UCI config related
int load_config(global_config *gconfig);
char *get_config_entry(struct uci_context *ctx, char *entry_name);
// SMS
void Get_SMS_Messages_List();
void Get_SMS_Message_Text(ULONG messageIndex, ULONG storageType);
ULONG Form_Message_In_PDU(char *pTelNumber, char *pText, ULONG *pMessageFailureCode);
char Status_SMS_After_Reboot();
char Send_Router_Status(char *psNumber);
int message_parser(char buf[], int buf_len, char *SMS_txt, char *SMS_Number);
void NewSMSCallback(ULONG storageType, ULONG messageIndex);
// Rounter info
static int gsmget_get_connstate(char *output);
static int gsmget_get_ifacetype(char *output);
static int gsmget_get_ip(char *output);
static int gsmget_get_operator(char *output);
static int gsmget_get_signal(char *output);
static void timeout_handler(int sig);
/************************************************
* gsmget_get_ifacetype
************************************************/
static int gsmget_get_ifacetype(char *output) 
{
	FILE *pCMD;
	if (!(pCMD = popen("gobiconnect get bearer", "r")))
		return READ_ERR;
	if (!fgets(output, 20, pCMD))
		return READ_ERR;
	output[strlen(output)-1] = '\0';
	pclose(pCMD);
	return 0;
}

/************************************************
* gsmget_get_ip
************************************************/
static int gsmget_get_ip(char *output)
{
	FILE *pCMD;
	if (!(pCMD = popen("gobiconnect get ip", "r")))
		return READ_ERR;
	if (!fgets(output, 30, pCMD))
		return READ_ERR;
	output[strlen(output)-1] = '\0';
	pclose(pCMD);
	return 0;
}

/************************************************
*  gsmget_get_connstate
************************************************/
static int gsmget_get_connstate(char *output)
{
	FILE *pCMD;
	if (!(pCMD = popen("gobiconnect get dstate", "r")))
		return READ_ERR;
	if (!fgets(output, 50, pCMD))
		return READ_ERR;
	output[strlen(output)-1] = '\0';
	pclose(pCMD);
	return 0;
}

/************************************************
* gsmget_get_operator
************************************************/
static int gsmget_get_operator(char *output) 
{
	FILE *pCMD;
	if (!(pCMD = popen("gobiconnect get netname", "r")))
		return READ_ERR;
	if (!fgets(output, 50, pCMD))
		return READ_ERR;
	output[strlen(output)-1] = '\0';
	pclose(pCMD);
	return 0;
}

/************************************************
* gsmget_get_signal
************************************************/
static int gsmget_get_signal(char *output) 
{
	FILE *pCMD;
	if (!(pCMD = popen("gobiconnect get signal", "r")))
		return READ_ERR;
	if (!fgets(output, 10, pCMD))
		return READ_ERR;
	output[strlen(output)-1] = '\0';
	pclose(pCMD);
	return 0;
}

/************************************************
* Status_SMS_After_Reboot
************************************************/
char Status_SMS_After_Reboot()
{
	FILE *pCMD;
	char Buffer[32];
	char cSendStatusFlag = 0;
	
	if(!(pCMD = popen("uci get sms_utils.reboot_set.reboot", "r")))
	{
		ERR("uci config read failed (reboot_set.enable).");
		return -1;
	}	
	else
	{
		fgets(Buffer, 3, pCMD);
		Buffer[strlen(Buffer)-1] = '\0'; //remove new line
		if (!strcmp(Buffer, "1"))
			cSendStatusFlag = 1;	
	}
	
	if (cSendStatusFlag)
	{
		if(!(pCMD = popen("uci get sms_utils.reboot_set.telnum", "r")))
		{
			ERR("uci config read failed (reboot_set.enable).");
			return -1;
		}	
		else
		{
			fgets(Buffer, 16, pCMD);
			Buffer[strlen(Buffer)-1] = '\0'; //remove new line
			if (strlen(Buffer) > 3)
			{
				sleep(120);
				Send_Router_Status(Buffer);
			}	
			else
				ERR("Wrong number (status after reboot)");
		}
		system("uci set sms_utils.reboot_set.reboot=0 && uci delete sms_utils.reboot_set.telnum && uci commit sms_utils");
	}
}

/************************************************
* Send_Router_Status
************************************************/
char Send_Router_Status(char *psNumber)
{
	int ret;
	char Buffer[161];
	char sConnState[50], sOperator[50], sSignal[10], sConnection[20], sIP[30];
	ULONG *pMessageFailureCode = 0;
	
	if ((ret = gsmget_get_connstate(sConnState)))
		ERR("Router status read error: %d\n", ret);
	
	if (!strcmp(sConnState, "diconnected"))
	{
		gsmget_get_operator(sOperator);
		gsmget_get_signal(sSignal);
		sprintf(Buffer, "3G state: diconnected. Operator %s. Signal strength: %s dBm.", sOperator, sSignal);
	}
	else
	{	
		gsmget_get_ip(sIP);
		gsmget_get_ifacetype(sConnection);
		gsmget_get_operator(sOperator);
		gsmget_get_signal(sSignal);
		sprintf(Buffer, "3G state: connected. IP: %s. Connection type: %s. Operator: %s. Signal strength: %s dBm.", sIP, sConnection, sOperator, sSignal);
	}
	
	DBG("Receiver number: %s\n", psNumber);
	DBG("SMS text: %s\n", Buffer);
	ret = Form_Message_In_PDU(psNumber, Buffer, &pMessageFailureCode);
}

/************************************************
* Form_Message_In_PDU
************************************************/
ULONG Form_Message_In_PDU(char *psTelNumber, char *psSMStext, ULONG *pMessageFailureCode)
{
	unsigned char *pSMStextPDU, sTelNumber[32], sTelNumberPDU[16], Buffer[300]; //TODO pakeist i optimalu
	int i, j, iTelNumberLength, iSMSLengthPDU;
	ULONG iStatus, messageFormat, messageSize;
	BYTE *pMessage;

	iTelNumberLength = strlen(psTelNumber);
	
	Buffer[0] = 0;
	Buffer[1] = 0x11;
	Buffer[2] = 0;
	Buffer[3] = iTelNumberLength;
	Buffer[4] = 0x91;

	strcpy(sTelNumber, psTelNumber);
	if ((iTelNumberLength % 2) != 0) // papildomas 0xFF, jei tel. numerio ilgis nelyginis
	{
		sTelNumber[iTelNumberLength] = 0xFF;
		iTelNumberLength++;
		sTelNumber[iTelNumberLength] = '\0';
	}
	
	for (i = 0, j = 0; i < (iTelNumberLength / 2); i++)
	{
		Buffer[5+i] = (sTelNumber[j] - 0x30) + ((sTelNumber[j+1] - 0x30)<<4);
		j += 2;
	}

	Buffer[5+i] = 0;
	Buffer[6+i] = 0;
	Buffer[7+i] = 0xAA;

	iSMSLengthPDU = ascii_to_pdu(psSMStext, &pSMStextPDU);
	Buffer[8+i] = strlen(psSMStext);
	
	j = 0;
	while(j < iSMSLengthPDU)
	{
		Buffer[9+i] = pSMStextPDU[j];
		i++;
		j++;
	}
	free(pSMStextPDU);
	Buffer[9+i] = '\0';

	messageFormat = 6;
	messageSize = 9 + i;
#ifdef DBG_FLAG
	DBG("PDU: ");
	for (j = 0; j < messageSize;)
	{
		DBG("%02X", Buffer[j]);
		j++;
	}
	DBG("\n");
	DBG("SMS text length: %d\n", strlen(psSMStext));
	DBG("PDU SMS text length: %d\n", iSMSLengthPDU);
#endif
	iStatus = SendSMS(messageFormat, messageSize, &Buffer, &pMessageFailureCode);

	return iStatus;
}

/************************************************
* Get_SMS_Messages_List
************************************************/
void Get_SMS_Messages_List()
{
	ULONG status, i, ret;
   ULONG storageType = 0; 
   ULONG *pRequestedTag, *pMessageIndex =2, *pMessageTag = 0;
   ULONG *pMessageListSize = 20; 
   BYTE pMessageList[20*16] = {0};
   
   
  // if ((ret = DeleteSMS(storageType, &pMessageIndex, &pMessageTag)))
	//	ERR("SMS delete failed: %d\n", ret);
   
   GetSMSList(storageType, &pRequestedTag, &pMessageListSize, &pMessageList);
   
   //printf("size: %d\n", sizeof(pMessageList));
   for (i = 0; i < sizeof(pMessageList); i++) {
   	if (pMessageList[i])
   		printf("%02X ", pMessageList[i]);	
	}
	return;
}

/************************************************
* Get_SMS_Message_Text
************************************************/
void Get_SMS_Message_Text(ULONG storageType, ULONG messageIndex)
{
	FILE *pCMD;
	ULONG *pMessageFormat, *pMessageTag, *pMessageSizePDU;
	ULONG *pMessageIndex;
	CHAR pMessagePDU[300]; // TODO pakeist i optimalu
	pMessageSizePDU = 300; // TODO pakeist i optimalu
	int ret;
	CHAR smsTxt[161], smsNumber[16], uciTxt[161], uciNumber[130], uciEnableTxt[4], uciEnableStat[4]; // TODO pritaikyta uci laikyt iki 10 numeriu
	char *pNumberFound;
	
	ret = GetSMS(storageType, messageIndex, &pMessageTag, &pMessageFormat, &pMessageSizePDU, &pMessagePDU);
	if (!ret)
	{
		message_parser(pMessagePDU, pMessageSizePDU, &smsTxt, &smsNumber);
		DBG("NUMBER:%s\nTEXT:%s\n", smsNumber, smsTxt);
	}	
	else
	{
		ERR("SMS reading failed: %d\n", ret);
		return;
	}
	
	pMessageIndex = messageIndex; // delete only received message
	if ((ret = DeleteSMS(storageType, &pMessageIndex, &pMessageTag)))
		ERR("SMS delete failed: %d\n", ret);
	
	/********** SMS reboot **********/	
	strcpy(uciTxt, "");
	strcpy(uciNumber, "");
	strcpy(uciEnableTxt, "");
	if(!(pCMD = popen("uci get sms_utils.sms_reboot.enable", "r")))
		ERR("uci config read failed (sms_reboot.enable).");
	else
	{
		fgets(uciEnableTxt, 3, pCMD);
		uciEnableTxt[strlen(uciEnableTxt)-1] = '\0'; //remove new line
	}
	
	if(!(pCMD = popen("uci get sms_utils.sms_reboot.psw", "r")))
		ERR("uci config read failed (sms_reboot.psw).");
	else	
	{
		fgets(uciTxt, 160, pCMD);
		uciTxt[strlen(uciTxt)-1] = '\0'; //remove new line
	}
	
	if(!(pCMD = popen("uci get sms_utils.sms_reboot.telnum", "r")))
		ERR("uci config read failed (sms_reboot.telnum).");
	else	
	{
		fgets(uciNumber, 130, pCMD);
		uciNumber[strlen(uciNumber)-1] = '\0'; //remove new line
	}
	
	if(!(pCMD = popen("uci get sms_utils.sms_reboot.status_sms", "r")))
		ERR("uci config read failed (reboot_set.reboot).");
	else	
	{
		fgets(uciEnableStat, 3, pCMD);
		uciEnableStat[strlen(uciEnableStat)-1] = '\0'; //remove new line
	}
	pclose(pCMD);
	
	DBG("uci config (reboot) values: %s %s %s\n", uciEnableTxt, uciTxt, uciNumber);
	
	pNumberFound = 0;
	pNumberFound = strstr(uciNumber, smsNumber);

	if (pNumberFound && !strcmp(uciTxt, smsTxt) && !strcmp(uciEnableTxt, "1"))
	{
		if (!strcmp(uciEnableStat, "1"))
		{
			sprintf(uciTxt, "uci set sms_utils.reboot_set.reboot=1 && uci set sms_utils.reboot_set.telnum=%s && uci commit sms_utils\n", smsNumber);
			system(uciTxt);
		}
		
		close_gapi();
		system("reboot");	
		DBG("Rebooting system...");
	}
	
	/********** rut500 status by SMS **********/
	strcpy(uciTxt, "");
	strcpy(uciNumber, "");
	strcpy(uciEnableTxt, "");
	if(!(pCMD = popen("uci get sms_utils.status_send.enable", "r")))
		ERR("uci config read failed (status_send.enable).");
	else
	{
		fgets(uciEnableTxt, 3, pCMD);
		uciEnableTxt[strlen(uciEnableTxt)-1] = '\0'; //remove new line
	}
	
	if(!(pCMD = popen("uci get sms_utils.status_send.psw", "r")))
		ERR("uci config read failed (status_send.psw).");
	else	
	{
		fgets(uciTxt, 160, pCMD);
		uciTxt[strlen(uciTxt)-1] = '\0'; //remove new line
	}
	
	if(!(pCMD = popen("uci get sms_utils.status_send.telnum", "r")))
		ERR("uci config read failed (status_send.telnum).");
	else	
	{
		fgets(uciNumber, 130, pCMD);
		uciNumber[strlen(uciNumber)-1] = '\0'; //remove new line
	}
	
	pclose(pCMD);
	
	DBG("uci config (status send) values: %s %s %s\n", uciEnableTxt, uciTxt, uciNumber);
	
	pNumberFound = 0;
	pNumberFound = strstr(uciNumber, smsNumber);

	if (pNumberFound && !strcmp(uciTxt, smsTxt) && !strcmp(uciEnableTxt, "1"))
	{
		Send_Router_Status(smsNumber);
	}
}

/************************************************
* interrupt_handler
************************************************/
void interrupt_handler(int sig)
{
	if (sig == SIGINT)
	{
		ERR("%s: interrupt signal received\n", __FILE__);
		close_gapi();
		exit(-1);
	}
}

/************************************************
* NewSMSCallback
************************************************/
void NewSMSCallback(ULONG storageType, ULONG messageIndex)
{
	DBG("Incoming new SMS. Storage type:%d index:%d\n", storageType, messageIndex);
	Get_SMS_Message_Text(storageType, messageIndex);
	//close_gapi(); // TODO nereikia 
}

/************************************************
* message_parser
************************************************/
int message_parser(char buf[], int buf_len, char *SMS_txt, char *SMS_Number)
{
	char *ascii;
	int address_len;
	char Sender_Number[16];
	int SMS_Len;
	char SMS_Text[170];
	int index, i, j;
	for (index = 0; index < buf[0]+1;)
		index++;
	index++;
	address_len = buf[index];
	index++;
	if (address_len % 2 != 0)
		address_len++;
	j = 0,
	index++;
	for (i = 0; i < address_len/2; i++)
	{	
		Sender_Number[j++] = (buf[index] & 0x0F) + 0x30;
		Sender_Number[j++] = ((buf[index++] & 0xF0) >> 4) + 0x30;
	}
	Sender_Number[address_len-1] = '\0';
	strcpy(SMS_Number, Sender_Number);
	//printf("%s %s\n", Sender_Number, SMS_Number);
	index++;
	index++;
	for (i = 0; i < 7; i++)
		index++;
	index++;
	i = 0;	
	while(index < buf_len)
	{
		SMS_Text[i++] = buf[index];
		index++;
	}
	SMS_Len = i;
	SMS_Text[SMS_Len] = '\0';
	i = 0;
	pdu_to_ascii(SMS_Text, SMS_Len, &ascii);
	//printf("%s\n", ascii);
	strcpy(SMS_txt, ascii);
	free(ascii);
}

/************************************************
* Main
************************************************/
int main(void) 
{   
	struct sms_reboot_status *config;
	ULONG ret, *pMessageFailureCode;
	char Number[32];
	char Text[161];

	if (signal(SIGINT, interrupt_handler) == SIG_ERR) 
	{
		ERR("%s: unknown process error\n", __FILE__);
		close_gapi();
		exit(-1);
	}

	if ((ret = init_gapi()))
	{
		ERR("API init error: %d\n", ret);
		close_gapi();
		exit(-1);
	}
		
	/*ret = load_config_sms_reboot_stat(config);
	if (ret)
		ERR("Config read error");*/

	if ((ret = SetNewSMSCallback(&NewSMSCallback)))
	{
		ERR("Callback register error: %d\n", ret);
		close_gapi();
		exit(-1);
	}
	
	Status_SMS_After_Reboot();
	
	while(1)
	{
		sleep(2);
	}
	exit(EXIT_SUCCESS);
}

/************************************************
* *get_config_entry
************************************************/
char *get_config_entry(struct uci_context *ctx, char *entry_name)
{
	struct uci_ptr ptr;
	struct uci_element *e;
	char name[UCI_ENTRY_SIZE];

	strncpy(name, entry_name, UCI_ENTRY_SIZE);
	if (uci_lookup_ptr(ctx, &ptr, name, 1) != UCI_OK)
		return NULL;
	e = ptr.last;
	if (!(ptr.flags & UCI_LOOKUP_COMPLETE))
		return NULL;
	switch(e->type) {
		case UCI_TYPE_SECTION:
			if ((ptr.s->type == NULL) || 
			(strcmp(ptr.s->type, "") == 0))			
				return NULL;	
			else 
				strncpy(uci_search_result, 
				(const char *)ptr.s->type, UCI_ENTRY_SIZE);
			break;
		case UCI_TYPE_OPTION:
			if ((ptr.o->v.string == NULL) || 
			(strcmp(ptr.o->v.string, "") == 0))
				return NULL;	
			else
				strncpy(uci_search_result, 
				(const char *)ptr.o->v.string, UCI_ENTRY_SIZE);
			break;
		default:
			break;
	}
	return &uci_search_result[0];
}

/************************************************
* load_config_sms_reboot_stat
************************************************/
int load_config_sms_reboot_stat(sms_reboot_status *config)
{
	char *entryp;
	struct uci_context *ctx;
	
	if (!(ctx = uci_alloc_context())) {
		/* default config */
		strcpy(config->reboot_enable, "");
		strcpy(config->reboot_psw, "");
		strcpy(config->reboot_telnum, "");
		strcpy(config->reboot_status_sms, "");
		return -1;
	}
	entryp = get_config_entry(ctx, "sms_utils.sms_reboot.enable");
	strcpy(config->reboot_enable, get_config_entry(ctx, "sms_utils.sms_reboot.enable"));
	
	if ((entryp = get_config_entry(ctx, "sms_utils.sms_reboot.enable")) == NULL)
		strcpy(config->reboot_enable, "");
	else
		strncpy(config->reboot_enable, entryp, UCI_ENTRY_SIZE);
	
	if ((entryp = get_config_entry(ctx, "sms_reboot.sms_reboot.psw")) == NULL)
		strcpy(config->reboot_psw, "");
	else
		strncpy(config->reboot_psw, entryp, UCI_ENTRY_SIZE);
	
	if ((entryp = get_config_entry(ctx, "sms_reboot.sms_reboot.telnum")) == NULL)
		strcpy(config->reboot_telnum, "");
	else
		strncpy(config->reboot_telnum, entryp, UCI_ENTRY_SIZE);
	
	if ((entryp = get_config_entry(ctx, "sms_reboot.sms_reboot.status_sms")) == NULL)
		strcpy(config->reboot_status_sms, "");
	else
		strncpy(config->reboot_status_sms, entryp, UCI_ENTRY_SIZE);
	
	uci_free_context(ctx);
	return 0;
}

/************************************************
* init_gapi
************************************************/
int init_gapi(void) {
	BYTE size = 10;
	qc_devices devs[10];
	ULONG ret;

	if(ret = QCWWANEnumerateDevices(&size, 
	(BYTE*)devs))
    	return (int)ret;
	ret = QCWWANConnect(devs[0].node, devs[0].key);
	return (int)ret;
}

/************************************************
* close_gapi
************************************************/
int close_gapi(void) 
{
	return (int)QCWWANDisconnect();
}

/************************************************
* is_gapi
************************************************/
int is_gapi(void) {
	char node[128], key[128];
	return (int)QCWWANGetConnectedDeviceID(128, 
		node, 128, key);
}
