#ifndef SMSD_H
#define SMSD_H

#define NAME "/var/lock/gobisocket"
#define LOCK_FILE "/var/lock/gobid"
#define DAEMON_NAME "gobid"
#define RUN_AS_USER "root"
#define EXIT_SUCCESS 0
#define EXIT_FAILURE 1
#define GENERIC_BUF_SIZE 64
#define UCI_ENTRY_SIZE 128
#define SOCK_BUF_SIZE 4096
#define REQ_TIMEOUT 100
#define RX_BUF_LEN		512
#define OPTION_ERR		-1
#define READ_ERR		-2
#define PACKET_ERR		-3
#define PARSE_ERR		-4

typedef struct {
    char node[256];
    char key[16];
} __attribute__((packed)) qc_devices;

typedef struct global_status {
	/* generic */
	ULONG wan_mode;
	ULONG hw_state;
	ULONG api_state; 
	/* device */
	char manufacturer[GENERIC_BUF_SIZE];
	char model_id[GENERIC_BUF_SIZE];
	char fw_rev[GENERIC_BUF_SIZE];
	char hw_rev[GENERIC_BUF_SIZE];
	char imei[GENERIC_BUF_SIZE];
	/* sim */
	ULONG sim_state;
	char imsi[GENERIC_BUF_SIZE];
	char iccid[GENERIC_BUF_SIZE];
	/* pin */
	ULONG pin_state;
	ULONG pin_rleft;
	ULONG pin_uleft;
	ULONG pin_last;
	/* registration */
	ULONG reg_state;
	char net_name[GENERIC_BUF_SIZE];
	char iface[GENERIC_BUF_SIZE];
	INT8 signal;
	ULONG ran;
	char net_pref[GENERIC_BUF_SIZE];
	ULONG roaming;
	ULONG cs_dom;
	ULONG ps_dom;
	WORD mcc;
	WORD mnc;
	/* data session */
	ULONG ds_state;
	ULONG autoconn_mode;
	ULONG bearer;
	ULONG ds_max_tx;
	ULONG ds_max_rx;
	ULONG cur_tx;
	ULONG cur_rx;
	ULONG pack_tx_ok;
	ULONG pack_tx_fail;
	ULONG pack_rx_ok;
	ULONG pack_rx_fail;
	ULONG pack_tx_over;
	ULONG pack_rx_over;
	ULONGLONG tx_bytes;
	ULONGLONG rx_bytes;
	char ip[GENERIC_BUF_SIZE];
	char duration[GENERIC_BUF_SIZE];	
} global_status;

typedef struct global_config {
	char pin[UCI_ENTRY_SIZE];
	char apn[UCI_ENTRY_SIZE];
	char user[UCI_ENTRY_SIZE];
	char password[UCI_ENTRY_SIZE];
	char auth_mode[UCI_ENTRY_SIZE];
	char net_mode[UCI_ENTRY_SIZE];	
} global_config;

typedef struct sms_reboot_status {
	char reboot_enable[UCI_ENTRY_SIZE];
	char reboot_psw[UCI_ENTRY_SIZE];
	char reboot_telnum[UCI_ENTRY_SIZE];
	char reboot_status_sms[UCI_ENTRY_SIZE];
	
} sms_reboot_status;

typedef struct {
    WORD	mcc;
    WORD 	mnc;
    ULONG	in_use;
    ULONG	roaming;
    ULONG	forbidden;
    ULONG	preferred;
    CHAR	description[255];
} __attribute__((packed)) net_info;

//-----------------------------------------------

static char *hardware_state[] = {
    "on",
    "off",
    "restarting",
    "unknown"
};

static char *gobi_api_state[] = {
    "active",
    "innactive",
    "unknown"
};

static char *radio_interfaces[] = {
    "no service",
    "CDMA 1xRTT",
    "CDMA 1xEV-DO",
    "AMPS (unsupported)",
    "GSM",
    "UMTS"
};

static char *session_state[] = {
    "unknown",
    "disconnected",
    "connected",
    "suspended",
    "authenticating"
};

static char *pin_state[] = {
    "not initialized",
    "enabled, not verified",
    "enabled, verified",
    "disabled",
    "blocked",
    "permanently blocked",
	"unknown"
};

static char *power_state[] = {
    "online",
    "low power mode",
    "factory test mode",
    "offline",
    "reset",
    "power off",
    "persistent low power mode"
};

static char *networks[] = {
    "unknown",
    "current serving network",
    "not current serving network, available"
};

static char *roaming[] = {
    "unknown",
    "home",
    "roam"
};

static char *forbidden[] = {
    "unknown",
    "forbidden",
    "not forbidden"
};

static char *preferred[] = {
    "unknown",
    "preferred",
    "not preferred"
};

static char *registration_state[] = {
    "not registered",
    "registered",
    "searching",
    "denied",
    "unknown"
};

static char *domain_state[] = {
    "unknown",
    "attached",
    "detached"
};

static char *ran_type[] = {
    "unknown",
    "CDMA 2000",
    "UMTS"
};

static char *radiotech_type[] = {
    "no service",
    "CDMA 1xRTT",
    "CDMA 1xEV-DO",
    "AMPS",
    "GSM",
    "UMTS"
};

static char *data_bearer[] = {
    "unknown",
    "CDMA 1xRTT",
    "CDMA 1xEV-DO Rev 0",
    "GPRS",
    "WCDMA",
    "CDMA 1xEV-DO Rev A",
    "EDGE",
    "HSDPA DL, WCDMA UL",
    "WCDMA DL, HSUPA UL",
    "HSDPA DL, HSUPA UL"
};

static char *roam_state[] = {
    "roaming",
    "home",
    "roaming partner"
};

static char *wan_mode[] = {
    "3g",
	"3g as backup",
    "not 3g",
	"unknown"
};

static char *last_entered_pin[] = {
    "correct",
	"incorrect",
    "unknown"
};

static char *update_condition[] = {
    "success",
	"hardware off",
    "api inncactive",
	"no sim",
	"link unregistered",
	"ds disconnected"
};

static char *autoconnection_mode[] = {
    "disabled",
	"enabled",
    "paused",
	"unknown"
};

static char *sim_state[] = {
    "inserted",
	"not inserted",
	"unknown"
};
#endif
