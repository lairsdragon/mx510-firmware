/* High Speed Option gsmd plugin
 *
 * (C) 2008 by Teltonika, JSC.
 * Written by Paulius Zaleckas <paulius.zaleckas@teltonika.lt>
 * All Rights Reserved
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */ 

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>

#include "gsmd.h"

#include <gsmd/gsmd.h>
#include <gsmd/usock.h>
#include <gsmd/event.h>
#include <gsmd/talloc.h>
#include <gsmd/extrsp.h>
#include <gsmd/atcmd.h>
#include <gsmd/vendorplugin.h>
#include <gsmd/unsolicited.h>


static int wan_call_parse(const char *buf, int len, const char *param,
		     struct gsmd *gsmd)
{
	int msg_len = len + 1;
	
	if(msg_len < sizeof(struct gsmd_evt_auxdata))
		msg_len = sizeof(struct gsmd_evt_auxdata);

	struct gsmd_ucmd *ucmd = usock_build_event(GSMD_MSG_EVENT,
			GSMD_EVT_NONE, msg_len);

	DEBUGP("WAN Call status: %s\n", buf);
	if (!ucmd)
		return -EINVAL;

	strcpy(ucmd->buf, buf);

	usock_evt_send(gsmd, ucmd, GSMD_EVT_NONE);

	return 0;
}

static const struct gsmd_unsolicit hso_unsolicit[] = {
	{ "_OWANCALL",	&wan_call_parse },	/* WAN call status */

	/* FIXME: add other messages when secondary interface will be working */
};

static int hso_detect(struct gsmd *g)
{
	/* FIXME: do actual detection of vendor if we have multiple vendors */
	return 1;
}

static int hso_initsettings(struct gsmd *g)
{
	/* FIXME: init unsolicit messages when secondary interface will be working */
	return 0;
}

struct gsmd_vendor_plugin gsmd_vendor_plugin = {
	.name = "High Speed Option",
	.ext_chars = "_",
	.num_unsolicit = ARRAY_SIZE(hso_unsolicit),
	.unsolicit = hso_unsolicit,
	.detect = &hso_detect,
	.initsettings = &hso_initsettings,
};
