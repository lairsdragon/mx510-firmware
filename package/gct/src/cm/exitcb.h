#ifndef EXITCB_H_04212008
#define EXITCB_H_04212008

int register_exit_cb(void (*cb)(int sig));
int unregister_exit_cb(void (*cb)(int sig));
void init_exit_cb(void);
void process_exit_cb(void);

#endif
