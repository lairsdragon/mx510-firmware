#if !defined(DHCLIENT_H_20100409)
#define DHCLIENT_H_20100409

#include <netinet/in.h>
#include "../sdk/gctapi.h"
#include "cm_msg.h"

void dh_start_dhclient(int dev_idx);
void dh_stop_dhclient(int dev_idx);
void dh_create_dhclient(void);
void dh_delete_dhclient(void);

#endif
