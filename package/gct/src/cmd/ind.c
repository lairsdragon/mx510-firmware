#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <wchar.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <fcntl.h>
#include <errno.h>
#include <assert.h>
#include <pthread.h>

#include "cm.h"
#include "dhclient.h"

extern int device_open(GDEV_ID_P pID);
extern int device_close(GDEV_ID_P pID);
extern void cmd_signal_event(int dev_idx, unsigned short event, char *data, int len);
#if defined(CONFIG_DM_INTERFACE)
int send_dm_tool(void *data, int len);
int dm_ind_connetion(int connect, int success);
#endif

char *StatusStr[] = {
	"UnInitialized",
	"RF_OFF_HW_SW",
	"RF_OFF_HW",
	"RF_OFF_SW",
	"Ready",
	"Scanning",
	"Connecting",
	"Data_Connected"
};

char *ConnStatusStr[] = {
	"Ranging",
	"SBC",
	"EAP_authentication_Device",
	"EAP_authentication_User",
	"3_way_handshake",
	"Registration",
	"De_registration",
	"Registered",
	"DSX"
};

static char *StatusReasonStr[] = {
	"Normal",
	"Fail_to_connect_to_NW",
	"Fail_to_connect_Ranging",
	"Fail_to_connect_SBC",
	"Fail_to_connect_EAP_AUTH_Device",
	"Fail_to_connect_EAP_AUTH_user",
	"Fail_to_connect_3_Way_Handshake",
	"Fail_to_connect_REG",
	"Fail_to_connect_datapath"
};

static void print_disconnect_reason(u8 *data, int len)
{
	char *msg = "disconnected by unknown reason";

	/*
	Reason(1 byte) : 
	Disconnect Reason
	0x00 : disconnected by DREG
	0x01 : disconnected by MS
	0x02 : disconnected by OoZ
	0x03 : disconnected by all other reason
	*/
	switch (*data) {
		case 0x00:
			msg = "disconnected by DREG";
			break;
		case 0x01:
			msg = "disconnected by MS";
			break;
		case 0x02:
			msg = "disconnected by OoZ";
			break;
		case 0x03:
			msg = "disconnected by all other reason";
			break;
	}

	cm_printf("%s\n", msg);
}

static void ind_recv_hci_packet(GDEV_ID_P pID, char *buf, int len)
{
	unsigned short event;
	unsigned short length;
	unsigned char *data, *p;
	hci_t *hci = (hci_t *) buf;
	cm_common_conf_t *pconf = &cm_common_conf;

	#if defined(CONFIG_DM_INTERFACE)
	if (pconf->dm_interface_enable && pconf->dm_interface_cfd > 0)
		send_dm_tool(buf, len);
	#endif

	event = _B2H(hci->cmd_evt);
	length = _B2H(hci->length);
	data = hci->data;

	switch (event) {
		case WIMAX_DISCONN_IND:
			print_disconnect_reason(data, length);
			break;
		case WIMAX_IP_RENEW_IND:
			dh_stop_dhclient(pID->deviceIndex);
			dh_start_dhclient(pID->deviceIndex);
			break;
		case WIMAX_CLI_RSP:
		case WIMAX_MANAGER_MSG:
			#if 1
			data[length] = 0;
			p = &data[WIMAX_PRINT_PAD];
			cm_flag_printf("%s", p);
			#else
			length -= WIMAX_PRINT_PAD;
			data += WIMAX_PRINT_PAD;
			cm_printf("%s", data);
			//fwrite(data, 1, length, stdout);
			//fflush(stdout);
			#endif
			#if (CM_LOG_FLAG & LOG_FLAG_NO_STDOUT_IN_LOGFILE)
// 			if (GAPI_LOG_LEVEL_IS_FILE(pconf->log_level)) {
// 				const char dm_prompt[] = "DM> ";
// 				if (*(int *) p == *(int *) dm_prompt) {
// 					printf("%s", p);
// 					fflush(stdout);
// 				}
// 			}
			#endif
			break;
		case WIMAX_DL_IMAGE_STATUS:
		case WIMAX_UL_IMAGE_RESULT:
		case WIMAX_FILE_RESULT:
		case WIMAX_IMAGE_CMD_STATUS:
			cmd_signal_event(pID->deviceIndex, event, (char *)data, length);
			break;
	}
}

static void ind_powermode_change(GDEV_ID_P pID, GCT_API_POWER_MODE nPowerMode)
{
	char *power_mode = "Unknown";

	switch (nPowerMode) {
		case WiMAXPowerModeIdle:
			power_mode = "Idle";
			break;
		case WiMAXPowerModeSleep:
			power_mode = "Sleep";
			break;
		case WiMAXPowerModeNormal:
			power_mode = "Normal";
			break;
		case WiMAXPowerModeMaximum:
			power_mode = "Maximum";
			break;
	}

	cm_printf("device[%d] power mode: %s\n", pID->deviceIndex, power_mode);
}

static void ind_connect_network(GDEV_ID_P pID,
	WIMAX_API_NETWORK_CONNECTION_RESP networkConnectionResponse)
{
	cm_common_conf_t *pconf = &cm_common_conf;
	FILE *pFile;
	
	#if defined(CONFIG_DM_INTERFACE)
	if (pconf->dm_interface_enable && pconf->dm_interface_cfd > 0)
		dm_ind_connetion(1, networkConnectionResponse==WIMAX_API_CONNECTION_SUCCESS);
	#endif

	cm_printf("device[%d] connection %s\n", pID->deviceIndex,
		networkConnectionResponse==WIMAX_API_CONNECTION_SUCCESS
		? "SUCCESS" : "FAIL");
	
	//-----------------------------------------------
	// Update DHCP lease if connection successful
	// kill -SIGUSR2 `pidof udhcpc`
	// kill -SIGUSR1 `pidof udhcpc`
	//-----------------------------------------------
	if (networkConnectionResponse == WIMAX_API_CONNECTION_SUCCESS) {
		cm_printf("sending signal to udhcpc lease update\n");
		pFile = popen("kill -SIGUSR2 `pidof udhcpc`; kill -SIGUSR1 `pidof udhcpc`", "r");
	}
	//-----------------------------------------------
	
	if (pconf->api_mode != GCT_WIMAX_API_PRIVILEGE_READ_ONLY) {
		if (networkConnectionResponse==WIMAX_API_CONNECTION_SUCCESS)
			dh_start_dhclient(pID->deviceIndex);
		#if defined(CONFIG_DM_INTERFACE)
		else if (pconf->auto_connect_enable)
			cm_retry_auto_connection(pID->deviceIndex);
		#endif
	}
}

static void ind_dev_disconnect_network(GDEV_ID_P pID,
	WIMAX_API_NETWORK_CONNECTION_RESP networkDisconnectResponse)
{
	cm_common_conf_t *pconf = &cm_common_conf;

	#if defined(CONFIG_DM_INTERFACE)
	if (pconf->dm_interface_enable && pconf->dm_interface_cfd > 0)
		dm_ind_connetion(0, networkDisconnectResponse==WIMAX_API_CONNECTION_SUCCESS);
	#endif

	cm_printf("device[%d] disconnection %s\n", pID->deviceIndex,
		networkDisconnectResponse==WIMAX_API_CONNECTION_SUCCESS
		? "SUCCESS" : "FAIL");
	if (networkDisconnectResponse==WIMAX_API_CONNECTION_SUCCESS)
		dh_stop_dhclient(pID->deviceIndex);

	if (pconf->api_mode != GCT_WIMAX_API_PRIVILEGE_READ_ONLY) {
		if (pconf->auto_connect_enable)
			cm_request_auto_connection(pID->deviceIndex);
	}
}

static void ind_dev_network_search_widescan(GDEV_ID_P pID,
	WIMAX_API_NSP_INFO_P pNspList, UINT32 listSize)
{
	int i;

	cm_printf("[NSP-Name] [NSP-ID] [RSSI] [CINR]\n");
	for (i = 0; i < listSize; i++) {
		cm_printf("%9S   %06X %6d %6d\n",
			(wchar_t *) pNspList[i].NSPName,
			(int) pNspList[i].NSPid,
			(int) pNspList[i].RSSI-123,
			(int) pNspList[i].CINR-10);
	}
}

static void ind_dev_provisioning_operation(GDEV_ID_P pID, 
				WIMAX_API_PROV_OPERATION provisioningOperation,
				WIMAX_API_CONTACT_TYPE contactType)
{
	char *op = NULL;
	switch (provisioningOperation) {
		case WIMAX_API_PROV_OPERATION_CFG_UPDATE_STARTED:
			op = "STARTED";
			break;
		case WIMAX_API_PROV_OPERATION_CFG_UPDATE_COMPLETED:
			op = "COMPLETED";
			break;
		case WIMAX_API_PROV_OPERATION_CFG_UPDATE_FAILED_NETWORK_DISCONNECT:
			op = "FAILED_NETWORK_DISCONNECT";
			break;
		case WIMAX_API_PROV_OPERATION_CFG_UPDATE_FAILED:
			op = "FAILED";
			break;
		case WIMAX_API_PROV_OPERATION_CFG_UPDATE_FAILED_INVALID_PROVISIONING:
			op = "FAILED_INVALID_PROVISIONING";
			break;
		case WIMAX_API_PROV_OPERATION_CFG_UPDATE_FAILED_BAD_AUTHENTICATION:
			op = "FAILED_BAD_AUTHENTICATION";
			break;
		case WIMAX_API_PROV_OPERATION_CFG_UPDATE_REQUEST_INITIAL_PROVISIONING:
			op = "REQUEST_INITIAL_PROVISIONING";
			break;
		case WIMAX_API_PROV_OPERATION_CFG_UPDATE_REQUEST_ONGOING_PROVISIONING:
			op = "REQUEST_ONGOING_PROVISIONING";
			break;
		case WIMAX_API_PROV_OPERATION_CFG_UPDATE_REQUEST_RESET_PROVISIONING:
			op = "REQUEST_RESET_PROVISIONING";
			break;
		case WIMAX_API_PROV_OPERATION_CFG_UPDATE_TRIGGER_CONTACT:
			op = "TRIGGER_CONTACT";
			break;
	}

	if (op)
		cm_printf("Provisioning:%s, contact-type=%d\n", op, contactType);
	else
		cm_printf("Provisioning:Unknown(%d), contact-type=%d\n", provisioningOperation, contactType);
}

static void ind_power_mng(
	GDEV_ID_P pID,
	WIMAX_API_RF_STATE powerState)
{
	cm_printf("device[%d] Power %s\n", pID->deviceIndex,
		powerState==WIMAX_API_RF_ON ? "ON" : "OFF");
}

static void ind_dev_insert_remove(GDEV_ID_P pID, BOOL cardPresence)
{
	cm_printf("device[%d] %s\n", pID->deviceIndex,
		cardPresence ? "INSERT" : "REMOVE");

	if (cardPresence)
		device_open(pID);
	else
		device_close(pID);
}

static void ind_dev_status(GDEV_ID_P pID,
	WIMAX_API_DEVICE_STATUS deviceStatus, WIMAX_API_STATUS_REASON statusReason, 
	WIMAX_API_CONNECTION_PROGRESS_INFO connectionProgressInfo)
{
	dev_conf_t *dconf = &cm_dev_conf[pID->deviceIndex];

	dconf->wimax_status = deviceStatus;

	if (deviceStatus > WIMAX_API_DEVICE_STATUS_Data_Connected) {
		cm_printf("device[%d] Unknown device status[%d]\n",
			pID->deviceIndex, deviceStatus);
		return;
	}

	cm_printf("device[%d] status [%s]\n", pID->deviceIndex, StatusStr[deviceStatus]);

	if (dconf->ip_acq_timed && deviceStatus != WIMAX_API_DEVICE_STATUS_Data_Connected) {
		cm_stop_timer(&dconf->ip_acq_timer);
		dconf->ip_acq_timed = 0;
	}

	if (deviceStatus == WIMAX_API_DEVICE_STATUS_Connecting) {
		if (connectionProgressInfo <= WIMAX_API_DEVICE_CONNECTION_PROGRESS_Registration_DSX) {
			cm_printf("device[%d] Connection Progress Info [%s]\n", 
				pID->deviceIndex, ConnStatusStr[connectionProgressInfo]);
		} else {
			cm_printf("device[%d] Unknown connection Progress Info [%d]\n",
				pID->deviceIndex, connectionProgressInfo);
			return;
		}
	}

	if (statusReason != WIMAX_API_STATUS_REASON_Normal) {
		if (statusReason <= WIMAX_API_STATUS_REASON_Fail_to_connect_datapath) {
			cm_printf("device[%d] Status Reason [%s]\n",
				pID->deviceIndex, StatusReasonStr[statusReason]);
		} else {
			cm_printf("device[%d] Unknown status Reason [%d]\n", 
				pID->deviceIndex, statusReason);
		}
	}
}

#if defined(CONFIG_ENABLE_SERVICE_FLOW)
static void ind_noti_service_flow(GDEV_ID_P pID, WIMAX_SF_EVENT_P pSfEvent)
{
	cm_printf("device[%d] WIMAX_SF_EVENT_TYPE[%d], CC=[%d]\n", pID->deviceIndex, pSfEvent->code, pSfEvent->sf.cc);
}
#endif // CONFIG_ENABLE_SERVICE_FLOW

#include "../sdk/eap/lib/include/eapretcodes.h"
#define E_WM_CR801_EAP_FAILURE		-201
#define E_WM_UNKNOWN_SERVER_REALM	-202

static void eap_get_error_string(int err_code, char *err_str)
{
	switch (err_code) {
		case E_WM_SUCCESS:
			strcpy(err_str, "Success.");
			break;
		case E_WM_CA_LOAD_FAIL:
			strcpy(err_str, "CA Certification File Load Failed");
			break;
		case E_WM_PRIVATE_LOAD_FAIL:
			strcpy(err_str, "Client Cert. or Private Key File Load Failed");
			break;
		case E_WM_TLS_FAIL:
			strcpy(err_str, "Mutual Authentication Failed");
			break;
		case E_WM_TTLS_PHASE2_FAIL:
			strcpy(err_str, "Phase 2 Authentication Failed");
			break;
		case E_WM_MEMORY_INSUFFICIENT:
			strcpy(err_str, "Insufficient Memory");
			break;
		case E_WM_NOT_SUPPORTED:
			strcpy(err_str, "Not Supported Functionality");
			break;
		case E_WM_SET_PARAM_FAILURE:
			strcpy(err_str, "Invalid Parameter Setting");
			break;
		case E_WM_AUTH_STR_NULL:
			strcpy(err_str, "EAP Configuration was not setted.");
			break;
		case E_WM_AUTH_FUNC_NULL:
			strcpy(err_str, "Internal Callback Function failed.");
			break;
		case E_WM_INIT_TLS_FAIL:
			strcpy(err_str, "TLS Initialization failed.");
			break;
		case E_WM_BUFFER_SMALL:
			strcpy(err_str, "Internal Static Buffer is too small.");
			break;
		case E_WM_NOT_SUCCESS:
			strcpy(err_str, "Getting the MSK was failed.");
			break;
		case E_WM_NO_DATA:
			strcpy(err_str, "There's no more date from TLSDealEAPPayload().");
			break;
		case E_WM_INVALID_FRAME:
			strcpy(err_str, "Invalid EAP Packet Frame.");
			break;
		case E_WM_SEND_TLS:
			strcpy(err_str, "Invalid TLS from server.");
			break;
		case E_WM_RECV_TLS:
			strcpy(err_str, "Invalid TLS to server.");
			break;
		case E_WM_CERT_NOT_FOUND:
			strcpy(err_str, "There's no certification file.");
			break;
		case E_WM_X509_CTX_FAIL:
			strcpy(err_str, "CTX Allocation failed.");
			break;
		case E_WM_CERT_LOAD_FAIL:
			strcpy(err_str, "Loading Certification File in TLSGetCertInfo()");
			break;
		case E_WM_MEMORY_NULL:
			strcpy(err_str, "p_szRtnBuf, p_nRtnBufSize is NULL.");
			break;
		case E_WM_CERT_WRITE_FAIL:
			strcpy(err_str, "Writing the P12 File is failed in TLSConvertP12().");
			break;
		case E_WM_CERT_VERIFY_FAIL:
			strcpy(err_str, "Invalid Password for P12 File in TLSConvertP12().");
			break;
		case E_WM_ID_NOT_FOUND:
			strcpy(err_str, "There's no ID (InnerNAI) for EAP-Response_Identity.");
			break;
		case E_WM_INVALID_PACKET:
			strcpy(err_str, "Invalid AVP in TTLS-MSCHAPv2.");
			break;
		case E_WM_SERVER_RESPONSE_FAIL:
			strcpy(err_str, "Received Invalid AVP from AAA in TTLS-MSCHAPv2.");
			break;
		case E_WM_BUFFER_NULL:
			strcpy(err_str, "Internal Buffer Pointer is NULL.");
			break;
		case E_WM_MSCHAP_AUTH_RESP_ERROR:
			strcpy(err_str, "There's no or invalid 'S=' Value in MSCHAPv2.");
			break;
		case E_WM_MSCHAP_AUTH_RESP_INVALID_IDENTIFIER:
			strcpy(err_str, "Invalid Identifier in MSCHAPv2.");
			break;
		case E_WM_NO_SSL_CONTEXT:
			strcpy(err_str, "There's no SSL Context.");
			break;
		case E_WM_TLS_INVALID_PACKET_LENGTH:
			strcpy(err_str, "TLS Record Length is over than internal buffer size.");
			break;
		case E_WM_TTLS_INVALID_MSG_SEQ:
			strcpy(err_str, "There's misorder in TTLS's internal authentication.");
			break;
		case E_WM_CR801_EAP_FAILURE:
			strcpy(err_str, "Receive failure packet.");
			break;
		case E_WM_UNKNOWN_SERVER_REALM:
			strcpy(err_str, "Unknown server realm");
			break;
		case F_WM_ID_NOT_FOUND:
			strcpy(err_str, "ID not found");
			break;
		case F_WM_PASSWORD_NOT_FOUND:
			strcpy(err_str, "Password not found");
			break;
		case F_WM_CA_CERT_ERROR:
			strcpy(err_str, "CA certificate error");
			break;
		case F_WM_PRIVATE_CERT_ERROR:
			strcpy(err_str, "Private certificate error");
			break;
		case F_WM_SERVER_REJECT:
			strcpy(err_str, "Server rejected");
			break;
		default:
			strcpy(err_str, "Unknown Error.");
			break;
	}
}

static void odm_get_error_string(int err_code, char *err_str)
{
	static const char *odm_err_str[] = {
		"NONE",
		"INVALID_BOOTSTRAP_MESSAGE",
		"CANNOT_CONNECT_ODM_SERVER",
		"HTTP_ERROR",
		"HTTP_RESPONSE_TIMEOUT",
		"STATUS_ERROR",
		"DNS_QUERY_FAIL",
		"CANNOT_CONNECT_WIB_SERVER"
	};

	if (_numof_array(odm_err_str) < err_code)
		cm_printf("OMA-DM invalid error code=%d\n", err_code);
	else
		strcpy(err_str, odm_err_str[err_code]);
}

void ind_notification(GDEV_ID_P pID, GCT_API_NOTI_CATEGORY category,
				GCT_API_NOTI_TYPE type, int buf_size, char *buf)
{
	char string[256];
	char *title = "Unknown";
	short code = 0;
	int odm_err = 0;

	switch (category) {
		case GCT_API_ERROR_NOTI_EAP:
			title = "EAP Error";
			if (type == GCT_API_NOTI_TYPE_CODE) {
				memcpy(&code, buf, sizeof(short));
				if (CAP_EEAP_ENABLED(pID->deviceIndex))
					code = _B2H(code);
				eap_get_error_string(code, string);
				cm_printf("\t%s: %s\n", title, string);
				if (code == E_WM_UNKNOWN_SERVER_REALM)
					cm_printf("\tserver realm: %s\n", &buf[sizeof(short)]);
			}
			break;
		case GCT_API_NOTI_EAP:
			title = "EAP Notification";
			break;
		case GCT_API_NOTI_ODM_NOTI:
			title = "ODM Notification";
			if (type == GCT_API_NOTI_TYPE_CODE) {
				code = buf[0];
				switch (code) {
					case GCT_API_ODM_NOTI_EXEC_REGISTRATION_PAGE_OPEN:
						cm_printf("\t%s EXEC_REGISTRATION_PAGE_OPEN: %s\n", title, &buf[1]);
						break;
					case GCT_API_ODM_NOTI_CHANGE_ACTIVATED:
						cm_printf("\t%s CHANGE_ACTIVATED\n", title);
						break;
					default:
						cm_printf("\t%s: code=%d\n", title, code);
						break;
				}
			}
			break;
		case GCT_API_NOTI_ODM_ERROR:
			memcpy(&odm_err, buf, sizeof(odm_err));
			odm_get_error_string(odm_err, string);
			cm_printf("OMA-DM Error: %s\n", string);
			break;
		default:
			title = "Unknown";
			break;
	}

	if (type == GCT_API_NOTI_TYPE_TEXT) 
		cm_printf("[%s]\n%s", title, buf);
}

#if defined(CONFIG_OMA_DM_CLIENT)
#define REPORT_FW_UPDATE_AFTER_REBOOT
//#define SUPPORT_RESUMING_DOWNLOAD_FW

#define FOTA_UPDATE_REPORT_TIMER_MS		1000
#define FOTA_DL_CONTINUE_TIMER_MS		3000

#define ODM_STR_LEN					256
#define UIMG_PATH					"/etc/gct/gdmuimg.bin"
#define DOWNLOAD_DIR				"fumo"
#define SESSION_FILE				"odm_sessoin.ini"
#define PKG_URL_FILE				"odm_pkgurl.ini"
#define IN_DOWNLOADING				"odm_in_downloading"
#define SHOULD_REPORT_UPDATE		"odm_should_report_update.ini"
#define FW_PKG_VER_NODE				"./DevDetail/SwV"
//#define ODM_FIXED_FW_NAME			"firmware.bin"
#if defined(CONFIG_OMA_DM_DRMD)
#define DRMD_REPORT_TIMER_MS	FOTA_UPDATE_REPORT_TIMER_MS
#define SHOULD_REPORT_DRMD		"odm_should_report_drmd.ini"
#define DRMD_DURATION_FILE		"odm_drmd_duration.ini"
#endif // CONFIG_OMA_DM_DRMD

typedef struct cm_odm_s {
	char fumo_dir[ODM_STR_LEN];
	char session_file[ODM_STR_LEN];
	char pkgurl_file[ODM_STR_LEN];
	#if defined(SUPPORT_RESUMING_DOWNLOAD_FW)
	char in_downloading_file[ODM_STR_LEN];
	#endif
	char should_report_update_file[ODM_STR_LEN];

	#if defined(CONFIG_OMA_DM_DRMD)
	char should_report_drmd_file[ODM_STR_LEN];
	char drmd_duration_file[ODM_STR_LEN];
	#endif // CONFIG_OMA_DM_DRMD

} cm_odm_t;

static cm_odm_t cm_odm;

static void setup_odm_config(void)
{
	cm_common_conf_t *pconf = &cm_common_conf;
	cm_odm_t *cmodm = &cm_odm;

	strcpy(cmodm->fumo_dir, pconf->nonvolatile_dir);
	strcat(cmodm->fumo_dir, "/"DOWNLOAD_DIR);
	mkdir(cmodm->fumo_dir, 0644);

	strcpy(cmodm->session_file, pconf->nonvolatile_dir);
	strcat(cmodm->session_file, "/"SESSION_FILE);

	strcpy(cmodm->pkgurl_file, pconf->nonvolatile_dir);
	strcat(cmodm->pkgurl_file, "/"PKG_URL_FILE);

	#if defined(SUPPORT_RESUMING_DOWNLOAD_FW)
	strcpy(cmodm->in_downloading_file, pconf->nonvolatile_dir);
	strcat(cmodm->in_downloading_file, "/"IN_DOWNLOADING);
	#endif

	strcpy(cmodm->should_report_update_file, pconf->nonvolatile_dir);
	strcat(cmodm->should_report_update_file, "/"SHOULD_REPORT_UPDATE);

	#if defined(CONFIG_OMA_DM_DRMD)
	strcpy(cmodm->should_report_drmd_file, pconf->nonvolatile_dir);
	strcat(cmodm->should_report_drmd_file, "/"SHOULD_REPORT_DRMD);

	strcpy(cmodm->drmd_duration_file, pconf->nonvolatile_dir);
	strcat(cmodm->drmd_duration_file, "/"DRMD_DURATION_FILE);
	#endif // CONFIG_OMA_DM_DRMD

	cm_dprintf("fumo_dir=%s\n", cmodm->fumo_dir);
	cm_dprintf("session_file=%s\n", cmodm->session_file);
	cm_dprintf("pkgurl_file=%s\n", cmodm->pkgurl_file);
	#if defined(SUPPORT_RESUMING_DOWNLOAD_FW)
	cm_dprintf("in_downloading_file=%s\n", cmodm->in_downloading_file);
	#endif
	cm_dprintf("should_report_update_file=%s\n", cmodm->should_report_update_file);
	#if defined(CONFIG_OMA_DM_DRMD)	
	cm_dprintf("should_report_drmd_file=%s\n", cmodm->should_report_drmd_file);
	cm_dprintf("drmd_duration=%s\n", cmodm->drmd_duration_file);
	#endif // CONFIG_OMA_DM_DRMD
}

static GCT_API_RET odm_report_fw_update(void *data)
{
	int dev_idx = (int) data;
	GDEV_ID ID;
	char uri[ODM_STR_LEN];
	char correlator[ODM_STR_LEN];
	char new_fw_ver[ODM_STR_LEN] = "0.0.2";
	FUMO_RESULT nResultCode;
	GCT_API_RET ret;

	ID.deviceIndex = dev_idx;
	ID.apiHandle = cm_api_handle;

	cm_printf("Start FW reporting!\n");

	if (!cm_load_file_lines(cm_odm.session_file, 2, uri, correlator)) {
		nResultCode = FUMO_CLIENT_ERR;
		goto out;
	}

	nResultCode = FUMO_SUCCESSFUL;

	/*NOTE: Change with your fw version.*/
	if ((ret = GAPI_DMCSetNodeValue(&ID, FW_PKG_VER_NODE, new_fw_ver, strlen(new_fw_ver)))
		!= GCT_API_RET_SUCCESS) {
		cm_eprintf("Updating SUCCESS & Set Node(%s):Value(%s) Failure\n",
			FW_PKG_VER_NODE, new_fw_ver);
		goto out;
	}
	else
		cm_printf("Updating SUCCESS & Set FW ver=%s\n", new_fw_ver);

	ret = GAPI_DMCTriggerFwUpdateResult(&ID, uri, correlator, nResultCode);
	cm_printf("TriggerFwUpdateResult=%d\n", ret);
	if (ret == GCT_API_RET_SUCCESS) {
		unlink(cm_odm.session_file);
		unlink(cm_odm.pkgurl_file);
		unlink(cm_odm.should_report_update_file);
	}
out:
	cm_printf("FOTA FW Update Result=%d\n", nResultCode);
	return ret;
}

static void target_update_scenario(int dev_idx, const char *fw_file)
{
	/*NOTE: Add your updating code.*/

	#if 0	/*Delete if you don't need it*/
	unlink(fw_file);
	#endif
}

static void *odm_fw_updater_thread(void *data)
{
	int dev_idx = (int) data;
	GDEV_ID ID;
	cm_odm_t *cmodm = &cm_odm;
	char fw_file[ODM_STR_LEN];
	#if !defined(ODM_FIXED_FW_NAME)
	char fw_file_name[ODM_STR_LEN];
	#endif

	ID.deviceIndex = dev_idx;
	ID.apiHandle = cm_api_handle;

	cm_printf("Start FW updating!\n");

	#if defined(ODM_FIXED_FW_NAME)
	sprintf(fw_file, "%s/%s", cmodm->fumo_dir, ODM_FIXED_FW_NAME);
	if (access(fw_file, 0) < 0) {
		cm_eprintf("Not found FW(%s)!!\n", fw_file);
		unlink(fw_file);
		goto out;
	}
	#else
	if (cm_get_dir_entry(cmodm->fumo_dir, 0, fw_file_name, sizeof(fw_file_name)) < 0) {
		cm_eprintf("Not found FW(in %s)!!\n", cmodm->fumo_dir);
		goto out;
	}
	sprintf(fw_file, "%s/%s", cmodm->fumo_dir, fw_file_name);
	#endif

	target_update_scenario(dev_idx, fw_file);
out:
	cm_printf("FOTA FW Updating Done\n");

	#if !defined(REPORT_FW_UPDATE_AFTER_REBOOT)
	odm_report_fw_update((void *)dev_idx);
	#endif
	return NULL;
}

static void ind_odm_get_fw_file_info(GDEV_ID_P pID, char *path)
{
	cm_odm_t *cmodm = &cm_odm;

	strcpy(path, cmodm->fumo_dir);
	cm_dprintf("path=%s\n", path);
}

static void ind_odm_get_fw_free_space(GDEV_ID_P pID, unsigned int *freeSize)
{
	/*Note that you should set up your free space size.*/
	*freeSize = 100*1024*1024;
	cm_dprintf("free-size=%d\n", *freeSize);
}

static void ind_odm_get_fw_session_info(GDEV_ID_P pID, char *uri, char *correlator)
{
	cm_load_file_lines(cm_odm.session_file, 2, uri, correlator);
	cm_dprintf("Get FW Session: uri=%s, correlator=%s\n", uri, correlator);
}

static void ind_odm_set_fw_session_info(GDEV_ID_P pID, char *uri, char *correlator)
{
	cm_store_file_lines(cm_odm.session_file, 2, uri, correlator);
	#if defined(SUPPORT_RESUMING_DOWNLOAD_FW)
	cm_creat_file(cm_odm.in_downloading_file, NULL, 0);
	#endif
	cm_dprintf("Set FW Session: uri=%s, correlator=%s\n", uri, correlator);
}

static void ind_odm_get_fw_pkg_url(GDEV_ID_P pID, char *szUrlNode, char *szURL)
{
	if (!access(cm_odm.pkgurl_file, 0))
		cm_load_file_lines(cm_odm.pkgurl_file, 2, szUrlNode, szURL);
	cm_dprintf("Get PKG-URL: szUrlNode=%s, szURL=%s\n", szUrlNode, szURL);
}

static void ind_odm_set_fw_pkg_url(GDEV_ID_P pID, char *szUrlNode, char *szURL)
{
	cm_store_file_lines(cm_odm.pkgurl_file, 2, szUrlNode, szURL);
	cm_dprintf("Set PKG-URL: szUrlNode=%s, szURL=%s\n", szUrlNode, szURL);
}

static void ind_odm_set_fw_dl_progress(GDEV_ID_P pID,
				unsigned int curr_size, unsigned int total_size)
{
	cm_printf("FUMO FW Downloaded %d/%d bytes\n", curr_size, total_size);
	#if defined(SUPPORT_RESUMING_DOWNLOAD_FW)
	if (curr_size == total_size)
		unlink(cm_odm.in_downloading_file);
	#endif
}

static void ind_odm_set_fw_dl_result(GDEV_ID_P pID, char *fw_pkg_filename, int success)
{
	cm_printf("FW(%s) DL Result %s\n", fw_pkg_filename, success ? "SUCCESS" : "FAILURE");
}

static void ind_odm_run_fw_update(GDEV_ID_P pID)
{
	pthread_t pthread;

	cm_creat_file(cm_odm.should_report_update_file, NULL, 0);
	cm_dprintf("Run FW-update\n");
	pthread_create(&pthread, NULL, odm_fw_updater_thread, (void *)(int)pID->deviceIndex);
	pthread_detach(pthread);
}

#if defined(SUPPORT_RESUMING_DOWNLOAD_FW)
static void odm_fota_resume_timer_callback(void *data)
{
	int dev_idx = (int) data;
	dev_conf_t *conf = &cm_dev_conf[dev_idx];
	GDEV_ID ID;
	GCT_API_RET gret;
	int restart_timer_ms = FOTA_DL_CONTINUE_TIMER_MS;

	ID.deviceIndex = dev_idx;
	ID.apiHandle = cm_api_handle;

	gret = GAPI_DMCTriggerFwDlContinue(&ID);
	if (gret == GCT_API_RET_SUCCESS)
		cm_printf("GAPI_DMCTriggerFwDlContinue SUCCESS\n");
	else if (gret == GCT_API_RET_BUSY) {
		cm_start_timer(&conf->fota_timer, restart_timer_ms);
		cm_printf("ODM is busy, restart timer %d ms.\n", restart_timer_ms);
	}
	else
		cm_eprintf("GAPI_DMCTriggerFwDlContinue FAILURE\n");
}

void odm_req_resuming_download(int dev_idx)
{
	dev_conf_t *conf = &cm_dev_conf[dev_idx];

	cm_init_timer(&conf->fota_timer, odm_fota_resume_timer_callback,
		(void *)dev_idx);
	cm_start_timer(&conf->fota_timer, FOTA_DL_CONTINUE_TIMER_MS);
	cm_printf("Resuming download timer started!\n");
}

static int odm_chk_req_resuming_download(int dev_idx)
{
	cm_odm_t *cmodm = &cm_odm;
	int ret = 0;

	if (!access(cmodm->in_downloading_file, 0)) {
		cm_eprintf("Active downloading file(%s)\n", cmodm->in_downloading_file);
		ret = 1;
	}
	return ret;
}
#endif

static void odm_fw_update_report_timer_callback(void *data)
{
	int dev_idx = (int) data;
	dev_conf_t *conf = &cm_dev_conf[dev_idx];
	GDEV_ID ID;
	GCT_API_RET gret;
	int restart_timer_ms = FOTA_UPDATE_REPORT_TIMER_MS;

	ID.deviceIndex = dev_idx;
	ID.apiHandle = cm_api_handle;

	if (++conf->fota_report_cnt > FW_UPDATE_REPORT_TRY_CNT) {
		cm_eprintf("FW Update Reporting Failure! (cnt=%d)\n", conf->fota_report_cnt);
		return;
	}

	gret = odm_report_fw_update((void *) dev_idx);
	if (gret == GCT_API_RET_SUCCESS)
		cm_printf("FW Update Reporting SUCCESS\n");
	else if (gret == GCT_API_RET_BUSY) {
		cm_start_timer(&conf->fota_timer, restart_timer_ms);
		cm_printf("ODM is busy, restart timer %d ms.\n", restart_timer_ms);
	}
	else
		cm_eprintf("FW Update Reporting FAILURE\n");
}

void odm_start_fw_update_reporter(int dev_idx)
{
	dev_conf_t *conf = &cm_dev_conf[dev_idx];

	conf->fota_report_cnt = 0;
	cm_init_timer(&conf->fota_timer, odm_fw_update_report_timer_callback,
		(void *)dev_idx);
	cm_start_timer(&conf->fota_timer, FOTA_UPDATE_REPORT_TIMER_MS);
	cm_printf("Update reporter timer started!\n");
}

static int odm_should_report_fw_update(int dev_idx)
{
	cm_odm_t *cmodm = &cm_odm;
	int ret = 0;

	if (!access(cmodm->should_report_update_file, 0)) {
		cm_printf("Activated update file(%s)\n", cmodm->should_report_update_file);
		ret = 1;
	}
	return ret;
}

int odm_chk_fota(int dev_idx)
{
	int ret = 0;

	#if defined(SUPPORT_RESUMING_DOWNLOAD_FW)
	if ((ret = odm_chk_req_resuming_download(dev_idx))) {
		odm_req_resuming_download(dev_idx);
		return ret;
	}
	#endif

	if ((ret = odm_should_report_fw_update(dev_idx)))
		odm_start_fw_update_reporter(dev_idx);
	return ret;
}

#if defined(CONFIG_OMA_DM_DRMD)
static GCT_API_RET odm_report_drmd(void *data)
{
	int dev_idx = (int) data;
	GDEV_ID ID;
	GCT_API_RET ret;
	dev_conf_t *conf = &cm_dev_conf[dev_idx];
	char szDuration[24];
	int duration = 0;

	ID.deviceIndex = dev_idx;
	ID.apiHandle = cm_api_handle;

	cm_printf("Start DRMD reporting!\n");

	if (!access(cm_odm.drmd_duration_file, 0))
		cm_load_file_lines(cm_odm.drmd_duration_file, 1, szDuration);
	else {
		cm_eprintf("Not found duration(%s)!!\n", cm_odm.drmd_duration_file);
		unlink(cm_odm.should_report_drmd_file);
		unlink(cm_odm.drmd_duration_file);		
		return GCT_API_RET_FAILED;
	}

	duration = atoi(szDuration);

	if (2 == duration) {
		if (!wcscmp((wchar_t*)conf->nsp_name16_before, (wchar_t*)conf->nsp_name16_current)) {
			cm_eprintf("Not re-entry to the same NSP.!!\n");
			unlink(cm_odm.should_report_drmd_file);
			unlink(cm_odm.drmd_duration_file);
			return GCT_API_RET_FAILED;
		}
	}

	ret = GAPI_DMCTriggerDrmd(&ID);
	cm_printf("TriggerDrmd=%d\n", ret);
	if (ret == GCT_API_RET_SUCCESS) {
		unlink(cm_odm.should_report_drmd_file);
		unlink(cm_odm.drmd_duration_file);
	}

	return ret;
}

static int target_drmd_scenario(int dev_idx, int duration)
{
	/*NOTE: Add your measurement code by duration */

	dev_conf_t *conf = &cm_dev_conf[dev_idx];
	u32	timestamp;

	if (1 == duration) {
		/* The client SHALL take whatever metric data is immediately available
		from the device and SHALL immediately notify the server of the metric 
		data availability. */

		// Collect your metric data
	}
	else if (2 == duration) {
		/* Measurement SHALL be terminated at the end of the current session 
		(i.e. when the SS/MS next detaches from the network), and the client SHALL 
		notify the server of the metric data availability on re-entry to the same NSP. */

		while(1)
		{
			// Collect your metric data

			// Check your connection state
			if (conf->wimax_status != WIMAX_API_DEVICE_STATUS_Data_Connected)
				break;	

			sleep(1);
		}
	}
	else if (5 <= duration) {
		/* The value of duration represents the duration of the 2 data collection period in seconds. */

		timestamp = cm_gettimemsofday();
		while(1)
		{
			// Collect your metric data

			// Check your connection state
			if (conf->wimax_status != WIMAX_API_DEVICE_STATUS_Data_Connected)
				return -1;	

			// Cheick duration
			if ( (cm_gettimemsofday() - timestamp) > duration * 1000)
				break;
		
			sleep(1);
		}
	}

	return 0; // If fail, return -1 
}

static void *odm_drmd_start_thread(void *data)
{
	int dev_idx = (int) data;
	GDEV_ID ID;
	cm_odm_t *cmodm = &cm_odm;
	char szDuration[24];
	int duration = 0;

	ID.deviceIndex = dev_idx;
	ID.apiHandle = cm_api_handle;

	cm_printf("Start DRMD!\n");

	if (!access(cmodm->drmd_duration_file, 0))
		cm_load_file_lines(cmodm->drmd_duration_file, 1, szDuration);
	else {
		cm_eprintf("Not found duration(%s)!!\n", cmodm->drmd_duration_file);
		unlink(cmodm->should_report_drmd_file);
		unlink(cmodm->drmd_duration_file);
		goto out;
	}

	duration = atoi(szDuration);

	if(!target_drmd_scenario(dev_idx, duration))
	{
		cm_printf("DRMD measurement Done\n");
		if (2 == duration) {
			cm_printf("When re-entry to the same NSP, will be run DRMD trigger\n");
		}
		else
		{
			odm_report_drmd((void *)dev_idx);		
		}
	}
	else
	{
		cm_eprintf("No running DRMD trigger because DRMD measurement fail\n");
		unlink(cmodm->should_report_drmd_file);
		unlink(cmodm->drmd_duration_file);
	}
out:

	return NULL;
}

static void ind_odm_start_drmd(GDEV_ID_P pID, int duration)
{
	pthread_t pthread;
	char szDuration[24];

	sprintf(szDuration, "%d", duration);

	cm_creat_file(cm_odm.should_report_drmd_file, NULL, 0);
	cm_store_file_lines(cm_odm.drmd_duration_file, 1, szDuration);
	cm_dprintf("Run DRMD-start\n");
	pthread_create(&pthread, NULL, odm_drmd_start_thread, (void *)(int)pID->deviceIndex);
	pthread_detach(pthread);
}

static void ind_odm_set_drmd_node_data(GDEV_ID_P pID, char *uri, char *data, int len)
{
	/*NOTE: Store value for target_drmd_scenario()*/

	// Sample code
	
	if (!strcmp(uri, "./WiMAX_Diagnostics/Generic/IPPingDiagnostics/DiagnosticsState")) {

	}
	else if (!strcmp(uri, "./WiMAX_Diagnostics/Generic/IPPingDiagnostics/Interface")) {

	}
	else if (!strcmp(uri, "./WiMAX_Diagnostics/Generic/IPPingDiagnostics/Host")) {

	}
	else if (!strcmp(uri, "./WiMAX_Diagnostics/Generic/IPPingDiagnostics/NumberOfRepetition")) {

	}
	else if (!strcmp(uri, "./WiMAX_Diagnostics/Generic/IPPingDiagnostics/Timeout")) {

	}
	else if (!strcmp(uri, "./WiMAX_Diagnostics/Generic/IPPingDiagnostics/DataBlockSize")) {

	}
	else if (!strcmp(uri, "./WiMAX_Diagnostics/Generic/IPPingDiagnostics/DSCP")) {

	}
}

static void ind_odm_get_drmd_node_data(GDEV_ID_P pID, char *uri, char* data, int *plen)
{
	/*NOTE: Retrieve your measurement value from target_drmd_scenario()*/

	// Sample code
	
	char *supVold = "5000"; // 5000mV
	char *batteryCap = "1500"; // 1500mAh
	char *GPSlocation_Latitude = "100";
	char * GPSlocation_Longitude = "100";
	char * GPSlocation_Altitude = "100";
	char * Enable = "True";
	char * MACAddressControlEnabled =  "True";
	char * MaxBitrate =  "100";
	char * DuplexMode = "Dummy";
	char * LANEtherMACresults = "Dummy";
	char * DeviceUptime = "100";
	char * DeviceLog = "Dummy";
	char * IPPingDiagnosticsResults = "Dummy";
	
	if (!strcmp(uri, "./WiMAX_Diagnostics/Generic/SupVolt")) {
		if (NULL == data || 0 == *plen) {
			*plen = strlen(supVold);
			return;
		}

		strcpy(data, supVold);
		*plen = strlen(supVold);
		return;
	}
	else if (!strcmp(uri, "./WiMAX_Diagnostics/Generic/BatteryCap")) {
		if (NULL == data || 0 == *plen) {
			*plen = strlen(batteryCap);
			return;
		}

		strcpy(data, batteryCap);
		*plen = strlen(batteryCap);
		return;
	}
	else if (!strcmp(uri, "./WiMAX_Diagnostics/Generic/GPSlocation_Latitude")) {
		if (NULL == data || 0 == *plen) {
			*plen = strlen(GPSlocation_Latitude);
			return;
		}

		strcpy(data, GPSlocation_Latitude);
		*plen = strlen(GPSlocation_Latitude);
		return;
	}
	else if (!strcmp(uri, "./WiMAX_Diagnostics/Generic/GPSlocation_Longitude")) {
		if (NULL == data || 0 == *plen) {
			*plen = strlen(GPSlocation_Longitude);
			return;
		}

		strcpy(data, GPSlocation_Longitude);
		*plen = strlen(GPSlocation_Longitude);
		return;
	}
	else if (!strcmp(uri, "./WiMAX_Diagnostics/Generic/GPSlocation_Altitude")) {
		if (NULL == data || 0 == *plen) {
			*plen = strlen(GPSlocation_Altitude);
			return;
		}

		strcpy(data, GPSlocation_Altitude);
		*plen = strlen(GPSlocation_Altitude);
		return;
	}
	else if (!strcmp(uri, "./WiMAX_Diagnostics/Generic/LANEtherMAC/Enable")) {
		if (NULL == data || 0 == *plen) {
			*plen = strlen(Enable);
			return;
		}

		strcpy(data, Enable);
		*plen = strlen(Enable);
		return;
	}
	else if (!strcmp(uri, "./WiMAX_Diagnostics/Generic/LANEtherMAC/MACAddressControlEnabled")) {
		if (NULL == data || 0 == *plen) {
			*plen = strlen(MACAddressControlEnabled);
			return;
		}

		strcpy(data, MACAddressControlEnabled);
		*plen = strlen(MACAddressControlEnabled);
		return;
	}
	else if (!strcmp(uri, "./WiMAX_Diagnostics/Generic/LANEtherMAC/MaxBitrate")) {
		if (NULL == data || 0 == *plen) {
			*plen = strlen(MaxBitrate);
			return;
		}

		strcpy(data, MaxBitrate);
		*plen = strlen(MaxBitrate);
		return;
	}
	else if (!strcmp(uri, "./WiMAX_Diagnostics/Generic/LANEtherMAC/DuplexMode")) {
		if (NULL == data || 0 == *plen) {
			*plen = strlen(DuplexMode);
			return;
		}

		strcpy(data, DuplexMode);
		*plen = strlen(DuplexMode);
		return;
	}
	else if (!strcmp(uri, "./WiMAX_Diagnostics/Generic/LANEtherMAC/LANEtherMACresults")) {
		if (NULL == data || 0 == *plen) {
			*plen = strlen(LANEtherMACresults);
			return;
		}

		strcpy(data, LANEtherMACresults);
		*plen = strlen(LANEtherMACresults);
		return;
	}
	else if (!strcmp(uri, "./WiMAX_Diagnostics/Generic/DeviceUptime")) {
		if (NULL == data || 0 == *plen) {
			*plen = strlen(DeviceUptime);
			return;
		}

		strcpy(data, DeviceUptime);
		*plen = strlen(DeviceUptime);
		return;
	}
	else if (!strcmp(uri, "./WiMAX_Diagnostics/Generic/DeviceLog")) {
		if (NULL == data || 0 == *plen) {
			*plen = strlen(DeviceLog);
			return;
		}

		strcpy(data, DeviceLog);
		*plen = strlen(DeviceLog);
		return;
	}
	else if (!strcmp(uri, "./WiMAX_Diagnostics/Generic/IPPingDiagnostics/IPPingDiagnosticsResults")) {
		if (NULL == data || 0 == *plen) {
			*plen = strlen(IPPingDiagnosticsResults);
			return;
		}

		strcpy(data, IPPingDiagnosticsResults);
		*plen = strlen(IPPingDiagnosticsResults);
		return;
	}
}
static void odm_drmd_report_timer_callback(void *data)
{
	int dev_idx = (int) data;
	dev_conf_t *conf = &cm_dev_conf[dev_idx];
	GDEV_ID ID;
	GCT_API_RET gret;
	int restart_timer_ms = DRMD_REPORT_TIMER_MS;

	ID.deviceIndex = dev_idx;
	ID.apiHandle = cm_api_handle;

	if (++conf->drmd_report_cnt > DRMD_REPORT_TRY_CNT) {
		cm_eprintf("DRMD Reporting Failure! (cnt=%d)\n", conf->drmd_report_cnt);
		return;
	}

	gret = odm_report_drmd((void *) dev_idx);
	if (gret == GCT_API_RET_SUCCESS)
		cm_printf("DRMD Reporting SUCCESS\n");
	else if (gret == GCT_API_RET_BUSY) {
		cm_start_timer(&conf->drmd_timer, restart_timer_ms);
		cm_printf("ODM is busy, restart timer %d ms.\n", restart_timer_ms);
	}
	else
		cm_eprintf("DRMD Reporting FAILURE\n");
}

void odm_start_drmd_reporter(int dev_idx)
{
	dev_conf_t *conf = &cm_dev_conf[dev_idx];

	conf->fota_report_cnt = 0;
	cm_init_timer(&conf->drmd_timer, odm_drmd_report_timer_callback,
		(void *)dev_idx);
	cm_start_timer(&conf->drmd_timer, DRMD_REPORT_TIMER_MS);
	cm_printf("DRMD reporter timer started!\n");
}

static int odm_should_report_drmd(int dev_idx)
{
	cm_odm_t *cmodm = &cm_odm;
	int ret = 0;

	if (!access(cmodm->should_report_drmd_file, 0)) {
		cm_printf("Activated DRMD file(%s)\n", cmodm->should_report_drmd_file);
		ret = 1;
	}
	return ret;
}

int odm_chk_drmd(int dev_idx)
{
	int ret = 0;

	if ((ret = odm_should_report_drmd(dev_idx)))
		odm_start_drmd_reporter(dev_idx);
	return ret;
}
#endif // CONFIG_OMA_DM_DRMD
#endif // CONFIG_OMA_DM_CLIENT

void reg_indications(APIHAND *api_handle)
{
	GCT_API_RET ret;

	ret = GAPI_RegRcvHCIPacketFunc(api_handle, ind_recv_hci_packet);
	if (ret != GCT_API_RET_SUCCESS)
		cm_eprintf("GAPI_RegRcvHCIPacketFunc error=%d\n", ret);

	ret = GAPI_RegPowerModeChange(api_handle, ind_powermode_change);
	if (ret != GCT_API_RET_SUCCESS)
		cm_eprintf("GAPI_RegPowerModeChange error=%d\n", ret);

	ret = GAPI_SubscribeDeviceInsertRemove(api_handle, ind_dev_insert_remove);
	if (ret != GCT_API_RET_SUCCESS)
		cm_eprintf("GAPI_SubscribeDeviceInsertRemove error=%d\n", ret);
	
	ret = GAPI_SubscribeControlPowerManagement(api_handle, ind_power_mng);
	if (ret != GCT_API_RET_SUCCESS)
		cm_eprintf("GAPI_SubscribeControlPowerManagement error=%d\n", ret);

	ret = GAPI_SubscribeConnectToNetwork(api_handle, ind_connect_network);
	if (ret != GCT_API_RET_SUCCESS)
		cm_eprintf("GAPI_SubscribeConnectToNetwork error=%d\n", ret);
	
	ret = GAPI_SubscribeDisconnectFromNetwork(api_handle, ind_dev_disconnect_network);
	if (ret != GCT_API_RET_SUCCESS)
		cm_eprintf("GAPI_SubscribeDisconnectFromNetwork error=%d\n", ret);
	
	ret = GAPI_SubscribeNetworkSearchWideScan(api_handle, ind_dev_network_search_widescan);
	if (ret != GCT_API_RET_SUCCESS)
		cm_eprintf("GAPI_SubscribeNetworkSearchWideScan error=%d\n", ret);

	ret = GAPI_SubscribeProvisioningOperation(api_handle, ind_dev_provisioning_operation);
	if (ret != GCT_API_RET_SUCCESS)
		cm_eprintf("GAPI_SubscribeProvisioningOperation error=%d\n", ret);
	
	ret = GAPI_SubscribeDeviceStatusChange(api_handle, ind_dev_status);
	if (ret != GCT_API_RET_SUCCESS)
		cm_eprintf("GAPI_SubscribeDeviceStatusChange error=%d\n", ret);
	
	ret = GAPI_SubscribeNotiFunc(api_handle, ind_notification);
	if (ret != GCT_API_RET_SUCCESS)
		cm_eprintf("GAPI_SubscribeNotiFunc error=%d\n", ret);

	#if defined(CONFIG_OMA_DM_CLIENT)
	if (CM_USE_ODM && cm_common_conf.api_mode != GCT_WIMAX_API_PRIVILEGE_READ_ONLY) {
		setup_odm_config();

		ret = GAPI_SubscribeODMFwGetFileInf(api_handle,
					ind_odm_get_fw_file_info, ind_odm_get_fw_free_space);
		if (ret != GCT_API_RET_SUCCESS)
			cm_eprintf("GAPI_SubscribeODMFwGetFileInf error=%d\n", ret);
		
		ret = GAPI_SubscribeODMFwSessionInf(api_handle,
					ind_odm_get_fw_session_info, ind_odm_set_fw_session_info);
		if (ret != GCT_API_RET_SUCCESS)
			cm_eprintf("GAPI_SubscribeODMFwSessionInf error=%d\n", ret);
		
		ret = GAPI_SubscribeODMFwPkgURL(api_handle,
					ind_odm_get_fw_pkg_url, ind_odm_set_fw_pkg_url);
		if (ret != GCT_API_RET_SUCCESS)
			cm_eprintf("GAPI_SubscribeODMFwPkgURL error=%d\n", ret);
		
		ret = GAPI_SubscribeODMFwSetDlStatus(api_handle,
					ind_odm_set_fw_dl_progress, ind_odm_set_fw_dl_result);
		if (ret != GCT_API_RET_SUCCESS)
			cm_eprintf("GAPI_SubscribeODMFwSetDlStatus error=%d\n", ret);
		
		ret = GAPI_SubscribeODMFwRunUpdate(api_handle, ind_odm_run_fw_update);
		if (ret != GCT_API_RET_SUCCESS)
			cm_eprintf("GAPI_SubscribeODMFwRunUpdate error=%d\n", ret);

		#if defined(CONFIG_OMA_DM_DRMD)
		ret = GAPI_SubscribeODMDrmdStart(api_handle, ind_odm_start_drmd);
		if (ret != GCT_API_RET_SUCCESS)
			cm_eprintf("GAPI_SubscribeODMDrmdStart error=%d\n", ret);

		ret = GAPI_SubscribeODMDrmdSetNodeData(api_handle, ind_odm_set_drmd_node_data);
		if (ret != GCT_API_RET_SUCCESS)
			cm_eprintf("GAPI_SubscribeODMDrmdSetNodeData error=%d\n", ret);

		ret = GAPI_SubscribeODMDrmdGetNodeData(api_handle, ind_odm_get_drmd_node_data);
		if (ret != GCT_API_RET_SUCCESS)
			cm_eprintf("GAPI_SubscribeODMDrmdGetNodeData error=%d\n", ret);
		#endif // CONFIG_OMA_DM_DRMD
	}
	#endif // CONFIG_OMA_DM_CLIENT

	#if defined(CONFIG_ENABLE_SERVICE_FLOW)
	ret = GAPI_SubscribeNotiServiceFlow(api_handle, ind_noti_service_flow);
	if (ret != GCT_API_RET_SUCCESS)
		cm_eprintf("GAPI_SubscribeNotiFunc error=%d\n", ret);
	#endif // CONFIG_ENABLE_SERVICE_FLOW
	
}

