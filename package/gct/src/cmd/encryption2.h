#ifndef __ENCRYPTION_H__
#define __ENCRYPTION_H__

#define GCT_KEY_DEC_UNIT	16
#define GCT_KEY_ENC_UNIT	16

typedef	unsigned char	uint1;
typedef	unsigned short	uint2;
typedef	unsigned int	uint4;

uint4	GCT2_Key_Enc(	uint1	SS_MAC_ADDR[/*6*/],
						uint1	Key[/*16*/],
						uint1	EncKey[/*16*/] );
uint4	GCT2_Key_Dec(	uint1	SS_MAC_ADDR[/*6*/],
						uint1	EncKey[/*16*/],
						uint1	Key[/*16*/]);

uint4	GCT2_Key_Enc(	uint1	SS_MAC_ADDR[/*6*/],
						uint1	Key[/*16*/],
						uint1	EncKey[/*16*/] );

#endif

