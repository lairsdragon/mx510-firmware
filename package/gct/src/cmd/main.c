#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <pwd.h>

#include "cm.h"
#include "exitcb.h"

#define CM_VERSION "1.4.5"
#define RUN_AS_USER "root"
#define DAEMON_NAME "cmd"
#define LOCK_FILE "/var/lock/cmd"

extern int cm_init(int read_only);
extern int cm_deinit(void);
extern void cmd_run(void);

static void serve_usr1() {
	cmd_log(LOG_NOTICE, 
		"this is serve_usr1 routine\n");
	/* for debug */	
}

static void serve_usr2() {
	cmd_log(LOG_NOTICE, 
		"this is serve_usr2 routine\n");
	/* for debug */	
}

static void soft_exit(int status) {
	int ret;
	cmd_log(LOG_NOTICE, 
		"exiting gracefully with status=%d\n", status);
	ret = cm_deinit();
	cmd_log(LOG_NOTICE, 
		"cm_deinit() (%d)\n", ret);
	process_exit_cb();
	cmd_log(LOG_NOTICE, 
		"process_exit_cb()\n");
	closelog();
	exit(status);
}

static void parent_sig_handler(int signum) {
	cmd_log(LOG_NOTICE, "parent: recieved signal %d", signum);

    switch(signum) {
    	case SIGCHLD:
			cmd_log(LOG_NOTICE, "parent: killing myself\n");
        	exit(EXIT_SUCCESS);
        	break;
    	case SIGTERM:
			cmd_log(LOG_NOTICE, "parent: killing myself\n");
        	exit(EXIT_SUCCESS);
        	break;
		case SIGALRM:
			cmd_log(LOG_NOTICE, "parent: killing myself\n");
        	exit(EXIT_SUCCESS);
        	break;
		case SIGUSR1:
			cmd_log(LOG_NOTICE, "parent: killing myself\n");
        	exit(EXIT_SUCCESS);
        	break;
    }
}

static void child_sig_handler(int signum) {
	cmd_log(LOG_NOTICE, 
		"recieved signal %d", signum);
	/* TODO implement handler */
    switch(signum) {
    	case SIGTERM:
			soft_exit(EXIT_SUCCESS);
        	break;
		case SIGALRM:
        	break;
		case SIGUSR1:
			serve_usr1();
        	break;
		case SIGUSR2:
			serve_usr2();
        	break;
    }
}

static void daemonize() {
	pid_t pid, sid, parent;
	struct passwd *pw = NULL;
	int fd = -1;
	char lockpid[64];

	/* open logs */
	openlog(DAEMON_NAME, LOG_PID, LOG_LOCAL5);
	cmd_log(LOG_NOTICE, "daemonizing...\n"); 

	/* already a daemon */
    if ((pid = getppid()) == 1) {
		cmd_log(LOG_NOTICE, "already daemon, switching to tasks\n");
		return;
	}

	/* trap signals */
    if (signal(SIGCHLD, parent_sig_handler) == SIG_ERR)
		cmd_log(LOG_ERR, "error registering handler for SIGCHLD\n");
    if (signal(SIGTERM, parent_sig_handler) == SIG_ERR)
		cmd_log(LOG_ERR, "error registering handler for SIGTERM\n");
	if (signal(SIGALRM, parent_sig_handler) == SIG_ERR)
		cmd_log(LOG_ERR, "error registering handler for SIGALRM\n");
	if (signal(SIGUSR1, parent_sig_handler) == SIG_ERR)
		cmd_log(LOG_ERR, "error registering handler for SIGUSR1\n");
	
	/* fork off the parent process */
	if ((pid = fork()) < 0) {
		cmd_log(LOG_ERR, "fork error\n");
		exit(EXIT_FAILURE);
	}

	/* exit the parent process */
	if (pid > 0) {
		cmd_log(LOG_NOTICE, "waiting for child\n");
		alarm(3);
		pause();
		exit(EXIT_SUCCESS);
	}

	/* we are executing as child now */
    parent = getppid();

	/* change the file mode mask */
	umask(0);  
                    
	/* create a new SID */
	if ((sid = setsid()) < 0) {
		cmd_log(LOG_ERR, "failed creating new SID; exiting\n");
		exit(EXIT_FAILURE);
	}

	/* change working directory */
	if ((chdir("/etc/gct")) < 0) {
		cmd_log(LOG_ERR, "failed changing directory; exiting\n");
		exit(EXIT_FAILURE);
	}
        
	/* close file descriptors */
	if (close(STDIN_FILENO) != 0)
		cmd_log(LOG_ERR, "error closing STDIN_FILENO\n");
	if (close(STDOUT_FILENO) != 0)
		cmd_log(LOG_ERR, "error closing STDOUT_FILENO\n");
	if (close(STDERR_FILENO) != 0)
		cmd_log(LOG_ERR, "error closing STDERR_FILENO\n");

	/* create lock file */
	if ((fd = open(LOCK_FILE, O_RDWR | O_CREAT, 0640)) < 0) {
		cmd_log(LOG_ERR, "unable to create lock file\n");
		exit(EXIT_FAILURE);
	}
	if (lockf(fd, F_TLOCK, 0) < 0) {
		cmd_log(LOG_NOTICE, "already running, exiting");
		exit(EXIT_SUCCESS);
	}
	sprintf(lockpid, "%d\n", getpid());
	write(fd, lockpid, strlen(lockpid));
   
	/* drop user if there is one */
    if (getuid() == 0 || geteuid() == 0) {
        if ((pw = getpwnam(RUN_AS_USER)) != NULL) {
            cmd_log(LOG_NOTICE, "setting user to " RUN_AS_USER);
            if (setuid(pw->pw_uid) != 0)
				cmd_log(LOG_ERR, "error seting uid\n");
        } else {
			cmd_log(LOG_NOTICE, "cannot set user to " RUN_AS_USER);
		}
    } else {
		cmd_log(LOG_NOTICE, "we have no root privilegies\n");
	}

	/* register child signal handlers */
	if (signal(SIGTERM, child_sig_handler) == SIG_ERR)
		cmd_log(LOG_ERR, "error registering handler for SIGTERM\n");
	if (signal(SIGALRM, child_sig_handler) == SIG_ERR)
		cmd_log(LOG_ERR, "error registering handler for SIGALRM\n");
	if (signal(SIGUSR1, child_sig_handler) == SIG_ERR)
		cmd_log(LOG_ERR, "error registering handler for SIGUSR1\n");
	if (signal(SIGUSR2, child_sig_handler) == SIG_ERR)
		cmd_log(LOG_ERR, "error registering handler for SIGUSR2\n");
	if (signal(SIGHUP, SIG_IGN) == SIG_ERR)
		cmd_log(LOG_ERR, "error registering handler for SIGHUP\n");
    if (signal(SIGCHLD, SIG_IGN) == SIG_ERR)
		cmd_log(LOG_ERR, "error registering handler for SIGCHLD\n");
    if (signal(SIGTSTP, SIG_IGN) == SIG_ERR)
		cmd_log(LOG_ERR, "error registering handler for SIGTSTP\n");
    if (signal(SIGTTOU, SIG_IGN) == SIG_ERR)
		cmd_log(LOG_ERR, "error registering handler for SIGTTOU\n");
    if (signal(SIGTTIN, SIG_IGN) == SIG_ERR)
		cmd_log(LOG_ERR, "error registering handler for SIGTTIN\n");

	/* tell parent that we are ok*/
    if (kill(parent, SIGUSR1) != 0)
		cmd_log(LOG_ERR, "error sending SIGUSR1 to parent\n");	
}

int init_gct(int mode) {
	int ret;
	init_exit_cb();
	cmd_log(LOG_NOTICE, "init_exit_cb()\n");
	ret = cm_init(mode);
	cmd_log(LOG_NOTICE, "cm_init() (%d) mode=%d\n", ret, mode);
	return ret;
}

int main(int argc, char *argv[])
{
	int ret, mode;
	
	/* TODO implement CLI args */
	if (argc == 1) {
		mode = 0;	/*Read/Write Mode*/
		fprintf(stdout, "(read/write mode)\n");
	} else {
		mode = 1;	/*Read Only Mode*/
		fprintf(stdout, "(read only mode) - ignoring CLI arguments\n");
	}
	
	/* conventional daemonization */
	daemonize();

	/* GCT logic initialization */	
	ret = init_gct(mode);
	
	/* command dispatcher */  
	cmd_run();
	
	exit(ret);
}
