#ifndef EAP_H_20081010
#define EAP_H_20081010

#if defined(CONFIG_OMA_DM_CLIENT)
#include "odm.h"
#endif

//#define SUPPORT_CR901
//#define SUPPORT_CR1074
//#define APPLY_REALM_VERIFICATION

//#define SUPPORT_802_11G

#define CR801_TRY_COUNT		3
#define CR801_SERVER_REJECT_LIMIT	3

#define WIMAX_EAP_STR_LEN			256
#define WIMAX_EAP_FILE_LEN			256
#define WIMAX_EAP_MSK_LEN			64
#define WIMAX_EAP_EMSK_LEN			64

#define MAX_DECORATION_NR			2
#define DECORATION_IDX1				0
#define DECORATION_IDX2				1
#define WIMAX_EAP_DECO_LEN			128

#define MAX_EAP_PARAM_SIZE			256
#define EAP_CHECK_REALM_LIST_SIZE	8

#define MAX_EAP_BUF_SIZE			2048

#define USE_MEM_CERT

#define CACERT_NUM					10

#define CERT_LINE_FEED				'\n'

#define E_WM_CR801_EAP_FAILURE		-201
#define E_WM_UNKNOWN_SERVER_REALM	-202

typedef struct file_data_s {
	int len;
	void *data;

} file_data_t;

typedef struct eap_internal_s {
	unsigned int data[8];

} eap_internal_t;

#define OMA_EAP_SERVER_REALMS	2
#define CR801_DISABLE		0
#define CR801_TTLS			1
#define CR801_TLS			2

typedef struct wm_eap_tls_param_s {
	/*Parameters*/
	u8		type;
	u16		frag_size;
	char	use_delimiter;
	bool	dev_cert_null;
	bool	ca_cert_null;
	bool	disable_resumptoin;
	bool	disable_sessionticket;

	bool	use_nv_info;
	char	userid[WIMAX_EAP_STR_LEN];		/*Inner NAI*/
	char	userid_pwd[WIMAX_EAP_STR_LEN];	/*Inner NAI password*/
	char	anony_id[WIMAX_EAP_STR_LEN];	/*Outer NAI*/
	char	pri_key_pwd[WIMAX_EAP_STR_LEN];
	char	visited_realm[WIMAX_EAP_STR_LEN];
	#if defined(CONFIG_HOST_EAP)
	bool	use_nv_cert;

	#if defined(USE_MEM_CERT)
	file_data_t	dev_cert_data;	/*client cert*/
	file_data_t	ca_cert_data[CACERT_NUM];
	#endif
	/*Cert files*/
	char	dev_cert_file[WIMAX_EAP_FILE_LEN];	/*client cert*/
	char	dev_cert_key_file[WIMAX_EAP_FILE_LEN];
	char	ca_cert_file[CACERT_NUM][WIMAX_EAP_FILE_LEN];
	#endif

	char	decoration[MAX_DECORATION_NR][WIMAX_EAP_DECO_LEN];

	bool	cr801_enabled;
	u8		cr801_mode;
	int		cr801_server_reject_cnt;

	bool	cr901_enabled;
	bool	cr1074_enabled;
	bool	check_realm;
	int		configured_realm_list_cnt;
	char	configured_realm_list[EAP_CHECK_REALM_LIST_SIZE][MAX_EAP_PARAM_SIZE];

	eap_internal_t eap_data;

	bool	log_enabled;
} wm_eap_param_t;

typedef struct wm_cert_type_s {
	u32		level;
	u16		source;
	char	subject_cn[WIMAX_EAP_STR_LEN];
	char	issuer_cn[WIMAX_EAP_STR_LEN];
	char	expire_date[WIMAX_EAP_STR_LEN];

} wm_cert_type_t;

void eap_start_e_eaplog_thread(int dev_idx);
void eap_prepare_log_env(int dev_idx, bool enable);
int eap_packet_handle(int dev_idx, char *buf, int len);
int eap_alert_net_entry(int dev_idx);
int eap_reinit_session(int dev_idx);
int eap_init(int dev_idx);
void eap_deinit(int dev_idx);
int eap_noti_text(int dev_idx, char *str, bool is_err);
int send_eap_tls_mask(int dev_idx, char *mask, int mask_len);
int send_eap_tls_resp(int dev_idx, char *buf, int len);

unsigned int ParsingOuterNAI(const char *Outer_NAI,			// IN
						  char		 *routing_info,			// OUT
						  char		 *wm_decoration,		// OUT
						  char		 *userid,				// OUT
						  char		 *realm);
#endif
