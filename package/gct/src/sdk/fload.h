#if !defined(FLOAD_H_20080930)
#define FLOAD_H_20080930
#include "global.h"

typedef enum {
	eap_param_type_outer_nai = 1,
	eap_param_type_priv_key_pwd,
	eap_param_type_inner_id,
	eap_param_type_inner_pwd

} eap_nvparam_type_t;

#define EAP_NVPARAM_SIZE	(2*1024)

int gzip_file(const char *file, const char *zip_file);
int gzip_buf(void *buf, int size, const char *zip_file);
int gunzip_file(const char *zip_file, const char *file);
int decrypt_file(const char *mac_addr, const char *enc_file, const char *file);
int encrypt_file(const char *mac_addr, const char *file, const char *enc_file);
int decrypt_buf(const char *mac_addr, char *src, char *dst, int size);

int fl_upload(int dev_idx, int type, const char *path, void (*progress)(int bytes));
int fl_download(int dev_idx, int type, const char *path, void (*progress)(int bytes));

int bl_upload(int dev_idx, int type, void *buf, int size, void (*progress)(int bytes));
int bl_download(int dev_idx, int type, void *buf, int size, void (*progress)(int bytes));

int fl_read_file(int dev_idx, const char *target_file,
		const char *host_file, void (*progress)(int bytes));

#endif

