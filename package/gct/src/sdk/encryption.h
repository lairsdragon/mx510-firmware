#ifndef __ENCRYPTION_H__
#define __ENCRYPTION_H__

#define GCT_KEY_DEC_UNIT	16
#define GCT_KEY_ENC_UNIT	16

typedef	unsigned char	uint1;
typedef	unsigned short	uint2;
typedef	unsigned int	uint4;

uint4	GCT_Key_Enc(	uint1	SS_MAC_ADDR[/*6*/],
						uint1	Key[/*16*/],
						uint1	EncKey[/*16*/] );
uint4	GCT_Key_Dec(	uint1	SS_MAC_ADDR[/*6*/],
						uint1	EncKey[/*16*/],
						uint1	Key[/*16*/]);
void OMA_DM_HMAC_Key_Generation(
	unsigned char * src, 
	unsigned char * dst);

int	OMA_DM_AES_128_CCM_Encrypt(	uint1	K[/*16*/],						// In
								uint1	P[],			uint4 PLen,		// In
								uint1	Nonce[/*13*/],					// In
								uint1	C[],			uint4 *CLen	);
int	OMA_DM_AES_128_CCM_Decrypt( uint1	K[/*16*/],						// In
								uint1	C[],			uint4 CLen,		// In
								uint1	Nonce[/*13*/],					// In
								uint1	P[],			uint4 *PLen );	// Out
#endif
