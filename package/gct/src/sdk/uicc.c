#define UICC_C_20090806

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdarg.h>
#include <assert.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <linux/kernel.h>
#include <sys/ioctl.h>

#include "pcsclite.h"
#include "winscard.h"

#include "gcttype.h"
#include "error.h"
#include "device.h"
#include "wimax.h"
#include "hci.h"
#include "fload.h"
#include "log.h"
#include "eap.h"
#include "sdk.h"

typedef unsigned long       ReturnType;

#define UICC_DEBUG_LEVEL 4

typedef enum _Vendor_T     // added by clark 2007.08.07.
{
    unknown = 0,
    kt_uicc,
    sk_telecom_uicc,
} Vendor_T;

//  EAP_UICC_RETURN_CODEs

#define EAP_UICC_BASE                           50
#define EAP_UICC_AUTH_FAILURE                   EAP_UICC_BASE + 1
#define EAP_UICC_MSG_IGNORED                    EAP_UICC_BASE + 2
#define EAP_UICC_WITH_ERROR                     EAP_UICC_BASE + 3
#define EAP_UICC_MSK_SUCCESS                    EAP_UICC_BASE + 4
#define EAP_UICC_WITH_NOTIFICATION_DATA         EAP_UICC_BASE + 5
#define EAP_UICC_ONLY_PACKET                    EAP_UICC_BASE + 6
#define EAP_UICC_WITH_AKA_NOTIFICATION_CODE     EAP_UICC_BASE + 7
#define EAP_UICC_WITH_ERROR_CODE                EAP_UICC_BASE + 8
#define EAP_UICC_WITH_UNKNOWN_CASE_DATA         EAP_UICC_BASE + 9

#define     MAX_STR_LEN     80
#define     MAX_BUF_LEN     256

// USIM
// CLASS
#define CLS_00              0x00
#define CLS_A0              0xA0

// INS
#define SELECT              0xA4
#define STATUS              0xF2

#define READ_BINARY         0xB0
#define UPDATE_BINARY       0xD6
#define READ_RECORD         0xB2
#define UPDATE_RECORD       0xDC
#define SEEK                0xA2
#define INCREASE            0x32

#define VERIFY_CHV          0x20
#define CHANGE_CHV          0x24
#define DISABLE_CHV         0x26
#define ENABLE_CHV          0x28
#define UNBLOCK_CHV         0x2C

#define INVALIDATE          0x04
#define REHABILITATE        0x44

#define RESET_STATE         0x19
#define RUN_GSM_ALGORITHM   0x88        // for GSM version.
#define AUTHENTICATE        0x88        // for WSIM version.

#define SLEEP               0xFA
#define GET_RESPONSE        0xC0
#define TERMINAL_PROFILE    0x10
#define ENVELOPE            0xC2
#define FETCH               0x12
#define TERMINAL_RESPONSE   0x14

//  FCP TAGs

#define FILE_DESC               0x82
#define FILE_ID                 0x83
#define DF_AID_NAME             0x84
#define PROPRIETRY_INFO         0xA5
#define LIFE_CYCLE_STATUS_INT   0x8A
#define SECURITY_ATTR_8C        0x8C
#define SECURITY_ATTR_8B        0x8B
#define SECURITY_ATTR_AB        0xAB
#define PIN_STATUS_DO           0xC6
#define FILE_SIZE               0x80
#define TOTAL_FILE_SIZE         0x81
#define SHORT_FILE_ID           0x88

//  Application Template TAGs

#define APPL_ID                 0x4F
#define APPL_LABEL              0x50
#define DISCRETIONARY           0x73


// end USIM

typedef struct _tPDU_
{
    unsigned char   m_Data[MAX_BUF_LEN];
    unsigned long   m_Len;

} tPDU;

typedef struct _tAPDU_
{
    unsigned char   CLS;
    unsigned char   INS;
    unsigned char   P1;
    unsigned char   P2;
    unsigned char   Lc;
    unsigned char   Data[256];
    unsigned char   Le;

} tAPDU;


typedef struct _tFileDescriptor_
{   // Tag : 0x82
    unsigned char   b_set;
    unsigned char   length;             // 2 or 5
    unsigned char   FileDescByte;
    unsigned char   DataCodingByte;
    unsigned char   RecordLength[2];    //[OPTION]
    unsigned char   NumberRecord;       //[OPTION]

} tFileDescriptor;

typedef struct _tFileID_
{   // Tag : 0x83
    unsigned char   b_set;
    unsigned char   length;             // 2
    unsigned char   ID[2];

} tFileID;

typedef struct _tDF_AID_Name_
{   // Tag : 0x84
    unsigned char   b_set;
    unsigned char   length;             // 1 <= X <= 16
    unsigned char   Name[16];

} tDF_AID_Name;

typedef struct _tProprietry_Info_
{   // Tag : 0xA5
    unsigned char   b_set;
    unsigned char   length;
    unsigned char   Data[256];

} tProprietry_Info;

typedef struct _tLife_Cycle_Status_Int_
{   // Tag : 0x8A
    unsigned char   b_set;
    unsigned char   length;
    unsigned char   Data;

} tLife_Cycle_Status_Int;

typedef struct _tSecurity_Attr_
{   // Tag : 0x8B, 0x8C ,(0xAB : LATER)
    unsigned char   b_set;
    unsigned char   tag;
    unsigned char   length;

    // Tag : 0x8C
    unsigned char   AM_Byte;
    unsigned char   SC_Bytes[256];

    // Tag : 0x8B
    unsigned char   EF_ARR_FID[2];

    //if length 2 + X * 2, (SEID,EF_ARR_Record_Num) set lists. LATER
    unsigned char   SEID;
    unsigned char   EF_ARR_Record_Num;

    // Tag : 0xAB
    unsigned char   Data[256];

} tSecurity_Attr;

typedef struct _tPIN_Status_Template_
{   // Tag : 0xC6
    unsigned char   b_set;
    unsigned char   length;
    unsigned char   Data[256];              // parsing LATER.

} tPIN_Status_Template;

typedef struct _tFile_Size_
{   // Tag : 0x80 (File Size), 0x81 (Total File Size)
    unsigned char   b_set;
    unsigned char   tag;
    unsigned char   length;
    unsigned char   Size[256];

} tFile_Size;

typedef struct _tSFI_
{   // Tag : 0x88
    unsigned char   b_set;
    unsigned char   length;
    unsigned char   ID[2];

} tSFI;

typedef struct _tFCP_
{   // 82,83,84,A5,8A,8B,8C,C6,81
    unsigned char           length;
    tFileDescriptor         FileDescriptor;         // 82
    tFileID                 FileID;                 // 83
    tDF_AID_Name            DF_AID_Name;            // 84
    tProprietry_Info        Proprietry_Info;        // A5
    tLife_Cycle_Status_Int  Life_Cycle_Status_Int;  // 8A
    tSecurity_Attr          Security_Attr;          // 8C,8B,(AB)
    tPIN_Status_Template    PIN_Status_Template;    // C6
    tFile_Size              File_Size;              // 80
    tFile_Size              Total_File_Size;        // 81
    tSFI                    SFI;            // 88

} tFCP;

typedef struct _tApplication_ID_
{
    unsigned char           b_set;
    unsigned char           length;
    unsigned char           ID[16];

} tApplication_ID;

typedef struct _tApplication_Label_
{
    unsigned char           b_set;
    unsigned char           length;
    unsigned char           Value[80];

} tApplication_Label;

typedef struct _tDiscretionary_
{
    unsigned char           b_set;
    unsigned char           length; 
    unsigned char           Data[256];

} tDiscretionary;


typedef struct _tApplication_Template_  
{
    unsigned char           length; 
    tApplication_ID         App_ID;                 // 4F   
    tApplication_Label      App_Label;              // 50   
    tDiscretionary          Discretionary;          // 73   

} tApplication_Template;


typedef struct _tUICC_CONTEXT_
{

    /* Now, m_hSCardContext is a global variable defined in eap.c */
	//SCARDCONTEXT    m_hScardContext;
    SCARDHANDLE     m_hScardHandle;

    tAPDU           C_APDU; 
    tAPDU           R_APDU; 

    tPDU            PDU;

    unsigned char   SW1;
    unsigned char   SW2;

    Vendor_T gVendor;	// astrocosmos

} tUICC_Context;

typedef struct _tEAP_AKA_UICC_CONTEXT_ 
{
	tUICC_Context           UICC_Context;
	tFCP                    fcp;
	tApplication_Template   app;

	unsigned char           MSK[ 64 ];
	unsigned char           EMSK[ 64 ];
	unsigned char           IdentityType;
	unsigned char           IdentityLength;
	unsigned char           Identity[ 256 ];
	unsigned char           AuthenticationMode;

	unsigned char           R_Buff[ MAX_BUF_LEN * 5 ];
	unsigned long           R_Buff_Len;

} tEAP_AKA_UICC_Context;


#define UICC_PRINT(fmt, args...)	xprintf(SDK_DBG, fmt, ## args)


static unsigned char uicc_desc_SW( unsigned char   SW1,
                            unsigned char   SW2);   
static unsigned long uicc_get_SW(unsigned char APDU[], 
                          unsigned long APDU_Len, 
                          unsigned char *SW1,
                          unsigned char *SW2);
static ReturnType uicc_send_APDU_T0(   tUICC_Context   *pUICC_Context,
                                tAPDU           *C_APDU,
                                unsigned char   R_APDU[],
                                unsigned long   *R_APDU_Len);

/* Only one context is needed for an application */
static SCARDCONTEXT	m_hScardContext = 0;

// EF IDs
static unsigned char   EF_DIR[2] = {0x2F,0x00};
static unsigned char   EF_EAPKEYS[2] = {0x4F,0x01};

//#define CHECK_PIN
#if defined(CHECK_PIN)
static unsigned char   PIN1[4] = { 0x30,0x30,0x30,0x30 }; //,0xFF,0xFF,0xFF,0xFF;
#endif

static void pdu_init(  tPDU *pdu,
                unsigned short len)
{
    memset(pdu->m_Data,0,MAX_BUF_LEN);
    pdu->m_Len = len;
}

#if (UICC_DEBUG_LEVEL == 4)
void apdu_print( tAPDU *APDU)
{		
	xprintf(SDK_DBG, "--------------------\n");
	xprintf(SDK_DBG, "   CLS  = %02X\n", APDU->CLS);
	xprintf(SDK_DBG, "   INS  = %02X\n", APDU->INS);
	xprintf(SDK_DBG, "   P1   = %02X\n", APDU->P1);
	xprintf(SDK_DBG, "   P2   = %02X\n", APDU->P2);
	xprintf(SDK_DBG, "   Lc   = %02X\n", APDU->Lc);
	xprintf_hex(SDK_DBG, "APDU", APDU->Data, sizeof(APDU->Data));
	xprintf(SDK_DBG, "   Le   = %02X\n", APDU->Le);
	xprintf(SDK_DBG, "--------------------\n");
}
#endif

static void _pdu_print(unsigned char PDU[], unsigned long PDU_Len)
{
    unsigned long i; 

    for(i = 0 ; i < PDU_Len ; i++)
    {
        if( (i%16 == 0) && (i != 0) ) 
            UICC_PRINT("\n");

        UICC_PRINT("0x%02X,",PDU[i]);
    }
    UICC_PRINT("\n");
}

static void apdu_init( tAPDU *apdu ) 
{
    apdu->CLS = 0;
    apdu->INS = 0;
    apdu->P1 = 0;
    apdu->P2 = 0;
    apdu->Lc = 0;
    memset(apdu->Data,0,256);
    apdu->Le = 0;
}

static int apdu_copy(  tAPDU *src, tAPDU *target )
{
    target->CLS = src->CLS;
    target->INS = src->INS;
    target->P1 = src->P1;
    target->P2 = src->P2;
    target->Lc = src->Lc;
    memcpy(target->Data,src->Data,256);
    target->Le = src->Le;

    return 0;
}

static int apdu_2_pdu( tAPDU *apdu,    tPDU        *pdu )
{

    unsigned char i = 0;
    pdu->m_Data[i++] = apdu->CLS;                       //0
    pdu->m_Data[i++] = apdu->INS;                       //1
    pdu->m_Data[i++] = apdu->P1;                        //2
    pdu->m_Data[i++] = apdu->P2;                        //3
    
    if(apdu->Lc != 0)
    {
        pdu->m_Data[i++] = apdu->Lc;                    //4
        memcpy(&(pdu->m_Data[i]),apdu->Data,apdu->Lc);  //5
        i += apdu->Lc;
    }

    if(apdu->Le != 0) 
    {
        pdu->m_Data[i++] = apdu->Le;
    }

    pdu->m_Len = i;

    return i;
}

static char uicc_chk_fail(ReturnType  value)
{
	if (SCARD_S_SUCCESS == value)
		return 0;
	else {   
		xprintf(SDK_ERR, "uicc_chk_fail: code 0x%0x (%s)\n",
			(int )value, pcsc_stringify_error(value));
		return 1;
	}
}

static unsigned long eap_aka_uicc_buf_init(tEAP_AKA_UICC_Context *pEAP_AKA_Context ,
                                        unsigned long   buf_len)
{
	memset(pEAP_AKA_Context->R_Buff,0,MAX_BUF_LEN * 5);
	pEAP_AKA_Context->R_Buff_Len = buf_len;

	return 0;
}

void eap_aka_uicc_init_MSK(tEAP_AKA_UICC_Context *pEAP_AKA_Context)
{
	memset(pEAP_AKA_Context->MSK,0,64);
	memset(pEAP_AKA_Context->EMSK,0,64);

	pEAP_AKA_Context->IdentityType = 0;
	pEAP_AKA_Context->IdentityLength = 0;
	memset(pEAP_AKA_Context->Identity,0,256);
	pEAP_AKA_Context->AuthenticationMode = 0;
}

int SCardIsPresent(wm_uicc_t *uicc)
{
	return uicc->uicc_active;
}

void    file_descriptor_print(  tFileDescriptor *FileDescriptor )
{
    UICC_PRINT("\tFile Descriptor (0x82)\n");
    UICC_PRINT("\t\tLength : 0x%02X\n",FileDescriptor->length);
    UICC_PRINT("\t\tFileDescByte : 0x%02X\n",FileDescriptor->FileDescByte);
    UICC_PRINT("\t\tDataCodingByte : 0x%02X\n",FileDescriptor->DataCodingByte);

    if(FileDescriptor->length == 5)
    {   
        UICC_PRINT("\t\tRecordLength : 0x%02X,0x%02X\n",
                                            FileDescriptor->RecordLength[0],
                                            FileDescriptor->RecordLength[1]);
        UICC_PRINT("\t\tNumberRecord : 0x%02X\n",
                                            FileDescriptor->NumberRecord);
    }
}

void    file_id_print(  tFileID *FileID )
{
    UICC_PRINT("\tFile ID (0x83)\n");
    UICC_PRINT("\t\tLength : 0x%02X\n",FileID->length);

    if(FileID->length == 2)
    {
        UICC_PRINT("\t\tFile ID : 0x%02X, 0x%02X\n",
                                            FileID->ID[0],
                                            FileID->ID[1]);
    }
}

void    df_aid_name_print(  tDF_AID_Name    *DF_AID_Name    )
{
    UICC_PRINT("\tDF or AID Name (0x84)\n");
    UICC_PRINT("\t\tLength : 0x%02X\n",DF_AID_Name->length);

    UICC_PRINT("\t\tName :\n\t\t");
    _pdu_print(DF_AID_Name->Name,DF_AID_Name->length);
}

void    proprietry_info_print(  tProprietry_Info    *Proprietry_Info    )
{
    UICC_PRINT("\tProprietry Info (0xA5)\n");
    UICC_PRINT("\t\tLength : 0x%02X\n",Proprietry_Info->length);

    UICC_PRINT("\t\tData : \n\t\t");
    _pdu_print(Proprietry_Info->Data,Proprietry_Info->length);
}

void    life_cycle_status_int_print(    tLife_Cycle_Status_Int  *LifeCycleStatusInt )
{
    UICC_PRINT("\tLife Cycle Status Int (0x8A)\n");
    UICC_PRINT("\t\tLength : 0x%02X\n",LifeCycleStatusInt->length);

    UICC_PRINT("\t\tData : 0x%02X\n",LifeCycleStatusInt->Data);
}

void    security_attr_print(    tSecurity_Attr  *Security_Attr  )
{
    unsigned char Len = 0;
    unsigned char tag = 0;

    UICC_PRINT("\tSecurity Attr. (0x%02X)\n",Security_Attr->tag);
    UICC_PRINT("\t\tLength : 0x%02X\n",Security_Attr->length);

    Len = Security_Attr->length;
    tag = Security_Attr->tag;

    if(tag == 0x8C)
    {
        UICC_PRINT("\t\tAM_Byte : 0x%02X\n",Security_Attr->AM_Byte);
        UICC_PRINT("\t\tSC_Bytes :\n\t\t");
        _pdu_print(Security_Attr->SC_Bytes,Len);
    }
    else if(tag == 0x8B)
    {
        if(Len == 3)
        {
            UICC_PRINT("\t\tEF_ARR_FID : 0x%02X, 0x%02X\n",
                                        Security_Attr->EF_ARR_FID[0],
                                        Security_Attr->EF_ARR_FID[1]);
            UICC_PRINT("\t\tEF_ARR_Record_Number : 0x%02X\n",
                                        Security_Attr->EF_ARR_Record_Num);
        }
        else
        {   // list of (SEID,EF_ARR_Record_Num) ....LATER
            UICC_PRINT("\t\tEF_ARR_FID : 0x%02X, 0x%02X\n",
                                        Security_Attr->EF_ARR_FID[0],
                                        Security_Attr->EF_ARR_FID[1]);

            UICC_PRINT("\t\tSEID : 0x%02X\n",Security_Attr->SEID);
            UICC_PRINT("\t\tEF_ARR_Record_Number : 0x%02X\n",
                                        Security_Attr->EF_ARR_Record_Num);
        }
    }
    else if(tag == 0xAB)
    {   //LATER
        UICC_PRINT("\t\tData : \n\t\t");
        _pdu_print(Security_Attr->Data,Len);
    }
}

void    pin_status_template_print(  tPIN_Status_Template * Pin_Status_DO    )
{
    UICC_PRINT("\tPIN Status DO. (0xC6)\n");
    UICC_PRINT("\t\tLength : 0x%02X\n",Pin_Status_DO->length);
    UICC_PRINT("\t\tData : \n\t\t");
    _pdu_print(Pin_Status_DO->Data,Pin_Status_DO->length);
}

void    file_size_print(    tFile_Size  *File_Size  )
{
    if(File_Size->tag == 0x80)
    {
        UICC_PRINT("\tFile Size (0x80)\n");
    }
    else
    {
        UICC_PRINT("\tTotal File Size (0x81)\n");
    }

    UICC_PRINT("\t\tLength : 0x%02X\n",File_Size->length);
    UICC_PRINT("\t\tSize :\n\t\t");
    _pdu_print(File_Size->Size,File_Size->length);
}

void    sfi_print(  tSFI    *SFI    )
{
    UICC_PRINT("\tSFI (0x88)\n");
    UICC_PRINT("\t\tLength : 0x%02X\n",SFI->length);
    UICC_PRINT("\t\tID : 0x%02X, 0x%02X\n", SFI->ID[0], SFI->ID[1]);
}

void    fcp_print(  tFCP    *FCP    )
{
    UICC_PRINT("\nFCP {");
    UICC_PRINT("Length : %d\n",FCP->length );
    
    if(FCP->FileDescriptor.b_set)
    {
        file_descriptor_print(  &(FCP->FileDescriptor) );
    }

    if(FCP->FileID.b_set)
    {
        file_id_print(  &(FCP->FileID) );
    }
    
    if(FCP->DF_AID_Name.b_set)
    {
        df_aid_name_print(  &(FCP->DF_AID_Name) );
    }

    if(FCP->Proprietry_Info.b_set)
    {
        proprietry_info_print(  &(FCP->Proprietry_Info) );
    }

    if(FCP->Life_Cycle_Status_Int.b_set)
    {
        life_cycle_status_int_print(    &(FCP->Life_Cycle_Status_Int)   );
    }

    if(FCP->Security_Attr.b_set)
    {
        security_attr_print(    &(FCP->Security_Attr)   );
    }

    if(FCP->PIN_Status_Template.b_set)
    {
        pin_status_template_print(  &(FCP->PIN_Status_Template) );
    }

    if(FCP->File_Size.b_set)
    {
        file_size_print(    &(FCP->File_Size)   );
    }

    if(FCP->Total_File_Size.b_set)
    {
        file_size_print(    &(FCP->Total_File_Size) );
    }

    if(FCP->SFI.b_set)
    {
        sfi_print(  &(FCP->SFI) );
    }

    UICC_PRINT("}\n");
}

void app_id_print(  tApplication_ID     *app_id )
{
    UICC_PRINT("\tApplication ID (0x4F)\n");
    UICC_PRINT("\t\tLength : 0x%02X\n",app_id->length);

    UICC_PRINT("\t\tID : \n\t\t");

    _pdu_print(app_id->ID,app_id->length);
}

void app_label_print(   tApplication_Label  *app_label)
{
    int i;
    UICC_PRINT("\tApplication Label (0x50)\n");
    UICC_PRINT("\t\tLength : 0x%02X\n",app_label->length);

    UICC_PRINT("\t\tLabel : \n\t\t");

    for(i = 0 ; i < app_label->length; i++)
        UICC_PRINT("%c",app_label->Value[i]);

    UICC_PRINT("\n");
}

void discretionary_print(   tDiscretionary  *discretionary  )
{
    UICC_PRINT("\tDiscretionary (0x73)\n");
    UICC_PRINT("\t\tLength : 0x%02X\n",discretionary->length);

    UICC_PRINT("\t\tData : \n\t\t");

    _pdu_print(discretionary->Data,discretionary->length);
}

void application_template_print(    tApplication_Template *app  ) 
{
    UICC_PRINT("\nApplication Template {\n");
    UICC_PRINT("\tLength : %d\n",app->length );
    
    if(app->App_ID.b_set)
    {
        app_id_print(&(app->App_ID));
    }

    if(app->App_Label.b_set)
    {
        app_label_print(&(app->App_Label));
    }

    if(app->Discretionary.b_set)
    {
        discretionary_print(&(app->Discretionary));
    }

    UICC_PRINT("}\n");
}

void fcp_init(  tFCP    *FCP)
{
    FCP->length = 0;

    // FileDescriptor.
    FCP->FileDescriptor.b_set = 0;
    FCP->FileDescriptor.length = 0;
    FCP->FileDescriptor.FileDescByte = 0;
    FCP->FileDescriptor.DataCodingByte = 0;
    FCP->FileDescriptor.RecordLength[0] = 0;
    FCP->FileDescriptor.RecordLength[1] = 0;
    FCP->FileDescriptor.NumberRecord = 0;

    // FileID.
    FCP->FileID.b_set = 0;
    FCP->FileID.length = 0;
    FCP->FileID.ID[0] = 0;
    FCP->FileID.ID[1] = 0;

    // DF_AID_Name.
    FCP->DF_AID_Name.b_set = 0;
    FCP->DF_AID_Name.length = 0;
    memset(FCP->DF_AID_Name.Name,0,16);

    // Proprietry_Info
    FCP->Proprietry_Info.b_set = 0;
    FCP->Proprietry_Info.length = 0;
    memset(FCP->Proprietry_Info.Data,0,256);

    // Life_Cycle_Status_Int
    FCP->Life_Cycle_Status_Int.b_set = 0;
    FCP->Life_Cycle_Status_Int.length = 0;
    FCP->Life_Cycle_Status_Int.Data = 0;

    // Security_Attr
    FCP->Security_Attr.b_set = 0;
    FCP->Security_Attr.tag = 0;
    FCP->Security_Attr.length = 0;
    FCP->Security_Attr.AM_Byte = 0;
    memset(FCP->Security_Attr.SC_Bytes,0,256);
    FCP->Security_Attr.EF_ARR_FID[0] = 0;
    FCP->Security_Attr.EF_ARR_FID[1] = 0;
    FCP->Security_Attr.SEID = 0;
    FCP->Security_Attr.EF_ARR_Record_Num = 0;
    memset(FCP->Security_Attr.Data,0,256);

    // PIN_Status_Template.
    FCP->PIN_Status_Template.b_set = 0;
    FCP->PIN_Status_Template.length = 0;
    memset(FCP->PIN_Status_Template.Data,0,256);

    // File Size
    FCP->File_Size.b_set = 0;
    FCP->File_Size.tag = 0;
    FCP->File_Size.length = 0;
    memset(FCP->File_Size.Size,0,256);

    // Total File Size
    FCP->Total_File_Size.b_set = 0;
    FCP->Total_File_Size.tag = 0;
    FCP->Total_File_Size.length = 0;
    memset(FCP->Total_File_Size.Size,0,256);

    // SFI
    FCP->SFI.b_set = 0;
    FCP->SFI.length = 0;
    memset(FCP->SFI.ID,0,2);
}

void application_template_init( tApplication_Template   *application_temp)
{
    application_temp->length = 0;

    application_temp->App_ID.b_set = 0;
    application_temp->App_ID.length = 0;
    memset(application_temp->App_ID.ID,0,16);

    application_temp->App_Label.b_set = 0;
    application_temp->App_Label.length = 0;
    memset(application_temp->App_Label.Value,0,80);

    application_temp->Discretionary.b_set = 0;
    application_temp->Discretionary.length = 0;
    memset(application_temp->Discretionary.Data,0,256);
}

int app_id_parsing( unsigned char   PDU[],                  //[IN]
                    unsigned char   PDU_Len,                //[IN]
                    tApplication_ID *App_ID)                //[OUT]
{
    App_ID->b_set = 1;
    App_ID->length = PDU_Len;
    memcpy(App_ID->ID,PDU,PDU_Len);

    return PDU_Len;
}

int app_label_parsing(  unsigned char       PDU[],                  //[IN]
                        unsigned char       PDU_Len,                //[IN]
                        tApplication_Label  *App_Label)             //[OUT]
{
    App_Label->b_set = 1;
    App_Label->length = PDU_Len;
    memcpy(App_Label->Value,PDU,PDU_Len);

    return PDU_Len;
}

int discretionary_parsing(  unsigned char   PDU[],                  //[IN]
                            unsigned char   PDU_Len,                //[IN]
                            tDiscretionary  *Discretionary)         //[OUT]
{
    Discretionary->b_set = 1;
    Discretionary->length = PDU_Len;
    memcpy(Discretionary->Data,PDU,PDU_Len);

    return PDU_Len;
}

int  application_template_parsing(  unsigned char   PDU[],
                                    unsigned short  PDU_Len,
                                    tApplication_Template   *application_temp)
{
    unsigned short  i;
    unsigned char   Len = 0;
    unsigned char   tag = 0;

    if(PDU[0] != 0x61)  // Application Template tag.
        return -1;

    if(PDU_Len <= 2)
        return -2;

    if(PDU[1] >= PDU_Len)
        return -3;

    application_temp->length = PDU[1];

    for( i = 2 ; i < PDU_Len ; )
    {
        switch(tag = PDU[i])
        {
            case APPL_ID:
            {
#if UICC_DEBUG_LEVEL >= 3
                UICC_PRINT("APPL_ID\n");
#endif //UICC_DEBUG_LEVEL

                i++;

                Len = PDU[i++];

                if(Len >= application_temp->length)
                    return -3;

#if UICC_DEBUG_LEVEL >= 3
                UICC_PRINT("Len : %d\n",Len);
#endif //UICC_DEBUG_LEVEL

                app_id_parsing( PDU+i,
                                Len,
                                &(application_temp->App_ID));

                i += Len;

                break;
            }
            case APPL_LABEL:
            {
#if UICC_DEBUG_LEVEL >= 3
                UICC_PRINT("APPL_LABEL\n");
#endif //UICC_DEBUG_LEVEL

                i++;

                Len = PDU[i++];

                if(Len >= application_temp->length)
                    return -3;

#if UICC_DEBUG_LEVEL >= 3
                UICC_PRINT("Len : %d\n",Len);
#endif //UICC_DEBUG_LEVEL

                app_label_parsing(  PDU+i,
                                    Len,
                                    &(application_temp->App_Label));

                i += Len;

                break;
            }
            case DISCRETIONARY:
            {
#if UICC_DEBUG_LEVEL >= 3
                UICC_PRINT("DISCRETIONARY\n");
#endif //UICC_DEBUG_LEVEL

                i++;

                Len = PDU[i++];

                if(Len >= application_temp->length)
                    return -3;

#if UICC_DEBUG_LEVEL >= 3
                UICC_PRINT("Len : %d\n",Len);
#endif //UICC_DEBUG_LEVEL

                discretionary_parsing(  PDU+i,
                                        Len,
                                        &(application_temp->Discretionary));

                i += Len;

                break;
            }
            case 0xFF:      // No data.
            {
#if UICC_DEBUG_LEVEL >= 3
                UICC_PRINT("NO DATA : 0xFF\n");
#endif //UICC_DEBUG_LEVEL
                i++;
                break;
            }
            default:
            {
                UICC_PRINT("Unknown Tag 0x%02X in Application Tag\n",PDU[i]);

                i++;

                Len = PDU[i++];

                if(Len >= application_temp->length)
                    return -3;

                UICC_PRINT("Len : %d\n",Len);

                _pdu_print(PDU+i,Len);

                i += Len;

                break;
            }
        }
    }

    return i;
}

int file_descriptor_parsing(    unsigned char   PDU[],      //[IN]
                                unsigned char   PDU_Len,    // [IN]
                                tFileDescriptor *FileDescriptor)    //[OUT]
{
    FileDescriptor->b_set = 1;
    FileDescriptor->length = (unsigned char)PDU_Len;
    FileDescriptor->FileDescByte = PDU[0];
    FileDescriptor->DataCodingByte = PDU[1];

    if(PDU_Len == 5)
    {   
        FileDescriptor->RecordLength[0] = PDU[2];
        FileDescriptor->RecordLength[1] = PDU[3];
        FileDescriptor->NumberRecord = PDU[4];
    }

    return PDU_Len;
}

int file_id_parsing(    unsigned char   PDU[],      // [IN]
                        unsigned char   PDU_Len,    // [IN]
                        tFileID         *FileID)    // [OUT]
{
    FileID->b_set = 1;
    FileID->length = PDU_Len;

    if(PDU_Len == 2)
    {   
        FileID->ID[0] = PDU[0];
        FileID->ID[1] = PDU[1];
    }

    return PDU_Len;
}

int df_aid_name_parsing(    unsigned char   PDU[],          //[IN]
                            unsigned char   PDU_Len,        //[IN]
                            tDF_AID_Name    *DF_AID_Name)   //[OUT]
{
    DF_AID_Name->b_set = 1;
    DF_AID_Name->length = PDU_Len;

    if(PDU_Len <= 16)
        memcpy(DF_AID_Name->Name,PDU,PDU_Len);

    return PDU_Len;
}

int proprietry_info_parsing(    unsigned char   PDU[],                  //[IN]
                                unsigned char   PDU_Len,                //[IN]
                                tProprietry_Info    *Proprietry_Info)   //[OUT]
{
    Proprietry_Info->b_set = 1;
    Proprietry_Info->length = PDU_Len;
    memcpy(Proprietry_Info->Data,PDU,PDU_Len);

    return PDU_Len;
}

int life_cycle_status_int_parsing(  unsigned char   PDU[],                          //[IN]
                                    unsigned char   PDU_Len,                        //[IN]
                                    tLife_Cycle_Status_Int  *Life_Cycle_Status_Int) //[OUT]
{
    Life_Cycle_Status_Int->b_set = 1;
    Life_Cycle_Status_Int->length = PDU_Len;

    if(PDU_Len == 1)
        Life_Cycle_Status_Int->Data = PDU[0];

    return PDU_Len;
}

int security_attr_parsing(  unsigned char   tag,                //[IN]
                            unsigned char   PDU[],              //[IN]
                            unsigned char   PDU_Len,            //[IN]
                            tSecurity_Attr  *Security_Attr)     //[OUT]
{
    Security_Attr->b_set = 1;
    Security_Attr->length = PDU_Len;
    Security_Attr->tag = tag;

    if(tag == 0x8C)
    {
        Security_Attr->AM_Byte = PDU[0];
        memcpy(Security_Attr->SC_Bytes,&PDU[1],PDU_Len-1);
    }
    else if(tag == 0x8B)
    {
        if(PDU_Len == 3)
        {
            memcpy(Security_Attr->EF_ARR_FID,PDU,2);
            Security_Attr->EF_ARR_Record_Num = PDU[2];
        }
        else
        {   // list of (SEID,EF_ARR_Record_Num) ....LATER
            memcpy(Security_Attr->EF_ARR_FID,PDU,2);
            Security_Attr->SEID = PDU[2];
            Security_Attr->EF_ARR_Record_Num = PDU[3];
        }
    }
    else if(tag == 0xAB)
    {   //LATER
        memcpy(Security_Attr->Data,PDU,PDU_Len);
    }

    return PDU_Len;
}

int pin_status_do_parsing(  unsigned char           PDU[],          //[IN]
                            unsigned char           PDU_Len,        //[IN]
                            tPIN_Status_Template    *Pin_Status_DO) //[OUT]
{
    Pin_Status_DO->b_set = 1;
    Pin_Status_DO->length = PDU_Len;
    memcpy(Pin_Status_DO->Data,PDU,PDU_Len);

    return PDU_Len;
}

int file_size_parsing(  unsigned char       tag,        //[IN]
                        unsigned char       PDU[],      //[IN]
                        unsigned char       PDU_Len,    //[IN]
                        tFile_Size          *File_Size) //[OUT]
{
    File_Size->b_set = 1;
    File_Size->tag = tag;
    File_Size->length = PDU_Len;
    memcpy(File_Size->Size,PDU,PDU_Len);

    return PDU_Len;
}

int sfi_parsing(    unsigned char   PDU[],      // [IN]
                    unsigned char   PDU_Len,    // [IN]
                    tSFI            *SFI)       // [OUT]
{
    SFI->b_set = 1;
    SFI->length = PDU_Len;
    memcpy(SFI->ID,PDU,PDU_Len);

    return PDU_Len;
}

int fcp_parsing(    unsigned char   PDU[],      // [IN]
                    unsigned long   PDU_Len,    // [IN]
                    tFCP            *FCP )      // [OUT]
{
    unsigned long   i;
    unsigned char   Len = 0;
    unsigned char   tag = 0;

    if(PDU[0] != 0x62)  // FCP tag.
        return -1;

    if(PDU_Len <= 2)
        return -2;

    if(PDU[1] >= PDU_Len)
        return -3;

    FCP->length = PDU[1];

    for( i = 2 ; i < PDU_Len ; )
    {   
        switch(tag = PDU[i])
        {   
            case FILE_DESC:
            {
#if UICC_DEBUG_LEVEL >= 3
                UICC_PRINT("FILE_DESC\n");
#endif //UICC_DEBUG_LEVEL

                i++;
                Len = PDU[i++];
                if(Len >= FCP->length)
                    return -3;

#if UICC_DEBUG_LEVEL >= 3
                UICC_PRINT("Len : %d\n",Len);
#endif  //UICC_DEBUG_LEVEL

                file_descriptor_parsing(PDU+i,
                                        Len,
                                        &(FCP->FileDescriptor));
                i += Len;
                break;
            }
            case FILE_ID:
            {
#if UICC_DEBUG_LEVEL >= 3
                UICC_PRINT("FILE_ID\n");
#endif //UICC_DEBUG_LEVEL

                i++;
                Len = PDU[i++];
                if(Len >= FCP->length)
                    return -3;

#if UICC_DEBUG_LEVEL >= 3
                UICC_PRINT("Len : %d\n",Len);
#endif //UICC_DEBUG_LEVEL

                file_id_parsing(PDU+i,
                                Len,
                                &(FCP->FileID));
                i += Len;
                break;
            }
            case DF_AID_NAME:
            {
#if UICC_DEBUG_LEVEL >= 3
                UICC_PRINT("DF_AID_NAME\n");
#endif //UICC_DEBUG_LEVEL

                i++;
                Len = PDU[i++];
                if(Len >= FCP->length)
                    return -3;

#if UICC_DEBUG_LEVEL >= 3
                UICC_PRINT("Len : %d\n",Len);
#endif //UICC_DEBUG_LEVEL

                df_aid_name_parsing(    PDU+i,
                                        Len,
                                        &(FCP->DF_AID_Name));
                i += Len;
                break;
            }
            case PROPRIETRY_INFO:
            {

#if UICC_DEBUG_LEVEL >= 3
                UICC_PRINT("PROPRIETRY_INFO\n");
#endif //UICC_DEBUG_LEVEL

                i++;
                Len = PDU[i++];
                if(Len >= FCP->length)
                    return -3;

#if UICC_DEBUG_LEVEL >= 3
                UICC_PRINT("Len : %d\n",Len);
#endif //UICC_DEBUG_LEVEL

                proprietry_info_parsing(PDU+i,
                                        Len,
                                        &(FCP->Proprietry_Info));
                i += Len;
                break;
            }
            case LIFE_CYCLE_STATUS_INT:
            {
#if UICC_DEBUG_LEVEL >= 3
                UICC_PRINT("LIFE_CYCLE_STATUS_INT\n");
#endif //UICC_DEBUG_LEVEL

                i++;
                Len = PDU[i++];
                if(Len >= FCP->length)
                    return -3;

#if UICC_DEBUG_LEVEL >= 3
                UICC_PRINT("Len : %d\n",Len);
#endif //UICC_DEBUG_LEVEL

                life_cycle_status_int_parsing(  PDU+i,
                                                Len,
                                                &(FCP->Life_Cycle_Status_Int));
                i += Len;
                break;
            }
            case SECURITY_ATTR_8C:
            case SECURITY_ATTR_8B:
            case SECURITY_ATTR_AB:
            {
#if UICC_DEBUG_LEVEL >= 3
                UICC_PRINT("SECURITY_ATTR\n");
#endif //UICC_DEBUG_LEVEL

                i++;
                Len = PDU[i++];
                if(Len >= FCP->length)
                    return -3;

#if UICC_DEBUG_LEVEL >= 3
                UICC_PRINT("Len : %d\n",Len);
#endif //UICC_DEBUG_LEVEL

                security_attr_parsing(  tag,
                                        PDU+i,
                                        Len,
                                        &(FCP->Security_Attr));
                i += Len;
                break;
            }
            case PIN_STATUS_DO:
            {
#if UICC_DEBUG_LEVEL >= 3
                UICC_PRINT("PIN_STATUS_DO\n");
#endif //UICC_DEBUG_LEVEL

                i++;
                Len = PDU[i++];
                if(Len >= FCP->length)
                    return -3;

#if UICC_DEBUG_LEVEL >= 3
                UICC_PRINT("Len : %d\n",Len);
#endif //UICC_DEBUG_LEVEL

                pin_status_do_parsing(PDU+i,
                                        Len,
                                        &(FCP->PIN_Status_Template));
                i += Len;
                break;
            }
            case FILE_SIZE:
            {
#if UICC_DEBUG_LEVEL >= 3
                UICC_PRINT("FILE_SIZE\n");
#endif//UICC_DEBUG_LEVEL

                i++;
                Len = PDU[i++];
                if(Len >= FCP->length)
                    return -3;

#if UICC_DEBUG_LEVEL >= 3
                UICC_PRINT("Len : %d\n",Len);
#endif //UICC_DEBUG_LEVEL

                file_size_parsing(  tag,
                                    PDU+i,
                                    Len,
                                    &(FCP->File_Size));
                i += Len;
                break;
            }
        case TOTAL_FILE_SIZE:
            {
#if UICC_DEBUG_LEVEL >= 3
                UICC_PRINT("TOTAL_FILE_SIZE\n");
#endif //UICC_DEBUG_LEVEL

                i++;
                Len = PDU[i++];
                if(Len >= FCP->length)
                    return -3;

#if UICC_DEBUG_LEVEL >= 3
                UICC_PRINT("Len : %d\n",Len);
#endif //UICC_DEBUG_LEVEL

                file_size_parsing(  tag,
                                    PDU+i,
                                    Len,
                                    &(FCP->Total_File_Size));
                i += Len;
                break;
            }
        case SHORT_FILE_ID:
            {
#if UICC_DEBUG_LEVEL >= 3
                UICC_PRINT("SFI");
#endif //UICC_DEBUG_LEVEL

                i++;
                Len = PDU[i++];
                if(Len >= FCP->length)
                    return -3;

#if UICC_DEBUG_LEVEL >= 3
                UICC_PRINT("Len : %d\n",Len);
#endif //UICC_DEBUG_LEVEL

                sfi_parsing(PDU+i,
                            Len,
                            &(FCP->SFI));
                i += Len;
                break;
            }
            default:
            {
                UICC_PRINT("Unknown Tag 0x%02X in FCP\n",PDU[i]);

                i++;
                Len = PDU[i++];
                if(Len >= FCP->length)
                    return -3;
                UICC_PRINT("Len : %d\n",Len);
                _pdu_print(PDU+i,Len);
                i += Len;
                break;
            }

        }
    }

    return i;
}


void uicc_print_error(ReturnType    error_code)
{
	if (error_code == SCARD_S_SUCCESS)
		return;

	xprintf(SDK_ERR, "Scard error: %s(0x%08X)\n", pcsc_stringify_error(error_code), (int) error_code);
	return;
}

ReturnType uicc_init_ctxt(tUICC_Context *pUICC_Context)
{
    ReturnType        lResult = 0;

    pUICC_Context->m_hScardHandle = 0; 

    apdu_init(&(pUICC_Context->C_APDU));
    apdu_init(&(pUICC_Context->R_APDU));

    pdu_init(&(pUICC_Context->PDU),0);

    pUICC_Context->SW1 = 0;
    pUICC_Context->SW2 = 0;

    if (!m_hScardContext) {
    	lResult = SCardEstablishContext(SCARD_SCOPE_SYSTEM,
                                        NULL,   
                                        NULL,   
                                        &m_hScardContext);

    	if(SCARD_S_SUCCESS != lResult)
    	{
      		xprintf(SDK_ERR, "ERROR (SCardEstablishContext) : %u",lResult);
        	//uicc_print_error(lResult);
    	}
    }

    return lResult;
}

ReturnType uicc_uninit_ctxt(void)
{
	ReturnType	lResult = 0;

	xprintf(SDK_INFO, "uicc_uninit_ctxt: Context %p\n", m_hScardContext);
	if(m_hScardContext) {
		lResult = SCardReleaseContext(m_hScardContext);
	}

	if(SCARD_S_SUCCESS != lResult)
	{	
		xprintf(SDK_ERR, "Uninit: SCardReleaseContext : %u\n",lResult);
		uicc_print_error(lResult);
	}
	m_hScardContext = 0;

	return lResult;
}

ReturnType uicc_leave_card(tUICC_Context *pUICC_Context)
{
	ReturnType 	lResult = 0;

	lResult = SCardDisconnect(pUICC_Context->m_hScardHandle, SCARD_LEAVE_CARD);

	if (SCARD_S_SUCCESS == lResult) {
		xprintf(SDK_INFO, "Leave: SCardDisconnect OK\n");
		pUICC_Context->m_hScardHandle = 0;
		return 0;
	}

	xprintf(SDK_ERR, "ERROR (SCardDisconnect_leave) : %u",lResult);
	uicc_print_error(lResult);

	return lResult;
}

ReturnType uicc_reset_card(tUICC_Context *pUICC_Context)
{
	ReturnType 	lResult;

	lResult = SCardDisconnect(pUICC_Context->m_hScardHandle, SCARD_RESET_CARD);

	if (SCARD_S_SUCCESS == lResult) {
		xprintf(SDK_INFO, "Reset: SCardDisconnect OK\n");
		pUICC_Context->m_hScardHandle = 0;
		return 0;
	}

	xprintf(SDK_ERR, "ERROR (SCardDisconnect_reset) : %u",lResult);
	uicc_print_error(lResult);

	return lResult;
}

ReturnType	uicc_unpower_card(tUICC_Context *pUICC_Context)
{
	ReturnType 	lResult;

	lResult = SCardDisconnect(pUICC_Context->m_hScardHandle, SCARD_UNPOWER_CARD);

	if (SCARD_S_SUCCESS == lResult) {
		xprintf(SDK_INFO, "Unpower: SCardDisconnect OK\n");
		pUICC_Context->m_hScardHandle = 0;
		return 0;
	}

	xprintf(SDK_ERR, "ERROR (SCardDisconnect_unpower) : %u",lResult);
	uicc_print_error(lResult);

	return lResult;
}

ReturnType	uicc_eject_card(tUICC_Context *pUICC_Context)
{
	ReturnType 	lResult;

	lResult = SCardDisconnect(pUICC_Context->m_hScardHandle, SCARD_EJECT_CARD);

	if (SCARD_S_SUCCESS == lResult) {
		xprintf(SDK_INFO, "Unpower: SCardDisconnect OK\n");
		pUICC_Context->m_hScardHandle = 0;
		return 0;
	}

	xprintf(SDK_ERR, "ERROR (SCardDisconnect_eject) : %u",lResult);
	uicc_print_error(lResult);

	return lResult;
}

static int lptzReaderName_arr[256]; 
static int lpbyATR_arr[256]; 

ReturnType  uicc_connect(   tUICC_Context *pUICC_Context, int index)
{
    ReturnType  lResult = 0;
    char *lptzReaderList = NULL; 
    char *lptzReaderName = (char *)lptzReaderName_arr; 
    char *lpbyATR = (char *)lpbyATR_arr; 
    int  i;
    DWORD dwActiveProtocol = 0;
    DWORD dwStatus;
    DWORD dwListLength;
    DWORD dwATRLength = 256;
	int readerIndex;

    lResult = SCardListReaders(m_hScardContext,
                                NULL,
                                NULL,
                                &dwListLength);
    if(SCARD_S_SUCCESS != lResult)
    {
        xprintf(SDK_ERR, "ERROR (SCardListReaders first) : %u, %u\n",
			m_hScardContext, lResult);

        uicc_print_error(lResult);

        goto release_memory;
    }

    lptzReaderList = sdk_malloc(sizeof(char) * dwListLength);
    lResult = SCardListReaders( m_hScardContext,
                                NULL,
                                lptzReaderList,
                                &dwListLength);

    if(SCARD_S_SUCCESS != lResult) {
        xprintf(SDK_ERR, "ERROR (SCardListReaders second) : %u, %u\n",
			m_hScardContext, lResult);
        uicc_print_error(lResult);
        goto release_memory;
    }
						
#if 0
    xprintf(SDK_INFO, "SCardListReaders :");
    for (i = 0; i < dwListLength - 1; i++)
        xprintf(SDK_INFO, "%c", lptzReaderList[i]);
    xprintf(SDK_INFO, "\n");
#endif

	if (lptzReaderList[0] == '\0'){
		xprintf(SDK_ERR, "uicc_connect: Reader not found. Please re-attach SDK or restart pcscd\n");
		goto release_memory;
	}

    for (i = 0; i < dwListLength ; i++) {
		lptzReaderName = &lptzReaderList[i];
		/*
		 * - GCT USB dongle supports only one slot
		 * 	- Reader Name Format: "X Reader 01 00"
		 * 			01: reader index
		 * 			00: slot index in the reader
		 */
		int len = strlen (lptzReaderName)-4;
		char tmp[3];
		tmp[1] = lptzReaderName[len--];
		tmp[0] = lptzReaderName[len--];
		tmp[2] = '\0';
		readerIndex = strtoul(tmp, NULL, 16);
		if (index == readerIndex) {
			break;
		}
		while (lptzReaderList[++i] != 0) ;
    }

	if (i==dwListLength){	/* no matching reader (end of lptzReaderList met) */
		xprintf(SDK_ERR, "uicc_connect: Reader not found\n");
		goto release_memory;
	}

    if(0 != pUICC_Context->m_hScardHandle)
    {
        UICC_PRINT("Connected Handle (Disconnecting)\n");

        dwListLength = 256;

        lResult = SCardStatus(pUICC_Context->m_hScardHandle,
                                lptzReaderName,
                                &dwListLength,
                                &dwStatus,
                                &dwActiveProtocol,
                                (u8 *)&lpbyATR,
                                &dwATRLength);

        UICC_PRINT("SCardStatus [%d/%d]\n", dwStatus, lResult);
        if(SCARD_S_SUCCESS == lResult)
        {
            lResult = SCardDisconnect(  pUICC_Context->m_hScardHandle,
                                        SCARD_LEAVE_CARD);

            if(SCARD_S_SUCCESS == lResult)
            {
                //SCardFreeMemory(pUICC_Context->m_hScardContext,lptzReaderName);
                //SCardFreeMemory(pUICC_Context->m_hScardContext,lpbyATR);
                pUICC_Context->m_hScardHandle = 0;
            }
            else
            {
                pUICC_Context->m_hScardHandle = 0;
                goto release_memory;
            }
        }
        else
        {
            pUICC_Context->m_hScardHandle = 0;
            goto release_memory;
        }
    }


    lResult = SCardConnect( m_hScardContext,
                            lptzReaderName,
                            //SCARD_SHARE_EXCLUSIVE,
                            SCARD_SHARE_SHARED, // astrocosmos
                            SCARD_PROTOCOL_T0,
                            &(pUICC_Context->m_hScardHandle),
                            &dwActiveProtocol);

	if(SCARD_S_SUCCESS == lResult)
		UICC_PRINT("SCardConnect: success\n");
	else
		xprintf(SDK_ERR, "SCardConnect failed [%x][%s]\n", lResult, lptzReaderName);

    if(0 == pUICC_Context->m_hScardHandle)
        xprintf(SDK_ERR, "Error : Failed to connect card\n");
    else
        UICC_PRINT("Success connecting card\n");

release_memory:
    sdk_free (lptzReaderList);
    if(0 != lptzReaderList)
    {
        //SCardFreeMemory(pUICC_Context->m_hScardContext,lptzReaderList);
    }

    return lResult;
}

ReturnType eap_aka_uicc_open(wm_uicc_t *uicc)
{
	tEAP_AKA_UICC_Context *ctxt = uicc->uicc_ctxt;
	ReturnType lResult = 0;

	lResult = uicc_init_ctxt(&(ctxt->UICC_Context));
	xprintf(SDK_DBG, "uicc_init_ctxt: lResult 0x%x\n", (int) lResult);

	if (uicc_chk_fail(lResult))
		return lResult;

	lResult = uicc_connect(&(ctxt->UICC_Context), uicc->reader_idx);
	xprintf(SDK_INFO, "uicc_connect: lResult 0x%x (%s)\n",
		(int) lResult, pcsc_stringify_error(lResult));

	return lResult;
}

ReturnType uicc_select( tUICC_Context   *pUICC_Context,
                        unsigned char   type,       // [IN] 0x00 (FileID), 0x04 (AID)
                        unsigned char   id_len,     // [IN] 
                        unsigned char   id[],       // [IN] 
                        unsigned char   R_APDU[],
                        unsigned long   *R_APDU_Len)
{
    apdu_init(&(pUICC_Context->C_APDU));

    (pUICC_Context->C_APDU).CLS = 0x00; 
    (pUICC_Context->C_APDU).INS = SELECT; 
    (pUICC_Context->C_APDU).P1 = type; 
    (pUICC_Context->C_APDU).P2  = 0x04;                 // get FCP 
    (pUICC_Context->C_APDU).Lc = id_len; 
    memcpy((pUICC_Context->C_APDU).Data, id, id_len);

    return uicc_send_APDU_T0(   pUICC_Context,
                                &(pUICC_Context->C_APDU),
                                R_APDU, 
                                R_APDU_Len);
}

ReturnType uicc_read_record(    tUICC_Context   *pUICC_Context,
                                unsigned char   index,      // [IN]
                                unsigned char   mode,       // [IN] read : 0x04
                                unsigned char   Le,         // [IN] expected length for response
                                unsigned char   R_APDU[],
                                unsigned long   *R_APDU_Len)
{
    apdu_init(&(pUICC_Context->C_APDU));

    (pUICC_Context->C_APDU).CLS = 0x00;
    (pUICC_Context->C_APDU).INS = READ_RECORD;
    (pUICC_Context->C_APDU).P1 = index;
    (pUICC_Context->C_APDU).P2 = mode;
    (pUICC_Context->C_APDU).Le = Le;

    return uicc_send_APDU_T0(   pUICC_Context,
                                &(pUICC_Context->C_APDU),
                                R_APDU,
                                R_APDU_Len);
}

ReturnType uicc_verify_pin( tUICC_Context   *pUICC_Context,
                            unsigned char   index,
                            unsigned char   Lc,
                            unsigned char   PIN[],
                            unsigned char   R_APDU[],
                            unsigned long   *R_APDU_Len)
{
    apdu_init(&(pUICC_Context->C_APDU));

    (pUICC_Context->C_APDU).CLS = 0x00;
    (pUICC_Context->C_APDU).INS = VERIFY_CHV;
    (pUICC_Context->C_APDU).P1 = 0x00;
    (pUICC_Context->C_APDU).P2 = index;
    (pUICC_Context->C_APDU).Lc = Lc;
    memcpy((pUICC_Context->C_APDU).Data,PIN,Lc);

    return uicc_send_APDU_T0(   pUICC_Context,
                                &(pUICC_Context->C_APDU),
                                R_APDU,
                                R_APDU_Len);
}

ReturnType uicc_read_binary(    tUICC_Context   *pUICC_Context,
                                unsigned char   Le,
                                unsigned char   R_APDU[],
                                unsigned long   *R_APDU_Len)
{
    apdu_init(&(pUICC_Context->C_APDU));

    (pUICC_Context->C_APDU).CLS = 0x00;
    (pUICC_Context->C_APDU).INS = READ_BINARY;
    (pUICC_Context->C_APDU).P1 = 0x00;
    (pUICC_Context->C_APDU).P2 = 0x00;
    (pUICC_Context->C_APDU).Le = Le;

    return uicc_send_APDU_T0(   pUICC_Context,
                                &(pUICC_Context->C_APDU),
                                R_APDU,
                                R_APDU_Len);
}

ReturnType eap_aka_uicc_prepare_authenticate(   tEAP_AKA_UICC_Context *pEAP_AKA_Context)
{
    unsigned char   SW1 = 0;
    unsigned char   SW2 = 0;
    unsigned long   Len = 0;
    unsigned char   i = 0;  
    unsigned char   b_WSIM_Record_Find = 0;
    ReturnType      lResult = 0;

    eap_aka_uicc_buf_init(  pEAP_AKA_Context,   MAX_BUF_LEN * 5 );

    lResult = uicc_select(  &(pEAP_AKA_Context->UICC_Context),
                            0x00,   
                            0x02,   
                            EF_DIR, 
                            pEAP_AKA_Context->R_Buff,
                            &(pEAP_AKA_Context->R_Buff_Len));

	#if (UICC_DEBUG_LEVEL == 4)
	apdu_print (&(pEAP_AKA_Context->UICC_Context.C_APDU));
	#endif

    if(uicc_chk_fail(lResult))
        return lResult;

#if UICC_DEBUG_LEVEL == 4
    _pdu_print(pEAP_AKA_Context->R_Buff, pEAP_AKA_Context->R_Buff_Len );
#endif // UICC_DEBUG_LEVEL

    Len = uicc_get_SW(  pEAP_AKA_Context->R_Buff,
                        pEAP_AKA_Context->R_Buff_Len,
                        &SW1,&SW2);

    if(uicc_desc_SW(SW1,SW2))
        return 1;

    fcp_init(&(pEAP_AKA_Context->fcp));
    fcp_parsing(pEAP_AKA_Context->R_Buff,
                Len,
                &(pEAP_AKA_Context->fcp));

#if UICC_DEBUG_LEVEL >= 4
    fcp_print(&(pEAP_AKA_Context->fcp));
#endif //UICC_DEBUG_LEVEL

    (pEAP_AKA_Context->UICC_Context).gVendor = unknown;

    for(i = 1 ; i <= (pEAP_AKA_Context->fcp).FileDescriptor.NumberRecord ; i++)
    {
        eap_aka_uicc_buf_init(  pEAP_AKA_Context,   MAX_BUF_LEN * 5 );

        uicc_read_record(   &(pEAP_AKA_Context->UICC_Context),
                            i,
                            0x04,
                            (pEAP_AKA_Context->fcp).FileDescriptor.RecordLength[1], //0x2F
                            pEAP_AKA_Context->R_Buff,
                            &(pEAP_AKA_Context->R_Buff_Len));

#if UICC_DEBUG_LEVEL == 4
        _pdu_print(pEAP_AKA_Context->R_Buff, pEAP_AKA_Context->R_Buff_Len );
#endif // UICC_DEBUG_LEVEL

        Len = uicc_get_SW(  pEAP_AKA_Context->R_Buff,
                            pEAP_AKA_Context->R_Buff_Len,
                            &SW1,&SW2);

        if(uicc_desc_SW(SW1,SW2))
            continue;
        else
        {


            application_template_init(&(pEAP_AKA_Context->app));

            application_template_parsing(   pEAP_AKA_Context->R_Buff,
                                            Len,
                                            &(pEAP_AKA_Context->app));

#if UICC_DEBUG_LEVEL >= 3
            application_template_print(&(pEAP_AKA_Context->app));
#endif //UICC_DEBUG_LEVEL

            if(memcmp(  (pEAP_AKA_Context->app).App_Label.Value,
                        "WSIM",
                        (pEAP_AKA_Context->app).App_Label.length) == 0)
            {
                b_WSIM_Record_Find = 1;

                (pEAP_AKA_Context->UICC_Context).gVendor = kt_uicc;      // added by clark 2007.08.07.

                break;
            }
            else if(memcmp( (pEAP_AKA_Context->app).App_Label.Value,
                        "SKTelecom PISIM",
                        (pEAP_AKA_Context->app).App_Label.length) == 0)
            {
                b_WSIM_Record_Find = 1;

                (pEAP_AKA_Context->UICC_Context).gVendor = sk_telecom_uicc;      // added by clark 2007.08.07.

                break;
            }

        }
    }

    if( b_WSIM_Record_Find ) //success
    {
        eap_aka_uicc_buf_init(  pEAP_AKA_Context,   MAX_BUF_LEN * 5 );

        lResult = uicc_select(  &(pEAP_AKA_Context->UICC_Context),
                                0x04,
                                (pEAP_AKA_Context->app).App_ID.length,
                                (pEAP_AKA_Context->app).App_ID.ID,
                                pEAP_AKA_Context->R_Buff,
                                &(pEAP_AKA_Context->R_Buff_Len));

        if(uicc_chk_fail(lResult))
            return lResult;

#if UICC_DEBUG_LEVEL == 4
        _pdu_print(pEAP_AKA_Context->R_Buff, pEAP_AKA_Context->R_Buff_Len );
#endif // UICC_DEBUG_LEVEL

        Len = uicc_get_SW(  pEAP_AKA_Context->R_Buff,
                            pEAP_AKA_Context->R_Buff_Len,
                            &SW1,&SW2);

        if(uicc_desc_SW(SW1,SW2))
            return 3;


        fcp_init(&(pEAP_AKA_Context->fcp));

        fcp_parsing(pEAP_AKA_Context->R_Buff,
                    Len,
                    &(pEAP_AKA_Context->fcp));

#if UICC_DEBUG_LEVEL >= 4
        fcp_print(&(pEAP_AKA_Context->fcp));
#endif //UICC_DEBUG_LEVEL

    }
    else
        return 2;

    return 0;
}

ReturnType eap_aka_uicc_verify_pin( tEAP_AKA_UICC_Context *pEAP_AKA_Context,
                                     unsigned char          index,
                                     unsigned char          PIN[/*8*/],
                                     unsigned char          PIN_Len)
{
    unsigned char       SW1 = 0;
    unsigned char       SW2 = 0;
    unsigned long       Len = 0;
    unsigned char       tPIN[8];
    ReturnType          lResult = 0;

    if ( PIN_Len > 8 )
        return 1;

    eap_aka_uicc_buf_init(  pEAP_AKA_Context,   MAX_BUF_LEN * 5 );

    memset(tPIN,0xFF,8);

    memcpy(tPIN,PIN,PIN_Len);

/*  if ( PIN_Len < 8 )
    {
        memset(tPIN+PIN_Len,0xFF,8-PIN_Len);
    }
*/
    lResult = uicc_verify_pin(  &(pEAP_AKA_Context->UICC_Context),
                                index,
                                8,
                                tPIN,
                                pEAP_AKA_Context->R_Buff,
                                &(pEAP_AKA_Context->R_Buff_Len));

    if(uicc_chk_fail(lResult))
        return lResult;

#if UICC_DEBUG_LEVEL == 4
    _pdu_print(pEAP_AKA_Context->R_Buff, pEAP_AKA_Context->R_Buff_Len );
#endif // UICC_DEBUG_LEVEL

    Len = uicc_get_SW(  pEAP_AKA_Context->R_Buff,
                        pEAP_AKA_Context->R_Buff_Len,
                        &SW1,&SW2);

    if(uicc_desc_SW(SW1,SW2))
        return 2;

    return 0;
}

ReturnType eap_aka_uicc_close(tEAP_AKA_UICC_Context *pEAP_AKA_Context)
{
	ReturnType          lResult = 0;

	xfunc_in();
	lResult = uicc_unpower_card(&(pEAP_AKA_Context->UICC_Context));
	xfunc_out("lResult=%d", lResult);

	return lResult;
}

ReturnType uicc_reset_state( 
    tUICC_Context *pUICC_Context,
    unsigned char R_APDU[],
    unsigned long *R_APDU_Len)
{
    apdu_init(&(pUICC_Context->C_APDU));

    (pUICC_Context->C_APDU).CLS = 0xA0; 
    (pUICC_Context->C_APDU).INS = RESET_STATE;//0x19;
    (pUICC_Context->C_APDU).P1 = 0x10; 
    (pUICC_Context->C_APDU).P2 = 0x00; 
    (pUICC_Context->C_APDU).Le = 0x01; 

    return uicc_send_APDU_T0(pUICC_Context,
        &(pUICC_Context->C_APDU),
        R_APDU, 
        R_APDU_Len);
}

static int USIMReset(wm_uicc_t *uicc)
{
	tEAP_AKA_UICC_Context *ctxt = uicc->uicc_ctxt;
	ReturnType  lResult = 0;

	lResult = uicc_reset_state(&ctxt->UICC_Context, ctxt->R_Buff, &ctxt->R_Buff_Len);
	xprintf(SDK_DBG, "uicc_reset_state lResult=%d\n", (int) lResult);

	if (lResult == SCARD_S_SUCCESS)
		return 0;
	else
		return -1;
}


static int USIMInit(wm_uicc_t *uicc)
{
	tEAP_AKA_UICC_Context *ctxt = uicc->uicc_ctxt;

	xfunc_in("inited=%d", uicc->inited);

	if (eap_aka_uicc_open(uicc)) {
		eap_aka_uicc_close(ctxt);
		uicc->inited = FALSE;
		return -1;
	}

	xprintf(SDK_DBG, "eap_aka_uicc_prepare_authenticate\n");
	if (eap_aka_uicc_prepare_authenticate(ctxt)) {
		eap_aka_uicc_close(ctxt);

		if (eap_aka_uicc_open(uicc)) {
			eap_aka_uicc_close(ctxt);
			goto error;
		}
		if (eap_aka_uicc_prepare_authenticate(ctxt)) {
			eap_aka_uicc_close(ctxt);
			goto error;
		}
	}

	#if defined(CHECK_PIN)
	if (eap_aka_uicc_verify_pin(ctxt, 1, PIN1, sizeof(PIN1))) {
		/* spurious: verify_pin always fails !!! */
		//xprintf(SDK_ERR, "Fail: eap_aka_uicc_verify_pin\n");
		//nic->gnUSIMInit = 0;
		//return -3;
	}
	#endif

	eap_aka_uicc_init_MSK(ctxt);
	uicc->inited = TRUE;
	xfunc_out();
	return 0;

error:
	xprintf(SDK_ERR, "%s failed\n", __FUNCTION__);
	uicc->inited = FALSE;
	xfunc_out();
	return -1;
}


static int USIMExit(wm_uicc_t *uicc)
{
	tEAP_AKA_UICC_Context *ctxt = uicc->uicc_ctxt;
	ReturnType uicc_ret;
	int ret = 0;

	xfunc_in("inited=%d", uicc->inited);

	if (uicc->inited && (uicc_ret = eap_aka_uicc_close(ctxt))) {
		xprintf(SDK_ERR, "eap_aka_uicc_close fail(%d)\n", (int)uicc_ret);
		ret = -1;
	}
	uicc->inited = FALSE;

	xfunc_out();
	return 0;
}

int SCardInit(int dev_idx)
{
	device_t *dev;
	wm_uicc_t *uicc;
	int ret = 0;

	xfunc_in();

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	uicc = &dev->wimax->dev_info.uicc;

	if (!uicc->uicc_active) {
		xprintf(SDK_ERR, "Smart card is not present");
		return -1;
	}

	if (uicc->inited)
		ret = USIMReset(uicc);
	else
		ret = USIMInit(uicc);

	xfunc_out("ret=%d", ret);
	return ret;
}

void SCardExit(int dev_idx)
{
	device_t *dev;
	wm_uicc_t *uicc;

	xfunc_in("dev=%d", dev_idx);

	if ((dev = dm_get_dev(dev_idx))) {
		uicc = &dev->wimax->dev_info.uicc;

		if (uicc->inited)
			USIMExit(uicc);
	}

	xfunc_out();
}

ReturnType uicc_send_APDU_T0(   tUICC_Context   *pUICC_Context,
                                tAPDU           *C_APDU,
                                unsigned char   R_APDU[],
                                unsigned long   *R_APDU_Len)
{
    unsigned long   Data_Len = 0;
    unsigned long   Total_Data_Len = 0;
    unsigned long   R_Max_Len = 0;
    ReturnType      lResult = 0;


    if(0 != pUICC_Context->m_hScardHandle)
    {
        if(C_APDU != &(pUICC_Context->C_APDU))
            apdu_copy(C_APDU,   &(pUICC_Context->C_APDU) );

        if(*R_APDU_Len == 0)
            R_Max_Len = MAX_BUF_LEN;
        else
            R_Max_Len = *R_APDU_Len;

send_cmd:
        apdu_2_pdu(&(pUICC_Context->C_APDU),&(pUICC_Context->PDU));

        pUICC_Context->SW1 = 0;
        pUICC_Context->SW2 = 0;

        lResult = 0;

        *R_APDU_Len = R_Max_Len;


        lResult = SCardTransmit(    pUICC_Context->m_hScardHandle,
                                    SCARD_PCI_T0,
                                    (pUICC_Context->PDU).m_Data,
                                    (pUICC_Context->PDU).m_Len,
                                    NULL,
                                    R_APDU + Total_Data_Len,
                                    R_APDU_Len);

        if(SCARD_S_SUCCESS != lResult)
        {
            UICC_PRINT("Error (SCardTransmit) : %u\n",lResult);

            return lResult;
        }

        Data_Len = 0;

        Data_Len = uicc_get_SW( R_APDU,
                                *R_APDU_Len,
                                &(pUICC_Context->SW1),
                                &(pUICC_Context->SW2));

        Total_Data_Len += Data_Len;

        if( 0x61 == pUICC_Context->SW1 )            // get response
        {
            if( Total_Data_Len + (pUICC_Context->SW2) >= (R_Max_Len - 2))
            {
                *R_APDU_Len = Total_Data_Len + 2;
                return lResult;
            }

            apdu_init( &(pUICC_Context->C_APDU));

            (pUICC_Context->C_APDU).CLS = 0x00;
            (pUICC_Context->C_APDU).INS = GET_RESPONSE;
            (pUICC_Context->C_APDU).P1 = 0x00;
            (pUICC_Context->C_APDU).P2 = 0x00;
            (pUICC_Context->C_APDU).Le = (pUICC_Context->SW2);

            goto send_cmd;
        }
        else if( (0x62 == pUICC_Context->SW1) || (0x63 == pUICC_Context->SW1) ) // warning. more data available ?
        {
            apdu_init( &(pUICC_Context->C_APDU));

            (pUICC_Context->C_APDU).CLS = 0x00;
            (pUICC_Context->C_APDU).INS = GET_RESPONSE;
            (pUICC_Context->C_APDU).P1 = 0x00;
            (pUICC_Context->C_APDU).P2 = 0x00;
            (pUICC_Context->C_APDU).Le = 0x00;

            goto send_cmd;
        }
        else if( 0x6C == pUICC_Context->SW1)        // Le = SW2
        {
            (pUICC_Context->C_APDU).Le = (pUICC_Context->SW2);

            goto send_cmd;
        }
        else if( 0x90 == pUICC_Context->SW1 )       // success.     // More Data 0x91,0x92 LATER.
        {
            *R_APDU_Len = Total_Data_Len + 2;

            return lResult;
        }
        else
        {
            *R_APDU_Len = Total_Data_Len + 2;
			//goto send_cmd;
			//uicc_init_ctxt ();
			//uicc_connect ();
			//goto send_cmd;

            return lResult;
        }

        return lResult;
    }
    else
    {
        xprintf(SDK_ERR, "Invalid Scard Handle (NULL)");
        return 1;
    }
}


ReturnType uicc_authenticate(   tUICC_Context   *pUICC_Context,
                                unsigned char   Lc,
                                unsigned char   msg[],
                                unsigned char   R_APDU[],
                                unsigned long   *R_APDU_Len)
{
    apdu_init(&(pUICC_Context->C_APDU));

    if( pUICC_Context->gVendor == kt_uicc)
    {
	xprintf(SDK_INFO, "gVendor: kt_uicc\n");
    	(pUICC_Context->C_APDU).CLS = 0xA0;
    	(pUICC_Context->C_APDU).INS = AUTHENTICATE;

    	(pUICC_Context->C_APDU).P1 = 0x00;
    	(pUICC_Context->C_APDU).P2 = 0xFF;
    }
    else if( pUICC_Context->gVendor == sk_telecom_uicc )
    {   
		xprintf(SDK_INFO, "gVendor: sk_uicc\n");
        (pUICC_Context->C_APDU).CLS = 0x00;
        (pUICC_Context->C_APDU).INS = AUTHENTICATE;

        (pUICC_Context->C_APDU).P1 = 0x00;
        (pUICC_Context->C_APDU).P2 = 0x00;
    }
    else
    {
		xprintf(SDK_INFO, "gVendor: other_uicc\n");
        (pUICC_Context->C_APDU).CLS = 0x00;
        (pUICC_Context->C_APDU).INS = AUTHENTICATE;

        (pUICC_Context->C_APDU).P1 = 0x00;
        (pUICC_Context->C_APDU).P2 = 0x00;
    }

	xprintf_hex(SDK_DBG, "UICC Msg", msg, Lc);

    (pUICC_Context->C_APDU).Lc = Lc;
    memcpy((pUICC_Context->C_APDU).Data,msg,Lc);

    return uicc_send_APDU_T0(   pUICC_Context,
                                &(pUICC_Context->C_APDU),
                                R_APDU,
                                R_APDU_Len);
}


unsigned char uicc_desc_SW( unsigned char   SW1,
                            unsigned char   SW2)
{
    unsigned char b_error = 1;

    switch(SW1)
    {
        case 0x90: //responses to commands which are correctly executed.
        {
            xprintf(SDK_DBG, "correctly executed : normal ending of the command\n");

            b_error = 0;

            break;  
        }
        case 0x91:
        {
            xprintf(SDK_ERR, "correctly executed : normal ending of the command, with extra information\n");
            xprintf(SDK_ERR, "from the proactive SIM containing a command for the ME.\n");
            xprintf(SDK_ERR, "Length 0x%02X of the response data.\n",SW2);
        
            break;  
        }
        case 0x9E:
        {
            xprintf(SDK_ERR, "length 0x%02X of the response data given in case of a SIM data download error.\n",SW2);
            break;  
        }
        case 0x9F:
        {
            xprintf(SDK_ERR, "length 0x%02X of the response data.\n",SW2);
            break;
        }   
        case 0x93:  // response to commands which are postponed.
        {
            xprintf(SDK_ERR, "commands which are postponed :\n");
		xprintf(SDK_ERR, "\tSIM Application Toolkit is busy. Command cannot be executed at present,\n");
            xprintf(SDK_ERR, "\tfurther normal commands are allowed.\n");
            break;
        }   
        case 0x92:  // memory management
        {
            if(SW2 == 0x40)
            {
                xprintf(SDK_ERR, "memory management : memory problem.\n");
            }   
            else
            {
                xprintf(SDK_ERR, "memory management : command successful but after using an internal update retry routine %d times.\n",SW2);   
            }
            break;
        }
        case 0x94:  // referencing management.
        {
            if((SW2&0x01) == 0x00)
            {
                xprintf(SDK_ERR, "referencing management : no EF selected.\n");
            }
            if((SW2&0x02) == 0x02)
            {
                xprintf(SDK_ERR, "referencing management : out of range (invalid address)\n");
            }
            if((SW2&0x04) == 0x04)
            {
                xprintf(SDK_ERR, "referencing management : file ID not found or pattern not found.\n");
            }
            if((SW2&0x08) == 0x08)
            {   
                xprintf(SDK_ERR, "referencing management : file is inconsistent with the command.\n");
            }

            break;
        }
        case 0x98:  // security management
        {
            if(SW2 == 0x02)
            {
                xprintf(SDK_ERR, "security management : no CHV(PIN) initialized.\n");
            }
            else if(SW2 == 0x04)
            {   
                xprintf(SDK_ERR, "security management : access condition not fulfilled.\n");
                xprintf(SDK_ERR, "unsuccessful CHV (PIN) verification, at least one attempt left.\n");
                xprintf(SDK_ERR, "unsuccessful UNBLOCK CHK(PIN) verification, at least one attempt left.\n");
                xprintf(SDK_ERR, "authentication failed.\n");
            }
            else if(SW2 == 0x08)
            {
                xprintf(SDK_ERR, "security management : in contradiction with CHV(PIN) status.\n");
            }
            else if(SW2 == 0x10)
            {
                xprintf(SDK_ERR, "security management : in contradiction with invalidation status.\n");
            }
            else if(SW2 == 0x40)
            {
                xprintf(SDK_ERR, "security management : unsuccessful CHV (PIN) verifiation, no attempt left.\n");
                xprintf(SDK_ERR, "unsuccessful UNBLOCK CHV(PIN) verification, no attempt left.\n");
                xprintf(SDK_ERR, "CHV blocked.\n");
                xprintf(SDK_ERR, "UNBLOCK CHV blocked.\n");
            }
            else if(SW2 == 0x50)
            {
                xprintf(SDK_ERR, "security management : increase cannot be performed, Max value reached.\n");
            }
            else if(SW2 == 0x62)
            {
                xprintf(SDK_ERR, "security management : authentication error (AUTHENTICATE command only)\n");
                xprintf(SDK_ERR, "incorrect MAC (3G security context).\n");
                xprintf(SDK_ERR, "EAP Failure packet received. (EAP authenticate).\n");
            }
            else if(SW2 == 0x64)
            {
                xprintf(SDK_ERR, "security management : Authentication error, security context not supported.\n");
            }
            else if(SW2 == 0x65)
            {
                xprintf(SDK_ERR, "security management : key refreshness failure (3G security context).\n");
            }
            else
            {
                xprintf(SDK_ERR, "security management : UNKNOWN STATUS BYTES. SW1 : 0x%02X, SW2 : 0x%02X\n",SW1,SW2);
            }
            break;
        }
        case 0x67:  // application independent error
        {
            xprintf(SDK_ERR, "application independent error : incorrect parameter P3\n");

            if(SW2 == 0x00)
            {
                xprintf(SDK_ERR, "Checking Error : Wrong Length \n");
            }
            else
            {
                xprintf(SDK_ERR, "Checking Error : the interpretation of this status word is command dependent.\n");
            }

            break;
        }
        case 0x62:  //Warning.
        {
            if(SW2 == 0x00)
            {
                xprintf(SDK_ERR, "Warning : no information given,state of non-volatile memory unchanged.\n");
            }
            else if(SW2 == 0x81)
            {
                xprintf(SDK_ERR, "Warning : part of returned data may be corrupted.\n");
            }
            else if(SW2 == 0x82)
            {
                xprintf(SDK_ERR, "Warning : end of file or record reached before reading Le bytes.\n");
            }
            else if(SW2 == 0x83)
            {
                xprintf(SDK_ERR, "Warning : selected file invalidated.\n");
            }
            else if(SW2 == 0x85)
            {
                xprintf(SDK_ERR, "Warning : selected DF in termination state.\n");
            }
            else if(SW2 == 0xF1)
            {
                xprintf(SDK_ERR, "Warning : more data available.\n");
            }
            else if(SW2 == 0xF2)
            {
                xprintf(SDK_ERR, "Warning : more data available and proactive command pending.\n");
            }
            else
            {
                xprintf(SDK_ERR, "UNKNOWN STATUS BYTES. SW1 : 0x%02X, SW2 : 0x%02X\n",SW1,SW2);
            }
            break;
        }
        case 0x63:  //Warning.
        {
            if(SW2 == 0xF1)
            {
                xprintf(SDK_ERR, "Warning : more data expected.\n");
            }
            else if(SW2 == 0xF2)
            {
                xprintf(SDK_ERR, "Warning : more data expected and proactive command pending.\n");
            }
            else if((SW2&0xC0) == 0xC0)
            {
                xprintf(SDK_ERR, "Warning : command successful but after using an internal update retry routine %d times\n",SW2);
                xprintf(SDK_ERR, "verification failed. %d retries remaining.\n",SW2);
            }
            else
            {
                xprintf(SDK_ERR, "Warning : UNKNOWN STATUS BYTES. SW1 : 0x%02X, SW2 : 0x%02X\n",SW1,SW2);
            }
            break;
        }
        case 0x64:
        {
            if(SW2 == 0x00)
            {
                xprintf(SDK_ERR, "no information given, state of non-volatile memory unchanged.\n");
            }
            else
            {
                xprintf(SDK_ERR, "UNKNOWN STATUS BYTES. SW1 : 0x%02X, SW2 : 0x%02X\n",SW1,SW2);
            }
            break;
        }
        case 0x65:
        {
            if(SW2 == 0x00)
            {
                xprintf(SDK_ERR, "no information given, state of non-volatile memory changed.\n");
            }
            else if(SW2 == 0x81)
            {
                xprintf(SDK_ERR, "memory problem.\n");
            }
            else
            {   
                xprintf(SDK_ERR, "UNKNOWN STATUS BYTES. SW1 : 0x%02X, SW2 : 0x%02X\n",SW1,SW2);
            }
            break;
        }
        case 0x6B:
        {
            xprintf(SDK_ERR, "application independent error : incorrect parameter P1 or P2\n");

            if(SW2 == 0x00)
            {
                xprintf(SDK_ERR, "Checking Error : wrong parameter(s) P1-P2\n");
            }
            else
            {
                xprintf(SDK_ERR, "Checking Error : UNKNOWN STATUS BYTES. SW1 : 0x%02X, SW2 : 0x%02X\n",SW1,SW2);
            }

            break;
        }
        case 0x6D:
        {
            xprintf(SDK_ERR, "application independent error : unknown instruction code given in the command.\n");

            if(SW2 == 0x00)
            {
                xprintf(SDK_ERR, "Checking Error : instruction code not supported or invalid.\n");
            }
            else
            {
                xprintf(SDK_ERR, "Checking Error : UNKNOWN STATUS BYTES. SW1 : 0x%02X, SW2 : 0x%02X\n",SW1,SW2);
            }

            break;
        }
        case 0x6E:
        {
            xprintf(SDK_ERR, "application independent error : wrong instruction class given in the command.\n");

            if(SW2 == 0x00)
            {
                xprintf(SDK_ERR, "Checking Error : class not supported.\n");
            }
            else
            {
                xprintf(SDK_ERR, "Checking Error : UNKNOWN STATUS BYTES. SW1 : 0x%02X, SW2 : 0x%02X\n",SW1,SW2);
            }

            break;
        }
        case 0x6F:
        {
            xprintf(SDK_ERR, "application independent error : technical problem with no diagnostic given.\n");

            if(SW2 == 0x00)
            {
                xprintf(SDK_ERR, "Checking Error : techinical problem, no precise diagnosis.\n");
            }
            else
            {
                xprintf(SDK_ERR, "Checking Error : the interpretation of this status word is command dependent.\n");
            }

            break;
        }
        case 0x68:
        {
            if(SW2 == 0x00)
            {
                xprintf(SDK_ERR, "CLS not supported : no information given.\n");
            }
            else if(SW2 == 0x81)
            {
                xprintf(SDK_ERR, "CLS not supported : logical channel not supported.\n");
            }
            else if(SW2 == 0x82)
            {
                xprintf(SDK_ERR, "CLS not supported : secure messaging not supported.\n");
            }
            else
            {
                xprintf(SDK_ERR, "CLS not supported : UNKNOWN STATUS BYTES. SW1 : 0x%02X, SW2 : 0x%02X\n",SW1,SW2);
            }
            break;
        }
        case 0x69:  // command not allowed
        {
            if(SW2 == 0x00)
            {
                xprintf(SDK_ERR, "command not allowed : no information given.\n");
            }
            else if(SW2 == 0x81)
            {
                xprintf(SDK_ERR, "command not allowed : command incompatible with file structure.\n");
            }
            else if(SW2 == 0x82)
            {
                xprintf(SDK_ERR, "command not allowed : security status not satisfied.\n");
            }
            else if(SW2 == 0x83)
            {
                xprintf(SDK_ERR, "command not allowed : authentication PIN method blocked.\n");
            }
            else if(SW2 == 0x84)
            {
                xprintf(SDK_ERR, "command not allowed : referenced data invalidated.\n");
            }
            else if(SW2 == 0x85)
            {
                xprintf(SDK_ERR, "command not allowed : conditions of use not satisfied.\n");
            }
            else if(SW2 == 0x86)
            {
                xprintf(SDK_ERR, "command not allowed : command not allowed (no EF selected).\n");
            }
            else
            {
                xprintf(SDK_ERR, "command not allowed : UNKNOWN STATUS BYTES. SW1 : 0x%02X, SW2 : 0x%02X\n",SW1,SW2);
            }
            break;
        }
        case 0x6A:  // wrong parameters
        {
            if(SW2 == 0x80)
            {
                xprintf(SDK_ERR, "wrong parameters : incorrect parameters in the data field.\n");
            }
            else if(SW2 == 0x81)
            {
                xprintf(SDK_ERR, "wrong parameters : function not supported.\n");
            }
            else if(SW2 == 0x82)
            {
                xprintf(SDK_INFO, "wrong parameters : file not found.\n");
            }
            else if(SW2 == 0x83)
            {
                xprintf(SDK_ERR, "wrong parameters : record not found.\n");
            }
            else if(SW2 == 0x84)
            {
                xprintf(SDK_ERR, "wrong parameters : not enough memory space.\n");
            }
            else if(SW2 == 0x86)
            {
                xprintf(SDK_ERR, "wrong parameters : incorrect parameters P1 to P2.\n");
            }
            else if(SW2 == 0x87)
            {
                xprintf(SDK_ERR, "wrong parameters : Lc inconsistent with P1 to P2.\n");
            }
            else if(SW2 == 0x88)
            {
                xprintf(SDK_ERR, "wrong parameters : referenced data not found.\n");
            }
            else
            {
                xprintf(SDK_ERR, "wrong parameters : UNKNOWN STATUS BYTES. SW1 : 0x%02X, SW2 : 0x%02X\n",SW1,SW2);
            }
            break;
        }
        default:
        {
            xprintf(SDK_ERR, "UNKNOWN STATUS BYTES. SW1 : 0x%02X, SW2 : 0x%02X\n",SW1,SW2);
        }
    }

    return b_error;
}




unsigned long uicc_get_SW(unsigned char APDU[], 
                          unsigned long APDU_Len, 
                          unsigned char *SW1,
                          unsigned char *SW2)
{
    if(2 <= APDU_Len)
    {
        *SW1 = APDU[APDU_Len-2];
        *SW2 = APDU[APDU_Len-1];

        return APDU_Len - 2;
    }

    return 0;
}

ReturnType   eap_aka_uicc_select_EF_EAPKEYS(tEAP_AKA_UICC_Context   *pEAP_AKA_Context)          //IN
{
    unsigned char   SW1 = 0;
    unsigned char   SW2 = 0;
    unsigned long   Len = 0;
    ReturnType      lResult = 0;

    eap_aka_uicc_buf_init(  pEAP_AKA_Context,   MAX_BUF_LEN * 5 );

    lResult = uicc_select(  &(pEAP_AKA_Context->UICC_Context),
                            0x00,   
                            0x02,   
                            EF_EAPKEYS,
                            pEAP_AKA_Context->R_Buff,
                            &(pEAP_AKA_Context->R_Buff_Len));

    if(uicc_chk_fail(lResult))
        return EAP_UICC_WITH_ERROR;

#if UICC_DEBUG_LEVEL == 4
    _pdu_print(pEAP_AKA_Context->R_Buff, pEAP_AKA_Context->R_Buff_Len );
#endif // UICC_DEBUG_LEVEL

    Len = uicc_get_SW(  pEAP_AKA_Context->R_Buff,
                        pEAP_AKA_Context->R_Buff_Len,
                        &SW1,&SW2);

    if(uicc_desc_SW(SW1,SW2))
        return EAP_UICC_WITH_ERROR;

    fcp_init(&(pEAP_AKA_Context->fcp));

    fcp_parsing(pEAP_AKA_Context->R_Buff,
                    Len,
                    &(pEAP_AKA_Context->fcp));

#if UICC_DEBUG_LEVEL >= 4
        fcp_print(&(pEAP_AKA_Context->fcp));
#endif //UICC_DEBUG_LEVEL


    eap_aka_uicc_buf_init(  pEAP_AKA_Context,   MAX_BUF_LEN * 5 );

    lResult = uicc_read_binary( &(pEAP_AKA_Context->UICC_Context),
                                pEAP_AKA_Context->fcp.Total_File_Size.Size[1], // 66*2 = 0x84.
                                pEAP_AKA_Context->R_Buff,
                                &(pEAP_AKA_Context->R_Buff_Len));

#if UICC_DEBUG_LEVEL == 4
    _pdu_print(pEAP_AKA_Context->R_Buff, pEAP_AKA_Context->R_Buff_Len );
#endif // UICC_DEBUG_LEVEL

    Len = uicc_get_SW(      pEAP_AKA_Context->R_Buff,
                            pEAP_AKA_Context->R_Buff_Len,
                            &SW1,&SW2);

    if(uicc_desc_SW(SW1,SW2))
        return EAP_UICC_WITH_ERROR;

    if( ( pEAP_AKA_Context->R_Buff[0] == 0x80 ) && ( pEAP_AKA_Context->R_Buff[1] == 64 ) )
        memcpy( pEAP_AKA_Context->MSK,      &(pEAP_AKA_Context->R_Buff[2]), 64);

    if( ( pEAP_AKA_Context->fcp.Total_File_Size.Size[1] >= 0x84 ) &&
        ( pEAP_AKA_Context->R_Buff[66] == 0x81 ) &&
        ( pEAP_AKA_Context->R_Buff[67] == 64 ) )
    {
        memcpy( pEAP_AKA_Context->EMSK,     &(pEAP_AKA_Context->R_Buff[68]),64);
    }

    return EAP_UICC_MSK_SUCCESS;
}

ReturnType   eap_aka_uicc_auth( tEAP_AKA_UICC_Context   *pEAP_AKA_Context,          //IN
                                unsigned char           packet[],                   //IN
                                unsigned char           packet_len,                 //IN
                                unsigned char           r_packet[],                 //OUT   
                                unsigned long           *r_packet_len,              //OUT   
                                unsigned char           notification_data[],        //OUT   
                                unsigned short          *notification_data_len,     //OUT   
                                unsigned short          *aka_notification_code,     //OUT   
                                unsigned char           error_code[/*3*/])          //OUT        
{
    unsigned char       SW1 = 0;
    unsigned char       SW2 = 0;
    unsigned long       Len = 0;
    unsigned char       uicc_packet_type = 0;
    unsigned char       uicc_packet_sub_type = 0;
    unsigned short      uicc_packet_len = 0;
    unsigned char       tag = 0;
    ReturnType          lResult;
    unsigned char       bEAP_Success = 0;

    eap_aka_uicc_buf_init(  pEAP_AKA_Context,   MAX_BUF_LEN * 5 );

    if ( (pEAP_AKA_Context->UICC_Context).gVendor != kt_uicc )
    {   
        if(packet[0] == 0x03)
            bEAP_Success = 1;
    }

    lResult = uicc_authenticate(    &(pEAP_AKA_Context->UICC_Context),
                                    packet_len,
                                    packet,
                                    pEAP_AKA_Context->R_Buff,
                                    &(pEAP_AKA_Context->R_Buff_Len));

    if (uicc_chk_fail(lResult))
        return lResult;

    Len = uicc_get_SW(  pEAP_AKA_Context->R_Buff,
                        (pEAP_AKA_Context->R_Buff_Len),
                        &SW1,&SW2);

    if (uicc_desc_SW(SW1,SW2))
    {
        (pEAP_AKA_Context->UICC_Context).SW1 = SW1;
        (pEAP_AKA_Context->UICC_Context).SW2 = SW2;

        if( (SW1 == 0x98) && (SW2 == 0x62) )
        {
            xprintf(SDK_ERR, "Authentication error (EAP Failure Packet Received)");
            return EAP_UICC_AUTH_FAILURE;
        }
        else if( (SW1 == 0x62) && (SW2 == 0x00) )
        {
            xprintf(SDK_ERR, "Unexpected EAP packet received, EAP Packet silently ignored.");
            return EAP_UICC_MSG_IGNORED;
        }

        return EAP_UICC_WITH_ERROR; 
    }
    else // SW1 == 0x90 : success
    {
        if(pEAP_AKA_Context->R_Buff[0] == 0xCD) // EAP_SUCCESS.
        {
            UICC_PRINT("get MSK from UICC.");

            memcpy( pEAP_AKA_Context->MSK,      &(pEAP_AKA_Context->R_Buff[1]), 64);
            memcpy( pEAP_AKA_Context->EMSK,     &(pEAP_AKA_Context->R_Buff[65]),    64);
            pEAP_AKA_Context->IdentityType =    pEAP_AKA_Context->R_Buff[129];
            pEAP_AKA_Context->IdentityLength =  pEAP_AKA_Context->R_Buff[130];

            memcpy(pEAP_AKA_Context->Identity,  &(pEAP_AKA_Context->R_Buff[131]),
                            pEAP_AKA_Context->IdentityLength);

            pEAP_AKA_Context->AuthenticationMode =
                        pEAP_AKA_Context->R_Buff[131+pEAP_AKA_Context->IdentityLength];

            return EAP_UICC_MSK_SUCCESS;
        } // EAP_SUCCESS
        else if( pEAP_AKA_Context->R_Buff[0] == 0x02)   // EAP_Response
        {
            UICC_PRINT("\nget EAP_Response Packet from UICC.\n");

            uicc_packet_type = pEAP_AKA_Context->R_Buff[4];
            uicc_packet_sub_type = pEAP_AKA_Context->R_Buff[5];

            uicc_packet_len = pEAP_AKA_Context->R_Buff[3];
            memcpy(r_packet,pEAP_AKA_Context->R_Buff,uicc_packet_len);
            *r_packet_len = uicc_packet_len;

            if( Len > uicc_packet_len )
            {
                tag = pEAP_AKA_Context->R_Buff[uicc_packet_len];

                if(uicc_packet_type == 0x02) // Notification.
                {
                    if(tag == 0xCB) // [Optional]
                    {
                        xprintf(SDK_INFO, "get Notification Data from UICC.");

                        *notification_data_len =
                                pEAP_AKA_Context->R_Buff[uicc_packet_len + 1]*256 +
                                pEAP_AKA_Context->R_Buff[uicc_packet_len + 2];

                        memcpy( notification_data,
                                &(pEAP_AKA_Context->R_Buff[uicc_packet_len + 3]),
                                *notification_data_len);

                        return EAP_UICC_WITH_NOTIFICATION_DATA;
                    }
                    else
                    {
                        xprintf(SDK_ERR, "Unknown tag 0x%02X in R_APDU from UICC",tag);

                        return EAP_UICC_ONLY_PACKET;
                    }
                }
                else if ( (uicc_packet_type == 23) && ( uicc_packet_sub_type == 12 ) ) // AKA-Notification.
                {
                    if(tag == 0xCA) // [Optional]
                    {
                        UICC_PRINT("get Notification Code from UICC.");

                        *aka_notification_code = pEAP_AKA_Context->R_Buff[uicc_packet_len + 1];

                        return EAP_UICC_WITH_AKA_NOTIFICATION_CODE;
                    }
                    else
                    {
                        xprintf(SDK_ERR, "Unknown tag 0x%02X in R_APDU from UICC",tag);

                        return EAP_UICC_ONLY_PACKET;
                    }
                }

                return EAP_UICC_ONLY_PACKET;
            } // Len > uicc_packet_len
            else
            {
                return EAP_UICC_ONLY_PACKET;
            }
        } // EAP_Response
        else if( ( Len == 3 ) ) // && (packet[0] == 0x04) ) //maybe client error
        {
            xprintf(SDK_ERR, "get Error code (EAP-Failure) from UICC.");

            memcpy(error_code,pEAP_AKA_Context->R_Buff,3);

            return EAP_UICC_WITH_ERROR_CODE;
        } // error code
        else if( bEAP_Success == 1 )
        {
            UICC_PRINT("get MSK from UICC.");

            bEAP_Success = 0;

            return eap_aka_uicc_select_EF_EAPKEYS(  pEAP_AKA_Context );
        }
        else
        {
            xprintf(SDK_ERR, "get Unknown Case Data from UICC (dumping).");

            return EAP_UICC_WITH_UNKNOWN_CASE_DATA;
        } // unknown case data.

    } // SW1 == 0x90
}

#if defined(USE_CHECK_THREAD)
/* Thread function for text CM */

void *SCStatusCheck_thr(void *data)
{
	int dev_idx = (int) data;
	device_t *dev;
	wm_uicc_t *uicc;
	int szReaders_arr[256];
	int szReaderName_arr[256];
	SCARDCONTEXT hSC = 0;
	int i, lResult;
	char *szReaders = (char *)szReaders_arr;
	char *szReaderName = (char *)szReaderName_arr;
	DWORD dwhReaders = 256;
	SCARD_READERSTATE sc_state;
	int readerIndex;

	if (!(dev = dm_get_dev(dev_idx)))
		return NULL;

	xfunc_in("dev=%d", dev_idx);

	uicc = &dev->wimax->dev_info.uicc;
	uicc->reader_idx = dev_idx - DEV_BASE_IDX;

	lResult = SCardEstablishContext(SCARD_SCOPE_SYSTEM, NULL, NULL, &hSC);

	if (lResult != SCARD_S_SUCCESS) {
		uicc_print_error(lResult);
		xprintf(SDK_ERR, "SCardEstablishContext failed [%x]\n", lResult);
		goto out;
	}

	lResult = SCardListReaders(hSC, NULL, NULL, &dwhReaders);
	if (lResult != SCARD_S_SUCCESS) {
		uicc_print_error(lResult);
		xprintf(SDK_ERR, "SCardListReaders failed [%x]\n", lResult);
		goto out;
	}

	lResult = SCardListReaders(hSC, NULL, (char *)szReaders, &dwhReaders);
	if (lResult != SCARD_S_SUCCESS) {
		uicc_print_error(lResult);
		xprintf(SDK_ERR, "SCardListReaders(%d) failed [%x]\n", dwhReaders, lResult);
		goto out;
	}

	#if 0
	xprintf(SDK_FORCE, "SCardListReaders :");
	for (i = 0; i < dwhReaders - 1; i++)
		xprintf(SDK_FORCE, "%c", szReaders[i]);
	xprintf(SDK_FORCE, "\n");
	#endif

	if (szReaders[0] == '\0')
		xprintf(SDK_ERR, "SCStatusCheck_Thr: Reader not found\n");

	for (i = 0; i < dwhReaders; i++) {
		xprintf(SDK_INFO, "Reader %s\n", &szReaders[i]);

		szReaderName = &szReaders[i];
		/*
		* 	- GCT USB dongle supports only one slot
		* 	- Reader Name Format: "X Reader 01 00"
		* 			01: reader index
		* 			00: slot index in the reader
		*/
		int len = strlen(szReaderName)-4;
		char tmp[3];
		tmp[1] = szReaderName[len--];
		tmp[0] = szReaderName[len--];
		tmp[2] = '\0';
		readerIndex = strtoul(tmp, NULL, 16);
		if (uicc->reader_idx == readerIndex) {
			sc_state.szReader = &szReaderName[i];
			sc_state.dwCurrentState = SCARD_STATE_UNAWARE;
			break;
		}
		while (szReaders[++i] != 0)
			;
	}

	if (i==dwhReaders)	/* no matching reader (end of lptzReaderList met) */
		xprintf(SDK_ERR, "SCStatusCheck_Thr: Reader not found\n");

	while(1) {
		lResult = SCardGetStatusChange(hSC, INFINITE, &sc_state, 1);
		if (lResult != SCARD_S_SUCCESS) {
			xprintf(SDK_ERR, "SCardStateCheck: SCardGetStatusChange failed [%x]\n", lResult);
			goto out;
		}

		if (sc_state.dwEventState & SCARD_STATE_CHANGED) {
			if (sc_state.dwEventState & SCARD_STATE_EMPTY) {
				eap_noti_text(dev_idx, "SCardStateCheck: SCard empty", TRUE);
				uicc->uicc_active = FALSE;
				SCardExit(dev_idx);
			}
			else if (sc_state.dwEventState & SCARD_STATE_PRESENT) {
				xprintf(SDK_INFO, "SCardStateCheck: SCard present\n");
				uicc->uicc_active = TRUE;
			}
			else {
				xprintf(SDK_ERR, "SCardStateCheck: Unknown state [%x]\n",
					(int) sc_state.dwEventState);
			}

			sc_state.dwCurrentState = sc_state.dwEventState;
		}
	}

out:
	if (hSC)
		SCardReleaseContext(hSC);
	uicc->reader_thr = (pthread_t) NULL;
	return NULL;
}
#endif

int do_eap_aka(int dev_idx, char *buf, int len)
{
	device_t *dev;
	wm_uicc_t *uicc;
	tEAP_AKA_UICC_Context *ctxt;
	u8 resp_pkt[MAX_BUF_LEN*2];
	u32 resp_pkt_len = 0;
	u8 notifi_data[MAX_BUF_LEN*2];
	u16 notifi_data_len = 0;
	u16 aka_notifi_code = 0;
	u8 error_code[3];
	int ret = -1, res, lmask = SDK_DBG;

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	xfunc_in("dev=%d, len=%d", dev_idx, len);

	uicc = &dev->wimax->dev_info.uicc;
	ctxt = uicc->uicc_ctxt;

	res = (int) eap_aka_uicc_auth(ctxt, (u8 *)buf, len, resp_pkt, 
							&resp_pkt_len, notifi_data, &notifi_data_len, 
							&aka_notifi_code, error_code);

	switch (res) {
		case EAP_UICC_MSK_SUCCESS :
			xprintf(lmask, "USIM: EAP_UICC_MSK_SUCCESS(%x)\n", res);
			ret = send_eap_tls_mask(dev_idx, (char *) ctxt->MSK, WIMAX_EAP_MSK_LEN);
			break;
		case EAP_UICC_ONLY_PACKET :
			xprintf(lmask, "USIM: EAP_UICC_ONLY_PACKET(%x)\n", res);
			ret = send_eap_tls_resp(dev_idx, (char *) resp_pkt, (int) resp_pkt_len);
			break;
		case EAP_UICC_WITH_NOTIFICATION_DATA :
			xprintf(lmask, "USIM: EAP_UICC_WITH_NOTIFICATION_DATA(%x)\n", res);
			ret = send_eap_tls_resp(dev_idx, (char *) resp_pkt, (int) resp_pkt_len);
			break;
		case EAP_UICC_WITH_AKA_NOTIFICATION_CODE :
			xprintf(lmask, "USIM: EAP_UICC_WITH_AKA_NOTIFICATION_CODE(%x)\n", res);
			ret = send_eap_tls_resp(dev_idx, (char *) resp_pkt, (int) resp_pkt_len);
			break;
		case EAP_UICC_WITH_ERROR_CODE :
			xprintf(SDK_ERR, "USIM: EAP_UICC_WITH_ERROR_CODE(%x)\n", res);
			xprintf_hex(SDK_ERR, "UICC EAP Error", error_code, sizeof(error_code));
			break;
		case EAP_UICC_AUTH_FAILURE :
			xprintf(SDK_ERR, "USIM: EAP_UICC_AUTH_FAILURE(%x)\n", res);
			break;
		case EAP_UICC_MSG_IGNORED :
			xprintf(SDK_ERR, "USIM: EAP_UICC_MSG_IGNORED(%x)\n", res);
			break;
		case EAP_UICC_WITH_ERROR :
			xprintf(SDK_ERR, "USIM: EAP_UICC_WITH_ERROR(%x)\n", res);
			break;
		case EAP_UICC_WITH_UNKNOWN_CASE_DATA :
			xprintf(SDK_ERR, "USIM: EAP_UICC_WITH_ERROR(%x)\n", res);
			break;
		default :
			xprintf(SDK_ERR, "USIM:default(%x)\n", res);
	}

	xfunc_out("ret=%d", ret);
	return ret;
}

void uicc_open(int dev_idx)
{
	device_t *dev;
	wm_uicc_t *uicc;

	xfunc_in("dev=%d", dev_idx);

	if (!(dev = dm_get_dev(dev_idx)))
		return;

	uicc = &dev->wimax->dev_info.uicc;

	if (!uicc->uicc_ctxt) {
		uicc->uicc_ctxt = sdk_malloc(sizeof(tEAP_AKA_UICC_Context));
		memset(uicc->uicc_ctxt, 0, sizeof(tEAP_AKA_UICC_Context));

		#if defined(USE_CHECK_THREAD)
		uicc->reader_thr = (pthread_t) NULL;
		pthread_create(&uicc->reader_thr, NULL, SCStatusCheck_thr, (void *)dev_idx);
		#else
		dev->wimax->dev_info.uicc.reader_idx = dev_idx - DEV_BASE_IDX;
		dev->wimax->dev_info.uicc.uicc_active = TRUE;
		#endif
	}
	xfunc_out();
}

void uicc_close(int dev_idx)
{
	device_t *dev;
	wm_uicc_t *uicc;
	#if defined(USE_CHECK_THREAD)
	pthread_t thread;
	#endif

	if (!(dev = dm_get_dev(dev_idx)))
		return;

	xfunc_in("dev=%d", dev_idx);
	assert(dev->wimax);

	uicc = &(dev->wimax->dev_info.uicc);

	if (uicc->uicc_ctxt) {
		#if defined(USE_CHECK_THREAD)
		if ((thread = uicc->reader_thr)) {
			uicc->reader_thr = (pthread_t) NULL;
			pthread_cancel(thread);
			xprintf(SDK_DBG, "uicc thread cancel[%d] done\n", dev_idx);
			ret = pthread_join(thread, NULL;
			xprintf(SDK_DBG, "uicc thread join[%d] ret=%d done\n", dev_idx, ret);
		}
		#endif

		if (dev->inserted)
			SCardExit(dev_idx);

		uicc_uninit_ctxt();

		sdk_free(uicc->uicc_ctxt);
		uicc->uicc_ctxt = NULL;
	}

	xfunc_out();
}

