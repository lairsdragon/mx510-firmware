#if !defined(ODM_DRMD_H_20100416) && defined(CONFIG_OMA_DM_CLIENT) && defined(CONFIG_OMA_DM_DRMD)
#define ODM_DRMD_H_20100416

#include "global.h"
#include "odm.h"
#include "wimax.h"

#define URI_WiMAX_Diagnostics "./WiMAX_Diagnostics"

#define NOT_SUPPORTED_VALUE		(-1)
typedef struct drmd_harq_retry_s {
	u32 n1st_attempt;
	u32 n2nd_retries;
	u32 n3rd_retries;
	u32 n4th_retries;
	u32 total_retries;
	u32 bursts_dropped;
	
} drmd_harq_retry_t;

typedef struct drmd_data_rate_s {
	/*All data rates are to be presented scaled in kilobits per second,
	rounded to the nearest integer and measured for convergence sublayer SDUs.*/
	u32 curr_rate;				/*current rate*/
	u32 peak_rate;				/*peak rate*/
	u32 avrg_rate;				/*average rate*/
	u32 min_rsvd_rate;			/*min reserved traffic rate*/
	u32 max_sustained_rate;		/*max sustained traffic rate*/
	u32 msmnt_period;			/*measuremant period (ms)*/
	u32 ext_msmnt_period;		/*extended measurement period (ms)*/

} drmd_data_rate_t;

typedef enum mac_state_s {
	scanning,
	synchronization,
	network_entry,
	normal,
	handover,
	idle

} mac_state_t;

typedef struct drmd_thread_arg_s {
	int dev_idx;
	int duration;
} drmd_thread_arg_t;

typedef struct drmd_context_s {
	int duration;
	char originationId[255 + 1];
	char serverId[20 + 1];

	u32 timestamp_s, timestamp_e;

	mac_report_t mrpt;
	drmd_data_rate_t downlink;
	drmd_data_rate_t uplink;
	
} drmd_context_t;

typedef struct node_info_s {
	char *uri;
	node_format_t format;
	int (*drmd_set_value)(int , drmd_context_t *, void *);
	int (*drmd_get_value)(int , drmd_context_t *, void *);	
} node_info_t;

int dm_drmd_start(int dev_idx);
void dm_drmd_cancel(int dev_idx);

int drmd_Set_Duration(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_Duration(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Set_Server_ID(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_Server_ID(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Set_Origination_ID(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_Origination_ID(int dev_idx, drmd_context_t *drmd, void *p);

int drmd_Get_ServBSID(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_DownlinkFreq(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_DownlinkBandwidth(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_DownlinkMeanRSSI(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_DownlinkStdDevRSSI(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_DownlinkMeanCINR(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_DownlinkStdDevCINR(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_TxPwr(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_TxHeadroomPwr(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_ScannedBaseStations(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_LinkUptime(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_HARQRetTX(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_HARQRetRX(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_InitRangeResp(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_InitRangeNoResp(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_PerRangeResp(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_PerRangeNoResp(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_HOSuccess(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_HOFail(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_MAPRecSuccess(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_MAPRecFail(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_MCSstatsDL(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_MCSstatsUL(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_VersionOfMCSMetric(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_DownlinkDataRate(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_UplinkDataRate(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_PacketsReceived(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_PacketsSent(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_MACState(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_PreambleIndex(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_HOLatency(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_FrameRatio(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_HOAttempts(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_NetworkEntryLatency(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_NetworkEntrySucceses(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_NetworkEntryFailures(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_NetworkEntryAttempts(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_UserAccessTime(int dev_idx, drmd_context_t *drmd, void *p);

int drmd_Get_Generic_RateLimiterStats(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_Generic_TimeActive(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_Generic_TimeIdle(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_Generic_TimeSleep(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_Generic_LastRebootCause(int dev_idx, drmd_context_t *drmd, void *p);
int drmd_Get_Generic_DeviceTemp(int dev_idx, drmd_context_t *drmd, void *p);

int dm_drmd_ind_connection(int dev_idx, bool connected);
int dm_drmd_add_node(int dev_idx);
int dm_drmd_get_node(int dev_idx, char *uri, void *data, int *plen);
int dm_drmd_set_node(int dev_idx, char *uri, void *data, int len);
#endif
