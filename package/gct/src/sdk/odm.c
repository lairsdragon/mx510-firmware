#define _GNU_SOURCE
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdarg.h>
#include <assert.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "gcttype.h"
#include "error.h"
#include "device.h"
#include "wimax.h"
#include "hci.h"
#include "fload.h"
#include "log.h"
#include "eap.h"
#include "./odm/lib/dmc.h"
#include "./odm/lib/kdm_error.h"
#include "odm.h"
#include "odm_drmd.h"
#include "encryption.h"
#include "sdk.h"

#if defined(DMC_NOT_SUPPORTED_CLENT_NOTI_START)
static void ODM_CBNotifyStart(int dev_idx, int status);
#endif

#if defined(ODM_XML_DBG)
#define ODM_XML_SDK_TO_DMC			"./sdk2dmc.xml"
#define ODM_XML_DMC_TO_SDK			"./dmc2sdk.xml"
#endif

#define ODM_WIB_FIRST_POLLING_MS		10*1000
#if defined(CONFIG_OMA_DM_OP_UQ)
#define ODM_WIB_CONTINUOUS_POLLING_MS	30*1000
#endif

#if defined(CONFIG_OMA_DM_OP_CLEARWIRE)
#define ODM_DISCONNECT_TIMER_MS			1*1000
#else
#define ODM_DISCONNECT_TIMER_MS			30*1000
#endif
#define ODM_IP_ACQUISITION_TIMER_MS		200
#define ODM_IP_ACQUISITION_TIMEOUT_MS	30*1000
#define ODM_DNS_WAIT_TIMER_MS			100

#define ODM_DEFAULT_NODE_LEN			256
#define ODM_DEFAULT_VALUE_LEN			256
#define ODM_MAX_URI_LEN					256

#define ODM_ERROR_NOT_XNODE				-101

typedef enum {
	DMC_STATE_IDLE,
	DMC_STATE_BUSY

} dmc_state_t;

static int ODM_ProvStart(int dev_idx);
int ODM_TriggerNormal(int dev_idx);
static int ODM_RunWIB(int dev_idx);
static void odm_disconnect_timer_callback(void *data);

static bool odm_parse_bool(char *value)
{
	if (!strcasecmp(value, "true") || !strcmp(value, "1"))
		return TRUE;
	else
		return FALSE;
}

#if defined(ODM_XML_DBG)
static void odm_write_file(const char *file, void *buf, int size)
{
	int fd, written;

	xfunc_in("buf=%x, size=%d", buf, size);

	if ((fd = open(file, O_CREAT|O_WRONLY|O_TRUNC, 0644)) > 0) {
		written = write(fd, buf, size);
		xprintf(SDK_FORCE, "%s has been written(%d)\n", file, written);
		close(fd);
	}
	xfunc_out("fd=%d", fd);
}
#endif

static const char *odm_fsm2str(fsm_event_t event)
{
	const char *str = "Unknown";

	switch ((int) event) {
		case MD_Start:
			str = STR(MD_Start);
			break;
		case MD_End:
			str = STR(MD_End);
			break;
		case CD_ConnectSuccess:
			str = STR(CD_ConnectSuccess);
			break;
		case ND_IPAcquisition:
			str = STR(ND_IPAcquisition);
			break;
		case CD_Disconnect:
			str = STR(CD_Disconnect);
			break;
		case DD_SessionComplete:
			str = STR(DD_SessionComplete);
			break;
	}

	return str;
}

static char *odm_strerror(int _errno)
{
	char *err_str = "Unknown error";

	switch(_errno) {
		case DMC_ERROR_UNKNOWN :
			err_str = "General error";
			break;
		case DMC_ERROR_INIT_NOT :
			err_str = "DMC was not initiated";
			break;
		case DMC_ERROR_ALREADY :
			err_str = "Duplicate request";
			break;
		case DMC_ERROR_MEMORY :
			err_str = "Momory is not enough";
			break;
		case DMC_ERROR_PARAM_INVALID :
			err_str = "Invalid parameters";
			break;
		case DMC_ERROR_PARAM_NULL :
			err_str = "Parameters have 'NULL' value";
			break;
		case DMC_ERROR_SOCKET_INIT :
			err_str = "Socket initiated fail";
			break;
		case DMC_ERROR_BUSY :
			err_str = "Busy error";
			break;
		case DMC_ERROR_NOT_FOUND :
			err_str = "Not found error";
			break;
		case DMC_ERROR_NOT_TREE:
			err_str = "Not found tree data";
			break;
		case DMC_ERROR_SOCKET_BIND:
			err_str = "Socket bind failure";
			break;
		case DMC_ERROR_SOCKET_LISTEN:
			err_str = "Socket listen failure";
			break;
		case DMC_ERROR_TREE_OPEN:
			err_str = "Tree open fail";
			break;
	}
	
	return err_str;
}

static char *odm_status_string(int status)
{
	char *str = "Unknown Error";
	
	switch (status) {
		case DMC_STATUS_OK:
			str = "OK";
			break;
		case DMC_STATUS_ACCEPTED_FOR_PROCESSING:
			str = "Accepted for processing";
			break;
		case DMC_STATUS_AUTH_ACCEPT:
			str = "Auth accept";
			break;
		case DMC_STATUS_BAD_REQUEST:
			str = "Bad request";
			break;
		case DMC_STATUS_INVALID_CREDENTIAL:
			str = "Invalid credential";
			break;
		case DMC_STATUS_FORBIDDEN:
			str = "Forbidden";
			break;
		case DMC_STATUS_NOT_FOUND:
			str = "Not found";
			break;
		case DMC_STATUS_CMD_NOT_ALLOWED:
			str = "Command not allowed";
			break;
		case DMC_STATUS_MISSING_CREDENTIALS:
			str = "Missing credential";
			break;
		case DMC_STATUS_ALREADY_EXIST:
			str = "Already exist";
			break;
		case DMC_STATUS_PERMISSION_DENIED:
			str = "Permission denied";
			break;
	}

	return str;
}


static int ODM_NotifyODMErrorToHost(int dev_idx, ODM_ERROR nErr)
{
	static const char *ODMErrorStr[ODM_ERROR_END+1] = {
		"NONE",
		"INVALID_BOOTSTRAP_MESSAGE",
		"CANNOT_CONNECT_ODM_SERVER",
		"HTTP_ERROR",
		"HTTP_RESPONSE_TIMEOUT",
		"STATUS_ERROR",
		"DNS_QUERY_FAIL",
		"CANNOT_CONNECT_WIB_SERVER"
	};

	assert(nErr >= ODM_ERROR_START && nErr <= ODM_ERROR_END);
	xprintf(SDK_ERR, "Error number is %s(%d)\n", ODMErrorStr[nErr], nErr);
	sdk_ind_noti(dev_idx, GCT_API_NOTI_ODM_ERROR, GCT_API_NOTI_TYPE_CODE,
		(char *)&nErr, sizeof(nErr));
	return 0;
}

static int odm_cmd_status_2_xprint_mask(int cmd_status)
{
	int lmask;
	
	switch (cmd_status) {
		case 0:
		case DMC_STATUS_OK:
		case DMC_STATUS_INVALID_CREDENTIAL:
		case DMC_STATUS_MISSING_CREDENTIALS:
		case DMC_STATUS_ALREADY_EXIST:
		case DMC_STATUS_PERMISSION_DENIED:
			lmask = SDK_INFO;
			break;
		default:
			lmask = SDK_ERR;
	}
	return lmask;
}

static int ODM_AnalyzeError(int dev_idx, int error)
{
	device_t *dev;
	wm_oma_dm_t *odm;
	int m_status, c_status;
	int ret = 0;
	ODM_ERROR ret_err = ODM_ERROR_NONE;

	xfunc_in("err=0x%x", error);

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	odm = &dev->wimax->dev_info.oma_dm;

	if (error < 0) {
		xprintf(SDK_ERR, "Invalid error code=%d \n", error);
		ret = -1;
		goto out;
	}

	xprintf(SDK_DBG, "error=0x%x, bootstrap_state=%d\n", error, odm->bootstrap_state);
	switch((int) odm->bootstrap_state) {
		case BOOTSTRAP_STATE_ONGOING:
			switch(error) {
				case HDM_ERR_BOOT_BAD_MESSAGE:
				case HDM_ERR_TRG_PARSE_BOOT_MESSAGE_FAIL:
					ret_err = ODM_ERROR_INVALID_BOOTSTRAP_MESSAGE;
					break;
				// Below 2cases are called after RunWIB
				case HDM_ERR_HTTP_SERVER_ERROR:
					ret_err = ODM_ERROR_CANNOT_CONNECT_WIB_SERVER;
					break;
				case HDM_ERR_DNS_QUERY:
					ret_err = ODM_ERROR_DNS_QUERY_FAIL;
					break;
				default:
					goto out;
			}
			break;
		case BOOTSTRAP_STATE_SUCCESS:
			switch(error) {
				case HDM_ERR_NET_CONNECT:
					ret = sdk_get_status(NULL, dev_idx, &m_status, &c_status);
					if ((odm->d_status > D_LISTEN) && (m_status == M_CONNECTED))
						ret_err = ODM_ERROR_CANNOT_CONNECT_ODM_SERVER;
					break;
				case HDM_ERR_HTTP_ACCESE_DENIED:
				case HDM_ERR_HTTP_SERVER_ERROR:
				case HDM_ERR_HTTP_COMM:
				case 0x40d : // Just for testing by Joshua
					ret_err = ODM_ERROR_HTTP_ERROR;
					break;
				case HDM_ERR_HTTP_RESPONSE_TIME_OUT:
					ret_err = ODM_ERROR_HTTP_RESPONSE_TIMEOUT;
					break;
				default:
					goto out;
			}
			break;
	}

out:
	if (ret_err != ODM_ERROR_NONE)
		ODM_NotifyODMErrorToHost(dev_idx, ret_err);

	xfunc_out();
	return 0;
}

void ODM_SetEMSK(int dev_idx, char *szEMSK, int nEMSKLen)
{
	device_t *dev;
	wm_oma_dm_t *odm;

	xfunc_in("dev=%d", dev_idx);

	if (nEMSKLen != EMSK_SIZE) {
		xprintf(SDK_ERR, "EMSK size mismatch(%d!=%d)\n", EMSK_SIZE, nEMSKLen);
		return;
	}

	if (!(dev = dm_get_dev(dev_idx)))
		return;

	odm = &dev->wimax->dev_info.oma_dm;
	memcpy(odm->eap_emsk, szEMSK, EMSK_SIZE);
	xprintf_hex(SDK_DBG, "Set EMSK", odm->eap_emsk, EMSK_SIZE);

	dm_put_dev(dev_idx);
	xfunc_out();
}

static int odm_setup_emsk(int dev_idx)
{
	u8 buf[HCI_MAX_PACKET];
	tlv_t tlv;
	int ret;

	xfunc_in();

	tlv.T = TLV_T(T_EMSK);
	ret = hci_req_getinfo(dev_idx, buf, &tlv, 1);
	xprintf(SDK_DBG, "T_EMSK ret=%d, length=%d\n", ret, tlv.L);
	if (!ret)
		ODM_SetEMSK(dev_idx, (char *)tlv.V, tlv.L);
	
	xfunc_out("ret=%d, rf_stat=%d", ret, (0 == ret) ? *tlv.V : 0);
	return ret;
}

#define WARNING_WAIT_FOR_IDLE_MS		3*1000
#define BASE_WAIT_FOR_IDLE_MS			7*1000
#define LISTENER_WAIT_FOR_IDLE_MS		15*1000

static int odm_wait_for_idle(int dev_idx, int timeout_ms)
{
	int ret, stime, elapsed, usleep_time = 50*1000;
	bool warned = FALSE;

	stime = gettimemsofday();

	while (1) {
		ret = DMCGetStatus(dev_idx);
		if (ret < 0) {
			xprintf(SDK_ERR, "DMCGetStatus %s(%d)\n", odm_strerror(ret), ret);
			return ret;
		}
		else if (ret == DMC_STATE_IDLE)
			break;

		assert(ret == DMC_STATE_BUSY);

		elapsed = gettimemsofday() - stime;
		if (elapsed > timeout_ms) {
			xprintf(SDK_ERR, "Waiting is over: %d ms.\n", elapsed);
			return -ETIME;
		}
		if (elapsed > WARNING_WAIT_FOR_IDLE_MS)
			warned = TRUE;

		usleep(usleep_time);
	}

	elapsed = gettimemsofday() - stime;
	if (warned)
		xprintf(SDK_NOTICE, "Elapsed ODM wait-for-idle: %d ms.\n", elapsed);

	return elapsed;
}

int odm_get_operator_x(int dev_idx, const char *oper_name, char *oper_x)
{
	NodeList *node_list, *p_node_list = NULL;
	char uri[256], val[256];
	char *p_oper_x;
	int ret = 0;

	*oper_x = 0;
	node_list = p_node_list = NULL;

	xfunc_in("dev=%d", dev_idx);

	p_oper_x = "./WiMAXSupp/Operator/<X>";

restart:
	ret = DMCGetNodeList(dev_idx, p_oper_x, &p_node_list);
	if (ret < 0) {
		if (ret == DMC_ERROR_BUSY) {
			xprintf(SDK_NOTICE, "%s DMC_ERROR_BUSY\n", __FUNCTION__);
			if ((ret = odm_wait_for_idle(dev_idx, BASE_WAIT_FOR_IDLE_MS)) >= 0)
				goto restart;
		}
		xprintf(SDK_ERR, "DMCGetNodeList(operator:%s, uri:%s) error %s(%d)\n",
			oper_name, p_oper_x, odm_strerror(ret), ret);
		ret = -1;
	}
	else {
		ret = -1;
		node_list = p_node_list;
		while (node_list) {
			p_oper_x = strrchr(node_list->URI, '/') + 1/*'/'*/;
			ret = sizeof(val);
			sprintf(uri, "./WiMAXSupp/Operator/%s/NetworkParameters/OperatorName",
				p_oper_x);
			if (!ODM_GetNodeValue(dev_idx, uri, val, &ret)) {
				if (!strcmp(val, oper_name)) {
					strcpy(oper_x, p_oper_x);
					ret = 0;
					break;
				}
			}
			node_list = node_list->next;
		}
		DMCFreeNodeList(dev_idx, p_node_list);
		if (!ret)
			xprintf(SDK_DBG, "Operator(%s)'s <x>=%s\n", oper_name, oper_x);
		else {
			xprintf(SDK_ERR, "Not found operator(%s)'s <x>\n", oper_name);
			ret = -1;
		}
	}

	xfunc_out("ret=%d", ret);
	return ret;
}

int odm_get_xnode_node(int dev_idx, char *uri, int index, char *node)
{
	NodeList *node_list = NULL, *p_node_list = NULL;
	int ret = 0, idx = 0, tmp;

	xfunc_in("dev=%d, uri=%s, index=%d", dev_idx, uri, index);

	if (!strstr(uri, "<X>")) {
		xprintf(SDK_ERR, "%s is not included <X>\n", uri);
		ret = -1;
		goto out;
	}

restart:
	ret = DMCGetNodeList(dev_idx, uri, &p_node_list);
	xprintf(SDK_DBG, "DMCGetNodeList: ret=%d, node_list=0x%08x\n", ret, (int)p_node_list);
	if (ret < 0) {
		if (ret == DMC_ERROR_BUSY) {
			xprintf(SDK_NOTICE, "%s DMC_ERROR_BUSY\n", __FUNCTION__);
			if ((ret = odm_wait_for_idle(dev_idx, BASE_WAIT_FOR_IDLE_MS)) >= 0)
				goto restart;
		}
		xprintf(SDK_ERR, "DMCGetNodeList(uri:%s) error %s(%d)\n",
			uri, odm_strerror(ret), ret);
		ret = -1;
	}
	else {
		ret = DMC_ERROR_NOT_FOUND;
		node_list = p_node_list;
		while (node_list) {
			xprintf(SDK_DBG, "node_list->URI=%s\n", node_list->URI);
			if (idx == index) {
				strcpy(node, node_list->URI);
				ret = 0;
				break;
			}
			node_list = node_list->next;
			idx++;
		}
		if ((tmp = DMCFreeNodeList(dev_idx, p_node_list)) < 0) {
			xprintf(SDK_ERR, "DMCFreeNodeList=%d\n", tmp);
			ret = -1;
		}
	}
out:
	xfunc_out("ret=%d", ret);
	return ret;
}

int odm_get_xnode_value(int dev_idx, char *uri, int index, char *val)
{
	NodeList *node_list = NULL, *p_node_list = NULL;
	int ret = 0, idx = 0, tmp, lmask = SDK_DBG;

	xfunc_in("dev=%d, uri=%s, index=%d", dev_idx, uri, index);

	if (!strstr(uri, "<X>")) {
		xprintf(SDK_ERR, "%s is not included <X>\n", uri);
		ret = -1;
		goto out;
	}

	*val = 0;
restart:
	ret = DMCGetNodeList(dev_idx, uri, &p_node_list);
	xprintf(SDK_DBG, "DMCGetNodeList: ret=%d, node_list=0x%08x\n", ret, (int)p_node_list);
	if (ret < 0) {
		if (ret == DMC_ERROR_BUSY) {
			xprintf(SDK_NOTICE, "%s DMC_ERROR_BUSY\n", __FUNCTION__);
			if ((ret = odm_wait_for_idle(dev_idx, BASE_WAIT_FOR_IDLE_MS)) >= 0)
				goto restart;
		}
		else if (!(ret == DMC_ERROR_NOT_FOUND)) {
			lmask = SDK_ERR;
			ret = -1;
		}
		xprintf(lmask, "DMCGetNodeList(uri:%s) error %s(%d)\n",
			uri, odm_strerror(ret), ret);
	}
	else if (p_node_list == NULL)
		ret = DMC_ERROR_NOT_FOUND;
	else {
		ret = -1;
		node_list = p_node_list;
		while (node_list) {
			if (idx == index) {
				ret = ODM_DEFAULT_VALUE_LEN;
				if (!ODM_GetNodeValue(dev_idx, node_list->URI, val, &ret)) {
					xprintf(SDK_DBG, "%s=%s\n", node_list->URI, val);
					ret = 0;
				}
				else
					ret = -1;
				break;
			}
			node_list = node_list->next;
			idx++;
		}
		if ((tmp = DMCFreeNodeList(dev_idx, p_node_list)) < 0) {
			xprintf(SDK_ERR, "DMCFreeNodeList=%d\n", tmp);
			ret = -1;
		}
	}
out:
	xfunc_out("ret=%d", ret);
	return ret;
}

int ODM_GetNodeValue(int dev_idx, const char *uri, void *val, int *val_len)
{
	int ret = 0;

	xfunc_in("dev=%d", dev_idx);

restart:
	ret = DMCGetNodeValue(dev_idx, (char *)uri, val, val_len);
	if (ret < 0) {
		if (ret == DMC_ERROR_BUSY) {
			xprintf(SDK_NOTICE, "%s DMC_ERROR_BUSY\n", __FUNCTION__);
			if ((ret = odm_wait_for_idle(dev_idx, BASE_WAIT_FOR_IDLE_MS)) >= 0)
				goto restart;
		}
		else if (ret != DMC_ERROR_NOT_FOUND) {
			xprintf(SDK_ERR, "DMCGetNodeValue(uri:%s) error %s(%d)\n",
				uri, odm_strerror(ret), ret);
			ret = -1;
		}
	}
	else {
		*val_len = ret;
		((char *)val)[ret] = 0;
		xprintf(SDK_DBG, "uri=%s, val_len=%d\n", uri, *val_len);
		ret = 0;
	}

	xfunc_out("ret=%d", ret);
	return ret;
}

int ODM_SetNodeValue(int dev_idx, const char *uri, void *val, int val_len)
{
	int ret = 0;

	xfunc_in("dev=%d", dev_idx);

restart:
	ret = DMCSetNodeValue(dev_idx, (char *)uri, (void *)val, val_len);
	if (ret < 0) {
		if (ret == DMC_ERROR_BUSY) {
			xprintf(SDK_NOTICE, "%s DMC_ERROR_BUSY\n", __FUNCTION__);
			if ((ret = odm_wait_for_idle(dev_idx, BASE_WAIT_FOR_IDLE_MS)) >= 0)
				goto restart;
		}
		xprintf(SDK_ERR, "DMCSetNodeValue(uri:%s) error %s(%d)\n",
			uri, odm_strerror(ret), ret);
		ret = -1;
	}
	else {
		xprintf(SDK_DBG, "uri=%s, val_len=%d\n", uri, val_len);
		ret = 0;
	}

	xfunc_out("ret=%d", ret);
	return ret;
}

static int ODM_ChangePollingThreadInterval(int dev_idx, int interval_ms)
{
	device_t *dev;
	wimax_t *wm;
	wm_oma_dm_t *odm;
	int ret = 0, lmask = SDK_DBG;

	xfunc_in("dev=%d, interval_ms=%d", dev_idx, interval_ms);

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	wm = dev->wimax;
	odm = &wm->dev_info.oma_dm;

	if (interval_ms == 0) {
		xprintf(SDK_DBG, "STOP PollingInterval Timer\n");
		stop_timer(&odm->bootstrap_timer);
	}
	else if (interval_ms < 0) {
		xprintf(lmask, "%u. PollingInterval Timer %d\n", odm->polling_count, interval_ms);
		if (odm->bootstrap_state == BOOTSTRAP_STATE_FAIL)
			ret = ODM_RunWIB(dev_idx);
		else if (odm->bootstrap_state == BOOTSTRAP_STATE_SUCCESS)
			ret = ODM_TriggerNormal(dev_idx);
	}
	else {
		xprintf(lmask, "%u. PollingInterval Timer %d sec.(%d ms)\n",
			odm->polling_count, interval_ms/1000, interval_ms);
		start_timer(&odm->bootstrap_timer, interval_ms);
	}
	odm->polling_count++;

	dm_put_dev(dev_idx);
	xfunc_out("ret=%d", ret);
	return ret;
}

static int ODM_SetBEKParam(int dev_idx, char *emsk, int emsk_len)
{
#define BEK_HMAC_SHA256_PARAM	"bek@wimaxforum.org"
	xfunc_in();
	assert(emsk_len == EMSK_SIZE);

	DMCSetBEKParam(dev_idx, emsk, emsk_len, BEK_HMAC_SHA256_PARAM);
	xfunc_out();
	return 0;
}

static int ODM_StartListener(int dev_idx, u32 ip_addr, int port)
{
	device_t *dev;
	wimax_t *wm;
	wm_oma_dm_t *odm;
	int ret = -1, stime, stime2;

	xfunc_in("dev=%d", dev_idx);

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	wm = dev->wimax;
	odm = &wm->dev_info.oma_dm;

	if (odm->listener_started) {
		xprintf(SDK_INFO, "ODM_StartListener has been activated already!\n");
		goto out;
	}

	stime = gettimemsofday();
restart:
	stime2 = gettimemsofday();
	ret = DMCStartListener(dev_idx, SOCKET_TYPE_UDP, ip_addr, port);
	xprintf(SDK_DBG, "DMCStartListener: elapsed-ms=%d\n", gettimemsofday()-stime2);
	if (ret < 0) {
		if (ret == DMC_ERROR_BUSY) {
			xprintf(SDK_NOTICE, "%s DMC_ERROR_BUSY\n", __FUNCTION__);
			if ((ret = odm_wait_for_idle(dev_idx, BASE_WAIT_FOR_IDLE_MS)) >= 0)
				goto restart;
		}
		xprintf(SDK_ERR, "DMCStartListener error %s(%d)\n", odm_strerror(ret), ret);
		ret = -1;
	}
	else {
		ret = 0;
		odm->listener_started = TRUE;
	}
	xprintf(SDK_DBG, "ODM_StartListener: elapsed-ms=%d\n", gettimemsofday()-stime);
out:
	xfunc_out("ret=%d", ret);
	dm_put_dev(dev_idx);
	return ret;
}

static int ODM_StopListener(int dev_idx)
{
	device_t *dev;
	wimax_t *wm;
	wm_oma_dm_t *odm;
	int ret = -1, stime, stime2;

	xfunc_in("dev=%d", dev_idx);

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	wm = dev->wimax;
	odm = &wm->dev_info.oma_dm;

	if (!odm->listener_started) {
		xprintf(SDK_INFO, "ODM_StopListener has not been activated!\n");
		goto out;
	}

	stime = gettimemsofday();
restart:
	xprintf(SDK_DBG, "+DMCStopListener\n");
	stime2 = gettimemsofday();
	ret = DMCStopListener(dev_idx);
	xprintf(SDK_DBG, "-DMCStopListener: elapsed-ms=%d\n", gettimemsofday()-stime2);
	if (ret < 0) {
		if (ret == DMC_ERROR_BUSY) {
			xprintf(SDK_NOTICE, "%s DMC_ERROR_BUSY\n", __FUNCTION__);
			if ((ret = odm_wait_for_idle(dev_idx, BASE_WAIT_FOR_IDLE_MS)) >= 0)
				goto restart;
		}
		xprintf(SDK_ERR, "DMCStopListener error %s(%d)\n", odm_strerror(ret), ret);
		ret = -1;
	}
	else {
		ret = 0;
		odm->listener_started = FALSE;
	}
	xprintf(SDK_DBG, "ODM_StopListener: elapsed-ms=%d\n", gettimemsofday()-stime);
out:
	xfunc_out("ret=%d", ret);
	dm_put_dev(dev_idx);
	return ret;
}

static int ODM_SetXMLToDMC(int dev_idx, void *buf, int size)
{
	int ret, stime, elapsed;

	xfunc_in("dev=%d", dev_idx);

	#if defined(ODM_XML_DBG)
	odm_write_file(ODM_XML_SDK_TO_DMC, buf, size);
	#endif

restart:
	xprintf(SDK_DBG, "+DMCXMLFromSDKToOMADM\n");
	stime = gettimemsofday();
	ret = DMCXMLFromSDKToOMADM(dev_idx, buf, size);
	elapsed = gettimemsofday() - stime;
	xprintf(SDK_DBG, "-DMCXMLFromSDKToOMADM: ret=%d, size=%d, elapsed-ms=%d\n",
		ret, size, elapsed);
	if (ret < 0) {
		if (ret == DMC_ERROR_BUSY) {
			xprintf(SDK_NOTICE, "%s DMC_ERROR_BUSY\n", __FUNCTION__);
			if ((ret = odm_wait_for_idle(dev_idx, BASE_WAIT_FOR_IDLE_MS)) >= 0)
				goto restart;
		}
		xprintf(SDK_ERR, "DMCXMLFromSDKToOMADM error %s(%d)\n",
			odm_strerror(ret), ret);
		ret = -1;
	}
	else
		ret = 0;

	xfunc_out("ret=%d", ret);
	return ret;
}

static int ODM_GetXMLFromDMC(int dev_idx, char **buf, int *size)
{
	int ret, warning_cnt = 0, stime, elapsed;

	xfunc_in("dev=%d", dev_idx);

	*buf = NULL;
	*size = 0;

restart:
	xprintf(SDK_DBG, "+DMCXMLFromOMADMToSDK\n");
	stime = gettimemsofday();
	ret = DMCXMLFromOMADMToSDK(dev_idx, *buf, size);
	elapsed = gettimemsofday() - stime;
	xprintf(SDK_DBG, "-DMCXMLFromOMADMToSDK: ret=%d, size=%d, elapsed-ms=%d\n",
		ret, *size, elapsed);
	if (ret < 0) {
		if (ret == DMC_ERROR_MEMORY) {
			assert(*size);
			if (*buf)
				sdk_free(*buf);
			*buf = sdk_malloc(*size);
			xprintf(SDK_DBG, "DMC_ERROR_MEMORY: malloc size=%d\n", *size);
			goto restart;
		}
		else if (ret == DMC_ERROR_BUSY) {
			if (++warning_cnt > 5)
				xprintf(SDK_NOTICE, "%s: warning_cnt=%d\n", __FUNCTION__, warning_cnt);
			if ((ret = odm_wait_for_idle(dev_idx, BASE_WAIT_FOR_IDLE_MS)) >= 0)
				goto restart;
		}
		xprintf(SDK_ERR, "ODM_GetXMLFromDMC error %s(%d)\n",
			odm_strerror(ret), ret);
		ret = -1;
	}
	else {
		#if defined(ODM_XML_DBG)
		odm_write_file(ODM_XML_DMC_TO_SDK, *buf, *size);
		#endif
		ret = 0;
	}

	xfunc_out("ret=%d", ret);
	return ret;
}

static int ODM_AddExcludingList(int dev_idx, void *list)
{
	int ret = 0;

	xfunc_in("dev=%d", dev_idx);

restart:
	ret = DMCAddExcludingList(dev_idx, list);
	if (ret < 0) {
		if (ret == DMC_ERROR_BUSY) {
			xprintf(SDK_NOTICE, "%s DMC_ERROR_BUSY\n", __FUNCTION__);
			if ((ret = odm_wait_for_idle(dev_idx, BASE_WAIT_FOR_IDLE_MS)) >= 0)
				goto restart;
		}
		xprintf(SDK_ERR, "DMCAddExcludingList error %s(%d)\n",
			odm_strerror(ret), ret);
		ret = -1;
	}
	else
		ret = 0;

	xfunc_out("ret=%d", ret);
	return ret;
}

#if 0
static int ODM_SetSecureLayer(int dev_idx)
{
	SecureInfo si;
	int ret = 0;

	xfunc_in("dev=%d", dev_idx);

restart:
	ret = DMCSetSecureLayer(dev_idx, &si);
	if (ret < 0) {
		if (ret == DMC_ERROR_BUSY) {
			xprintf(SDK_NOTICE, "%s DMC_ERROR_BUSY\n", __FUNCTION__);
			if ((ret = odm_wait_for_idle(dev_idx, BASE_WAIT_FOR_IDLE_MS)) < 0)
				break;
		}
		xprintf(SDK_ERR, "DMCSetSecureLayer error %s(%d)\n",
			odm_strerror(ret), ret);
		ret = -1;
	}
	else
		ret = 0;

	xfunc_out("ret=%d", ret);
	return ret;
}
#endif

static int ODM_PopulateNode(int dev_idx)
{
	device_t *dev;
	wm_oma_dm_t *odm;
	u8 *mac;
	char mac_val[128];
	char *uri;
	char *val;
	int ret = -1;

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	xfunc_in();

	odm = &dev->wimax->dev_info.oma_dm;
	mac = dev->wimax->dev_info.device_mac;

	sprintf(mac_val, "MAC:%02X%02X%02X%02X%02X%02X",
		mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);

	uri = "./DevInfo/DevId";
	val = mac_val;
	if ((ret = ODM_SetNodeValue(dev_idx, uri, val, strlen(val))) < 0)
		goto out;

	#if defined(CONFIG_OMA_DM_OP_UQ)
	uri = "./DevDetail/SwV";
	val = "0.0.1";
	if ((ret = ODM_SetNodeValue(dev_idx, uri, val, strlen(val))) < 0)
		goto out;
	#endif

	uri = "./WiMAX/WiMAXRadioModule/0/MACAddress";
	val = mac_val;
	if ((ret = ODM_SetNodeValue(dev_idx, uri, val, strlen(val))) < 0)
		goto out;

	#if defined(CONFIG_OMA_DM_DRMD)
	dm_drmd_add_node(dev_idx);
	#endif // CONFIG_OMA_DM_DRMD

out:
	dm_put_dev(dev_idx);
	xfunc_out("ret=%d", ret);
	return ret;
}

void odm_stamp_dns_info_mtime(int dev_idx)
{
	device_t *dev;
	wm_oma_dm_t *odm;

	if ((dev = dm_get_dev(dev_idx))) {
		odm = &dev->wimax->dev_info.oma_dm;

		odm->resolv_conf_st_mtime = net_get_dns_info_mtime();
		dm_put_dev(dev_idx);
	}
}

static int odm_set_wib_param(int dev_idx)
{
	#define MAX_RETRY_CNT		10
	device_t *dev;
	wimax_t *wm;
	wm_oma_dm_t *odm;
	char url[128];
	dns_info_t dns_info;
	char dns_svr[MAX_DNS_SERVERS*MAX_NET_STR_LEN+MAX_DNS_SERVERS*2];
	char *svr_name, *protocol;
	int retry_cnt = 0;
	int ret = -1;

	xfunc_in();

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	wm = dev->wimax;
	odm = &wm->dev_info.oma_dm;

	#if 0
	assert(wm->scan.selected_subs);

	if (wm->scan.selected_subs->activated) {
		xprintf(SDK_DBG, "odm_set_wib_param already activated\n",
			wm->scan.selected_subs->subscription_id.hnspid.name);
		goto out;
	}
	#endif

	sprintf(url, "/bootstrap.wib?version=0&msid=%02X%02X%02X%02X%02X%02X&protocol={0}", 
		wm->dev_info.device_mac[0],
		wm->dev_info.device_mac[1],
		wm->dev_info.device_mac[2],
		wm->dev_info.device_mac[3],
		wm->dev_info.device_mac[4],
		wm->dev_info.device_mac[5]);

	memset(&dns_info, 0, sizeof(dns_info));

retry:
	usleep(ODM_DNS_WAIT_TIMER_MS*1000);
	if (odm->resolv_conf_st_mtime == net_get_dns_info_mtime()) {
		if (++retry_cnt >= MAX_RETRY_CNT) {
			xprintf(SDK_ERR, "Getting DNS Info timeout: retry=%d\n", retry_cnt);
			goto out;
		}
		xprintf(SDK_DBG, "DNS info was not changed: retry=%d\n", retry_cnt);
		goto retry;
	}
	if ((ret = net_get_dns_info(&dns_info)) < 0) {
		if (ret == -ENOENT)
			goto retry;
		xprintf(SDK_ERR, "Getting DNS Info error: retry_cnt=%d\n", retry_cnt);
		goto out;
	}
	
	strcpy(dns_svr, dns_info.dns_svr[0]);

	#if defined(USE_SECONDARY_DNS)
	if (dns_info.dns_svr[1][0])
		sprintf(dns_svr+strlen(dns_svr), ",%s", dns_info.dns_svr[1]);
	#endif

	xprintf(SDK_INFO, "DMCSetWIBParam dns(%s) domain(%s) uri(%s)\n",
		dns_svr, dns_info.domain, url);

	svr_name = "_wimax-bootstrap";
	protocol = "_tcp";
	DMCSetWIBParam(dev_idx, dns_svr, svr_name, protocol,
		dns_info.domain, url, "application/vnd.wmf.bootstrap");

out:
	dm_put_dev(dev_idx);
	xfunc_out("ret=%d", ret);
	return 0;
}

static int ODM_RunWIB(int dev_idx)
{
	device_t *dev;
	wimax_t *wm;
	wm_oma_dm_t *odm;
	int ret = -1;

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	xfunc_in();

	wm = dev->wimax;
	odm = &wm->dev_info.oma_dm;

	if (odm->d_status != D_IPACQUISITION) {
		xprintf(SDK_ERR, "Invalid dm fsm state=%d \n", odm->d_status);
		ret = -1;
		goto out;
	}

restart:
	ret = DMCRunWIB(dev_idx);
	if (ret < 0) {
		if (ret == DMC_ERROR_BUSY) {
			xprintf(SDK_NOTICE, "%s DMC_ERROR_BUSY\n", __FUNCTION__);
			if ((ret = odm_wait_for_idle(dev_idx, BASE_WAIT_FOR_IDLE_MS)) < 0)
				goto restart;
		}
		xprintf(SDK_ERR, "DMCRunWIB error %s(%d)\n", odm_strerror(ret), ret);
		ret = -1;
	}
	else {
		static int bootstrap_try_cnt;
		odm->bootstrap_state = BOOTSTRAP_STATE_ONGOING;
		ret = 0;
		xprintf(SDK_DBG, "Run WIB: tried count=%d\n", ++bootstrap_try_cnt);
	}
out:
	xfunc_out("ret=%d", ret);
	dm_put_dev(dev_idx);
	return ret;
}

#define FUMO_REPORT_FLAG_FILE	"should_report_fumo"
static void active_fumo_reporting(int dev_idx)
{
	char file[256];
	sprintf(file, "%s/%s", sdk_mng.nonvolatile_dir, FUMO_REPORT_FLAG_FILE);
	sdk_creat_file(file, NULL, 0);
}

static bool should_report_fumo(int dev_idx)
{
	char file[256];
	sprintf(file, "%s/%s", sdk_mng.nonvolatile_dir, FUMO_REPORT_FLAG_FILE);
	if (!access(file, 0))
		return TRUE;
	else
		return FALSE;
}

static void clear_report_fumo(int dev_idx)
{
	char file[256];
	sprintf(file, "%s/%s", sdk_mng.nonvolatile_dir, FUMO_REPORT_FLAG_FILE);
	unlink(file);
}

int ODM_TriggerNormal(int dev_idx)
{
	device_t *dev;
	wimax_t *wm;
	wm_oma_dm_t *odm;
	int m_status, c_status;
	int ret = -1;

	if (should_report_fumo(dev_idx)) {
		xprintf(SDK_NOTICE, "CM should report FUMO result\n");
		return 0;
	}

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	xfunc_in();

	wm = dev->wimax;
	odm = &wm->dev_info.oma_dm;

	ret = sdk_get_status(NULL, dev_idx, &m_status, &c_status);
	if ((m_status != M_CONNECTED) || (odm->d_status != D_IPACQUISITION)) {
		xprintf(SDK_ERR, "Invalid main fsm state(%d) or dm fsm state(%d)\n", 
			m_status, odm->d_status);
		goto out;
	}

	ret = DMCTriggerNormal(dev_idx);
	xprintf(SDK_DBG, "DMCTriggerNormal(%s)=%d\n", get_cur_time(), ret);
	if (ret < 0) {
		if (ret == DMC_ERROR_BUSY) {
			xprintf(SDK_ERR, "DMCTriggerNormal DMC_ERROR_BUSY\n");
			ret = 0;
		}
		else {
			xprintf(SDK_ERR, "DMCTriggerNormal error %s(%d)\n", odm_strerror(ret), ret);
			ret = -1;
		}
	}
	#if defined(DMC_NOT_SUPPORTED_CLENT_NOTI_START)
	else
		ODM_CBNotifyStart(dev_idx, 2/*ODM_NOTI_CLIENT_NOTI_START*/);
	#endif

out:
	dm_put_dev(dev_idx);
	xfunc_out("ret=%d", ret);
	return ret;
}

int odm_trig_fw_update_result(int dev_idx, char *uri, char *correlator, int result)
{
	int ret = 0;

	xfunc_in("dev=%d", dev_idx);

restart:
	ret = DMCTriggerFwUpdateResult(dev_idx, uri, correlator, result);
	if (ret < 0) {
		if (ret == DMC_ERROR_BUSY) {
			xprintf(SDK_NOTICE, "%s DMC_ERROR_BUSY(%s)\n", __FUNCTION__, uri);
			if ((ret = odm_wait_for_idle(dev_idx, BASE_WAIT_FOR_IDLE_MS)) < 0)
				goto restart;
		}
		xprintf(SDK_ERR, "DMCTriggerFwUpdateResult(uri:%s) error %s(%d)\n",
			uri, odm_strerror(ret), ret);
		ret = -1;
	}

	#if defined(DMC_NOT_SUPPORTED_CLENT_NOTI_START)
	if (ret == 0)
		ODM_CBNotifyStart(dev_idx, 2/*ODM_NOTI_CLIENT_NOTI_START*/);
	#endif

	clear_report_fumo(dev_idx);
	xfunc_out("ret=%d", ret);
	return ret;
}

int odm_trig_fw_dl_continue(int dev_idx)
{
	int ret = 0;

	xfunc_in("dev=%d", dev_idx);

	ret = DMCTriggerFwDlContinue(dev_idx);
	if (ret == DMC_ERROR_BUSY)
		ret = -ERR_BUSY;
	else if (ret < 0) {
		xprintf(SDK_ERR, "DMCTriggerFwDlContinue error %s(%d)\n",
			odm_strerror(ret), ret);
		ret = -1;
	}
	else
		ret = 0;

	#if defined(DMC_NOT_SUPPORTED_CLENT_NOTI_START)
	if (ret == 0)
		ODM_CBNotifyStart(dev_idx, 2/*ODM_NOTI_CLIENT_NOTI_START*/);
	#endif

	xfunc_out("ret=%d", ret);
	return ret;
}

static void logging_xml(int dev_idx)
{
	device_t *dev;
	wm_subscription_info_t	*ss;
	char node_val[128];
	int node_val_size, ret;
	char uri[256], str[256];
	char *client_x = "0";
	char *server_x = "1";
	#if defined(ODM_DBG)
	int lmask = SDK_NOTICE;
	#else
	int lmask = SDK_DBG;
	#endif

	xfunc_in();

	if (!(dev = dm_get_dev(dev_idx)))
		return;

	if (!(ss = dev->wimax->scan.selected_subs)) {
		xprintf(SDK_NOTICE, "Selected Subscription is NULL\n");
		return;
	}

	sprintf(uri, "./DMAcc/%s/AppAuth/%s/AAuthData", ss->subscription_id.oper_x, client_x);
	node_val_size = sizeof(node_val);
	ret = ODM_GetNodeValue(dev_idx, uri, node_val, &node_val_size);
	sprintf(str, "Get Node: %s, ret=%d, len=%d", uri, ret, node_val_size);
	xprintf_hex(lmask, str, node_val, node_val_size);
	
	sprintf(uri, "./DMAcc/%s/AppAuth/%s/AAuthData", ss->subscription_id.oper_x, server_x);
	node_val_size = sizeof(node_val);
	ret = ODM_GetNodeValue(dev_idx, uri, node_val, &node_val_size);
	sprintf(str, "Get Node: %s, ret=%d, len=%d", uri, ret, node_val_size);
	xprintf_hex(lmask, str, node_val, node_val_size);
	
	#if 0
	sprintf(uri, "./FUMO/DownloadAndUpdate/PkgURL");
	node_val_size = sizeof(node_val);
	ret = ODM_GetNodeValue(dev_idx, uri, node_val, &node_val_size);
	xprintf(lmask, "%s=[%s], ret=%d\n", uri, node_val, ret);
	xprintf(lmask, "%s=[%s], ret=%d\n", uri, node_val, ret);
	#endif

	#if 0
	strcpy(uri, "./DevInfo/DmV");
	node_val_size = sizeof(node_val);
	ret = ODM_GetNodeValue(dev_idx, uri, node_val, &node_val_size);
	xprintf(lmask, "%s=[%s], ret=%d\n", uri, node_val, ret);
	
	strcpy(uri, "./DevDetail/SwV");
	node_val_size = sizeof(node_val);
	ret = ODM_GetNodeValue(dev_idx, uri, node_val, &node_val_size);
	xprintf(lmask, "%s=[%s], ret=%d\n", uri, node_val, ret);
	#endif

	dm_put_dev(dev_idx);
	xfunc_out();
}

static int download_xml(int dev_idx)
{
	char *pBuf = NULL;
	int nBufSize = 0;
	int ret;

	xfunc_in();

	ret = ODM_GetXMLFromDMC(dev_idx, &pBuf, &nBufSize);
	if (ret < 0)
		goto out;

	if ((ret = bl_download(dev_idx, DLIMG_OMA_XML, pBuf, nBufSize, NULL)) < 0)
		goto out;
out:
	if (pBuf)
		sdk_free(pBuf);

	logging_xml(dev_idx);
	xfunc_out("ret=%d", ret);
	return ret;
}

static int ODM_CBExecURI(int dev_idx, char *szURI, unsigned char *szArg, int nLength)
{
	device_t *dev;
	wm_oma_dm_t *odm;
	int i, ret = DMC_STATUS_OK;
	struct {
		char *node;
		u32 flag;
	} const exec[] = {
		{"./WiMAXSupp/Operator/",		SESSION_EXEC_CONTACT},
		{"./WiMAX_Diagnostics/Start",	SESSION_EXEC_DRMD},
		{"/Download",					SESSION_EXEC_FUMO},
		{"/Update",						SESSION_EXEC_FUMO},
		{"/DownloadAndUpdate",			SESSION_EXEC_FUMO},
	};

	xfunc_in("dev=%d", dev_idx);

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	odm = &dev->wimax->dev_info.oma_dm;

	for (i = 0; i < numof_array(exec); i++) {
		if (strstr(szURI, exec[i].node)) {
			odm->session_flag |= exec[i].flag;
			break;
		}
	}

	#if defined(CONFIG_OMA_DM_OP_CLEARWIRE)
	if ((odm->session_flag & SESSION_EXEC_FUMO) == SESSION_EXEC_FUMO)
		ret = DMC_STATUS_ACCEPTED_FOR_PROCESSING;
	#endif

	#if defined(CONFIG_OMA_DM_DRMD)
	if ((odm->session_flag & SESSION_EXEC_DRMD) == SESSION_EXEC_DRMD) {
		if (0 == odm->drmd->duration || 3 == odm->drmd->duration || 4 == odm->drmd->duration) {
			xprintf(SDK_INFO, "Exec failed(500) because DRMD's duration is invalid value(%d)\n", odm->drmd->duration);
			ret = DMC_STATUS_COMMAND_FAILED;
		}
	}
	#endif

	xprintf(SDK_INFO, "Exec node: %s, session_flag=0x%x\n", szURI, odm->session_flag);

	xfunc_out();
	dm_put_dev(dev_idx);
	return ret;
}

static void print_session_flag(u32 flag)
{
	char buf[512], *p = buf;
	const int lmask = SDK_INFO;

	*p = 0;
	if ((flag & SESSION_XML_CHANGED) == SESSION_XML_CHANGED)
		p += sprintf(p, "\t%s\n", STR(SESSION_XML_CHANGED));
	if ((flag & SESSION_PROVISIONING) == SESSION_PROVISIONING)
		p += sprintf(p, "\t%s\n", STR(SESSION_PROVISIONING));
	if ((flag & SESSION_SERVER_NOTI) == SESSION_SERVER_NOTI)
		p += sprintf(p, "\t%s\n", STR(SESSION_SERVER_NOTI));
	if ((flag & SESSION_CLIENT_NOTI) == SESSION_CLIENT_NOTI)
		p += sprintf(p, "\t%s\n", STR(SESSION_CLIENT_NOTI));
	if ((flag & SESSION_BOOTSTRAP) == SESSION_BOOTSTRAP)
		p += sprintf(p, "\t%s\n", STR(SESSION_BOOTSTRAP));
	if ((flag & SESSION_EXEC_CONTACT) == SESSION_EXEC_CONTACT)
		p += sprintf(p, "\t%s\n", STR(SESSION_EXEC_CONTACT));
	if ((flag & SESSION_EXEC_FUMO) == SESSION_EXEC_FUMO)
		p += sprintf(p, "\t%s\n", STR(SESSION_EXEC_FUMO));
	if ((flag & SESSION_EXEC_DRMD) == SESSION_EXEC_DRMD)
		p += sprintf(p, "\t%s\n", STR(SESSION_EXEC_DRMD));
	if ((flag & SESSION_ERROR) == SESSION_ERROR)
		p += sprintf(p, "\t%s\n", STR(SESSION_ERROR));

	assert(p-buf < sizeof(buf));
	xprintf(lmask, "Current session\n%s",
		p==buf ? "\tEmpty\n" : buf);
}

static bool is_xml_changed(wm_oma_dm_t *odm)
{
	u32 flag = SESSION_XML_CHANGED;
	bool ret;

	if ((odm->session_flag & flag) == flag)
		ret = TRUE;
	else
		ret = FALSE;

	return ret;
}

static bool is_provisioning_session(wm_oma_dm_t *odm)
{
	u32 flag = SESSION_XML_CHANGED | SESSION_PROVISIONING;
	bool ret;

	if ((odm->session_flag & flag) == flag)
		ret = TRUE;
	else
		ret = FALSE;

	return ret;
}

static bool is_fumo_session(wm_oma_dm_t *odm)
{
	u32 flag = SESSION_EXEC_FUMO;
	bool ret;

	if ((odm->session_flag & flag) == flag)
		ret = TRUE;
	else
		ret = FALSE;

	return ret;
}

static bool should_download_xml(wm_oma_dm_t *odm)
{
	u32 ignore_flag = SESSION_EXEC_FUMO | SESSION_EXEC_DRMD;
	bool ret;

	#if defined(CHECK_LAST_PACKAGE_TO_STORE_NONCE)
	if (odm->last_package > 2/*If package is grater than 2, Nonce would be chainged.*/)
		ret = TRUE;
	else
	#endif
	if (is_provisioning_session(odm))
		ret = TRUE;
	else if (odm->session_flag & ignore_flag)
		ret = FALSE;
	else if (odm->session_flag & SESSION_XML_CHANGED)
		ret = TRUE;
	else
		ret = FALSE;

	return ret;
}

inline static void test_set_provisioning(int dev_idx, wm_oma_dm_t *odm,
	const char *cmd, NodeList *node_list)
{
	#define OP_NODE		"./WiMAXSupp/Operator/"
	const int op_node_len = sizeof(OP_NODE)-1;
	const char *op_end;

	if (!(odm->session_flag & SESSION_PROVISIONING)) {
		if (cmd[0] != 'A' || cmd[1] != 'd' || cmd[2] != 'd')
			return;
		while (node_list) {
			if (node_list->URI[2] == 'W' && node_list->URI[7] == 'S' &&
				!strncmp(OP_NODE, node_list->URI, op_node_len)) {
				op_end = strchr(node_list->URI+op_node_len, '/');
				if (!op_end/* || *(op_end+1) == 0*/) {
					odm->session_flag |= SESSION_PROVISIONING;
					ODM_ProvStart(dev_idx);
					break;
				}
			}
			node_list = node_list->next;
		}
	}
}

static int load_operator_pollinginterval(int dev_idx)
{
	device_t *dev;
	wimax_t *wm;
	wm_oma_dm_t *odm;
	char uri_buf[ODM_MAX_URI_LEN], *uri = uri_buf;
	char node_val[32];
	int node_size;
	int polling_atmpts;
	int ret = -1;
	int lmask = SDK_INFO;

	xfunc_in();

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	wm = dev->wimax;
	odm = &wm->dev_info.oma_dm;

	sprintf(uri, "./WiMAXSupp/Operator/%s/NetworkParameters/PollingAttempts",
		wm->scan.selected_subs->subscription_id.oper_x);
	node_size = sizeof(node_val);
	ret = ODM_GetNodeValue(dev_idx, uri, node_val, &node_size);
	if (!ret) {
		if ((polling_atmpts = atoi(node_val)) >= 0)
			odm->polling_attempts = polling_atmpts;
		else
			odm->polling_attempts = POLLING_ATTEMPT_INFINITY;
		xprintf(lmask, "Operator PollingAttempts=%d\n", odm->polling_attempts);
	}
	else if (ret != DMC_ERROR_NOT_FOUND)
		goto out;

	uri = uri_buf;
	sprintf(uri, "./WiMAXSupp/Operator/%s/NetworkParameters/PollingInterval",
		wm->scan.selected_subs->subscription_id.oper_x);
	node_size = sizeof(node_val);
	ret = ODM_GetNodeValue(dev_idx, uri, node_val, &node_size);
	if (!ret) {
		odm->polling_interval = atoi(node_val);
		xprintf(lmask, "Operator PollingInterval=%d\n", odm->polling_interval);
	}
out:
	dm_put_dev(dev_idx);
	xfunc_out("ret=%d", ret);
	return ret;
}

static int load_devcap_pollinginterval(int dev_idx)
{
	device_t *dev;
	wimax_t *wm;
	wm_oma_dm_t *odm;
	char uri_buf[ODM_MAX_URI_LEN], *uri = uri_buf;
	char node_val[32];
	int node_size;
	int polling_atmpts;
	int ret = -1;
	int lmask = SDK_INFO;

	xfunc_in();

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	wm = dev->wimax;
	odm = &wm->dev_info.oma_dm;

	uri = "./WiMAX/DevCap/UpdateMethods/ClientInitiated/PollingSupported";
	node_size = sizeof(node_val);
	ret = ODM_GetNodeValue(dev_idx, uri, node_val, &node_size);
	if (!ret) {
		if (odm_parse_bool(node_val))
			odm->polling_supported = TRUE;
		else
			odm->polling_supported = FALSE;
		xprintf(lmask, "DevCap PollingSupported=%d\n", odm->polling_supported);
	}
	
	uri = "./WiMAX/DevCap/UpdateMethods/ClientInitiated/PollingAttempts";
	node_size = sizeof(node_val);
	ret = ODM_GetNodeValue(dev_idx, uri, node_val, &node_size);
	if (!ret) {
		if ((polling_atmpts = strtol(node_val, NULL, 0)) >= 0)
			odm->polling_attempts = polling_atmpts;
		else
			odm->polling_attempts = POLLING_ATTEMPT_INFINITY;
		xprintf(lmask, "DevCap PollingAttempts=%d\n", odm->polling_attempts);
	}
	else if (ret != DMC_ERROR_NOT_FOUND)
		goto out;
	
	uri = "./WiMAX/DevCap/UpdateMethods/ClientInitiated/PollingInterval";
	node_size = sizeof(node_val);
	ret = ODM_GetNodeValue(dev_idx, uri, node_val, &node_size);
	if (!ret) {
		odm->polling_interval = strtol(node_val, NULL, 0);
		xprintf(lmask, "DevCap PollingInterval=%d\n", odm->polling_interval);
	}
	else if (ret != DMC_ERROR_NOT_FOUND)
		goto out;
out:
	dm_put_dev(dev_idx);
	xfunc_out("ret=%d", ret);
	return ret;
}

static int load_pollinginterval(int dev_idx)
{
	device_t *dev;
	wimax_t *wm;
	wm_oma_dm_t *odm;
	int ret = -1;

	xfunc_in();

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	wm = dev->wimax;
	if (!wm->scan.selected_subs) {
		xprintf(SDK_ERR, "Subscription was not set up\n");
		goto out;
	}

	odm = &wm->dev_info.oma_dm;
	odm->polling_supported = FALSE;
	odm->polling_attempts = POLLING_ATTEMPT_INFINITY;
	odm->polling_interval = POLLING_INTERVAL_DISABLE;

	if ((ret = load_devcap_pollinginterval(dev_idx)) < 0 && ret != DMC_ERROR_NOT_FOUND)
		goto out;

	xprintf(SDK_DBG, "odm->polling_attempts=%d\n", odm->polling_attempts);
	xprintf(SDK_DBG, "odm->polling_interval=%d\n", odm->polling_interval);

	#if (CW_DEVICE_CATEGORY==2)
	if (!wm->scan.selected_subs->activated) {
		if (!odm->polling_supported) {
			xprintf(SDK_NOTICE, "ClientInitiated is not supported\n");
			odm->polling_interval = POLLING_INTERVAL_DISABLE;
		}
		goto out;
	}
	#endif

	if (wm->scan.selected_subs->activated)
		ret = load_operator_pollinginterval(dev_idx);
out:
	if (ret == DMC_ERROR_NOT_FOUND)
		ret = 0;
	dm_put_dev(dev_idx);
	xfunc_out("ret=%d", ret);
	return ret;
}

static int issue_pollinginterval(int dev_idx, int lost_time_ms)
{
	device_t *dev;
	wm_oma_dm_t *odm;
	int polling_intv;
	int ret = 0;

	xfunc_in();

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	odm = &dev->wimax->dev_info.oma_dm;

	if (odm->polling_attempts <= 0) {
		xprintf(SDK_NOTICE, "PollingAttempts has been expired(%d)!\n",
			odm->polling_attempts);
		goto out;
	}
	xprintf(SDK_DBG, "session_flag=0x%x, polling_attempts=%d, polling_interval=%d\n",
		odm->session_flag, odm->polling_attempts, odm->polling_interval);
	odm->polling_attempts--;

	polling_intv = odm->polling_interval;
	if (polling_intv > 0 && odm->polling_attempts > 0) {
		polling_intv *= POLLING_INTERVAL_BASE_UNIT;
		polling_intv -= lost_time_ms;
		if (polling_intv < 100)
			polling_intv = 100;
		ret = ODM_ChangePollingThreadInterval(dev_idx, polling_intv);
	}
	else if (polling_intv == POLLING_INTERVAL_ONCE && !odm->session_flag)
		ret = ODM_ChangePollingThreadInterval(dev_idx, polling_intv);
	else
		ret = ODM_ChangePollingThreadInterval(dev_idx, 0);
out:
	dm_put_dev(dev_idx);
	xfunc_out("ret=%d", ret);
	return ret;
}

static int apply_provisioned_pollinginterval(int dev_idx)
{
	device_t *dev;
	wm_oma_dm_t *odm;
	int prev_polling_interval;
	int ret;

	xfunc_in();

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	odm = &dev->wimax->dev_info.oma_dm;
	prev_polling_interval = odm->polling_interval;

	if ((ret = load_pollinginterval(dev_idx)) >= 0) {
		xprintf(SDK_DBG, "prev_polling_interval=%d, polling_interval=%d\n",
			prev_polling_interval, odm->polling_interval);
		issue_pollinginterval(dev_idx, 0);
	}

	dm_put_dev(dev_idx);
	xfunc_out("ret=%d", ret);
	return ret;
}

static void ODM_CBNotifyExit(int dev_idx, int change, int status)
{
	device_t *dev;
	wimax_t *wm;
	wm_oma_dm_t *odm;
	int event = -1, lmask;

	if (!(dev = dm_get_dev(dev_idx)))
		return;

	xfunc_in("dev=%d, change=%d, status=0x%x", dev_idx, change, status);

	wm = dev->wimax;
	odm = &dev->wimax->dev_info.oma_dm;

	if (status) {
		ODM_AnalyzeError(dev_idx, status);

		if (odm->bootstrap_state == BOOTSTRAP_STATE_SUCCESS &&
			status == HDM_ERR_NET_CONNECT &&
			odm->d_status == D_LISTEN) {
			xprintf(SDK_DBG, "Trigger after obtain ip completely.\n");
			ODM_ChangePollingThreadInterval(dev_idx, 500);
		}
		#if defined(CONFIG_OMA_DM_OP_MOBILY) || defined(CONFIG_OMA_DM_OP_CLEARWIRE)
		else if (status == HDM_ERR_NOTIF_BAD_DIGEST) {
			xprintf(SDK_NOTICE, "NONCE mismatch: "
				"Client initiates a session to synchronize Nonce.\n");
			ODM_ChangePollingThreadInterval(dev_idx, 100);
		}
		#endif
		else if (odm->bootstrap_state == BOOTSTRAP_STATE_ONGOING)
			odm->bootstrap_state = BOOTSTRAP_STATE_FAIL;
		odm->session_flag |= SESSION_ERROR;
	}
	else {
		if ((odm->session_flag & SESSION_EXEC_CONTACT) == SESSION_EXEC_CONTACT) {
			int node_val_size;
			char node_val[256];
			char uri[256];
			int ret;

			assert(wm->scan.selected_subs);
			sprintf(uri, "./WiMAXSupp/Operator/%s/Contacts/%d/URI", 
				wm->scan.selected_subs->subscription_id.oper_x, 0);
			node_val_size = sizeof(node_val);
			ret = ODM_GetNodeValue(dev_idx, uri, node_val, &node_val_size);
			if (ret < 0)
				xprintf(SDK_ERR, "GetNodeValue Failure %s %d \n", uri, ret);
			else {
				char tmp[256+2];

				tmp[0] = GCT_API_ODM_NOTI_EXEC_REGISTRATION_PAGE_OPEN;
				memcpy(tmp+1, node_val, sizeof(node_val));
				sdk_ind_noti(dev_idx, GCT_API_NOTI_ODM_NOTI, GCT_API_NOTI_TYPE_CODE,
					tmp, 1+sizeof(node_val));
				xprintf(SDK_DBG, "Notify to open URL(%s)\n", node_val);
			}
		}
		#if defined(CONFIG_OMA_DM_DRMD)
		else if ((odm->session_flag & SESSION_EXEC_DRMD) == SESSION_EXEC_DRMD)
			dm_drmd_start(dev_idx);
		#endif

		if (change)
			odm->session_flag |= SESSION_XML_CHANGED;

		event = DD_SessionComplete;
	}

	print_session_flag(odm->session_flag);
	if (event >= 0)
		odm_send_msg(dev_idx, event);
	lmask = odm_cmd_status_2_xprint_mask(status);
	xprintf(lmask, "Notify Exit: change=%d, status=0x%x\n", change, status);

	dm_put_dev(dev_idx);
	xfunc_out();
	return;
}

static void ODM_CBNotifyStart(int dev_idx, int status)
{
	device_t *dev;
	wm_oma_dm_t *odm;
	char *str = "Unknown";
	enum {
		ODM_NOTI_SERVER_NOTI_START,
		ODM_NOTI_SERVER_BOOTSTRAP_START,
		ODM_NOTI_CLIENT_NOTI_START
	};

	if (!(dev = dm_get_dev(dev_idx)))
		return;

	xfunc_in("dev=%d, status=%d", dev_idx, status);

	odm = &dev->wimax->dev_info.oma_dm;
	odm->session_flag = 0;

	switch (status) {
		case ODM_NOTI_CLIENT_NOTI_START:
			str = "Client initiated session";
			odm->session_flag |= SESSION_CLIENT_NOTI;
			break;
		case ODM_NOTI_SERVER_NOTI_START:
			str = "Server initiated session";
			odm->session_flag |= SESSION_SERVER_NOTI;
			break;
		case ODM_NOTI_SERVER_BOOTSTRAP_START:
			odm->bootstrap_state = BOOTSTRAP_STATE_ONGOING;
			str = "Server bootstrap";
			odm->session_flag |= SESSION_BOOTSTRAP;
			break;
	}
	xprintf(SDK_DBG, "Notify Start: status=0x%x(%s)\n", status, str);
	xfunc_out();
	return;
}

static void ODM_CBNotifyError(int dev_idx, int nError)
{
	xfunc_in("dev=%d, error=%d", dev_idx, nError);

	ODM_AnalyzeError(dev_idx, nError);

	xfunc_out();
}

static void ODM_CBAES128CCMDecrypt(unsigned char *pBEK, unsigned char *pE,
	unsigned int nELen, unsigned char *pNonce, unsigned char *pD, unsigned int *pnDLen)
{
	int ret;

	xfunc_in("nELen=%d", nELen);
	xprintf_hex(SDK_DBG, "BEK", pBEK, 16);
	xprintf_hex(SDK_DBG, "Nonce", pNonce, 13);
	//xprintf_hex(SDK_DBG, "Bootstrap Data", pE, nELen);
	ret = OMA_DM_AES_128_CCM_Decrypt(pBEK, pE, nELen, pNonce, pD, pnDLen);
	xfunc_out("ret=%d", ret);
}

static void ODM_CBGetPin(int dev_idx, DMC_SECURITY_TYPE pinType,
	unsigned char *pBuf, int nBufLen, int *pnBufLen)
{
#define HMAC_KEY_LENGTH 7
	device_t *dev;
	wimax_t *wm;
	u8 szKey[HMAC_KEY_LENGTH];

	if (!(dev = dm_get_dev(dev_idx)))
		return;

	xfunc_in("dev=%d, pin-type=%d", dev_idx, pinType);

	wm = dev->wimax;

	if (pinType == DMC_SECURITY_NETWPIN) {
		memset(szKey, 0x00, sizeof(szKey));
		OMA_DM_HMAC_Key_Generation(wm->dev_info.device_mac, szKey);

		xprintf_mac(SDK_DBG, "PINSRC", wm->dev_info.device_mac);
		xprintf_hex(SDK_DBG, "HMAC Key", szKey, HMAC_KEY_LENGTH);

		memcpy(pBuf, szKey, HMAC_KEY_LENGTH);
		*pnBufLen = HMAC_KEY_LENGTH;
	}

	xfunc_out();
}

static void ODM_CBNotifyBootstrap(int dev_idx, int status)
{
	device_t *dev;
	wm_oma_dm_t *odm;

	xfunc_in("dev=%d, status=%d", dev_idx, status);
	if (!(dev = dm_get_dev(dev_idx)))
		return;

	odm = &dev->wimax->dev_info.oma_dm;

	if (odm->bootstrap_state == BOOTSTRAP_STATE_ONGOING) {
		if (status) {
			odm->bootstrap_state = BOOTSTRAP_STATE_SUCCESS;
			issue_pollinginterval(dev_idx, 0);
			xprintf(SDK_NOTICE, "BOOTSTRAP success: status=%d\n", status);
		}
		else {
			odm->bootstrap_state = BOOTSTRAP_STATE_FAIL;
			xprintf(SDK_ERR, "BOOTSTRAP error status=%d\n", status);
		}
	}

	if (odm->bootstrap_state != BOOTSTRAP_STATE_SUCCESS)
		xprintf(SDK_ERR, "BOOTSTRAP error bootstrap_state=%d, status=%d\n",
			odm->bootstrap_state, status);
	xfunc_out();
	return;
}

int ODM_CBBinaryEncrypt(int dev_idx, unsigned char *pSrc, unsigned int nSrcLen,
	unsigned char *pDst, unsigned int *pnDstLen)
{
	device_t *dev;
	wimax_t *wm;
	wm_oma_dm_t *odm;
	u8 szSrcTmp[GCT_KEY_ENC_UNIT];
	u8 szEncTmp[GCT_KEY_ENC_UNIT];
	unsigned pos;
	int tmp;
	int ret = -1;

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	xfunc_in("dev=%d, src-len=%d", dev_idx, nSrcLen);

	wm = dev->wimax;
	odm = &dev->wimax->dev_info.oma_dm;

	tmp = nSrcLen%GCT_KEY_ENC_UNIT;
	if (*pnDstLen < nSrcLen + tmp) {
		xprintf(SDK_ERR, "Invalid dst-len=%d need=%d \n", *pnDstLen, nSrcLen+tmp);
		*pnDstLen = nSrcLen + tmp;
		ret = DMC_ERROR_MEMORY;
		goto out;
	}

	pos = 0;
	while (pos < nSrcLen) {
		memset(szSrcTmp, 0x00, sizeof(GCT_KEY_ENC_UNIT));
		memset(szEncTmp, 0x00, sizeof(GCT_KEY_ENC_UNIT));
		if (nSrcLen-pos < GCT_KEY_ENC_UNIT)
			memcpy(szSrcTmp, pSrc+pos, nSrcLen-pos);
		else	
			memcpy(szSrcTmp, pSrc+pos, GCT_KEY_ENC_UNIT);

		GCT_Key_Enc(wm->dev_info.device_mac, szSrcTmp, szEncTmp);
		memcpy(pDst+pos, szEncTmp, GCT_KEY_ENC_UNIT);
		
		pos += GCT_KEY_ENC_UNIT;
	}
	*pnDstLen = pos;

out:
	xfunc_out("ret=%d", ret);
	dm_put_dev(dev_idx);
	ret = !ret ? 1 : 0;
	return ret;
}


int ODM_CBBinaryDecrypt(int dev_idx, unsigned char *src, unsigned int src_len,
	 unsigned char *dst, unsigned int *dst_len_ptr)
{
	device_t *dev;
	wimax_t *wm;
	wm_oma_dm_t *odm;
	u8 src_buf[GCT_KEY_ENC_UNIT];
	u8 dec_buf[GCT_KEY_ENC_UNIT];
	int pos, ret = 0;

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	xfunc_in("dev=%d, src-len=%d", dev_idx, src_len);

	wm = dev->wimax;
	odm = &dev->wimax->dev_info.oma_dm;

	if (*dst_len_ptr < src_len) {
		xprintf(SDK_ERR, "Invalid dst_len(%d) need(%d)\n", *dst_len_ptr, src_len);
		*dst_len_ptr = src_len;
		ret = DMC_ERROR_MEMORY;
		goto out;
	}

	pos = 0;
	while (pos < src_len) {
		memset(src_buf, 0x00, sizeof(GCT_KEY_ENC_UNIT));
		memset(dec_buf, 0x00, sizeof(GCT_KEY_ENC_UNIT));
		if (src_len-pos < GCT_KEY_ENC_UNIT)
			memcpy(src_buf, src+pos, src_len-pos);
		else	
			memcpy(src_buf, src+pos, GCT_KEY_ENC_UNIT);

		GCT_Key_Dec(wm->dev_info.device_mac, src_buf, dec_buf);
		memcpy(dst+pos, dec_buf, GCT_KEY_ENC_UNIT);
		
		pos += GCT_KEY_ENC_UNIT;
	}
	*dst_len_ptr = pos;

out:
	xfunc_out("ret=%d", ret);
	dm_put_dev(dev_idx);
	ret = !ret ? 1 : 0;
	return ret;
}

static void ODM_CBNotifyDMCmdStatus(int dev_idx, int package, char *cmd,
	NodeList *node_list, int status)
{
	device_t *dev;
	wimax_t *wm;
	wm_oma_dm_t *odm;
	int err_mask;

	if (!(dev = dm_get_dev(dev_idx)))
		return;

	xfunc_in("dev=%d, pkg=%d, cmd=%s, code=%d",
				dev_idx, package, cmd, status);

	wm = dev->wimax;
	odm = &dev->wimax->dev_info.oma_dm;

	#if defined(CHECK_LAST_PACKAGE_TO_STORE_NONCE)
	odm->last_package = package;
	#endif
	switch (status) {
		case DMC_STATUS_OK:
		case DMC_STATUS_ACCEPTED_FOR_PROCESSING:
		case DMC_STATUS_AUTH_ACCEPT:
			test_set_provisioning(dev_idx, odm, cmd, node_list);
			break;
		default:
			err_mask = odm_cmd_status_2_xprint_mask(status);
			xprintf(err_mask, "package=%d cmd=%s status=%s(%d)\n", 
				package, cmd, odm_status_string(status), status);
			if (err_mask == SDK_ERR) {
				while (node_list) {
					xprintf(SDK_NOTICE, "\t uri=%s\n", node_list->URI);
					node_list = node_list->next;
				}
			}
			break;
	}
	
	xfunc_out();
}

static void ODM_CBGetFwFilePath(int dev_id, char *path)
{
	sdk_internal_t *sdk = sdk_get_rw_handle();
	dev_hand_t dev_hand;

	xfunc_in("path=%s", path);

	if (sdk->ind.odm_get_fw_file_path) {
		dev_hand.api = sdk->api;
		dev_hand.dev_idx = dev_id;
		xprintf(SDK_DBG, "Call callback(%s)\n", __FUNCTION__);
		sdk->ind.odm_get_fw_file_path(&dev_hand, path);
	}
	
	xfunc_out();
}

static void ODM_CBGetFwFreeSpace(int dev_id, unsigned int *free_size)
{
	sdk_internal_t *sdk = sdk_get_rw_handle();
	dev_hand_t dev_hand;

	xfunc_in("free_size=%d", *free_size);

	if (sdk->ind.odm_get_fw_free_space) {
		dev_hand.api = sdk->api;
		dev_hand.dev_idx = dev_id;
		xprintf(SDK_DBG, "Call callback(%s)\n", __FUNCTION__);
		sdk->ind.odm_get_fw_free_space(&dev_hand, free_size);
	}
	
	xfunc_out("free_size=%d", *free_size);
}

static void ODM_CBGetFwSessionInf(int dev_id, char *szURI, char *correlator)
{
	sdk_internal_t *sdk = sdk_get_rw_handle();
	dev_hand_t dev_hand;

	xfunc_in("uir=%s, correlator=%s", szURI, correlator);

	if (sdk->ind.odm_get_fw_session_info) {
		dev_hand.api = sdk->api;
		dev_hand.dev_idx = dev_id;
		xprintf(SDK_DBG, "Call callback(%s)\n", __FUNCTION__);
		sdk->ind.odm_get_fw_session_info(&dev_hand, szURI, correlator);
	}

	xfunc_out("uir=%s, correlator=%s", szURI, correlator);
}

static void ODM_CBSetFwSessionInf(int dev_id, char *szURI, char *correlator)
{
	sdk_internal_t *sdk = sdk_get_rw_handle();
	dev_hand_t dev_hand;

	xfunc_in("uir=%s, correlator=%s", szURI, correlator);

	if (sdk->ind.odm_set_fw_session_info) {
		dev_hand.api = sdk->api;
		dev_hand.dev_idx = dev_id;
		xprintf(SDK_DBG, "Call callback(%s)\n", __FUNCTION__);
		sdk->ind.odm_set_fw_session_info(&dev_hand, szURI, correlator);
	}
	
	xfunc_out();
}

static void ODM_CBGetFwPkgURL(int dev_id, char *szUrlNode, char *szURL)
{
	sdk_internal_t *sdk = sdk_get_rw_handle();
	dev_hand_t dev_hand;

	xfunc_in("szUrlNode=%s, szURL=%s", szUrlNode, szURL);

	if (sdk->ind.odm_get_fw_pkg_url) {
		dev_hand.api = sdk->api;
		dev_hand.dev_idx = dev_id;
		xprintf(SDK_DBG, "Call callback(%s)\n", __FUNCTION__);
		sdk->ind.odm_get_fw_pkg_url(&dev_hand, szUrlNode, szURL);
	}

	xfunc_out("szUrlNode=%s, szURL=%s", szUrlNode, szURL);
}

static void ODM_CBSetFwPkgURL(int dev_id, char *szUrlNode, char *szURL)
{
	sdk_internal_t *sdk = sdk_get_rw_handle();
	dev_hand_t dev_hand;

	xfunc_in("szUrlNodeuir=%s, szURL=%s", szUrlNode, szURL);

	if (sdk->ind.odm_set_fw_pkg_url) {
		dev_hand.api = sdk->api;
		dev_hand.dev_idx = dev_id;
		xprintf(SDK_DBG, "Call callback(%s)\n", __FUNCTION__);
		sdk->ind.odm_set_fw_pkg_url(&dev_hand, szUrlNode, szURL);
	}
	
	xfunc_out();
}

static void ODM_CBSetFwDlProgress(int dev_id, unsigned int cur_size,
	unsigned int total_size)
{
	sdk_internal_t *sdk = sdk_get_rw_handle();
	dev_hand_t dev_hand;

	xfunc_in("current-size=%d, total-size=%d", cur_size, total_size);

	if (sdk->ind.odm_set_fw_dl_prog) {
		dev_hand.api = sdk->api;
		dev_hand.dev_idx = dev_id;
		xprintf(SDK_DBG, "Call callback(%s)\n", __FUNCTION__);
		sdk->ind.odm_set_fw_dl_prog(&dev_hand, cur_size, total_size);
	}
	
	xfunc_out();
}

static void ODM_CBSetFwDlResult(int dev_id, char *fw_pkg_filename, int success)
{
	sdk_internal_t *sdk = sdk_get_rw_handle();
	dev_hand_t dev_hand;

	xfunc_in("fw=%s, %s", fw_pkg_filename, success ? "success" : "failure");

	if (sdk->ind.odm_set_fw_dl_result) {
		dev_hand.api = sdk->api;
		dev_hand.dev_idx = dev_id;
		xprintf(SDK_DBG, "Call callback(%s)\n", __FUNCTION__);
		sdk->ind.odm_set_fw_dl_result(&dev_hand, fw_pkg_filename, success);
	}
	
	xfunc_out();
}

static void ODM_CBRunFwUpdate(int dev_id)
{
	sdk_internal_t *sdk = sdk_get_rw_handle();
	dev_hand_t dev_hand;

	xfunc_in();

	if (sdk->ind.odm_run_fw_update) {
		dev_hand.api = sdk->api;
		dev_hand.dev_idx = dev_id;
		xprintf(SDK_DBG, "Call callback(%s)\n", __FUNCTION__);
		sdk->ind.odm_run_fw_update(&dev_hand);
		active_fumo_reporting(dev_id);
	}
	
	xfunc_out();
}

#if defined(CONFIG_OMA_DM_DRMD)
int ODM_AddNode(int dev_idx, char *uri, char *format)
{
	int ret = 0;

	xfunc_in("dev=%d", dev_idx);

restart:
	ret = DMCAddNode(dev_idx, uri, format);
	if (ret < 0) {
		if (ret == DMC_ERROR_BUSY) {
			xprintf(SDK_NOTICE, "%s DMC_ERROR_BUSY\n", __FUNCTION__);
			if ((ret = odm_wait_for_idle(dev_idx, BASE_WAIT_FOR_IDLE_MS)) >= 0)
				goto restart;
		}
		xprintf(SDK_ERR, "DMCAddNode(uri:%s) error %s(%d)\n",
			uri, odm_strerror(ret), ret);
		ret = -1;
	}
	else {
		xprintf(SDK_DBG, "uri=%s, format=%s\n", uri, format);
		ret = 0;
	}

	xfunc_out("ret=%d", ret);
	return ret;
}

int ODM_AddNodeByType(int dev_idx, char *uri, node_format_t format)
{
	char *szFormat;

	switch (format) {
	case NODE_FORMAT_node:
		szFormat = "node";
		break;
	case NODE_FORMAT_int:
		szFormat = "int";
		break;
	case NODE_FORMAT_chr:
		szFormat = "chr";			
		break;
	case NODE_FORMAT_bool:
		szFormat = "bool";
		break;
	case NODE_FORMAT_float:
		szFormat = "float";			
		break;
	case NODE_FORMAT_b64:
		szFormat = "b64";			
		break;
	}

	return ODM_AddNode(dev_idx, uri, szFormat);
}

int odm_trig_drmd(int dev_idx)
{
	device_t *dev;
	drmd_context_t *drmd;

	int ret = 0;

	xfunc_in("dev=%d", dev_idx);

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	drmd = dev->wimax->dev_info.oma_dm.drmd;

	drmd->duration = 0;

	xprintf(SDK_DBG, "+pthread_join(DRMD)\n");
	pthread_join(dev->wimax->dev_info.oma_dm.drmd_thread, NULL);
	xprintf(SDK_DBG, "-pthread_join(DRMD)\n");
restart:
	ret = DMCTriggerDrmdReport(dev_idx, drmd->serverId);
	if (ret < 0) {
		if (ret == DMC_ERROR_BUSY) {
			xprintf(SDK_NOTICE, "%s DMC_ERROR_BUSY\n", __FUNCTION__);
			if ((ret = odm_wait_for_idle(dev_idx, BASE_WAIT_FOR_IDLE_MS)) < 0)
				goto restart;
		}
		xprintf(SDK_ERR, "DMCTriggerDrmdReport(serverId:%s) error %s(%d)\n",
			drmd->serverId, odm_strerror(ret), ret);
		ret = -1;
	}

#if defined(DMC_NOT_SUPPORTED_CLENT_NOTI_START)
	if (ret == 0)
		ODM_CBNotifyStart(dev_idx, 2/*ODM_NOTI_CLIENT_NOTI_START*/);
#endif

	dm_put_dev(dev_idx);
	xfunc_out("ret=%d", ret);
	return ret;
}

static int ODM_CBSetDrmdData(int dev_id, char *szURI, void *pData, int nBufLen)
{
	sdk_internal_t *sdk = sdk_get_rw_handle();
	dev_hand_t dev_hand;

	xfunc_in("uri=%s, data=%s, len=%d", szURI, pData ? (char*)pData : "", nBufLen);

	if (dm_drmd_set_node(dev_id, szURI, pData, nBufLen))
		if (sdk->ind.odm_set_drmd_node_data) {
			dev_hand.api = sdk->api;
			dev_hand.dev_idx = dev_id;
			xprintf(SDK_DBG, "Call callback(%s)\n", __FUNCTION__);
			sdk->ind.odm_set_drmd_node_data(&dev_hand, szURI, pData, nBufLen);
		}
	
	xfunc_out();
	return 0;
}

static int ODM_CBGetDrmdData(int dev_id, char *szURI, void *pBuffer, int *nBufLen)
{
	sdk_internal_t *sdk = sdk_get_rw_handle();
	dev_hand_t dev_hand;

	xfunc_in("uri=%s", szURI);

	if (dm_drmd_get_node(dev_id, szURI, pBuffer, nBufLen))
		if (sdk->ind.odm_get_drmd_node_data) {
			dev_hand.api = sdk->api;
			dev_hand.dev_idx = dev_id;
			xprintf(SDK_DBG, "Call callback(%s)\n", __FUNCTION__);
			sdk->ind.odm_get_drmd_node_data(&dev_hand, szURI, pBuffer, nBufLen);
		}
	
	xfunc_out();
	return 0;
}
#endif // CONFIG_OMA_DM_DRMD

static void ODM_RegCallBackFunc(void)
{
	xfunc_in();

	DMCRegExecURI(ODM_CBExecURI);
	DMCRegNotifyExit(ODM_CBNotifyExit);
	DMCRegNotifyStart(ODM_CBNotifyStart);
	DMCRegNotifyError(ODM_CBNotifyError);
	DMCRegAES128CCMDecrypt(ODM_CBAES128CCMDecrypt);
	DMCRegGetPin(ODM_CBGetPin);
	DMCRegNotifyDMCmdStatus(ODM_CBNotifyDMCmdStatus);
	DMCRegNotifyBootstrap(ODM_CBNotifyBootstrap);
	DMCRegFwGetFileInf(ODM_CBGetFwFilePath, ODM_CBGetFwFreeSpace);
	DMCRegFwSessionInf(ODM_CBGetFwSessionInf, ODM_CBSetFwSessionInf);
	DMCRegFwPkgUrl(ODM_CBGetFwPkgURL, ODM_CBSetFwPkgURL);
	DMCRegFwSetDlStatus(ODM_CBSetFwDlProgress, ODM_CBSetFwDlResult);
	DMCRegFwRunUpdate(ODM_CBRunFwUpdate);
	#if defined(CONFIG_OMA_DM_DRMD)
	DMCRegDrmdData(ODM_CBGetDrmdData, ODM_CBSetDrmdData);
	#endif // CONFIG_OMA_DM_DRMD

	xfunc_out();
}

static bool sdk_ind_provisioning_msg(int dev_idx, bool activated)
{
	device_t *dev;
	wimax_t *wm;
	wm_subscription_info_t *ss_info;
	char text[512], *p = text;
	int len;
	bool ret = FALSE;

	xfunc_in();

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	wm = dev->wimax;

	if (!(ss_info = wm->scan.selected_subs)) {
		xprintf(SDK_ERR, "Subscriptoin is NULL\n");
		goto out;
	}
 
	if (wm->dev_info.eapp.cr801_enabled) {
		if (activated &&
			 wm->dev_info.eapp.cr801_server_reject_cnt == CR801_SERVER_REJECT_LIMIT) {
			xprintf(SDK_INFO, "CR801 Notification!\n");
			#if defined(CONFIG_OMA_DM_OP_UQ)
			if (wm->dev_info.oma_dm.polling_interval != POLLING_INTERVAL_ONCE)
				goto next;
			#endif

			p += sprintf(p, "    %s:%s re-activated\n",
				ss_info->subscription_id.hnspid.name, ss_info->subscription_id.user_hnai);
		}
	}
#if defined(CONFIG_OMA_DM_OP_UQ)
next:
#endif
	
	p += sprintf(p, "    %s:%s has been added newly with %s.\n",
		ss_info->subscription_id.user_hnai,
		ss_info->subscription_id.hnspid.name,
		ss_info->activated ? "activated" : "deactivated");
	assert(p - text < sizeof(text));

	if ((len = p - text)) {	
		sdk_ind_noti(dev_idx, GCT_API_NOTI_ODM_NOTI, GCT_API_NOTI_TYPE_TEXT, 
			text, len);
		xprintf(SDK_INFO, "ODM NOTI\n%s", text);
	}
out:
	xfunc_out("ret=%d", ret);
	dm_put_dev(dev_idx);
	return ret;
}

static int ODM_ProvStart(int dev_idx)
{
	u8 contact_type = 0;

	sdk_ind_provisioning_op(dev_idx, WIMAX_API_PROV_OPERATION_CFG_UPDATE_STARTED,
		contact_type);

	return 0;
}

static void disconnect_after_sessoin(int dev_idx)
{
	device_t *dev;
	wimax_t *wm;
	wm_oma_dm_t *odm;

	if (!(dev = dm_get_dev(dev_idx)))
		return;

	wm = dev->wimax;
	odm = &wm->dev_info.oma_dm;

	init_timer(&odm->disconnect_timer, odm_disconnect_timer_callback, (void *)dev_idx);
	xprintf(SDK_DBG, "Start disconnect-timer(%d sec.)\n", ODM_DISCONNECT_TIMER_MS/1000);
	start_timer(&odm->disconnect_timer, ODM_DISCONNECT_TIMER_MS);

	dm_put_dev(dev_idx);
}

static int fumo_complete(int dev_idx)
{
	int ret = 0;

	xfunc_in();
	#if defined(CONFIG_OMA_DM_OP_CLEARWIRE)
	//disconnect_after_sessoin(dev_idx);	//for test
	#endif
	xfunc_out("ret=%d", ret);
	return ret;
}

static int ODM_ProvComplete(int dev_idx)
{
	device_t *dev;
	wimax_t *wm;
	wm_oma_dm_t *odm;
	bool prev_activated = FALSE;
	int ret = -1;
	u8 contact_type = 0;
	WIMAX_API_PROV_OPERATION operation = WIMAX_API_PROV_OPERATION_CFG_UPDATE_COMPLETED;

	xfunc_in();

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	wm = dev->wimax;
	odm = &wm->dev_info.oma_dm;

	if (wm->scan.selected_subs)
		prev_activated = wm->scan.selected_subs->activated;

	apply_provisioned_pollinginterval(dev_idx);

	if (!(ret = wm_sync_subscription(dev_idx)))
		sdk_ind_provisioning_msg(dev_idx, prev_activated);

	sdk_ind_provisioning_op(dev_idx, operation, contact_type);

	disconnect_after_sessoin(dev_idx);

	dm_put_dev(dev_idx);
	xfunc_out("ret=%d", ret);
	return ret;
}

static int load_eap_param(int dev_idx, char *oper_x,
	char *userid, char *userid_pass, char *anony_id)
{
	device_t *dev;
	wimax_t *wm;
	wm_eap_param_t *eapp;
	wm_oma_dm_t *odm;
	char szURI[ODM_MAX_URI_LEN];
	char eap_x[ODM_MAX_URI_LEN];
	char read_val[ODM_DEFAULT_VALUE_LEN];
	char szUserID[MAX_EAP_PARAM_SIZE/2];
	char szRealm[MAX_EAP_PARAM_SIZE/2];
	char szPassword[MAX_EAP_PARAM_SIZE/2];
	int nMethodType;
	#if 0
	int vendor_id = 0;
	#endif
	int ret = 0, anony_id_len = 0;
	#if defined(ODM_DBG)
	int lmask = SDK_NOTICE;
	#else
	int lmask = SDK_INFO;
	#endif

	xfunc_in("dev=%d, operator=%s", dev_idx, oper_x);

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	wm = dev->wimax;
	eapp = &wm->dev_info.eapp;
	odm = &wm->dev_info.oma_dm;

	sprintf(eap_x, "./WiMAXSupp/Operator/%s/SubscriptionParameters/Primary/EAP/<X>",
		oper_x);

	/*Get Method-Type*/
	sprintf(szURI, "%s/METHOD-TYPE", eap_x);
	ret = odm_get_xnode_value(dev_idx, szURI, 0, read_val);
	if (ret == DMC_ERROR_NOT_FOUND) {
		xprintf(SDK_ERR, "Not found node(%s)\n", szURI);
		goto out;
	}
	xprintf(lmask, "XML METHOD-TYPE='%s'\n", read_val);
	nMethodType = atoi(read_val);
	#if 0
	/*Get VENDOR-ID*/
	sprintf(szURI, "%s/VENDOR-ID", eap_x);
	ret = odm_get_xnode_value(dev_idx, szURI, 0, read_val);
	if (ret == DMC_ERROR_NOT_FOUND)
		goto not_found;
	xprintf(lmask, "XML METHOD-TYPE='%s'\n", read_val);
	vendor_id = atoi(read_val);
	#endif

	/*Get USER-IDENTITY for MSCHAPv2 (InnerNAI)*/
	sprintf(szURI, "%s/USER-IDENTITY", eap_x);
	ret = odm_get_xnode_value(dev_idx, szURI, 0, szUserID);
	if (ret < 0 && ret != DMC_ERROR_NOT_FOUND)
		goto out;
	xprintf(lmask, "XML USER-IDENTITY='%s'\n", szUserID);

	/*Get Password for MSCHAPv2 (InnerPassword)*/
	sprintf(szURI, "%s/PASSWORD", eap_x);
	ret = odm_get_xnode_value(dev_idx, szURI, 0, szPassword);
	if (ret < 0 && ret != DMC_ERROR_NOT_FOUND)
		goto out;
	xprintf(lmask, "XML PASSWORD='%s'\n", szPassword);

	/*Get REALM*/
	sprintf(szURI, "%s/REALM", eap_x);
	ret = odm_get_xnode_value(dev_idx, szURI, 0, szRealm);
	if (ret < 0 && ret != DMC_ERROR_NOT_FOUND)
		goto out;
	xprintf(lmask, "XML REALM='%s'\n", szRealm);

	/*Get VFY-SRV-REALM*/
	sprintf(szURI, "%s/VFY-SRV-REALM", eap_x);
	ret = odm_get_xnode_value(dev_idx, szURI, 0, read_val);
	if (!ret && odm_parse_bool(read_val)) {
		char eap_x_node[256];
		int i = 0, j = 0, k = 0, max_x_node = 8;
		eapp->check_realm = TRUE;
		eapp->configured_realm_list_cnt = 0;
		while (max_x_node > i) {
			ret = odm_get_xnode_node(dev_idx, eap_x, i, eap_x_node);
			if (ret < 0)
				break;

			#if 1	/*Temporary code against HUMIT BUG*/
			sprintf(szURI, "%s/SERVER-REALMS", eap_x_node);
			ret = 4;
			ret = ODM_GetNodeValue(dev_idx, szURI, read_val, &ret);/*min val_len is 3*/
			if (ret < 0) {
				i++;
				continue;
			}
			#endif
			
			/*Get First EAP/X/SERVER-REALMS*/
			sprintf(szURI, "%s/SERVER-REALMS/<X>/SERVER-REALM", eap_x_node);
			k = 0;
			while (j < EAP_CHECK_REALM_LIST_SIZE) {
				ret = odm_get_xnode_value(dev_idx, szURI, k, 
					eapp->configured_realm_list[j]);
				xprintf(SDK_DBG, "ret=%d, eapp->configured_realm_list[j]=%s\n",
					ret, eapp->configured_realm_list[j]);
				if (ret < 0)
					break;
				eapp->configured_realm_list_cnt++;
				j++, k++;
			}
			i++;
		}
	}

	if (eapp->cr801_enabled && 
		eapp->cr801_server_reject_cnt == CR801_SERVER_REJECT_LIMIT) {
		sprintf(anony_id, "%02x%02x%02x%02x%02x%02x@%s",
			wm->dev_info.device_mac[0],
			wm->dev_info.device_mac[1],
			wm->dev_info.device_mac[2],
			wm->dev_info.device_mac[3],
			wm->dev_info.device_mac[4],
			wm->dev_info.device_mac[5],
			szRealm);
		eapp->type = W_EAP_TLS;
	}
	else {
		xprintf(lmask, "eapp->anony_id=%s\n", eapp->anony_id);
		anony_id_len = strlen(eapp->anony_id);
		if (anony_id_len) {
			ParsingOuterNAI(eapp->anony_id, NULL, NULL, read_val, NULL);
			if (*read_val)
				sprintf(anony_id, "%s@%s", read_val, szRealm);
			else {
				anony_id_len = 0;
				xprintf(SDK_NOTICE, "anonyid(%s)'s id part is empty\n", eapp->anony_id);
			}
		}
		if (!anony_id_len) {
			switch (nMethodType) {
				case OMA_DM_EAP_TLS:
					eapp->type = W_EAP_TLS;
					szUserID[0] = szPassword[0] = 0;
					sprintf(anony_id, "MAC@%s", szRealm);
					break;
				case OMA_DM_EAP_TTLS:
					eapp->type = W_EAP_TTLS_MSCHAPV2;
					sprintf(anony_id, "RANDOM@%s", szRealm);
					#if 0
					if (vendor_id != OMA_EAP_VENDERID_MSCHAPV2)
						xprintf(SDK_NOTICE, "Unknown VENDOR-ID=%d\n", vendor_id);
					#endif
					break;
				case OMA_DM_EAP_AKA:
					eapp->type = W_EAP_AKA;
					break;
			}
		}
	}

	/*Outer NAI*/
	xprintf(lmask, "anony_id=%s\n", anony_id);

	/*Inner ID*/
	strcpy(userid, szUserID);
	xprintf(lmask, "userid=%s\n", userid);

	/*Inner Password*/
	strcpy(userid_pass, szPassword);
	xprintf(lmask, "userid_pwd=%s\n", userid_pass);

	ret = 0;
out:
	dm_put_dev(dev_idx);
	xfunc_out("ret=%d", ret);
	return ret;
}

static int configure_bootstrap(int dev_idx)
{
	device_t *dev;
	wimax_t *wm;
	wm_eap_param_t *eapp;
	wm_oma_dm_t *odm;
	wm_subscription_info_t *ss_info;
	int polling_intv = POLLING_INTERVAL_NO_INIT;
	int ret = -1;

	xfunc_in();

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	wm = dev->wimax;
	eapp = &wm->dev_info.eapp;
	odm = &wm->dev_info.oma_dm;

	if (!wm->scan.selected_subs) {
		xprintf(SDK_ERR, "Subscription was not set up\n");
		goto out;
	}

	ss_info = wm->scan.selected_subs;
	xprintf(SDK_DBG, "Subscription(%s) activated=%d, cr801e=%d, cr801m=%d\n",
		ss_info->subscription_id.hnspid.name, ss_info->activated,
		eapp->cr801_enabled, eapp->cr801_mode);

	if (ss_info->activated) {
		odm->bootstrap_state = BOOTSTRAP_STATE_SUCCESS;
		if (eapp->cr801_enabled &&  eapp->cr801_mode == CR801_TLS) {
			xprintf(SDK_DBG, "Chagne to BOOTSTRAP_STATE_FAIL state for CR801\n");
			odm->bootstrap_state = BOOTSTRAP_STATE_FAIL;
		}
		ret = 0;
	}
	else
		odm->bootstrap_state = BOOTSTRAP_STATE_FAIL;
	xprintf(SDK_INFO, "bootstrap state=%d\n", odm->bootstrap_state);

	if (odm->bootstrap_state == BOOTSTRAP_STATE_FAIL)
		odm_set_wib_param(dev_idx);

	#if defined(CALC_BOOTSTRAP_TIMER_SINCE_CONNECTED)
	xprintf(SDK_DBG, "Gap(Connect & IP): %d ms\n", 
			gettimemsofday() - odm->connected_time_ms);
	#endif

	if ((ret = load_pollinginterval(dev_idx)) < 0)
		goto out;

	#if defined(ODM_WIB_FIRST_POLLING_MS)
	if (!ss_info->activated) {
		polling_intv = ODM_WIB_FIRST_POLLING_MS;
		#if defined(CALC_BOOTSTRAP_TIMER_SINCE_CONNECTED)
		polling_intv -= gettimemsofday() - odm->connected_time_ms;
		#endif
		ODM_ChangePollingThreadInterval(dev_idx, polling_intv);
		goto out;
	}
	#endif

	#if defined(CALC_BOOTSTRAP_TIMER_SINCE_CONNECTED)
	issue_pollinginterval(dev_idx, gettimemsofday() - odm->connected_time_ms);
	#else
	issue_pollinginterval(dev_idx, 0);
	#endif
out:
	dm_put_dev(dev_idx);
	xfunc_out("ret=%d", ret);
	return ret;
}

int odm_setup_eap_param(int dev_idx, char *userid, char *userid_pass, char *anony_id)
{
	#define DECORATION_SM_1	"{sm=1}"
	device_t *dev;
	wimax_t *wm;
	wm_subscription_info_t	*selected_subs;
	wm_eap_param_t	*eapp;
	int ret = 0;

	xfunc_in("dev=%d", dev_idx);

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	userid[0] = userid_pass[0] = anony_id[0] = 0;

	wm = dev->wimax;
	selected_subs = wm->scan.selected_subs;
	eapp = &wm->dev_info.eapp;

	if (!selected_subs) {
		xprintf(SDK_ERR, "Subscription was not set up\n");
		goto out;
	}
	eapp->check_realm = FALSE;
	eapp->configured_realm_list_cnt = 0;

	xprintf(SDK_DBG, "selected_subs->activated=%d, hnspid.name=%s, user_hnai=%s\n",
		selected_subs->activated,
		selected_subs->subscription_id.hnspid.name,
		selected_subs->subscription_id.user_hnai);
	if (selected_subs->activated) {
		load_eap_param(dev_idx, (char *) selected_subs->subscription_id.oper_x,
			userid, userid_pass, anony_id);
	}
	else {
		#if (CW_DEVICE_CATEGORY == 1 || CW_DEVICE_CATEGORY == 2)
		xprintf(SDK_DBG, "IS_VISITED_NSP ? %s\n", IS_VISITED_NSP(wm) ? "YES" : "NO");
		if (!IS_VISITED_NSP(wm)) {
			strcpy(anony_id, DECORATION_SM_1);
			load_eap_param(dev_idx, (char *) selected_subs->subscription_id.oper_x,
				userid, userid_pass, &anony_id[strlen(anony_id)]);
			xprintf(SDK_DBG, "anony_id=%s\n", anony_id);
			goto out;
		}
		#endif
		eapp->type = W_EAP_TLS;

		sprintf(anony_id, "%s%02x%02x%02x%02x%02x%02x", 
			DECORATION_SM_1,
			wm->dev_info.device_mac[0],
			wm->dev_info.device_mac[1],
			wm->dev_info.device_mac[2],
			wm->dev_info.device_mac[3],
			wm->dev_info.device_mac[4],
			wm->dev_info.device_mac[5]);
		#if defined(KIBA_LAB_ONLY)
		#define PREFIX_REALM	"@uqc.ne.jp"
		strcat(anony_id, PREFIX_REALM);
		#endif
	}
out:
	dm_put_dev(dev_idx);
	return ret;
}

int odm_init(void)
{
	int ret = 0;

	xfunc_in();

	#if defined(ODM_SUPPORT_MULTIDEVICE)
	if ((ret = DMCInit(0, "./")) < 0) {
		xprintf(SDK_ERR, "DMCInit=%d\n", ret);
		return -1;
	}

	xprintf(SDK_DBG, "DMCInit OK\n");
	ODM_RegCallBackFunc();
	#endif

	odm_event_thread_create(&sdk_mng.odm_evt);

	xfunc_out("ret=%d", ret);
	return ret;
}

int odm_deinit(void)
{
	#if defined(ODM_SUPPORT_MULTIDEVICE)
	int stime, elapsed, ret_mask = SDK_DBG;
	#endif
	int ret = 0;

	xfunc_in();

	odm_event_thread_delete(&sdk_mng.odm_evt);

	#if defined(ODM_SUPPORT_MULTIDEVICE)
	stime = gettimemsofday();
	if ((ret = DMCTerm(0)) < 0)
		ret_mask = SDK_ERR;
	elapsed = gettimemsofday() - stime;
	xprintf(ret_mask, "DMCTerm=%d, elapsed-ms=%d\n", ret, elapsed);
	#endif

	xfunc_out("ret=%d", ret);
	return ret;
}

static void odm_bootstrap_timer_callback(void *data)
{
	int dev_idx = (int) data;
	device_t *dev;
	wimax_t *wm;
	wm_oma_dm_t *odm;

	xfunc_in("dev=%d", dev_idx);

	if (!(dev = dm_get_dev(dev_idx)))
		return;

	wm = dev->wimax;
	odm = &wm->dev_info.oma_dm;

	xprintf(SDK_DBG, "BOOTSTRAP Callback: bootstrapstate=%d, pollinginterval=%d\n",
		odm->bootstrap_state, odm->polling_interval);

	if (odm->d_status != D_IPACQUISITION) {
		xprintf(SDK_DBG, "Invalid ODM status=%d\n", odm->d_status);
		start_timer(&odm->bootstrap_timer, 500);
		return;
	}

	switch ((int) odm->bootstrap_state) {
		case BOOTSTRAP_STATE_FAIL:
			ODM_RunWIB(dev_idx);
			break;
		case BOOTSTRAP_STATE_ONGOING:
			xprintf(SDK_ERR, "BOOTSTRAP_STATE_ONGOING\n");
			break;
		case BOOTSTRAP_STATE_SUCCESS:
			xprintf(SDK_DBG, "BOOTSTRAP_STATE_SUCCESS\n");
			ODM_TriggerNormal(dev_idx);
			break;
	}
	#if defined(ODM_WIB_CONTINUOUS_POLLING_MS)
	if (wm->scan.selected_subs && !wm->scan.selected_subs->activated)
		ODM_ChangePollingThreadInterval(dev_idx, ODM_WIB_CONTINUOUS_POLLING_MS);
	else
	#endif
	issue_pollinginterval(dev_idx, 0);
	xfunc_out();
}

#if defined(AUTO_CONNECT_NOT_SUPPORTED)
static void reconnect_timer_callback(void *data)
{
	int dev_idx = (int) data;
	device_t *dev;
	wm_oma_dm_t *odm;
	int m_status, c_status;
	bool should_restart = FALSE;
	int timer_ms;
	int ret;

	if (!(dev = dm_get_dev(dev_idx)))
		return;
	odm = &dev->wimax->dev_info.oma_dm;

	xfunc_in("dev=%d", dev_idx);
	ret = sdk_get_status(NULL, dev_idx, &m_status, &c_status);
	if (!ret) {
		if (m_status == M_OPEN_ON || m_status == M_SCAN) {
			ret = wm_re_connect_network(dev_idx);
			xprintf(SDK_DBG, "Re-Connect network: ret=%d\n", ret);
			should_restart = TRUE;
			timer_ms = ODM_RECONNECT_TIMER_MS;
		}
		else if (m_status == M_CONNECTING) {
			should_restart = TRUE;
			timer_ms = ODM_RECONNECT_TIMER_MS*3;
		}

		if (should_restart && odm->reconnect_count++ < MAX_RECONNECT_CNT) {
			start_timer(&odm->disconnect_timer, timer_ms);
			xprintf(SDK_DBG, "Re-start reconnect timer(%d)\n", timer_ms);
		}
	}
	xfunc_out();
	dm_put_dev(dev_idx);
}
#endif

static void odm_disconnect_timer_callback(void *data)
{
	int dev_idx = (int) data;
	int m_status, c_status;
	int ret;

	xfunc_in("dev=%d", dev_idx);

	ret = sdk_get_status(NULL, dev_idx, &m_status, &c_status);
	if (!ret) {
		if (m_status == M_CONNECTED) {
			ret = wm_disconnect_network(dev_idx, SDK_DISCONN_RESP_TIMEOUT_SEC);
			xprintf(SDK_DBG, "Disconnect ret=%d\n", ret);
		}
		#if defined(AUTO_CONNECT_NOT_SUPPORTED)
		{
			device_t *dev;
			wm_oma_dm_t *odm;
			if (!(dev = dm_get_dev(dev_idx)))
				return;
			odm = &dev->wimax->dev_info.oma_dm;
			odm->reconnect_count = 0;
			init_timer(&odm->disconnect_timer, reconnect_timer_callback, (void *)dev_idx);
			start_timer(&odm->disconnect_timer, ODM_RECONNECT_TIMER_MS);
			xprintf(SDK_DBG, "Start reconnect-timer(%d sec.)\n", ODM_RECONNECT_TIMER_MS/1000);
			dm_put_dev(dev_idx);
		}
		#endif
	}

	xfunc_out();
}

static void odm_ip_acquisition_timer_callback(void *data)
{
	int dev_idx = (int) data;
	device_t *dev;
	wimax_t *wm;
	wm_oma_dm_t *odm;
	int ret = 0, elapsed_ms;

	xfunc_in("dev=%d", dev_idx);

	if (!(dev = dm_get_dev(dev_idx)))
		return;

	wm = dev->wimax;
	odm = &wm->dev_info.oma_dm;

	if (!(ret = net_get_dhcp_info(dev_idx, &wm->dev_info.dhcp_info))) {
		xprintf(SDK_INFO, "IPAcquisition: %s\n", wm->dev_info.dhcp_info.ip);
		ret = odm_send_msg(dev_idx, ND_IPAcquisition);
	}
	else {
		elapsed_ms = gettimemsofday() - odm->connected_time_ms;
		if (elapsed_ms < ODM_IP_ACQUISITION_TIMEOUT_MS) {
			xprintf(SDK_DBG, "Restart ip_acquisition_timer (%d ms.)\n",
				ODM_IP_ACQUISITION_TIMER_MS);
			ret = start_timer(&odm->ip_acquisition_timer, ODM_IP_ACQUISITION_TIMER_MS);
		}
		else
			xprintf(SDK_DBG, "ip_acquisition_timer expired(%d ms.)!!\n", elapsed_ms);
	}

	dm_put_dev(dev_idx);
	xfunc_out("ret=%d", ret);
}

int odm_upload_xml(int dev_idx)
{
	int max_xml_size = 128 * 1024;
	char *buf = NULL;
	char *exc_list[] = {"./WiMAX_Diagnostics"};
	int ret, i;

	xfunc_in("dev=%d", dev_idx);

	buf = sdk_malloc(max_xml_size);
	assert(buf);

	if ((ret = bl_upload(dev_idx, DLIMG_OMA_XML, buf, max_xml_size, NULL)) > 0)
		ret = ODM_SetXMLToDMC(dev_idx, buf, ret);
	else
		ret = -1;

	if (buf)
		sdk_free(buf);
	if (!ret) {
		for (i = 0; i < numof_array(exc_list); i++)
			ODM_AddExcludingList(dev_idx, exc_list[i]);

		ODM_PopulateNode(dev_idx);
	}
	xfunc_out("ret=%d", ret);
	return ret;
}

int odm_open(int dev_idx)
{
	device_t *dev;
	wimax_t *wm;
	wm_oma_dm_t *odm;
	int ret;

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	xfunc_in("dev=%d", dev_idx);

	#if !defined(ODM_SUPPORT_MULTIDEVICE)
	xprintf(SDK_DBG, "+DMCInit\n");
	if ((ret = DMCInit(dev_idx, "./")) < 0) {
		xprintf(SDK_ERR, "-DMCInit=%d\n", ret);
		goto out;
	}
	xprintf(SDK_DBG, "-DMCInit\n");

	ODM_RegCallBackFunc();
	#endif

	wm = dev->wimax;
	odm = &wm->dev_info.oma_dm;
	odm->port = UDP_NOTI_PORT;

	#if defined(CONFIG_OMA_DM_DRMD)
	odm->drmd = malloc(sizeof(drmd_context_t));
	memset(odm->drmd, 0x00, sizeof(drmd_context_t));
	#endif

	init_timer(&odm->bootstrap_timer, odm_bootstrap_timer_callback, (void *)dev_idx);
	init_timer(&odm->disconnect_timer, odm_disconnect_timer_callback, (void *)dev_idx);
	init_timer(&odm->ip_acquisition_timer, odm_ip_acquisition_timer_callback,
		(void *)dev_idx);

	if ((ret = odm_upload_xml(dev_idx)) < 0)
		goto out;
	
	#if 0
	ODM_SetSecureLayer(dev_idx);
	#endif

	DMCSetBootStrapParam(dev_idx, DMC_SECURITY_NETWPIN);

	odm->bootstrap_state = BOOTSTRAP_STATE_FAIL;
	odm->polling_interval = POLLING_INTERVAL_ONCE;
	
	ret = ODM_StartListener(dev_idx, odm->ip_addr, odm->port);
	odm->opened = !ret ? TRUE : FALSE;
out:
	dm_put_dev(dev_idx);
	xfunc_out("ret=%d", ret);
	return ret;
}

int odm_close(int dev_idx)
{
	device_t *dev;
	wm_oma_dm_t *odm;
	int ret = 0, ret_mask = SDK_DBG;
	#if !defined(ODM_SUPPORT_MULTIDEVICE)
	int stime, elapsed;
	#endif

	xfunc_in();

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	ODM_StopListener(dev_idx);

	//download_xml(dev_idx)

	odm = &dev->wimax->dev_info.oma_dm;
	odm->opened = FALSE;

	if (odm->bootstrap_timer.to_created) del_timer(&odm->bootstrap_timer);
	if (odm->disconnect_timer.to_created) del_timer(&odm->disconnect_timer);
	if (odm->ip_acquisition_timer.to_created) del_timer(&odm->ip_acquisition_timer);

	#if !defined(ODM_SUPPORT_MULTIDEVICE)
	stime = gettimemsofday();
	if ((ret = DMCTerm(0)) < 0)
		ret_mask = SDK_ERR;
	elapsed = gettimemsofday() - stime;
	xprintf(ret_mask, "DMCTerm=%d, elapsed-ms=%d\n", ret, elapsed);
	#endif

	#if defined(CONFIG_OMA_DM_DRMD)
	free(odm->drmd);
	#endif

	dm_put_dev(dev_idx);
	xfunc_out("ret=%d", ret);
	return ret;
}

int odm_send_msg(int dev_idx, int event)
{
	device_t *dev;
	msg_thr_t *odm_evt = &sdk_mng.odm_evt;

	xfunc_in("event=%d", event);

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	wm_in_wimax(dev->wimax, WM_USING_LOCK_ODM);
	msg_send(&odm_evt->msg_cb, dev_idx, (void *)event);

	xfunc_out();
	dm_put_dev(dev_idx);
	return 0;
}

static void odm_init_connection_state(wm_oma_dm_t *odm)
{
	odm->session_flag = 0;
	odm->polling_supported = FALSE;
	odm->polling_attempts = POLLING_ATTEMPT_INFINITY;
	odm->polling_interval = POLLING_INTERVAL_DISABLE;
	odm->polling_count = 0;
	odm->connected_time_ms = gettimemsofday();
}

static int odm_on_app_event(int dev_idx, fsm_event_t event)
{
	device_t *dev;
	wimax_t *wm;
	wm_oma_dm_t *odm;
	int d_status;
	unsigned evt_mask = SDK_INFO;
	int ret = 0;

	xfunc_in("dev=%d, event=%d", dev_idx, event);

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	assert(dev->wimax);
	wm = dev->wimax;
	odm = &wm->dev_info.oma_dm;
	d_status = odm->d_status;

	xprintf(evt_mask, "ODM Event=%s d_status=%d\n", odm_fsm2str(event), d_status);

	switch ((int) event) {
		case MD_Start:
			odm->d_status = D_READY;
			odm_open(dev_idx);
			break;
		case MD_End:
			odm->d_status = D_INIT;
			if (odm->bootstrap_timer.to_created) stop_timer(&odm->bootstrap_timer);
			if (odm->disconnect_timer.to_created) stop_timer(&odm->disconnect_timer);
			if (odm->ip_acquisition_timer.to_created) stop_timer(&odm->ip_acquisition_timer);
			break;
		case CD_ConnectSuccess:
			odm_init_connection_state(odm);
			ret = start_timer(&odm->ip_acquisition_timer, ODM_IP_ACQUISITION_TIMER_MS);
			switch (d_status) {
				case D_READY:
				case D_LISTEN:
				case D_IPACQUISITION:
					odm->d_status = D_LISTEN;
					if (E_EAP_ENABLED(dev))
						odm_setup_emsk(dev_idx);
					ODM_SetBEKParam(dev_idx, odm->eap_emsk, EMSK_SIZE);
					#if defined(CONFIG_OMA_DM_DRMD)
					dm_drmd_ind_connection(dev_idx, TRUE);
					#endif
					break;
			}
			break;
		case ND_IPAcquisition:
			switch (d_status) {
				case D_LISTEN:
					odm->d_status = D_IPACQUISITION;
					configure_bootstrap(dev_idx);
					break;
				#if 0
				case D_IPACQUISITION:
					odm_set_wib_param(dev_idx);
					break;
				#endif
			}
			break; 
		case CD_Disconnect:
			switch (d_status) {
				case D_LISTEN:
				case D_IPACQUISITION:
					odm->d_status = D_READY;
					stop_timer(&odm->bootstrap_timer);
					stop_timer(&odm->disconnect_timer);
					#if defined(CONFIG_OMA_DM_DRMD)
					dm_drmd_ind_connection(dev_idx, FALSE);
					#endif
					break;
			}
			break;
		case DD_SessionComplete:
			switch (d_status) {
				case D_LISTEN:
					xprintf(SDK_NOTICE, "ODM state is D_LISTEN\n");
					odm->d_status = D_IPACQUISITION;
					//odm_set_wib_param(dev_idx);
					break;
				case D_IPACQUISITION:
					/*Normal Case*/
					break;
				default:
					xprintf(SDK_NOTICE, "Unknown ODM state(%d)\n", d_status);
					break;
			}
			if (should_download_xml(odm) && download_xml(dev_idx) < 0)
				xprintf(SDK_ERR, "XML downloading error\n");
			if (is_provisioning_session(odm))
				ODM_ProvComplete(dev_idx);
			else if (is_xml_changed(odm))
				apply_provisioned_pollinginterval(dev_idx);
			if (is_fumo_session(odm))
				fumo_complete(dev_idx);
			break;
	}

	wm_out_wimax(dev->wimax, WM_USING_LOCK_ODM);
	xfunc_out("ret=%d", ret);
	dm_put_dev(dev_idx);
	return ret;
}

static void *odm_event_thread(void *data)
{
	msg_thr_t *msg_thr = (msg_thr_t *) data;
	int event;
	int dev_idx;

	xfunc_in();

	msg_init(&msg_thr->msg_cb);

	pthread_sem_signal(&msg_thr->sync_lock);
	
	while (1) {
		xprintf(SDK_DBG, "In odm event\n");

		if (msg_recv(&msg_thr->msg_cb, &dev_idx, (void **)&event) < 0) {
			xprintf(SDK_ERR, "msg_recv error\n");
			break;
		}

		if (event == (int) THREAD_EXIT_MSG) {
			xprintf(SDK_INFO, "%s thread exit...\n", __FUNCTION__);
			break;
		}

		odm_on_app_event(dev_idx, event);
	}

	pthread_sem_signal(&msg_thr->sync_lock);

	msg_deinit(&msg_thr->msg_cb);
	xprintf(SDK_INFO, "Exit odm-event-thread\n");

	xfunc_out();
	return NULL;
}

int odm_event_thread_create(struct msg_thr_s *msg_thr)
{	
	xfunc_in();

	pthread_sem_init(&msg_thr->sync_lock, 0);

	pthread_create(&msg_thr->thread, NULL, odm_event_thread, (void *)msg_thr);

	xprintf(SDK_DBG, "Enter ODM Create Sync\n");
	pthread_sem_wait(&msg_thr->sync_lock);
	xprintf(SDK_DBG, "Exit ODM Create Sync\n");

	xfunc_out();
	return 0;
}

int odm_event_thread_delete(struct msg_thr_s *msg_thr)
{
	pthread_t thread;

	xfunc_in();

	if ((thread = msg_thr->thread)) {
		msg_thr->thread = (pthread_t) NULL;
		msg_send(&msg_thr->msg_cb, 0, THREAD_EXIT_MSG);
		
		xprintf(SDK_DBG, "+ODM pthread_join\n");
		pthread_join(thread, NULL);
		xprintf(SDK_DBG, "-ODM pthread_join\n");
	}

	pthread_sem_destroy(&msg_thr->sync_lock);

	xfunc_out("thread=0x%08X", (int) thread);
	return 0;
}

