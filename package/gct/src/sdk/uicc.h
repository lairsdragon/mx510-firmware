#if !defined(UICC_H_20090806)
#define UICC_H_20090806

//#define USE_CHECK_THREAD

typedef struct wm_uicc_s {
	bool	inited;
	int		reader_idx;
	bool	uicc_active;

	#if defined(USE_CHECK_THREAD)
	pthread_t		reader_thr;
	#endif
	void	*uicc_ctxt;

} wm_uicc_t;

// function declaration

int SCardInit(int dev_idx);
void SCardExit(int dev_idx);

void uicc_open(int dev_idx);
void uicc_close(int dev_idx);

int do_eap_aka(int dev_idx, char *buf, int len);

#endif

