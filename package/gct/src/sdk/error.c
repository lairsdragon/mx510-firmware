#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <assert.h>

#include "error.h"
#include "device.h"
#include "log.h"


static int sdk_errno;

int sdk_set_errno(int err_no)
{
	int lmask = SDK_ERR;

	switch (err_no) {
		case ERR_PERM:
			xprintf(lmask, "Permission denied.\n");
			break;
	}
	sdk_errno = err_no;

	return -1;
}

int sdk_get_errno(void)
{
	return sdk_errno;
}
