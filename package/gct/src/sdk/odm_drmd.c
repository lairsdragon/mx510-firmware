#define _GNU_SOURCE
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdarg.h>
#include <assert.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "gcttype.h"
#include "error.h"
#include "device.h"
#include "wimax.h"
#include "hci.h"
#include "fload.h"
#include "log.h"
#include "eap.h"
#include "./odm/lib/dmc.h"
#include "./odm/lib/kdm_error.h"
#include "odm.h"
#include "odm_drmd.h"
#include "sdk.h"


extern void active_drmd_reporting(int dev_idx);

node_info_t drmd_node_list[] = {
	{"./WiMAX_Diagnostics", NODE_FORMAT_node},
		
	{"./WiMAX_Diagnostics/Start", NODE_FORMAT_node},
	{"./WiMAX_Diagnostics/Start/Duration", NODE_FORMAT_int, drmd_Set_Duration, drmd_Get_Duration},
	{"./WiMAX_Diagnostics/Start/Server_ID", NODE_FORMAT_chr, drmd_Set_Server_ID, drmd_Get_Server_ID},
	{"./WiMAX_Diagnostics/Start/Origination_ID", NODE_FORMAT_chr, drmd_Set_Origination_ID, drmd_Get_Origination_ID},

	{"./WiMAX_Diagnostics/WiMAX", NODE_FORMAT_node},
	{"./WiMAX_Diagnostics/WiMAX/ServBSID", NODE_FORMAT_int, NULL, drmd_Get_ServBSID},
	{"./WiMAX_Diagnostics/WiMAX/DownlinkFreq", NODE_FORMAT_int, NULL, drmd_Get_DownlinkFreq},
	{"./WiMAX_Diagnostics/WiMAX/DownlinkBandwidth", NODE_FORMAT_int, NULL, drmd_Get_DownlinkBandwidth},
	{"./WiMAX_Diagnostics/WiMAX/DownlinkMeanRSSI", NODE_FORMAT_chr, NULL, drmd_Get_DownlinkMeanRSSI},
	{"./WiMAX_Diagnostics/WiMAX/DownlinkStdDevRSSI", NODE_FORMAT_chr, NULL, drmd_Get_DownlinkStdDevRSSI},
	{"./WiMAX_Diagnostics/WiMAX/DownlinkMeanCINR", NODE_FORMAT_chr, NULL, drmd_Get_DownlinkMeanCINR},
	{"./WiMAX_Diagnostics/WiMAX/DownlinkStdDevCINR", NODE_FORMAT_chr, NULL, drmd_Get_DownlinkStdDevCINR},
	{"./WiMAX_Diagnostics/WiMAX/TxPwr", NODE_FORMAT_int, NULL, drmd_Get_TxPwr},
	{"./WiMAX_Diagnostics/WiMAX/TxHeadroomPwr", NODE_FORMAT_int, NULL, drmd_Get_TxHeadroomPwr},
	{"./WiMAX_Diagnostics/WiMAX/ScannedBaseStations", NODE_FORMAT_chr, NULL, drmd_Get_ScannedBaseStations},
	{"./WiMAX_Diagnostics/WiMAX/LinkUptime", NODE_FORMAT_int, NULL, drmd_Get_LinkUptime},
	{"./WiMAX_Diagnostics/WiMAX/HARQRetTX", NODE_FORMAT_chr, NULL, drmd_Get_HARQRetTX},
	{"./WiMAX_Diagnostics/WiMAX/HARQRetRX", NODE_FORMAT_chr, NULL, drmd_Get_HARQRetRX},
	{"./WiMAX_Diagnostics/WiMAX/InitRangeResp", NODE_FORMAT_int, NULL, drmd_Get_InitRangeResp},
	{"./WiMAX_Diagnostics/WiMAX/InitRangeNoResp", NODE_FORMAT_int, NULL, drmd_Get_InitRangeNoResp},
	{"./WiMAX_Diagnostics/WiMAX/PerRangeResp", NODE_FORMAT_int, NULL, drmd_Get_PerRangeResp},
	{"./WiMAX_Diagnostics/WiMAX/PerRangeNoResp", NODE_FORMAT_int, NULL, drmd_Get_PerRangeNoResp},
	{"./WiMAX_Diagnostics/WiMAX/HOSuccess", NODE_FORMAT_int, NULL, drmd_Get_HOSuccess},
	{"./WiMAX_Diagnostics/WiMAX/HOFail", NODE_FORMAT_int, NULL, drmd_Get_HOFail},
	{"./WiMAX_Diagnostics/WiMAX/MAPRecSuccess", NODE_FORMAT_int, NULL, drmd_Get_MAPRecSuccess},
	{"./WiMAX_Diagnostics/WiMAX/MAPRecFail", NODE_FORMAT_int, NULL, drmd_Get_MAPRecFail},
	{"./WiMAX_Diagnostics/WiMAX/MCSstatsDL", NODE_FORMAT_chr, NULL, drmd_Get_MCSstatsDL},
	{"./WiMAX_Diagnostics/WiMAX/MCSstatsUL", NODE_FORMAT_chr, NULL, drmd_Get_MCSstatsUL},
	{"./WiMAX_Diagnostics/WiMAX/VersionOfMCSMetric", NODE_FORMAT_int, NULL, drmd_Get_VersionOfMCSMetric},
	{"./WiMAX_Diagnostics/WiMAX/DownlinkDataRate", NODE_FORMAT_chr, NULL, drmd_Get_DownlinkDataRate},
	{"./WiMAX_Diagnostics/WiMAX/UplinkDataRate", NODE_FORMAT_chr, NULL, drmd_Get_UplinkDataRate},
	{"./WiMAX_Diagnostics/WiMAX/PacketsReceived", NODE_FORMAT_int, NULL, drmd_Get_PacketsReceived},
	{"./WiMAX_Diagnostics/WiMAX/PacketsSent", NODE_FORMAT_int, NULL, drmd_Get_PacketsSent},
	{"./WiMAX_Diagnostics/WiMAX/MACState", NODE_FORMAT_chr, NULL, drmd_Get_MACState},
	{"./WiMAX_Diagnostics/WiMAX/PreambleIndex", NODE_FORMAT_chr, NULL, drmd_Get_PreambleIndex},
	{"./WiMAX_Diagnostics/WiMAX/HOLatency", NODE_FORMAT_int, NULL, drmd_Get_HOLatency},
	{"./WiMAX_Diagnostics/WiMAX/FrameRatio", NODE_FORMAT_chr, NULL, drmd_Get_FrameRatio},
	{"./WiMAX_Diagnostics/WiMAX/HOAttempts", NODE_FORMAT_int, NULL, drmd_Get_HOAttempts},
	{"./WiMAX_Diagnostics/WiMAX/NetworkEntryLatency", NODE_FORMAT_int, NULL, drmd_Get_NetworkEntryLatency},
	{"./WiMAX_Diagnostics/WiMAX/NetworkEntrySucceses", NODE_FORMAT_int, NULL, drmd_Get_NetworkEntrySucceses},
	{"./WiMAX_Diagnostics/WiMAX/NetworkEntryFailures", NODE_FORMAT_int, NULL, drmd_Get_NetworkEntryFailures},
	{"./WiMAX_Diagnostics/WiMAX/NetworkEntryAttempts", NODE_FORMAT_int, NULL, drmd_Get_NetworkEntryAttempts},
	{"./WiMAX_Diagnostics/WiMAX/UserAccessTime", NODE_FORMAT_int, NULL, drmd_Get_UserAccessTime},
	
	{"./WiMAX_Diagnostics/Generic", NODE_FORMAT_node},
	{"./WiMAX_Diagnostics/Generic/RateLimiterStats", NODE_FORMAT_int, NULL, drmd_Get_Generic_RateLimiterStats},
	{"./WiMAX_Diagnostics/Generic/TimeActive", NODE_FORMAT_int, NULL, drmd_Get_Generic_TimeActive},
	{"./WiMAX_Diagnostics/Generic/TimeIdle", NODE_FORMAT_int, NULL, drmd_Get_Generic_TimeIdle},
	{"./WiMAX_Diagnostics/Generic/TimeSleep", NODE_FORMAT_int, NULL, drmd_Get_Generic_TimeSleep},
	{"./WiMAX_Diagnostics/Generic/LastRebootCause", NODE_FORMAT_chr, NULL, drmd_Get_Generic_LastRebootCause},
	{"./WiMAX_Diagnostics/Generic/DeviceTemp", NODE_FORMAT_int, NULL, drmd_Get_Generic_DeviceTemp},

	// Below is vender's measurements
//	{"./WiMAX_Diagnostics/Generic/SupVolt", NODE_FORMAT_int},
//	{"./WiMAX_Diagnostics/Generic/BatteryCap", NODE_FORMAT_int},
//	{"./WiMAX_Diagnostics/Generic/GPSlocation_Latitude", NODE_FORMAT_int},
//	{"./WiMAX_Diagnostics/Generic/GPSlocation_Longitude", NODE_FORMAT_int},
//	{"./WiMAX_Diagnostics/Generic/GPSlocation_Altitude", NODE_FORMAT_int},
//	{"./WiMAX_Diagnostics/Generic/NATAddrTable", ?}, // This metric is not supported in this version of the specification. Refer to DRMD Spec.

//	{"./WiMAX_Diagnostics/Generic/LANEtherMAC", NODE_FORMAT_node},
//	{"./WiMAX_Diagnostics/Generic/LANEtherMAC/Enable", NODE_FORMAT_chr},
//	{"./WiMAX_Diagnostics/Generic/LANEtherMAC/MACAddressControlEnabled", NODE_FORMAT_chr},
//	{"./WiMAX_Diagnostics/Generic/LANEtherMAC/MaxBitrate", NODE_FORMAT_chr},
//	{"./WiMAX_Diagnostics/Generic/LANEtherMAC/DuplexMode", NODE_FORMAT_chr},
//	{"./WiMAX_Diagnostics/Generic/LANEtherMAC/LANEtherMACresults", NODE_FORMAT_chr},

//	{"./WiMAX_Diagnostics/Generic/DeviceUptime", NODE_FORMAT_int},
//	{"./WiMAX_Diagnostics/Generic/DeviceLog", NODE_FORMAT_chr},

//	{"./WiMAX_Diagnostics/Generic/IPPingDiagnostics", NODE_FORMAT_node},
//	{"./WiMAX_Diagnostics/Generic/IPPingDiagnostics/DiagnosticsState", NODE_FORMAT_chr},
//	{"./WiMAX_Diagnostics/Generic/IPPingDiagnostics/Interface", NODE_FORMAT_chr},
//	{"./WiMAX_Diagnostics/Generic/IPPingDiagnostics/Host", NODE_FORMAT_chr},
//	{"./WiMAX_Diagnostics/Generic/IPPingDiagnostics/NumberOfRepetition", NODE_FORMAT_int},
//	{"./WiMAX_Diagnostics/Generic/IPPingDiagnostics/Timeout", NODE_FORMAT_int},
//	{"./WiMAX_Diagnostics/Generic/IPPingDiagnostics/DataBlockSize", NODE_FORMAT_int},
//	{"./WiMAX_Diagnostics/Generic/IPPingDiagnostics/DSCP", NODE_FORMAT_int},
//	{"./WiMAX_Diagnostics/Generic/IPPingDiagnostics/IPPingDiagnosticsResults", NODE_FORMAT_chr},
};

static void drmd_harq_retry_value(drmd_harq_retry_t *harq, u32 *buf, u32 burst_drop)
{
	harq->n1st_attempt = buf[0];
	harq->n2nd_retries = buf[1];
	harq->n3rd_retries = buf[2];
	harq->n4th_retries = buf[3];
	harq->total_retries = buf[0]+buf[1]+buf[2]+buf[3];
	harq->bursts_dropped = burst_drop;
}

int drmd_Set_Duration(int dev_idx, drmd_context_t *drmd, void *p)
{
	int ret = 0;
	char *string = (char *)p;

	drmd->duration = atoi(string);
	xprintf(SDK_DBG, "Set Duration=%s\n", string);

	return ret;
}

int drmd_Get_Duration(int dev_idx, drmd_context_t *drmd, void *p)
{
	int ret = 0;
	char *string = (char *)p;

	ret = sprintf(string, "%d", drmd->duration);
	xprintf(SDK_DBG, "Get Duration=%s\n", string);

	return ret;
}

int drmd_Set_Server_ID(int dev_idx, drmd_context_t *drmd, void *p)
{
	int ret = 0;
	char *string = (char *)p;

	strcpy(drmd->serverId, string);
	xprintf(SDK_DBG, "Set Server_ID=%s\n", string);

	return ret;
}

int drmd_Get_Server_ID(int dev_idx, drmd_context_t *drmd, void *p)
{
	int ret = 0;
	char *string = (char *)p;

	ret = sprintf(string, "%s", drmd->serverId);
	xprintf(SDK_DBG, "Get Server_ID=%s\n", string);

	return ret;
}

int drmd_Set_Origination_ID(int dev_idx, drmd_context_t *drmd, void *p)
{
	int ret = 0;
	char *string = (char *)p;

	strcpy(drmd->originationId, string);
	xprintf(SDK_DBG, "Set Origination_ID=%s\n", string);

	return ret;
}

int drmd_Get_Origination_ID(int dev_idx, drmd_context_t *drmd, void *p)
{
	int ret = 0;
	char *string = (char *)p;

	ret = sprintf(string, "%s", drmd->originationId);
	xprintf(SDK_DBG, "Get Origination_ID=%s\n", string);

	return ret;
}

int drmd_Get_ServBSID(int dev_idx, drmd_context_t *drmd, void *p)
{
	mac_report_t *mrpt = &drmd->mrpt;
	int ret = 0;
	char *string = (char *)p;
	u8  *bsid = mrpt->phy_mac_basic.bsid;

	ret = sprintf(string, "%02X%02X%02X%02X%02X%02X",
		bsid[0], bsid[1], bsid[2], bsid[3], bsid[4], bsid[5]);
	xprintf(SDK_DBG, "BSID=%s\n", string);

	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/WiMAX/ServBSID", string, ret);
	return ret;
}

int drmd_Get_DownlinkFreq(int dev_idx, drmd_context_t *drmd, void *p)
{
	mac_report_t *mrpt = &drmd->mrpt;
	int ret = 0;
	char *string = (char *)p;

	ret = sprintf(string, "%lu", mrpt->phy_mac_basic.frequency);
	xprintf(SDK_DBG, "Freq=%s\n", string);

	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/WiMAX/DownlinkFreq", string, ret);
	return ret;
}

int drmd_Get_DownlinkBandwidth(int dev_idx, drmd_context_t *drmd, void *p)
{
	mac_report_t *mrpt = &drmd->mrpt;
	int ret = 0;
	char *string = (char *)p;

	ret = sprintf(string, "%u", mrpt->phy_mac_basic.bandwidth);
	xprintf(SDK_DBG, "BandWidth=%s\n", string);

	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/WiMAX/DownlinkBandwidth", string, ret);
	return ret;
}

#define DE_COMBINE_VAL 3
int drmd_Get_DownlinkMeanRSSI(int dev_idx, drmd_context_t *drmd, void *p)
{
	mac_report_t *mrpt = &drmd->mrpt;
	int ret = 0;
	char *string = (char *)p;
	int combined_rssi = mrpt->phy_cinr_rssi.rssi_mean;
	int rssi = mrpt->phy_cinr_rssi.rssi_mean - DE_COMBINE_VAL;
	int rssi2 = mrpt->phy_cinr_rssi.rssi_mean - DE_COMBINE_VAL;
		
	ret = sprintf(string, "[0,(%d),1,(%d),2,(%d)]", combined_rssi, rssi, rssi2);
	xprintf(SDK_DBG, "rssi_mean=%s\n", string);

	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/WiMAX/DownlinkMeanRSSI", string, ret);
	return ret;
}

int drmd_Get_DownlinkStdDevRSSI(int dev_idx, drmd_context_t *drmd, void *p)
{
	mac_report_t *mrpt = &drmd->mrpt;
	int ret = 0;
	char *string = (char *)p;
	int combined_rssi = mrpt->phy_cinr_rssi.rssi_std_dev;
	int rssi = mrpt->phy_cinr_rssi.rssi_std_dev - DE_COMBINE_VAL;
	int rssi2 = mrpt->phy_cinr_rssi.rssi_std_dev - DE_COMBINE_VAL;

	ret = sprintf(string, "[0,(%d),1,(%d),2,(%d)]", combined_rssi, rssi, rssi2);
	xprintf(SDK_DBG, "std_dev_rssi=%s\n", string);

	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/WiMAX/DownlinkStdDevRSSI", string, ret);
	return ret;
}

int drmd_Get_DownlinkMeanCINR(int dev_idx, drmd_context_t *drmd, void *p)
{
	mac_report_t *mrpt = &drmd->mrpt;
	int ret = 0;
	char *string = (char *)p;
	int combined_cinr = mrpt->phy_cinr_rssi.cinr_mean;
	int cinr = mrpt->phy_cinr_rssi.cinr_a_mean; 
	int cinr2 = mrpt->phy_cinr_rssi.cinr_b_mean;

	ret = sprintf(string, "[0,(%d),1,(%d),2,(%d)]", combined_cinr, cinr, cinr2);
	xprintf(SDK_DBG, "cinr_mean=%s\n", string);

	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/WiMAX/DownlinkMeanCINR", string, ret);
	return ret;
}

int drmd_Get_DownlinkStdDevCINR(int dev_idx, drmd_context_t *drmd, void *p)
{
	mac_report_t *mrpt = &drmd->mrpt;
	int ret = 0;
	char *string = (char *)p;
	int combined_cinr = mrpt->phy_cinr_rssi.cinr_std_dev;
	int cinr = mrpt->phy_cinr_rssi.cinr_std_dev;
	int cinr2 = mrpt->phy_cinr_rssi.cinr_std_dev;

	ret = sprintf(string, "[0,(%d),1,(%d),2,(%d)]", combined_cinr, cinr, cinr2);
	xprintf(SDK_DBG, "cinr_std_dev=%s\n", string);

	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/WiMAX/DownlinkStdDevCINR", string, ret);
	return ret;
}

int drmd_Get_TxPwr(int dev_idx, drmd_context_t *drmd, void *p)
{
	mac_report_t *mrpt = &drmd->mrpt;
	int ret = 0;
	char *string = (char *)p;

	ret = sprintf(string, "%d", mrpt->power_ctrl.last_tx_power);
	xprintf(SDK_DBG, "TxPwr=%s\n", string);

	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/WiMAX/TxPwr", string, ret);
	return ret;
}

int drmd_Get_TxHeadroomPwr(int dev_idx, drmd_context_t *drmd, void *p)
{
	mac_report_t *mrpt = &drmd->mrpt;
	int ret = 0;	
	char *string = (char *)p;

	ret = sprintf(string, "%d", (mrpt->power_ctrl.tx_max_power - mrpt->power_ctrl.last_tx_power));
	xprintf(SDK_DBG, "TxHeadroomPwr=%s\n", string);

	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/WiMAX/TxHeadroomPwr", string, ret);
	return ret;
}

int drmd_Get_ScannedBaseStations(int dev_idx, drmd_context_t *drmd, void *p)
{
	#define data_len		233

	mac_report_t *mrpt = &drmd->mrpt;
	int ret = 0, i;
	char *string = (char *)p;	
	u8 *bsid;
	wm_bs_neigh_t *list = mrpt->neighbor.list;
	int list_cnt = mrpt->neighbor.nr_list;
	u16 bandwidth = 0;
	int offset = 0;

	offset = sprintf(string, "[");
	string += offset;
	ret += offset;

	for (i = 0; ret < data_len-2 && i < list_cnt; i++) {
		bsid = list[i].bsid;
		bandwidth = 125 * list[i].bandwidth;
		if((s8)list[i].rssi > -110 || (s8)list[i].cinr > -4)
		{
			offset = sprintf(string, "%02X%02X%02X%02X%02X%02X",
				bsid[0], bsid[1], bsid[2], bsid[3], bsid[4], bsid[5]); /*full BSID*/
			string += offset;
			ret += offset;

			offset = sprintf(string, ",%d,%d,%d,%d,%d",
				list[i].preamble, (int)list[i].frequency, bandwidth, list[i].rssi, list[i].cinr);
			string += offset;
			ret += offset;

			if(i+1 < list_cnt)
			{
				offset = sprintf(string,",");
				string += offset;
				ret += offset;
			}
		}
	}
	if (1 < ret) {
		ret += sprintf(string,"]");
	} else {
		((char *)p)[0] = '\0' ;
		ret = 0;
	}

	xprintf(SDK_DBG, "ScannedBaseStations=%s\n", (char *)p);	

	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/WiMAX/ScannedBaseStations", string, ret);
	return ret;
}

int drmd_Get_LinkUptime(int dev_idx, drmd_context_t *drmd, void *p)
{
	mac_report_t *mrpt = &drmd->mrpt;
	int ret = 0;
	char *string = (char *)p;

	ret = sprintf(string, "%u", mrpt->net_entry_stat.rng_time);
	xprintf(SDK_DBG, "LinkUptime=%s\n", string);

	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/WiMAX/LinkUptime", string, ret);
	return ret;
}

int drmd_Get_HARQRetTX(int dev_idx, drmd_context_t *drmd, void *p)
{
	mac_report_t *mrpt = &drmd->mrpt;
	int ret = 0;
	char *string = (char *)p;
	drmd_harq_retry_t harq;

	drmd_harq_retry_value(&harq, 
			      mrpt->harq_stat.num_ul_harq_retry, 
			      0);

		ret = sprintf(string, "00:%lu,01:%lu,02:%lu,03:%lu,04:%lu,05:%lu",
			harq.n1st_attempt, harq.n2nd_retries,
			harq.n3rd_retries, harq.n4th_retries,
			harq.total_retries, harq.bursts_dropped);
	
	xprintf(SDK_DBG, "HARQRetTX=%s\n", string);

	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/WiMAX/HARQRetTX", string, ret);
	return ret;
}

int drmd_Get_HARQRetRX(int dev_idx, drmd_context_t *drmd, void *p)
{
	mac_report_t *mrpt = &drmd->mrpt;
	int ret = 0;
	char *string = (char *)p;
	drmd_harq_retry_t harq;

	drmd_harq_retry_value(&harq, 
			      mrpt->harq_stat.num_dl_harq_retry, 
			      mrpt->phy_burst.num_harq_dl_burst_drop);

		ret = sprintf(string, "00:%lu,01:%lu,02:%lu,03:%lu,04:%lu,05:%lu",
			harq.n1st_attempt, harq.n2nd_retries,
			harq.n3rd_retries, harq.n4th_retries,
			harq.total_retries, harq.bursts_dropped);
	xprintf(SDK_DBG, "HARQRetRx=%s\n", string);

	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/WiMAX/HARQRetRX", string, ret);
	return ret;
}

int drmd_Get_InitRangeResp(int dev_idx, drmd_context_t *drmd, void *p)
{
	mac_report_t *mrpt = &drmd->mrpt;
	int ret = 0;
	char *string = (char *)p;

	ret = sprintf(string, "%lu", mrpt->phy_ctrl_chan.num_irng_rsp);
	xprintf(SDK_DBG, "InitRangeResp=%s\n", string);

	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/WiMAX/InitRangeResp", string, ret);
	return ret;
}

int drmd_Get_InitRangeNoResp(int dev_idx, drmd_context_t *drmd, void *p)
{
	mac_report_t *mrpt = &drmd->mrpt;
	int ret = 0;
	char *string = (char *)p;

	ret = sprintf(string, "%lu", (mrpt->phy_ctrl_chan.num_irng_sent - mrpt->phy_ctrl_chan.num_irng_rsp));
	xprintf(SDK_DBG, "InitRangeNoResp=%s\n", string);

	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/WiMAX/InitRangeNoResp", string, ret);
	return ret;
}

int drmd_Get_PerRangeResp(int dev_idx, drmd_context_t *drmd, void *p)
{
	mac_report_t *mrpt = &drmd->mrpt;
	int ret = 0;
	char *string = (char *)p;

	ret = sprintf(string, "%lu", mrpt->phy_ctrl_chan.num_prng_rsp);
	xprintf(SDK_DBG, "PerRangeResp=%s\n", string);

	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/WiMAX/PerRangeResp", string, ret);
	return ret;
}

int drmd_Get_PerRangeNoResp(int dev_idx, drmd_context_t *drmd, void *p)
{
	mac_report_t *mrpt = &drmd->mrpt;
	int ret = 0;
	char *string = (char *)p;

	ret = sprintf(string, "%lu", (mrpt->phy_ctrl_chan.num_prng_sent - mrpt->phy_ctrl_chan.num_prng_rsp));
	xprintf(SDK_DBG, "PerRangeNoResp=%s\n", string);

	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/WiMAX/PerRangeNoResp", string, ret);
	return ret;
}

int drmd_Get_HOSuccess(int dev_idx, drmd_context_t *drmd, void *p)
{
	mac_report_t *mrpt = &drmd->mrpt;
	int ret = 0;
	char *string = (char *)p;

	ret = sprintf(string, "%u", mrpt->handover.ho_count);
	xprintf(SDK_DBG, "HOSuccess=%s\n", string);

	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/WiMAX/HOSuccess", string, ret);
	return ret;
}

int drmd_Get_HOFail(int dev_idx, drmd_context_t *drmd, void *p)
{
	mac_report_t *mrpt = &drmd->mrpt;
	int ret = 0;
	char *string = (char *)p;

	ret = sprintf(string, "%u", mrpt->handover.ho_fail_count);
	xprintf(SDK_DBG, "HOFail=%s\n", string);

	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/WiMAX/HOFail", string, ret);
	return ret;
}

int drmd_Get_MAPRecSuccess(int dev_idx, drmd_context_t *drmd, void *p)
{
	mac_report_t *mrpt = &drmd->mrpt;
	int ret = 0;
	char *string = (char *)p;

	ret = sprintf(string, "%lu", (mrpt->phy_burst.num_dl_map + mrpt->phy_burst.num_ul_map));
	xprintf(SDK_DBG, "MAPRecSuccess=%s\n", string);

	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/WiMAX/MAPRecSuccess", string, ret);
	return ret;
}

int drmd_Get_MAPRecFail(int dev_idx, drmd_context_t *drmd, void *p)
{
	mac_report_t *mrpt = &drmd->mrpt;
	int ret = 0;
	char *string = (char *)p;

	ret = sprintf(string, "%lu", (mrpt->phy_burst.num_dl_map_error + mrpt->phy_burst.num_ul_map_error));
	xprintf(SDK_DBG, "MAPRecFail=%s\n", string);

	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/WiMAX/MAPRecFail", string, ret);
	return ret;
}

int drmd_Get_MCSstatsDL(int dev_idx, drmd_context_t *drmd, void *p)
{
	mac_report_t *mrpt = &drmd->mrpt;
	char *string = (char *)p;
	char szData[256];
	int nCurPos = 0;
	int i, j, k;

	for (i = 0 ; i < OFDMA_FEC_MODE_CNT ; i++)
	{
		for (j = 0 ; j < REPETITION_CODING_CNT ; j++)
		{
			for (k = 0 ; k < MIMO_TYPE_CNT ; k++)
			{
				if (mrpt->phy_mcs.dl_used[i][j][k])
				{
					memset(szData, 0x00, sizeof(szData));
					sprintf(szData, "%02x,%02x,%02x,%lu,", i, j, k, mrpt->phy_mcs.dl[i][j][k].num_burst);
					sprintf(string + nCurPos, "%s", szData);
					nCurPos += strlen(szData);
				}
			}
		}
	}
	if (0 < nCurPos) {
		string[nCurPos-1] = '\0';
		nCurPos--;
	}
	xprintf(SDK_DBG, "MCSstatsDL=%s\n", string);

	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/WiMAX/MCSstatsDL", string, nCurPos);
	return nCurPos;
}

int drmd_Get_MCSstatsUL(int dev_idx, drmd_context_t *drmd, void *p)
{
	mac_report_t *mrpt = &drmd->mrpt;
	char *string = (char *)p;
	char szData[256];
	int nCurPos = 0;
	int i, j, k;

	for (i = 0 ; i < OFDMA_FEC_MODE_CNT ; i++)
	{
		for (j = 0 ; j < REPETITION_CODING_CNT ; j++)
		{
			for (k = 0 ; k < MIMO_TYPE_CNT ; k++)
			{
				if (mrpt->phy_mcs.ul_used[i][j][k])
				{
					memset(szData, 0x00, sizeof(szData));
					sprintf(szData, "%02x,%02x,%02x,%lu,", i, j, k, mrpt->phy_mcs.ul[i][j][k].num_burst);
					sprintf(string + nCurPos, "%s", szData);
					nCurPos += strlen(szData);
				}
			}
		}
	}
	if (0 < nCurPos) {
		string[nCurPos-1] = '\0';
		nCurPos--;
	}
	xprintf(SDK_DBG, "MCSstatsUL=%s\n", string);

	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/WiMAX/MCSstatsUL", string, nCurPos);
	return nCurPos;
}

#define VERSION_OF_MCS_METRIC 1
int drmd_Get_VersionOfMCSMetric(int dev_idx, drmd_context_t *drmd, void *p)
{
	int ret = 0;
	char *string = (char *)p;

	ret = sprintf(string, "%d", VERSION_OF_MCS_METRIC);
	xprintf(SDK_DBG, "VersionOfMCSMetric=%s\n", string);

	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/WiMAX/VersionOfMCSMetric", string, ret);
	return ret;
}

int drmd_Get_DownlinkDataRate(int dev_idx, drmd_context_t *drmd, void *p)
{
	int ret = 0;
	char *string = (char *)p;

	ret = sprintf(string, "%lu,%lu,%lu,,,%lu,%lu", 
		0 == drmd->downlink.curr_rate ? 0 : drmd->downlink.curr_rate /drmd->downlink.msmnt_period * 1000 ,
		drmd->downlink.peak_rate, 
		0 == drmd->downlink.avrg_rate ? 0 : drmd->downlink.avrg_rate / drmd->downlink.ext_msmnt_period * 1000, 
		drmd->downlink.msmnt_period, 
		drmd->downlink.ext_msmnt_period);

	xprintf(SDK_DBG, "DownlinkDataRate=%s\n", string);

	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/WiMAX/DownlinkDataRate", string, ret);
	return ret;
}

int drmd_Get_UplinkDataRate(int dev_idx, drmd_context_t *drmd, void *p)
{
	int ret = 0;
	char *string = (char *)p;

	ret = sprintf(string, "%lu,%lu,%lu,,,%lu,%lu", 
		drmd->uplink.curr_rate, drmd->uplink.peak_rate, drmd->uplink.avrg_rate, 
		drmd->uplink.msmnt_period, drmd->uplink.ext_msmnt_period);

	xprintf(SDK_DBG, "UplinkDataRate=%s\n", string);

	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/WiMAX/UplinkDataRate", string, ret);
	return ret;

}

int drmd_Get_PacketsReceived(int dev_idx, drmd_context_t *drmd, void *p)
{
	mac_report_t *mrpt = &drmd->mrpt;
	int ret = 0;
	char *string = (char *)p;

	ret = sprintf(string, "%lu", mrpt->pdu_statistics.num_rx_pdu);
	xprintf(SDK_DBG, "PacketsReceived=%s\n", string);

	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/WiMAX/PacketsReceived", string, ret);
	return ret;
}

int drmd_Get_PacketsSent(int dev_idx, drmd_context_t *drmd, void *p)
{
	mac_report_t *mrpt = &drmd->mrpt;
	int ret = 0;
	char *string = (char *)p;

	ret = sprintf(string, "%lu", mrpt->pdu_statistics.num_tx_pdu);
	xprintf(SDK_DBG, "PacketsSent=%s\n", string);
	
	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/WiMAX/PacketsSent", string, ret);
	return ret;
}

int drmd_Get_MACState(int dev_idx, drmd_context_t *drmd, void *p)
{
	int ret = 0;
	char *string = (char *)p;
	char *s;
	mac_state_t state; 

	int m_status, c_status;

	if (sdk_get_status(NULL, dev_idx, &m_status, &c_status) < 0)
		return ret;

	switch(m_status) {
	case M_INIT:
	case M_OPEN_OFF:
	case M_OPEN_ON:
		state = idle;
		break;
	case M_SCAN:
		state = scanning;
		break;
	case M_CONNECTING:
		state = network_entry;
		break;
	case M_CONNECTED:
		state = normal;
		break;
	}

	switch (state) {
		case scanning:
			s = "scanning";
			break;
		case synchronization:
			s = "synchronization";
			break;
		case network_entry:
			s = "network entry";
			break;
		case normal:
			s = "normal";
			break;
		case handover:
			s = "handover";
			break;
		case idle:
			s = "idle";
			break;
		default:
			s = "";
			break;
	}
	ret = sprintf(string, "%s", s);
	xprintf(SDK_DBG, "val=%s\n", string);

	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/WiMAX/MACState", string, ret);
	return ret;
}

int drmd_Get_PreambleIndex(int dev_idx, drmd_context_t *drmd, void *p)
{
	mac_report_t *mrpt = &drmd->mrpt;
	int ret = 0;
	char *string = (char *)p;

	ret = sprintf(string, "%02X", mrpt->phy_mac_basic.current_pi);
	xprintf(SDK_DBG, "PreambleIndex=%s\n", string);

	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/WiMAX/PreambleIndex", string, ret);
	return ret;
}

int drmd_Get_HOLatency(int dev_idx, drmd_context_t *drmd, void *p)
{
	mac_report_t *mrpt = &drmd->mrpt;
	int ret = 0;
	char *string = (char *)p;

	ret = sprintf(string, "%u", mrpt->handover.ho_latency);
	xprintf(SDK_DBG, "HOLatency=%s\n", string);

	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/WiMAX/HOLatency", string, ret);
	return ret;
}

int drmd_Get_FrameRatio(int dev_idx, drmd_context_t *drmd, void *p)
{
	mac_report_t *mrpt = &drmd->mrpt;
	int ret = 0;
	char *string = (char *)p;

	ret = sprintf(string, "%d", 0 == mrpt->phy_mac_basic.num_dl_symbol ?  0 : (mrpt->phy_mac_basic.num_dl_symbol / mrpt->phy_mac_basic.num_ul_symbol));
	xprintf(SDK_DBG, "FrameRatio=%s\n", string);

	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/WiMAX/FrameRatio", string, ret);
	return ret;
}

int drmd_Get_HOAttempts(int dev_idx, drmd_context_t *drmd, void *p)
{
	mac_report_t *mrpt = &drmd->mrpt;
	int ret = 0;
	char *string = (char *)p;

	ret = sprintf(string, "%u", mrpt->handover.ho_count);
	xprintf(SDK_DBG, "HOAttempts=%s\n", string);
	
	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/WiMAX/HOAttempts", string, ret);
	return ret;
}

int drmd_Get_NetworkEntryLatency(int dev_idx, drmd_context_t *drmd, void *p)
{
	mac_report_t *mrpt = &drmd->mrpt;
	int ret = 0;
	char *string = (char *)p;
	
	ret = sprintf(string, "%u", mrpt->net_entry_stat.rng_time * 5);
	xprintf(SDK_DBG, "val=%s\n", string);
	
	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/WiMAX/NetworkEntryLatency", string, ret);
	return ret;
}

int drmd_Get_NetworkEntrySucceses(int dev_idx, drmd_context_t *drmd, void *p)
{
	mac_report_t *mrpt = &drmd->mrpt;
	int ret = 0;
	char *string = (char *)p;

	ret = sprintf(string, "%lu", mrpt->phy_ctrl_chan.num_irng_req_success);
	xprintf(SDK_DBG, "NetworkEntrySucceses=%s\n", string);

	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/WiMAX/NetworkEntrySucceses", string, ret);
	return ret;
}

int drmd_Get_NetworkEntryFailures(int dev_idx, drmd_context_t *drmd, void *p)
{
	mac_report_t *mrpt = &drmd->mrpt;
	int ret = 0;
	char *string = (char *)p;

	ret = sprintf(string, "%lu", (mrpt->phy_ctrl_chan.num_irng_req - mrpt->phy_ctrl_chan.num_irng_req_success));
	xprintf(SDK_DBG, "NetworkEntryFailures=%s\n", string);
	
	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/WiMAX/NetworkEntryFailures", string, ret);
	return ret;
}

int drmd_Get_NetworkEntryAttempts(int dev_idx, drmd_context_t *drmd, void *p)
{
	mac_report_t *mrpt = &drmd->mrpt;
	int ret = 0;
	char *string = (char *)p;

	ret = sprintf(string, "%lu", mrpt->phy_ctrl_chan.num_irng_req);
	xprintf(SDK_DBG, "NetworkEntryAttempts=%s\n", string);
	
	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/WiMAX/NetworkEntryAttempts", string, ret);
	return ret;
}

int drmd_Get_UserAccessTime(int dev_idx, drmd_context_t *drmd, void *p)
{
	int ret = 0;
	char *string = (char *)p;

	device_t *dev;

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	ret = sprintf(string, "%d", (dev->wimax->dev_info.oma_dm.connected_time_ms - dev->wimax->scan.last_scan_ms));
	xprintf(SDK_DBG, "UserAccessTime=%s\n", string);

	dm_put_dev(dev_idx);

	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/WiMAX/UserAccessTime", string, ret);
	return ret;
}

int drmd_Get_Generic_RateLimiterStats(int dev_idx, drmd_context_t *drmd, void *p)
{
	mac_report_t *mrpt = &drmd->mrpt;
	int ret = 0;
	char *string = (char *)p;

	ret = sprintf(string, "%lu", mrpt->phy_ul_stat.num_tx_sdu_drop);
	xprintf(SDK_DBG, "val=%s\n", string);

	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/Generic/RateLimiterStats", string, ret);
	return ret;
}

int drmd_Get_Generic_TimeActive(int dev_idx, drmd_context_t *drmd, void *p)
{
	mac_report_t *mrpt = &drmd->mrpt;
	int ret = 0;
	char *string = (char *)p;

	ret = sprintf(string, "%lu", mrpt->phy_mac_basic.time_active);
	xprintf(SDK_DBG, "TimeActive=%s\n", string);

	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/Generic/TimeActive", string, ret);
	return ret;
}

int drmd_Get_Generic_TimeIdle(int dev_idx, drmd_context_t *drmd, void *p)
{
	mac_report_t *mrpt = &drmd->mrpt;
	int ret = 0;
	char *string = (char *)p;

	ret = sprintf(string, "%lu", mrpt->phy_mac_basic.time_idle);
	xprintf(SDK_DBG, "TimeIdle=%s\n", string);

	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/Generic/TimeIdle", string, ret);
	return ret;
}

int drmd_Get_Generic_TimeSleep(int dev_idx, drmd_context_t *drmd, void *p)
{
	mac_report_t *mrpt = &drmd->mrpt;
	int ret = 0;
	char *string = (char *)p;

	ret = sprintf(string, "%lu", mrpt->phy_mac_basic.time_sleep);
	xprintf(SDK_DBG, "TimeSleep=%s\n", string);

	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/Generic/TimeSleep", string, ret);
	return ret;
}

int drmd_Get_Generic_LastRebootCause(int dev_idx, drmd_context_t *drmd, void *p)
{
	int ret = 0;
	char *string = (char *)p;

	ret = sprintf(string, "power on");
	xprintf(SDK_DBG, "LastRebootCause=%s\n", string);

	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/Generic/TimeSleep", string, ret);
	return ret;
}

int drmd_Get_Generic_DeviceTemp(int dev_idx, drmd_context_t *drmd, void *p)
{
	mac_report_t *mrpt = &drmd->mrpt;
	int ret = 0;
	char *string = (char *)p;

	ret = sprintf(string, "%d", mrpt->phy_temp_adc.temperature);
	xprintf(SDK_DBG, "DeviceTemp=%s\n", string);

	//ODM_SetNodeValue(dev_idx, URI_WiMAX_Diagnostics"/Generic/DeviceTemp", string, ret);
	return ret;
}

static int drmd_retrieve_modem_report(int dev_idx, mac_report_t *mrpt, u8 *buf, int len)
{
	device_t *dev;
	u8 T, *V;
	u16 L;
	int pos = 0, ret = -1;

	xfunc_in();

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;
	while (pos < len) {
		pos += hci_get_tlv(&buf[pos], &T, &L, &V);
		switch (T) {
			case TLV_T(W_PDU_STATISTIC):
				ret = wm_retrieve_mac_pdu_statistics(&mrpt->pdu_statistics, V, L);
				break;
			case TLV_T(W_PHY_MAC_BASIC):
				ret = wm_retrieve_mac_phy_mac_basic(&mrpt->phy_mac_basic, V, L);
				break;
			case TLV_T(W_POWER_CONTROL):
				ret = wm_retrieve_mac_power_ctrl(&mrpt->power_ctrl, V, L);
				break;
			case TLV_T(W_PHY_BURST):
				ret = wm_retrieve_mac_phy_burst(&mrpt->phy_burst, V, L);
				break;
			case TLV_T(W_PHY_MSC):
				ret = wm_retrieve_mac_phy_mcs(&mrpt->phy_mcs, V, L);
				break;
			case TLV_T(W_PHY_ZONE):
				ret = wm_retrieve_mac_phy_zone(&mrpt->phy_zone, V, L);
				break;
			case TLV_T(W_PHY_CINR_RSSI):
				ret = wm_retrieve_mac_phy_cinr_rssi(&mrpt->phy_cinr_rssi, V, L);
				break;
			case TLV_T(W_TEMP_ADC):
				ret = wm_retrieve_mac_phy_temp_adc(&mrpt->phy_temp_adc, V, L);
				break;
			case TLV_T(W_PHY_CTRL_CHANNEL):
				ret = wm_retrieve_mac_phy_ctrl_chan(&mrpt->phy_ctrl_chan, V, L);
				break;
			case TLV_T(W_DL_SF_STATISTICS):
				ret = wm_retrieve_mac_phy_dl_statistics(&mrpt->phy_dl_stat, V, L);
				break;
			case TLV_T(W_UL_SF_STATISTICS):
				ret = wm_retrieve_mac_phy_ul_statistics(&mrpt->phy_ul_stat, V, L);
				break;
			case TLV_T(W_HANDOVER):
				ret = wm_retrieve_mac_handover(&mrpt->handover, V, L);
				break;
			case TLV_T(W_NEIGHBOR_LIST):
				ret = wm_retrieve_mac_neighbor(&mrpt->neighbor, V, L,
					dev->wimax->conn_comp.net_id.bs.id);
				break;
			case TLV_T(W_HARQ_STATISTICS):
				ret = wm_retrieve_mac_harq_statistics(&mrpt->harq_stat, V, L);
				break;
			case TLV_T(W_NETENTRY_STATISTICS):
				ret = wm_retrieve_mac_net_entry_statistics(&mrpt->net_entry_stat, V, L);
				break;
			default:
				ret = -1;
		}
		xprintf(SDK_DBG, "Type(%02x): L=%u, ret=%d\n", T, L, ret);
		if (ret < 0) {
			xprintf(SDK_NOTICE, "Unknown Type=0x%02x\n", T);
			break;
		}
	}
	dm_put_dev(dev_idx);
	xfunc_out("ret=%d", ret);
	return ret;
}

static int get_mac_report(int dev_idx, mac_report_t *mrpt)
{
	u8 buf[HCI_MAX_PACKET], *p = buf;
	int ret;

	*p++ = MODE_MODEM_REPORT;
	*p++ = W_PDU_STATISTIC;
	*p++ = W_PHY_MAC_BASIC;
	*p++ = W_POWER_CONTROL;
	*p++ = W_PHY_BURST;
	*p++ = W_PHY_MSC;
	*p++ = W_PHY_ZONE;
	*p++ = W_PHY_CINR_RSSI;
	*p++ = W_TEMP_ADC;
	*p++ = W_PHY_CTRL_CHANNEL;
	*p++ = W_DL_SF_STATISTICS;
	*p++ = W_UL_SF_STATISTICS;
	*p++ = W_HANDOVER;
	*p++ = W_NEIGHBOR_LIST;
	*p++ = W_HARQ_STATISTICS;
	*p++ = W_NETENTRY_STATISTICS;
	ret = hci_send_wait(dev_idx, WIMAX_MODEM_REQUEST, buf, p-buf,
		WIMAX_MODEM_REPORT, buf, sizeof(buf), 0, 0, 5);
	if (ret <= HCI_HEADER_SIZE) {
		xprintf(SDK_ERR, "HCI response error: len=%d\n", ret);
		ret = -1;
		goto out;
	}

	ret = drmd_retrieve_modem_report(dev_idx, mrpt, buf, ret);
out:
	return ret;
}

static void drmd_check_data_rate(drmd_context_t *drmd)
{
	// DownlinkDataRate
	drmd->downlink.curr_rate = drmd->mrpt.pdu_statistics.num_tx_pdu;
	if (drmd->downlink.peak_rate < drmd->mrpt.pdu_statistics.num_tx_pdu) 
		drmd->downlink.peak_rate = drmd->mrpt.pdu_statistics.num_tx_pdu;
	
	// UplinkDataRate
	drmd->uplink.curr_rate = drmd->mrpt.pdu_statistics.num_rx_pdu;
	if (drmd->uplink.peak_rate < drmd->mrpt.pdu_statistics.num_rx_pdu) 
		drmd->uplink.peak_rate = drmd->mrpt.pdu_statistics.num_rx_pdu;
}

static void *drmd_metrics_collector(void *data)
{
	#define timeval2timespec(tv,ts)	\
		((ts)->tv_sec = (tv)->tv_sec, (ts)->tv_nsec = (tv)->tv_usec * 1000)

	drmd_thread_arg_t *thread_arg = (drmd_thread_arg_t *)data;
	int dev_idx = thread_arg->dev_idx;
	int duration = thread_arg->duration;
	device_t *dev;
	drmd_context_t *drmd;
	int m_status, c_status;
	u32	timestamp;

	free(thread_arg);

	xfunc_in();

	if (!(dev = dm_get_dev(dev_idx)))
		return NULL;

	drmd = dev->wimax->dev_info.oma_dm.drmd;

	memset(&drmd->mrpt, 0x00, sizeof(mac_report_t));
	memset(&drmd->downlink, 0x00, sizeof(drmd_data_rate_t));
	memset(&drmd->uplink, 0x00, sizeof(drmd_data_rate_t));

	drmd->timestamp_s = gettimemsofday();
	
	if (1 == duration) { 
		if (get_mac_report(dev_idx, &drmd->mrpt) < 0)
			goto out;

		drmd_check_data_rate(drmd);
	}
	else if (2 == duration) { 
		while(1)
		{
			if (get_mac_report(dev_idx, &drmd->mrpt) < 0)
				goto out;

			drmd_check_data_rate(drmd);

			if (sdk_get_status(NULL, dev_idx, &m_status, &c_status) < 0)
				goto out;
			
			if (m_status != M_CONNECTED)
				break;	

			sleep(1);
		}
	}
	else if (5 <= duration) {
		timestamp = gettimemsofday();
		while(1)
		{
			if (get_mac_report(dev_idx, &drmd->mrpt) < 0)
				goto out;

			drmd_check_data_rate(drmd);

			if (sdk_get_status(NULL, dev_idx, &m_status, &c_status) < 0)
				goto out;
			
			if (m_status != M_CONNECTED)
				goto out;	

			if ( (gettimemsofday() - timestamp) > duration * 1000)
				break;
		
			sleep(1);
		}
	}

	drmd->timestamp_e = gettimemsofday();

	if (1 == duration || 2 == duration) { 
		drmd->downlink.msmnt_period = drmd->timestamp_e - drmd->timestamp_s;
		drmd->downlink.ext_msmnt_period = 0;
	}
	else if (5 <= duration) {
		drmd->downlink.msmnt_period = duration * 1000;
		drmd->downlink.ext_msmnt_period  = (drmd->timestamp_e - drmd->timestamp_s);
	}
	
out:
	dm_put_dev(dev_idx);
	xfunc_out();
	return NULL;
}

int dm_drmd_start(int dev_idx)
{
	device_t *dev;
	wm_oma_dm_t *odm;
	drmd_thread_arg_t *thread_arg;
	int duration = 0;
	int ret = -1;

	sdk_internal_t *sdk = sdk_get_rw_handle();
	dev_hand_t dev_hand;

	xfunc_in();

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	odm = &dev->wimax->dev_info.oma_dm;

	duration = odm->drmd->duration;

	if (0 == duration) {
		xprintf(SDK_INFO, "DRMD was canceled because value of 0 is ignored executive command\n");
		goto out;
	} else if (3 == duration || 4 == duration) {
		xprintf(SDK_INFO, "DRMD was canceled because values of 3 & 4 are reserved for use in futured\n");
		goto out;
	}

	thread_arg = malloc(sizeof(drmd_thread_arg_t));
	thread_arg->dev_idx = dev_idx;
	thread_arg->duration = duration;
	
	odm = &dev->wimax->dev_info.oma_dm;
	pthread_create(&odm->drmd_thread, NULL, drmd_metrics_collector, (void *)thread_arg);

	// Indicate DRMD start to CM
	if (sdk->ind.odm_start_drmd) {
		dev_hand.api = sdk->api;
		dev_hand.dev_idx = dev_idx;
		xprintf(SDK_DBG, "Call callback (odm_start_drmd, duration=%d)\n", duration);
		sdk->ind.odm_start_drmd(&dev_hand, duration);
	}

	ret = 0;

out:
	dm_put_dev(dev_idx);
	xfunc_out();
	return ret;
}

void dm_drmd_cancel(int dev_idx)
{
	device_t *dev;
	wm_oma_dm_t *odm;
	pthread_t thread;

	xfunc_in();
	if (!(dev = dm_get_dev(dev_idx)))
		return;
	odm = &dev->wimax->dev_info.oma_dm;

	if ((thread = odm->drmd_thread)) {
		odm->drmd_thread = (pthread_t) NULL;
		pthread_cancel(thread);
		pthread_join(thread, NULL);
	}

	dm_put_dev(dev_idx);
	xfunc_out();

}

int dm_drmd_ind_connection(int dev_idx, bool connected)
{
	if (connected) {	/*Connected*/
	}
	else {	/*Disconnected*/
	}
	return 0;
}

int dm_drmd_add_node(int dev_idx)
{
	int i = 0;

	for ( i = 0; i < numof_array(drmd_node_list) ; i++)
		if(ODM_AddNodeByType(dev_idx, drmd_node_list[i].uri, drmd_node_list[i].format))
			return -1;

	return 0;
} 

int dm_drmd_set_node(int dev_idx, char *uri, void *data, int len)
{
	device_t *dev;
	wm_oma_dm_t *odm;
	int ret = -1;

	int i = 0;
	
	if (!(dev = dm_get_dev(dev_idx)))
		return -1;
	odm = &dev->wimax->dev_info.oma_dm;

	for ( i = 0; i < numof_array(drmd_node_list) ; i++) {
		if(!strcmp(uri, drmd_node_list[i].uri)) {

			if(drmd_node_list[i].drmd_set_value) {

				drmd_node_list[i].drmd_set_value(dev_idx, odm->drmd, data);
				ret = 0;
				goto out;
			}
			else
			{
				ret = -1;
				goto out;
			}
		}
	}

	ret = -1;

out:

	dm_put_dev(dev_idx);
	xfunc_out();
	return ret;
}

int dm_drmd_get_node(int dev_idx, char *uri, void *data, int *plen)
{
#define MAX_DRMD_DATA_LEN 6720

	device_t *dev;
	wm_oma_dm_t *odm;
	int ret = -1;

	int i = 0;
	int len = 0;
	void *val;

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;
	odm = &dev->wimax->dev_info.oma_dm;

	val = (void *)malloc(MAX_DRMD_DATA_LEN);

	for ( i = 0; i < numof_array(drmd_node_list) ; i++) {
		if(!strcmp(uri, drmd_node_list[i].uri)) {

			if(drmd_node_list[i].drmd_get_value) {

				len = drmd_node_list[i].drmd_get_value(dev_idx, odm->drmd, val);

				if (NULL == data || 0 == *plen) {
					*plen = len;
					ret = 0;
					goto out;
				}

				memcpy(data, val, len);
				((char*)data)[len] = '\0';
				*plen = len;

				ret = 0;
				goto out;
			}
			else
			{
				ret = -1;
				goto out;
			}
		}
	}

	ret = -1;
out:

	free(val);
	dm_put_dev(dev_idx);
	xfunc_out();
	return ret;
}


