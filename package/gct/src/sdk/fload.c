#include <zlib.h>
#include "global.h"
#include "error.h"
#include "wimax.h"
#include "wm_ioctl.h"
#include "io.h"
#include "device.h"
#include "hci.h"
#include "log.h"
#include "encryption.h"
#include "fload.h"

typedef const char * ld_file_t;

typedef struct {
	void *p;
	int size;
} ld_buf_t;

typedef struct {
	bool	is_file;
	union {
		ld_file_t path;
		ld_buf_t buf;
	} u;
} ld_dst_t;

#define END_OF_BLOCK			(-1)
#define END_OF_FILE				(-2)
#define SEQUENCE_READ			(-1)
#define PAYLOAD_HEAD_SIZE		sizeof(hci_image_payload_t)
#define PAYLOAD_DATA_LEN(n)		((n) - PAYLOAD_HEAD_SIZE)
#define RESPONSE_HEAD_SIZE		sizeof(hci_image_response_t)

static int read_file(const char *file, char *buf, int buf_size)
{
	int fd, len, total = 0;

	xfunc_in("file=%s, buf_size=%d", file, buf_size);

	if ((fd = open(file, O_RDONLY)) < 0) {
		xprintf(SDK_STD_ERR, "open(%s) fail\n", file);
		return -1;
	}
	while ((len = read(fd, &buf[total], buf_size-total)) > 0) {
		total += len;
		if (buf_size-total == 0)
			break;
	}

	if (len < 0)
		xprintf(SDK_STD_ERR, "read(%s) fail\n", file);

	close(fd);
	xfunc_out("total=%d", total);
	return total;
}

static int write_file(const char *file, char *buf, int buf_size)
{
	int fd, len, written = 0;

	xfunc_in("file=%s, buf_size=%d", file, buf_size);

	if ((fd = open(file, O_CREAT|O_WRONLY|O_TRUNC, 0644)) < 0) {
		xprintf(SDK_STD_ERR, "open(%s) fail\n", file);
		return -1;
	}
	while ((len = write(fd, &buf[written], buf_size-written)) > 0) {
		written += len;
		if (buf_size-written == 0)
			break;
	}

	if (len < 0)
		xprintf(SDK_STD_ERR, "write(%s) fail\n", file);

	close(fd);
	xfunc_out("written=%d", written);
	return written;
}

int gzip_file(const char *file, const char *zip_file)
{
	#define BUFFER_SIZE	4096
	gzFile out = NULL;
	int in = 0;
	int len, ret = 0, err;
	char buf[BUFFER_SIZE];

	xfunc_in("file=%s, zip_file=%s", file, zip_file);

	if ((in = open(file, O_RDONLY)) < 0) {
		xprintf(SDK_STD_ERR, "open(%s) fail\n", file);
		ret = -1;
		goto out;
	}
	
	if ((out = gzopen(zip_file, "wb6")) == NULL) {
		xprintf(SDK_ERR, "gzopen(%s) fail\n", zip_file);
		ret = -1;
		goto out;
	}

	while ((len = read(in, buf, sizeof(buf)))) {
		if (len < 0) {
			xprintf(SDK_STD_ERR, "read fail\n");
			ret = -1;
			break;
		}

		if (gzwrite(out, buf, len) != len) {
			xprintf(SDK_ERR, "gzwrite(%s) fail, err=%s\n", file, gzerror(out, &err));
			ret = -1;
			break;
		}
	}

out:
	if (in > 0)
		close(in);
	if (out)
		gzclose(out);
	xfunc_out("ret=%d", ret);
	return ret;
}

int gzip_buf(void *buf, int size, const char *zip_file)
{
	gzFile zfd = NULL;
	int len, ret = 0, err;
	char *p = (char *) buf;
	int rest = size;
	const int chuck_size = 4096;

	xfunc_in("size=%d, zip_file=%s", size, zip_file);
	
	if ((zfd = gzopen(zip_file, "wb6")) == NULL) {
		xprintf(SDK_ERR, "gzopen(%s) fail, err=%s\n", zip_file, gzerror(zfd, &err));
		ret = -1;
		goto out;
	}

	while (rest) {
		len = rest > chuck_size ? chuck_size : rest;
		rest -= len;
		
		if (gzwrite(zfd, p, len) != len) {
			xprintf(SDK_ERR, "gzwrite fail, err=%s\n", gzerror(zfd, &err));
			ret = -1;
			break;
		}

		p += len;
	}

out:
	if (zfd)
		gzclose(zfd);
	xfunc_out("ret=%d", ret);
	return ret;
}

int gunzip_file(const char *zip_file, const char *file)
{
	#define BUFFER_SIZE	4096
	gzFile in = NULL;
	int out = 0;
	int len, ret = 0, err;
	char buf[BUFFER_SIZE];

	xfunc_in("zip_file=%s, file=%s", zip_file, file);

	if ((in = gzopen(zip_file, "rb")) == NULL) {
		xprintf(SDK_STD_ERR, "gzopen(%s) fail\n", zip_file);
		ret = -1;
		goto out;
	}

	if ((out = open(file, O_CREAT|O_WRONLY|O_TRUNC, 0644)) < 0) {
		xprintf(SDK_STD_ERR, "open(%s) fail\n", file);
		ret = -1;
		goto out;
	}

	while ((len = gzread(in, buf, sizeof(buf)))) {
		if (len < 0) {
			xprintf(SDK_ERR, "gzread fail, err=%s\n", gzerror(in, &err));
			ret = -1;
			break;
		}

		if (write(out, buf, len) != len) {
			xprintf(SDK_STD_ERR, "write(%s) fail\n", file);
			ret = -1;
			break;
		}
	}
out:
	if (in)
		gzclose(in);
	if (out > 0)
		close(out);
	xfunc_out("ret=%d", ret);
	return ret;
}

int decrypt_file(const char *mac_addr, const char *enc_file, const char *file)
{
	#define HEAD_SIZE	4
	#define MAX_BUF_SIZE	4096
	int in = 0, out = 0;
	int ret = -1, readn, len, written = 0, woffs;
	u32 filesize = 0;
	char rbuf[MAX_BUF_SIZE];
	char wbuf[MAX_BUF_SIZE];

	xfunc_in("enc_file=%s, file=%s", enc_file, file);

	xprintf_mac(SDK_DBG, "mac", mac_addr);

	if ((in = open(enc_file, O_RDONLY)) < 0) {
		xprintf(SDK_STD_ERR, "open(%s) fail\n", enc_file);
		goto out;
	}

	if ((out = open(file, O_CREAT|O_WRONLY|O_TRUNC, 0644)) < 0) {
		xprintf(SDK_STD_ERR, "open(%s) fail\n", file);
		goto out;
	}

	ret = 0;
	readn = sizeof(rbuf);
	while ((readn = read(in, rbuf, readn))) {
		if (readn < 0) {
			xprintf(SDK_STD_ERR, "read(%s) fail\n", enc_file);
			ret = -1;
			break;
		}

		woffs = !filesize ? HEAD_SIZE : 0;

		len = 0;
		while (readn > len)
			len += GCT_Key_Dec((uint1 *)mac_addr, (uint1 *)&rbuf[len],
								(uint1 *)&wbuf[len]);
		if (readn != len) {
			xprintf(SDK_ERR, "Invalid cert file(%d!=%d)\n", readn, len);
			ret = -1;
			goto out;
		}

		if (!filesize) {
			memcpy(&filesize, wbuf, 4);
			filesize = DB2H(filesize);
			xprintf(SDK_DBG, "decrypt_file filesize=%d\n", filesize);
		}

		readn -= woffs;
		if (write(out, &wbuf[woffs], readn) != readn) {
			xprintf(SDK_STD_ERR, "write(%s) fail\n", file);
			ret = -1;
			break;
		}
		written += readn;
		readn = sizeof(rbuf);
	}

	if (!ret) {
		if (filesize > written+HEAD_SIZE) {
			xprintf(SDK_ERR, "File size is wrong(%d>%d)\n", filesize, written);
			ret = -1;
		}
		else if ((ret = ftruncate(out, filesize)) < 0) {
			xprintf(SDK_STD_ERR, "ftruncate(%s) fail, (%d=>%d)\n", file, written, filesize);
		}
	}

out:
	if (in > 0)
		close(in);
	if (out > 0)
		close(out);
	xfunc_out("ret=%d", ret);
	return ret;
}

int encrypt_file(const char *mac_addr, const char *file, const char *enc_file)
{
	#define HEAD_SIZE	4
	#define MAX_BUF_SIZE	4096
	int in = 0, out = 0;
	int ret = -1, len, written = 0, writing, rest;
	u32 filesize, no_aligned;
	char *filebuf = NULL;
	char wbuf[MAX_BUF_SIZE];

	xfunc_in("enc_file=%s, file=%s", enc_file, file);

	xprintf_mac(SDK_DBG, "mac", mac_addr);

	if ((in = open(file, O_RDONLY)) < 0) {
		xprintf(SDK_STD_ERR, "gzopen(%s) fail\n", file);
		goto out;
	}
	filesize = ret = lseek(in, 0, SEEK_END);
	if (ret <= 0) {
		xprintf(SDK_STD_ERR, "lseek(%s) failed(%d)\n", file, ret);
		goto out;
	}
	lseek(in, 0, SEEK_SET);
	filebuf = sdk_malloc(filesize + GCT_KEY_ENC_UNIT);

	ret = read_file(file, &filebuf[HEAD_SIZE], filesize + GCT_KEY_ENC_UNIT - HEAD_SIZE);
	if (filesize != ret) {
		xprintf(SDK_STD_ERR, "Filesize mismatch %d!=%d\n", filesize, ret);
		ret = -1;
		goto out;
	}

	*(u32 *) filebuf = DH2B(filesize);
	filesize += HEAD_SIZE;
	if ((no_aligned = filesize % GCT_KEY_ENC_UNIT))
		filesize += GCT_KEY_ENC_UNIT - no_aligned;

	if ((out = open(enc_file, O_CREAT|O_WRONLY|O_TRUNC, 0644)) < 0) {
		xprintf(SDK_STD_ERR, "open(%s) fail\n", enc_file);
		ret = -1;
		goto out;
	}

	rest = filesize;
	while (rest > 0) {
		writing = rest > sizeof(wbuf) ? sizeof(wbuf) : rest;
		assert(!(writing % GCT_KEY_ENC_UNIT));
		len = 0;
		while (writing > len)
			len += GCT_Key_Enc((uint1 *)mac_addr, (uint1 *)&filebuf[written+len],
								(uint1 *)&wbuf[len]);

		if (write(out, wbuf, len) != len) {
			xprintf(SDK_STD_ERR, "write(%s) fail\n", enc_file);
			ret = -1;
			break;
		}
		written += len;
		rest -= len;
	}

	if (filesize == written)
		ret = 0;
	xprintf(SDK_DBG, "Enc filesize=%d, written=%d\n", filesize, written);
out:
	if (filebuf)
		sdk_free(filebuf);
	if (in > 0)
		close(in);
	if (out > 0)
		close(out);
	xfunc_out("ret=%d", ret);
	return ret;
}

int decrypt_buf(const char *mac_addr, char *src, char *dst, int size)
{
	int ret, rest = size;
	char *r = src;
	char *w = dst;

	xfunc_in("size=%d", size);
	xprintf_mac(SDK_DBG, "mac", mac_addr);

	while (rest) {
		ret = GCT_Key_Dec((uint1 *)mac_addr, (uint1 *)r, (uint1 *)w);
		rest -= ret;
		r += ret;
		w += ret;
	}

	xfunc_out("ret=%d", size - rest);
	return size - rest;
}

static int do_upload(int dev_idx, int type, ld_dst_t *dst, void (*progress)(int bytes))
{
	device_t *dev;
	u8 buf[HCI_MAX_PACKET];
	hci_image_payload_t *img = (hci_image_payload_t *) buf;
	hci_image_response_t *result = (hci_image_response_t *) buf;
	int fd = 0;
	int ret = -1, len, filesize = 0, tmp, max_size = 0;
	char *p = NULL;
	int chk_len;
	u16 cmd;
	void *param;
	int plen;

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	xfunc_in("[%d]", dev_idx);

	if (dst->is_file) {
		if ((fd = open(dst->u.path, O_CREAT|O_WRONLY|O_TRUNC, 0644)) < 0) {
			xprintf(SDK_STD_ERR, "open(%s) fail\n", dst->u.path);
			goto out;
		}
	}
	else {
		if (!dst->u.buf.p || !dst->u.buf.size) {
			xprintf(SDK_ERR, "buf=%p, size=%d\n", dst->u.buf.p, dst->u.buf.size);
			goto out;
		}
		p = dst->u.buf.p;
		max_size = dst->u.buf.size;
	}

	pthread_mutex_lock(&dev->load_lock);

	img->type = H2B(type);
	chk_len = sizeof(img->type);
	memcpy(buf, &img->type, chk_len);
	tmp = 0;
	memcpy(&img->offset, &tmp, 4);

	cmd = WIMAX_UL_IMAGE;
	param = img;
	plen = sizeof(*img);
	while (1) {
		ret = hci_send_wait(dev_idx, cmd, param, plen,
			WIMAX_UL_IMAGE_RESULT, buf, sizeof(buf), chk_len, 0, 3);
		if (ret < PAYLOAD_HEAD_SIZE)
			break;

		if (!(len = PAYLOAD_DATA_LEN(ret))) {
			ret = 0;
			break;
		}

		if (dst->is_file) {
			ret = write(fd, img->data, len);
			if (ret != len) {
				xprintf(SDK_ERR, "Write fail %s(%d!=%d)\n", dst->u.path, ret, len);
				ret = -1;
				break;
			}
		}
		else {
			if (filesize + len > max_size) {
				xprintf(SDK_ERR, "buffer is too small(%d > %d)\n",
					filesize + len, max_size);
				ret = -1;
				break;
			}
			memcpy(p+filesize, img->data, len);
		}
		
		filesize += len;
		if (progress)
			progress(filesize);

		result->type = H2B(type);
		tmp = DH2B(filesize);
		memcpy(&result->offset, &tmp, 4);
		tmp = 0;
		memcpy(&result->result, &tmp, 4);

		cmd = WIMAX_UL_IMAGE_STATUS;
		param = result;
		plen = sizeof(*result);
	}
	pthread_mutex_unlock(&dev->load_lock);

out:
	if (fd > 0)
		close(fd);
	xprintf(SDK_INFO, "Upload file size=%d\n", filesize);
	if (!ret) {
		if (!filesize)
			ret = -ENOENT;
		else
			ret = filesize;
	}

	xfunc_out("ret=%d", ret);
	dm_put_dev(dev_idx);
	return ret;
}

static int do_download(int dev_idx, int type, ld_dst_t *dst, void (*progress)(int bytes))
{
	device_t *dev;
	#define DL_DATA_CHUCK	1024
	u8 buf[HCI_MAX_PACKET];
	hci_image_payload_t *img = (hci_image_payload_t *) buf;
	hci_image_response_t *img_result = (hci_image_response_t *) buf;
	char *p = NULL;
	int fd = 0;
	int ret = -1, len, written = 0, rest = 0, result;
	s32 offs = 0, eof = 0, tmp;

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	if (type < DLIMG_OMA_XML)
		eof = END_OF_FILE;
	else
		eof = END_OF_BLOCK;

	xfunc_in("[%d]", dev_idx);

	if (dst->is_file) {
		if ((fd = open(dst->u.path, O_RDONLY)) < 0)
			goto out;
	}
	else {
		if (!dst->u.buf.p || !dst->u.buf.size)
			goto out;
		p = dst->u.buf.p;
		rest = dst->u.buf.size;
	}

	pthread_mutex_lock(&dev->load_lock);

	while (1) {
		img->type = H2B(type);
		tmp = DH2B(offs);
		memcpy(&img->offset, &tmp, 4);
		len = sizeof(*img);
		if (offs >= 0) {
			if (dst->is_file) {
				ret = read(fd, img->data, DL_DATA_CHUCK);
				if (ret < 0)
					break;
				else if (ret == 0) {
					tmp = DH2B(eof);
					memcpy(&img->offset, &tmp, 4);
				}
				else if (ret < DL_DATA_CHUCK)
					offs = eof;
				else
					offs += DL_DATA_CHUCK;
			}
			else {
				if (rest == 0) {
					tmp = DH2B(eof);
					memcpy(&img->offset, &tmp, 4);
					ret = 0;
				}
				else if (rest < DL_DATA_CHUCK) {
					ret = rest;
					memcpy(img->data, p+offs, rest);
					rest = 0;
					offs = eof;
				}
				else {
					ret = DL_DATA_CHUCK;
					memcpy(img->data, p+offs, ret);
					rest -= ret;
					offs += ret;
				}
			}
			len += ret;
			written += ret;
		}

		ret = hci_send_wait(dev_idx, WIMAX_DL_IMAGE, (u8 *)img, len,
			WIMAX_DL_IMAGE_STATUS, buf, sizeof(buf), 0, 0, 5);
		if (ret != RESPONSE_HEAD_SIZE)
			break;

		memcpy(&result, &img_result->result, 4);
		if (result) {
			xprintf(SDK_ERR, "Image result failed(%d)\n", result);
			ret = -1;
			break;
		}

		tmp = DH2B(eof);
		if (!memcmp(&img->offset, &tmp, 4)) {
			ret = 0;
			break;
		}

		if (progress)
			progress(written);
	}
	
	pthread_mutex_unlock(&dev->load_lock);

out:
	if (fd > 0)
		close(fd);
	xprintf(SDK_DBG, "written size=%d\n", written);
	xfunc_out("ret=%d", ret);
	if (ret)
		ret = -1;
	return ret;
}

static bool is_encrypted_file(const u8 *mac, const char *file, int filesize)
{
	u8 enc_buf[GCT_KEY_DEC_UNIT];
	u8 dec_buf[GCT_KEY_DEC_UNIT];
	s32 dec_filesize;
	int no_aligned;

	if (read_file(file, (char *)enc_buf, sizeof(enc_buf)) != sizeof(enc_buf))
		return FALSE;

	GCT_Key_Dec((uint1 *)mac, (uint1 *)enc_buf, (uint1 *)dec_buf);
	memcpy(&dec_filesize, dec_buf, sizeof(dec_filesize));
	dec_filesize = DB2H(dec_filesize);

	if ((no_aligned = (dec_filesize+sizeof(dec_filesize)) % GCT_KEY_DEC_UNIT))
		filesize -= sizeof(dec_filesize) + (GCT_KEY_DEC_UNIT - no_aligned);
	else
		filesize -= sizeof(dec_filesize);

	xprintf(SDK_DBG, "Check filesize: %d, %d\n", dec_filesize, filesize);
	return dec_filesize == filesize;
}

int fl_upload(int dev_idx, int type, const char *path, void (*progress)(int bytes))
{
	device_t *dev;
	u8 *mac;
	ld_dst_t dst;
	char gz[256];
	int ret;

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	xfunc_in("dev=%d, type=%d", dev_idx, type);

	mac = dev->wimax->dev_info.device_mac;

	dst.is_file = TRUE;
	dst.u.path = path;
	ret = do_upload(dev_idx, type, &dst, progress);
	xprintf(SDK_DBG, "%s: type=0x%04X, ret=%d\n", __FUNCTION__, type, ret);
	if (ret > 0) {
		if (type == DLIMG_OMA_XML && !is_encrypted_file(mac, path, ret))
			goto out;
		sprintf(gz, "%s%s", path, ".gz");

		ret = decrypt_file((char *) mac, path, gz);
		if (!ret)
			ret = gunzip_file(gz, path);

		unlink(gz);
	}

	if (ret < 0)
		unlink(path);
out:
	dm_put_dev(dev_idx);
	xfunc_out("ret=%d", ret);
	return ret;
}

int fl_download(int dev_idx, int type, const char *path, void (*progress)(int bytes))
{
	device_t *dev;
	u8 *mac;
	ld_dst_t dst;
	char enc[256];
	char gz[256];
	const char *src_enc;
	int ret = 0;

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	xfunc_in("dev=%d, type=%d", dev_idx, type);

	gz[0] = enc[0] = 0;

	mac = dev->wimax->dev_info.device_mac;

	dst.is_file = TRUE;
	dst.u.path = path;

	if (type == DLIMG_DEV_CERT || type == DLIMG_CERT1_U || type == DLIMG_CERT1_L ||
		type == DLIMG_CERT2_U || type == DLIMG_CERT_BIG || type == DLIMG_EAP_PARAM ||
		(type == DLIMG_OMA_XML && ENC_XML_ENABLED(dev))) {
		sprintf(enc, "%s%s", dst.u.path, ".enc");
		if (type == DLIMG_EAP_PARAM)
			src_enc = path;
		else {
			sprintf(gz, "%s%s", dst.u.path, ".gz");
			ret = gzip_file(dst.u.path, gz);
			src_enc = gz;
		}
		if (!ret)
			ret = encrypt_file((char *) mac, src_enc, enc);
		if (!ret) {
			dst.u.path = enc;
			ret = do_download(dev_idx, type, &dst, progress);
		}
		unlink(enc);
		if (gz[0])
			unlink(gz);
	}
	else
		ret = do_download(dev_idx, type, &dst, progress);

	dm_put_dev(dev_idx);
	xfunc_out("ret=%d", ret);
	return ret;
}

int bl_upload(int dev_idx, int type, void *buf, int size, void (*progress)(int bytes))
{
	device_t *dev;
	u8 *mac;
	char *tmp = NULL;
	ld_dst_t dst;
	char *tmp_file = "tmp.xml";
	int ret = 0;

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	xfunc_in("dev=%d, type=%d, buf=0x%08X, size=%d", dev_idx, type, buf, size);
	mac = dev->wimax->dev_info.device_mac;

	dst.is_file = FALSE;

	if (type == DLIMG_OMA_XML) {
		ret = fl_upload(dev_idx, type, tmp_file, NULL);
		ret = read_file(tmp_file, buf, size);
		unlink(tmp_file);
	}
	else if (type == DLIMG_EAP_PARAM) {
		if (size < EAP_NVPARAM_SIZE) {
			xprintf(SDK_ERR, "size(%d) is too small\n", size);
			goto out;
		}
		tmp = (char *) sdk_malloc(size+16/*rsvd*/);
		dst.u.buf.p = buf;
		dst.u.buf.size = size;
		ret = do_upload(dev_idx, type, &dst, progress);
 		if (ret > 0) {
			decrypt_buf((char *) mac, buf, tmp, ret);
			ret = U82U32(tmp);
			if ((u32)ret >= EAP_NVPARAM_SIZE) {
				xprintf(SDK_ERR, "eap param size(%d) is wrong\n", ret);
				goto out;
			}
			memcpy(buf, &tmp[4], ret);
 		}
	}
	else {
		xprintf(SDK_ERR, "not supported type(%d)\n", type);
		ret = -1;
	}

out:
	xfunc_out("type=%d, ret=%d", type, ret);
	if (tmp)
		sdk_free(tmp);
	dm_put_dev(dev_idx);
	return ret;
}

int bl_download(int dev_idx, int type, void *buf, int size, void (*progress)(int bytes))
{
	device_t *dev;
	u8 *mac;
	ld_dst_t dst;
	char *enc = "enc.tmp";
	char *gz = "gz.tmp";
	char *src_enc = "tmp.tmp";
	int ret = 0;

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	xfunc_in("dev=%d, type=%d", dev_idx, type);

	mac = dev->wimax->dev_info.device_mac;

	if (type == DLIMG_DEV_CERT || type == DLIMG_CERT1_U || type == DLIMG_CERT1_L ||
		type == DLIMG_CERT2_U || type == DLIMG_CERT_BIG || type == DLIMG_EAP_PARAM ||
		(type == DLIMG_OMA_XML && ENC_XML_ENABLED(dev))) {
		xprintf(SDK_DBG, "Encrypt 0x%x\n", type);
		if (type == DLIMG_EAP_PARAM) {
			if (write_file(src_enc, buf, size) != size) {
				ret = -1;
				goto out;
			}
			ret = 0;
		}
		else {
			ret = gzip_buf(buf, size, gz);
			src_enc = gz;
		}
		if (!ret)
			ret = encrypt_file((char *) mac, src_enc, enc);
		if (!ret) {
			dst.is_file = TRUE;
			dst.u.path = enc;
			ret = do_download(dev_idx, type, &dst, progress);
		}
		unlink(enc);
		unlink(src_enc);
	}
	else {
		xprintf(SDK_DBG, "No Encrypt 0x%x\n", type);
		dst.is_file = FALSE;
		dst.u.buf.p = buf;
		dst.u.buf.size = size;
		ret = do_download(dev_idx, type, &dst, progress);
	}
out:
	dm_put_dev(dev_idx);
	xfunc_out("ret=%d", ret);
	return ret;
}

static int do_read_file(int dev_idx, const char *target_file,
		ld_dst_t *dst, void (*progress)(int bytes))
{
	device_t *dev;
	u8 send_buf[HCI_MAX_PACKET];
	u8 recv_buf[HCI_MAX_PACKET];
	hci_file_read_t *file = (hci_file_read_t *) send_buf;
	hci_file_response_t *rsp;
	u32 offset = 0;
	int fd = 0;
	int ret = -1, len, max_size = 0;
	char *p = NULL;
	u16 cmd;
	void *param;
	int plen;

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	xfunc_in("[%d]", dev_idx);

	if (dst->is_file) {
		if ((fd = open(dst->u.path, O_CREAT|O_WRONLY|O_TRUNC, 0644)) < 0) {
			xprintf(SDK_STD_ERR, "open(%s) fail\n", dst->u.path);
			goto out;
		}
	}
	else {
		if (!dst->u.buf.p || !dst->u.buf.size) {
			xprintf(SDK_ERR, "buf=%p, size=%d\n", dst->u.buf.p, dst->u.buf.size);
			goto out;
		}
		p = dst->u.buf.p;
		max_size = dst->u.buf.size;
	}

	strcpy((char *)file->path, target_file);
	cmd = WIMAX_READ_FILE;
	param = file;
	plen = sizeof(hci_file_read_t) + strlen((char *)file->path) + 1/*null*/;

	pthread_mutex_lock(&dev->load_lock);
	while (1) {
		file->offset = DH2B(offset);
		ret = hci_send_wait(dev_idx, cmd, param, plen,
				WIMAX_FILE_RESULT, recv_buf, sizeof(recv_buf), 0, 0, 3);
		if (ret < 0)
			break;

		rsp = (hci_file_response_t *) recv_buf;
		if (!(len = DB2H(rsp->result))) {
			ret = 0;
			break;
		}
		if (len < 0) {
			xprintf(SDK_ERR, "Read file failed(%d)\n", ret);
			ret = -1;
			break;
		}

		if (dst->is_file) {
			ret = write(fd, file_response_data(rsp), len);
			if (ret <= 0) {
				xprintf(SDK_ERR, "Write fail %s(%d!=%d)\n", dst->u.path, ret, len);
				ret = -1;
				break;
			}
		}
		else {
			if (offset + len > max_size) {
				xprintf(SDK_ERR, "buffer is too small(%d > %d)\n",
					offset + len, max_size);
				ret = -1;
				break;
			}
			memcpy(p+offset, file_response_data(rsp), len);
		}

		offset += len;
		if (progress)
			progress(offset);
	}
	pthread_mutex_unlock(&dev->load_lock);

out:
	if (fd > 0)
		close(fd);
	xprintf(SDK_INFO, "Read file size=%d\n", offset);
	if (!ret) {
		if (!offset)
			ret = -ENOENT;
		else
			ret = offset;
	}

	xfunc_out("ret=%d", ret);
	dm_put_dev(dev_idx);
	return ret;
}

int fl_read_file(int dev_idx, const char *target_file,
		const char *host_file, void (*progress)(int bytes))
{
	ld_dst_t dst;
	int ret;
	
	dst.is_file = TRUE;
	dst.u.path = host_file;
	ret = do_read_file(dev_idx, target_file, &dst, progress);

	return ret;
}
