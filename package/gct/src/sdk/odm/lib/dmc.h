#ifndef _DMC__HEADER__INCLUDED_
#define _DMC__HEADER__INCLUDED_

#if defined(WIN32) || defined(_WIN32_WCE) || defined(WIN64)
	#ifdef DMC_EXPORTS
	#define DMC_API __declspec(dllexport)
	#else
	#define DMC_API __declspec(dllimport)
	#endif
#else
	#define DMC_API
#endif

#ifdef __cplusplus
extern "C" {
#endif

#define DMC_ERROR_UNKNOWN				-1	// 일반 오류
#define DMC_ERROR_INIT_NOT				-2	// Init 미 호출
#define DMC_ERROR_ALREADY				-3	// 중복 호출
#define DMC_ERROR_MEMORY				-4	// 메모리 부족
#define DMC_ERROR_PARAM_INVALID			-5	// 파라미터가 잘못 되었음
#define DMC_ERROR_PARAM_NULL			-6	// 파라미터에 NULL이 있음
#define DMC_ERROR_SOCKET_INIT			-7	// 소켓 초기화 오류
#define DMC_ERROR_SSL_INIT				-8	// SSL 초기화 오류
#define DMC_ERROR_BUSY					-9	// 동시에 진행이 불가능한 작업이 앞서 진행 중이거나 이미 호출되어 진행 중임
#define DMC_ERROR_TIME_OUT				-10	// 요청한 작업이 지연되어 취소되었음
#define DMC_ERROR_NOT_FOUND				-11 // 요청한 데이터가 존재하지 않음
#define DMC_ERROR_NOT_TREE				-12 // Tree Data가 준비 되지 않았음

#define DMC_ERROR_SOCKET_CREATE			-20	// 소켓 생성 실패 
#define DMC_ERROR_SOCKET_BIND			-21	// 소켓 Bind 실패
#define DMC_ERROR_SOCKET_LISTEN			-22 // 소켓 Listen 실패
#define DMC_ERROR_SOCKET_SELECT			-23 // 소켓 Select 실패
#define DMC_ERROR_SOCKET_ACCEPT			-24 // 소켓 Accept 실패
#define DMC_ERROR_SOCKET_SEND			-25	// 소켓 Send 실패
#define DMC_ERROR_SOCKET_RECV			-26 // 소켓 Receive 실패

#define DMC_ERROR_FILE_OPEN				-30	// 파일 Open 실패
#define DMC_ERROR_FILE_CLOSE			-31 // 파일 Close 실패
#define DMC_ERROR_FILE_SEEK				-32	// 파일 Seek 실패 
#define DMC_ERROR_FILE_GETPOS			-33	// 파일 Get Position 실패
#define DMC_ERROR_FILE_READ				-34	// 파일 Read 실패
#define DMC_ERROR_FILE_WRITE			-35 // 파일 Write 실패
#define DMC_ERROR_FILE_DELETE			-36 // 파일 Delete 실패

#define DMC_ERROR_TREE_OPEN				-40	// Tree Open 중 실패
#define DMC_ERROR_TREE_CLOSE			-41	// Tree Close 중 실패
#define DMC_ERROR_TREE_SAVE				-42	// Tree Save 중 실패
#define DMC_ERROR_TREE_GET_NODE			-43	// Tree Get Node 중 실패
#define DMC_ERROR_TREE_SET_NODE			-44	// Tree Set Node 중 실패

#define DMC_ERROR_OMADM2SDK_ENCODE		-50	// OMA DM to SDK 인코딩 중 실패
#define DMC_ERROR_SDK2OMADM_ENCODE		-60	// SDK to OMA DM 인토딩 중 실패



typedef enum tag_SOCKET_TYPE
{
	SOCKET_TYPE_TCP,
	SOCKET_TYPE_UDP
} SOCKET_TYPE;

typedef enum tag_SECURE_LAYER
{
	SECURE_LAYER_NONE,
	SECURE_LAYER_SSLv3,
	SECURE_LAYER_TLS
} SECURE_LAYER;

typedef enum tag_DMC_SECURITY_TYPE
{
	DMC_SECURITY_DEFAULT,					// 기본값 사용
    DMC_SECURITY_NONE,						// 인증 안함
	DMC_SECURITY_NETWPIN,
	DMC_SECURITY_USERPIN,
	DMC_SECURITY_USERNETWPIN,
	DMC_SECURITY_USERPINMAC
} DMC_SECURITY_TYPE;

// TLS_RSA_WITH_3DES_EDE_CBC_SHA = DES-CBC3-SHA

typedef struct tag_NodeList {
	struct tag_NodeList *prev;
	char                *URI;
	struct tag_NodeList *next;
} NodeList, *NodeListPtr;

typedef struct tag_SecureInfo {
	SECURE_LAYER  nSecureLayer;		/* 암호화 방식 */
	char  *pszCACert;				/* Root CA 인증서를 담고 있는 buffer */
	int nCACertSize;				/* Root CA 인증서 size */
	char  *pszClientCert;			/* Client 인증서를 담고 있는 buffer */
	int nClientCertSize;			/* Client 인증서 size */
	char  *pszPrivKey;				/* Client 인증서 Private Key를 담고 있는 buffer */
	int nPrivKeySize;				/* Client 인증서 Private Key size */
	char *pszPrivKeyPassword;		/* 위 Private Key가 암호화되어 있을 경우 풀기 위한 passphrase */
	int nPrivKeyPasswordSize;		/* Password size */
} SecureInfo, *SecureInfoPtr;

// --mons20100322
//typedef void (*CB_ExecURI)(int dev_id, char *szURI, unsigned char *szARG, int nLength);
// ++mons20100322
typedef int (*CB_ExecURI)(int dev_id, char *szURI, unsigned char *szARG, int nLength);
typedef void (*CB_NotifyStart)(int dev_id, int nStatus);
typedef void (*CB_NotifyExit)(int dev_id, int nChange, int nStatus);
typedef void (*CB_NotifyError)(int dev_id, int nError);
typedef void (*CB_GetPin)(int dev_id, DMC_SECURITY_TYPE pinType, unsigned char* pBuf, int nBufLen, int* pnBufLen);
typedef void (*CB_AES128CCMDecrypt)(
									unsigned char *pBEK, // in, 16bytes
									unsigned char *pE, // in
									unsigned int nELen, // in
									unsigned char *pNonce, // in, 13bytes
									unsigned char *pD, // out
									unsigned int *pnDLen // out
									);
typedef void (*CB_NotifyBootstrap)(int dev_id, int nStatus);
typedef void (*CB_NotifyDMCmdStatus)(int dev_id, int nPackage, char* szCmd, NodeList *pNodeListPtr, int nResultCode);


//
// Firmware Update Function Pointer
//
typedef void (*CB_GetFwFilePath)(int dev_id, char *szPath);
typedef void (*CB_GetFwFreeSpace)(int dev_id, unsigned int *nFreeSize);
typedef void (*CB_GetFwSessionInf)(int dev_id, char *szURI, char *szCorrelator);
typedef void (*CB_SetFwSessionInf)(int dev_id, char *szURI, char *szCorrelator);
typedef void (*CB_SetFwDlProgress)(int dev_id, unsigned int nCurSize, unsigned int nTotalSize);
typedef void (*CB_SetFwDlResult)(int dev_id, char *szFwPkgFilename, int nSuccess);
typedef void (*CB_RunFwUpdate)(int dev_id);
typedef void (*CB_GetFwPkgURL)(int dev_id, char *szUrlNode, char *szURL);
typedef void (*CB_SetFwPkgURL)(int dev_id, char *szUrlNode, char *szURL);

//
// DRMD Function Pointer
//
typedef int (*CB_GetDrmdData)(int dev_id, char *szURI, void *pBuffer, int *nBufLen );
typedef int (*CB_SetDrmdData)(int dev_id, char *szURI, void *pData, int nDataLen );


//
// DM Control API
//
DMC_API int DMCInit(int dev_id, char *szPath);
DMC_API int DMCTerm(int dev_id);
DMC_API int DMCStartListener(int dev_id, SOCKET_TYPE nSocket_Type, unsigned long ip_addr, int nPort);
DMC_API int DMCStopListener(int dev_id);
DMC_API int DMCRunWIB(int dev_id); 
DMC_API void DMCSetWIBParam( int dev_id, char *dns, char *serviceName, char *protocol, char *name, char *requestURI, char *httpRequestHeaderAccept);
DMC_API void DMCSetBEKParam( int dev_id, char *szEMSK, int nEMSKLen, char *szStr );
DMC_API void DMCSetBootStrapParam( int dev_id, DMC_SECURITY_TYPE security);
DMC_API int DMCGetStatus(int dev_id); 
DMC_API char *DMCGetVersion( void );

//
// DM Trigger API
//
DMC_API int DMCTriggerNormal(int dev_id); 
DMC_API int DMCTriggerExecResult(int dev_id, char *szURI, int nResultCode); 
DMC_API int DMCTriggerFwUpdateResult(int dev_id, char *szURI, char *szCorrelator, int nResultCode);
DMC_API int DMCTriggerFwDlContinue( int dev_id );
DMC_API int DMCTriggerDrmdReport( int dev_id, char *szServerID );

//
// XML Control API
//
DMC_API int DMCXMLFromSDKToOMADM( int dev_id, char *pTndsTree, int nLen );
DMC_API int DMCXMLFromOMADMToSDK( int dev_id, char *pTndsTree, int *pnLen );
DMC_API int DMCAddExcludingList(int dev_id, char *szURI); 
DMC_API int DMCGetNodeValue(int dev_id, char *URI, void *val, int *pval_len); 
DMC_API int DMCSetNodeValue(int dev_id, char *URI, void *val, int val_len); 
DMC_API int DMCGetNodeList(int dev_id, char *szURI, NodeList **ppRetNodeList);
DMC_API int DMCFreeNodeList(int dev_id, NodeList *pNodeListPtr);
DMC_API int DMCAddNode(int dev_id, char *szURI, char *szFormat );

//
// Notify API
//
DMC_API void DMCRegExecURI(CB_ExecURI pExecURIFunc);
DMC_API void DMCRegNotifyStart(CB_NotifyStart pNotifyStart);
DMC_API void DMCRegNotifyExit(CB_NotifyExit pNotifyExit);
DMC_API void DMCRegNotifyError(CB_NotifyError pNotifyErrorFunc);
DMC_API void DMCRegNotifyBootstrap(CB_NotifyBootstrap pNotifyBootstrapFunc);
DMC_API void DMCRegNotifyDMCmdStatus(CB_NotifyDMCmdStatus pNotifyDMCmdStatus);

//
// Bootstrap API
//
DMC_API void DMCRegGetPin(CB_GetPin pGetPin);
DMC_API void DMCRegAES128CCMDecrypt(CB_AES128CCMDecrypt pAES128CCMDecrypt);

//
// Firmware Update API
//
DMC_API void DMCRegFwGetFileInf( CB_GetFwFilePath pGetFwFilePath
								,CB_GetFwFreeSpace pGetFwFreeSpace);
DMC_API void DMCRegFwSessionInf( CB_GetFwSessionInf pGetFwSessionInf
								,CB_SetFwSessionInf pSetFwSessionInf );
DMC_API void DMCRegFwSetDlStatus( CB_SetFwDlProgress pSetFwDlProgress
								 ,CB_SetFwDlResult pSetFwDlResult );
DMC_API void DMCRegFwRunUpdate( CB_RunFwUpdate pRunFwUpdate );
DMC_API void DMCRegFwPkgUrl(  CB_GetFwPkgURL pGetFwPkgURL
							, CB_SetFwPkgURL pSetFwPkgURL );
//
// Diagnostic API
//
DMC_API void DMCRegDrmdData( CB_GetDrmdData pGetDrmdData, CB_SetDrmdData pSetDrmdData );




#ifdef __cplusplus
} /* extern "C" */
#endif

#endif // _DMC__HEADER__INCLUDED_


