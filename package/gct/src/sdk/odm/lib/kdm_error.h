/*[
 *      Name:                   kdm_error.h
 *
 *      Project:                HDM
 *
]*/

/*! \file
 *      Defines the error codes returned by HDM funtions.
 *
 * \brief   Error codes
 *
 * Errors are defined in groups. The top bits of an error code
 * define the group to which it belongs; the bottom bits
 * distinguish different errors of that group. (The first error
 * of each group is conventionally reserved for a generic
 * error.)
 *
 * It is possible for implementation specific errors to be added
 * to the predefined groups but it is recommended that they be
 * defined using the spare groups (starting at HDM_ERR_TYPE_IMPL).
 * The exceptions are comms and time slicing error codes where
 * it is recommended that new error codes be defined in the
 * existing groups.
 */

#ifndef _HDM_HDM_ERROR_H_
#define _HDM_HDM_ERROR_H_

/*!
 * Number of bits used in the "error subtype" field.
 */
#define HDM_ERR_SUBTYPE_BITS        8

/*!
 * Mask to get the sub-type within a group of errors.  Note that this must have
 * all bits set in the bottom of the word (e.g. the bottom 8 bits).
 */
#define HDM_ERR_SUBTYPE_MASK        ((1 << HDM_ERR_SUBTYPE_BITS) - 1)

/*!
 * Mask to get the error group type.
 */
#define HDM_ERR_TYPE_MASK           (~HDM_ERR_SUBTYPE_MASK)

/*!
 * Test whether an error belongs to a particular group.
 */
#define HDM_ERR_IS_TYPE(err,typ)    (((err) & HDM_ERR_TYPE_MASK) == (typ))


/*! \defgroup HDM_ERR_defs HDM_ERR_
 * @{
 */

/*
 * Success 'error' codes (ie. not really errors at all)
 * ---------------------
 */
#define HDM_ERR_TYPE_SUCCESS            (0x00 << HDM_ERR_SUBTYPE_BITS)

/*! Success */
#define HDM_ERR_OK                      (HDM_ERR_TYPE_SUCCESS + 0x00)

/*! Break out of a loop */
#define HDM_ERR_BREAK                   (HDM_ERR_TYPE_SUCCESS + 0x01)

/*! Large object item has been handled */
#define HDM_ERR_LO_HANDLED              (HDM_ERR_TYPE_SUCCESS + 0x02)

/*! Command does not need a status response */
#define HDM_ERR_NO_STATUS_RESPONSE      (HDM_ERR_TYPE_SUCCESS + 0x03)

/*! No more commands on a queue */
#define HDM_ERR_NO_MORE_COMMANDS        (HDM_ERR_TYPE_SUCCESS + 0x04)

/*! User selected cancel */
#define HDM_ERR_CANCEL                  (HDM_ERR_TYPE_SUCCESS + 0x05)


#define HDM_ERR_FUMO_STATE              (HDM_ERR_TYPE_SUCCESS + 0x10)

#define HDM_ERR_SCOMO_STATE             (HDM_ERR_TYPE_SUCCESS + 0x11)



#define HDM_ERR_DL_SENDREQ              (HDM_ERR_TYPE_SUCCESS + 0x20)

#define HDM_ERR_DL_READRESPONSE         (HDM_ERR_TYPE_SUCCESS + 0x21)

#define HDM_ERR_DL_MISMATCH_LEN         (HDM_ERR_TYPE_SUCCESS + 0x22)

#define HDM_ERR_DL_UNAVAILABLE          (HDM_ERR_TYPE_SUCCESS + 0x23)



/*
 * General errors
 * --------------
 */
#define HDM_ERR_TYPE_GENERAL            (0x01 << HDM_ERR_SUBTYPE_BITS)

/*! Unspecific error */
#define HDM_ERR_UNSPECIFIC              (HDM_ERR_TYPE_GENERAL + 0x00)

/*! Memory error */
#define HDM_ERR_MEMORY                  (HDM_ERR_TYPE_GENERAL + 0x01)

/*! Supplied buffer isn't long enough */
#define HDM_ERR_BUFFER_OVERFLOW         (HDM_ERR_TYPE_GENERAL + 0x02)

/*! Bad input */
#define HDM_ERR_BAD_INPUT               (HDM_ERR_TYPE_GENERAL + 0x03)

/*! Something impossible happened */
#define HDM_ERR_INTERNAL                (HDM_ERR_TYPE_GENERAL + 0x04)

/*! Client has decided to abort */
#define HDM_ERR_ABORT                   (HDM_ERR_TYPE_GENERAL + 0x05)

/*! User has requested an abort */
#define HDM_ERR_USER_ABORT              (HDM_ERR_TYPE_GENERAL + 0x06)

/*! Server has requested an abort */
#define HDM_ERR_SESSION_ABORT           (HDM_ERR_TYPE_GENERAL + 0x07)

/*! Server has requested a redirection */
#define HDM_ERR_SESSION_REDIRECT        (HDM_ERR_TYPE_GENERAL + 0x08)

/*! Server has required a payment */
#define HDM_ERR_PAYMENT_REQUIRED        (HDM_ERR_TYPE_GENERAL + 0x09)

/*! 4xx/5xx exception occurs */
#define HDM_ERR_COMMAND_FAILURE         (HDM_ERR_TYPE_GENERAL + 0x0A)

/*! Start of sub-range for general SyncML errors */
#define HDM_ERR_GENERAL_SYNCML          (HDM_ERR_TYPE_GENERAL + 0x80)

#define HDM_ERR_USER_CANCELED           (HDM_ERR_TYPE_GENERAL + 0x10)
#define HDM_ERR_DOWNLOAD_CANCELED       (HDM_ERR_TYPE_GENERAL + 0x11)
#define HDM_ERR_DOWNLOAD_MEMORY_LACK    (HDM_ERR_TYPE_GENERAL + 0x12)

#define HDM_ERR_SMA_DB_READ             (HDM_ERR_TYPE_GENERAL + 0x20)

#define HDM_ERR_SMA_DB_NOT_FOUND        (HDM_ERR_TYPE_GENERAL + 0x21)

#define HDM_ERR_NV_CFG_SET              (HDM_ERR_TYPE_GENERAL + 0x30)

#define HDM_ERR_INVALID_STATE           (HDM_ERR_TYPE_GENERAL + 0x40) // FUMO, EFSMO

#define HDM_ERR_TREE_WR_MEMORY_LACK     (HDM_ERR_TYPE_GENERAL + 0x50)

#define HDM_ERR_DIGMON_CFG_SET              (HDM_ERR_TYPE_GENERAL + 0x30)



/*
 * SyncML errors
 * -------------
 */
#define HDM_ERR_TYPE_SYNCML             (0x02 << HDM_ERR_SUBTYPE_BITS)

/*
 * Actual error codes defined in smlerr.h
 */

// 단말기에서 MMI 서비스 사용 중에 Notification에 의한 MMI 요청시에 리턴 값으로 사용
#define HDM_ERR_MMI_IN_SERVICE          0x200


/*
 * Authentication errors
 * ---------------------
 */
#define HDM_ERR_TYPE_AUTH               (0x03 << HDM_ERR_SUBTYPE_BITS)

/*! Authentication failure */
#define HDM_ERR_AUTHENTICATION          (HDM_ERR_TYPE_AUTH + 0x00)

/*! Command failed authentication but not terminally. */
#define HDM_ERR_UNAUTHENTICATED         (HDM_ERR_TYPE_AUTH + 0x01)

/*! Server supplied the wrong type of credentials after a challenge. */
#define HDM_ERR_WRONG_CRED_TYPE         (HDM_ERR_TYPE_AUTH + 0x02)

/*! Server supplied incorrect credentials after a challenge. */
#define HDM_ERR_BAD_CREDENTIALS         (HDM_ERR_TYPE_AUTH + 0x03)

/*! Server supplied incorrect BASIC credentials. */
#define HDM_ERR_BAD_BASIC_CREDENTIALS   (HDM_ERR_TYPE_AUTH + 0x04)

/*! Server failed to supply credentials after a challenge. */
#define HDM_ERR_CRED_MISSING            (HDM_ERR_TYPE_AUTH + 0x05)

/*! Server specified an unknown HMAC algorithm. */
#define HDM_ERR_HMAC_ALGORITHM          (HDM_ERR_TYPE_AUTH + 0x06)

/*! Server failed to supply or supplied incomplete HMAC credentials. */
#define HDM_ERR_HMAC_INCOMPLETE         (HDM_ERR_TYPE_AUTH + 0x07)

/*! Server rejected the client's credentials but did not challenge. */
#define HDM_ERR_NO_CHALLENGE            (HDM_ERR_TYPE_AUTH + 0x08)

/*! Server returned unexpected result code for SyncHdr. */
#define HDM_ERR_BAD_SYNCHDR_STATUS      (HDM_ERR_TYPE_AUTH + 0x09)


/*
 * Protocol and similar errors
 * ---------------------------
 */
#define HDM_ERR_TYPE_PROTO              (0x04 << HDM_ERR_SUBTYPE_BITS)

/*! Unspecific protocol error */
#define HDM_ERR_PROTO                   (HDM_ERR_TYPE_PROTO + 0x00)

/*! The message failed to fit in the buffer (fatal)
 *  cf. \ref HDM_ERR_MESSAGE_OVERFLOW */
#define HDM_ERR_MESSAGE_TOO_LONG        (HDM_ERR_TYPE_PROTO + 0x01)

/*! The command failed to fit in the buffer (recoverable).
 *  cf. \ref HDM_ERR_MESSAGE_TOO_LONG */
#define HDM_ERR_MESSAGE_OVERFLOW        (HDM_ERR_TYPE_PROTO + 0x02)

/*! SyncML message protocol error */
#define HDM_ERR_INVALID_PROTOCOL        (HDM_ERR_TYPE_PROTO + 0x03)

/*! SyncML message protocol version error */
#define HDM_ERR_INVALID_VERSION         (HDM_ERR_TYPE_PROTO + 0x04)

/*! Data is too long to pass back as a large object */
#define HDM_ERR_TOO_BIG                 (HDM_ERR_TYPE_PROTO + 0x05)

/*! Large object length does not match specified size */
#define HDM_ERR_SIZE_MISMATCH           (HDM_ERR_TYPE_PROTO + 0x06)

/*! Missing start message command */
#define HDM_ERR_MISSING_START_MESSAGE_CMD           \
                                        (HDM_ERR_TYPE_PROTO + 0x07)

/*! Missing end message command */
#define HDM_ERR_MISSING_STATUS_CMD      (HDM_ERR_TYPE_PROTO + 0x08)

/*! Optional feature not implemented */
#define HDM_ERR_OPTIONAL_FEATURE_NOT_IMPLEMENTED    \
                                        (HDM_ERR_TYPE_PROTO + 0x09)

/*! No commands in a message */
#define HDM_ERR_NO_COMMANDS             (HDM_ERR_TYPE_PROTO + 0x0A)

/*! Data element with no data */
#define HDM_ERR_NO_DATA                 (HDM_ERR_TYPE_PROTO + 0x0B)

/*! Not enough items with Alert */
#define HDM_ERR_ALERT_MISSING_ITEMS     (HDM_ERR_TYPE_PROTO + 0x0C)

/*! Unknown mime type for message data */
#define HDM_ERR_MESSAGE_MIME_TYPE       (HDM_ERR_TYPE_PROTO + 0x0D)

/*! Command without devinf present when expected. */
#define HDM_ERR_DEVINF_MISSING          (HDM_ERR_TYPE_PROTO + 0x0E)

/*! Missing Source tag. */
#define HDM_ERR_SOURCE_MISSING          (HDM_ERR_TYPE_PROTO + 0x0F)

/*! Command is missing items/data/etc. */
#define HDM_ERR_MALFORMED_COMMAND       (HDM_ERR_TYPE_PROTO + 0x10)


/*
 * DM specific protocol errors
 */
#define TYPE_DM_PROTO_BASE              (HDM_ERR_TYPE_PROTO + 0x80)

/*! Alert options parsing error */
#define HDM_ERR_ALERT_PARSING_ERROR     (TYPE_DM_PROTO_BASE + 0x00)

/*! Item without any data in Alert */
#define HDM_ERR_ALERT_MISSING_DATA      (TYPE_DM_PROTO_BASE + 0x01)

/*! Too many choices passed to implementation */
#define HDM_ERR_ALERT_TOO_MANY_CHOICES  (TYPE_DM_PROTO_BASE + 0x02)


/*
 * DS specific protocol errors
 */
#define TYPE_DS_PROTO_BASE              (HDM_ERR_TYPE_PROTO + 0xC0)

/*! Missing Target tag. */
#define HDM_ERR_TARGET_MISSING          (TYPE_DS_PROTO_BASE + 0x00)

/*! Unknown meta type. */
#define HDM_ERR_BAD_META_TYPE           (TYPE_DS_PROTO_BASE + 0x01)


/*
 * Tree errors (DM only)
 *  ----------
 */
#define HDM_ERR_TYPE_TREE               (0x05 << HDM_ERR_SUBTYPE_BITS)

/*! Tree node already exists */
#define HDM_ERR_NODE_EXISTS             (HDM_ERR_TYPE_TREE + 0x01)

/*! Tree node is missing */
#define HDM_ERR_NODE_MISSING            (HDM_ERR_TYPE_TREE + 0x02)

/*! Parent node is missing */
#define HDM_ERR_PARENT_MISSING          (HDM_ERR_TYPE_TREE + 0x03)

/*! Error in leaf node */
#define HDM_ERR_LEAF_NODE               (HDM_ERR_TYPE_TREE + 0x04)

/*! Leaf node expected */
#define HDM_ERR_NOT_LEAF_NODE           (HDM_ERR_TYPE_TREE + 0x05)

/*! Unknown property */
#define HDM_ERR_UNKNOWN_PROPERTY        (HDM_ERR_TYPE_TREE + 0x06)

/*! An attempt was made to delete a permanent node */
#define HDM_ERR_PERMANENT_NODE          (HDM_ERR_TYPE_TREE + 0x07)

/*! Not allowed by AccessType */
#define HDM_ERR_NOT_ALLOWED             (HDM_ERR_TYPE_TREE + 0x08)

/*! Bad program name passed to Tree Access API */
#define HDM_ERR_PROGRAM_NAME            (HDM_ERR_TYPE_TREE + 0x09)

/*! Partial write of external data not allowed */
#define HDM_ERR_EXT_NOT_PARTIAL         (HDM_ERR_TYPE_TREE + 0x0A)

/*! Write of external data not allowed at this time */
#define HDM_ERR_EXT_NOT_ALLOWED         (HDM_ERR_TYPE_TREE + 0x0B)

/*! May not replace */
#define HDM_ERR_MAY_NOT_REPLACE         (HDM_ERR_TYPE_TREE + 0x0C)

/*! Tree read error */
#define HDM_ERR_TREE_READ               (HDM_ERR_TYPE_TREE + 0x0D)

/*! Tree write error */
#define HDM_ERR_TREE_WRITE              (HDM_ERR_TYPE_TREE + 0x0E)

/*! Access denied by ACL */
#define HDM_ERR_ACCESS_DENIED           (HDM_ERR_TYPE_TREE + 0x0F)

/*! External data value is not readable */
#define HDM_ERR_VALUE_NOT_READABLE      (HDM_ERR_TYPE_TREE + 0x10)

/*! External data value is not writeable */
#define HDM_ERR_VALUE_NOT_WRITEABLE     (HDM_ERR_TYPE_TREE + 0x11)

/*! Node not registered for execute */
#define HDM_ERR_NOT_EXECUTABLE          (HDM_ERR_TYPE_TREE + 0x12)

/*! Tree out of sync error */
#define HDM_ERR_TREE_SYNC               (HDM_ERR_TYPE_TREE + 0x13)

/*! Tree node rename error */
#define HDM_ERR_TREE_RENAME             (HDM_ERR_TYPE_TREE + 0x14)

/*! TNDS replace error */
#define HDM_ERR_SUBTREE_REPLACE         (HDM_ERR_TYPE_TREE + 0x15)

/*! MO URI not found */
#define HDM_ERR_NOT_FOUND_URI           (HDM_ERR_TYPE_TREE + 0x16)

/*
 * Datastore errors (DS only)
 * ----------------
 */
#define HDM_ERR_TYPE_DSTORE             (0x06 << HDM_ERR_SUBTYPE_BITS)

/*! Datastore name not found. */
#define HDM_ERR_UNKNOWN_DATASTORE       (HDM_ERR_TYPE_DSTORE + 0x01)

/*! No more changes in datastore objects for this server. */
#define HDM_ERR_NO_MORE_CHANGES         (HDM_ERR_TYPE_DSTORE + 0x02)

/*! No more matching datastore objects. */
#define HDM_ERR_NO_MORE_OBJECTS         (HDM_ERR_TYPE_DSTORE + 0x03)

/*! Datastore already exists. */
#define HDM_ERR_DUPLICATE_DATASTORE     (HDM_ERR_TYPE_DSTORE + 0x04)

/*! Failed to find an object in a datastore. */
#define HDM_ERR_OBJECT_NOT_FOUND        (HDM_ERR_TYPE_DSTORE + 0x05)

/*! Unknown search grammar specified */
#define HDM_ERR_UNKNOWN_GRAMMAR         (HDM_ERR_TYPE_DSTORE + 0x06)

/*! Failed to find an open datastore. */
#define HDM_ERR_DATASTORE_NOT_OPEN      (HDM_ERR_TYPE_DSTORE + 0x07)

/*! Failed to delete an object. This should be returned when not found. */
#define HDM_ERR_OBJECT_NOT_DELETED      (HDM_ERR_TYPE_DSTORE + 0x08)

/*! Object deleted rather than archived. */
#define HDM_ERR_OBJECT_NOT_ARCHIVED     (HDM_ERR_TYPE_DSTORE + 0x09)

/*! Invalid object data */
#define HDM_ERR_BAD_OBJECT              (HDM_ERR_TYPE_DSTORE + 0x0A)

/*! Mime type not supported. */
#define HDM_ERR_BAD_MIME_TYPE           (HDM_ERR_TYPE_DSTORE + 0x0B)

/*! Mime data format not supported. */
#define HDM_ERR_BAD_FORMAT              (HDM_ERR_TYPE_DSTORE + 0x0C)

/*! A sync failed because the server returned a failure code for a command */
#define HDM_ERR_SYNC_FAILED             (HDM_ERR_TYPE_DSTORE + 0x0D)

/*! Account not found */
#define HDM_ERR_UNKNOWN_DS_SERVERID     (HDM_ERR_TYPE_DSTORE + 0x0E)

/*! Datastore or device full */
#define HDM_ERR_DATASTORE_FULL          (HDM_ERR_TYPE_DSTORE + 0x0F)

/*! Datastore object chunk error */
#define HDM_ERR_DATASTORE_CHUNK         (HDM_ERR_TYPE_DSTORE + 0x10)

/*
 * Trigger errors
 * --------------
 */
#define HDM_ERR_TYPE_TRIGGER            (0x07 << HDM_ERR_SUBTYPE_BITS)

/*! Notification message has invalid length */
#define HDM_ERR_NOTIF_BAD_LENGTH        (HDM_ERR_TYPE_TRIGGER + 0x01)

/*! Notification message has invalid digest */
#define HDM_ERR_NOTIF_BAD_DIGEST        (HDM_ERR_TYPE_TRIGGER + 0x02)

/*! Boot message has invalid digest */
#define HDM_ERR_BOOT_DIGEST             (HDM_ERR_TYPE_TRIGGER + 0x03)

/*! Could not get NSS for bootstrap */
#define HDM_ERR_BOOT_NSS                (HDM_ERR_TYPE_TRIGGER + 0x04)

/*! Could not get PIN for bootstrap */
#define HDM_ERR_BOOT_PIN                (HDM_ERR_TYPE_TRIGGER + 0x05)

/*! Bad bootstrap PIN length */
#define HDM_ERR_BOOT_PINLENGTH          (HDM_ERR_TYPE_TRIGGER + 0x06)

/*! Bad bootstrap SEC value */
#define HDM_ERR_BOOT_BAD_SEC            (HDM_ERR_TYPE_TRIGGER + 0x07)

/*! Bad bootstrap MAC */
#define HDM_ERR_BOOT_BAD_MAC            (HDM_ERR_TYPE_TRIGGER + 0x08)

/*! Bad bootstrap message */
#define HDM_ERR_BOOT_BAD_MESSAGE        (HDM_ERR_TYPE_TRIGGER + 0x09)

/*! Bad bootstrap profile */
#define HDM_ERR_BOOT_BAD_PROF           (HDM_ERR_TYPE_TRIGGER + 0x0A)

/*! Bad trigger reason */
#define HDM_ERR_TRG_BAD_REASON          (HDM_ERR_TYPE_TRIGGER + 0x0B)

/*! No trigger set */
#define HDM_ERR_TRG_MISSING             (HDM_ERR_TYPE_TRIGGER + 0x0C)

/*! Account (server ID) not found */
#define HDM_ERR_TRG_BAD_ACCOUNT         (HDM_ERR_TYPE_TRIGGER + 0x0D)

/*! Datastore sync request missing */
#define HDM_ERR_TRG_BAD_SYNC            (HDM_ERR_TYPE_TRIGGER + 0x0E)

/*! No suspended session to resume */
#define HDM_ERR_NO_SUSPENDED_SESSION    (HDM_ERR_TYPE_TRIGGER + 0x0F)

/*! Notification message has invalid version number */
#define HDM_ERR_NOTIF_BAD_VERSION       (HDM_ERR_TYPE_TRIGGER + 0x10)

/*! Bad trigger alert */
#define HDM_ERR_TRG_BAD_ALERT           (HDM_ERR_TYPE_TRIGGER + 0x11)

/*! Bad DM/DS version */
#define HDM_ERR_TRG_BAD_VERSION         (HDM_ERR_TYPE_TRIGGER + 0x12)

/*! Given DM/DS version is invalid */
#define HDM_ERR_TRG_VERSION_INVALID     (HDM_ERR_TYPE_TRIGGER + 0x13)

/*! Suspend is reqested but fail to suspend */
#define HDM_ERR_TRG_SUSPEND_FAIL        (HDM_ERR_TYPE_TRIGGER + 0x14)

/*! Failed to parse boot message */
#define HDM_ERR_TRG_PARSE_BOOT_MESSAGE_FAIL        (HDM_ERR_TYPE_TRIGGER + 0x15)

/*
 * FUMO errors
 * -----------
 */
#define HDM_ERR_TYPE_FUMO               (0x08 << HDM_ERR_SUBTYPE_BITS)

/*! Error accessing SSP workspace */
#define HDM_ERR_WORKSPACE               (HDM_ERR_TYPE_FUMO + 0x01)

/*! Error accessing SSP foto */
#define HDM_ERR_FOTO                    (HDM_ERR_TYPE_FUMO + 0x02)

/*! Could not initiate update client */
#define HDM_ERR_UPDATE_INIT             (HDM_ERR_TYPE_FUMO + 0x03)


/*
 * Communication errors
 * --------------------
 */
#define HDM_ERR_TYPE_COMMS              (0x09 << HDM_ERR_SUBTYPE_BITS)

/*! General transport error */
#define HDM_ERR_COMMS                   (HDM_ERR_TYPE_COMMS + 0x00)

#define HDM_ERR_SOCKET                  (HDM_ERR_TYPE_COMMS + 0x01)

#define HDM_ERR_INVALID_COMM_ID         (HDM_ERR_TYPE_COMMS + 0x02)

#define HDM_ERR_INVALID_COMM_STATE      (HDM_ERR_TYPE_COMMS + 0x03)

#define HDM_ERR_WRONG_PARAM             (HDM_ERR_TYPE_COMMS + 0x04)

#define HDM_ERR_HTTP_PARAM              (HDM_ERR_TYPE_COMMS + 0x05)

#define HDM_ERR_HTTP_COMM               (HDM_ERR_TYPE_COMMS + 0x06)

#define HDM_ERR_TCP_SEND                (HDM_ERR_TYPE_COMMS + 0x07)

#define HDM_ERR_INVALID_SERVICE_ID      (HDM_ERR_TYPE_COMMS + 0x08)

#define HDM_ERR_CLOSE_SOCKET            (HDM_ERR_TYPE_COMMS + 0x09)

#define HDM_ERR_INVALID_SOCKET          (HDM_ERR_TYPE_COMMS + 0x0A)

#define HDM_ERR_INVALID_PARAM           (HDM_ERR_TYPE_COMMS + 0x0B)

#define HDM_ERR_TCP_READ                (HDM_ERR_TYPE_COMMS + 0x0C)

#define HDM_ERR_TCP_TIMEOUT             (HDM_ERR_TYPE_COMMS + 0x0D)

///* %%% luz:2003-04-17: added these for SSL */
//#define  TCP_TC_SLL_CERT_EXPIRED   20
//#define  TCP_TC_SLL_CERT_INVALID   21
#define HDM_ERR_TCP_SSL_CERT_EXPIRED    (HDM_ERR_TYPE_COMMS + 0x0E)

#define HDM_ERR_TCP_SSL_CERT_INVALID    (HDM_ERR_TYPE_COMMS + 0x0F)

#define HDM_ERR_HTTP_ACCESE_DENIED      (HDM_ERR_TYPE_COMMS + 0x10)

#define HDM_ERR_HTTP_SERVER_ERROR       (HDM_ERR_TYPE_COMMS + 0x11)

#define HDM_ERR_HTTP_NOT_ALLOWED        (HDM_ERR_TYPE_COMMS + 0x12)

#define HDM_ERR_NET_INIT                (HDM_ERR_TYPE_COMMS + 0x13)

#define HDM_ERR_GETHOSTBYNAME           (HDM_ERR_TYPE_COMMS + 0x14)

#define HDM_ERR_NET_CONNECT             (HDM_ERR_TYPE_COMMS + 0x15)

#define HDM_ERR_CONN_CLOSED             (HDM_ERR_TYPE_COMMS + 0x16)

#define HDM_ERR_END_OF_TRANSMISSION     (HDM_ERR_TYPE_COMMS + 0x17)
// 서버 License 제한으로 연결 안됨
#define HDM_ERR_SERVER_FORBIDDEN        (HDM_ERR_TYPE_COMMS + 0x18)

#define HDM_ERR_SERVER_FILE_NOT_FOUND   (HDM_ERR_TYPE_COMMS + 0x19)

#define HDM_ERR_NET_TERM                (HDM_ERR_TYPE_COMMS + 0x20)

#define HDM_ERR_TCP_READ_TIME_OUT       (HDM_ERR_TYPE_COMMS + 0x21)

#define HDM_ERR_HTTP_RESPONSE_TIME_OUT  (HDM_ERR_TYPE_COMMS + 0x22)

/*
 * If using XPT then specific sub-error codes are defined in xpt.h
 */



/*
 * Parsing library errors
 * ----------------------
 */
#define HDM_ERR_TYPE_PARSE              (0x0A << HDM_ERR_SUBTYPE_BITS)

/*! General parsing error */
#define HDM_ERR_PARSE                   (HDM_ERR_TYPE_PARSE + 0x00)

/*! End of data in the buffer while parsing (unexpected) */
#define HDM_ERR_PARSE_EOF               (HDM_ERR_TYPE_PARSE + 0x01)

/*! XML tag mismatch (end not same as start) */
#define HDM_ERR_PARSE_MISMATCH          (HDM_ERR_TYPE_PARSE + 0x02)

/*! Mixed nodes and text as XML PCDATA */
#define HDM_ERR_PARSE_MIXED             (HDM_ERR_TYPE_PARSE + 0x03)

/*! Unexpected XML end tag */
#define HDM_ERR_PARSE_ENDTAG            (HDM_ERR_TYPE_PARSE + 0x04)

/*! XML tag format error (end tag with attributes, etc.) */
#define HDM_ERR_PARSE_TAGFMT            (HDM_ERR_TYPE_PARSE + 0x05)

/*! Error in text encoding */
#define HDM_ERR_PARSE_ENC               (HDM_ERR_TYPE_PARSE + 0x06)

/*! Error in object tree */
#define HDM_ERR_PARSE_TREE              (HDM_ERR_TYPE_PARSE + 0x20)

/*! Error in DTD parsing */
#define HDM_ERR_DTD_PARSE               (HDM_ERR_TYPE_PARSE + 0x40)

/*! Error in DTD attlist parsing */
#define HDM_ERR_DTD_PARSE_ATT           (HDM_ERR_TYPE_PARSE + 0x41)

/*! Error in DTD validation */
#define HDM_ERR_VALIDATE                (HDM_ERR_TYPE_PARSE + 0x50)

/*! Error in validation -- required element missing */
#define HDM_ERR_VAL_REQ_MISSING         (HDM_ERR_TYPE_PARSE + 0x51)

/*! Error in validation -- element did not match DTD */
#define HDM_ERR_VAL_UNMATCHED           (HDM_ERR_TYPE_PARSE + 0x52)

/*! Error in validation -- required content of an element missing */
#define HDM_ERR_VAL_CONTENT_MISSING     (HDM_ERR_TYPE_PARSE + 0x53)

/*! Error in validation -- content where it should be empty */
#define HDM_ERR_VAL_NON_EMPTY           (HDM_ERR_TYPE_PARSE + 0x54)

/*
 * TNDS errors
 * ----------------------
 */
#define HDM_ERR_TYPE_TNDS               (0x0B << HDM_ERR_SUBTYPE_BITS)

/*! General TNDS parsing error */
#define HDM_ERR_TNDS_PARSE              (HDM_ERR_TYPE_TNDS + 0x00)

/*! TNDS DATA missing */
#define HDM_ERR_TNDS_DATA_MISSING       (HDM_ERR_TYPE_TNDS + 0x01)

/*! End of data in the buffer while parsing (unexpected) */
#define HDM_ERR_TNDS_PARSING_EOF        (HDM_ERR_TYPE_TNDS + 0x02)

/*! TNDS unknown tag */
#define HDM_ERR_TNDS_UNKNOWN_TAG        (HDM_ERR_TYPE_TNDS + 0x03)

/*! TNDS tag mismatch (end not same as start) */
#define HDM_ERR_TNDS_MISMATCH           (HDM_ERR_TYPE_TNDS + 0x04)

/*! Document does not conform to TNDS DTD  */
#define HDM_ERR_TNDS_INVAL_TNDS_DTD     (HDM_ERR_TYPE_TNDS + 0x05)

/*! Error in encoding TNDS Obj  */
#define HDM_ERR_TNDS_ENC                (HDM_ERR_TYPE_TNDS + 0x06)

/*! TNDS object given to encode is null  */
#define HDM_ERR_TNDS_OBJ_MISSING        (HDM_ERR_TYPE_TNDS + 0x07)

/*! Unknown property */
#define HDM_ERR_TNDS_UNKNOWN_PROPERTY   (HDM_ERR_TYPE_TNDS + 0x08)

/*! Unsupported WBXML global token */
#define HDM_ERR_TNDS_UNSUPPORTED_TOKEN  (HDM_ERR_TYPE_TNDS + 0x09)

/*! Invalid property list */
#define HDM_ERR_TNDS_INVALID_PROPERTY   (HDM_ERR_TYPE_TNDS + 0x0A)

/*! Over max path of node */
#define HDM_ERR_TNDS_MAX_PATH           (HDM_ERR_TYPE_TNDS + 0x0B)

/*! Invalid DTD version */
#define HDM_ERR_TNDS_INVALID_DTD_VER    (HDM_ERR_TYPE_TNDS + 0x0C)

/*! There is no target URI */
#define HDM_ERR_TNDS_NO_TARGET_URI      (HDM_ERR_TYPE_TNDS + 0x0D)

/*! Duplicate parameter of a node exist  */
#define HDM_ERR_TNDS_DUPLICATE_NODE_PARAM       (HDM_ERR_TYPE_TNDS + 0x0E)

/*
 * SCOMO errors
 * ----------------------
 */
#define HDM_ERR_TYPE_SCM                (0x0C << HDM_ERR_SUBTYPE_BITS)

/*! Software data write fail */
#define HDM_ERR_SCM_WRITE_DATA          (HDM_ERR_TYPE_SCM + 0x00)

/*! Software id get fail */
#define HDM_ERR_SCM_GET_ID              (HDM_ERR_TYPE_SCM + 0x01)


/*
 * Error types up to and including 0x7F are reserved for future DM error
 * classes.
 *
 * Error types from 0x80 onwards may be used by inplementations for their own
 * error values.
 */


/*
 * DM implementation errors
 * ------------------------------
 *
 * These are at the top of the HDM range.
 *
 * The following error codes are specific to DM reference
 * implementations. They should not really be defined here but
 * it is convenient to do so.
 */
#define HDM_ERR_TYPE_IMPL               (0x7F << HDM_ERR_SUBTYPE_BITS)

/*! Text too large to display on the screen. */
#define HDM_ERR_TEXT_TOO_LARGE          (HDM_ERR_TYPE_IMPL + 0x01)

/*! Configuration open error */
#define HDM_ERR_CONFIG_OPEN             (HDM_ERR_TYPE_IMPL + 0x02)

/*! Configuration read error */
#define HDM_ERR_CONFIG_READ             (HDM_ERR_TYPE_IMPL + 0x03)

/*! Unsuppported protocol */
#define HDM_ERR_BAD_PROTOCOL            (HDM_ERR_TYPE_IMPL + 0x04)

/*! Error getting account details */
#define HDM_ERR_DSACC                   (HDM_ERR_TYPE_IMPL + 0x05)

/*! Tree open error */
#define HDM_ERR_TREE_OPEN               (HDM_ERR_TYPE_IMPL + 0x06)

/*! Tree commit error */
#define HDM_ERR_TREE_COMMIT             (HDM_ERR_TYPE_IMPL + 0x07)

/*! Failed to open datastore for reading/writing. */
#define HDM_ERR_DATASTORE_OPEN          (HDM_ERR_TYPE_IMPL + 0x08)

/*! Failed to read datastore. */
#define HDM_ERR_DATASTORE_READ          (HDM_ERR_TYPE_IMPL + 0x09)

/*! Failed to write datastore. */
#define HDM_ERR_DATASTORE_WRITE         (HDM_ERR_TYPE_IMPL + 0x0A)

/*! Failed to commit newly written datastore. */
#define HDM_ERR_DATASTORE_COMMIT        (HDM_ERR_TYPE_IMPL + 0x0B)

/*! Failed to open datastore config file for reading/writing. */
#define HDM_ERR_DATASTORE_CONFIG_OPEN   (HDM_ERR_TYPE_IMPL + 0x0C)

/*! Failed to read datastore config file. */
#define HDM_ERR_DATASTORE_CONFIG_READ   (HDM_ERR_TYPE_IMPL + 0x0D)

/*! Error accessing flash */
#define HDM_ERR_FLASH                   (HDM_ERR_TYPE_IMPL + 0x0E)

/*! File read error */
#define HDM_ERR_FILE_READ               (HDM_ERR_TYPE_IMPL + 0x0F)

/*! Failed to connect the DL server */
#define HDM_ERR_DL_CONNECTION_FAIL      (HDM_ERR_TYPE_IMPL + 0x10)

/*! Error for malformed or bad DL URL */
#define HDM_ERR_BAD_URL                 (HDM_ERR_TYPE_IMPL + 0x11)

/*! Error getting diagmon */
#define HDM_ERR_DIAGMON                 (HDM_ERR_TYPE_IMPL + 0x12)


/*! File Open error */
#define HDM_ERR_FILE_OPEN               (HDM_ERR_TYPE_IMPL + 0x13)

/*! File Write error */
#define HDM_ERR_FILE_WRITE              (HDM_ERR_TYPE_IMPL + 0x14)

/*! Listener failure the while operating */
#define HDM_ERR_LISTENER_FAILURE        (HDM_ERR_TYPE_IMPL + 0x20)

/*! Failed in dns query */
#define HDM_ERR_DNS_QUERY               (HDM_ERR_TYPE_IMPL + 0x21)

/*
 * Time slicing 'error' codes
 * --------------------------
 */

/*
 * These are not true error values (i.e. they don't indicate that there is a
 * fault), and are the only negative values used by the HDM_Error type. They
 * are used by the porting interface to indicate waits and yields and should
 * never appear as real errors when running HDM.
 */
#define HDM_ERR_TYPE_SLICE              (-1 & HDM_ERR_TYPE_MASK)

/*! HDM needs to run again soon */
#define HDM_ERR_YIELD                   (-1)     // 0xFFFFFFFF

/*! HDM needs to run again once an external event has occurred */
#define HDM_ERR_WAIT                    (-2)     // 0xFFFFFFFE

/*! HDM needs to run again once an external event has occurred */
#define HDM_ERR_HAL_WAIT                (-3)     // 0xFFFFFFFD

#define HDM_ERR_DM_STOP_AND_REBOOT      (-4)     // 0xFFFFFFFC




/* @} */


/*
 * ALL negative error codes are reserved for use by time slicing code.
 * (This test is used a lot so it is worth making it cheaper than
 * testing the type using HDM_ERR_IS_TYPE.)
 */
#define IS_SLICE_REQUESTED(ERROR)   (ERROR < 0)

#endif /* !_HDM_HDM_ERROR_H_ */
