#if !defined(OMA_DM_H_20090616) && defined(CONFIG_OMA_DM_CLIENT)
#define OMA_DM_H_20090616

#include "msg.h"
#include "msg_thread.h"
#include "wimax.h"

#if defined(SDK_TEST)
#define ODM_DBG
#define ODM_XML_DBG
#endif

//#define CONFIG_OMA_DM_DRMD

//#define AUTO_CONNECT_NOT_SUPPORTED

#if defined(CONFIG_OMA_DM_OP_CLEARWIRE)
#define CW_DEVICE_CATEGORY	1
#else
#define CW_DEVICE_CATEGORY	0
#endif

#define DMC_NOT_SUPPORTED_CLENT_NOTI_START
//#define KIBA_LAB_ONLY

#define LISTEN_ALL_IP_PORT

#define UDP_NOTI_PORT	2948
#define EMSK_SIZE		64

#define OMA_DM_EAP_TLS				13
#define OMA_DM_EAP_TTLS				21
#define OMA_DM_EAP_AKA				23

#define OMA_EAP_VENDERID_MSCHAPV2	24757

typedef enum {
	ODM_ERROR_START = 0,

	ODM_ERROR_NONE = ODM_ERROR_START,

	// IP error case 1,2
	ODM_ERROR_INVALID_BOOTSTRAP_MESSAGE,

	// IP error case 4
	ODM_ERROR_CANNOT_CONNECT_ODM_SERVER,

	// IP error case 5~8
	ODM_ERROR_HTTP_ERROR,

	// IP error case 9~12
	ODM_ERROR_HTTP_RESPONSE_TIMEOUT,

	// IP error case 13~16
	ODM_ERROR_STATUS_ERROR,

	// IP error case 24
	ODM_ERROR_DNS_QUERY_FAIL,

	// IP error case 25
	ODM_ERROR_CANNOT_CONNECT_WIB_SERVER,

	ODM_ERROR_END = ODM_ERROR_CANNOT_CONNECT_WIB_SERVER
} ODM_ERROR, *PODM_ERROR;

#define DMC_STATUS_RESULT_SUCCESS				200

    /* Check the status code for the last message we sent */
    /* 101 - In progress...? */
    /* 200 - OK */
    /* 203 - Non-authoratitive response ? */
    /* 204 - No content ? */
    /* 212 - Authentication accepted */
    /* 214 - Operation cancelled ? */
    /* 300 : Multiple choice */
    /* 301 : Moved permanently */
    /* 302 : Found. Temporarily moved to a different URI.
            Authentication is required for new URI. */
    /* 303 : See other. Temporarily moved to a different URI. */
    /* 304 : Not modified. The requested SyncML command was not excuted. */
    /* 305 : Use proxy. */
    /* 400 - Bad request */
    /* 401 - Invalid credentials */
    /* 402 - Payment required */
    /* 403 - Forbidden */
    /* 404 - Not Found */
    /* 405 - Command not allowed */
    /* 407 - Missing Credentials */
    /* 408 - Request timeout */
    /* 410 - Gone */
    /* 411 - Size required */
    /* 412 - Incomplete command */
    /* 413 - Request entity too large */
    /* 414 - URI too long */
    /* ... */

typedef enum dmc_status_s {
	DMC_STATUS_OK = 200,
	DMC_STATUS_ACCEPTED_FOR_PROCESSING = 202,
	DMC_STATUS_AUTH_ACCEPT = 212,
	DMC_STATUS_BAD_REQUEST = 400,
	DMC_STATUS_INVALID_CREDENTIAL = 401,
	DMC_STATUS_FORBIDDEN = 403,
	DMC_STATUS_NOT_FOUND = 404,
	DMC_STATUS_CMD_NOT_ALLOWED = 405,
	DMC_STATUS_MISSING_CREDENTIALS = 407,
	DMC_STATUS_ALREADY_EXIST = 418,
	DMC_STATUS_PERMISSION_DENIED = 425,
	DMC_STATUS_COMMAND_FAILED = 500,	
	
	DMC_STATUS_END

} dmc_status_t;

typedef enum {
	BOOTSTRAP_STATE_FAIL,
	BOOTSTRAP_STATE_ONGOING,
	BOOTSTRAP_STATE_SUCCESS

} BOOSTSTRAP_STATE, *PBOOSTSTRAP_STATE;

#define POLLING_INTERVAL_BASE_UNIT			60000	/*1 minute*/
#define POLLING_INTERVAL_NO_INIT			-2	/*GCT Spec.*/
#define POLLING_INTERVAL_ONCE				-1	/*WiMax Spec.*/
#define POLLING_INTERVAL_DISABLE			0	/*WiMax Spec.*/

#define POLLING_ATTEMPT_INFINITY			0x7fffffff

//#define CALC_BOOTSTRAP_TIMER_SINCE_CONNECTED

#define SESSION_PROVISIONING		(1<<0)
#define SESSION_XML_CHANGED			(1<<2)
#define SESSION_NOTIFICATION		(1<<10)
#define SESSION_SERVER_NOTI			((1<<11)|SESSION_NOTIFICATION)
#define SESSION_CLIENT_NOTI			((1<<12)|SESSION_NOTIFICATION)
#define SESSION_BOOTSTRAP			(1<<13)
#define SESSION_EXEC				(1<<20)
#define SESSION_EXEC_CONTACT		((1<<21)|SESSION_EXEC)
#define SESSION_EXEC_DRMD			((1<<22)|SESSION_EXEC)
#define SESSION_EXEC_FUMO			((1<<23)|SESSION_EXEC)
#define SESSION_ERROR				(1<<31)

#define ODM_RECONNECT_TIMER_MS		3000
#define MAX_RECONNECT_CNT	8

//#define CHECK_LAST_PACKAGE_TO_STORE_NONCE
#if defined(CONFIG_OMA_DM_DRMD)
struct drmd_context_s;
#endif // CONFIG_OMA_DM_DRMD
typedef struct wm_oma_dm_s {
	bool opened;
	u32 ip_addr;
	int port;

	int d_status;
	
	timer_obj_t			bootstrap_timer;
	timer_obj_t			polling_timer;
	timer_obj_t			disconnect_timer;
	timer_obj_t			ip_acquisition_timer;

	char eap_emsk[EMSK_SIZE];
	BOOSTSTRAP_STATE bootstrap_state;

	time_t    resolv_conf_st_mtime;

	bool listener_started;

	int connected_time_ms;

	u32	session_flag;

	bool polling_supported;
	int polling_attempts;
	int polling_interval;

	int polling_count;

	#if defined(CHECK_LAST_PACKAGE_TO_STORE_NONCE)
	int last_package;
	#endif

	#if defined(AUTO_CONNECT_NOT_SUPPORTED)
	int reconnect_count;
	#endif

	#if defined(CONFIG_OMA_DM_DRMD)
	struct drmd_context_s *drmd;
	pthread_t drmd_thread;
	#endif
} wm_oma_dm_t;

typedef enum node_format_s {
	NODE_FORMAT_node,
	NODE_FORMAT_int,
	NODE_FORMAT_chr,
	NODE_FORMAT_bool,
	NODE_FORMAT_float,
	NODE_FORMAT_b64,
	//NODE_FORMAT_bin, // Not supported
} node_format_t;

typedef struct odm_msg_t {
	int event;

} odm_msg_t;

//#define ODM_SUPPORT_MULTIDEVICE

#define odm_msg_wait_until_done(wm) do {\
		wm_in_wimax(wm, WM_USING_LOCK_ODM);		\
		wm_out_wimax(wm, WM_USING_LOCK_ODM);	\
	} while (0)

int odm_init(void);
int odm_deinit(void);
void odm_stamp_dns_info_mtime(int dev_idx);
int odm_get_operator_x(int dev_idx, const char *oper_name, char *oper_x);
int odm_upload_xml(int dev_idx);
int odm_open(int dev_idx);
int odm_close(int dev_idx);
int odm_event_thread_create(struct msg_thr_s *msg_thr);
int odm_event_thread_delete(struct msg_thr_s *msg_thr);
int odm_send_msg(int dev_idx, int event);
int odm_trig_fw_update_result(int dev_idx, char *uri, char *correlator, int result);
int odm_trig_fw_dl_continue(int dev_idx);
int ODM_GetNodeValue(int dev_idx, const char *uri, void *val, int *val_len);
int ODM_SetNodeValue(int dev_idx, const char *uri, void *val, int val_len);
int ODM_AddNode(int dev_idx, char *uri, char *format);
int ODM_AddNodeByType(int dev_idx, char *uri, node_format_t format);
#if defined(CONFIG_OMA_DM_DRMD)
int odm_trig_drmd(int dev_idx);
#endif // CONFIG_OMA_DM_DRMD
void ODM_SetEMSK(int dev_idx, char *szEMSK, int nEMSKLen);
int odm_setup_eap_param(int dev_idx, char *userid, char *userid_pass, char *anony_id);

#endif
