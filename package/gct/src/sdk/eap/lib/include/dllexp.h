#if defined(WIN32) || defined(WINCE) || defined(_WIN32_WCE)
#ifdef CYGNUS
#	define DLL_API
#else
#	ifdef DLL_EXPORTS
#		define DLL_API __declspec(dllexport)
#	else
#		define DLL_API __declspec(dllimport)
#	endif
#endif
#else
#	ifndef DLL_API
#	define DLL_API
#	endif
#endif
