/**
	@brief EAP Library Header
	@author Jaeyoung Heo
	@create 2007.7.3
*/

#ifndef __EAPLIB_H__
#define __EAPLIB_H__

#include "dllexp.h"

// HANDLE
typedef void * EAPHANDLE;

// Callback function for realm check
// return 0 means failre, 1 means success 
typedef int (*CBEAPREALMPROC)(EAPHANDLE,const char * p_cServerCertRealm,const  int nServerCertRealmLen,
								const char **pp_cConfigRealmList,const int p_nConfigRealmListNum);

// TLS Extension function
#define TLSEXT_DIR_CLIENTHELLO_SVR	0	// Currently, not supported.
#define TLSEXT_DIR_SERVERHELLO		1
#define TLSEXT_DIR_CLIENTHELLO		2

// TLS Extension type.
#define TLSEXT_NAMETYPE_HOST_NAME		0
#define TLSEXT_TYPE_RENEGO				0xff01
#define TLSEXT_TYPE_ELLIPTIC_CURVES		10
#define TLSEXT_TYPE_EC_POINT_FORMAT		11
#define TLSEXT_TYPE_SESSION_TICKET		35
#define TLSEXT_TYPE_STATUS_REQUEST		5

typedef int (*CBTLSEXTPROC)(EAPHANDLE,int nDirection,int nType,unsigned char *p_cData,int nLen);

// TLS Extenstion setting
typedef int (*CBTLSEXTSETCUSTOMPROC)(EAPHANDLE,int nDirection,int *p_nType,unsigned char *p_cData,int *p_nLen);


// TLS Versions.
#define TLS1_VERSION			0x0301 // minor number is TLSx version
#define SSL3_VERSION			0x0300

// IF Structure.
// Authentication Structure.
typedef struct tagEAPAUTHSTR{
    unsigned short usAuthFlags;

	unsigned char ucCACertFlags;
	unsigned char ucPrivateCertFlags;

	char *p_cCACert;
	char *p_cPrivateCert;
	char *p_cPrivatePassword;

    char *p_cAnonymousId;
	char *p_cID;
    char *p_cPassword;
	
}EAPAUTHSTR;

typedef struct tagEAPAUTHSTR2{
    unsigned short usAuthFlags;

	unsigned char ucCACertFlags;
	unsigned char ucPrivateCertFlags;

	char *p_cCACert;
	char *p_cPrivateCert;
	char *p_cPrivatePassword;

    char *p_cAnonymousId;
	char *p_cID;
    char *p_cPassword;

	// --- to here, same member with EAPAUTHSTR version1.

	// Realm Comparison Callback 
	char **pp_cRealmList;
	unsigned int nRealmCount;
	CBEAPREALMPROC fnCBRealmProc;	
	
	// add Extended Flags.
	unsigned long ulAuthFlagsExt; // Extended Flags.

}EAPAUTHSTR2;


// Authentication Main Structure Parameter
typedef struct tagEAPDASTR{
	unsigned short usFragmentSize;
	unsigned short usAuthStrCount; 
	EAPAUTHSTR stAuthStr[1];
}EAPDASTR;

typedef struct tagEAPDASTR2{
	unsigned short usFragmentSize;
	unsigned short usAuthStrCount;
	EAPAUTHSTR2 stAuthStr[1];
}EAPDASTR2;


// CERT Memory
typedef struct tagCERTMEM{
	int nSize;
	char *p_cCertMem;
}CERTMEM;

typedef struct tagCERTMEMSTORE{
	int nCount;
	CERTMEM stCert[1];
}CERTMEMSTORE;


// for Structure change Flags
#define STR_EAPDA			0x00
#define STR_EAPDA2			0x01



// Structure Related Flags.
#define ENABLE_AUTH_TLS					0x01
#define ENABLE_AUTH_TLS_SEND_ALL_CERT	0x08
#define ENABLE_AUTH_TTLS				0x02
#define ENABLE_AUTH_ANONYMOUS			0x04
#define ENABLE_AUTH_TTLS_MD5			0x08
#define ENABLE_AUTH_TTLS_CHAP			0x10
#define ENABLE_AUTH_TLS_USE_CN_AS_ID	0x20
#define ENABLE_AUTH_MSPEAP				0x40
#define ENABLE_AUTH_MSPEAP_MSCHAPV2		0x0100
#define ENABLE_AUTH_TTLS_MSCHAPV2		0x0100
#define ENABLE_WRITE_PCAP				0x0200
#define ENABLE_DEBUG					0x0200
#define ENABLE_DOMAIN_CHAR_ALPHA		0x0400
#define ENABLE_LENGTH_BIT_ALWAYS		0x0800
#define ENABLE_AUTH_TTLS_MSCHAPV2_STRICT	0x1000
#define ENABLE_AUTH_TLS_NEXT_FRAG_NO_LENGTH_BIT	0x2000
#define ENABLE_SESSION_RESUME				0x4000
#define ENABLE_AUTH_TTLS_MSCHAPV2_NO_SVR_CHECK	0x8000
#define ENABLE_AUTH_REMOVE_TLS_DH_CIPHER		0x80

// Extended Flags.
#define ENABLE_REALM_CHECK						0x00010000
#define ENABLE_REALM_CHECK_EX					0x00010000
#define ENABLE_NO_SVR_CERT_EXPIRE_CHK					0x00200000
#define ENABLE_NO_SESSION_TICKET						0x00400000

// Function Related Flags.

#define	CERT_FILE					0x01
#define CERT_MEM					0x02
#define CERT_TYPE_PEM				0x40
#define CERT_TYPE_DER				0x80

#define CERT_GET_SUBJECT_CN			0x04
#define CERT_GET_ISSUER_CN			0x08
#define CERT_GET_LAST_CN			0x10
#define CERT_GET_EXPIRE_DATE		0x20 
#define CERT_GET_CERT_LEVEL			0x40


// Define Level Type
#define CERT_LEVEL_CA				0x01
#define CERT_LEVEL_CLIENT			0x02
#define CERT_LEVEL_SERVER			0x04
#define CERT_LEVEL_SUBCA			0x08


// TLS Status Return.
#define	WM_TLS_STATUS_INIT		1
#define	WM_TLS_STATUS_SUCCESS	2
#define	WM_TLS_STATUS_FAILURE	3
#define	WM_TLS_STATUS_TLS_AUTH	4
#define	WM_TLS_STATUS_PHASE2_AUTH 5

// Debug option for Memory Debug
#define TLS_DBG_CREATE_ALWAYS		1
#define TLS_DBG_APPEND				0

// Debug Flag for default Debug
#define TLS_DBG_FLAG_DEFAULT		0
#define TLS_DBG_FLAG_NO				1
#define TLS_DBG_FLAG_CREATE_SESSION	2
#define TLS_DBG_FLAG_CREAE_SESSION	2

// TLS ALERT Defines
#define TLS_ALERT_UNEXPECTED_MESSAGE		10
#define TLS_ALERT_BAD_RECORD_MAC			20
#define TLS_ALERT_DECRYPTION_FAILED			21
#define TLS_ALERT_RECORD_OVERFLOW			22
#define TLS_ALERT_DECOMPRESSION_FAILED		30
#define TLS_ALERT_HANDSHAKE_FAILURE			40
#define TLS_ALERT_BAD_CERTIFICATE			42
#define TLS_ALERT_UNSUPPORTED_CERTIFICATE	43
#define TLS_ALERT_CERTIFICATE_REVOKED		44
#define TLS_ALERT_CERTIFICATE_EXPIRED		45
#define TLS_ALERT_CERTIFICATE_UNKNOWN		46
#define TLS_ALERT_ILLEGAL_PARAMETER			47
#define TLS_ALERT_UNKNOWN_CA				48
#define TLS_ALERT_ACCESS_DENIED				49
#define TLS_ALERT_DECODE_ERROR				50
#define TLS_ALERT_DECRYPT_ERROR				51
#define TLS_ALERT_EXPORT_RESTRICTION		60
#define TLS_ALERT_PROTOCOL_VERSION			70
#define TLS_ALERT_INSUFFICIENT_SECURITY		71
#define TLS_ALERT_INTERNAL_ERROR			80
#define TLS_ALERT_USER_CANCELED				90
#define TLS_ALERT_NO_RENEGOTICATION			100


// Function Prototypes.
#ifdef __cplusplus
extern "C"
{
#endif
// Get Version
DLL_API int TLSGetVersion(char *p_cVersion,unsigned int nLength);

// Handle Fuctions
DLL_API EAPHANDLE TLSNewSessionHND();
DLL_API int TLSDeleteSessionHND(EAPHANDLE);

// Main Functions
DLL_API int TLSSetParametersHND(EAPHANDLE,EAPDASTR *p_stStr);
DLL_API int TLSSetParametersExtHND(EAPHANDLE,unsigned long ulFlags,EAPDASTR *p_stStr);
DLL_API int TLSDealEAPPayloadHND(EAPHANDLE,char *p_szBuf,int nSize, char *p_szRtnBuf,int *p_nRtnBufSize);
DLL_API int TLSGetMSKHND(EAPHANDLE,char *p_szBuf,int *p_nRtnBufSize);
DLL_API int TLSClearParametersHND(EAPHANDLE);

// Support Functions
DLL_API int TLSGetNotificationDataHND(EAPHANDLE,char *p_szRtnBuf,int *p_nRtnBufSize);
DLL_API int TLSGetAuthErrorMsgHND(EAPHANDLE,char *p_szRtnBuf,int *p_nRtnBufSize);

DLL_API int TLSGetNotificationBinDataHND(EAPHANDLE,char *p_szRtnBuf,int *p_nRtnBufSize);

// Return TLS Alert Messages.
DLL_API int TLSGetAlertHND(EAPHANDLE,int *p_nAlert,char *p_szRtnBuf,int *p_nRtnBufSize);

DLL_API int TLSGetStatusHND(EAPHANDLE);
DLL_API int TLSGetFailReasonHND(EAPHANDLE);
DLL_API	int TLSSetFailReasonHND(EAPHANDLE,int nReason);
DLL_API int TLSSetLastErrorHND(EAPHANDLE,int nError);
DLL_API int TLSGetLastErrorHND(EAPHANDLE);

// Cleanup
DLL_API void TLSCleanup();

// GetTLS Version.
DLL_API int TLSGetTLSVersionHND(EAPHANDLE); 

// TLS Extension related.
DLL_API int TLSSetTLSExtInfoCBHND(EAPHANDLE,CBTLSEXTPROC);
DLL_API int TLSSetTLSExtCustomCBHND(EAPHANDLE,CBTLSEXTSETCUSTOMPROC);
DLL_API int TLSSetTLSExtHND(EAPHANDLE,int nType,char *p_cData,int nLen);


// ------------------------------------------------------------
// Original Functions - depricated.
// It can be used, but not recommened to use.
// Main Functions
DLL_API int TLSSetParameters(EAPDASTR *p_stStr);
DLL_API int TLSSetParametersExt(unsigned long ulFlags,EAPDASTR *p_stStr);

DLL_API int TLSDealEAPPayload(char *p_szBuf,int nSize, char *p_szRtnBuf,int *p_nRtnBufSize);
DLL_API int TLSGetMSK(char *p_szBuf,int *p_nRtnBufSize);
DLL_API int TLSClearParameters();

// Support Functions
DLL_API int TLSGetNotificationData(char *p_szRtnBuf,int *p_nRtnBufSize);
DLL_API int TLSGetCertInfo(int nType,char *p_cCert,char *p_szRtnBuf,int *p_nRtnBufSize);
DLL_API int TLSGetAuthErrorMsg(char *p_szRtnBuf,int *p_nRtnBufSize);

// Return TLS Alert Messages.
DLL_API int TLSGetAlert(int *p_nAlert,char *p_szRtnBuf,int *p_nRtnBufSize);


DLL_API int TLSGetStatus();
DLL_API	int TLSSetStatus(int nReason);
DLL_API int TLSGetFailReason();
DLL_API	int TLSSetFailReason(int nReason);
DLL_API int TLSSetLastError(int nError);
DLL_API int TLSGetLastError();
// ----------------------------------------------------------------------------------- 

// Utility Functions
DLL_API int TLSConvertP12(int nCertType,char *p_cCert,  int nP12Type,char *p_cP12,char *p_cPassword);
// PCAP
DLL_API	int TLSSetPCAPInfo(char *p_cFileName,char *p_cPSSMAC,char *p_cBSSMAC);
DLL_API	int TLSSetDebugInfo(char *p_cFileName);
DLL_API int TLSConvertCert(int nDCertType,char *p_cDCert,int nSCertType,char * p_cSCert,char *p_Reserved);
DLL_API int TLSSetDebugFlag(unsigned long ulFlag);

// Debug functions
DLL_API	int TLSSetPCAPInfo2HND(EAPHANDLE hEAP,char *p_cPSSMAC,char *p_cBSSMAC,int nMemSize);
DLL_API int TLSSetDebugInfo2HND(EAPHANDLE hEAP,int nMemSize);
DLL_API int TLSCreatePCAPInfo2HND(EAPHANDLE hEAP,char *p_cFileName,int nOption);
DLL_API int TLSCreateDebugInfo2HND(EAPHANDLE hEAP,char *p_cFileName,int nOption);

#ifdef __cplusplus
}
#endif


#endif

