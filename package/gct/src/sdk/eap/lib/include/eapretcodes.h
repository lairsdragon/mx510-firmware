/**
	@brief EAP Return Codes
	@author Jaeyoung Heo
	@create 2007.7.3
*/
#ifndef __EAPRETCODES_H__
#define __EAPRETCODES_H__

// Return Codes.
#define S_WM_EAP_TLS_SUCCESS	 0
#define S_WM_EAP_TLS_RESPONSE		 1
#define S_WM_EAP_TLS_NOTIFICATION	 2

// Authentication Fail Reasons
#define F_WM_SUCCESS				0
#define F_WM_ID_NOT_FOUND			1
#define F_WM_PASSWORD_NOT_FOUND		2
#define F_WM_CA_CERT_ERROR			3
#define F_WM_PRIVATE_CERT_ERROR		4
#define F_WM_SERVER_REJECT			5

// Error Codes
#define E_WM_SUCCESS				0
#define E_WM_CA_LOAD_FAIL			-1
#define E_WM_PRIVATE_LOAD_FAIL		-2
#define E_WM_TLS_FAIL				-3
#define E_WM_TTLS_PHASE2_FAIL		-4
#define E_WM_MEMORY_INSUFFICIENT	-5
#define E_WM_NOT_SUPPORTED			-6
#define E_WM_SET_PARAM_FAILURE		-7
#define E_WM_AUTH_STR_NULL			-8
#define E_WM_AUTH_FUNC_NULL			-9
#define E_WM_INIT_TLS_FAIL			-10
#define E_WM_BUFFER_SMALL			-11
#define E_WM_NOT_SUCCESS			-12
#define E_WM_NO_DATA				-13
#define E_WM_INVALID_FRAME			-14
#define	E_WM_SEND_TLS				-15
#define	E_WM_RECV_TLS				-16
#define E_WM_CERT_NOT_FOUND			-17
#define E_WM_X509_CTX_FAIL			-18
#define E_WM_CERT_LOAD_FAIL			-19
#define E_WM_MEMORY_NULL			-20
#define E_WM_CERT_WRITE_FAIL		-21
#define E_WM_CERT_VERIFY_FAIL		-22
#define E_WM_ID_NOT_FOUND			-23
#define	E_WM_INVALID_PACKET			-24
#define E_WM_SERVER_RESPONSE_FAIL	-25
#define E_WM_BUFFER_NULL			-26
#define E_WM_MSCHAP_AUTH_RESP_ERROR					-27
#define E_WM_MSCHAP_AUTH_RESP_INVALID_IDENTIFIER	-28
#define E_WM_NO_SSL_CONTEXT							-29
#define E_WM_TLS_INVALID_PACKET_LENGTH				-30
#define E_WM_TTLS_INVALID_MSG_SEQ					-31
#define E_WM_EAPHANDLE_NULL							-32
#define E_WM_WAIT_TIMEOUT							-33
#define E_WM_SSL_NULL								-34
#define E_WM_NOT_SUPPORTED_TLS_EXTENSTION_TYPE		-35

#define E_CERT_INVALID_DATE			42
#define E_CERT_EXPIRED				45
#endif
