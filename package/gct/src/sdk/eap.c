#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdarg.h>
#include <assert.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "gcttype.h"
#include "error.h"
#include "device.h"
#include "wimax.h"
#include "hci.h"
#include "fload.h"
#include "log.h"
#include "eap.h"
#include "sdk.h"
#include "eap/lib/include/eaplib.h"
#include "eap/lib/include/eapretcodes.h"


#define EAP_TMP_DIR			"./eaptmp"
#define EAP_LOG_DIR			"eaplog"
#define PCAP_LOG_FILE		"pcap.pcap"
#define DEC_LOG_FILE		"dec.log"

#if defined(CONFIG_HOST_EAP)
#define EAP_TMP_EXP		"eaptmp"

#define DEV_CERT_EXP		".devcert"
#define ROOT_CACERT_EXP		".cacert"

#define MAX_EAP_ERR_STR	128

#if defined(SDK_TEST)
#define PROFILE_EAP
#define PRINT_EAP_DBG
#else
//#define PROFILE_EAP
//#define PRINT_EAP_DBG
#endif

#if defined(PROFILE_EAP)
static int elapsed_lib_time;
static int eap_index;
static struct timeval elapsed_eap_tv;
static unsigned int elapsed_time_us(struct timeval *start)
{
	struct timeval tv, now;
	unsigned int us;

	gettimeofday(&now, NULL);

	timersub(&now, start, &tv);

	us = tv.tv_sec * 1000000 + tv.tv_usec;
	return us;
}
#define elapsed_time_ms(start)	(elapsed_time_us(start)/1000)
#endif

#if defined(CONFIG_OMA_DM_CLIENT)
static int eap_get_dev_idx(EAPHANDLE handle)
{
	dev_mng_t *dm = dm_get_dev_mng();
	device_t *dev;
	int cnt = 0, i, dev_idx = -1;

	for (i = 0; i < MAX_DEVICE; i++) {
		if ((dev = dm->devices[i]) != NULL) {
			if (dev->wimax->dev_info.eap_handle == handle) {
				dev_idx = i;
				break;
			}
			if (++cnt >= dm->dev_cnt) {
				xprintf(SDK_ERR, "Not found handle(0x%08X)\n", handle);
				break;
			}
		}
	}

	return dev_idx;
}

int eap_noti_code_text(int dev_idx, short code, const char *str, bool is_err)
{
	char msg[MAX_EAP_ERR_STR];
	int ret;

	memcpy(msg, &code, sizeof(code));
	strcpy(&msg[sizeof(code)], str);

	ret = sdk_ind_noti(dev_idx,
		(is_err ? GCT_API_ERROR_NOTI_EAP : GCT_API_NOTI_EAP),
		GCT_API_NOTI_TYPE_CODE, msg, sizeof(code)+strlen(str));

	return ret;
}
#endif

static int eap_noti_code(int dev_idx, short code, bool is_err)
{
	int ret;

	ret = sdk_ind_noti(dev_idx,
		(is_err ? GCT_API_ERROR_NOTI_EAP : GCT_API_NOTI_EAP),
		GCT_API_NOTI_TYPE_CODE, (char *)&code, sizeof(code));

	return ret;
}

int eap_noti_text(int dev_idx, char *str, bool is_err)
{
	char err_str[MAX_EAP_ERR_STR];
	int ret;

	strcpy(err_str, "EAP ");
	strcat(err_str, str);

	ret = sdk_ind_noti(dev_idx,
		(is_err ? GCT_API_ERROR_NOTI_EAP : GCT_API_NOTI_EAP),
		GCT_API_NOTI_TYPE_TEXT, err_str, strlen(err_str));

	return ret;
}

static void print_eap_info(wm_eap_param_t *eapp)
{
	xprintf(SDK_DBG, "type=%d\n", eapp->type);
	if (eapp) {
		if (eapp->use_nv_info)
			xprintf(SDK_DBG, "Use NV-param Info\n");
		else {
			xprintf(SDK_DBG, "No use NV-param Info\n");
		}
		xprintf(SDK_DBG, "frag_size=%d\n", eapp->frag_size);
		xprintf(SDK_DBG, "use_delimiter=%d\n", eapp->use_delimiter);
	}
}

#if !defined(USE_MEM_CERT)
static void print_eap_file(wm_eap_param_t *eapp)
{
	int i;

	for (i = 0; i < CACERT_NUM; i++) {
		if (eapp->ca_cert_file[i])
			xprintf(SDK_DBG, "ca_cert_file[%d]=%s\n", i, eapp->ca_cert_file[i]);
	}

	xprintf(SDK_DBG, "dev_cert_file=%s\n", eapp->dev_cert_file);
	xprintf(SDK_DBG, "dev_cert_key_file=%s\n", eapp->dev_cert_key_file);
}

static int init_tls_certs(int dev_idx, wm_eap_param_t *eapp)
{
	device_t *dev;
	int ret = 0, ca_certs = 0;

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	xfunc_in();

 	if (eapp->use_nv_cert) {
 		sprintf(eapp->dev_cert_file, "%s/%s%s", EAP_TMP_DIR, dev->name, DEV_CERT_EXP);
		eapp->dev_cert_key_file[0] = 0;
		memset(eapp->ca_cert_file, 0, sizeof(eapp->ca_cert_file));

		ret = fl_upload(dev_idx, DLIMG_DEV_CERT, eapp->dev_cert_file, NULL);
		if (ret < 0) {
			eapp->dev_cert_file[0] = 0;
			if (ret != -ENOENT) goto out;
		}

		sprintf(eapp->ca_cert_file[ca_certs], "%s/%s%s%d", EAP_TMP_DIR,
			dev->name, ROOT_CACERT_EXP, ca_certs);
		ret = fl_upload(dev_idx, DLIMG_CERT1_U, eapp->ca_cert_file[ca_certs], NULL);
		if (!ret) ca_certs++;
		else if (ret != -ENOENT) goto out;
			
		sprintf(eapp->ca_cert_file[ca_certs], "%s/%s%s%d", EAP_TMP_DIR,
			dev->name, ROOT_CACERT_EXP, ca_certs);
		ret = fl_upload(dev_idx, DLIMG_CERT1_L, eapp->ca_cert_file[ca_certs], NULL);
		if (!ret) ca_certs++;
		else if (ret != -ENOENT) goto out;
			
		sprintf(eapp->ca_cert_file[ca_certs], "%s/%s%s%d", EAP_TMP_DIR,
			dev->name, ROOT_CACERT_EXP, ca_certs);
		ret = fl_upload(dev_idx, DLIMG_CERT2_U, eapp->ca_cert_file[ca_certs], NULL);
		if (!ret) ca_certs++;
		else if (ret != -ENOENT) goto out;
			
		sprintf(eapp->ca_cert_file[ca_certs], "%s/%s%s%d", EAP_TMP_DIR,
			dev->name, ROOT_CACERT_EXP, ca_certs);
		ret = fl_upload(dev_idx, DLIMG_CERT_BIG, eapp->ca_cert_file[ca_certs], NULL);
		if (!ret) ca_certs++;
		else if (ret != -ENOENT) goto out;
		ret = 0;
		eapp->ca_cert_file[ca_certs][0] = 0;
	}
out:
	#if defined(PRINT_EAP_DBG)
	xprintf(SDK_NOTICE, "CA cert. count = %d\n", ca_certs);
	#endif
	print_eap_file(eapp);

	xfunc_out("ret=%d", ret);
	dm_put_dev(dev_idx);
	return ret;
}
#else
typedef struct cert_mem_s {
	CERTMEM		dev_cert;
	CERTMEMSTORE	ca_cert;

} cert_mem_t;

static void *alloc_filedata(file_data_t *pfd, int size)
{
	assert(size);
	pfd->data = sdk_malloc(size);
	pfd->len = size;
	assert(pfd->data);
	return pfd->data;
}

static void free_filedata(file_data_t *pfd)
{
	if (pfd->data) {
		assert(pfd->len);
		sdk_free(pfd->data);
		pfd->data = NULL;
		pfd->len = 0;
	}
}

#define alloc_certmem(p,l)		alloc_filedata((file_data_t *)p, l)
#define free_certmem(p)			free_filedata((file_data_t *)p)

static int file2data(const char *path, file_data_t *pfd)
{
	int fd, total, rest, ret;
	char *p = NULL;

	if ((fd = open(path, O_RDONLY)) < 0) {
		xprintf(SDK_STD_ERR, "Open fail(%s)\n", path);
		return -1;
	}

	rest = total = lseek(fd, 0, SEEK_END);
	lseek(fd, 0, SEEK_SET);
	p = alloc_filedata(pfd, total);

	while (rest) {
		ret = read(fd, p, rest);
		if (ret < 0) {
			xprintf(SDK_STD_ERR, "Read error(%s)\n", path);
			break;
		}

		rest -= ret;

		if (!ret && rest) {
			xprintf(SDK_STD_ERR, "Read error: rest=%d\n", rest);
			break;
		}
	}

	if (!rest)
		ret = 0;
	else
		free_filedata(pfd);
	xprintf(SDK_DBG, "Read total=%d, ret=%d\n", total, ret);

	close(fd);
	return ret;
}

static int upload2data(int dev_idx, int type, file_data_t *pfd)
{
	device_t *dev;
	char tmpfile[256];
	int ret;

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	sprintf(tmpfile, "%s.%s", dev->name, EAP_TMP_EXP);

	ret = fl_upload(dev_idx, type, tmpfile, NULL);
	xprintf(SDK_INFO, "Load type=0x%X, ret=%d\n", type, ret);
	if (!ret)
		ret = file2data(tmpfile, pfd);

	unlink(tmpfile);
	dm_put_dev(dev_idx);
	return ret;
}

static void setup_cert_mem(wm_eap_param_t *eapp)
{
	cert_mem_t *cm = (cert_mem_t *) &eapp->eap_data;
	int i, total = 0;
	#if defined(PRINT_EAP_DBG)
	int ca_certs = 0;
	#endif
	char *p;
	
	cm->dev_cert.nSize = eapp->dev_cert_data.len;
	eapp->dev_cert_data.len = 0;
	cm->dev_cert.p_cCertMem = eapp->dev_cert_data.data;
	eapp->dev_cert_data.data = NULL;

	for (i = 0; i < numof_array(eapp->ca_cert_data); i++)
		total += eapp->ca_cert_data[i].len;

	if (total) {
		cm->ca_cert.nCount = 1;
		alloc_certmem(&cm->ca_cert.stCert[0], total+CACERT_NUM/*for linefeed*/);
		p = cm->ca_cert.stCert[0].p_cCertMem;
		assert(p);

		for (i = 0; i < numof_array(eapp->ca_cert_data); i++) {
			if (eapp->ca_cert_data[i].len) {
				#if defined(PRINT_EAP_DBG)
				ca_certs++;
				#endif
				assert(eapp->ca_cert_data[i].data);
				memcpy(p, eapp->ca_cert_data[i].data, eapp->ca_cert_data[i].len);
				p += eapp->ca_cert_data[i].len;
				*p++ = CERT_LINE_FEED;
				*p = 0;
				free_filedata(&eapp->ca_cert_data[i]);
			}
		}
	}

	#if defined(PRINT_EAP_DBG)
	xprintf(SDK_NOTICE, "CA cert. count = %d\n", ca_certs);
	#endif
}

static int init_tls_certs(int dev_idx, wm_eap_param_t *eapp)
{
	int ret = 0, ca_certs = 0, i;
	#if defined(PRINT_EAP_DBG)
	int lmask = SDK_NOTICE;
	#else
	int lmask = SDK_INFO;
	#endif

	xfunc_in();

	memset(&eapp->dev_cert_data, 0, sizeof(eapp->dev_cert_data));
	memset(&eapp->ca_cert_data, 0, sizeof(eapp->ca_cert_data));

	if (eapp->use_nv_cert) {
		if (!eapp->dev_cert_null) {
			ret = upload2data(dev_idx, DLIMG_DEV_CERT, &eapp->dev_cert_data);
			if (ret < 0 && ret != -ENOENT)
				goto out;
		}

		if (!eapp->ca_cert_null) {
			ret = upload2data(dev_idx, DLIMG_CERT1_U, &eapp->ca_cert_data[ca_certs]);
			if (!ret) 	ca_certs++;
			else if (ret != -ENOENT) goto out;
			
			ret = upload2data(dev_idx, DLIMG_CERT1_L, &eapp->ca_cert_data[ca_certs]);
			if (!ret) 	ca_certs++;
			else if (ret != -ENOENT) goto out;
			
			ret = upload2data(dev_idx, DLIMG_CERT2_U, &eapp->ca_cert_data[ca_certs]);
			if (!ret) 	ca_certs++;
			else if (ret != -ENOENT) goto out;
			
			ret = upload2data(dev_idx, DLIMG_CERT_BIG, &eapp->ca_cert_data[ca_certs]);
			if (!ret) 	ca_certs++;
			else if (ret != -ENOENT) goto out;
		}

		ret = 0;
	}
	else {
		if (eapp->dev_cert_file[0] && !eapp->dev_cert_null) {
			if ((ret = file2data(eapp->dev_cert_file, &eapp->dev_cert_data)) < 0)
				goto out;
			xprintf(lmask, "dev_cert_file=%s\n", eapp->dev_cert_file);
			if (eapp->dev_cert_key_file[0]) {
				file_data_t dev_cert_key_data;
				file_data_t merged;
				char *p;
				int len;

				if ((ret = file2data(eapp->dev_cert_key_file, &dev_cert_key_data)) < 0)
					goto out;
				xprintf(lmask, "dev_cert_key_file=%s\n", eapp->dev_cert_key_file);

				len = eapp->dev_cert_data.len + dev_cert_key_data.len + 16/*reserved*/;
				p = alloc_filedata(&merged, len);

				memcpy(p, eapp->dev_cert_data.data, eapp->dev_cert_data.len);
				p += eapp->dev_cert_data.len;
				if (*p != CERT_LINE_FEED)
					*p++ = CERT_LINE_FEED;
				memcpy(p, dev_cert_key_data.data, dev_cert_key_data.len);

				free_filedata(&eapp->dev_cert_data);
				free_filedata(&dev_cert_key_data);
				memcpy(&eapp->dev_cert_data, &merged, sizeof(file_data_t));
			}
		}
		else
			xprintf(lmask, "dev_cert_file was not set!\n");

		if (!eapp->ca_cert_null) {
			for (i = 0; i < CACERT_NUM; i++) {
				if (eapp->ca_cert_file[i][0]) {
					ret = file2data(eapp->ca_cert_file[i], &eapp->ca_cert_data[ca_certs++]);
					if (ret < 0)
						goto out;

					xprintf(lmask, "ca_cert_file[%d]=%s\n", i, eapp->ca_cert_file[i]);
				}
			}
		}
	}

	xprintf(lmask, "ca_certs=%d\n", ca_certs);

	if (ca_certs > CACERT_NUM) {
		xprintf(SDK_ERR, "Too many ca certifications!!", ca_certs);
		ret = -1;
	}

out:
	if (!ret)
		setup_cert_mem(eapp);
	xfunc_out("ret=%d", ret);
	dm_put_dev(dev_idx);
	return ret;
}

static void free_eap_param(wm_eap_param_t *eapp)
{
	int i;

	free_filedata(&eapp->dev_cert_data);

	for (i = 0; i < numof_array(eapp->ca_cert_data); i++)
		free_filedata(&eapp->ca_cert_data[i]);
}
#endif

static int load_tls_nv_param(int dev_idx, wm_eap_param_t *eapp)
{
	char buf[EAP_NVPARAM_SIZE];
	char *p = buf;
	int ret, len;
	u8 T;
	u16 L;
	char *V;

	xfunc_in();

	ret = bl_upload(dev_idx, DLIMG_EAP_PARAM, p, sizeof(buf), NULL);
	if (ret > 0) {
		while (ret) {
			T = p[0];
			L = U82U16(&p[1]);
			if (L > WIMAX_EAP_STR_LEN) {
				xprintf(SDK_ERR, "Length(%d) is too long\n", L);
				ret = -1;
				break;
			}
			V = &p[3];
			switch (T) {
				case eap_param_type_outer_nai:
					memcpy(eapp->anony_id, V, L);
					eapp->anony_id[L] = 0;
					break;
				case eap_param_type_priv_key_pwd:
					memcpy(eapp->pri_key_pwd, V, L);
					eapp->pri_key_pwd[L] = 0;
					break;
				case eap_param_type_inner_id:
					memcpy(eapp->userid, V, L);
					eapp->userid[L] = 0;
					break;
				case eap_param_type_inner_pwd:
					memcpy(eapp->userid_pwd, V, L);
					eapp->userid_pwd[L] = 0;
					break;
				default:
					xprintf(SDK_ERR, "Unknown eap param type(%u)\n", T);
					break;
			}
			len = 1/*type*/ + 2/*lenth*/ + L/*data lenth*/;
			ret -= len;
			p += len;
		}
	}
	#if defined(SDK_TEST)
	xprintf(SDK_FORCE, "eapp->anony_id=[%s]\n", eapp->anony_id);
	xprintf(SDK_FORCE, "eapp->pri_key_pwd=[%s]\n", eapp->pri_key_pwd);
	xprintf(SDK_FORCE, "eapp->userid=[%s]\n", eapp->userid);
	xprintf(SDK_FORCE, "eapp->userid_pwd=[%s]\n", eapp->userid_pwd);
	#endif

	xfunc_out("ret=%d", ret);

	return ret;
}

int SetPseudonymID(char id[])
{
	sprintf(id, "%04X%04X%04X%04X%04X%04X%04X%04X",
		(u16) rand(), (u16) rand(), (u16) rand(), (u16) rand(),
		(u16) rand(), (u16) rand(), (u16) rand(), (u16) rand());

	return 0;
}

#if defined(CONFIG_OMA_DM_CLIENT)
static bool match_sub_realms(const char *realm, const char *realm2)
{
	int rlen, rlen2, index;

	rlen = strlen(realm);
	rlen2 = strlen(realm2);

	if (rlen2 > rlen) {
		index = rlen2 - rlen;
		if (!strcasecmp(realm + index, realm2) && realm[index - 1] == '.')
			return TRUE;
	}
	else if (rlen > rlen2) {
		index = rlen - rlen2;
		if (!strcasecmp(realm2 + index, realm) && realm2[index - 1] == '.')
			return TRUE;
	}
	else {
		if (!strcasecmp(realm2, realm))
			return TRUE;
	}

	return FALSE;
}
#endif

static int chk_realm_cb(EAPHANDLE handle, const char *server_realm)
{
	#if defined(APPLY_REALM_VERIFICATION)
	bool realm_ok = FALSE;
	#else
	bool realm_ok = TRUE;
	#endif
	#if defined(CONFIG_OMA_DM_CLIENT)
	device_t *dev;
	int dev_idx;
	wm_eap_param_t *eapp;
	char tmp[128];
	char realm[128];
	unsigned this_mask = SDK_INFO;
	int i;
	int ret;

	xfunc_in("handle=0x%08X", handle);

	if ((dev_idx = eap_get_dev_idx(handle)) < 0)
		return -1;

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	eapp = &dev->wimax->dev_info.eapp;

	xprintf(this_mask, "Server Cert Realm:%s, check_realm=%d\n",
		server_realm, eapp->check_realm);
	
	if (eapp->check_realm) {
		if (eapp->cr901_enabled || eapp->cr1074_enabled) {
			if (ParsingOuterNAI(eapp->anony_id, tmp, tmp, tmp, realm) < 0)
				goto out;
			
			/*Check with OuterNAI's realm*/
			xprintf(this_mask, "realm: %s\n", realm);
			if (match_sub_realms(realm+1, server_realm)) {
				xprintf(this_mask, "Received server realm(%s) matched "
					"with OuterNAI's realm(%s)\n", server_realm, realm);
				realm_ok = TRUE;
			}
		}

		/*Check with configured server realm.*/
		if (!realm_ok) {
			for (i = 0; i < eapp->configured_realm_list_cnt; i++) {
				if (match_sub_realms(eapp->configured_realm_list[i], server_realm)) {
					xprintf(this_mask, "Received server realm[%s] matched "
						"with configured realm[%s]\n",
						server_realm, eapp->configured_realm_list[i]);
					realm_ok = TRUE;
					break;
				}
			}
		}

		if (!realm_ok)
			ret = eap_noti_code_text(dev_idx, E_WM_UNKNOWN_SERVER_REALM,
				server_realm, TRUE);
		
	}
	else
		realm_ok = TRUE;

out:
	dm_put_dev(dev_idx);
	xfunc_out();
	#endif
	return realm_ok;
}

#if defined(USE_MEM_CERT)
static int setup_auth(wm_eap_param_t *eapp, EAPAUTHSTR *auth)
{
	int lmask = SDK_INFO;
	cert_mem_t *cm = (cert_mem_t *) &eapp->eap_data;

	auth->ucPrivateCertFlags = (unsigned char) CERT_MEM;
	auth->ucCACertFlags = (unsigned char) CERT_MEM;

	xprintf(lmask, "dev_cert.nSize=%d\n", cm->dev_cert.nSize);
	xprintf(lmask, "ca_cert.nCount=%d\n", cm->ca_cert.nCount);

	if (!eapp->dev_cert_null && cm->dev_cert.nSize)
		auth->p_cPrivateCert = (char *) &cm->dev_cert;
	else
		auth->p_cPrivateCert = NULL;

	if (eapp->ca_cert_null)
		auth->p_cCACert = NULL;
	else
		auth->p_cCACert = (char *) &cm->ca_cert;

	return 0;
}

static void cleanup_cert(wm_eap_param_t *eapp)
{
	cert_mem_t *cm = (cert_mem_t *) &eapp->eap_data;

	free_certmem(&cm->dev_cert);

	if (cm->ca_cert.nCount) {
		free_certmem(&cm->ca_cert.stCert[0]);
		cm->ca_cert.nCount = 0;
	}

	free_eap_param(eapp);
}
#else
static int setup_auth(wm_eap_param_t *eapp, EAPAUTHSTR *auth)
{
	char *client_cert_list;
	char *cacert_list;
	char *p_cacert;
	int ret = 0, i;

	client_cert_list = sdk_malloc(WIMAX_EAP_FILE_LEN*2);
	assert(client_cert_list);
	p_cacert = cacert_list = sdk_malloc(WIMAX_EAP_FILE_LEN * CACERT_NUM + CACERT_NUM);
	assert(cacert_list);

	client_cert_list[0] = 0;
	cacert_list[0] = 0;

	for (i = 0; i < CACERT_NUM; i++) {
		if (strlen(eapp->ca_cert_file[i])) {
			if ((ret = access(eapp->ca_cert_file[i], 0)) < 0) {
				xprintf(SDK_ERR, "access fail(%s) %s(%d)\n",
					eapp->ca_cert_file[i], strerror(errno), errno);
				goto out;
			}
			p_cacert += sprintf(p_cacert, "%s;", eapp->ca_cert_file[i]);
		}
	}
	xprintf(SDK_DBG, "cacert_list=%s\n", cacert_list);

	if (eapp->dev_cert_file[0]) {
		if ((ret = access(eapp->dev_cert_file, 0)) < 0) {
			xprintf(SDK_ERR, "access fail(%s) %s(%d)\n",
				eapp->dev_cert_file, strerror(errno), errno);
			goto out;
		}

		if (strlen(eapp->dev_cert_key_file))
			sprintf(client_cert_list,"cert:%s;key:%s",
				eapp->dev_cert_file, eapp->dev_cert_key_file);
		else
			sprintf(client_cert_list,"cert:%s;key:%s",
				eapp->dev_cert_file, eapp->dev_cert_file);
	}

	if (eapp->ca_cert_null)
		auth->p_cCACert = NULL;
	else
		auth->p_cCACert = cacert_list;

	if (!eapp->dev_cert_null && client_cert_list[0])
		auth->p_cPrivateCert = client_cert_list;
	else
		auth->p_cPrivateCert = NULL;
out:
	if (ret < 0) {
		sdk_free(client_cert_list);
		sdk_free(cacert_list);
	}
	return ret;
}
static void cleanup_cert(wm_eap_param_t *eapp)
{
}
#endif

static int init_tls_param(int dev_idx, wm_eap_param_t *eapp)
{
	device_t *dev;
	wimax_t *wm;
	u8 *mac;
	EAPDASTR2 eap_tls;
	unsigned long sp_flags = STR_EAPDA2;
	char anony_id[256], userid[256], userid_pass[256];
	char routing_info[128], wm_decoration[128], id[128], realm[128];
	char outer_nai[256];
	char eaplib_ver[64];
	int ret = 0;
	int eap_log_mask = SDK_DBG;
	#if defined(PROFILE_EAP)
	struct timeval start;
	unsigned int elapsed_ms;

	eap_index = 0;
	#endif

	#if defined(SDK_TEST)
	eap_log_mask = SDK_FORCE;
	#endif

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	xfunc_in("type=%d", eapp->type);

	wm = dev->wimax;

	mac = wm->dev_info.device_mac;

	memset(&eap_tls, 0x00, sizeof(eap_tls));
	routing_info[0] = wm_decoration[0] = id[0] = realm[0] = 
		outer_nai[0] = eaplib_ver[0] = 0;
	
	#if defined(CONFIG_OMA_DM_CLIENT)
	if (SDK_USE_ODM)
		odm_setup_eap_param(dev_idx, userid, userid_pass, anony_id);
	else {
		strcpy(userid, eapp->userid);
		strcpy(userid_pass, eapp->userid_pwd);
		strcpy(anony_id, eapp->anony_id);
	}
	#else
	strcpy(userid, eapp->userid);
	strcpy(userid_pass, eapp->userid_pwd);
	strcpy(anony_id, eapp->anony_id);
	#endif
renew:
	if (!wm->dev_info.eap_handle) {
		wm->dev_info.eap_handle = TLSNewSessionHND();
		if (!wm->dev_info.eap_handle) {
			xprintf(SDK_ERR, "handle was NULL\n");
			ret = -1;
			goto out;
		}
		else
			xprintf(SDK_DBG, "Set handle: 0x%08X\n", wm->dev_info.eap_handle);
	}
	else {
		TLSDeleteSessionHND(wm->dev_info.eap_handle);
		wm->dev_info.eap_handle = NULL;
		goto renew;
	}

	eap_tls.usFragmentSize = eapp->frag_size;
	eap_tls.usAuthStrCount = 1;

	/*
		#define ENABLE_AUTH_TTLS_MSCHAPV2_STRICT	0x1000
		#define ENABLE_AUTH_TLS_NEXT_FRAG_NO_LENGTH_BIT	0x2000
	*/

	eap_tls.stAuthStr[0].usAuthFlags = 
		ENABLE_WRITE_PCAP | ENABLE_LENGTH_BIT_ALWAYS|
		ENABLE_AUTH_TTLS_MSCHAPV2_STRICT | ENABLE_AUTH_TLS_NEXT_FRAG_NO_LENGTH_BIT |
		ENABLE_AUTH_REMOVE_TLS_DH_CIPHER;

	switch (eapp->type) {
		case W_EAP_TLS:
			xprintf(eap_log_mask, "TLS\n");
			eap_tls.stAuthStr[0].usAuthFlags |= ENABLE_AUTH_TLS;
			eap_tls.stAuthStr[0].usAuthFlags |= ENABLE_AUTH_TLS_SEND_ALL_CERT;
			break;
		case W_EAP_TTLS_MD5:
			xprintf(eap_log_mask, "W_EAP_TTLS_MD5\n");
			eap_tls.stAuthStr[0].usAuthFlags |= ENABLE_AUTH_TTLS;
			eap_tls.stAuthStr[0].usAuthFlags |= ENABLE_AUTH_TTLS_MD5;
			break;
		case W_EAP_TTLS_MSCHAPV2:
			xprintf(eap_log_mask, "W_EAP_TTLS_MSCHAPV2\n");
			eap_tls.stAuthStr[0].usAuthFlags |= ENABLE_AUTH_TTLS;
			eap_tls.stAuthStr[0].usAuthFlags |= ENABLE_AUTH_TTLS_MSCHAPV2;
			eap_tls.stAuthStr[0].usAuthFlags |= ENABLE_AUTH_TTLS_MSCHAPV2_NO_SVR_CHECK;
			break;
		case W_EAP_TTLS_CHAP:
			xprintf(eap_log_mask, "W_EAP_TTLS_CHAP\n");
			eap_tls.stAuthStr[0].usAuthFlags |= ENABLE_AUTH_TTLS;
			eap_tls.stAuthStr[0].usAuthFlags |= ENABLE_AUTH_TTLS_CHAP;
			break;
	}

	if (eapp->use_delimiter)
		eap_tls.stAuthStr[0].usAuthFlags |= ENABLE_DOMAIN_CHAR_ALPHA;

	if (!eapp->disable_resumptoin)
		eap_tls.stAuthStr[0].usAuthFlags |= ENABLE_SESSION_RESUME;

	eap_tls.stAuthStr[0].ulAuthFlagsExt |= ENABLE_NO_SVR_CERT_EXPIRE_CHK;
	if (eapp->disable_sessionticket)
		eap_tls.stAuthStr[0].ulAuthFlagsExt |= ENABLE_NO_SESSION_TICKET;

	if (wm->scan.selected_subs && wm->scan.selected_subs->activated &&
		(eapp->configured_realm_list_cnt || eapp->cr901_enabled)) {
		eap_tls.stAuthStr[0].ulAuthFlagsExt = ENABLE_REALM_CHECK;
		eap_tls.stAuthStr[0].nRealmCount = 0;
		eap_tls.stAuthStr[0].pp_cRealmList = NULL;
		eap_tls.stAuthStr[0].fnCBRealmProc = (CBEAPREALMPROC) chk_realm_cb;
	}

	ret = setup_auth(eapp, (EAPAUTHSTR *) &eap_tls.stAuthStr[0]);
	if (ret < 0)
		goto out;

	if (strlen(eapp->pri_key_pwd))
		eap_tls.stAuthStr[0].p_cPrivatePassword = eapp->pri_key_pwd;
	else
		eap_tls.stAuthStr[0].p_cPrivatePassword = NULL;

	if (strlen(anony_id)) {
		eap_tls.stAuthStr[0].p_cAnonymousId = anony_id;

		if (ParsingOuterNAI(anony_id, routing_info, wm_decoration, id, realm)) {
			if (strlen(eapp->visited_realm)) {
				xprintf(eap_log_mask, "visited_realm=%s, realm=%s\n ",
					eapp->visited_realm, realm);
				if (strlen(realm)) {
					if (strcmp(eapp->visited_realm, realm)) {
						sprintf(routing_info, "%s!", realm + 1/*skip '@'*/);
						sprintf(realm, "@%s", eapp->visited_realm);
					}
				}
				else
					sprintf(realm, "@%s", eapp->visited_realm);
			}
			if (!strcasecmp(id, "MAC") || eapp->cr801_mode == CR801_TLS) {
				sprintf(outer_nai,"%s%s%02X%02X%02X%02X%02X%02X%s",
					routing_info,wm_decoration,
					mac[0], mac[1], mac[2], mac[3], mac[4], mac[5],
					realm);
			}
			else if (!strcmp(id,"RANDOM")) {
				SetPseudonymID(id);
				sprintf(outer_nai, "%s%s%s%s", routing_info, wm_decoration, id, realm);
			}
			else
				strcpy(outer_nai, anony_id);
		}
		xprintf(eap_log_mask, "outer_nai=%s\n", outer_nai);
	}

	if (strlen(outer_nai))
		eap_tls.stAuthStr[0].p_cAnonymousId = outer_nai;
	else
		eap_tls.stAuthStr[0].p_cAnonymousId = userid;

	if (eapp->type == W_EAP_TLS) {
		if (strlen(outer_nai))
			eap_tls.stAuthStr[0].p_cID = outer_nai;
		else if (strlen(anony_id))
			eap_tls.stAuthStr[0].p_cID = anony_id;

		eap_tls.stAuthStr[0].p_cPassword = eapp->pri_key_pwd;
	}
	else {	/* TTLS */
		eap_tls.stAuthStr[0].p_cID = userid;
		eap_tls.stAuthStr[0].p_cPassword = userid_pass;
	}

	TLSGetVersion(eaplib_ver, sizeof(eaplib_ver));
	xprintf(SDK_NOTICE, "EAP Lib. Verion:%s\n", eaplib_ver);

	xprintf(eap_log_mask, "usAuthFlags=0x%08X\n", (int) eap_tls.stAuthStr[0].usAuthFlags);
	xprintf(eap_log_mask, "ulAuthFlagsExt=0x%08X\n", (int) eap_tls.stAuthStr[0].ulAuthFlagsExt);
	xprintf(eap_log_mask, "ucCACertFlags=0x%08X\n", (int) eap_tls.stAuthStr[0].ucCACertFlags);
	xprintf(eap_log_mask, "ucPrivateCertFlags=0x%08X\n", (int) eap_tls.stAuthStr[0].ucPrivateCertFlags);
	if (eap_tls.stAuthStr[0].ucCACertFlags == CERT_MEM)
		xprintf(eap_log_mask, "p_cCACert=0x%08X\n", eap_tls.stAuthStr[0].p_cCACert);
	else
		xprintf(eap_log_mask, "p_cCACert=%s\n", eap_tls.stAuthStr[0].p_cCACert);
	if (eap_tls.stAuthStr[0].ucPrivateCertFlags == CERT_MEM)
		xprintf(eap_log_mask, "p_cPrivateCert=0x%08X\n", eap_tls.stAuthStr[0].p_cPrivateCert);
	else
		xprintf(eap_log_mask, "p_cPrivateCert=%s\n", eap_tls.stAuthStr[0].p_cPrivateCert);
	xprintf(eap_log_mask, "p_cPrivatePassword='%s'\n", eap_tls.stAuthStr[0].p_cPrivatePassword);
	xprintf(eap_log_mask, "p_cAnonymousId='%s'\n", eap_tls.stAuthStr[0].p_cAnonymousId);
	xprintf(eap_log_mask, "p_cID='%s'\n", eap_tls.stAuthStr[0].p_cID);
	xprintf(eap_log_mask, "p_cPassword='%s'\n", eap_tls.stAuthStr[0].p_cPassword);

	#if defined(PROFILE_EAP)
	gettimeofday(&start, NULL);
	#endif
	
	ret = TLSSetParametersExtHND(wm->dev_info.eap_handle, sp_flags, (EAPDASTR*)&eap_tls);

	#if !defined(USE_MEM_CERT)
	if (eap_tls.stAuthStr[0].ucCACertFlags != CERT_MEM) {
		if (eap_tls.stAuthStr[0].p_cCACert)
			sdk_free(eap_tls.stAuthStr[0].p_cCACert);
	}

	if (eap_tls.stAuthStr[0].ucPrivateCertFlags != CERT_MEM) {
		if (eap_tls.stAuthStr[0].p_cPrivateCert)
			sdk_free(eap_tls.stAuthStr[0].p_cPrivateCert);
	}
	#endif

	#if defined(PROFILE_EAP)
	elapsed_ms = elapsed_time_ms(&start);
	xprintf(SDK_FORCE, "EAP TLSSetParameters : ret=%d, elapsed-ms=%d\n", ret, elapsed_ms);
	#endif

	if (ret < 0) {
		eap_noti_code(dev_idx, ret, TRUE);
		xprintf(SDK_ERR, " Parameter error ret=%d, Handle=0x%08X\n",
			ret, wm->dev_info.eap_handle);
		ret = -1;
	}
	else
		ret = 0;

out:
	xfunc_out("ret=%d", ret);
	dm_put_dev(dev_idx);
	return ret;
}

int eap_alert_net_entry(int dev_idx)
{
	device_t *dev;
	wimax_t *wm;
	int ret = 0;

	xfunc_in();

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;
	wm = dev->wimax;
	wm->dev_info.eapp.visited_realm[0] = 0;

	#if !defined(SUPPORT_802_11G)
	ret = eap_reinit_session(dev_idx);
	#endif

	xfunc_out();
	return ret;
}

int eap_reinit_session(int dev_idx)
{
	device_t *dev;
	wimax_t *wm;
	wm_eap_param_t *eapp;
	int ret = 0;

	xfunc_in();

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;
	wm = dev->wimax;
	eapp = &wm->dev_info.eapp;
	if (IS_EAP_TLS(eapp->type)) {
		#if defined(PROFILE_EAP)
		eap_index = 0;
		#endif
		ret = init_tls_param(dev_idx, eapp);
	}

	xfunc_out("ret=%d", ret);
	return ret;
}

int eap_init(int dev_idx)
{	
	device_t *dev;
	wimax_t *wm;
	wm_eap_param_t *eapp;
	int ret = 0;

	xfunc_in("dev=%d", dev_idx);

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	wm = dev->wimax;
	eapp = &wm->dev_info.eapp;

	eap_deinit(dev_idx);

	#if !defined(USE_MEM_CERT)
	mkdir(EAP_TMP_DIR, 0644);
	#endif
	
	print_eap_info(eapp);
	
	if (GCT_API_IS_EAP_TLS(eapp->type)) {
		if (wm->dev_info.eapp.use_nv_info)
			ret = load_tls_nv_param(dev_idx, &wm->dev_info.eapp);
		if (!ret)
			ret = init_tls_certs(dev_idx, &wm->dev_info.eapp);
		#if defined(CONFIG_OMA_DM_CLIENT)
		if (!ret && !SDK_USE_ODM)
			ret = init_tls_param(dev_idx, &wm->dev_info.eapp);
		#else
		if (!ret)
			ret = init_tls_param(dev_idx, &wm->dev_info.eapp);
		#endif
	}
	else {
		xprintf(SDK_ERR, "Type(%d) is NOT Supported\n", eapp->type);
		ret = -1;
	}

	dm_put_dev(dev_idx);
	xfunc_out("ret=%d", ret);
	return ret;
}

void eap_deinit(int dev_idx)
{
	device_t *dev;
	struct wimax_s *wm;

	xfunc_in("dev=%d", dev_idx);

	if (!(dev = dm_get_dev(dev_idx)))
		return;

	if (!sdk_get_rw_handle())
		return;

	wm = dev->wimax;

	cleanup_cert(&wm->dev_info.eapp);

	if (GCT_API_IS_EAP_TLS(wm->dev_info.eapp.type)) {
		#if !defined(USE_MEM_CERT)
		char cmd[256];
		sprintf(cmd, "rm -rf %s 2> /dev/null", EAP_TMP_DIR);
		system(cmd);
		#endif
		if (wm->dev_info.eap_handle) {
			xprintf(SDK_DBG, "Delete handle: 0x%08X\n", wm->dev_info.eap_handle);
			TLSDeleteSessionHND(wm->dev_info.eap_handle);
			wm->dev_info.eap_handle = NULL;
		}
	}
	dm_put_dev(dev_idx);
	xfunc_out();
}

static int do_eap_tls(int dev_idx, char *buf, int len)
{
	device_t *dev;
	wimax_t *wm;
	void * eap_handle;
	int ret;
	char send_buf[MAX_EAP_BUF_SIZE] = {0};
	int send_size = sizeof(send_buf);
	char msk[WIMAX_EAP_MSK_LEN+WIMAX_EAP_EMSK_LEN];
	char err_str[MAX_EAP_ERR_STR] = {0};
	int err_str_len = sizeof(err_str);
	int msk_size;
	#if defined(PROFILE_EAP)
	struct timeval start;
	unsigned int lib_time, elapsed_eap_time;
	#endif

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	wm = dev->wimax;
	eap_handle = wm->dev_info.eap_handle;

	xfunc_in("len=%d, eap_handle=%p", len, eap_handle);

	#if defined(PROFILE_EAP)
	gettimeofday(&start, NULL);
	if (!eap_index) {
		elapsed_lib_time = 0;
		memcpy(&elapsed_eap_tv, &start, sizeof(start));
		xprintf(SDK_FORCE, "----------------------------------------------------------------------------\n");
		xprintf(SDK_FORCE, "E A P : Return  Length  lib-current-time  lib-elapsed-time  eap-elapsed-time\n");
	}
	#endif

	ret = TLSDealEAPPayloadHND(eap_handle, buf, len, send_buf, &send_size);

	#if defined(PROFILE_EAP)
	lib_time = elapsed_time_ms(&start);
	elapsed_lib_time += lib_time;
	elapsed_eap_time = elapsed_time_ms(&elapsed_eap_tv);
	xprintf(SDK_FORCE, "%5d  %7d  %6d  %16d  %16d  %16d\n",
		 ++eap_index, ret, len, lib_time, elapsed_lib_time, elapsed_eap_time);
	#endif

	if (ret < 0) {
		if (wm->dev_info.eapp.cr801_enabled &&
			TLSGetFailReasonHND(eap_handle) == F_WM_SERVER_REJECT &&
			wm->dev_info.eapp.type == W_EAP_TTLS_MSCHAPV2) {
			dev->wimax->dev_info.eapp.cr801_server_reject_cnt++;
			ret = E_WM_CR801_EAP_FAILURE;
		}

		if (ret == E_WM_MSCHAP_AUTH_RESP_ERROR) {
			TLSGetAuthErrorMsgHND(eap_handle, err_str, &err_str_len);
			eap_noti_text(dev_idx, err_str, TRUE);
			xprintf(SDK_ERR, "%s\n", err_str);
		}
		else {
			eap_noti_code(dev_idx, ret, TRUE);
			xprintf(SDK_ERR, "EAP error code:%d\n", ret);
		}
		goto out;
	}

	switch (ret) {
		case S_WM_EAP_TLS_SUCCESS:
			msk_size = sizeof(msk);
			memset(msk, 0x00, sizeof(msk));
			TLSGetMSKHND(eap_handle, msk, &msk_size);
			#if defined(CONFIG_OMA_DM_CLIENT)
			if (SDK_USE_ODM) {
				if (msk_size == WIMAX_EAP_MSK_LEN+WIMAX_EAP_EMSK_LEN)
					ODM_SetEMSK(dev_idx, msk+WIMAX_EAP_MSK_LEN, WIMAX_EAP_EMSK_LEN);
			}
			#endif
			if (msk_size > WIMAX_EAP_MSK_LEN)
				msk_size = WIMAX_EAP_MSK_LEN;
			ret = send_eap_tls_mask(dev_idx, msk, WIMAX_EAP_MSK_LEN);
			break;
		case S_WM_EAP_TLS_NOTIFICATION:
			ret = TLSGetNotificationDataHND(eap_handle, err_str, &err_str_len);
			if (ret == E_WM_SUCCESS)
				ret = eap_noti_text(dev_idx, send_buf, FALSE);
			else
				xprintf(SDK_ERR, "EAP TLS Notification Length is Over! (0x%x)\n", err_str_len);
			break;
		default:
			ret = send_eap_tls_resp(dev_idx, send_buf, send_size);
			break;
	}
out:
	xfunc_out("ret=%d", ret);
	dm_put_dev(dev_idx);
	return ret;
}
#endif

unsigned int ParsingOuterNAI(const char *Outer_NAI,			// IN
						  char		 *routing_info,			// OUT
						  char		 *wm_decoration,		// OUT
						  char		 *userid,				// OUT
						  char		 *realm)				// OUT
{
	char		  ch;
	enum { OUT_SCOPE, IN_SCOPE };

	unsigned char fRoutingInfo		= OUT_SCOPE;
	unsigned char fWiMAXDecoration	= OUT_SCOPE;
	unsigned char fRealm			= OUT_SCOPE;

	unsigned int  i;
	unsigned int  nRoutingInfo		= 0;	//index for Routing Info.
	unsigned int  nWiMAXDecoration  = 0;	//index for wm_decoration.
	unsigned int  nUserID			= 0;
	unsigned int  nRealm			= 0;

	if( Outer_NAI == NULL )
		return -1;

	if( strlen(Outer_NAI) == 0 )
		return -2;

	if (routing_info)
		routing_info[0] = 0;
	if (wm_decoration)
		wm_decoration[0] = 0;
	if (userid)
		userid[0] = 0;
	if (realm)
		realm[0] = 0;

	for( i = 0; i < strlen(Outer_NAI) ; i++ )
	{
		ch = Outer_NAI[i];

		if( ( ch == ' ') || (ch == '\t') || (ch == '\n') || (ch == '\r')) // skip character.
			continue;

		if( ch == '[' )
		{
			fRoutingInfo = IN_SCOPE;
			if (routing_info)
				routing_info[nRoutingInfo++] = ch;
		}
		else if( (fRoutingInfo == IN_SCOPE ) && ( ch == ']') )
		{
			fRoutingInfo = OUT_SCOPE;
			if (routing_info)
				routing_info[nRoutingInfo++] = ch;
		}
		else if( fRoutingInfo == IN_SCOPE )
		{
			if (routing_info)
				routing_info[nRoutingInfo++] = ch;
		}
		else if( ch == '{')
		{
			fWiMAXDecoration = IN_SCOPE;
			if (wm_decoration)
				wm_decoration[nWiMAXDecoration++] = ch;
		}
		else if( (fWiMAXDecoration == IN_SCOPE) && ( ch == '}') )
		{
			fWiMAXDecoration = OUT_SCOPE;
			if (wm_decoration)
				wm_decoration[nWiMAXDecoration++] = ch;
		}
		else if( fWiMAXDecoration == IN_SCOPE )
		{
			if (wm_decoration)
				wm_decoration[nWiMAXDecoration++] = ch;
		}
		else if( ch == '@' )
		{
			fRealm = IN_SCOPE;
			if (realm)
				realm[nRealm++] = ch;
		}
		else if( fRealm == IN_SCOPE )
		{
			if (realm)
				realm[nRealm++] = ch;
		}
		else
		{
			if (userid)
				userid[nUserID++] = ch;
		}
	}

	if (userid)
		userid[nUserID]						= 0;
	if (realm)
		realm[nRealm]						= 0;
	if (wm_decoration)
		wm_decoration[nWiMAXDecoration]	= 0;
	if (routing_info)
		routing_info[nRoutingInfo]			= 0;	//NULL Terminated String.

	return i;
}

static void *get_eaplog_thread(void *data)
{
	#define FW_PCAP_LOG			"/log/pcap.pcap"
	#define FW_DEC_LOG			"/log/dec.log"

	int dev_idx = (int) data;
	const char *host_file, *target_file;
	int ret, lmask = SDK_INFO;

	xfunc_in("dev=%d", dev_idx);

	target_file = FW_PCAP_LOG;
	host_file = sdk_mng.eap_pcap;
	ret = fl_read_file(dev_idx, target_file, host_file, NULL);
	if (ret > 0)
		xprintf(lmask, "\t%s <= %s (%d bytes)\n", host_file, target_file, ret);

	target_file = FW_DEC_LOG;
	host_file = sdk_mng.eap_dec;
	ret = fl_read_file(dev_idx, target_file, host_file, NULL);
	if (ret > 0)
		xprintf(lmask, "\t%s <= %s (%d bytes)\n", host_file, target_file, ret);

	xfunc_out();
	return NULL;
}

void eap_start_e_eaplog_thread(int dev_idx)
{
	pthread_t pthread;

	xfunc_in();
	pthread_create(&pthread, NULL, get_eaplog_thread, (void *)dev_idx);
	pthread_detach(pthread);
	xfunc_out();
}

void eap_prepare_log_env(int dev_idx, bool enable)
{
	device_t *dev;
	static bool inited;

	if (!(dev = dm_get_dev(dev_idx)))
		return;

	xfunc_in();

	if (enable && !inited && sdk_get_rw_handle()) {
		char path[256];
		char *p_pcap = sdk_mng.eap_pcap;
		char *p_dec = sdk_mng.eap_dec;

		inited = TRUE;

		sprintf(path, "%s/%s", sdk_mng.log_path, EAP_LOG_DIR);
		mkdir(path, 0644);

		sprintf(p_pcap, "%s/%s", path, PCAP_LOG_FILE);
		sprintf(p_dec, "%s/%s", path, DEC_LOG_FILE);

		#if defined(CONFIG_HOST_EAP)
		if (!E_EAP_TLS_ENABLED(dev)) {
			TLSSetPCAPInfo(p_pcap, NULL, NULL);
			TLSSetDebugInfo(p_dec);
		}
		#endif
	}

	#if 0 //defined(CONFIG_HOST_EAP)
	if (!E_EAP_TLS_ENABLED(dev)) {
		if (enable)
			TLSSetDebugFlag(TLS_DBG_FLAG_CREAE_SESSION);
		else
			TLSSetDebugFlag(TLS_DBG_FLAG_NO);
	}
	#endif
	dm_put_dev(dev_idx);
	xfunc_out();
}

int send_eap_tls_mask(int dev_idx, char *mask, int mask_len)
{
	device_t *dev;
	struct wimax_s *wm;
	u8 buf[HCI_MAX_PARAM];
	u8 *pos = buf;
	u8 T, *V;
	u16 L;
	int len, ret;

	if (!sdk_get_rw_handle())
		return 0;

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	xfunc_in("len=%d", mask_len);

	wm = dev->wimax;

	T = TLV_T(T_MSK);
	L = (u16) mask_len;
	V = (u8 *) mask;
	pos += hci_set_tlv(pos, T, L, V);

	len = pos - buf;
	assert(len < HCI_MAX_PARAM);
	ret = hci_send(dev_idx, WIMAX_SET_INFO, buf, len);
	if (len == ret)
		ret = 0;
	else {
		xprintf(SDK_STD_ERR, "[%d] hci_send(%d!=%d)\n", dev_idx, len, ret);
		ret = sdk_set_errno(ERR_STD);
	}
	xfunc_out("ret=%d", ret);
	return ret;
}

int send_eap_tls_resp(int dev_idx, char *buf, int len)
{
	int ret;

	xfunc_in("len=%d", len);
	if (!sdk_get_rw_handle())
		return 0;

	assert(len < HCI_MAX_PARAM);
	ret = hci_send(dev_idx, WIMAX_TX_EAP, (u8 *) buf, len);
	if (len == ret)
		ret = 0;
	else {
		xprintf(SDK_STD_ERR, "[%d] hci_send(%d!=%d)\n", dev_idx, len, ret);
		ret = sdk_set_errno(ERR_STD);
	}
	xfunc_out("ret=%d", ret);
	return ret;
}

int eap_packet_handle(int dev_idx, char *buf, int len)
{
	device_t *dev;
	wimax_t *wm;
	int ret = -1;

	xfunc_out("len=%d", len);

	if (!(dev = dm_get_dev(dev_idx)))
		return -1;

	wm = dev->wimax;

	if (sdk_get_rw_handle()) {
		#if defined(CONFIG_HOST_EAP_AKA)
		if (wm->dev_info.eapp.type == W_EAP_AKA)
			ret = do_eap_aka(dev_idx, buf, len);
		#endif
		#if defined(CONFIG_HOST_EAP)
		if (IS_EAP_TLS(wm->dev_info.eapp.type))
			ret = do_eap_tls(dev_idx, buf, len);
		#else
			xprintf(SDK_ERR, "Unknown EAP Configuration!!\n");
		#endif
	}
	else
		xprintf(SDK_DBG, "In readonly mode, eap packet is not handled!\n");

	xfunc_out("ret=%d", ret);
	dm_put_dev(dev_idx);
	return ret;
}

