#!/bin/sh
# PING reboot shell script

CONFIG_GET="uci get ping_reboot.ping_reboot"
CONFIG_SET="uci set ping_reboot.ping_reboot"

HOST=`$CONFIG_GET.host`
LOG_LEVEL=`$CONFIG_GET.log_level`
TIMEOUT=`$CONFIG_GET.timeout`
PACKET_SIZE=`$CONFIG_GET.packetsize`
LOG_LEVEL_DBG=7

if [ ! `ls /tmp/ping_reboot_offset 2>/dev/null` ]; 
then
  echo `tr -cd 0-1 </dev/urandom | head -c 1``tr -cd 0-9 </dev/urandom | head -c 2` > /tmp/ping_reboot_offset
  logger -p $LOG_LEVEL "ping reboot offset is set to" `cat /tmp/ping_reboot_offset`
fi

sleep `cat /tmp/ping_reboot_offset`

if [ `$CONFIG_GET.enable` -eq 1 ] ; then
	if [ `$CONFIG_GET.reboot` -eq 1 ] ; then
	
		ping -c 1 -W $TIMEOUT -s $PACKET_SIZE $HOST > /dev/null
		
		case $? in
                0)
					`$CONFIG_SET.fail_counter=0`
					logger -p $LOG_LEVEL_DBG "PING Reboot. successful reply from $HOST"
                    exit 0 ;;
                1)
					failure=`$CONFIG_GET.fail_counter`
                    : $(( failure++ ))
					`$CONFIG_SET.fail_counter=$failure` 
					logger -p $LOG_LEVEL "PING Reboot. PING echo receive failed. Retry $failure"
			
					if [ $failure -ge `$CONFIG_GET.retry` ] ; then
						logger -p $LOG_LEVEL "PING Reboot. Rebooting router after $failure unsuccessful retries"		
						reboot
					else
						exit 0
					fi 
					;;
					*)
                        exit 1 
                        ;;
        esac
		
	elif [ `$CONFIG_GET.reboot` -eq 0 ] ; then
	
		ping -c 1 -W $TIMEOUT -s $PACKET_SIZE $HOST > /dev/null
		
		if [ $? -eq 1 ] ; then
			logger -p $LOG_LEVEL_DBG "PING Reboot. Keep-Alive function has not received echo from host"
		fi
	fi
fi
