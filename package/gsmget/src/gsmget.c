/* 
 * gsmget.c - minimal gsmd client for GSM/3G/4G
 * modem parameters retrieval
 * 
 * Copyright (c) 2013 Teltonika M2M Solutions
 *
 * Authors: Justinas Grauslis	(initial version)
 *
 * Revision 1.0 2013/01/06 justinas
 * Initial release
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <getopt.h>
#include <libgsmd/libgsmd.h>
#include <libgsmd/pin.h>
#include <libgsmd/misc.h>

#define RX_BUF_LEN		512
#define MAXOPT			32
/* timout in s */
#define REQ_TIMEOUT		 2
/* some error codes */
#define OPTION_ERR		-1
#define READ_ERR		-2
#define PACKET_ERR		-3
#define PARSE_ERR		-4
/* final output to cli */
char output_buf[RX_BUF_LEN];
/* data from gsmd */
char input_buf[RX_BUF_LEN];
/* interface to retrieve net info */
char ifname[RX_BUF_LEN / 16];
/* AT command to send from CLI*/
char atcmd[RX_BUF_LEN / 16];

typedef enum 
{
	IP,
	CONNSTATE,
	NETSTATE,
	IMEI,
	MODEL,
	MANUF,
	SERIAL,
	REVISION,
	IMSI,
	PINSTATE,
	SIMSTATE,
	SIGNAL,
	OPERATOR,
	OPERNUM,
	CONNTYPE,
	ICCID,
	BSENT,
	BRECV,
	ATCMD,
	
} rq_option;

/* supported modems */
typedef enum 
{
	HE910,
	GTM661,
	MC8700,
	EM820W,
	LP10,

} modem_dev;

/*
 * In case output buffer is empty we will print "n/a" to stdout.
 */
void trim_buffer_output(char *output_buf)
{
	if (output_buf[0] == '\0' || output_buf[0] == ' ')
		strcpy(output_buf, "n/a\n");
}

static void usage(void)
{
	fprintf(stderr, "\nNOTE: this utility defaults to Telit HE910-EUD if no modem specified.\n\n");
	fprintf(stderr, "usage: gsmget OPTIONS\n" );
	fprintf(stderr, "  -p, --ip <INTERFACE>      Get IP of logical interface\n");
	fprintf(stderr, "  -s, --connstate           Get 3G connection state\n");
	fprintf(stderr, "  -g, --netstate            Get network link state\n");
	fprintf(stderr, "  -i, --imei                Get device IMEI\n");
	fprintf(stderr, "  -m, --model               Get device model\n");
	fprintf(stderr, "  -w, --manuf               Get device manufacturer\n");
	fprintf(stderr, "  -a, --serial              Get device serial number\n");
	fprintf(stderr, "  -y, --revision            Get device revision number\n");
	fprintf(stderr, "  -x, --imsi                Get IMSI\n");
	fprintf(stderr, "  -z, --simstate            Get SIM card state\n");
	fprintf(stderr, "  -u, --pinstate            Get PIN state\n");
	fprintf(stderr, "  -l, --signal              Get gsm signal level\n");
	fprintf(stderr, "  -o, --operator            Get name of operator used\n");
	fprintf(stderr, "  -f, --opernum             Get operator number\n");
	fprintf(stderr, "  -t, --conntype            Get data carrier type\n");
	fprintf(stderr, "  -q, --iccid               Get SIM ICCID\n");
	fprintf(stderr, "  -e, --bsent <INTERFACE>   Get number of bytes sent\n");
	fprintf(stderr, "  -r, --brecv <INTERFACE>   Get number of bytes recieved\n");
	fprintf(stderr, "  -h, --help                Prints this information\n\n");
	fprintf(stderr, "compatibility options:\n");
	fprintf(stderr, "  -N, --he910               Support for Telit HE910-EUD\n");
	fprintf(stderr, "  -O, --gtm661              Support for Option GTM661W\n");
	fprintf(stderr, "  -P, --mc8700              Support for Sierra Wireless MC8700\n");
	fprintf(stderr, "  -R, --em820w              Support for Huawei EM820W\n");
	fprintf(stderr, "  -T, --lp10                Support for Broadmobi LP10\n\n");
	fprintf(stderr, "auxiliary options:\n");
	fprintf(stderr, "  -A, --at <COMMAND>        Send AT command to device\n");
}

void init_option_list(rq_option *op, int size)
{
	int i;

	for (i = 0; i < size; i++) 
		*(op+i) = -1;

	/* init buffers to empty string */
	output_buf[0] = '\0';
	input_buf[0] = '\0';
}

void soft_exit(struct lgsm_handle *gsmh, int code) 
{
	if (lgsm_exit(gsmh) != 0)
		fprintf(stderr, 
			"%s: dirty disconnect\n", 
			__FILE__);
	exit(code);
}

static void timeout_handler(int sig)
{
	fprintf(stderr, 
		"%s: request timeout\n", 
		__FILE__);
	exit(-1);
}

/* handler for GSMD_MSG_NETWORK family messages */
static int network_handler(struct lgsm_handle *lh, 
	struct gsmd_msg_hdr *gmh)
{
	struct gsmd_signal_quality *sq;
	enum gsmd_netreg_state state;

	switch (gmh->msg_subtype) 
	{
		case GSMD_NETWORK_SIGQ_GET:
			sq = (struct gsmd_signal_quality *)gmh->data;
			if (sq->rssi == 99)
				sprintf(&output_buf[strlen(output_buf)], 
					"unknown\n");
			else
				sprintf(&output_buf[strlen(output_buf)], 
					"%i\n", -113 + sq->rssi * 2);
			break;
		case GSMD_NETWORK_OPER_GET:
			sprintf(&output_buf[strlen(output_buf)], 
				"%s\n", (char *)gmh->data);
			break;
		case GSMD_NETWORK_OPER_N_GET:
			sprintf(&output_buf[strlen(output_buf)], 
				"%s\n", (char *)gmh->data);
			break;
		case GSMD_NETWORK_QUERY_REG:
			state = *(enum gsmd_netreg_state *)gmh->data;
			switch (state) {
				case GSMD_NETREG_UNREG:
					sprintf(&output_buf[strlen(output_buf)], 
						"unregistered\n");
					break;
				case GSMD_NETREG_REG_HOME:
					sprintf(&output_buf[strlen(output_buf)], 
						"registered (home network)\n");
					break;
				case GSMD_NETREG_UNREG_BUSY:
					sprintf(&output_buf[strlen(output_buf)], 
						"searching for network\n");
					break;
				case GSMD_NETREG_DENIED:
					printf(&output_buf[strlen(output_buf)], 
						"registration denied\n");
					break;
				case GSMD_NETREG_REG_ROAMING:
					sprintf(&output_buf[strlen(output_buf)], 
						"registered (roaming)\n");
					break;
				default:
					sprintf(&output_buf[strlen(output_buf)], 
						"unknown\n");
					break;
			}
			break;
		default:
			return -EINVAL;
	}
	return 0;
}

/* handler for GSMD_MSG_PHONE family messages */
static int phone_handler(struct lgsm_handle *lh, 
	struct gsmd_msg_hdr *gmh)
{
	switch (gmh->msg_subtype) 
	{
		case GSMD_PHONE_GET_MODEL:
			sprintf(&output_buf[strlen(output_buf)], 
				"%s\n", (char *)gmh->data);
			break;
		case GSMD_PHONE_GET_MANUF:
			sprintf(&output_buf[strlen(output_buf)], 
				"%s\n", (char *)gmh->data);
			break;
		case GSMD_PHONE_GET_SERIAL:
			sprintf(&output_buf[strlen(output_buf)], 
				"%s\n", (char *)gmh->data);
			break;
		case GSMD_PHONE_GET_REVISION:
			sprintf(&output_buf[strlen(output_buf)], 
				"%s\n", (char *)gmh->data);
			break;
		case GSMD_PHONE_GET_IMSI:
			sprintf(&output_buf[strlen(output_buf)], 
				"%s\n", (char *)gmh->data);
			break;
		default:
			return -EINVAL;
	}
	return 0;
}

/* handler for GSMD_MSG_PIN family messages */
static int pin_handler(struct lgsm_handle *lh, 
	struct gsmd_msg_hdr *gmh)
{
	switch (gmh->msg_subtype) { 
		case GSMD_PIN_GET_STATUS:
			sprintf(&output_buf[strlen(output_buf)], 
				"%s\n", lgsm_pin_name(*(int *)gmh->data));
			break;
		default:
			return -EINVAL;
	}
	return 0;
}

static int gsmget_get_serial(struct lgsm_handle *gsmh, 
	char *output, modem_dev dv)
{
	char rx_buf[RX_BUF_LEN];
	unsigned int rlen = RX_BUF_LEN;
	
	switch(dv)
	{
		case HE910:
		case GTM661:
		case MC8700:
		case EM820W:
		case LP10:
			if (lgsm_passthrough(gsmh, "AT+GSN", rx_buf, &rlen) <= 0)
				return READ_ERR;

			if(sscanf(rx_buf, "%s", output + strlen(output)) < 1)
				return PARSE_ERR;

			sprintf(output + strlen(output), "\n");
			break;
			
		default:
			sprintf(output + strlen(output), 
				"not supported\n");
			break;
	}
	return 0;
}

static int gsmget_get_imsi(struct lgsm_handle *gsmh, 
	char *output, modem_dev dv)
{
	char rx_buf[RX_BUF_LEN];
	unsigned int rlen = RX_BUF_LEN;
	
	switch(dv)
	{
		case HE910:
		case GTM661:
		case MC8700:
		case EM820W:
		case LP10:
			if (lgsm_passthrough(gsmh, "AT+CIMI", rx_buf, &rlen) <= 0)
				return READ_ERR;
			
			if (strncmp(rx_buf, "+CME ERROR: 10", RX_BUF_LEN-1) == 0)
				sprintf(output + strlen(output), "unknown\n");
			else
				sprintf(output + strlen(output), "%s\n", rx_buf);
			break;
			
		default:
			sprintf(output + strlen(output), 
				"not supported\n");
			break;
	}
	return 0;
}

static int gsmget_get_iccid(struct lgsm_handle *gsmh, 
	char *output, modem_dev dv)
{
	char rx_buf[RX_BUF_LEN];
	char parsed[RX_BUF_LEN];
	unsigned int rlen = RX_BUF_LEN;
	
	switch(dv)
	{
		case HE910:
			if (lgsm_passthrough(gsmh, "AT#CCID", rx_buf, &rlen) <= 0)
				return READ_ERR;
			
			if (strncmp(rx_buf, "+CME ERROR: 10", RX_BUF_LEN-1) == 0)
				sprintf(output + strlen(output), "unknown\n");
			else
				if(sscanf(rx_buf, "#CCID: %s", parsed) < 1)
					return PARSE_ERR;
				sprintf(output + strlen(output), "%s\n", parsed);
			break;
		
		case GTM661:
		case MC8700:
		case EM820W:
		case LP10:
			sprintf(output + strlen(output), 
				"not supported\n");
			break;
			
		default:
			sprintf(output + strlen(output), 
				"not supported\n");
			break;
	}
	return 0;
}

static int gsmget_get_imei(struct lgsm_handle *gsmh, 
	char *output, modem_dev dv)
{
	char rx_buf[RX_BUF_LEN];
	unsigned int rlen = RX_BUF_LEN;
	
	switch(dv)
	{
		case HE910:
		case MC8700:
		case EM820W:
		case LP10:
			if (lgsm_passthrough(gsmh, "AT+CGSN", rx_buf, &rlen) <= 0)
				return READ_ERR;
	
			if(sscanf(rx_buf, "%s", output + strlen(output)) < 1)
				return PARSE_ERR;
	
			sprintf(output + strlen(output), "\n");
			break;
			

		case GTM661:
			if (lgsm_passthrough(gsmh, "AT+CGSN", rx_buf, &rlen) <= 0)
				return READ_ERR;
	
			rx_buf[15] = '\0';
	
			if(sscanf(rx_buf, "%s", output + strlen(output)) < 1)
				return PARSE_ERR;
	
			sprintf(output + strlen(output), "\n");
			break;
			
		default:
			sprintf(output + strlen(output), 
				"not supported\n");
			break;
	}
	return 0;
}

static int gsmget_get_simstate(struct lgsm_handle *gsmh, 
	char *output, modem_dev dv)
{
	char rx_buf[RX_BUF_LEN];
	unsigned int rlen = RX_BUF_LEN, simstate;
	int p = 0;
	
	switch(dv)
	{
		case HE910:
			if (lgsm_passthrough(gsmh, "AT#QSS?", rx_buf, &rlen) <= 0)
				return READ_ERR;
		
			if(sscanf(rx_buf, "#QSS: %*[^,],%d", &simstate) < 1)
				return PARSE_ERR;

			switch (simstate)
			{
				case 0:
					sprintf(output + strlen(output), 
						"not inserted\n");
					break;
				case 1:
					sprintf(output + strlen(output), 
						"inserted\n");
					break;
				default:
					sprintf(output + strlen(output), 
						"unknown\n");
			}
			break;
			
		case GTM661:
		case MC8700:
		case LP10:
			if (lgsm_passthrough(gsmh, "AT+CPIN?", rx_buf, &rlen) <= 0)
				return READ_ERR;
		
			if ( !strncmp( rx_buf, "+CPIN: READY", RX_BUF_LEN ) ) {
				simstate = 1;
			} else if ( !strncmp( rx_buf, "+CME ERROR: 10", RX_BUF_LEN ) ) {
				simstate = 0;
			} else {
				simstate = -1;
			}
			
			switch (simstate)
			{
				case 0:
					sprintf(output + strlen(output), 
						"not inserted\n");
					break;
				case 1:
					sprintf(output + strlen(output), 
						"inserted\n");
					break;
				default:
					sprintf(output + strlen(output), 
						"unknown\n");
			}
			break;
		
		case EM820W:
			if (lgsm_passthrough(gsmh, "AT+CPIN?", rx_buf, &rlen) <= 0)
				return READ_ERR;
		
			if (!strncmp( rx_buf, "+CME ERROR: 10", RX_BUF_LEN ))
				sprintf(output + strlen(output), 
						"not inserted\n");
			else if (!strncmp( rx_buf, "ERROR", RX_BUF_LEN ))
				sprintf(output + strlen(output), 
						"not inserted\n");
			else 
				sprintf(output + strlen(output), 
						"inserted\n");
			break;
		
		default:
			sprintf(output + strlen(output), 
				"not supported\n");
			break;
	}
	return 0;
}

static int gsmget_get_ip(char *output)
{
	FILE *fp;
	char ip[RX_BUF_LEN], cmd[RX_BUF_LEN];
	
	cmd[0] = '\0';
	strcat(cmd, "ifconfig ");
	strcat(cmd, ifname);
	strcat(cmd,  " 2>/dev/null | \
	grep \"inet addr\" |\
	awk '{ print $2 }' |\
	awk 'BEGIN { FS=\":\" } { print $2 }'");
	
	if ((fp = popen(cmd, "r")) == NULL)
		return READ_ERR;
	
	if (fgets(ip, sizeof(ip), fp) == NULL)
		return READ_ERR;
	
	pclose(fp);
	sprintf(output + strlen(output), "%s", ip);
	return 0;
}

static int gsmget_get_bsent(char *output)
{
	FILE *fp;
	char bsent[RX_BUF_LEN], cmd[RX_BUF_LEN];
	
	cmd[0] = '\0';
	strcat(cmd, "cat /sys/class/net/");
	strcat(cmd, ifname);
	strcat(cmd,  "/statistics/tx_bytes 2>/dev/null");
	
	if ((fp = popen(cmd, "r")) == NULL)
		return READ_ERR;
	
	if (fgets(bsent, sizeof(bsent), fp) == NULL)
		return READ_ERR;
	
	pclose(fp);
	sprintf(output + strlen(output), "%s", bsent);
	return 0;
}

static int gsmget_get_brecv(char *output)
{
	FILE *fp;
	char brecv[RX_BUF_LEN], cmd[RX_BUF_LEN];
	
	cmd[0] = '\0';
	strcat(cmd, "cat /sys/class/net/");
	strcat(cmd, ifname);
	strcat(cmd,  "/statistics/rx_bytes 2>/dev/null");
	
	if ((fp = popen(cmd, "r")) == NULL)
		return READ_ERR;
	
	if (fgets(brecv, sizeof(brecv), fp) == NULL)
		return READ_ERR;
	
	pclose(fp);
	sprintf(output + strlen(output), "%s", brecv);
	return 0;
}

/* generic function to get connection state.
 * Not very trustful though */
static int is_connected()
{
	FILE *fp;
	char brecv[RX_BUF_LEN], cmd[RX_BUF_LEN];
	
	cmd[0] = '\0';
	strcat(cmd, "cat /sys/class/net/");
	/* FIXME: hardcoded iface-name */
	strcat(cmd, "3g-ppp");
	strcat(cmd,  "/statistics/rx_bytes 2>/dev/null");
	
	if ((fp = popen(cmd, "r")) == NULL)
		return 0;
	
	if (fgets(brecv, sizeof(brecv), fp) == NULL)
		return 0;

	pclose(fp);
	return 1;
}

static int gsmget_get_conntype(struct lgsm_handle *gsmh, 
	char *output, modem_dev dv)
{
	char rx_buf[RX_BUF_LEN], *result, conntype_str[10];
	unsigned int rlen = RX_BUF_LEN, conntype;
	int reg_state, count = 0;
	
	if (!is_connected()) {
		sprintf(output + strlen(output), 
				"\n");
		return 0;
	}
	
	switch(dv)
	{
		case HE910:
			if (lgsm_passthrough(gsmh, "AT#PSNT?", rx_buf, &rlen) <= 0)
				return READ_ERR;

			if(sscanf(rx_buf, "#PSNT: %*[^,],%d", &conntype) < 1)
				return PARSE_ERR;

			switch (conntype)
			{
				case 0:
					sprintf(output + strlen(output), 
						"GPRS\n");
					break;
				case 1:
					sprintf(output + strlen(output), 
						"EDGE\n");
					break;
				case 2:
					sprintf(output + strlen(output), 
						"WCDMA\n");
					break;
				case 3:
					sprintf(output + strlen(output), 
						"HSDPA\n");
					break;
				case 4:
					sprintf(output + strlen(output), 
						"unknown or not registered\n");
					break;
				default:
					sprintf(output + strlen(output), 
						"unknown\n");
			}
			break;
			
		case EM820W:
			if (lgsm_passthrough(gsmh, "AT^SYSINFO", rx_buf, &rlen) <= 0)
				return READ_ERR;
			
			result = NULL;
			result = strtok(rx_buf, ",");
			
			while (result != NULL) {
				if (count == 5)
					sscanf(result, "%d", &conntype);
				result = strtok(NULL, ",");
				count++;
			}
			
			switch (conntype)
			{
				case 0:
					sprintf(output + strlen(output), 
						"\n");
					break;
				case 1:
					sprintf(output + strlen(output), 
						"GSM\n");
					break;
				case 2:
					sprintf(output + strlen(output), 
						"GPRS\n");
					break;
				case 3:
					sprintf(output + strlen(output), 
						"EDGE\n");
					break;
				case 4:
					sprintf(output + strlen(output), 
						"WCDMA\n");
					break;
				case 5:
					sprintf(output + strlen(output), 
						"HSDPA\n");
					break;
				case 6:
					sprintf(output + strlen(output), 
						"HSUPA\n");
					break;
				case 7:
					sprintf(output + strlen(output), 
						"HSDPA+HSUPA\n");
					break;
				case 8:
					sprintf(output + strlen(output), 
						"TD_SCDMA\n");
					break;
				case 9:
					sprintf(output + strlen(output), 
						"HSPA+\n");
					break;
				case 17:
					sprintf(output + strlen(output), 
						"HSPA+ (64QAM)\n");
					break;
				case 18:
					sprintf(output + strlen(output), 
						"HSPA+ (MIMO)\n");
					break;
				default:
					sprintf(output + strlen(output), 
						"unknown\n");
			}
			break;
				
		case MC8700:
			if (lgsm_passthrough(gsmh, "AT+CREG=1", rx_buf, &rlen) <= 0)
				return READ_ERR;
			
			rlen = RX_BUF_LEN;
			
			if (lgsm_passthrough(gsmh, "AT+CREG?", rx_buf, &rlen) <= 0)
				return READ_ERR;

			if(sscanf(rx_buf, "+CREG: %*[^,],%d", &reg_state) < 1)
				return PARSE_ERR;

			switch (reg_state)
			{
				case 1:
					rx_buf[0] = '\0';
					rlen = RX_BUF_LEN;
					if (lgsm_passthrough(gsmh, "AT*CNTI=0", rx_buf, &rlen) <= 0)
						return READ_ERR;
					
					char *ptr = strstr(rx_buf, "0,");
					
					if(sscanf(ptr, "%*[^,],%s", output + strlen(output)) < 1)
						return PARSE_ERR;
					
					sprintf(output + strlen(output), "\n");
					break;

				default:
					sprintf(output + strlen(output), 
						"\n");
					break;
			}
			break;
			
		case LP10:
			if (lgsm_passthrough(gsmh, "AT+BMRAT", rx_buf, &rlen) <= 0)
				return READ_ERR;
			
			char *ptr = strstr(rx_buf, "+BMRAT:");

			if (sscanf(ptr, "+BMRAT: %[^\n^\r]", conntype_str) < 1)
				return PARSE_ERR;

			sprintf(output + strlen(output), "%s\n",conntype_str);
			break;
	}
	return 0;
}

static int gsmget_get_connstate(struct lgsm_handle *gsmh, 
	char *output, modem_dev dv)
{
	FILE *fp;
	char rx_buf[RX_BUF_LEN];
	unsigned int rlen = RX_BUF_LEN, connstate;
	char brecv[RX_BUF_LEN], cmd[RX_BUF_LEN];
		
	switch(dv)
	{
		case HE910:
		case MC8700:
		case EM820W:
		case LP10:
			cmd[0] = '\0';
			strcat(cmd, "cat /sys/class/net/");
			/* FIXME: hardcoded iface-name */
			strcat(cmd, "3g-ppp");
			strcat(cmd,  "/statistics/rx_bytes 2>/dev/null");
			
			if ((fp = popen(cmd, "r")) == NULL) {
				sprintf(output + strlen(output), 
				"disconnected\n");
				break;
			}
			
			if (fgets(brecv, sizeof(brecv), fp) == NULL) {
				sprintf(output + strlen(output), 
				"disconnected\n");
				break;
			}
			pclose(fp);
			
			sprintf(output + strlen(output), 
				"connected\n");
			break;
				
		default:
			sprintf(output + strlen(output), 
				"not supported\n");
			break;
	}
	return 0;
}

static int gsmget_get_regstate(struct lgsm_handle *gsmh, 
	char *output, modem_dev dv)
{
	char rx_buf[RX_BUF_LEN];
	unsigned int rlen = RX_BUF_LEN, reg_state;
	
	switch(dv)
	{
		case HE910:
		case GTM661:
		case MC8700:
		case EM820W:
		case LP10:
			if (lgsm_passthrough(gsmh, "AT+CREG=1", rx_buf, &rlen) <= 0)
				return READ_ERR;
			
			rlen = RX_BUF_LEN;
			
			if (lgsm_passthrough(gsmh, "AT+CREG?", rx_buf, &rlen) <= 0)
				return READ_ERR;

			if(sscanf(rx_buf, "+CREG: %*[^,],%d", &reg_state) < 1)
				return PARSE_ERR;

			switch (reg_state)
			{
				case 0:
					sprintf(output + strlen(output), 
						"unregistered\n");
					break;
				case 1:
					sprintf(output + strlen(output), 
						"registered (home)\n");
					break;
				case 2:
					sprintf(output + strlen(output), 
						"searching\n");
					break;
				case 3:
					sprintf(output + strlen(output), 
						"denied\n");
					break;
				case 4:
					sprintf(output + strlen(output), 
						"unknown\n");
					break;
				case 5:
					sprintf(output + strlen(output), 
						"registered (roaming)\n");
					break;
				default:
					sprintf(output + strlen(output), 
						"unknown\n");
			}
			break;
			
		default:
			sprintf(output + strlen(output), 
				"not supported\n");
			break;
	}
	return 0;
}

static int gsmget_send_atcmd(struct lgsm_handle *gsmh, 
	char *output, char *cmd, modem_dev dv)
{
	char rx_buf[RX_BUF_LEN];
	unsigned int rlen = RX_BUF_LEN;
	
	switch(dv)
	{
		case HE910:
		case MC8700:
		case GTM661:
		case EM820W:
		case LP10:
			if (lgsm_passthrough(gsmh, cmd, rx_buf, &rlen) <= 0)
				return READ_ERR;
	
			sprintf(output + strlen(output), "%s\n", rx_buf);
			break;
			
		default:
			sprintf(output + strlen(output), 
				"device not supported\n");
			break;
	}
	return 0;
}

int process_single(struct lgsm_handle *gsmh, int gsmfd, 
	rq_option op, char *recv_buf, modem_dev dev)
{
	int rc = 0;

	switch (op)
	{
		case IP:
			return gsmget_get_ip(output_buf);
		case CONNSTATE:
			return gsmget_get_connstate(gsmh, output_buf, dev);
		case NETSTATE:
			return gsmget_get_regstate(gsmh, output_buf, dev);
		case IMEI:
			return gsmget_get_imei(gsmh, output_buf, dev);
		case MODEL:
			lgsm_get_model(gsmh);
			break;
		case MANUF:
			lgsm_get_manufacturer(gsmh);
			break;
		case SERIAL:
			return gsmget_get_serial(gsmh, output_buf, dev);
			break;
		case REVISION:
			lgsm_get_revision(gsmh);
			break;
		case IMSI:
			return gsmget_get_imsi(gsmh, output_buf, dev);
		case PINSTATE:
			lgsm_pin_status(gsmh);
			break;
		case SIMSTATE:
			return gsmget_get_simstate(gsmh, output_buf, dev);
		case SIGNAL:
			lgsm_signal_quality(gsmh);
			break;
		case OPERATOR:
			lgsm_oper_get(gsmh);
			break;
		case OPERNUM:
			lgsm_oper_n_get(gsmh);
			break;
		case ICCID:
			return gsmget_get_iccid(gsmh, output_buf, dev);
			break;
		case CONNTYPE:
			return gsmget_get_conntype(gsmh, output_buf, dev);
		case BSENT:
			return gsmget_get_bsent(output_buf);
		case BRECV:
			return gsmget_get_brecv(output_buf);
		case ATCMD:
			return gsmget_send_atcmd(gsmh, output_buf, atcmd, dev);
		default:
			return OPTION_ERR;
	}

	if ((rc = read(gsmfd, recv_buf, 
			RX_BUF_LEN)) <= 0)
		return READ_ERR;

	if (lgsm_handle_packet(gsmh, 
			recv_buf, rc) < 0)
		return PACKET_ERR;

	return 0;
}

int main(int argc, char **argv)
{
	int i, c, ret;
	int option_index = 0, op_index = 0, gsm_fd = 0;
	rq_option option_list[MAXOPT];
	modem_dev device = HE910;
	struct lgsm_handle *lgsmh = NULL;
	
	/* cli options */
	static struct option long_options[] =
	{
		{"ip",			required_argument,	0,	'p'},
		{"connstate",	no_argument,		0,	's'},
		{"netstate",	no_argument,		0,	'g'},
		{"imei",		no_argument,		0,	'i'},
		{"model",		no_argument,		0,	'm'},
		{"manuf",		no_argument,		0,	'w'},
		{"serial",		no_argument,		0,	'a'},
		{"revision",	no_argument,		0,	'y'},
		{"imsi",		no_argument,		0,	'x'},
		{"simstate",	no_argument,		0,	'z'},
		{"pinstate",	no_argument,		0,	'u'},
		{"signal",  	no_argument,		0,	'l'},
		{"operator",	no_argument,		0,	'o'},
		{"opernum",		no_argument,		0,	'f'},
		{"conntype",	no_argument,		0,	't'},
		{"iccid",		no_argument,		0,	'q'},
		{"bsent",		required_argument,	0,	'e'},
		{"brecv",		required_argument,	0,	'r'},
		{"help",		no_argument,		0,	'h'},
		{"he910",		no_argument,		0,	'N'},
		{"gtm661",		no_argument,		0,	'O'},
		{"mc8700",		no_argument,		0,	'P'},
		{"em820w",		no_argument,		0,	'R'},
		{"lp10",		no_argument,		0,	'T'},
		{"at",			required_argument,	0,	'A'},
	};

	/* init option list to empty */
	init_option_list(option_list, MAXOPT);
	
	/* parse cli options */
	while (1)
	{
		if ((c = getopt_long_only(argc, argv, 
				"p:sgimwayxzuloftqe:r:hNOPRTA:", 
				long_options, &option_index)) == -1)
			break;
     
		switch (c)
		{
			case 'p':
				option_list[op_index] = IP;
				op_index++;
				/* handle argument */
				if (optarg == NULL) {
					usage();
					exit(0);
				} else {
					strncpy(ifname, optarg, RX_BUF_LEN / 16);
				}
				break;
			case 's':
				option_list[op_index] = CONNSTATE;
				op_index++;
				break;
			case 'g':
				option_list[op_index] = NETSTATE;
				op_index++;
				break;
			case 'i':
				option_list[op_index] = IMEI;
				op_index++;;
				break;
			case 'm':
				option_list[op_index] = MODEL;
				op_index++;
				break;
			case 'w':
				option_list[op_index] = MANUF;
				op_index++;
				break;
			case 'a':
				option_list[op_index] = SERIAL;
				op_index++;
				break;
			case 'y':
				option_list[op_index] = REVISION;
				op_index++;
				break;
			case 'x':
				option_list[op_index] = IMSI;
				op_index++;
				break;
			case 'z':
				option_list[op_index] = SIMSTATE;
				op_index++;
				break;
			case 'u':
				option_list[op_index] = PINSTATE;
				op_index++;
				break;	
			case 'l':
				option_list[op_index] = SIGNAL;
				op_index++;
				break;	
			case 'o':
				option_list[op_index] = OPERATOR;
				op_index++;
				break;
			case 'f':
				option_list[op_index] = OPERNUM;
				op_index++;
				break;	
			case 't':
				option_list[op_index] = CONNTYPE;
				op_index++;
				break;
			case 'q':
				option_list[op_index] = ICCID;
				op_index++;
				break;
			case 'e':
				option_list[op_index] = BSENT;
				op_index++;
				/* handle argument */
				if (optarg == NULL) {
					usage();
					exit(0);
				} else {
					strncpy(ifname, optarg, RX_BUF_LEN / 16);
				}
				break;	
			case 'r':
				option_list[op_index] = BRECV;
				op_index++;
				/* handle argument */
				if (optarg == NULL) {
					usage();
					exit(0);
				} else {
					strncpy(ifname, optarg, RX_BUF_LEN / 16);
				}
				break;
			/* options for compatibility */
			case 'N':
				device = HE910;
				break;
			case 'O':
				device = GTM661;
				break;
			case 'P':
				device = MC8700;
				break;
			case 'R':
				device = EM820W;
				break;
			case 'T':
				device = LP10;
				break;
			/* auxiliary options */
			case 'A':
				option_list[op_index] = ATCMD;
				op_index++;
				/* handle argument */
				if (optarg == NULL) {
					usage();
					exit(0);
				} else {
					strncpy(atcmd, optarg, RX_BUF_LEN / 16);
				}
				break;	
			case 'h':
				usage();
				exit(0);
			default:
				usage();
				exit(0);
		}
	}
	
	/* handle rest of arguments */
	if (optind < argc || argc == 1) {
		usage();

		exit(0);
	} 
	
	/* connect to gsmd daemon */
	if ((lgsmh = lgsm_init(LGSMD_DEVICE_GSMD)) == NULL) {
		fprintf(stderr, 
			"%s: i can't connect to gsmd\n", 
			__FILE__);
		exit(-1);
	}
	
	/* get file descriptor for communication */
   	if ((gsm_fd = lgsm_fd(lgsmh)) <= 0) {
		fprintf(stderr, 
			"%s: i can't get file descriptor\n", 
			__FILE__);
		soft_exit(lgsmh, -1);
	}

	/* register handler for "GSMD_MSG_PHONE" type */
	if (lgsm_register_handler(lgsmh, GSMD_MSG_PHONE, 
			phone_handler) != 0) {
		fprintf(stderr, 
			"%s: i can't register GSMD_MSG_PHONE type\n", 
			__FILE__);
		soft_exit(lgsmh, -1);
	}

	/* register handler for "GSMD_MSG_PIN" type */
	if (lgsm_register_handler(lgsmh, GSMD_MSG_PIN, 
			pin_handler) != 0) {
		fprintf(stderr, 
			"%s: i can't register GSMD_MSG_PIN type\n", 
			__FILE__);
		soft_exit(lgsmh, -1);
	}

	/* register handler for "GSMD_MSG_NETWORK" type */
	if (lgsm_register_handler(lgsmh, GSMD_MSG_NETWORK, 
			network_handler) != 0) {
		fprintf(stderr, 
			"%s: i can't register GSMD_MSG_NETWORK type\n", 
			__FILE__);
		soft_exit(lgsmh, -1);
	}

	/* register timeout handler */
	if (signal(SIGALRM, timeout_handler) == SIG_ERR) {
		fprintf(stderr, 
			"%s: i got no answer, leaving\n", 
			__FILE__);
		soft_exit(lgsmh, -1);
	}

	alarm(REQ_TIMEOUT);

	/* process user requested options by one */
	for (i = 0; i < MAXOPT; i++) {
		if (option_list[i] != -1) {
			ret = process_single(lgsmh, gsm_fd, 
					option_list[i], input_buf, device);
			if (ret != 0) {
				switch (ret)
				{
					case OPTION_ERR:
						fprintf(stderr, 
							"%s: unsupported option\n", 
							__FILE__);
						break;
					case READ_ERR:
						fprintf(stderr, 
							"%s: error reading message\n", 
							__FILE__);
						break;
					case PACKET_ERR:
						fprintf(stderr, 
							"%s: error handling packet\n", 
							__FILE__);
						break;
					case PARSE_ERR:
						fprintf(stderr, 
							"%s: error parsing output\n",
							__FILE__);
						break;
					default:
						fprintf(stderr, 
							"%s: unknown process error\n", 
							__FILE__);
				}
				break;
			}
		} else {
			continue;
		}
	}

	/* output results */
	trim_buffer_output(output_buf);
	fprintf(stdout, "%s", output_buf);
	
	/* unregister type handlers */
	lgsm_unregister_handler(lgsmh, GSMD_MSG_NETWORK);
	lgsm_unregister_handler(lgsmh, GSMD_MSG_PIN);
	lgsm_unregister_handler(lgsmh, GSMD_MSG_PHONE);
	
	/* disconnect from daemon */
	if (lgsm_exit(lgsmh) != 0)
		fprintf(stderr, 
			"%s: dirty disconnect\n", 
			__FILE__);

	/* success */
	return 0;
}
