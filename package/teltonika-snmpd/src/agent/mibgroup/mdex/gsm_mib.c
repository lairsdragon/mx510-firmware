/*
 *  GSM MIB group implementation - gsm.c
 *
 */

#include <net-snmp/net-snmp-config.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <ctype.h>
#include <net-snmp/net-snmp-includes.h>
#include <net-snmp/agent/net-snmp-agent-includes.h>

#include "util_funcs.h"
#include "gsm_mib.h"
#include "struct.h"
//#include "uci.h"

#define GSM_STRING_LEN	256

char uci_search_result[GSM_STRING_LEN];

int header_gsm(struct variable *, oid *, size_t *, int,
                              size_t *, WriteMethod **);

/*
 * define the structure we're going to ask the agent to register our
 * information at 
 */
struct variable1 gsm_variables[] = {
    {RADIO_MODE,	ASN_OCTET_STR, RONLY, var_gsm, 1, {1}},
    {REG_MODE,		ASN_OCTET_STR, RONLY, var_gsm, 1, {2}},
    {MCC,			ASN_OCTET_STR, RONLY, var_gsm, 1, {3}},
    {MNC, 			ASN_OCTET_STR, RONLY, var_gsm, 1, {4}},
    {CELL_ID, 		ASN_OCTET_STR, RONLY, var_gsm, 1, {5}},
    {LAC, 			ASN_OCTET_STR, RONLY, var_gsm, 1, {6}},
    {NET_TECH, 		ASN_OCTET_STR, RONLY, var_gsm, 1, {7}},
    {ARFCN, 		ASN_OCTET_STR, RONLY, var_gsm, 1, {8}},
    {UARFCN, 		ASN_OCTET_STR, RONLY, var_gsm, 1, {9}},
    {RSSI, 			ASN_OCTET_STR, RONLY, var_gsm, 1, {10}},
    {RSCP, 			ASN_OCTET_STR, RONLY, var_gsm, 1, {11}},
    {ECIO, 			ASN_OCTET_STR, RONLY, var_gsm, 1, {12}},
    {IMEI, 			ASN_OCTET_STR, RONLY, var_gsm, 1, {13}},
    {IMSI, 			ASN_OCTET_STR, RONLY, var_gsm, 1, {14}},
    {ICCID,			ASN_OCTET_STR, RONLY, var_gsm, 1, {15}},
    {SPN,			ASN_OCTET_STR, RONLY, var_gsm, 1, {16}},
    {LAN_MAC, 		ASN_OCTET_STR, RONLY, var_gsm, 1, {17}},
    {WIFI_MAC, 		ASN_OCTET_STR, RONLY, var_gsm, 1, {18}},
    {RT_MODEL, 		ASN_OCTET_STR, RONLY, var_gsm, 1, {19}},
    {RT_TYPE, 		ASN_OCTET_STR, RONLY, var_gsm, 1, {20}},
    {RT_SERIAL, 	ASN_OCTET_STR, RONLY, var_gsm, 1, {21}},
    {RT_FWVER, 		ASN_OCTET_STR, RONLY, var_gsm, 1, {22}},
    {MD_MODEL, 		ASN_OCTET_STR, RONLY, var_gsm, 1, {23}},
    {MD_TYPE, 		ASN_OCTET_STR, RONLY, var_gsm, 1, {24}},
    {MD_SERIAL, 	ASN_OCTET_STR, RONLY, var_gsm, 1, {25}},
    {MD_FWVER, 		ASN_OCTET_STR, RONLY, var_gsm, 1, {26}},
    {MD_TEMP, 		ASN_OCTET_STR, RONLY, var_gsm, 1, {27}}
// 	{CI0,          ASN_OCTET_STR, RONLY, var_gsm, 1, {28}},
// 	{CI1,          ASN_OCTET_STR, RONLY, var_gsm, 1, {29}},
// 	{CI2,          ASN_OCTET_STR, RONLY, var_gsm, 1, {30}},
// 	{CI3,          ASN_OCTET_STR, RONLY, var_gsm, 1, {31}},
// 	{CI4,          ASN_OCTET_STR, RONLY, var_gsm, 1, {32}},
// 	{CI5,          ASN_OCTET_STR, RONLY, var_gsm, 1, {33}},
// 	{CI6,          ASN_OCTET_STR, RONLY, var_gsm, 1, {34}}
};

/*
 * Define the OID pointer to the mdex proprietrary oid
 */
oid gsm_variables_oid[] = { 1, 3, 6, 1, 4, 1, 15398, 10, 1 };

/*
 * Wrapper function to retrieve gsm objects
 * 
 */
static int getparam(char *param, char *buffer)
{
 char cmd[GSM_STRING_LEN];
 FILE* name = NULL;
 int iLength = 0;
 
 sprintf(cmd, "snmpget %s 2>/dev/null", param);
 
 if ((name = popen(cmd, "r")) == NULL)
    return -1;
 
 if (fgets(buffer, GSM_STRING_LEN, name) == NULL) {
	pclose(name);
	return -1;
 }
 
 iLength = strlen(buffer);
 // trim 'new line' symbol at the end
 if (buffer[iLength-1] == '\n')
	buffer[iLength-1] = '\0';
 
 pclose(name);
 return 0;
};

void
init_gsm_mib(void)
{
	/* We might initialize our vars here */

    /*
     * register ourselves with the agent to handle our mib tree 
     */
    REGISTER_MIB("mdex", gsm_variables, variable1,
                 gsm_variables_oid);
}

u_char*
var_gsm(struct variable *vp,
           oid *name,
           size_t *length,
           int exact, size_t *var_len, WriteMethod **write_method)
{
    if (header_generic(vp, name, length, exact, var_len, write_method) ==
        MATCH_FAILED)
        return NULL;

    switch (vp->magic) {

    case RADIO_MODE:
		if (getparam("--radmode", uci_search_result) != 0)
			strncpy(uci_search_result, "unknown", sizeof("unknown"));
        *var_len = strlen(uci_search_result);
        *write_method = 0;
        return (u_char *) uci_search_result;
		
	case REG_MODE:
		if (getparam("--regmode", uci_search_result) != 0)
			strncpy(uci_search_result, "unknown", sizeof("unknown"));
        *var_len = strlen(uci_search_result);
        *write_method = 0;
        return (u_char *) uci_search_result;
		
	case MCC:
		if (getparam("--mcc", uci_search_result) != 0)
			strncpy(uci_search_result, "unknown", sizeof("unknown"));
        *var_len = strlen(uci_search_result);
        *write_method = 0;
        return (u_char *) uci_search_result;
		
	case MNC:
		if (getparam("--mnc", uci_search_result) != 0)
			strncpy(uci_search_result, "unknown", sizeof("unknown"));
        *var_len = strlen(uci_search_result);
        *write_method = 0;
        return (u_char *) uci_search_result;
		
	case CELL_ID:
		if (getparam("--cid", uci_search_result) != 0)
			strncpy(uci_search_result, "unknown", sizeof("unknown"));
        *var_len = strlen(uci_search_result);
        *write_method = 0;
        return (u_char *) uci_search_result;
		
	case LAC:
		if (getparam("--lac", uci_search_result) != 0)
			strncpy(uci_search_result, "unknown", sizeof("unknown"));
        *var_len = strlen(uci_search_result);
        *write_method = 0;
        return (u_char *) uci_search_result;
	
	case NET_TECH:
		if (getparam("--nettech", uci_search_result) != 0)
			strncpy(uci_search_result, "unknown", sizeof("unknown"));
        *var_len = strlen(uci_search_result);
        *write_method = 0;
        return (u_char *) uci_search_result;
		
	case ARFCN:
		if (getparam("--arfcn", uci_search_result) != 0)
			strncpy(uci_search_result, "unknown", sizeof("unknown"));
        *var_len = strlen(uci_search_result);
        *write_method = 0;
        return (u_char *) uci_search_result;
		
	case UARFCN:
		if (getparam("--uarfcn", uci_search_result) != 0)
			strncpy(uci_search_result, "unknown", sizeof("unknown"));
        *var_len = strlen(uci_search_result);
        *write_method = 0;
        return (u_char *) uci_search_result;
		
	case RSSI:
		if (getparam("--rssi", uci_search_result) != 0)
			strncpy(uci_search_result, "unknown", sizeof("unknown"));
        *var_len = strlen(uci_search_result);
        *write_method = 0;
        return (u_char *) uci_search_result;
		
	case RSCP:
		if (getparam("--rscp", uci_search_result) != 0)
			strncpy(uci_search_result, "unknown", sizeof("unknown"));
        *var_len = strlen(uci_search_result);
        *write_method = 0;
        return (u_char *) uci_search_result;
		
	case ECIO:
		if (getparam("--ecio", uci_search_result) != 0)
			strncpy(uci_search_result, "unknown", sizeof("unknown"));
        *var_len = strlen(uci_search_result);
        *write_method = 0;
        return (u_char *) uci_search_result;
		
	case IMEI:
		if (getparam("--imei", uci_search_result) != 0)
			strncpy(uci_search_result, "unknown", sizeof("unknown"));
        *var_len = strlen(uci_search_result);
        *write_method = 0;
        return (u_char *) uci_search_result;
		
	case IMSI:
		if (getparam("--imsi", uci_search_result) != 0)
			strncpy(uci_search_result, "unknown", sizeof("unknown"));
        *var_len = strlen(uci_search_result);
        *write_method = 0;
        return (u_char *) uci_search_result;
		
	case ICCID:
		if (getparam("--iccid", uci_search_result) != 0)
			strncpy(uci_search_result, "unknown", sizeof("unknown"));
        *var_len = strlen(uci_search_result);
        *write_method = 0;
        return (u_char *) uci_search_result;
		
	case SPN:
		if (getparam("--spn", uci_search_result) != 0)
			strncpy(uci_search_result, "unknown", sizeof("unknown"));
        *var_len = strlen(uci_search_result);
        *write_method = 0;
        return (u_char *) uci_search_result;
		
	case LAN_MAC:
		if (getparam("--maclan", uci_search_result) != 0)
			strncpy(uci_search_result, "unknown", sizeof("unknown"));
        *var_len = strlen(uci_search_result);
        *write_method = 0;
        return (u_char *) uci_search_result;
		
	case WIFI_MAC:
		if (getparam("--macwifi", uci_search_result) != 0)
			strncpy(uci_search_result, "unknown", sizeof("unknown"));
        *var_len = strlen(uci_search_result);
        *write_method = 0;
        return (u_char *) uci_search_result;
		
	case RT_MODEL:
		if (getparam("--rtmodel", uci_search_result) != 0)
			strncpy(uci_search_result, "unknown", sizeof("unknown"));
        *var_len = strlen(uci_search_result);
        *write_method = 0;
        return (u_char *) uci_search_result;
		
	case RT_TYPE:
		if (getparam("--rttype", uci_search_result) != 0)
			strncpy(uci_search_result, "unknown", sizeof("unknown"));
        *var_len = strlen(uci_search_result);
        *write_method = 0;
        return (u_char *) uci_search_result;
		
	case RT_SERIAL:
		if (getparam("--rtserial", uci_search_result) != 0)
			strncpy(uci_search_result, "unknown", sizeof("unknown"));
        *var_len = strlen(uci_search_result);
        *write_method = 0;
        return (u_char *) uci_search_result;
		
	case RT_FWVER:
		if (getparam("--rtfwver", uci_search_result) != 0)
			strncpy(uci_search_result, "unknown", sizeof("unknown"));
        *var_len = strlen(uci_search_result);
        *write_method = 0;
        return (u_char *) uci_search_result;
		
	case MD_MODEL:
		if (getparam("--mdmodel", uci_search_result) != 0)
			strncpy(uci_search_result, "unknown", sizeof("unknown"));
        *var_len = strlen(uci_search_result);
        *write_method = 0;
        return (u_char *) uci_search_result;
		
	case MD_TYPE:
		if (getparam("--mdtype", uci_search_result) != 0)
			strncpy(uci_search_result, "unknown", sizeof("unknown"));
        *var_len = strlen(uci_search_result);
        *write_method = 0;
        return (u_char *) uci_search_result;
		
	case MD_SERIAL:
		if (getparam("--mdserial", uci_search_result) != 0)
			strncpy(uci_search_result, "unknown", sizeof("unknown"));
        *var_len = strlen(uci_search_result);
        *write_method = 0;
        return (u_char *) uci_search_result;
		
	case MD_FWVER:
		if (getparam("--mdfwver", uci_search_result) != 0)
			strncpy(uci_search_result, "unknown", sizeof("unknown"));
        *var_len = strlen(uci_search_result);
        *write_method = 0;
        return (u_char *) uci_search_result;
		
	case MD_TEMP:
		if (getparam("--mdtemp", uci_search_result) != 0)
			strncpy(uci_search_result, "unknown", sizeof("unknown"));
        *var_len = strlen(uci_search_result);
        *write_method = 0;
        return (u_char *) uci_search_result;

// 	case CI0:
//                 if (read_config_entry("status.gsm_mib.ci0", uci_search_result) != 0)
//                         strncpy(uci_search_result, "unknown", sizeof("unknown"));
//         *var_len = strlen(uci_search_result);
//         *write_method = 0;
//         return (u_char *) uci_search_result;
// 
// 	case CI1:
//                 if (read_config_entry("status.gsm_mib.ci1", uci_search_result) != 0)
//                         strncpy(uci_search_result, "unknown", sizeof("unknown"));
//         *var_len = strlen(uci_search_result);
//         *write_method = 0;
//         return (u_char *) uci_search_result;
// 	
// 	case CI2:
//                 if (read_config_entry("status.gsm_mib.ci2", uci_search_result) != 0)
//                         strncpy(uci_search_result, "unknown", sizeof("unknown"));
//         *var_len = strlen(uci_search_result);
//         *write_method = 0;
//         return (u_char *) uci_search_result;
// 
// 	case CI3:
//                 if (read_config_entry("status.gsm_mib.ci3", uci_search_result) != 0)
//                         strncpy(uci_search_result, "unknown", sizeof("unknown"));
//         *var_len = strlen(uci_search_result);
//         *write_method = 0;
//         return (u_char *) uci_search_result;
// 
// 	case CI4:
//                 if (read_config_entry("status.gsm_mib.ci4", uci_search_result) != 0)
//                         strncpy(uci_search_result, "unknown", sizeof("unknown"));
//         *var_len = strlen(uci_search_result);
//         *write_method = 0;
//         return (u_char *) uci_search_result;
// 
// 	case CI5:
//                 if (read_config_entry("status.gsm_mib.ci5", uci_search_result) != 0)
//                         strncpy(uci_search_result, "unknown", sizeof("unknown"));
//         *var_len = strlen(uci_search_result);
//         *write_method = 0;
//         return (u_char *) uci_search_result;
// 	
// 	 case CI6:
//                 if (read_config_entry("status.gsm_mib.ci6", uci_search_result) != 0)
//                         strncpy(uci_search_result, "unknown", sizeof("unknown"));
//         *var_len = strlen(uci_search_result);
//         *write_method = 0;
//         return (u_char *) uci_search_result;
	
    default:
        DEBUGMSGTL(("snmpd", "unknown sub-id %d in var_gsm\n",
                    vp->magic));
    }
    return NULL;
}
