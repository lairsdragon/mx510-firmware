/*
 *  GSM MIB group interface - gsm.h
 *
 */

#ifndef _MIBGROUP_GSM_MIB_H
#define _MIBGROUP_GSM_MIB_H

config_require(util_funcs)

	void init_gsm_mib(void);
	extern FindVarMethod var_gsm;

#define RADIO_MODE		1
#define REG_MODE		2
#define MCC				3
#define MNC				4
#define CELL_ID			5
#define LAC				6
#define NET_TECH		7
#define ARFCN			8
#define UARFCN			9
#define RSSI			10
#define RSCP			11
#define ECIO			12
#define IMEI			13
#define IMSI			14
#define ICCID			15
#define SPN				16
#define LAN_MAC			17
#define WIFI_MAC		18
#define RT_MODEL		19
#define RT_TYPE			20
#define RT_SERIAL		21
#define RT_FWVER		22
#define MD_MODEL		23
#define MD_TYPE			24
#define MD_SERIAL		25
#define MD_FWVER		26
#define MD_TEMP			27
// #define CI0				28
// #define CI1				29
// #define CI2				30
// #define CI3				31
// #define CI4				32
// #define CI5				33
// #define CI6				34

#endif	/* _MIBGROUP_GSM_MIB_H */

