MDEX-MIB DEFINITIONS ::= BEGIN

IMPORTS
    OBJECT-TYPE, NOTIFICATION-TYPE, MODULE-IDENTITY,
    Integer32, Opaque, enterprises, Counter32
        FROM SNMPv2-SMI

    TEXTUAL-CONVENTION, DisplayString, TruthValue
	FROM SNMPv2-TC;

mdex MODULE-IDENTITY
    LAST-UPDATED "201307240000A"
    ORGANIZATION "MDEX"
    DESCRIPTION
            "The MIB module for MDEX routers.
            "
    REVISION      "201307240000A"
    DESCRIPTION
            "Initial version"
    ::= { enterprises 15398 }

-- the GSM group
--
-- a collection of objects to represent MX510 status.

gsm   OBJECT IDENTIFIER ::= { mdex 10 1 }

gsmRadioMode OBJECT-TYPE
    SYNTAX      DisplayString (SIZE (0..255))
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "MX510 radio mode"
    ::= { gsm 1 }
    
gsmRegistrationMode OBJECT-TYPE
    SYNTAX      DisplayString (SIZE (0..255))
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "MX510 registration mode"
    ::= { gsm 2 }
    
gsmMcc OBJECT-TYPE
    SYNTAX      DisplayString (SIZE (0..255))
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "MX510 mobile country code"
    ::= { gsm 3 }
    
gsmMnc OBJECT-TYPE
    SYNTAX      DisplayString (SIZE (0..255))
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "MX510 mobile network code"
    ::= { gsm 4 }
    
gsmCellId OBJECT-TYPE
    SYNTAX      DisplayString (SIZE (0..255))
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "MX510 cell id"
    ::= { gsm 5 }
    
gsmLac OBJECT-TYPE
    SYNTAX      DisplayString (SIZE (0..255))
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "MX510 LAC"
    ::= { gsm 6 }
    
gsmNetworkTechnology OBJECT-TYPE
    SYNTAX      DisplayString (SIZE (0..255))
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "MX510 network technology"
    ::= { gsm 7 }
    
gsmArfcn OBJECT-TYPE
    SYNTAX      DisplayString (SIZE (0..255))
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "MX510 ARFCN"
    ::= { gsm 8 }
    
gsmUarfcn OBJECT-TYPE
    SYNTAX      DisplayString (SIZE (0..255))
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "MX510 UARFCN"
    ::= { gsm 9 }
    
gsmRssi OBJECT-TYPE
    SYNTAX      DisplayString (SIZE (0..255))
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "MX510 RSSI"
    ::= { gsm 10 }
    
gsmRscp OBJECT-TYPE
    SYNTAX      DisplayString (SIZE (0..255))
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "MX510 RSCP"
    ::= { gsm 11 }
    
gsmEcio OBJECT-TYPE
    SYNTAX      DisplayString (SIZE (0..255))
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "MX510 ECIO"
    ::= { gsm 12 }
    
gsmImei OBJECT-TYPE
    SYNTAX      DisplayString (SIZE (0..255))
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "MX510 IMEI"
    ::= { gsm 13 }
    
gsmImsi OBJECT-TYPE
    SYNTAX      DisplayString (SIZE (0..255))
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "MX510 IMSI"
    ::= { gsm 14 }
    
gsmIccid OBJECT-TYPE
    SYNTAX      DisplayString (SIZE (0..255))
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "MX510 ICCID"
    ::= { gsm 15 }
    
gsmSpn OBJECT-TYPE
    SYNTAX      DisplayString (SIZE (0..255))
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "MX510 SPN"
    ::= { gsm 16 }
    
gsmLanMac OBJECT-TYPE
    SYNTAX      DisplayString (SIZE (0..255))
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "MX510 LAN mac address"
    ::= { gsm 17 }
    
gsmWifiMac OBJECT-TYPE
    SYNTAX      DisplayString (SIZE (0..255))
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "MX510 WIFI mac address"
    ::= { gsm 18 }
    
gsmRouterModel OBJECT-TYPE
    SYNTAX      DisplayString (SIZE (0..255))
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "MX510 router model"
    ::= { gsm 19 }
    
gsmRouterType OBJECT-TYPE
    SYNTAX      DisplayString (SIZE (0..255))
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "MX510 router type"
    ::= { gsm 20 }
    
gsmRouterSerial OBJECT-TYPE
    SYNTAX      DisplayString (SIZE (0..255))
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "MX510 router serial"
    ::= { gsm 21 }
    
gsmRouterFirmwareVersion OBJECT-TYPE
    SYNTAX      DisplayString (SIZE (0..255))
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "MX510 router firmware version"
    ::= { gsm 22 }
    
gsmModemModel OBJECT-TYPE
    SYNTAX      DisplayString (SIZE (0..255))
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "MX510 modem model"
    ::= { gsm 23 }
    
gsmModemType OBJECT-TYPE
    SYNTAX      DisplayString (SIZE (0..255))
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "MX510 modem type"
    ::= { gsm 24 }
    
gsmModemSerial OBJECT-TYPE
    SYNTAX      DisplayString (SIZE (0..255))
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "MX510 modem serial"
    ::= { gsm 25 }
    
gsmModemFirmwareVersion OBJECT-TYPE
    SYNTAX      DisplayString (SIZE (0..255))
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "MX510 modem firmware version"
    ::= { gsm 26 }
    
gsmModemTemperature OBJECT-TYPE
    SYNTAX      DisplayString (SIZE (0..255))
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "MX510 modem temperature"
    ::= { gsm 27 }

END
