#!/bin/sh

# This script contains general Teltonika-specific
# bash functions to be included in other scripts
# in order to avoid multiple funtion definitions.

# Tests if at least one of provided VID:PID is
# currently attached to the system.
#
# @param $1 - VID:PID list in capital
#
# @return - true if at least one VID:PID is found,
# false otherwise.
tlt_is_usb_attached() {
	local vidpid="$1"
	local devs=`cat /proc/bus/usb/devices | \
	sed -n '/.* Vendor=.* ProdID=.*/p' | \
	sed -e 's/.*Vendor=\(.*\) ProdID=\(.*\) Rev.*/\1:\2/' | \
	sed 'y/abcdefghijklmnopqrstuvwxyz/ABCDEFGHIJKLMNOPQRSTUVWXYZ/'`
	
	for i in $devs
	do
		for j in $vidpid
		do
			if [ "$j" = "$i" ] 
			then 
				return 0
			fi
		done
	done 
	return 1
}

# Checks external usb device and returns
# it's VID:PID.
#
# @return - VID:PID if found,
# 1 otherwise.
tlt_get_ext_vidpid() {
	local extvidpid=`cat /proc/bus/usb/devices | \
	sed -n '/.* Vendor=.* ProdID=.*/p' | \
	sed -e 's/.*Vendor=\(.*\) ProdID=\(.*\) Rev.*/\1:\2/' | \
	sed 'y/abcdefghijklmnopqrstuvwxyz/ABCDEFGHIJKLMNOPQRSTUVWXYZ/' | \
	awk 'END{print}'`

	echo $extvidpid
}

# Writes one config OPTION entry in provided
# uci config file.
#
# @param $1 - config file name
# @param $2 - section type
# @param $3 - section name
# @param $4 - option name
# @param $5 - value
#
# @return - true on success, false otherwise.
# NOTE: if you provide non-existing section
# type or name, behaviuor is undefined.
tlt_uci_write_opt() {
	local config_name=$1
	local section_type=$2
	local section_name=$3
	local option_name=$4
	local value=$5
	local ftest_cmd=`uci changes $config_name 2>&1`
	
	if [ "$config_name" == "" -o \
		"$section_type" == "" -o \
		"$section_name" == "" -o \
		"$option_name" == "" -o \
		"$value" == "" ]
	then
		return 1
	fi
	
	if [ "$ftest_cmd" = "uci: Entry not found" ]
	then
		touch /etc/config/$config_name
		uci add $config_name $section_type
		uci rename $config_name.@$section_type[0]=$section_name
		uci set $config_name.$section_name.$option_name=$value
		uci commit $config_name
	else
		local ret_cmd0=`uci get $config_name.$section_name 2>&1`
		local ret_cmd1=`uci get $config_name.$section_name.$option_name 2>&1`
		# FIXME: issue with existing type/name
		if [ "$ret_cmd1" != "$value" ]
		then
			if [ "$ret_cmd0" = "uci: Entry not found" ]
			then
				uci add $config_name $section_type
				uci rename $config_name.@$section_type[0]=$section_name
			fi
			uci set $config_name.$section_name.$option_name=$value
			uci commit $config_name
		fi
	fi
	return 0
}

# converts ordinary netmask to CIDR
#
# @param $1 - subnet mask "dot notion"
#
# @return - CIDR
tlt_mask2cidr() {
	nbits=0
	IFS=.
	for dec in $1 ; do
	case $dec in
		255) let nbits+=8;;
		254) let nbits+=7;;
		252) let nbits+=6;;
		248) let nbits+=5;;
		240) let nbits+=4;;
		224) let nbits+=3;;
		192) let nbits+=2;;
		128) let nbits+=1;;
		0);;
		*) echo "Error: $dec is not recognised"; exit 1
	esac
	done
	echo "$nbits"
}

# Wait for WAN to get IP
#
# @return - CIDR
#
tlt_wait_for_wan() {
	TYPE=`uci get network.wan.proto`
	while [ true ]; do
		if [ "$TYPE" == "static" ]; then
			EXTERNAL=`uci get network.wan.ipaddr`
		else 	
			WANIF=`grep network.wan.device /tmp/state/network | cut -d= -f2`
			if [ "$WANIF" == "3g-ppp" ]; then
				EXTERNAL=`grep network.ppp.ipaddr /tmp/state/network | cut -d= -f2`
			else
				EXTERNAL=`grep network.wan.ipaddr /tmp/state/network | cut -d= -f2`
			fi
		fi
		if [ "$EXTERNAL" == "" ]; then
			logger "Waiting for WAN IP to show up"
			sleep 5
		else
			echo "$EXTERNAL"
			break
		fi
	done
}
