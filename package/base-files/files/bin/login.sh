#!/bin/sh
# Copyright (C) 2006-2011 OpenWrt.org

if ( ! grep -qs '^root:[!x]\?:' /etc/shadow || \
     ! grep -qs '^root:[!x]\?:' /etc/passwd ) && \
   [ -z "$FAILSAFE" -a -z "$LOCKDOWN" ]
then
	echo "Login failed."
	exit 0
fi

exec /bin/ash --login
