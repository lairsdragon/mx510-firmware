#!/bin/sh

mkdir /tmp/trouble
/bin/sh /sbin/collect.sh 2>&1 | gzip > /tmp/trouble/trouble.gz
tar -cvzf /tmp/trouble/etc_configs.tar.gz /etc/config /etc/crontabs /etc/dropbear /etc/firewall.user /etc/group /etc/hosts /etc/inittab /etc/passwd /etc/profile /etc/rc.local /etc/shells /etc/sysctl.conf /etc/uhttpd.crt /etc/uhttpd.key >/dev/null 2>&1
tar -cvzf /tmp/trouble/tmp_logs.tar.gz /tmp/.uci /tmp/TZ /tmp/dhcp.leases /tmp/etc /tmp/lock /tmp/log /tmp/overlay /tmp/resolv.conf /tmp/resolv.conf.auto /tmp/run /tmp/spool /tmp/state /tmp/status >/dev/null 2>&1
cd /tmp
tar -cf /tmp/troubleshoot.tar trouble/ >/dev/null 2>&1
cd /
rm -r /tmp/trouble
