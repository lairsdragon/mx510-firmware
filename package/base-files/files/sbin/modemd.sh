#!/bin/sh

# modemd.sh - a daemon for 3G/4G modem maintainance.
# Mainly used to periodically check modem and perform
# some possible reactive operations on some exeptional
# modem condition, such as changed device files.

PROBE_INTERVAL="40"
PING_INTERVAL="5" # if no response
PING_RETRY="10"
IFACE_INTERVAL="5" # if no iface found
IFACE_RETRY="10"
TESTIP="8.8.8.8"
IFNAME="3g-ppp"

is_3g_mode() {
	local en=$(uci get network.ppp.enabled)
	
	if [ "$en" == "1" ]
	then
		return 0
	else
		return 1
	fi
}

init() {
	if [ -f /var/run/modemd.sh.pid ]
	then
		exit 0
	fi
	echo "$$" > /var/run/modemd.sh.pid

	if ( ! is_3g_mode )
	then
		exit 0
	fi
	
	local pr_en=$(uci get ping_reboot.ping_reboot.enable)
	local pr_host=$(uci get ping_reboot.ping_reboot.host)
	
	if [ "$pr_en" == "1" -a -n "$pr_host" ]
	then
		TESTIP="$pr_host"
	fi
}

is_iface_down() {
	local ret=$(ifconfig | grep -c $IFNAME)
	
	if [ "$ret" == "0" ]; then
		return 0
	else
		return 1
	fi		
}

# this may block up to 5s
is_ping_response_to() {
	local ip="$1"
	local ifname="$2"
	local ret="$(ping -c 1 -I $ifname -q -w 5 $ip | grep packets | cut -d , -f 2 | cut -d ' ' -f 2)"
	
	if [ "$ret" == "1" ]; then
		return 0
	else
		return 1
	fi	
}

is_ping_failure() {
	local failed=0
	
	while [ "$failed" -le "$PING_RETRY" ]
	do
		if ( is_ping_response_to $TESTIP $IFNAME )
		then
			return 1
		else
			failed=`expr $failed + 1`
			sleep $PING_INTERVAL
			continue
		fi
	done
	return 0
}

is_iface_failure() {
	local failed=0
	
	while [ "$failed" -le "$IFACE_RETRY" ]
	do
		if ( ! is_iface_down )
		then
			return 1
		else
			failed=`expr $failed + 1`
			sleep $IFACE_INTERVAL
			continue
		fi
	done
	return 0
}

do_3g_restart_soft() {
	/etc/init.d/3g restart &
	sleep 20
}

do_3g_restart_heavy() {
	/etc/init.d/network restart &
	sleep 20
}

do_3g_restart_hard() {
	telit --hardreset &
	sleep 60
}

maintain_he910eud() {
	while [ 1 ]
	do
		sleep $PROBE_INTERVAL
		
# 		if ( is_iface_failure )
# 		then
# 			logger "modemd.sh: hard 3g restart due lost interface"
# 			do_3g_restart_hard
# 			continue	
# 		fi
		
		if ( is_ping_failure )
		then
			logger "modemd.sh: soft 3g restart due long idle state"
			do_3g_restart_soft
			continue
		fi
		
	done
}

init
maintain_he910eud
