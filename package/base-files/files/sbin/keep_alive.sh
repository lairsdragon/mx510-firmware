#!/bin/sh

if [ $# -lt 3  ]
then
	echo "Usage: $0 <IP address> <period (s)> <interface>"
	exit
fi

address=$1
period=$2
iface=$3

while [ 1 ]
do
	/bin/sleep "$period"
	/bin/ping -c 1 -w 1 -I -q -I "$iface" "$address" > /dev/null 2>&1
done

