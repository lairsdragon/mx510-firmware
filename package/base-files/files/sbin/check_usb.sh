#!/bin/sh

# This script is called by HOTPLUG daemon
# on usb device insert or remove events.
# Mainly used for device-specific boot time
# house-keeping.

. /lib/teltonika-functions.sh

EVIDPID=$(tlt_get_ext_vidpid)
VID=`echo "$EVIDPID" | awk -F':' '{ print $1 }'`
PID=`echo "$EVIDPID" | awk -F':' '{ print $2 }'`
IS_FIRST_LOGIN=`uci get teltonika.sys.first_login`

case "$1" in
# do this on USB plug-in event
add)
	case "$EVIDPID" in

	# Teltonika GM62xx/UM62xx
	148E:A000 |\
	148E:099A)
		
		if [ "$IS_FIRST_LOGIN" = "1" ]
		then
			tlt_uci_write_opt network interface wan "ifname" "eth1"
		fi
		
		tlt_uci_write_opt system removable_device module "name" "TELTONIKA-WIMAX"
		tlt_uci_write_opt system removable_device module "vid" "$VID"
		tlt_uci_write_opt system removable_device module "pid" "$PID"
		tlt_uci_write_opt system removable_device module "type" "wimax"
		tlt_uci_write_opt system removable_device module "iface" "eth1"
		tlt_uci_write_opt system led usb_led "dev" "eth1"
		;;

	# Teltonika GM61xx
	1076:7F00 |\
	1076:7F30)

		if [ "$IS_FIRST_LOGIN" = "1" ]
		then
			tlt_uci_write_opt network interface wan "ifname" "wm0"
		fi
		
		tlt_uci_write_opt system removable_device module "name" "TELTONIKA-GCT"
		tlt_uci_write_opt system removable_device module "vid" "$VID"
		tlt_uci_write_opt system removable_device module "pid" "$PID"
		tlt_uci_write_opt system removable_device module "type" "wimax"
		tlt_uci_write_opt system removable_device module "iface" "wm0"
		tlt_uci_write_opt system led usb_led "dev" "wm0"
		;;

	# Option GTM681W
	0AF0:8120)

		if [ "$IS_FIRST_LOGIN" = "1" ]
		then
			tlt_uci_write_opt network interface wan "ifname" "usb0"
		fi
		
		tlt_uci_write_opt system removable_device module "name" "Option-GTM681W"
		tlt_uci_write_opt system removable_device module "vid" "$VID"
		tlt_uci_write_opt system removable_device module "pid" "$PID"
		tlt_uci_write_opt system removable_device module "type" "3g"
		tlt_uci_write_opt system removable_device module "iface" "usb0"
		tlt_uci_write_opt system led usb_led "dev" "usb0"
		;;

	# Option GTM661W
	0AF0:9000)

		if [ "$IS_FIRST_LOGIN" = "1" ]
		then
			tlt_uci_write_opt network interface wan "ifname" "3g-ppp"
			tlt_uci_write_opt network interface ppp "enabled" "1"
			tlt_uci_write_opt network interface ppp "proto" "3g"
			tlt_uci_write_opt network interface ppp "service" "umts"
			tlt_uci_write_opt network interface ppp "device" "/dev/ttyHS5"
			tlt_uci_write_opt network interface ppp "dialnumber" "*99#"
			tlt_uci_write_opt network interface ppp "regmode" "auto"
			tlt_uci_write_opt network interface ppp "mcc" "n/a"
			tlt_uci_write_opt network interface ppp "mnc" "n/a"
			tlt_uci_write_opt network interface ppp "radio" "n/a"
		fi
		
		tlt_uci_write_opt system removable_device module "name" "Option-GTM661W"
		tlt_uci_write_opt system removable_device module "vid" "$VID"
		tlt_uci_write_opt system removable_device module "pid" "$PID"
		tlt_uci_write_opt system removable_device module "type" "3g_ppp"
		tlt_uci_write_opt system removable_device module "iface" "3g-ppp"
		tlt_uci_write_opt system led usb_led "dev" "3g-ppp"
		;;
		
	# Telit Wireless HE910-EUD/D
	1BC7:0021)

		if [ "$IS_FIRST_LOGIN" = "1" ]
		then
			tlt_uci_write_opt network interface wan "ifname" "3g-ppp"
			tlt_uci_write_opt network interface ppp "enabled" "1"
			tlt_uci_write_opt network interface ppp "proto" "3g"
			tlt_uci_write_opt network interface ppp "service" "auto"
			tlt_uci_write_opt network interface ppp "device" "/dev/ttyACM3"
			tlt_uci_write_opt network interface ppp "dialnumber" "*99#"
			tlt_uci_write_opt network interface ppp "regmode" "auto"
			tlt_uci_write_opt network interface ppp "keepalive" "10,30"
			tlt_uci_write_opt network interface ppp "apn" "m2m.cda.vodafone.de"
			tlt_uci_write_opt network interface ppp "auth_mode" "pap"
			tlt_uci_write_opt network interface ppp "username" "mdex@m2m.mdex.de"
			tlt_uci_write_opt network interface ppp "password" "mdex"
			tlt_uci_write_opt network interface ppp "peerdns" "1"
		fi
		
		tlt_uci_write_opt system removable_device module "name" "Telit-HE910-EUD"
		tlt_uci_write_opt system removable_device module "vid" "$VID"
		tlt_uci_write_opt system removable_device module "pid" "$PID"
		tlt_uci_write_opt system removable_device module "type" "3g_ppp"
		tlt_uci_write_opt system removable_device module "iface" "3g-ppp"
		tlt_uci_write_opt system led usb_led "dev" "3g-ppp"
		;;

	# Sierra Wireless MC8700
	1199:68A3)

		if [ "$IS_FIRST_LOGIN" = "1" ]
		then
			tlt_uci_write_opt network interface wan "ifname" "3g-ppp"
			tlt_uci_write_opt network interface ppp "enabled" "1"
			tlt_uci_write_opt network interface ppp "proto" "3g"
			tlt_uci_write_opt network interface ppp "service" "umts"
			tlt_uci_write_opt network interface ppp "device" "/dev/ttyUSB3"
			tlt_uci_write_opt network interface ppp "dialnumber" "*99#"
			tlt_uci_write_opt network interface ppp "regmode" "auto"
			tlt_uci_write_opt network interface ppp "mcc" "n/a"
			tlt_uci_write_opt network interface ppp "mnc" "n/a"
			tlt_uci_write_opt network interface ppp "radio" "n/a"
		fi
		
		tlt_uci_write_opt system removable_device module "name" "Sierra-MC8700"
		tlt_uci_write_opt system removable_device module "vid" "$VID"
		tlt_uci_write_opt system removable_device module "pid" "$PID"
		tlt_uci_write_opt system removable_device module "type" "3g_ppp"
		tlt_uci_write_opt system removable_device module "iface" "3g-ppp"
		tlt_uci_write_opt system led usb_led "dev" "3g-ppp"
		;;
		
	# Huawei EM820W
	12D1:1404)

		if [ "$IS_FIRST_LOGIN" = "1" ]
		then
			tlt_uci_write_opt network interface wan "ifname" "3g-ppp"
			tlt_uci_write_opt network interface ppp "enabled" "1"
			tlt_uci_write_opt network interface ppp "proto" "3g"
			tlt_uci_write_opt network interface ppp "service" "umts"
			tlt_uci_write_opt network interface ppp "device" "/dev/ttyUSB4"
			tlt_uci_write_opt network interface ppp "dialnumber" "*99#"
			tlt_uci_write_opt network interface ppp "regmode" "auto"
			tlt_uci_write_opt network interface ppp "mcc" "n/a"
			tlt_uci_write_opt network interface ppp "mnc" "n/a"
			tlt_uci_write_opt network interface ppp "radio" "n/a"
		fi
		
		tlt_uci_write_opt system removable_device module "name" "Huawei-EM820W"
		tlt_uci_write_opt system removable_device module "vid" "$VID"
		tlt_uci_write_opt system removable_device module "pid" "$PID"
		tlt_uci_write_opt system removable_device module "type" "3g_ppp"
		tlt_uci_write_opt system removable_device module "iface" "3g-ppp"
		tlt_uci_write_opt system led usb_led "dev" "3g-ppp"
		;;

	# Broadmobi LP10
	2020:1005)

		if [ "$IS_FIRST_LOGIN" = "1" ]
		then
			tlt_uci_write_opt network interface wan "ifname" "3g-ppp"
			tlt_uci_write_opt network interface ppp "enabled" "1"
			tlt_uci_write_opt network interface ppp "proto" "3g"
			tlt_uci_write_opt network interface ppp "service" "umts"
			tlt_uci_write_opt network interface ppp "device" "/dev/ttyUSB3"
			tlt_uci_write_opt network interface ppp "dialnumber" "*99#"
			tlt_uci_write_opt network interface ppp "regmode" "auto"
			tlt_uci_write_opt network interface ppp "mcc" "n/a"
			tlt_uci_write_opt network interface ppp "mnc" "n/a"
			tlt_uci_write_opt network interface ppp "radio" "n/a"
		fi

		tlt_uci_write_opt system removable_device module "name" "Broadmobi-LP10"
		tlt_uci_write_opt system removable_device module "vid" "$VID"
		tlt_uci_write_opt system removable_device module "pid" "$PID"
		tlt_uci_write_opt system removable_device module "type" "3g_ppp"
		tlt_uci_write_opt system removable_device module "iface" "3g-ppp"
		tlt_uci_write_opt system led usb_led "dev" "3g-ppp"
		;;

	# Ralink OTG Controller
	1D6B:0002)
		logger -t "$0" "no USB device is attached"
		
		if [ "$IS_FIRST_LOGIN" = "1" ]
		then
			tlt_uci_write_opt network interface wan ifname "eth0.2"
		fi
		
		tlt_uci_write_opt system removable_device module "name" "none"
		tlt_uci_write_opt system removable_device module "vid" "none"
		tlt_uci_write_opt system removable_device module "pid" "none"
		tlt_uci_write_opt system removable_device module "type" "none"
		tlt_uci_write_opt system removable_device module "iface" "none"
		tlt_uci_write_opt system led usb_led "dev" "eth0.2"
		;;

	# unknown device      
	*)
		logger -t "$0" "cannot identify USB device"

		if [ "$IS_FIRST_LOGIN" = "1" ]
		then
			tlt_uci_write_opt network interface wan ifname "eth0.2"
		fi
		
		tlt_uci_write_opt system removable_device module "name" "unknown"
		tlt_uci_write_opt system removable_device module "vid" "unknown"
		tlt_uci_write_opt system removable_device module "pid" "unknown"
		tlt_uci_write_opt system removable_device module "type" "unknown"
		tlt_uci_write_opt system removable_device module "iface" "unknown"
		tlt_uci_write_opt system led usb_led "dev" "eth0.2"
		;;
	
	esac
	;;

# do this on USB plug-out event
remove)
	local oldvidpid=`echo "$(uci get system.module.vid):$(uci get system.module.pid)"`
	
	case "$oldvidpid" in
	0AF0:9000 |\
	1BC7:0021 |\
	1199:68A3 |\
	12D1:1404 |\
	2020:1005)
		killall gsmd > /dev/null 2>&1
		killall pppd > /dev/null 2>&1
		rm /var/lock/*tty* > /dev/null 2>&1
		;;
	*)
		logger -t "$0" "no need for service mod to this device ($oldvidpid)"
		;;
	esac

	if [ "$IS_FIRST_LOGIN" = "1" ]
	then
		tlt_uci_write_opt network interface wan ifname "eth0.2"
	fi
	
	tlt_uci_write_opt system removable_device module "name" "none"
	tlt_uci_write_opt system removable_device module "vid" "none"
	tlt_uci_write_opt system removable_device module "pid" "none"
	tlt_uci_write_opt system removable_device module "type" "none"
	tlt_uci_write_opt system removable_device module "iface" "none"
	tlt_uci_write_opt system led usb_led "dev" "eth0.2"
	;;
	
*)
	logger -t "$0" "unsupported command line argument"
	;;
	
esac