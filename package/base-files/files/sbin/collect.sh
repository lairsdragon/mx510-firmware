#!/bin/sh
#set -x
line="#############################################";

ifaces=`ls /sys/class/net/`
wifi=`ls /sys/class/net/ | grep wlan`
lan=`ls /sys/class/net/ | grep -v wlan | grep -v br-lan | grep -v usb0 | grep -v eth1`
wimax=`ls /sys/class/net/ | grep eth1`
g3g=`ls /sys/class/net/ | grep usb0`
br_if=$(brctl show | sed -n "2,\$s/\([a-zA-Z0-9\-]*\).*/\1/p")
tun_if=$(ip tun | sed "s/\([a-zA-Z0-9]*\).*/\1/")


echo -e "\n\n$line SYSTEM INFORMATION $line" 
echo "$line firmware $line" 
cat /etc/version

echo -e "\n$line uptime $line"
uptime


echo -e "\n$line files from /proc $line" 
if [ -e /proc/version ] ; then
	echo -e "\n$line /proc/version $line" 
    cat /proc/version
fi

if [ -e /proc/mtd ] ; then
	echo -e "\n$line /proc/mtd $line" 
	cat /proc/mtd 
fi
if [ -e /proc/meminfo ] ; then
	echo -e "\n$line /proc/meminfo $line" 
	cat /proc/meminfo 
fi

echo -e "\n$line process list $line" 
ps -w


echo -e "\n\n$line NETWORK INFORMATION $line"

echo -e "\n$line available interfaces $line" 
echo $ifaces

echo -e "\n$line wired interfaces $line" 
for iface in $lan ; do
	echo -e "\n$line $iface $line" 
	ifconfig $iface
	ip a sh dev $iface
done


echo -e "\n$line ip routes $line" 
ip r

echo -e "\n$line ip rules $line" 
ip rule

echo -e "\n$line ip neighbours $line" 
ip nei 

echo -e "\n$line ip tunnels $line" 
ip tun 

if [ "$g3g" != "" ] ; then  
    echo -e "\n\n$line 3G INTERFACES $line" 
	ifconfig $g3g
	ip a sh dev $g3g

	if [ $(ps | grep -q gobid && echo $?)  ] ; then 
		echo -e "\n$line gobiconnect get all $line" 
		gobiconnect get all&
        sleep 2
        killall gobiconnect >/dev/null 2>&1
	fi
    
fi

if [ "$wimax" != "" ] ; then  
    echo -e "\n\n$line WIMAX INTERFACES $line" 
	ifconfig $wimax
	ip a sh dev $wimax

    echo -e "\n$line wimax status $line"   
    #curl --connect-timeout 1 --user user:user --anyauth http://192.168.0.1/cgi/uptime
    curl --connect-timeout 1 --user user:user --anyauth http://192.168.0.1/cgi/state
    curl --connect-timeout 1 --user user:user --anyauth http://192.168.0.1/cgi/signal-quality-dbm
    #curl --connect-timeout 1 --user user:user --anyauth http://192.168.0.1/cgi/time
    curl --connect-timeout 1 --user user:user --anyauth http://192.168.0.1/cgi/signal-strength
    curl --connect-timeout 1 --user user:user --anyauth http://192.168.0.1/cgi/extip
    curl --connect-timeout 1 --user user:user --anyauth http://192.168.0.1/cgi/macaddress
    #curl --connect-timeout 1 --user user:user --anyauth http://192.168.0.1/cgi/downlink-speed
    #curl --connect-timeout 1 --user user:user --anyauth http://192.168.0.1/cgi/uplink-speed
    #curl --connect-timeout 1 --user user:user --anyauth http://192.168.0.1/cgi/bsid
    #curl --connect-timeout 1 --user user:user --anyauth http://192.168.0.1/cgi/uplink-coding
    #curl --connect-timeout 1 --user user:user --anyauth http://192.168.0.1/cgi/downlink-coding
    #curl --connect-timeout 1 --user user:user --anyauth http://192.168.0.1/cgi/firmware-version
    #curl --connect-timeout 1 --user user:user --anyauth http://192.168.0.1/cgi/microcode-version
    #curl --connect-timeout 1 --user user:user --anyauth http://192.168.0.1/cgi/driver-version
    #curl --connect-timeout 1 --user user:user --anyauth http://192.168.0.1/cgi/sequansd-version
    #curl --connect-timeout 1 --user user:user --anyauth http://192.168.0.1/cgi/server-version
fi


echo -e "\n\n$line WIRELESS SETTINGS $line" 
for iface in $wifi ; do
	echo -e "\n$line $iface $line" 
	ifconfig $iface
	ip a sh dev $iface
done

echo -e "\n\n$line wireless configuration $line" 
cat /var/run/hostapd-phy0.conf

echo -e "\n\n$line BRIDGE SETTINGS $line" 
if [ -z != "$br_if" ] ; then  
	echo -e "\n$line brctl show $line" 
	brctl show 
fi

echo -e "\n\n$line LOADED MODULES $line" 
lsmod


if [ $(lsmod |grep -q ^ip_tables && echo $?)  ] ; then  
	echo -e "\n\n$line IPTABLE rules $line"
	echo -e "\n$line iptables-save $line"
    iptables-save
	echo -e "\n$line iptables -L -v $line"
	iptables -L -v 
	if [ $(lsmod |grep -q ^iptable_nat && echo $?)  ] ; then 
		echo -e "\n$line iptables -t nat -L -v $line" 
		iptables -t nat -L -v 
	fi
	if [ $(lsmod |grep -q ^iptable_mangle && echo $?)  ] ; then 
		echo -e "\n$line iptables -t mangle -L -v $line" 
		iptables -t mangle -L -v 
	fi
fi

if [ $(lsmod |grep -q ^ebtables && echo $?)  ] ; then 
	echo -e "\n\n$line EBTABLE rules $line" 
	echo -e "\n$line ebtables -t filter -L --Lc $line" 
	ebtables -t filter -L --Lc 
	if [ $(lsmod |grep -q ^ebtable_nat && echo $?)  ] ; then 
		echo -e "\n$line ebtables -t nat -L --Lc $line" 
		ebtables -t nat -L --Lc 
	fi
	if [ $(lsmod |grep -q ^ebtable_broute && echo $?)  ] ; then 
		echo -e "\n$line ebtables -t broute -L -Lc $line" 
		ebtables -t broute -L --Lc 
	fi
fi

echo -e "\n\n$line dmesg output $line" 
dmesg 

echo -e "\n\n$line logread output $line"
logread


