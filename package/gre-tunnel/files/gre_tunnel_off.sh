#!/bin/sh
# close the GRE tunnel
  	
CONFIG_GET="uci get gre_tunnel.gre_tunnel"    
	local dev_name="tun1"
	local remote_net=`$CONFIG_GET.remote_net`
	local localTunIp=`$CONFIG_GET.tun_ip`
	local localTunCidr=`$CONFIG_GET.tun_cidr`
	local localTunNw=`maccalc ipand $localTunIp $localTunCidr`
	
	killall -9 gre_tunnel_keepalive.sh
	
	if [ "$remote_net" ]; then
		ip route del $remote_net
	fi
	if [ "$localTunIp" -a "$localTunCidr" ]; then
		ip route del $localTunIp/$localTunCidr
	fi
	ifconfig $dev_name down                   
	ip tunnel del $dev_name                	
