#!/bin/sh
# create GRE tunnel

. /lib/teltonika-functions.sh

CONFIG_GET="uci get gre_tunnel.gre_tunnel"
sh /sbin/gre_tunnel_off.sh

if [ `$CONFIG_GET.enable` -eq 1 ] ; then
	
	EXTERNAL=`tlt_wait_for_wan`
	LOCAL=`uci get network.lan.ipaddr`

	local remote_ip=`$CONFIG_GET.remote_ip`
		
	local dev_name="tun1"
	local ttl=`$CONFIG_GET.ttl`
	local pmtud=`$CONFIG_GET.pmtud`
	local mtu=`$CONFIG_GET.mtu`
	local remote_net=`$CONFIG_GET.remote_net`
	local rcidr=`$CONFIG_GET.rcidr`
	local localTunIp=`$CONFIG_GET.tun_ip`
	local localTunCidr=`$CONFIG_GET.tun_cidr`
	local localTunNw=`maccalc ipand $localTunIp $localTunCidr`
	local remoteTunNw=`maccalc ipand $remote_ip $rcidr`

	if [ $pmtud -eq 1 ] ; then
		`ip tunnel add $dev_name mode gre remote $remote_net local $EXTERNAL nopmtudisc`
	else
		`ip tunnel add $dev_name mode gre remote $remote_net local $EXTERNAL ttl $ttl`
	fi

	ifconfig $dev_name up

	ifconfig $dev_name $localTunIp
	
	ifconfig $dev_name pointopoint 0.0.0.0

	if [ $mtu -ne 0 ] ; then
		ip link set $dev_name mtu $mtu
	fi

	echo $remote_ip
	ip route add $remoteTunNw/$rcidr dev $dev_name
	ip route add $localTunNw/$localTunCidr dev $dev_name
	sleep 5
	ping "$remote_ip" -c 3
	sleep 1
	gre_tunnel_keepalive.sh $remote_ip & 2>&1 1>/dev/null
fi
