#include <stdlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include "cgi.h"
#include "functions.h"

/************************************************
 main
************************************************/
int main(int argc, char **argv)
{
	char Buffer[256];
	char Number[4];
	FILE *pP;
	char Return[32];
	
	cgi_init();
	cgi_process_form();
	//cgi_init_headers();
	
	if (cgi_param("number")) {
		if (strlen(cgi_param("number")) > 3) {
			printf("WRONG_NUMBER");
			cgi_end();
			return 0;
		}
			
		strncpy(Number, cgi_param("number"), sizeof(Number));
		Number[3] = '\0';
		if (IsInteger(Number, 0)) {
			sprintf(Buffer, "/sbin/sms -d %s", Number);
			pP = popen(Buffer, "r");
			if (pP) {
				if (fgets(Return, sizeof(Return), pP)) {
					printf(Return);
				}
				else
					printf("TIMEOUT");
				pclose(pP);
			}
			else
				printf("TIMEOUT");
		}
		else
			printf("WRONG_NUMBER");
	}
	else
		printf("ERROR");
	
	cgi_end();
	
	return 0;
}
