#include <stdlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include "cgi.h"
#include "functions.h"
#include "base64.h"

/************************************************
 main
************************************************/
int main(int argc, char **argv)
{
	char Text[170];
	char Return[32];
	char Number[32];
	char Buffer[512];
	char *cpTemp = NULL;
	FILE *pF, *pP;
	int len;
	
	if (!IsClientLocal()) {
		printf("You shall not pass!");
		
		return 0;
	}
	
	cgi_init();
	cgi_process_form();
	//cgi_init_headers();
	pF = popen("uci get sms_utils.sendmsg_lan.enable", "r");
	if (pF) {
		fgets(Buffer, sizeof(Buffer), pF);
		Buffer[strlen(Buffer)-1] = '\0';
		if (strcmp(Buffer, "1")) {
			printf("SMS sending via HTTP Post/Get is disabled in WebUI");
			cgi_end();
			return 0;
		}	
	}
	
	if (cgi_param("number") && cgi_param("text")) {
		strncpy(Number, cgi_param("number"), sizeof(Number));
		strncpy(Text, cgi_param("text"), sizeof(Text));
		Number[31] = Text[160] = '\0';
		if (!IsInteger(&Number[0], 0) || Number[0] != '0' || Number[1] != '0') {
			printf("WRONG_NUMBER");
			cgi_end();
			return 0;
		}
		
		cpTemp = encode_base64(strlen(Text), (unsigned char *) Text);
		
		snprintf(Buffer, sizeof(Buffer), "/sbin/sms -sb %s \"%s\"", Number, cpTemp);
		
		if (cpTemp)
			free(cpTemp);
			
		pP = popen(Buffer, "r");
		
		if (pP) {
			if (fgets(Return, sizeof(Return), pP)) {
				printf(Return);
			}
			else
				printf("TIMEOUT");
			pclose(pP);
		}
		else
			printf("TIMEOUT");
	}
	else
		printf("ERROR");
	
	cgi_end();
	
	return 0;
}
