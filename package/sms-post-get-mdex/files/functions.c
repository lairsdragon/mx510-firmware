#include <sys/stat.h>
#include <signal.h>
#include <fcntl.h>
#include <errno.h>
#include <syslog.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include "functions.h"

char sText[DEFAULT_STRING_LENGTH + 1];
char *cpStringNew, *cpBuf;
int iStringStriped;

//-------------------------------------------------------------------------------------------------
int IsInteger(char *cpString, int iNegative)
{
int ii = 0;

if(cpString)
	{
		if(iNegative == 0)		//tik teigiami
			{
				for(ii = 0; ii < strlen(cpString); ii++)
				  if((cpString[ii] >= '0') && (cpString[ii] <= '9'))
				   {}
				   else
				    return 0;
			}
			else			//teigiami ir neigiami
				{
					if(((cpString[0] == '-') && (strlen(cpString) >= 2)) || ((cpString[0] >= '0') && (cpString[0] <= '9')))
						{
							for(ii = 1; ii < strlen(cpString); ii++)
							  if((cpString[ii] >= '0') && (cpString[ii] <= '9'))
							   {}
							   else
							    return 0;
						}
						else
							return 0;
				}
	}

return 1;
}

//-------------------------------------------------------------------------------------------------
int SafeToPrint(char *cpValue, int iInteger, int iLength)
{
sText[0] = '\0';

if(strlen(cpValue) > iLength)
	{
		strncpy(sText, cpValue, iLength);
		sText[iLength] = '\0';
	}
	else
		strcpy(sText, cpValue);

if(iInteger == 1)
	if(!IsInteger(sText, 0))
		return 0;

return 1;
}

//-------------------------------------------------------------------------------------------------
char *StripString(char *cpString, char *cpLeave, int iLength)
{
int a = 0, j = 0, ii = 0;

SafeToPrint(cpString, 0, iLength);

cpStringNew = (char *) malloc(strlen(sText) + 1);
CheckMemoryAllocation(cpStringNew);

for(a = 0; a < strlen(sText); a++)
	{
		if(((cpString[a] >= '0') && (cpString[a] <= '9')) || ((cpString[a] >= 'A') && (cpString[a] <= 'Z')) || ((cpString[a] >= 'a') && (cpString[a] <= 'z')))
   			{
				cpStringNew[j] = sText[a];
				j++;
			}
			else
				{
					for(ii = 0; ii < strlen(cpLeave); ii++)
						{
							if(sText[a] == cpLeave[ii])
								{
									cpStringNew[j] = sText[a];
									j++;
									break;
								}
						}
				}
	}
cpStringNew[j] = '\0';

if (strlen(cpString) != strlen(cpStringNew))
	iStringStriped = 1;

return cpStringNew;
}

//-------------------------------------------------------------------------------------------------
void CheckMemoryAllocation(char *cpVar)
{
if(cpVar == NULL)
	{
		printf("ERROR_CANT_ALLOCATE_MEMORY");
		
		fflush(NULL);
		
		exit(EXIT_FAILURE);
	}
}

//-------------------------------------------------------------------------------------------------
int CheckInterval(char *cpValue, int iLength, int iBeginning, int iEnding)
{
if(SafeToPrint(cpValue, 0, iLength))
	if((atoi(sText) >= iBeginning) && (atoi(sText) <= iEnding))
		return 1;

return 0;
}
//-------------------------------------------------------------------------------------------------

/*
Patikrina ar klientas atejo is LAN
Grazina 1, jeigu is LAN ir 0, jeigu ne arba ivyko klaida
*/
int IsClientLocal() {
	FILE *pF;
	char sBuf[256];
	struct in_addr stClientIP, stRouterIP, stNetMask;
	
	pF = popen("uci get network.lan.ipaddr", "r");
	if (pF) {
		fgets(sBuf, sizeof(sBuf), pF);
		sBuf[strlen(sBuf) - 1] = '\0';
		
		pclose(pF);
		
		if (inet_aton((const char *) sBuf, &stRouterIP) == 0)
			return 0;
	}
	
	pF = popen("uci get network.lan.netmask", "r");
	if (pF) {
		fgets(sBuf, sizeof(sBuf), pF);
		sBuf[strlen(sBuf) - 1] = '\0';
		
		pclose(pF);
		
		if (inet_aton((const char *) sBuf, &stNetMask) == 0)
			return 0;
	}	

	
	if (inet_aton(getenv("REMOTE_ADDR"), &stClientIP) == 0)
		return 0;
	
	if ((stClientIP.s_addr & stNetMask.s_addr) != (stRouterIP.s_addr & stNetMask.s_addr))
		return 0;
	
	return 1;
}
//-------------------------------------------------------------------------------------------------