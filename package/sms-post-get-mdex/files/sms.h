#ifndef SMS_MAIN_H
#define SMS_MAIN_H

#define RX_BUF_LEN 128
#define STRING_LEN 512

typedef struct {
	char SMSC_Length; // sms center number length
	char SMSC_Type;
	char SMSC_Number[32];
	char FO_SMSD;
	char AddrLength;
	char AddrType;
	char Sender[32];
	char TP_PID;
	char TP_DCS;
	char TimeStamp[24];
	char DataLength;
	char Data[161];
} SMS;

#endif