#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <libgsmd/libgsmd.h>
#include <libgsmd/pin.h>
#include <libgsmd/misc.h>
#include <libgsmd/sms.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <syslog.h>
#include "base64.h"
#include "sms.h"

//#define DBG_FLAG
#define BUF_LEN			512
#define SMS_TXT_LEN		160
#define SMS_PDU_LEN		300
#define NUMBER_LEN		16
#define TRUE 			1
#define FALSE			0
/************************************************
* Debug
************************************************/
#ifdef DBG_FLAG
#define DBG(fmt, args...)	fprintf(stderr, fmt, ##args)
#else
#define DBG(fmt, args...)
#endif
#define ERR(fmt, args...)	fprintf(stderr, fmt, ##args)

/************************************************
 Globals
************************************************/
char gVidPid[BUF_LEN / 16];
int gsm_fd;
struct lgsm_handle *lgsmh;
char StringBuffer[STRING_LEN];
char RequestNumber[32];
char cSuccessFlag = FALSE, cSendResponse = FALSE;
char cDeleteFlag = 0;

/************************************************
 Prototypes
************************************************/
static void timeout(int sig);
void quit(int exit_code, struct lgsm_handle *lgsmh);
static int sms_handler(struct lgsm_handle *lh, struct gsmd_msg_hdr *gmh);
char Message_Send(char *pNumberchar, char *pText);
char Message_Delete(char *pNumber);
char Message_Get(char *pNumber);
char Message_Inbox_Count();
char Check_CLI_Args(int argc, char **argv);
char Print_String();
int Read_SMS_Telit();
int message_parser(char *pBuffer, int iBufLength, SMS *pSMS);
static int convert_7bit_to_ascii(char *a7bit, int length, char **ascii);
static int convert_ascii_to_7bit(char *ascii, char **a7bit);
int ascii_to_pdu(char *ascii, unsigned char **pdu, int *cnt_7bit);

/************************************************
* message_parser
************************************************/
int message_parser(char *pBuffer, int iBufLength, SMS *pSMS)
{
	char *ascii, tmp[170];
	int addrLenLoop;
	int index, i, j;
	
	index = 0;
	pSMS->SMSC_Length = pBuffer[index++];
	
	pSMS->SMSC_Type = pBuffer[index++];
	
	// pSMS.SMSC_Length - 1 <- because SMSC_Type is already filled
	for (j = 0, i = 0; j < pSMS->SMSC_Length - 1; j++) {
		pSMS->SMSC_Number[i] = (pBuffer[index] & 0x0F) + 0x30;
		i++;
		pSMS->SMSC_Number[i] = ((pBuffer[index] & 0xF0) >> 4) + 0x30;
		i++;
		index++;
	}
	// now we will remove 'F' at the end of SMSC_Number or insert '\0' otherwise
	pSMS->SMSC_Number[i] = '\0';
	if (pSMS->SMSC_Number[i-1] == 0x3F)
		pSMS->SMSC_Number[i-1] = '\0';
	
	pSMS->FO_SMSD = pBuffer[index++];
	
	pSMS->AddrLength = pBuffer[index++];
	if ((pSMS->AddrLength % 2) != 0)
		addrLenLoop = (pSMS->AddrLength / 2) + 1;
	else
		addrLenLoop = pSMS->AddrLength / 2;

	pSMS->AddrType = pBuffer[index++];

	for (j = 0, i = 0; j < addrLenLoop; j++) {
		pSMS->Sender[i] = (pBuffer[index] & 0x0F) + 0x30;
		i++;
		pSMS->Sender[i] = ((pBuffer[index] & 0xF0) >> 4) + 0x30;
		i++;
		index++;
	}
	// now we will remove 'F' at the end of SMSC_Number or insert '\0' otherwise
	pSMS->Sender[i] = '\0';
	if (pSMS->Sender[i-1] == 0x3F)
		pSMS->Sender[i-1] = '\0';
	
	pSMS->TP_PID = pBuffer[index++];
	
	pSMS->TP_DCS = pBuffer[index++];
	
	for (i = 0, j = 0; i < 7; i++) {
		tmp[j++] = pBuffer[index] & 0x0F;
		tmp[j++] = (pBuffer[index++] & 0xF0) >> 4;
	}
	// convert time stamp to human-readable format
	sprintf(pSMS->TimeStamp, "20%i%i-%i%i-%i%i %i%i:%i%i:%i%i", tmp[0], tmp[1], tmp[2],
			tmp[3], tmp[4], tmp[5], tmp[6], tmp[7], tmp[8], tmp[9], tmp[10], tmp[11]);
	
	pSMS->DataLength = pBuffer[index++];
	
	i = 0;
	while (index < iBufLength) {
		tmp[i] = pBuffer[index++];
		i++;
	}
	tmp[i] = '\0';
	pdu_to_ascii(tmp, i, &ascii);
	strcpy(pSMS->Data, ascii);
	free(ascii);

	// may be useful debug info
#ifdef DBG_FLAG
	printf("%i\n", pSMS->SMSC_Length);
	printf("%02X\n", pSMS->SMSC_Type);
	printf("%s\n", pSMS->SMSC_Number);
	printf("%02X\n", pSMS->FO_SMSD);
	printf("%i\n", pSMS->AddrLength);
	printf("%02X\n", pSMS->AddrType);
	printf("%s\n", pSMS->Sender);
	printf("%02X\n", pSMS->TP_PID);
	printf("%02X\n", pSMS->TP_DCS);
	printf("%s\n", pSMS->TimeStamp);
	printf("%i\n", pSMS->DataLength);
	printf("%s\n", pSMS->Data);
#endif	
	return 0;
}

int Read_SMS_Telit(int iMsgNumber)
{
	char rx_buf[512], *pChar;
	int rlen = 512, iStatus, iMessageSizePDU, i, j;
	char CmdBuffer[128], SMSTextPDU[300], smsTxt[161], Number[18], smsNumber[18], smsDate[32];
	const char *msgtype[] = {"Unread", "Read", "Unsent", "Sent"};
	SMS Struct_SMS;
	
	sprintf(CmdBuffer, "AT+CMGR=%d", iMsgNumber);
	
	if (lgsm_passthrough(lgsmh, CmdBuffer, rx_buf, &rlen) <= 0)
 		return -1;
 		
	if (sscanf(rx_buf, "+CMGR: %d,", &iStatus) == 1) {
		if (pChar = strchr(rx_buf, '\n')) {
			strcpy(SMSTextPDU, pChar+1);
			DBG("SMS TEXT PDU:%s\n", SMSTextPDU);
			iMessageSizePDU = strlen(SMSTextPDU);
			
			j = 0;
			// kadangi PDU parseris skirtas apdorot hex skaiciam, o ne string, modifikuojam buferi.
			for (i = 0; i < iMessageSizePDU; i++)
			{
				SMSTextPDU[j] = ((SMSTextPDU[i] <= '9') ? SMSTextPDU[i] - 0x30 : SMSTextPDU[i] - 0x37) << 4;
				i++;
				SMSTextPDU[j] |= (SMSTextPDU[i] <= '9') ? SMSTextPDU[i] - 0x30 : SMSTextPDU[i] - 0x37;
				j++;
			}
			iMessageSizePDU = j;
			message_parser(SMSTextPDU, iMessageSizePDU, &Struct_SMS);
			strcpy(smsNumber, "+");
			strcat(smsNumber, Struct_SMS.Sender);
			
			sprintf(StringBuffer, "Date: %s\nSender: %s\nText: %s\nStatus: %s\n",
				Struct_SMS.TimeStamp, smsNumber, Struct_SMS.Data, msgtype[iStatus]);
			Print_String();
			exit(0);
			
		}
		else
			DBG("sms parsing error");
	}
	else {
		DBG("no msg, exit\n");
		return 0;
	}
	
	return 0;
}

/************************************************
* timeout
************************************************/
static void timeout(int sig)
{
	if (cSendResponse == TRUE) {
		if (cSuccessFlag == TRUE) {
			sprintf(StringBuffer, "OK\n");
			Print_String();
		}	
		else {
			sprintf(StringBuffer, "ERROR\n");
			Print_String();
		}	
	}	
	DBG("no response, exit\n");
	exit(0);
}

/************************************************
* quit
************************************************/
void quit(int exit_code, struct lgsm_handle *lgsmh)
{
	if (lgsmh)
		lgsm_exit(lgsmh);
	exit(exit_code);
}

/************************************************
* sms_handler
************************************************/
static int sms_handler(struct lgsm_handle *lh, struct gsmd_msg_hdr *gmh)
{
	char payload[GSMD_SMS_DATA_MAXLEN];
	int *result;
	struct gsmd_sms_list *sms;
	struct gsmd_addr *addr;
	struct gsmd_sms_storage *mem;
	static const char *msgtype[] = {"Unread", "Read", "Unsent", "Sent"};
	static const char *memtype[] = {"Unknown", "Broadcast", "Me message", "MT", "SIM", "TA", "SR"};
	char NumberInternational[32];
	
	switch (gmh->msg_subtype) {
		case GSMD_SMS_LIST:
		case GSMD_SMS_READ:
			sms = (struct gsmd_sms_list *) ((void *) gmh + sizeof(*gmh));
			if(sms->payload.is_voicemail)
				DBG("it's a voicemail \n");
			DBG("%s message %i from/to %s%s, at %i%i-%i%i-%i%i "
					"%i%i:%i%i:%i%i, GMT%c%i\n",
					msgtype[sms->stat], sms->index,
					((sms->addr.type & __GSMD_TOA_TON_MASK) == GSMD_TOA_TON_INTERNATIONAL) ? "+" : "",
					sms->addr.number,
					sms->time_stamp[0] & 0xf,
					sms->time_stamp[0] >> 4,
					sms->time_stamp[1] & 0xf,
					sms->time_stamp[1] >> 4,
					sms->time_stamp[2] & 0xf,
					sms->time_stamp[2] >> 4,
					sms->time_stamp[3] & 0xf,
					sms->time_stamp[3] >> 4,
					sms->time_stamp[4] & 0xf,
					sms->time_stamp[4] >> 4,
					sms->time_stamp[5] & 0xf,
					sms->time_stamp[5] >> 4,
					(sms->time_stamp[6] & 8) ? '-' : '+',
					(((sms->time_stamp[6] << 4) |
					(sms->time_stamp[6] >> 4)) & 0x3f) >> 2);
			if (sms->payload.coding_scheme == ALPHABET_DEFAULT) {
				unpacking_7bit_character(&sms->payload, payload);
				DBG("\"%s\"\n", payload);
			} else if (sms->payload.coding_scheme == ALPHABET_8BIT)
				DBG("8-bit encoded data\n");
			else if (sms->payload.coding_scheme == ALPHABET_UCS2)
				DBG("Unicode-16 encoded text\n");
			if (sms->is_last)
				DBG("Last sms\n");
			
			// check number format (with or withou '+' at the beginning)
			if ((sms->addr.type & __GSMD_TOA_TON_MASK) == GSMD_TOA_TON_INTERNATIONAL) {
				strcpy(NumberInternational, "+");
				strcat(NumberInternational, sms->addr.number);
			}
			else
				strcpy(NumberInternational, sms->addr.number);
		
			sprintf(StringBuffer, "Date: 20%i%i-%i%i-%i%i %i%i:%i%i:%i%i\nSender: %s\nText: %s\nStatus: %s\n", 
									(sms->time_stamp[0] & 0x0F),
									(sms->time_stamp[0] & 0xF0)>>4,
									(sms->time_stamp[1] & 0x0F),
									(sms->time_stamp[1] & 0xF0)>>4,
									(sms->time_stamp[2] & 0x0F),
									(sms->time_stamp[2] & 0xF0)>>4,
									(sms->time_stamp[3] & 0x0F),
									(sms->time_stamp[3] & 0xF0)>>4,
									(sms->time_stamp[4] & 0x0F),
									(sms->time_stamp[4] & 0xF0)>>4,
									(sms->time_stamp[5] & 0x0F),
									(sms->time_stamp[5] & 0xF0)>>4,
									NumberInternational,
									payload,
									msgtype[sms->stat]);
			//cSuccessFlag = TRUE;
			Print_String();
			exit(0);
			break;
		case GSMD_SMS_SEND:
			result = (int *) ((void *) gmh + sizeof(*gmh));
			if (*result >= 0) {
				DBG("Send: message sent as ref %i\n", *result);
				syslog(LOG_NOTICE, "SMS_SEND: SMS sent successfully");
				sprintf(StringBuffer, "OK\n");
				Print_String();
				exit(0);
			break;
			}
			switch (-*result) {
			case 42:
				DBG("Store: congestion\n");
				break;
			default:
				ERR("Store: error %i\n", *result);
				break;
			}
			break;
		case GSMD_SMS_WRITE:
			result = (int *) ((void *) gmh + sizeof(*gmh));
			if (*result >= 0) {
				DBG("Store: message stored with index %i\n", *result);
				break;
			}
			switch (-*result) {
				case GSM0705_CMS_SIM_NOT_INSERTED:
					ERR("Store: SIM not inserted\n");
					break;
				default:
					ERR("Store: error %i\n", *result);
					break;
				}
			break;
		case GSMD_SMS_DELETE:
			result = (int *) ((void *) gmh + sizeof(*gmh));
			switch (*result) {
				case 0:
					DBG("Delete: success\n");
					sprintf(StringBuffer, "OK");
					Print_String();
					exit(0);
					break;
				case GSM0705_CMS_SIM_NOT_INSERTED:
					ERR("Delete: SIM not inserted\n");
					break;
				case GSM0705_CMS_INVALID_MEMORY_INDEX:
					ERR("Delete: invalid memory index\n");
					break;
				default:
					ERR("Delete: error %i\n", *result);
					break;
			}
			break;
		case GSMD_SMS_GET_MSG_STORAGE:
			mem = (struct gsmd_sms_storage *)((void *) gmh + sizeof(*gmh));
			/*printf("mem1: %s (%i)       Occupied: %i / %i\n",
					memtype[mem->mem[0].memtype],
					mem->mem[0].memtype,
					mem->mem[0].used,
					mem->mem[0].total);
			printf("mem2: %s (%i)       Occupied: %i / %i\n",
					memtype[mem->mem[1].memtype],
					mem->mem[1].memtype,
					mem->mem[1].used,
					mem->mem[1].total);
			printf("mem3: %s (%i)       Occupied: %i / %i\n",
					memtype[mem->mem[2].memtype],
					mem->mem[2].memtype,
					mem->mem[2].used,
					mem->mem[2].total);*/
			cSendResponse = FALSE;
			sprintf(StringBuffer, "Occupied: %i\nTotal: %i\n", mem->mem[0].used, mem->mem[0].total);
			Print_String();
			exit(0);
			break;
		case GSMD_SMS_GET_SERVICE_CENTRE:
			addr = (struct gsmd_addr *) ((void *) gmh + sizeof(*gmh));
			DBG("Number of the default Service Centre is %s\n", addr->number);
			break;
		default:
			return -EINVAL;
	}
	return 0;
}

/************************************************
* main
************************************************/
int main(int argc, char **argv)
{
	FILE *pCMD;
	char buf[512], at_buf[512];
	int rc = 0, rlen=512;
	int ret;
	
	// update device vid:pid first
	pCMD = popen("echo \"`uci get system.module.vid`:`uci get system.module.pid`\"", "r");
	fgets(gVidPid, 32, pCMD);
	
	// timeout alarm 
	signal(SIGALRM, timeout);
	
	// Connect to gsmd daemon 
	lgsmh = lgsm_init(LGSMD_DEVICE_GSMD);
	if (!lgsmh) {
		fprintf(stderr, "Can't connect to gsmd\n");
		exit(1);
	}

   	gsm_fd = lgsm_fd(lgsmh);
   	if (gsm_fd <= 0)
   		fprintf(stderr, "Bad file descriptor\n");
	
	if (!strcmp(gVidPid, "1BC7:0021\n")) {
		if (lgsm_passthrough(lgsmh, "AT+CSMP=17,167,0,16", at_buf, &rlen) <= 0)
			fprintf(stderr, "Can't initialize SMS in Telit modem\n");
	}
	else
		quit(2, lgsmh);
	
	// register phone handler
	ret = lgsm_register_handler(lgsmh, GSMD_MSG_SMS, sms_handler);
	
	Check_CLI_Args(argc, argv);
	//ret = lgsm_sms_list(lgsmh, GSMD_SMS_REC_UNREAD);

	while(1)
	{
		rc = read(gsm_fd, buf + rc, sizeof(buf) - rc) + rc;
		if (rc <= 0) {
			fprintf(stderr, "ERROR reading from gsm_fd\n");
			quit(2, lgsmh);
		}

		rc = lgsm_handle_packet(lgsmh, buf, rc);
		if (rc < 0)
		{
			fprintf(stderr, "ERROR processing packet: %d(%s)\n", rc, strerror(-rc));
			rc = 0;
		}
	}
	
	return(-1);
}
/************************************************
* Usage:
* send sms: 				./sms -s phoneNumber text(read text from stdin)
* send sms: 				./sms -sb phoneNumber text(encoded in base64)
* total messages in inbox: 	./sms -t
* read sms by number:		./sms -r number
* delete sms by number:		./sms -d number
************************************************/
char Check_CLI_Args(int argc, char **argv)
{
	char *cpTemp;
	int iResult;
	
	if (argc == 1) {
		ERR("No command line arguments provided!\n");
		exit(-1);
	}
	
	if (!strcmp(argv[1], "-s")) {
		if (argc == 4) {
			alarm(15); // kartais labai ilgai uztrunka siuntimas, kitais atvejais uztenka 5s
			Message_Send(argv[2], argv[3]);
			return 0;
		}
	}
	
	if (!strcmp(argv[1], "-sb")) {
		if (argc == 4) {
			cpTemp = (char *) malloc(strlen(argv[3]) + 1);
			if (cpTemp == NULL) {
				ERR("Can't allocate memory!\n");
				exit(EXIT_FAILURE);
			}
			
			iResult = decode_base64((unsigned char *) cpTemp, argv[3]);
			
			if(iResult > 0) {
				cpTemp[iResult] = '\0';
				alarm(15); // kartais labai ilgai uztrunka siuntimas, kitais atvejais uztenka 5s
				Message_Send(argv[2], cpTemp);
				free(cpTemp);
			} else {
				free(cpTemp);
				ERR("Can't decode string!\n");
				exit(EXIT_FAILURE);
			}
			
			return 0;
		}
	}
	
	/*else if (!strcmp(argv[1], "-t")) {
		if (argc == 2) {
			alarm(5);
			Message_Inbox_Count();
			return 0;
		}
	}	
	else if (!strcmp(argv[1], "-r")) {
		if (argc == 3) {
			alarm(5);
			Message_Get(argv[2]);
			return 0;
		}
	}
	else if (!strcmp(argv[1], "-d")) {
		if (argc == 3) {
			alarm(5);
			Message_Delete(argv[2]);
			return 0;
		}	
	}*/
	
	ERR("Command line arguments incorrect!\n");
	ERR("Usage: ./sms -s 00{COUNTRY_CODE}{PHONE_NUMBER} {SMS_TEXT}\n");
	ERR("Usage: ./sms -sb 00{COUNTRY_CODE}{PHONE_NUMBER} {SMS_TEXT_ENCODED_IN_BASE64}\n");
	exit(-1);
}
/************************************************
* Message_Send
************************************************/ 
char Message_Send(char *pNumber, char *pText)
{
	char Number[32], Text[170], trimNumber[32];
	struct lgsm_sms sms;
	int ret, iLength, i, pdu_len, cnt_7bit;
	FILE *pF;
	unsigned char *pdu;
	char logBuffer[256];
	cSendResponse = TRUE;
	
	strncpy(Number, pNumber, sizeof(Number));
	Number[31] = '\0'; 
	strncpy(Text, pText, sizeof(Text));
	Text[160] = '\0';
	
	if (Number[0] != '0' || Number[1] != '0') {
		ERR("Wrong phone number format!\n");
		exit(0);
	}
	for (i = 0; i < strlen(Number); i++) {
		if (Number[i] >= '0' && Number[i] <= '9'){ 
		
		}
		else {
			ERR("Phone number must contain digits only!\n");
			exit(0);
		}
			
	}
	trimNumber[0] = '+';
	trimNumber[1] = '\0';
	strcat(trimNumber, &Number[2]);
	sms.alpha=ALPHABET_DEFAULT;
	sms.ask_ds = 0; // ask device status
	strcpy(sms.addr, trimNumber);
	// blogai pavercia, jei 7 simboliai, nenaudojam
	//packing_7bit_character(Text, &sms);
	pdu_len = ascii_to_pdu(Text, &pdu, &cnt_7bit);
	sms.length = cnt_7bit; // ammount of 7 bit bytes
	memcpy(sms.data, pdu, pdu_len); // 140+1 bytes
	sprintf(logBuffer, "SMS_SEND: Trying to send SMS \"%s\" to number: %s\n", Text, trimNumber);
	syslog(LOG_NOTICE, logBuffer);
	ret = lgsm_sms_send(lgsmh, &sms);
	free(pdu);
	if (ret) {
		ERR("Function Message_Send error!\n");
		return -1;
	}
	
	return 0;
}

/************************************************
* Message_Inbox_Count
************************************************/ 
char Message_Inbox_Count()
{	
	cSendResponse = TRUE;
	lgsm_sms_get_storage(lgsmh);	
	
	return 0;
}

/************************************************
* Message_Get
************************************************/ 
char Message_Get(char *pNumber)
{
	int iNumber;
	char Buffer[32];
	FILE *pCMD;
	
	pCMD = popen("echo \"`uci get system.module.vid`:`uci get system.module.pid`\"", "r");
	fgets(Buffer, 32, pCMD);
	
	// Telit modem (doesn't support sms read API function)
	if (!strcmp(Buffer, "1BC7:0021\n")) {
		cSendResponse = TRUE;
		iNumber = atoi(pNumber);
		Read_SMS_Telit(iNumber);
	}	
	// Other modem
	else {
		cSendResponse = TRUE;
		iNumber = atoi(pNumber);
		lgsm_sms_read(lgsmh, iNumber);
	}
	
	return 0;
}

/************************************************
* Message_Delete
************************************************/ 
char Message_Delete(char *pNumber)
{
	struct lgsm_sms_delete sms_del;

	cSendResponse = TRUE;
  	sms_del.index = atoi(pNumber);
	sms_del.delflg = 0; // delete messages by index
	lgsm_sms_delete(lgsmh, &sms_del);
	
	return 0;
}

/************************************************
* Print_String
************************************************/
char Print_String()
{
	fprintf(stdout, "%s", StringBuffer);
	
	return 0;
}