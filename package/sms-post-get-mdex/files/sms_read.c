#include <stdlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include "cgi.h"
#include "functions.h"

/************************************************
 main
************************************************/
int main(int argc, char **argv)
{
	char Text[256], cFound = 0;
	char Number[4];
	char Buffer[256];
	FILE *pCMD;
	
	cgi_init();
	cgi_process_form();
	//cgi_init_headers();
	
	if (cgi_param("number")) {
		if (strlen(cgi_param("number")) > 3) {
			printf("WRONG_NUMBER");
			cgi_end();
			return 0;
		}
		
		strncpy(Number, cgi_param("number"), sizeof(Number));
		Number[3] = '\0';
		if (!IsInteger(Number, 0)) {
			printf("WRONG_NUMBER");
			cgi_end();
			return 0;
		}
			
		sprintf(Buffer, "/sbin/sms -r %s", Number);
		pCMD = popen(Buffer, "r");
		if (pCMD) {
			if (fgets(Text, sizeof(Text), pCMD)) {
				printf("%s", Text);
				cFound = 1;
			}
			else {
				printf("TIMEOUT");
				pclose(pCMD);
				cgi_end();
				return 0;
			}
			
			if (fgets(Text, sizeof(Text), pCMD))
				printf("%s", Text);
			if (fgets(Text, sizeof(Text), pCMD))
				printf("%s", Text);
			if (fgets(Text, sizeof(Text), pCMD)) {
				Text[strlen(Text)-1] = '\0';
				printf("%s", Text);
			}
			
			pclose(pCMD);
			if (cFound) {
				cgi_end();
				return 0;
			}	
		}
		else
			printf("TIMEOUT");
	}

	printf("ERROR");
	cgi_end();
	
	return 0;
}
