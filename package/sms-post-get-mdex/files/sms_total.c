#include <stdlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include "cgi.h"
#include "functions.h"

/************************************************
 main
************************************************/
int main(int argc, char **argv)
{
	char Text[170];
	char Buffer[256];
	FILE *pCMD;
	char cFound = 0;
	
	cgi_init();
	cgi_process_form();
	//cgi_init_headers();
	
	sprintf(Buffer, "/sbin/sms -t");
	pCMD = popen(Buffer, "r");
	if (pCMD) {
		if(fgets(Text, sizeof(Text), pCMD)) {
			printf("%s", Text);
			cFound = 1;
		}
		else {
			printf("TIMEOUT");
			pclose(pCMD);
			cgi_end();
			return 0;
		}
		
		if(fgets(Text, sizeof(Text), pCMD)) {
			Text[strlen(Text)-1] = '\0'; // rm new line
			printf("%s", Text);
		}
		pclose(pCMD);
		
		if(cFound) {
			cgi_end();
			return 0;
		}
	}
	else
		printf("TIMEOUT");
	
	cgi_end();
	
	return 0;
}
