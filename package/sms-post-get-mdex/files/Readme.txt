# AT commands via TCP/IP socket
# Default port: 9000
# NOTE:	User must ensure that AT command is valid.
#		If incorrect AT command is sent GSM modem can stop responding (timeout will be replied). 
#		In such case user must wait about 1 min until GSM modem starts operating normally. 
#		Otherwise router reboot is required.
		
		
# RUT500 SMS functionality and examples using HTTP Post/Get.

/cgi-bin/sms_send?number=PHONE_NUMBER&text=SMS_TEXT
# Send SMS to recipient.
# If action success "OK" will be replied, otherwise "ERROR" will be replied.
# If no response from modem received, "TIMEOUT" will be replied.
# Recipient phone number must contain country code.
# If recipient phone number has no country code prefix, "WRONG_NUMBER" will be replied.
# e.g.: http://192.168.1.1/cgi-bin/sms_send?number=%2B37012345678&text=test
# Router response:
# OK

/cgi-bin/sms_total?
# Total messages in inbox.
# If action success, response is:
# Occupied: COUNT
# Total: TOTAL_COUNT
# If action was unsuccessful, "ERROR" will be replied.
# If no response from modem received, "TIMEOUT" will be replied.
# e.g.: http://192.168.1.1/cgi-bin/sms_total?
# And the response is:
# Occupied: 2
# Total: 30

/cgi-bin/sms_read?number=MESSAGE_NUMBER
# Read message by its position in memory.
# If action success, response is:
# Date: FULL_DATE_AND_TIME
# Sender: SENDER_PHONE_NUMBER
# Text: MESSAGE_TEXT
# Status: READ_OR_UNREAD
# If action fails or HTTP request contains errors, "ERROR" will be replied.
# If message ID number is invalid, "ERROR" or "WRONG_NUMBER" will be replied.
# If no response from modem received, "TIMEOUT" will be replied.
# e.g.: http://192.168.1.1/cgi-bin/sms_read?number=1
# Router response is:
# Date: 2013-03-08 08:40:27
# Sender: +37012345678
# Text: Test
# Status: Read

/cgi-bin/sms_delete?number=MESSAGE_NUMBER
# Delete message by its position in memory.
# If action success and HTTP request is valid then response is "OK", otherwise response is "ERROR".
# If no response from modem received, "TIMEOUT" will be replied.
# e.g.: http://192.168.1.1/cgi-bin/sms_delete?number=0
# Response:
# OK