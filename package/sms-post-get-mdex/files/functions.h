#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#define DEBUG
#define DEFAULT_STRING_LENGTH 100

extern char sText[DEFAULT_STRING_LENGTH + 1];
extern char *cpStringNew, *cpBuf;
extern int iStringStriped;

int IsInteger(char *cpString, int iNegative);
int SafeToPrint(char *cpValue, int iInteger, int iLength);
char *StripString(char *cpString, char *cpLeave, int iLength);
void CheckMemoryAllocation(char *cpVar);
int CheckInterval(char *cpValue, int iLength, int iBeginning, int iEnding);
int IsClientLocal();

#endif
